#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/signal.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include "bitdef.h"

unsigned char *lefti=(unsigned char *)0xa000;
unsigned char *righti=(unsigned char *)0xa001;
unsigned char *dotsi=(unsigned char *)0xA004;
unsigned char *LED_REG=(unsigned char *)0xa006;
unsigned char *pA=(unsigned char *)0x8000;
unsigned char *pB=(unsigned char *)0x8001;
unsigned char *pC=(unsigned char *)0x8002;

volatile unsigned int delay_msec,need_zoom=500;

#include "delays.c"

SIGNAL(SIG_OUTPUT_COMPARE1A)
{
  PORTB ^= _10000000;
  if (delay_msec) delay_msec--;
  if (need_zoom) { need_zoom--; PORTB ^= _01000000; }
}

void hardware_init()
{
  DDRB = _11000000;
  MCUCR = _11000000;	// ext. RAM enable

  OCR1A = 114;		// period ~1ms
  TCCR1B = _00001011;	// CTC1, PS=64
  TIMSK |= (1 << OCIE1A);
  sei();

  *pA = 0x00;
  *pB = 0;
  *pC = 0xff;
  *LED_REG = 0;
  *dotsi = 0;
}

int main ()
{
  unsigned char i;
  unsigned char temp,j=0;

  hardware_init();

q:
  need_zoom = 50;

  for (i=0;i<16;i++) {
    temp = i | (i << 4);
    *lefti = temp;
    *righti = temp;
    delay(100);
  }
  
  for (i=0;i<4;i++) {
    temp = _00000001;
    do {
      *dotsi = temp;
      *LED_REG = temp;
      *pB = temp;
      *pC = i;
      delay(100);
      temp = (temp << 1);
    } while (temp);
  }

  *pB = 0;
  *pC = 0xff;
  *dotsi = 0xff;
  *LED_REG = 0;

  *pC = _11111110;
  for (j=1;j!=_00100000;j=(j<<1)) {
    *pA = j;
    for (i=1;i;i=(i<<1)) {
      *pC = ~i;
      delay(100);
    }
  }
  
  *dotsi = 0;
  *pC = 0xff;
  *pA = 0;
  goto q;
}
