void test_t0_t1(unsigned char tc_number)
{
  unsigned int i;
  unsigned char f_index=0,index;
  xdata unsigned int freq[16];

start:
  if (tc_number) {
    TL1=0; TH1=0;		// ���㫨�� t/c1
    TL0=0;		// 7,37MHz/12 = 614400Hz 0,01sec => 6144d ��� 1800h
    TH0=0xe8;		// �।��⠭���� t/c0 = 10000h - 1800h = e800h
    TMOD=_01010001;	// c/t1-counter, c/t0-timer, mode 1 for both
    TCON=_01010000;	// ��砫�                                   
    while(!TF0);		// ������� ��९������� t/c0 (�१ 0,1 ᥪ)
    TCON=0;		// �⮯
    i=(TH1<<8) | TL1;	// ����������
  }
  else {
    TL0=0; TH0=0;	// ���㫨�� t/c0
    TL1=0;		// 7,37MHz/12 = 614400Hz 0,01sec => 6144d ��� 1800h
    TH1=0xe8;		// �।��⠭���� t/c1 = 10000h - 1800h = e800h
    TMOD=_00010101;	// c/t1-timer, c/t0-counter, mode 1 for both
    TCON=_01010000;	// ��砫�                                   
    while(!TF1);		// ������� ��९������� t/c1 (�१ 0,1 ᥪ)
    TCON=0;		// �⮯
    i=(TH0<<8) | TL0;	// ����������
  }

  if (++f_index == 16) f_index=0;	//
  freq[f_index] = i;			// ��
  i=0;					//   ।
  for (index=0; index<16; index++){	//      ��
    i = i + freq[f_index];		//        ��
  }					//
  i = i >> 4;				//

  display_int(i,_01000000);
//  lefti = ((i/1000) << 4) | ((i/100)%10);
//  righti = (((i/10)%10) << 4) | (i%10);
  delay16(20000);

  if (key_scan() == 12) goto start;
}
