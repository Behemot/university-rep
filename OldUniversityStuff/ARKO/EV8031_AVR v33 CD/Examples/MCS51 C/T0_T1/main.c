#include <8051.h>
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\bitdef.h"
#include "t0_t1.c"

int main()
{
  unsigned char tc_number=1;

q:
  lefti=0x12;
  righti=0x34;
  new_dotsi=0;
  if (key_scan() < 12) { delay16(20000); test_t0_t1(tc_number); delay16(20000); tc_number=~tc_number; }
  goto q;
}

        