/*
�ணࠬ�� ����஢���� UART
������� INT1 �롨ࠥ��� ����� ��।��/�ਥ��

CFG1	CFG0	channel		direction

0	0	0(RS232_0 PC)	recieve/transmit
0	1	1(RS232_1)	recieve/transmit
1	0	2(RS485)	recieve
1	1	3(RS485)	transmit
*/

#include <8051.h>
#include "bitdef.h"
#include "ev8031.c"

static void uart_init()
{
  PCON = _10000000;	// SMOD = 1 Double Baud rate bit
  SCON = _01010000;	// 8-bit UART baud rate variable (set by timer)
  TMOD = _00100000;	// Timer 1 mode 2 - 8-bit Auto Reload
  TCON = 0;		// timer control register, byte operation
//  TH1 = 256-1;   // 38400
//  TH1 = 256-2;   // 19200
//  TH1 = 256-4;   // 9600
//  TH1 = 256-8;   // 4800
//  TH1 = 256-16;   // 2400
  TH1 = 256-32;   // 1200
  TR1 = 1;      // start serial timer T1
}

int main()
{
  char channel=0,tmp;

  new_dotsi=0;
  uart_init();
  UC_REG = channel;			// set UART channel
  lefti = channel;			// show UART channel on left part of static indication
q:
  if (!P3_3) {				// if INT1 button is pressed then
    if (++channel > 2) channel = 0;	// change UART channel
    UC_REG = channel;			// set UART channel
    lefti = channel;			// show UART channel on left part of static indication
    delay16(20000);
  }

  tmp = layout[kb_scan()];
  if (tmp != 12) {			// if button on keyboard is pressed then
    if (channel != 2) SBUF = tmp+48;	// transmit symbol
    else {
      UC_REG = 3;
      SBUF = tmp+48;
      TI = 0; while(!TI);
      UC_REG = 2;
    }
    delay16(20000);
  }

  if (RI) {				// if recieve complete then
    RI = 0;				// clear recieve flag
    tmp = SBUF - 48; righti = tmp;	// and show recieved symbol on right part of static indication
  }

  goto q;
}

