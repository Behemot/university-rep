xdata at 0xA000 unsigned char lefti; // left part of indication
xdata at 0xB000 unsigned char righti; // right part of indication
xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
xdata at 0x8000 unsigned char PA_REG;
xdata at 0x8001 unsigned char PB_REG;
xdata at 0x8002 unsigned char PC_REG;

xdata at 0x8004 unsigned char LCD_CMD;
xdata at 0x8005 unsigned char LCD_DATA;

xdata at 0xf000 unsigned char pDAC;
xdata at 0xe000 unsigned char pADC;

xdata at 0xA000 unsigned char DISPLAY[4];
xdata at 0xA004 unsigned char DC_REG;
xdata at 0xA005 unsigned char EDC_REG;
xdata at 0xA007 unsigned char SYS_CTL;

xdata at 0xB000 unsigned char DISPLAYB;
xdata at 0xC000 unsigned char UC_REG = 0;

#define KB_BASE 0x9000

#define MEM(Addr) (*((xdata unsigned char *)(Addr)))
#define HIGH(Value) ((unsigned char)((Value)>>8))
#define LOW(Value) ((unsigned char)(Value))

#define delay8(n) { unsigned char i=n; do { } while(--i); }
//#define delay16(n) { unsigned int i=n; do { } while(--i); }
//void delay8(unsigned char i) { do { } while(--i); }
void delay16(unsigned int i) { do { } while(--i); }

// One Wire
#define DQ P1_2
// One Wire

// I2C
#define SCL P1_1
#define SDA P1_0
#define I2CDELAY() delay8(3)
// I2C

unsigned char key_scan()
{
  _asm;
  	mov	r0, #0
  	mov	dptr, #KB_BASE+0xFE
  next_key_line:
  	; check scan line
  	movx	a, @dptr
  	jnb	acc.0, end_key_scan
  	inc	r0
  	jnb	acc.1, end_key_scan
  	inc	r0
  	jnb	acc.2, end_key_scan
  	inc	r0
  	jnb	acc.3, end_key_scan
  	inc	r0
	; goto next scan line
	mov	a, dpl
	rl	a
	mov	dpl, a
	cjne	a, #0xF7, next_key_line
  end_key_scan:
  	mov	dpl, r0
  _endasm;
}
/*
���⢥��⢨� ᪠� ���� �����頥���� �㭪樥� key_scan �ᯮ������� ������
0 4 8
1 5 9
2 6 10
3 7 11
*/
code unsigned char layout [] = {10,1,4,7, 0,2,5,8, 11,3,6,9, 12};
/*
	*	0	#
	1	2	3
	4	5	6
	7	8	9
*/
char kb_scan()
{
  static unsigned char prev_scan;
  static unsigned char timer;
//  unsigned char key=12;
  char scan;
  scan = key_scan();
  if (scan == prev_scan) {
    timer++;
//    if (timer == 0) key = scan;
    if (timer == 0) return (scan);
  } else {
    timer = 0;
  }
  prev_scan = scan;
//  return key;
  return 12;
}
void lights_off()
{
  LED_REG=0;
  pA=0;
  pB=0;
  pC=0xFF;
  new_dotsi=0x0f;
}
void display_int(unsigned int value, unsigned char dc_mask)
{
  static unsigned char buffer[4];
  unsigned char data *ptr = buffer;
  unsigned char i, dc = 0;
  /* �८�ࠧ������ */
  i = 4; do {
    char c;
    c = value % 10;
    value /= 10;
    *ptr++ = c;
  } while (--i);
  /* ��襭�� �㫥� */
  i = 3; do {
    --ptr;
    if (!*ptr) dc = (dc << 1) | 1;
    else break;
  } while (--i);
  DC_REG = dc | dc_mask;
  /* �뢮� */
  DISPLAY[0] = buffer[3] << 4 | buffer[2];
  DISPLAY[1] = buffer[1] << 4 | buffer[0];
}

void hd_turn_on()
{
  delay16(30000); LCD_CMD = 0x30;
  delay16(450); LCD_CMD = 0x30;
  delay16(150); LCD_CMD = 0x30;

  delay16(150); LCD_CMD = _01001000;

  delay16(150); LCD_DATA= _00001100;	//
  delay16(150); LCD_DATA= _00010010;	//                    
  delay16(150); LCD_DATA= _00001100;	//                    
  delay16(150); LCD_DATA= _00000000;	//                    
  delay16(150); LCD_DATA= _00000000;	// ᨬ��� �ࠤ�� \x04
  delay16(150); LCD_DATA= _00000000;	//                    
  delay16(150); LCD_DATA= _00000000;	//                    
  delay16(150); LCD_DATA= _00000000;	//                    

}
void hd_init()
{
  delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
  delay16(150); LCD_CMD = _00001100;	// display on, cursor off, blinc char at cursor pos
  delay16(150); LCD_CMD = 0x06;		// increment, no shift
  delay16(150); LCD_CMD = 1;		// clear display & cursor to home
  delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
}
void set_cursor(unsigned char x, unsigned char y)
{
  switch (y) {
    case 0: y=0;    break;
    case 1: y=0x40; break;
    case 2: y=0x0A; break;
    case 3: y=0x4A; break;
  }
  y=y+x;
  LCD_CMD = (_10000000 | y);		// set start position
}

void string_to_hd(unsigned char x, unsigned char y, char *string)
{
  set_cursor(x,y);
  while((y = *string++)) {
//    if (y > 0x7f) y=recode_table[y-0x80];
    LCD_DATA = y;
  }
}

