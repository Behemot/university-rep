
#include <8051.h>
#include <stdlib.h>
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\bitdef.h"
#include "knopki.c"

int main()
{
q:
  lefti=0x12;
  righti=0x34;
  new_dotsi=0;
  if (key_scan() < 12) { delay16(20000); test_knopki(); delay16(20000); }
  goto q;
}
        