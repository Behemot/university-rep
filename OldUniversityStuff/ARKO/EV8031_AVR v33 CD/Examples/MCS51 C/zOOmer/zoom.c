void t0_isr () interrupt 2	// interrupt service routine for T0 � 
{ � 
  P1_0 ^= 1;
}�

void t0_for_zoom_init()
{
  EA=0;
  TH0 = 0xff-60;
  TMOD = _00100010;	// c/t0-timer, mode 2
  TR0 = 1;
  ET0 = 1;
  EA = 1;
}

void test_zoom()
{
  unsigned char i=60;
  t0_for_zoom_init();
  do {
    if (!P3_2) i--;
    if (!P3_3) i++;
    lefti = i;
    TH0 = 0xff-i;
    delay16(1000);
  }while (key_scan() == 12);
}
