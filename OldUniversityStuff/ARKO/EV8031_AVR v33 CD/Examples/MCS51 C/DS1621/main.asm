;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Thu Oct 30 14:51:06 2003

;--------------------------------------------------------
	.module main
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _test_ds1621
	.globl _show_T
	.globl _i2c_rb
	.globl _i2c_wb
	.globl _i2c_stop
	.globl _i2c_start
	.globl _display_int
	.globl _lights_off
	.globl _kb_scan
	.globl _key_scan
	.globl _delay16
	.globl _DISPLAYB
	.globl _SYS_CTL
	.globl _EDC_REG
	.globl _DC_REG
	.globl _DISPLAY
	.globl _pADC
	.globl _pDAC
	.globl _PC_REG
	.globl _PB_REG
	.globl _PA_REG
	.globl _pC
	.globl _pB
	.globl _pA
	.globl _vv55RUS
	.globl _LED_REG
	.globl _new_dotsi
	.globl _dotsi
	.globl _righti
	.globl _lefti
	.globl _t
	.globl _sign
	.globl _display_int_PARM_2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_kb_scan_prev_scan_1_1:
	.ds 1
_kb_scan_timer_1_1:
	.ds 1
_display_int_buffer_1_1:
	.ds 4
_display_int_PARM_2::
	.ds 1
_sign::
	.ds 1
_t::
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_lefti	=	0xa000
_righti	=	0xb000
_dotsi	=	0xc000
_new_dotsi	=	0xa004
_LED_REG	=	0xa006
_vv55RUS	=	0x8003
_pA	=	0x8000
_pB	=	0x8001
_pC	=	0x8002
_PA_REG	=	0x8000
_PB_REG	=	0x8001
_PC_REG	=	0x8002
_pDAC	=	0xf000
_pADC	=	0xe000
_DISPLAY	=	0xa000
_DC_REG	=	0xa004
_EDC_REG	=	0xa005
_SYS_CTL	=	0xa007
_DISPLAYB	=	0xb000
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
;	main.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_vv55RUS
	mov	a,#0x80
	movx	@dptr,a
;	main.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_pA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_pB
	mov	a,#0xFF
	movx	@dptr,a
;	main.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'delay16'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:33: void delay16(unsigned int i) { do { } while(--i); }
;	-----------------------------------------
;	 function delay16
;	-----------------------------------------
_delay16:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	genAssign
00101$:
;	genDjnz
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00105$
	dec	r3
00105$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00101$
00106$:
00102$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'key_scan'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:45: unsigned char key_scan()
;	-----------------------------------------
;	 function key_scan
;	-----------------------------------------
_key_scan:
;	..\ev8031.lib\ev8031.c:68: _endasm;
;	genInline
;
	        mov r0, #0
	        mov dptr, #0x9000 +0xFE
  next_key_line:
	        ; check scan line
	        movx a, @dptr
	        jnb acc.0, end_key_scan
	        inc r0
	        jnb acc.1, end_key_scan
	        inc r0
	        jnb acc.2, end_key_scan
	        inc r0
	        jnb acc.3, end_key_scan
	        inc r0
	        ; goto next scan line
	        mov a, dpl
	        rl a
	        mov dpl, a
	        cjne a, #0xF7, next_key_line
  end_key_scan:
	        mov dpl, r0
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'kb_scan'
;------------------------------------------------------------
;prev_scan                 Allocated to in memory with name '_kb_scan_prev_scan_1_1'
;timer                     Allocated to in memory with name '_kb_scan_timer_1_1'
;scan                      Allocated to registers r2 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:77: char kb_scan()
;	-----------------------------------------
;	 function kb_scan
;	-----------------------------------------
_kb_scan:
;	..\ev8031.lib\ev8031.c:83: scan = key_scan();
;	genCall
	lcall	_key_scan
	mov	a,dpl
;	genAssign
;	..\ev8031.lib\ev8031.c:84: if (scan == prev_scan) {
;	genCmpEq
; Peephole 105   removed redundant mov
	mov  r2,a
	cjne	a,_kb_scan_prev_scan_1_1,00110$
	sjmp	00111$
00110$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00111$:
;	..\ev8031.lib\ev8031.c:85: timer++;
;	genPlus
;	genPlusIncr
	inc	_kb_scan_timer_1_1
;	..\ev8031.lib\ev8031.c:87: if (timer == 0) return (scan);
;	genCmpEq
	mov	a,_kb_scan_timer_1_1
; Peephole 162   removed sjmp by inverse jump logic
	jz   00113$
00112$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00113$:
;	genRet
	mov	dpl,r2
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00104$:
;	..\ev8031.lib\ev8031.c:89: timer = 0;
;	genAssign
	mov	_kb_scan_timer_1_1,#0x00
00105$:
;	..\ev8031.lib\ev8031.c:91: prev_scan = scan;
;	genAssign
	mov	_kb_scan_prev_scan_1_1,r2
;	..\ev8031.lib\ev8031.c:93: return 12;
;	genRet
	mov	dpl,#0x0C
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lights_off'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:95: void lights_off()
;	-----------------------------------------
;	 function lights_off
;	-----------------------------------------
_lights_off:
;	..\ev8031.lib\ev8031.c:97: LED_REG=0;
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
;	..\ev8031.lib\ev8031.c:98: pA=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
;	..\ev8031.lib\ev8031.c:99: pB=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219a removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_pA
	movx  @dptr,a
	mov   dptr,#_pB
	movx  @dptr,a
;	..\ev8031.lib\ev8031.c:100: pC=0xFF;
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:101: new_dotsi=0x0f;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x0F
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display_int'
;------------------------------------------------------------
;buffer                    Allocated to in memory with name '_display_int_buffer_1_1'
;dc_mask                   Allocated to in memory with name '_display_int_PARM_2'
;value                     Allocated to registers r2 r3 
;ptr                       Allocated to registers r4 
;i                         Allocated to registers 
;dc                        Allocated to registers r5 
;c                         Allocated to registers r6 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:103: void display_int(unsigned int value, unsigned char dc_mask)
;	-----------------------------------------
;	 function display_int
;	-----------------------------------------
_display_int:
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	..\ev8031.lib\ev8031.c:106: unsigned char data *ptr = buffer;
;	genAssign
	mov	r4,#_display_int_buffer_1_1
;	..\ev8031.lib\ev8031.c:107: unsigned char i, dc = 0;
;	genAssign
	mov	r5,#0x00
;	..\ev8031.lib\ev8031.c:109: i = 4; do {
;	genAssign
	mov	ar0,r4
;	genAssign
	mov	r4,#0x04
00101$:
;	..\ev8031.lib\ev8031.c:111: c = value % 10;
;	genAssign
	clr	a
	mov	(__moduint_PARM_2 + 1),a
	mov	__moduint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar0
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	pop	ar0
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
;	..\ev8031.lib\ev8031.c:112: value /= 10;
;	genAssign
	clr	a
	mov	(__divuint_PARM_2 + 1),a
	mov	__divuint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar0
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
	pop	ar0
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	r2,a
	mov	r3,b
;	..\ev8031.lib\ev8031.c:113: *ptr++ = c;
;	genPointerSet
;	genNearPointerSet
	mov	@r0,ar6
	inc	r0
;	..\ev8031.lib\ev8031.c:114: } while (--i);
;	genDjnz
	djnz	r4,00116$
	sjmp	00117$
00116$:
	ljmp	00101$
00117$:
;	..\ev8031.lib\ev8031.c:116: i = 3; do {
;	genAssign
	mov	r2,#0x03
;	genAssign
00107$:
;	..\ev8031.lib\ev8031.c:117: --ptr;
;	genMinus
;	genMinusDec
	dec	r0
;	..\ev8031.lib\ev8031.c:118: if (!*ptr) dc = (dc << 1) | 1;
;	genPointerGet
;	genNearPointerGet
	mov	ar3,@r0
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00109$
00118$:
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r5
	add	a,acc
	mov	r3,a
;	genOr
	mov	a,#0x01
	orl	a,r3
	mov	r5,a
;	..\ev8031.lib\ev8031.c:120: } while (--i);
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00107$
00119$:
00120$:
00109$:
;	..\ev8031.lib\ev8031.c:121: DC_REG = dc | dc_mask;
;	genOr
	mov	dptr,#_DC_REG
	mov	a,_display_int_PARM_2
	orl	a,r5
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:123: DISPLAY[0] = buffer[3] << 4 | buffer[2];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0003 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,0x0002 + _display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#_DISPLAY
	mov	a,r2
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:124: DISPLAY[1] = buffer[1] << 4 | buffer[0];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0001 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,_display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#(_DISPLAY + 0x0001)
	mov	a,r2
	movx	@dptr,a
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_start'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function i2c_start
;	-----------------------------------------
_i2c_start:
;	..\ev8031.lib\i2c.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	setb	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	..\ev8031.lib\i2c.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	clr	_P1_0
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_stop'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function i2c_stop
;	-----------------------------------------
_i2c_stop:
;	..\ev8031.lib\i2c.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	clr	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	..\ev8031.lib\i2c.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_wb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:15: xdata at 0xe000 unsigned char pADC;
;	-----------------------------------------
;	 function i2c_wb
;	-----------------------------------------
_i2c_wb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:17: xdata at 0xA000 unsigned char DISPLAY[4];
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:18: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	mov	r4,#0x08
00103$:
;	..\ev8031.lib\i2c.c:19: xdata at 0xA005 unsigned char EDC_REG;
;	genAnd
	mov	a,r2
	mov	c,acc.7
	mov	_P1_0,c
;	..\ev8031.lib\i2c.c:20: xdata at 0xA007 unsigned char SYS_CTL;
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	add	a,acc
	mov	r2,a
;	..\ev8031.lib\i2c.c:21: 
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00123$:
00124$:
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00125$:
00126$:
;	..\ev8031.lib\i2c.c:23: 
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00103$
00127$:
00128$:
;	..\ev8031.lib\i2c.c:24: #define KB_BASE 0x9000
;	genAssign
	setb	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00106$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00106$
00129$:
00130$:
;	..\ev8031.lib\i2c.c:25: 
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P1_0,00108$
00131$:
;	genAssign
	mov	r3,#0x01
00108$:
;	..\ev8031.lib\i2c.c:26: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00109$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00109$
00132$:
00133$:
;	..\ev8031.lib\i2c.c:27: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genRet
	mov	dpl,r3
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_rb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:30: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	-----------------------------------------
;	 function i2c_rb
;	-----------------------------------------
_i2c_rb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:32: //void delay8(unsigned char i) { do { } while(--i); }
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:33: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
	setb	_P1_0
;	..\ev8031.lib\i2c.c:34: 
;	genAssign
	mov	r4,#0x08
00103$:
;	..\ev8031.lib\i2c.c:35: // One Wire
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r3,a
;	..\ev8031.lib\i2c.c:36: #define DQ P1_2
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00123$:
00124$:
;	..\ev8031.lib\i2c.c:37: // One Wire
;	genAssign
	clr	a
	mov	c,_P1_0
	rlc	a
;	genOr
; Peephole 105   removed redundant mov
	mov  r5,a
	orl	ar3,a
;	..\ev8031.lib\i2c.c:38: 
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00125$:
00126$:
;	..\ev8031.lib\i2c.c:40: #define SCL P1_1
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00103$
00127$:
00128$:
;	..\ev8031.lib\i2c.c:41: #define SDA P1_0
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00107$
00129$:
;	genAssign
	clr	_P1_0
00107$:
;	..\ev8031.lib\i2c.c:42: #define I2CDELAY() delay8(3)
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00108$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00108$
00130$:
00131$:
;	genAssign
	clr	_P1_1
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00109$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00109$
00132$:
00133$:
;	..\ev8031.lib\i2c.c:43: // I2C
;	genRet
	mov	dpl,r3
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'show_T'
;------------------------------------------------------------
;------------------------------------------------------------
;	ds1621.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	-----------------------------------------
;	 function show_T
;	-----------------------------------------
_show_T:
;	genReceive
	mov	r2,dpl
;	ds1621.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_dotsi
	mov	a,_sign
	movx	@dptr,a
;	ds1621.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x63
	subb	a,r2
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00102$
00111$:
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xF1
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00102$:
;	ds1621.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
00103$:
;	ds1621.c:13: 
;	genDiv
;	genDivOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
;	genIfx
; Peephole 105   removed redundant mov
	mov  r3,a
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00105$
00112$:
;	ds1621.c:14: xdata at 0xf000 unsigned char pDAC;
;	genMod
;	genModOneByte
	mov	b,#0x0A
	mov	a,r3
	div	ab
	mov	r3,b
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	swap	a
	anl	a,#0xf0
	mov	r3,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00105$:
;	ds1621.c:15: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	r3,#0xF0
00106$:
;	ds1621.c:17: xdata at 0xA000 unsigned char DISPLAY[4];
;	genMod
;	genModOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
	mov	r2,b
;	genOr
	mov	dptr,#_righti
	mov	a,r2
	orl	a,r3
	movx	@dptr,a
;	ds1621.c:18: xdata at 0xA004 unsigned char DC_REG;
00107$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_ds1621'
;------------------------------------------------------------
;no_sensor                 Allocated to registers r2 
;------------------------------------------------------------
;	ds1621.c:21: 
;	-----------------------------------------
;	 function test_ds1621
;	-----------------------------------------
_test_ds1621:
;	ds1621.c:26: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
00101$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00126$
00126$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00101$
00127$:
;	ds1621.c:27: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
;	ds1621.c:28: #define LOW(Value) ((unsigned char)(Value))
00104$:
;	ds1621.c:30: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genCall
	lcall	_i2c_start
;	ds1621.c:31: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
;	ds1621.c:32: //void delay8(unsigned char i) { do { } while(--i); }
;	genCall
	mov	dpl,#0xAC
	lcall	_i2c_wb
;	ds1621.c:33: void delay16(unsigned int i) { do { } while(--i); }
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	ds1621.c:34: 
;	genCall
	lcall	_i2c_stop
;	ds1621.c:36: #define DQ P1_2
;	genCall
	lcall	_i2c_start
;	ds1621.c:37: // One Wire
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
;	ds1621.c:38: 
;	genCall
	mov	dpl,#0xEE
	lcall	_i2c_wb
;	ds1621.c:39: // I2C
;	genCall
	lcall	_i2c_stop
;	ds1621.c:40: #define SCL P1_1
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	ds1621.c:42: #define I2CDELAY() delay8(3)
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0xEA)<<8) + 0x60)
	lcall	_delay16
;	ds1621.c:44: 
;	genAssign
	mov	dptr,#_LED_REG
	mov	a,#0x01
	movx	@dptr,a
;	ds1621.c:45: unsigned char key_scan()
;	genCall
	lcall	_i2c_start
;	ds1621.c:46: {
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
	mov	r2,dpl
;	genNot
	mov	a,r2
	cjne	a,#0x01,00128$
00128$:
	clr	a
	rlc	a
	mov	r2,a
;	ds1621.c:48: mov	r0, #0
;	genCall
	mov	dpl,#0xAA
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	ds1621.c:49: mov	dptr, #KB_BASE+0xFE
;	genCall
	push	ar2
	lcall	_i2c_start
	pop	ar2
;	ds1621.c:50: next_key_line:
;	genCall
	mov	dpl,#0x91
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	ds1621.c:51: ; check scan line
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar2
;	genAssign
	mov	_t,a
;	ds1621.c:52: movx	a, @dptr
;	genCall
	push	ar2
	lcall	_i2c_stop
	pop	ar2
;	ds1621.c:54: inc	r0
;	genCmpLt
;	genCmp
	mov	a,_t
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  acc.7,00106$
00129$:
;	genAssign
	mov	_sign,#0xFF
;	genUminus
	clr	c
	clr	a
	subb	a,_t
	mov	_t,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00107$
00106$:
;	genAssign
	mov	_sign,#0x00
00107$:
;	ds1621.c:56: inc	r0
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00109$
00130$:
;	genAssign
	mov	dptr,#_dotsi
	mov	a,#0xFF
	movx	@dptr,a
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0xF0
	movx	@dptr,a
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xFF
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00110$
00109$:
;	ds1621.c:57: jnb	acc.2, end_key_scan
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x03
	movx	@dptr,a
;	genAssign
	mov	dpl,_t
;	genCall
	lcall	_show_T
00110$:
;	ds1621.c:59: jnb	acc.3, end_key_scan
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00131$
00131$:
;	genIfxJump
	jc	00132$
	ljmp	00104$
00132$:
;	genCall
	lcall	_lights_off
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
00111$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00133$
00133$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00111$
00134$:
;	genRet
;	ds1621.c:61: ; goto next scan line
00116$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCall
	lcall	_lights_off
;	main.c:11: xdata at 0x8001 unsigned char PB_REG;
00101$:
;	main.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x12
	movx	@dptr,a
;	main.c:13: 
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x34
	movx	@dptr,a
;	main.c:14: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:15: xdata at 0xe000 unsigned char pADC;
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00108$
00108$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00101$
00109$:
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	genCall
	lcall	_test_ds1621
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	main.c:16: 
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
	.area CSEG    (CODE)
	.area XINIT   (CODE)
