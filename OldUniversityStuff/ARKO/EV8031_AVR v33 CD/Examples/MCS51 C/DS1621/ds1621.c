unsigned char sign;
char t;

void show_T(unsigned char t)
{
  unsigned char temp;

  dotsi=sign;

  if (t > 99) lefti=0xf1;
  else lefti=0xff;

  if (t / 10) {
    temp = (t/10%10)<<4;
  } else temp=0xf0;

  temp |= (t%10);
  righti = temp;
}

void test_ds1621()
{
  unsigned char no_sensor;
//  unsigned int tmp;

  while (key_scan() < 12);
  delay16(1000);
start:

  i2c_start();
  i2c_wb(_10010000);
  i2c_wb(0xac);// access config
  i2c_wb(0x00); 
  i2c_stop();

  i2c_start();
  i2c_wb(_10010000);
  i2c_wb(0xee);// start convert T
  i2c_stop();
  LED_REG = 0;

  delay16(60000);

  LED_REG = 1;
  i2c_start();
  no_sensor = !i2c_wb(_10010000);

  i2c_wb(0xaa);// read temperature
  i2c_start();
  i2c_wb(_10010001);
  t=i2c_rb(0);
  i2c_stop();

  if (t < 0) { sign = 0xff; t = -t; } else sign = 0;

  if (no_sensor) { dotsi=0xff; new_dotsi=0xf0; lefti=0xff; righti=0xff; }
  else { new_dotsi=0x03; show_T(t); }

  if (key_scan() < 12) { lights_off(); delay16(1000); while (key_scan() < 12); return; }

  goto start;
}



