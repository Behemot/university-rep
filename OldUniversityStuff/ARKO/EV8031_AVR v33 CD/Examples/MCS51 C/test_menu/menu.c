#include "..\ev8031.lib\zoom.c"

#include "..\t0_t1\t0_t1.c"
#include "..\leds\leds.c"
#include "..\e40281\e40281.c"
#include "..\ta07-11\ta07_11.c"
#include "..\ad7801\adc.c"
#include "..\knopki\knopki.c"
#include "..\triangle.dac\dac.c"
#include "..\ds1307_\ds1307.c"
#include "..\ds1621\ds1621.c"
#include "..\testi\testi.c"
#include "..\at24\at24.c"
#include "..\ds1822\ds1822.c"
#include "..\uart\uart.c"

void test_menu()
{
  unsigned char key=0,test_number[2] = {0, 0},index=0,number=0;

  new_dotsi=_11111111;
  while(key_scan() < 12);
  delay16(20000);
  new_dotsi=_00000011;
  t0_for_zoom_init();
  hd_turn_on(); hd_init(); string_to_hd(0,3,"v3.21");
start:
  key = layout[kb_scan()];
  if (key != 12) need_zoom=1000;
  switch (key)
  {
    case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: 
      test_number[1]=test_number[0];      test_number[0]=key;
      if (index) index=0;
      else index=1;
      number = (test_number[1] << 4) | (test_number[0]);
      hd_init();
      switch (number)
      {
        case 0x01:
          string_to_hd(3,0,"Test");          string_to_hd(0,1,"generator");          string_to_hd(4,2,"T0");        break;        case 0x02:
          string_to_hd(3,0,"Test");
          string_to_hd(0,1,"generator");
          string_to_hd(4,2,"T1");
        break;        case 0x03:
          string_to_hd(3,0,"Test");
          string_to_hd(3,1,"leds");
 	break;
        case 0x04:
          string_to_hd(3,0,"Test");
          string_to_hd(1,1,"dinamic");
          string_to_hd(0,2,"indication");
 	break;
        case 0x05:
          string_to_hd(3,0,"Test");
          string_to_hd(2,1,"matrix");
          string_to_hd(0,2,"indication");
 	break;
        case 0x06:
          string_to_hd(3,0,"Test");
          string_to_hd(1,1,"AD7801 in");
          string_to_hd(1,2,"ADC mode");
 	break;
        case 0x07:
          string_to_hd(3,0,"Test");          string_to_hd(0,1,"buttons on");          string_to_hd(0,2,"P3.2 P3.3");          string_to_hd(0,3,"INT0 INT1");
 	break;
        case 0x08:
          string_to_hd(3,0,"Test");
          string_to_hd(1,1,"AD7801 in");
          string_to_hd(1,2,"DAC mode");
 	break;
        case 0x09:
          string_to_hd(3,0,"Test");
          string_to_hd(2,1,"static");
          string_to_hd(0,2,"indication");
 	break;
        case 0x10:
          string_to_hd(3,0,"Test");
          string_to_hd(0,1,"RTC DS1307");//          string_to_hd(2,2,"clock");//          string_to_hd(2,3,"DS1307"); 	break;
        case 0x11:
          string_to_hd(3,0,"Test");
          string_to_hd(0,1,"t\x01""C sensor");
          string_to_hd(0,2,"DS1621(31)");
 	break;
        case 0x12:
          string_to_hd(3,0,"Test");
          string_to_hd(0,2,"AT24series");
          string_to_hd(2,1,"EEPROM");
 	break;
        case 0x13:
          string_to_hd(3,0,"Test");
          string_to_hd(0,1,"t\x01""C sensor");
          string_to_hd(2,2,"DS1822");
 	break;
        case 0x14:
          string_to_hd(0,0,"Test UART");
          string_to_hd(0,1,"RS-232 0&1");          string_to_hd(2,2,"RS-485"); 	break;
      }

    break;
    case 10: index=0; test_number[0]=0; test_number[1]=0; break;
    case 11:
      delay16(40000);
      EA=0;
      switch (number)
      {
       case 0x01: test_t0_t1(0);	break;
       case 0x02: test_t0_t1(1);	break;
       case 0x03: test_leds();		break;
       case 0x04: test_e40281();	break;
       case 0x05: test_ta07_11();	break;
       case 0x06: test_adc();		break;
       case 0x07: test_knopki();	break;
       case 0x08: test_dac();		break;
       case 0x09: test_i();		break;
       case 0x10: test_ds1307();	break;
       case 0x11: test_ds1621();	break;
       case 0x12: test_at24();		break;
       case 0x13: test_ds1822();	break;
       case 0x14: test_uart();		break;

       case 0x99: return; break;
      }
      t0_for_zoom_init();
      index=0; test_number[0]=0; test_number[1]=0; need_zoom = 1000;
    break;
  }
  lights_off();
  new_dotsi=_00000011;
  righti = number;

  if (key != 12) delay16(20000);
  goto start;}