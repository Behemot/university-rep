void test_leds()
{
  unsigned char temp,key_pressed=0;

  lefti=0x33;
  righti=0x33;
  new_dotsi=0;

start:
  temp=1;
  do
  {
    LED_REG=temp;
    delay16(10000);
    temp = temp << 1;
    if (key_scan() < 12) key_pressed=~key_pressed;
  }
  while(temp && (key_scan() == 12));
  if (!temp) goto start;
}
