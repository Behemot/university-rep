#include <8051.h>
#include "..\ev8031.lib\bitdef.h"
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\i2c.c"
#include "..\ev8031.lib\ow.c"
#include "..\test_menu\menu.c"

static unsigned int memory_size;

static void hardware_init()
{
  /* ����ன�� ��᫥����⥫쭮�� ���� */
  PCON = 0x80;  /* ��⠭����� ��� SMOD � ॣ���� PCON */
  SCON = 0x50;
  TMOD = 0x21;  /* ����ன�� ⠩��� ��� ��᫥����⥫쭮�� ���� */
  TCON = 0;     /* timer control register, byte operation */

  TH1 = 0xFC;   /* serial reload value, 9,600 baud at 7.3728 Mhz */
  TR1 = 1;      /* start serial timer */
  lights_off();
}

static void memory_test()
{
  _asm;
  	mov	dptr, #_DC_REG
  	mov	a, #0x1F
  	movx	@dptr, a

	; write memory
  	mov	dptr, #0
  next_byte_load:
	mov	a, dph
	add	a, dpl
	movx	@dptr, a
	inc	dptr
	mov	a, dph
	cjne	a, #0x80, next_byte_load

	mov	dptr, #_DC_REG
	mov	a, #0x3F
	movx	@dptr, a

	; verify memory
	mov	dptr, #0
  next_byte_verify:
  	movx	a, @dptr
  	mov	r0, a
  	mov	a, dph
  	add	a, dpl
  	cjne	a, ar0, exit_test

	inc	dptr
	mov	a, dph
	cjne	a, #0x80, next_byte_verify

  exit_test:
  	mov	_memory_size, dpl
  	mov	_memory_size+1, dph
  _endasm;
  display_int(memory_size >> 7,0);
}

int main()
{
  unsigned int addr=0,timeout=0,i_timeout=0;
  unsigned char checksum=0,dc_switch=0;
start:
  hardware_init();
  LED_REG = 0;
  ds1307_init();
  hd_turn_on(); hd_init();
  string_to_hd(3,0,"Open"); string_to_hd(2,1,"System"); string_to_hd(0,3,"EV8031/AVR");
  memory_test();
  if (key_scan() < 12) { test_menu(); goto start; }
  for(;;) {
    if (RI) {
      char c;
      c = SBUF; RI = 0;
      MEM(addr) = c;
      checksum += c;
      DISPLAY[1] = checksum;
      DC_REG = 0x83;
      addr++;
      timeout = 1;
    }
    if (timeout) {
      timeout++;
      if (timeout == 30000) {
        timeout = 0;
        DC_REG = 0x00;
        /* ����� �ணࠬ�� */
        if (checksum == 0) {
          DISPLAY[0] = 0xFF;
          SYS_CTL = 0x01;
        }
      }
    }

    if ((i_timeout++) > 20000) {
      i_timeout=0;
      if (dc_switch = ~dc_switch) DC_REG = _00000001;
      else DC_REG = 0x0f;
    }

  }
}
