;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Fri Oct 08 18:34:57 2004

;--------------------------------------------------------
	.module boot
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _set_cursor_PARM_2
	.globl _layout
	.globl _main
	.globl _test_menu
	.globl _test_uart
	.globl _test_i
	.globl _test_ds1621
	.globl _show_T
	.globl _test_ds1307
	.globl _get_time
	.globl _ds1307_init
	.globl _bcd2bin
	.globl _test_dac
	.globl _test_knopki
	.globl _test_adc
	.globl _test_ta07_11
	.globl _test_e40281
	.globl _test_leds
	.globl _test_t0_t1
	.globl _t0_for_zoom_init
	.globl _t0_isr
	.globl _ow_wb
	.globl _ow_rb
	.globl _ow_wbit
	.globl _ow_rbit
	.globl _ow_reset
	.globl _i2c_rb
	.globl _i2c_wb
	.globl _i2c_stop
	.globl _i2c_start
	.globl _string_to_hd
	.globl _set_cursor
	.globl _hd_init
	.globl _hd_turn_on
	.globl _display_int
	.globl _lights_off
	.globl _kb_scan
	.globl _key_scan
	.globl _delay16
	.globl _UC_REG
	.globl _DISPLAYB
	.globl _SYS_CTL
	.globl _EDC_REG
	.globl _DC_REG
	.globl _DISPLAY
	.globl _pADC
	.globl _pDAC
	.globl _LCD_DATA
	.globl _LCD_CMD
	.globl _PC_REG
	.globl _PB_REG
	.globl _PA_REG
	.globl _pC
	.globl _pB
	.globl _pA
	.globl _vv55RUS
	.globl _LED_REG
	.globl _new_dotsi
	.globl _dotsi
	.globl _righti
	.globl _lefti
	.globl _t
	.globl _sign
	.globl _hour_counter
	.globl _min_counter
	.globl _time
	.globl _need_zoom
	.globl _string_to_hd_PARM_3
	.globl _string_to_hd_PARM_2
	.globl _display_int_PARM_2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_kb_scan_prev_scan_1_1:
	.ds 1
_kb_scan_timer_1_1:
	.ds 1
_display_int_buffer_1_1:
	.ds 4
_display_int_PARM_2::
	.ds 1
_string_to_hd_PARM_2::
	.ds 1
_string_to_hd_PARM_3::
	.ds 3
_need_zoom::
	.ds 2
_test_t0_t1_tc_number_1_1::
	.ds 1
_test_t0_t1_f_index_1_1::
	.ds 1
_test_t0_t1_index_1_1::
	.ds 1
_test_adc_last_1_1::
	.ds 2
_test_adc_display_counter_1_1::
	.ds 2
_time::
	.ds 3
_min_counter::
	.ds 1
_hour_counter::
	.ds 1
_sign::
	.ds 1
_t::
	.ds 1
_test_at24_i_1_1::
	.ds 1
_test_at24_result_1_1::
	.ds 1
_test_at24_testbyte_1_1::
	.ds 1
_test_at24_volume_1_1::
	.ds 1
_test_at24_shifted_one_1_1::
	.ds 2
_get_T_scratchpad_1_1::
	.ds 2
_test_menu_test_number_1_1::
	.ds 2
_memory_size:
	.ds 2
_main_addr_1_1::
	.ds 2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
_set_cursor_PARM_2::
	.ds 1
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_lefti	=	0xa000
_righti	=	0xb000
_dotsi	=	0xc000
_new_dotsi	=	0xa004
_LED_REG	=	0xa006
_vv55RUS	=	0x8003
_pA	=	0x8000
_pB	=	0x8001
_pC	=	0x8002
_PA_REG	=	0x8000
_PB_REG	=	0x8001
_PC_REG	=	0x8002
_LCD_CMD	=	0x8004
_LCD_DATA	=	0x8005
_pDAC	=	0xf000
_pADC	=	0xe000
_DISPLAY	=	0xa000
_DC_REG	=	0xa004
_EDC_REG	=	0xa005
_SYS_CTL	=	0xa007
_DISPLAYB	=	0xb000
_UC_REG	=	0xc000
_test_t0_t1_freq_1_1::
	.ds 32
_test_adc_sample_array_1_1::
	.ds 64
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_t0_isr
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
;	boot.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	genAssign
	clr	a
	mov	(_need_zoom + 1),a
	mov	_need_zoom,a
;	boot.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_vv55RUS
	mov	a,#0x80
	movx	@dptr,a
;	boot.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_pA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_pB
	mov	a,#0xFF
	movx	@dptr,a
;	boot.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	boot.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAssign
	mov	dptr,#_UC_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'delay16'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	-----------------------------------------
;	 function delay16
;	-----------------------------------------
_delay16:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	genAssign
00101$:
;	genDjnz
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00105$
	dec	r3
00105$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00101$
00106$:
00102$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'key_scan'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:49: unsigned char key_scan()
;	-----------------------------------------
;	 function key_scan
;	-----------------------------------------
_key_scan:
;	..\ev8031.lib\ev8031.c:72: _endasm;
;	genInline
;
	        mov r0, #0
	        mov dptr, #0x9000 +0xFE
  next_key_line:
	        ; check scan line
	        movx a, @dptr
	        jnb acc.0, end_key_scan
	        inc r0
	        jnb acc.1, end_key_scan
	        inc r0
	        jnb acc.2, end_key_scan
	        inc r0
	        jnb acc.3, end_key_scan
	        inc r0
	        ; goto next scan line
	        mov a, dpl
	        rl a
	        mov dpl, a
	        cjne a, #0xF7, next_key_line
  end_key_scan:
	        mov dpl, r0
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'kb_scan'
;------------------------------------------------------------
;prev_scan                 Allocated to in memory with name '_kb_scan_prev_scan_1_1'
;timer                     Allocated to in memory with name '_kb_scan_timer_1_1'
;scan                      Allocated to registers r2 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:97: char kb_scan()
;	-----------------------------------------
;	 function kb_scan
;	-----------------------------------------
_kb_scan:
;	..\ev8031.lib\ev8031.c:103: scan = key_scan();
;	genCall
	lcall	_key_scan
	mov	a,dpl
;	genAssign
;	..\ev8031.lib\ev8031.c:104: if (scan == prev_scan) {
;	genCmpEq
; Peephole 105   removed redundant mov
	mov  r2,a
	cjne	a,_kb_scan_prev_scan_1_1,00110$
	sjmp	00111$
00110$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00111$:
;	..\ev8031.lib\ev8031.c:105: timer++;
;	genPlus
;	genPlusIncr
	inc	_kb_scan_timer_1_1
;	..\ev8031.lib\ev8031.c:107: if (timer == 0) return (scan);
;	genCmpEq
	mov	a,_kb_scan_timer_1_1
; Peephole 162   removed sjmp by inverse jump logic
	jz   00113$
00112$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00113$:
;	genRet
	mov	dpl,r2
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00104$:
;	..\ev8031.lib\ev8031.c:109: timer = 0;
;	genAssign
	mov	_kb_scan_timer_1_1,#0x00
00105$:
;	..\ev8031.lib\ev8031.c:111: prev_scan = scan;
;	genAssign
	mov	_kb_scan_prev_scan_1_1,r2
;	..\ev8031.lib\ev8031.c:113: return 12;
;	genRet
	mov	dpl,#0x0C
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lights_off'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:115: void lights_off()
;	-----------------------------------------
;	 function lights_off
;	-----------------------------------------
_lights_off:
;	..\ev8031.lib\ev8031.c:117: LED_REG=0;
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
;	..\ev8031.lib\ev8031.c:118: pA=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
;	..\ev8031.lib\ev8031.c:119: pB=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219a removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_pA
	movx  @dptr,a
	mov   dptr,#_pB
	movx  @dptr,a
;	..\ev8031.lib\ev8031.c:120: pC=0xFF;
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:121: new_dotsi=0x0f;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x0F
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display_int'
;------------------------------------------------------------
;buffer                    Allocated to in memory with name '_display_int_buffer_1_1'
;dc_mask                   Allocated to in memory with name '_display_int_PARM_2'
;value                     Allocated to registers r2 r3 
;ptr                       Allocated to registers r4 
;i                         Allocated to registers 
;dc                        Allocated to registers r5 
;c                         Allocated to registers r6 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:123: void display_int(unsigned int value, unsigned char dc_mask)
;	-----------------------------------------
;	 function display_int
;	-----------------------------------------
_display_int:
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	..\ev8031.lib\ev8031.c:126: unsigned char data *ptr = buffer;
;	genAssign
	mov	r4,#_display_int_buffer_1_1
;	..\ev8031.lib\ev8031.c:127: unsigned char i, dc = 0;
;	genAssign
	mov	r5,#0x00
;	..\ev8031.lib\ev8031.c:129: i = 4; do {
;	genAssign
	mov	ar0,r4
;	genAssign
	mov	r4,#0x04
00101$:
;	..\ev8031.lib\ev8031.c:131: c = value % 10;
;	genAssign
	clr	a
	mov	(__moduint_PARM_2 + 1),a
	mov	__moduint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar0
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	pop	ar0
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
;	..\ev8031.lib\ev8031.c:132: value /= 10;
;	genAssign
	clr	a
	mov	(__divuint_PARM_2 + 1),a
	mov	__divuint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar0
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
	pop	ar0
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	r2,a
	mov	r3,b
;	..\ev8031.lib\ev8031.c:133: *ptr++ = c;
;	genPointerSet
;	genNearPointerSet
	mov	@r0,ar6
	inc	r0
;	..\ev8031.lib\ev8031.c:134: } while (--i);
;	genDjnz
	djnz	r4,00116$
	sjmp	00117$
00116$:
	ljmp	00101$
00117$:
;	..\ev8031.lib\ev8031.c:136: i = 3; do {
;	genAssign
	mov	r2,#0x03
;	genAssign
00107$:
;	..\ev8031.lib\ev8031.c:137: --ptr;
;	genMinus
;	genMinusDec
	dec	r0
;	..\ev8031.lib\ev8031.c:138: if (!*ptr) dc = (dc << 1) | 1;
;	genPointerGet
;	genNearPointerGet
	mov	ar3,@r0
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00109$
00118$:
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r5
	add	a,acc
	mov	r3,a
;	genOr
	mov	a,#0x01
	orl	a,r3
	mov	r5,a
;	..\ev8031.lib\ev8031.c:140: } while (--i);
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00107$
00119$:
00120$:
00109$:
;	..\ev8031.lib\ev8031.c:141: DC_REG = dc | dc_mask;
;	genOr
	mov	dptr,#_DC_REG
	mov	a,_display_int_PARM_2
	orl	a,r5
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:143: DISPLAY[0] = buffer[3] << 4 | buffer[2];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0003 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,0x0002 + _display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#_DISPLAY
	mov	a,r2
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:144: DISPLAY[1] = buffer[1] << 4 | buffer[0];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0001 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,_display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#(_DISPLAY + 0x0001)
	mov	a,r2
	movx	@dptr,a
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_turn_on'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:147: void hd_turn_on()
;	-----------------------------------------
;	 function hd_turn_on
;	-----------------------------------------
_hd_turn_on:
;	..\ev8031.lib\ev8031.c:149: delay16(30000); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:150: delay16(450); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x01)<<8) + 0xC2)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:151: delay16(150); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:153: delay16(150); LCD_CMD = _01001000;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x48
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:155: delay16(150); LCD_DATA= _00001100;	//
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:156: delay16(150); LCD_DATA= _00010010;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x12
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:157: delay16(150); LCD_DATA= _00001100;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:158: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:159: delay16(150); LCD_DATA= _00000000;	// ᨬ��� �ࠤ�� \x04
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:160: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:161: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:162: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:165: void hd_init()
;	-----------------------------------------
;	 function hd_init
;	-----------------------------------------
_hd_init:
;	..\ev8031.lib\ev8031.c:167: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:168: delay16(150); LCD_CMD = _00001100;	// display on, cursor off, blinc char at cursor pos
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:169: delay16(150); LCD_CMD = 0x06;		// increment, no shift
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x06
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:170: delay16(150); LCD_CMD = 1;		// clear display & cursor to home
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x01
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:171: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_cursor'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:173: void set_cursor(unsigned char x, unsigned char y)
;	-----------------------------------------
;	 function set_cursor
;	-----------------------------------------
_set_cursor:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\ev8031.c:175: switch (y) {
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,_set_cursor_PARM_2
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00105$
00109$:
;	genJumpTab
	mov	a,_set_cursor_PARM_2
	add	a,acc
	add	a,_set_cursor_PARM_2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	..\ev8031.lib\ev8031.c:176: case 0: y=0;    break;
00101$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x00
;	..\ev8031.lib\ev8031.c:177: case 1: y=0x40; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00102$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x40
;	..\ev8031.lib\ev8031.c:178: case 2: y=0x0A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00103$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x0A
;	..\ev8031.lib\ev8031.c:179: case 3: y=0x4A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00104$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x4A
;	..\ev8031.lib\ev8031.c:180: }
00105$:
;	..\ev8031.lib\ev8031.c:181: y=y+x;
;	genPlus
	mov	a,ar2
	add	a,_set_cursor_PARM_2
;	..\ev8031.lib\ev8031.c:182: LCD_CMD = (_10000000 | y);		// set start position
;	genOr
	mov	dptr,#_LCD_CMD
	orl	a,#0x80
	movx	@dptr,a
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'string_to_hd'
;------------------------------------------------------------
;y                         Allocated to in memory with name '_string_to_hd_PARM_2'
;string                    Allocated to in memory with name '_string_to_hd_PARM_3'
;x                         Allocated to registers 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:185: void string_to_hd(unsigned char x, unsigned char y, char *string)
;	-----------------------------------------
;	 function string_to_hd
;	-----------------------------------------
_string_to_hd:
;	genReceive
;	..\ev8031.lib\ev8031.c:187: set_cursor(x,y);
;	genAssign
	mov	_set_cursor_PARM_2,_string_to_hd_PARM_2
;	genCall
	lcall	_set_cursor
;	..\ev8031.lib\ev8031.c:188: while((y = *string++)) {
;	genAssign
	mov	r2,_string_to_hd_PARM_3
	mov	r3,(_string_to_hd_PARM_3 + 1)
	mov	r4,(_string_to_hd_PARM_3 + 2)
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r5,a
	inc	dptr
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
;	genAssign
	mov	ar6,r5
;	genIfx
	mov	a,r5
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00104$
00108$:
;	..\ev8031.lib\ev8031.c:190: LCD_DATA = y;
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,r6
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_start'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function i2c_start
;	-----------------------------------------
_i2c_start:
;	..\ev8031.lib\i2c.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	setb	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	..\ev8031.lib\i2c.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	clr	_P1_0
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_stop'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function i2c_stop
;	-----------------------------------------
_i2c_stop:
;	..\ev8031.lib\i2c.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	clr	_P1_0
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_wb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	-----------------------------------------
;	 function i2c_wb
;	-----------------------------------------
_i2c_wb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:17: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	r4,#0x08
00104$:
;	..\ev8031.lib\i2c.c:19: 
;	genAnd
	mov	a,r2
	mov	c,acc.7
	mov	_P1_0,c
;	..\ev8031.lib\i2c.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	add	a,acc
	mov	r2,a
;	..\ev8031.lib\i2c.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00129$:
00130$:
;	..\ev8031.lib\i2c.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00131$:
00132$:
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00103$
00133$:
00134$:
;	..\ev8031.lib\i2c.c:24: 
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00104$
00135$:
00136$:
;	..\ev8031.lib\i2c.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00107$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00107$
00137$:
00138$:
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00108$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00108$
00139$:
00140$:
;	..\ev8031.lib\i2c.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P1_0,00110$
00141$:
;	genAssign
	mov	r3,#0x01
00110$:
;	..\ev8031.lib\i2c.c:27: 
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00111$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00111$
00142$:
00143$:
;	..\ev8031.lib\i2c.c:28: #define KB_BASE 0x9000
;	genRet
	mov	dpl,r3
00112$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_rb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	-----------------------------------------
;	 function i2c_rb
;	-----------------------------------------
_i2c_rb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:33: 
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genAssign
	setb	_P1_0
;	..\ev8031.lib\i2c.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genAssign
	mov	r4,#0x08
00103$:
;	..\ev8031.lib\i2c.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r3,a
;	..\ev8031.lib\i2c.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00123$:
00124$:
;	..\ev8031.lib\i2c.c:38: 
;	genAssign
	clr	a
	mov	c,_P1_0
	rlc	a
;	genOr
; Peephole 105   removed redundant mov
	mov  r5,a
	orl	ar3,a
;	..\ev8031.lib\i2c.c:39: // One Wire
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00125$:
00126$:
;	..\ev8031.lib\i2c.c:41: // One Wire
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00103$
00127$:
00128$:
;	..\ev8031.lib\i2c.c:42: 
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00107$
00129$:
;	genAssign
	clr	_P1_0
00107$:
;	..\ev8031.lib\i2c.c:43: // I2C
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00108$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00108$
00130$:
00131$:
;	genAssign
	clr	_P1_1
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00109$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00109$
00132$:
00133$:
;	..\ev8031.lib\i2c.c:44: #define SCL P1_1
;	genRet
	mov	dpl,r3
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ow_reset'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ow.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	-----------------------------------------
;	 function ow_reset
;	-----------------------------------------
_ow_reset:
;	..\ev8031.lib\ow.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	clr	_P1_2
;	..\ev8031.lib\ow.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	r2,#0x96
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00112$:
00113$:
;	..\ev8031.lib\ow.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	setb	_P1_2
;	..\ev8031.lib\ow.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	r2,#0x1E
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00114$:
00115$:
;	..\ev8031.lib\ow.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	clr	a
	mov	c,_P1_2
	rlc	a
	mov	r2,a
;	..\ev8031.lib\ow.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	mov	r3,#0x78
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r3,00103$
00116$:
00117$:
;	..\ev8031.lib\ow.c:13: 
;	genRet
	mov	dpl,r2
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ow_rbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ow.c:17: xdata at 0xf000 unsigned char pDAC;
;	-----------------------------------------
;	 function ow_rbit
;	-----------------------------------------
_ow_rbit:
;	..\ev8031.lib\ow.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genAssign
	clr	_P1_2
;	..\ev8031.lib\ow.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	setb	_P1_2
;	..\ev8031.lib\ow.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genAssign
	mov	r2,#0x02
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00109$:
00110$:
;	..\ev8031.lib\ow.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genAssign
	clr	a
	mov	c,_P1_2
	rlc	a
	mov	r2,a
;	..\ev8031.lib\ow.c:24: 
;	genAssign
	mov	r3,#0x14
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r3,00102$
00111$:
00112$:
;	..\ev8031.lib\ow.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genRet
	mov	dpl,r2
00103$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ow_wbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ow.c:29: 
;	-----------------------------------------
;	 function ow_wbit
;	-----------------------------------------
_ow_wbit:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\ow.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genAssign
	clr	_P1_2
;	..\ev8031.lib\ow.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00108$
00111$:
;	genAssign
	setb	_P1_2
;	..\ev8031.lib\ow.c:33: 
00108$:
;	genAssign
	mov	r2,#0x14
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00112$:
00113$:
;	..\ev8031.lib\ow.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genAssign
	setb	_P1_2
;	..\ev8031.lib\ow.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genAssign
	mov	r2,#0x02
00104$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00104$
00114$:
00115$:
00105$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ow_rb'
;------------------------------------------------------------
;i                         Allocated to registers 
;value                     Allocated to registers r2 
;------------------------------------------------------------
;	..\ev8031.lib\ow.c:39: // One Wire
;	-----------------------------------------
;	 function ow_rb
;	-----------------------------------------
_ow_rb:
;	..\ev8031.lib\ow.c:41: // One Wire
;	genAssign
	mov	r2,#0x00
;	..\ev8031.lib\ow.c:42: 
;	genAssign
	mov	r3,#0x08
00105$:
;	..\ev8031.lib\ow.c:43: // I2C
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	a,r2
	clr	c
	rrc	a
	mov	r2,a
;	..\ev8031.lib\ow.c:44: #define SCL P1_1
;	genCall
	push	ar2
	push	ar3
	lcall	_ow_rbit
	mov	a,dpl
	pop	ar3
	pop	ar2
;	genIfx
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00102$
00111$:
;	genOr
	orl	ar2,#0x80
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r3,00105$
00112$:
00113$:
;	..\ev8031.lib\ow.c:42: 
;	..\ev8031.lib\ow.c:46: #define I2CDELAY() delay8(3)
;	genRet
	mov	dpl,r2
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ow_wb'
;------------------------------------------------------------
;val                       Allocated to registers r2 
;shifted_one               Allocated to registers r3 
;------------------------------------------------------------
;	..\ev8031.lib\ow.c:50: {
;	-----------------------------------------
;	 function ow_wb
;	-----------------------------------------
_ow_wb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\ow.c:52: mov	r0, #0
;	genAssign
	mov	r3,#0x01
;	..\ev8031.lib\ow.c:53: mov	dptr, #KB_BASE+0xFE
00101$:
;	..\ev8031.lib\ow.c:54: next_key_line:
;	genAnd
	mov	a,r3
	anl	a,r2
	mov	dpl,a
;	genCall
	push	ar2
	push	ar3
	lcall	_ow_wbit
	pop	ar3
	pop	ar2
;	..\ev8031.lib\ow.c:55: ; check scan line
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
;	..\ev8031.lib\ow.c:56: movx	a, @dptr
;	genIfx
; Peephole 105   removed redundant mov
	mov  r3,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00101$
00107$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 't0_isr'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ev8031.lib\zoom.c:2: xdata at 0xB000 unsigned char righti; // right part of indication
;	-----------------------------------------
;	 function t0_isr
;	-----------------------------------------
_t0_isr:
	push	acc
	push	b
	push	dpl
	push	dph
	push	psw
	mov	psw,#0x00
;	..\test_menu/..\ev8031.lib\zoom.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genIfx
	mov	a,_need_zoom
	orl	a,(_need_zoom + 1)
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00103$
00106$:
;	genMinus
;	genMinusDec
	mov	a,_need_zoom
	add	a,#0xff
	mov	_need_zoom,a
	mov	a,(_need_zoom + 1)
	addc	a,#0xff
	mov	(_need_zoom + 1),a
;	genXor
	cpl	_P1_6
00103$:
	pop	psw
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 't0_for_zoom_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ev8031.lib\zoom.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	-----------------------------------------
;	 function t0_for_zoom_init
;	-----------------------------------------
_t0_for_zoom_init:
;	..\test_menu/..\ev8031.lib\zoom.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	clr	_EA
;	..\test_menu/..\ev8031.lib\zoom.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	_TH0,#0xAF
;	..\test_menu/..\ev8031.lib\zoom.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	mov	_TMOD,#0x22
;	..\test_menu/..\ev8031.lib\zoom.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	setb	_TR0
;	..\test_menu/..\ev8031.lib\zoom.c:13: 
;	genAssign
	setb	_ET0
;	..\test_menu/..\ev8031.lib\zoom.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	setb	_EA
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_t0_t1'
;------------------------------------------------------------
;tc_number                 Allocated to in memory with name '_test_t0_t1_tc_number_1_1'
;i                         Allocated to registers r4 r5 
;f_index                   Allocated to in memory with name '_test_t0_t1_f_index_1_1'
;index                     Allocated to in memory with name '_test_t0_t1_index_1_1'
;freq                      Allocated to in memory with name '_test_t0_t1_freq_1_1'
;------------------------------------------------------------
;	..\test_menu/..\t0_t1\t0_t1.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function test_t0_t1
;	-----------------------------------------
_test_t0_t1:
;	genReceive
	mov	_test_t0_t1_tc_number_1_1,dpl
;	..\test_menu/..\t0_t1\t0_t1.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	_test_t0_t1_f_index_1_1,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
00101$:
;	..\test_menu/..\t0_t1\t0_t1.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genIfx
	mov	a,_test_t0_t1_tc_number_1_1
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00109$
00129$:
;	..\test_menu/..\t0_t1\t0_t1.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	mov	_TL1,#0x00
;	genAssign
	mov	_TH1,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	_TL0,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	mov	_TH0,#0xE8
;	..\test_menu/..\t0_t1\t0_t1.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	mov	_TMOD,#0x51
;	..\test_menu/..\t0_t1\t0_t1.c:13: 
;	genAssign
	mov	_TCON,#0x50
;	..\test_menu/..\t0_t1\t0_t1.c:14: xdata at 0x8004 unsigned char LCD_CMD;
00102$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _TF0,00102$
00130$:
;	..\test_menu/..\t0_t1\t0_t1.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	_TCON,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:16: 
;	genCast
	mov	r4,_TH1
	mov	r5,#0x00
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar5,r4
	mov	r4,#0x00
;	genCast
	mov	r6,_TL1
	mov	r7,#0x00
;	genOr
	mov	a,r6
	orl	ar4,a
	mov	a,r7
	orl	ar5,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00110$
00109$:
;	..\test_menu/..\t0_t1\t0_t1.c:19: 
;	genAssign
	mov	_TL0,#0x00
;	genAssign
	mov	_TH0,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genAssign
	mov	_TL1,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	mov	_TH1,#0xE8
;	..\test_menu/..\t0_t1\t0_t1.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genAssign
	mov	_TMOD,#0x15
;	..\test_menu/..\t0_t1\t0_t1.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genAssign
	mov	_TCON,#0x50
;	..\test_menu/..\t0_t1\t0_t1.c:24: 
00105$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _TF1,00105$
00131$:
;	..\test_menu/..\t0_t1\t0_t1.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	mov	_TCON,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genCast
	mov	r6,_TH0
	mov	r7,#0x00
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar7,r6
	mov	r6,#0x00
;	genCast
	mov	r0,_TL0
	mov	r1,#0x00
;	genOr
	mov	a,r0
	orl	a,r6
	mov	r4,a
	mov	a,r1
	orl	a,r7
	mov	r5,a
00110$:
;	..\test_menu/..\t0_t1\t0_t1.c:29: 
;	genPlus
;	genPlusIncr
	inc	_test_t0_t1_f_index_1_1
;	genCmpEq
	mov	a,_test_t0_t1_f_index_1_1
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne a,#0x10,00112$
;00132$:
; Peephole 200   removed redundant sjmp
00133$:
;	genAssign
	mov	_test_t0_t1_f_index_1_1,#0x00
00112$:
;	..\test_menu/..\t0_t1\t0_t1.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genMult
;	genMultOneByte
	mov	b,#0x02
	mov	a,_test_t0_t1_f_index_1_1
	mul	ab
;	genPlus
	add	a,#_test_t0_t1_freq_1_1
	mov	r6,a
	mov	a,b
	addc	a,#(_test_t0_t1_freq_1_1 >> 8)
	mov	r7,a
;	genPointerSet
;	genFarPointerSet
	mov	dpl,r6
	mov	dph,r7
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
;	..\test_menu/..\t0_t1\t0_t1.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
;	..\test_menu/..\t0_t1\t0_t1.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genAssign
	mov	_test_t0_t1_index_1_1,#0x10
00117$:
;	..\test_menu/..\t0_t1\t0_t1.c:33: 
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	a,ar1
	add	a,ar4
	mov	r3,a
	mov	a,ar2
	addc	a,ar5
	mov	r0,a
;	genAssign
	mov	ar4,r3
	mov	ar5,r0
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz _test_t0_t1_index_1_1,00117$
00134$:
00135$:
;	..\test_menu/..\t0_t1\t0_t1.c:32: #define LOW(Value) ((unsigned char)(Value))
;	..\test_menu/..\t0_t1\t0_t1.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	a,r5
	swap	a
	xch	a,r4
	swap	a
	anl	a,#0x0f
	xrl	a,r4
	xch	a,r4
	anl	a,#0x0f
	xch	a,r4
	xrl	a,r4
	xch	a,r4
	mov	r5,a
;	..\test_menu/..\t0_t1\t0_t1.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
	mov	_display_int_PARM_2,#0x40
;	genCall
	mov	dpl,r4
	mov	dph,r5
	push	ar6
	push	ar7
	lcall	_display_int
	pop	ar7
	pop	ar6
;	..\test_menu/..\t0_t1\t0_t1.c:40: #define DQ P1_2
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar6
	push	ar7
	lcall	_delay16
	pop	ar7
	pop	ar6
;	..\test_menu/..\t0_t1\t0_t1.c:42: 
;	genCall
	push	ar6
	push	ar7
	lcall	_key_scan
	mov	r2,dpl
	pop	ar7
	pop	ar6
;	genCmpEq
	cjne	r2,#0x0C,00136$
	ljmp	00101$
00136$:
00118$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_leds'
;------------------------------------------------------------
;temp                      Allocated to registers r3 
;key_pressed               Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\leds\leds.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function test_leds
;	-----------------------------------------
_test_leds:
;	..\test_menu/..\leds\leds.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	r2,#0x00
;	..\test_menu/..\leds\leds.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x33
	movx	@dptr,a
;	..\test_menu/..\leds\leds.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x33
	movx	@dptr,a
;	..\test_menu/..\leds\leds.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\leds\leds.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
00101$:
;	..\test_menu/..\leds\leds.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	r3,#0x01
;	..\test_menu/..\leds\leds.c:11: xdata at 0x8001 unsigned char PB_REG;
00105$:
;	..\test_menu/..\leds\leds.c:13: 
;	genAssign
	mov	dptr,#_LED_REG
	mov	a,r3
	movx	@dptr,a
;	..\test_menu/..\leds\leds.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x27)<<8) + 0x10)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu/..\leds\leds.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r4,a
;	genAssign
	mov	ar3,r4
;	..\test_menu/..\leds\leds.c:16: 
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_key_scan
	mov	r5,dpl
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r5,#0x0C,00116$
00116$:
;	genIfx
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
; Peephole 128   jump optimization
	jnc  00106$
00117$:
;	genCpl
	mov	a,r2
	cpl	a
	mov	r6,a
;	genAssign
	mov	ar2,r6
00106$:
;	..\test_menu/..\leds\leds.c:18: xdata at 0xe000 unsigned char pADC;
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00107$
00118$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_key_scan
	mov	r7,dpl
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpEq
	cjne	r7,#0x0C,00119$
	ljmp	00105$
00119$:
00107$:
;	..\test_menu/..\leds\leds.c:19: 
;	genIfx
	mov	a,r3
;	genIfxJump
	jnz	00120$
	ljmp	00101$
00120$:
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_e40281'
;------------------------------------------------------------
;temp                      Allocated to registers r3 
;key_pressed               Allocated to registers 
;adress                    Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\e40281\e40281.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function test_e40281
;	-----------------------------------------
_test_e40281:
;	..\test_menu/..\e40281\e40281.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x44
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x44
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	mov	r2,#0xFC
;	..\test_menu/..\e40281\e40281.c:12: xdata at 0x8002 unsigned char PC_REG;
00101$:
;	..\test_menu/..\e40281\e40281.c:13: 
;	genAssign
	mov	r3,#0x01
;	..\test_menu/..\e40281\e40281.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	mov	dptr,#_pC
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:15: xdata at 0x8005 unsigned char LCD_DATA;
00104$:
;	..\test_menu/..\e40281\e40281.c:17: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	dptr,#_pB
	mov	a,r3
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:18: xdata at 0xe000 unsigned char pADC;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x27)<<8) + 0x10)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu/..\e40281\e40281.c:19: 
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r4,a
;	genAssign
	mov	ar3,r4
;	..\test_menu/..\e40281\e40281.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_key_scan
	mov	r5,dpl
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r5,#0x0C,00118$
00118$:
;	genIfx
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
; Peephole 129   jump optimization
	jc   00106$
00119$:
;	..\test_menu/..\e40281\e40281.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00104$
00120$:
00106$:
;	..\test_menu/..\e40281\e40281.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	push	ar2
	lcall	_key_scan
	mov	r3,dpl
	pop	ar2
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne r3,#0x0C,00111$
;00121$:
; Peephole 200   removed redundant sjmp
00122$:
;	..\test_menu/..\e40281\e40281.c:24: 
;	genPlus
;	genPlusIncr
	inc	r2
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00108$
00123$:
;	genAssign
	mov	r2,#0xFC
00108$:
;	..\test_menu/..\e40281\e40281.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	mov	dptr,#_pC
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\e40281\e40281.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
	ljmp	00101$
00111$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_ta07_11'
;------------------------------------------------------------
;tempA                     Allocated to registers r2 
;tempC                     Allocated to registers r3 
;key_pressed               Allocated to registers 
;adress                    Allocated to registers 
;------------------------------------------------------------
;	..\test_menu/..\ta07-11\ta07_11.c:2: xdata at 0xB000 unsigned char righti; // right part of indication
;	-----------------------------------------
;	 function test_ta07_11
;	-----------------------------------------
_test_ta07_11:
;	..\test_menu/..\ta07-11\ta07_11.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	r2,#0x01
;	genAssign
	mov	r3,#0x01
;	..\test_menu/..\ta07-11\ta07_11.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x55
	movx	@dptr,a
;	..\test_menu/..\ta07-11\ta07_11.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x55
	movx	@dptr,a
;	..\test_menu/..\ta07-11\ta07_11.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\ta07-11\ta07_11.c:11: xdata at 0x8001 unsigned char PB_REG;
00112$:
;	..\test_menu/..\ta07-11\ta07_11.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genCpl
	mov	dptr,#_pC
	mov	a,r3
	cpl	a
	movx	@dptr,a
;	..\test_menu/..\ta07-11\ta07_11.c:15: xdata at 0x8005 unsigned char LCD_DATA;
00104$:
;	..\test_menu/..\ta07-11\ta07_11.c:17: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	dptr,#_pA
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\ta07-11\ta07_11.c:18: xdata at 0xe000 unsigned char pADC;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x13)<<8) + 0x88)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu/..\ta07-11\ta07_11.c:19: 
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	add	a,acc
	mov	r4,a
;	genAssign
	mov	ar2,r4
;	..\test_menu/..\ta07-11\ta07_11.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_key_scan
	mov	r5,dpl
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r5,#0x0C,00121$
00121$:
;	genIfx
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
; Peephole 129   jump optimization
	jc   00106$
00122$:
;	..\test_menu/..\ta07-11\ta07_11.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne r2,#0x20,00104$
;00123$:
; Peephole 200   removed redundant sjmp
00124$:
00106$:
;	..\test_menu/..\ta07-11\ta07_11.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	push	ar3
	lcall	_key_scan
	mov	r2,dpl
	pop	ar3
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00125$
00125$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00114$
00126$:
;	..\test_menu/..\ta07-11\ta07_11.c:24: 
;	genAssign
	mov	r2,#0x01
;	..\test_menu/..\ta07-11\ta07_11.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r3,a
;	..\test_menu/..\ta07-11\ta07_11.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genCmpEq
	cjne	r3,#0x80,00127$
	sjmp	00128$
00127$:
	ljmp	00112$
00128$:
;	genAssign
	mov	r3,#0x01
	ljmp	00112$
00114$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_adc'
;------------------------------------------------------------
;shifted_one               Allocated to registers r0 
;result                    Allocated to registers r7 
;sample_index              Allocated to registers r2 
;i                         Allocated to registers 
;sample                    Allocated to registers r7 r1 
;summ                      Allocated to registers r3 r4 
;last                      Allocated to in memory with name '_test_adc_last_1_1'
;display_counter           Allocated to in memory with name '_test_adc_display_counter_1_1'
;i                         Allocated to registers 
;sample_array              Allocated to in memory with name '_test_adc_sample_array_1_1'
;------------------------------------------------------------
;	..\test_menu/..\ad7801\adc.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	-----------------------------------------
;	 function test_adc
;	-----------------------------------------
_test_adc:
;	..\test_menu/..\ad7801\adc.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
;	..\test_menu/..\ad7801\adc.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
;	genAssign
; Peephole 3.b   changed mov to clr
; Peephole 3.b   changed mov to clr
; Peephole 3.b   changed mov to clr
	clr  a
	mov  r2,a
	mov  r3,a
	mov  r4,a
	mov	(_test_adc_display_counter_1_1 + 1),a
	mov	_test_adc_display_counter_1_1,a
;	..\test_menu/..\ad7801\adc.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	r7,#0x00
00112$:
;	genCmpLt
;	genCmp
	cjne	r7,#0x20,00127$
00127$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00109$
00128$:
;	genMult
;	genMultOneByte
	mov	b,#0x02
	mov	a,r7
	mul	ab
;	genPlus
	add	a,#_test_adc_sample_array_1_1
	mov	dpl,a
	mov	a,b
	addc	a,#(_test_adc_sample_array_1_1 >> 8)
	mov	dph,a
;	genPointerSet
;	genFarPointerSet
; Peephole 101   removed redundant mov
; Peephole 180   changed mov to clr
	clr  a
	movx @dptr,a
	inc  dptr
	movx @dptr,a
;	genPlus
;	genPlusIncr
	inc	r7
;	..\test_menu/..\ad7801\adc.c:12: xdata at 0x8002 unsigned char PC_REG;
; Peephole 132   changed ljmp to sjmp
	sjmp 00112$
00109$:
;	..\test_menu/..\ad7801\adc.c:13: 
;	genAssign
	mov	dptr,#_pDAC
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	genAssign
	mov	r7,#0x00
;	genAssign
	mov	r0,#0x80
;	..\test_menu/..\ad7801\adc.c:14: xdata at 0x8004 unsigned char LCD_CMD;
00104$:
;	..\test_menu/..\ad7801\adc.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genOr
	mov	a,r0
	orl	a,r7
;	genAssign
	mov	dptr,#_pDAC
	movx	@dptr,a
;	..\test_menu/..\ad7801\adc.c:16: 
;	genAssign
	mov	r1,#0x0A
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r1,00101$
00129$:
00130$:
;	..\test_menu/..\ad7801\adc.c:17: xdata at 0xf000 unsigned char pDAC;
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P1_7,00103$
00131$:
;	genOr
	mov	a,r0
	orl	a,r7
;	genAssign
	mov	r7,a
00103$:
;	..\test_menu/..\ad7801\adc.c:18: xdata at 0xe000 unsigned char pADC;
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	a,r0
	clr	c
	rrc	a
	mov	r1,a
;	genAssign
	mov	ar0,r1
;	..\test_menu/..\ad7801\adc.c:19: 
;	genIfx
	mov	a,r0
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00104$
00132$:
;	..\test_menu/..\ad7801\adc.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genCast
	mov	r1,#0x00
;	..\test_menu/..\ad7801\adc.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genMult
;	genMultOneByte
	mov	b,#0x02
	mov	a,r2
	mul	ab
;	genPlus
	add	a,#_test_adc_sample_array_1_1
	mov	r5,a
	mov	a,b
	addc	a,#(_test_adc_sample_array_1_1 >> 8)
	mov	r6,a
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r5
	mov	dph,r6
	movx	a,@dptr
	mov	_test_adc_last_1_1,a
	inc	dptr
	movx	a,@dptr
	mov	(_test_adc_last_1_1 + 1),a
;	..\test_menu/..\ad7801\adc.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genPointerSet
;	genFarPointerSet
	mov	dpl,r5
	mov	dph,r6
	mov	a,r7
	movx	@dptr,a
	inc	dptr
	mov	a,r1
	movx	@dptr,a
;	..\test_menu/..\ad7801\adc.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genPlus
;	genPlusIncr
	inc	r2
;	genMod
;	genModOneByte
	mov	b,#0x20
	mov	a,r2
	div	ab
	mov	r2,b
;	..\test_menu/..\ad7801\adc.c:28: #define KB_BASE 0x9000
;	genPlus
	mov	a,ar7
	add	a,ar3
	mov	r5,a
	mov	a,ar1
	addc	a,ar4
	mov	r6,a
;	genMinus
	mov	a,r5
	clr	c
	subb	a,_test_adc_last_1_1
	mov	r3,a
	mov	a,r6
	subb	a,(_test_adc_last_1_1 + 1)
	mov	r4,a
;	..\test_menu/..\ad7801\adc.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,_test_adc_display_counter_1_1
	mov	_test_adc_display_counter_1_1,a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,(_test_adc_display_counter_1_1 + 1)
	mov	(_test_adc_display_counter_1_1 + 1),a
;	genCmpEq
	mov	a,_test_adc_display_counter_1_1
	cjne	a,#0x64,00133$
	mov	a,(_test_adc_display_counter_1_1 + 1)
; Peephole 162   removed sjmp by inverse jump logic
	jz   00134$
00133$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00110$
00134$:
;	..\test_menu/..\ad7801\adc.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar7,r3
	mov	a,r4
	swap	a
	rr	a
	xch	a,r7
	swap	a
	rr	a
	anl	a,#0x07
	xrl	a,r7
	xch	a,r7
	anl	a,#0x07
	xch	a,r7
	xrl	a,r7
	xch	a,r7
	mov	r1,a
;	genAssign
	mov	_display_int_PARM_2,#0x00
;	genCall
	mov	dpl,r7
	mov	dph,r1
	push	ar2
	push	ar3
	push	ar4
	push	ar0
	lcall	_display_int
	pop	ar0
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	clr	a
	mov	(_test_adc_display_counter_1_1 + 1),a
	mov	_test_adc_display_counter_1_1,a
00110$:
;	..\test_menu/..\ad7801\adc.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar0
	lcall	_key_scan
	mov	r5,dpl
	pop	ar0
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpEq
	cjne	r5,#0x0C,00135$
	ljmp	00109$
00135$:
00116$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_knopki'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\knopki\knopki.c:2: xdata at 0xB000 unsigned char righti; // right part of indication
;	-----------------------------------------
;	 function test_knopki
;	-----------------------------------------
_test_knopki:
;	..\test_menu/..\knopki\knopki.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
00101$:
;	..\test_menu/..\knopki\knopki.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P3_2,00103$
00118$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P3_3,00103$
00119$:
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0xFF
	movx	@dptr,a
00103$:
;	..\test_menu/..\knopki\knopki.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P3_2,00106$
00120$:
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x32
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x32
	movx	@dptr,a
00106$:
;	..\test_menu/..\knopki\knopki.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P3_3,00108$
00121$:
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x33
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x33
	movx	@dptr,a
00108$:
;	..\test_menu/..\knopki\knopki.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
;	..\test_menu/..\knopki\knopki.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00122$
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00122$:
00111$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_dac'
;------------------------------------------------------------
;i                         Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\triangle.dac\dac.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function test_dac
;	-----------------------------------------
_test_dac:
;	..\test_menu/..\triangle.dac\dac.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	r2,#0x00
;	..\test_menu/..\triangle.dac\dac.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x0D
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xAC
	movx	@dptr,a
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x01
	movx	@dptr,a
;	..\test_menu/..\triangle.dac\dac.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
00102$:
;	..\test_menu/..\triangle.dac\dac.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	dptr,#_pDAC
	mov	a,r2
	movx	@dptr,a
;	genPlus
;	genPlusIncr
	inc	r2
;	..\test_menu/..\triangle.dac\dac.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00102$
00116$:
;	..\test_menu/..\triangle.dac\dac.c:13: 
;	genAssign
	mov	ar3,r2
00105$:
;	..\test_menu/..\triangle.dac\dac.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genMinus
;	genMinusDec
	dec	r3
;	genAssign
	mov	ar2,r3
;	genAssign
	mov	dptr,#_pDAC
	mov	a,r3
	movx	@dptr,a
;	..\test_menu/..\triangle.dac\dac.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00105$
00117$:
;	..\test_menu/..\triangle.dac\dac.c:16: 
;	genCall
	push	ar2
	lcall	_key_scan
	mov	r3,dpl
	pop	ar2
;	genCmpEq
	cjne	r3,#0x0C,00118$
; Peephole 132   changed ljmp to sjmp
	sjmp 00102$
00118$:
00111$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'bcd2bin'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ds1307_\ds1307.c:12: xdata at 0x8002 unsigned char PC_REG;
;	-----------------------------------------
;	 function bcd2bin
;	-----------------------------------------
_bcd2bin:
;	genReceive
	mov	r2,dpl
;	..\test_menu/..\ds1307_\ds1307.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	a,r2
	swap	a
	anl	a,#0x0f
	mov	r3,a
;	genMult
;	genMultOneByte
	mov	b,#0x0A
	mov	a,r3
	mul	ab
	mov	r3,a
	mov	r4,b
;	genAnd
	anl	ar2,#0x0F
;	genCast
	mov	r5,#0x00
;	genPlus
	mov	a,ar2
	add	a,ar3
	mov	r3,a
	mov	a,ar5
	addc	a,ar4
	mov	r4,a
;	genCast
	mov	dpl,r3
;	genRet
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ds1307_init'
;------------------------------------------------------------
;s                         Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\ds1307_\ds1307.c:17: xdata at 0xf000 unsigned char pDAC;
;	-----------------------------------------
;	 function ds1307_init
;	-----------------------------------------
_ds1307_init:
;	..\test_menu/..\ds1307_\ds1307.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD1
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_rb
	mov	a,dpl
;	genAnd
	anl	a,#0x7F
	mov	r2,a
;	genCall
	push	ar2
	lcall	_i2c_stop
	pop	ar2
;	..\test_menu/..\ds1307_\ds1307.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	push	ar2
	lcall	_i2c_start
	pop	ar2
;	genCall
	mov	dpl,#0xD0
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	genCall
	mov	dpl,r2
	lcall	_i2c_wb
;	genCall
	lcall	_i2c_stop
;	..\test_menu/..\ds1307_\ds1307.c:24: 
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x07
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
;	genCall
	lcall	_i2c_stop
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'get_time'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ds1307_\ds1307.c:27: 
;	-----------------------------------------
;	 function get_time
;	-----------------------------------------
_get_time:
;	..\test_menu/..\ds1307_\ds1307.c:29: 
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD1
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genCall
	mov	dpl,#0x01
	lcall	_i2c_rb
	mov	a,dpl
;	genAnd
	anl	a,#0x7F
	mov	r2,a
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_time,r2
;	..\test_menu/..\ds1307_\ds1307.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar2
;	genAnd
	anl	a,#0x7F
	mov	r3,a
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0001),r3
;	..\test_menu/..\ds1307_\ds1307.c:33: 
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar3
	pop	ar2
;	genAnd
	anl	a,#0x3F
	mov	r4,a
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0002),r4
;	..\test_menu/..\ds1307_\ds1307.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_stop
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\ds1307_\ds1307.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genAssign
	mov	dptr,#_lefti
	mov	a,r4
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,r3
	movx	@dptr,a
;	..\test_menu/..\ds1307_\ds1307.c:38: 
;	genCall
	mov	dpl,r2
	push	ar3
	push	ar4
	lcall	_bcd2bin
	mov	r2,dpl
	pop	ar4
	pop	ar3
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_time,r2
;	..\test_menu/..\ds1307_\ds1307.c:39: // One Wire
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar4
	lcall	_bcd2bin
	mov	a,dpl
	pop	ar4
	pop	ar2
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0001),a
;	..\test_menu/..\ds1307_\ds1307.c:40: #define DQ P1_2
;	genCall
	mov	dpl,r4
	push	ar2
	lcall	_bcd2bin
	mov	a,dpl
	pop	ar2
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0002),a
;	..\test_menu/..\ds1307_\ds1307.c:42: 
;	genAnd
	mov	a,r2
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  acc.0,00102$
00107$:
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x20
	movx	@dptr,a
;	genAssign
	mov	dptr,#_dotsi
	mov	a,#0x20
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00102$:
;	..\test_menu/..\ds1307_\ds1307.c:43: // I2C
;	genAssign
	mov	dptr,#_dotsi
; Peephole 180   changed mov to clr
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_new_dotsi
	movx  @dptr,a
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_ds1307'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ds1307_\ds1307.c:46: #define I2CDELAY() delay8(3)
;	-----------------------------------------
;	 function test_ds1307
;	-----------------------------------------
_test_ds1307:
;	..\test_menu/..\ds1307_\ds1307.c:48: 
;	genCall
	lcall	_ds1307_init
;	..\test_menu/..\ds1307_\ds1307.c:50: {
00121$:
;	..\test_menu/..\ds1307_\ds1307.c:51: _asm;
;	genCall
	lcall	_get_time
;	..\test_menu/..\ds1307_\ds1307.c:53: mov	dptr, #KB_BASE+0xFE
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P3_3,00109$
00135$:
;	genAssign
	mov	_hour_counter,#0x00
	ljmp	00110$
00109$:
;	..\test_menu/..\ds1307_\ds1307.c:56: movx	a, @dptr
;	genIfx
	mov	a,_hour_counter
;	genIfxJump
	jz	00136$
	ljmp	00105$
00136$:
;	..\test_menu/..\ds1307_\ds1307.c:58: inc	r0
;	genAssign
;	genCmpLt
;	genCmp
	clr	c
	mov	a,0x0002 + _time
	xrl	a,#0x80
	subb	a,#0x97
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00102$
00137$:
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,0x0002 + _time
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0002),a
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00102$:
;	..\test_menu/..\ds1307_\ds1307.c:59: jnb	acc.1, end_key_scan
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0002),#0x00
00103$:
;	..\test_menu/..\ds1307_\ds1307.c:60: inc	r0
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x02
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:61: jnb	acc.2, end_key_scan
;	genAssign
;	genDiv
;	genDivOneByte
	mov	a,0x0002 + _time
	xrl	a,#0x0A
	push	acc
	mov	a,#0x0A
	jnb	acc.7,00138$
	cpl	a
	inc	a
00138$:
	mov	b,a
	mov	a,0x0002 + _time
	jnb	acc.7,00139$
	cpl	a
	inc	a
00139$:
	div	ab
	mov	b,a
	pop	acc
	jb	ov,00140$
	jnb	acc.7,00140$
	clr	c
	clr	a
	subb	a,b
	mov	b,a
00140$:
	mov	r2,b
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genMod
;	genModOneByte
	mov	a,0x0002 + _time
	xrl	a,#0x0A
	push	acc
	mov	a,#0x0A
	jnb	acc.7,00141$
	cpl	a
	inc	a
00141$:
	mov	b,a
	mov	a,0x0002 + _time
	jnb	acc.7,00142$
	cpl	a
	inc	a
00142$:
	div	ab
	pop	acc
	jb	ov,00143$
	jnb	acc.7,00143$
	clr	c
	clr	a
	subb	a,b
	mov	b,a
00143$:
	mov	r3,b
;	genOr
	mov	a,r3
	orl	a,r2
	mov	dpl,a
;	genCall
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:62: inc	r0
;	genCall
	lcall	_i2c_stop
00105$:
;	..\test_menu/..\ds1307_\ds1307.c:64: inc	r0
;	genPlus
;	genPlusIncr
	inc	_hour_counter
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x64
	subb	a,_hour_counter
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00110$
00144$:
;	genAssign
	mov	_hour_counter,#0x00
00110$:
;	..\test_menu/..\ds1307_\ds1307.c:67: rl	a
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P3_2,00119$
00145$:
;	genAssign
	mov	_min_counter,#0x00
	ljmp	00122$
00119$:
;	..\test_menu/..\ds1307_\ds1307.c:70: end_key_scan:
;	genIfx
	mov	a,_min_counter
;	genIfxJump
	jz	00146$
	ljmp	00115$
00146$:
;	..\test_menu/..\ds1307_\ds1307.c:72: _endasm;
;	genAssign
;	genCmpLt
;	genCmp
	clr	c
	mov	a,0x0001 + _time
	xrl	a,#0x80
	subb	a,#0xbb
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00112$
00147$:
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,0x0001 + _time
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0001),a
; Peephole 132   changed ljmp to sjmp
	sjmp 00113$
00112$:
;	..\test_menu/..\ds1307_\ds1307.c:73: }
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_time + 0x0001),#0x00
00113$:
;	..\test_menu/..\ds1307_\ds1307.c:74: 
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xD0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x01
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:75: /*
;	genAssign
;	genDiv
;	genDivOneByte
	mov	a,0x0001 + _time
	xrl	a,#0x0A
	push	acc
	mov	a,#0x0A
	jnb	acc.7,00148$
	cpl	a
	inc	a
00148$:
	mov	b,a
	mov	a,0x0001 + _time
	jnb	acc.7,00149$
	cpl	a
	inc	a
00149$:
	div	ab
	mov	b,a
	pop	acc
	jb	ov,00150$
	jnb	acc.7,00150$
	clr	c
	clr	a
	subb	a,b
	mov	b,a
00150$:
	mov	r2,b
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genMod
;	genModOneByte
	mov	a,0x0001 + _time
	xrl	a,#0x0A
	push	acc
	mov	a,#0x0A
	jnb	acc.7,00151$
	cpl	a
	inc	a
00151$:
	mov	b,a
	mov	a,0x0001 + _time
	jnb	acc.7,00152$
	cpl	a
	inc	a
00152$:
	div	ab
	pop	acc
	jb	ov,00153$
	jnb	acc.7,00153$
	clr	c
	clr	a
	subb	a,b
	mov	b,a
00153$:
	mov	r3,b
;	genOr
	mov	a,r3
	orl	a,r2
	mov	dpl,a
;	genCall
	lcall	_i2c_wb
;	..\test_menu/..\ds1307_\ds1307.c:76: ��⢥��⢨� ᪠� ���� �����頥���� �㭪樥� key_scan �ᯮ������� ������
;	genCall
	lcall	_i2c_stop
00115$:
;	..\test_menu/..\ds1307_\ds1307.c:78: 1 5 9
;	genPlus
;	genPlusIncr
	inc	_min_counter
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x64
	subb	a,_min_counter
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00122$
00154$:
;	genAssign
	mov	_min_counter,#0x00
00122$:
;	..\test_menu/..\ds1307_\ds1307.c:81: */
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00155$
	ljmp	00121$
00155$:
00124$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'show_T'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ds1621\ds1621.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	-----------------------------------------
;	 function show_T
;	-----------------------------------------
_show_T:
;	genReceive
	mov	r2,dpl
;	..\test_menu/..\ds1621\ds1621.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_dotsi
	mov	a,_sign
	movx	@dptr,a
;	..\test_menu/..\ds1621\ds1621.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x63
	subb	a,r2
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00102$
00111$:
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xF1
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00102$:
;	..\test_menu/..\ds1621\ds1621.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
00103$:
;	..\test_menu/..\ds1621\ds1621.c:13: 
;	genDiv
;	genDivOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
;	genIfx
; Peephole 105   removed redundant mov
	mov  r3,a
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00105$
00112$:
;	..\test_menu/..\ds1621\ds1621.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genMod
;	genModOneByte
	mov	b,#0x0A
	mov	a,r3
	div	ab
	mov	r3,b
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	swap	a
	anl	a,#0xf0
	mov	r3,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00105$:
;	..\test_menu/..\ds1621\ds1621.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	r3,#0xF0
00106$:
;	..\test_menu/..\ds1621\ds1621.c:17: xdata at 0xf000 unsigned char pDAC;
;	genMod
;	genModOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
	mov	r2,b
;	genOr
	mov	dptr,#_righti
	mov	a,r2
	orl	a,r3
	movx	@dptr,a
;	..\test_menu/..\ds1621\ds1621.c:18: xdata at 0xe000 unsigned char pADC;
00107$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_ds1621'
;------------------------------------------------------------
;no_sensor                 Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\ds1621\ds1621.c:21: xdata at 0xA004 unsigned char DC_REG;
;	-----------------------------------------
;	 function test_ds1621
;	-----------------------------------------
_test_ds1621:
;	..\test_menu/..\ds1621\ds1621.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
00101$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00126$
00126$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00101$
00127$:
;	..\test_menu/..\ds1621\ds1621.c:27: 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
;	..\test_menu/..\ds1621\ds1621.c:28: #define KB_BASE 0x9000
00104$:
;	..\test_menu/..\ds1621\ds1621.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCall
	lcall	_i2c_start
;	..\test_menu/..\ds1621\ds1621.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
;	..\test_menu/..\ds1621\ds1621.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genCall
	mov	dpl,#0xAC
	lcall	_i2c_wb
;	..\test_menu/..\ds1621\ds1621.c:33: 
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	..\test_menu/..\ds1621\ds1621.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genCall
	lcall	_i2c_stop
;	..\test_menu/..\ds1621\ds1621.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genCall
	lcall	_i2c_start
;	..\test_menu/..\ds1621\ds1621.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
;	..\test_menu/..\ds1621\ds1621.c:38: 
;	genCall
	mov	dpl,#0xEE
	lcall	_i2c_wb
;	..\test_menu/..\ds1621\ds1621.c:39: // One Wire
;	genCall
	lcall	_i2c_stop
;	..\test_menu/..\ds1621\ds1621.c:40: #define DQ P1_2
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\ds1621\ds1621.c:42: 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0xEA)<<8) + 0x60)
	lcall	_delay16
;	..\test_menu/..\ds1621\ds1621.c:44: #define SCL P1_1
;	genAssign
	mov	dptr,#_LED_REG
	mov	a,#0x01
	movx	@dptr,a
;	..\test_menu/..\ds1621\ds1621.c:45: #define SDA P1_0
;	genCall
	lcall	_i2c_start
;	..\test_menu/..\ds1621\ds1621.c:46: #define I2CDELAY() delay8(3)
;	genCall
	mov	dpl,#0x90
	lcall	_i2c_wb
	mov	r2,dpl
;	genNot
	mov	a,r2
	cjne	a,#0x01,00128$
00128$:
	clr	a
	rlc	a
	mov	r2,a
;	..\test_menu/..\ds1621\ds1621.c:48: 
;	genCall
	mov	dpl,#0xAA
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	..\test_menu/..\ds1621\ds1621.c:49: unsigned char key_scan()
;	genCall
	push	ar2
	lcall	_i2c_start
	pop	ar2
;	..\test_menu/..\ds1621\ds1621.c:50: {
;	genCall
	mov	dpl,#0x91
	push	ar2
	lcall	_i2c_wb
	pop	ar2
;	..\test_menu/..\ds1621\ds1621.c:51: _asm;
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar2
;	genAssign
	mov	_t,a
;	..\test_menu/..\ds1621\ds1621.c:52: mov	r0, #0
;	genCall
	push	ar2
	lcall	_i2c_stop
	pop	ar2
;	..\test_menu/..\ds1621\ds1621.c:54: next_key_line:
;	genCmpLt
;	genCmp
	mov	a,_t
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  acc.7,00106$
00129$:
;	genAssign
	mov	_sign,#0xFF
;	genUminus
	clr	c
	clr	a
	subb	a,_t
	mov	_t,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00107$
00106$:
;	genAssign
	mov	_sign,#0x00
00107$:
;	..\test_menu/..\ds1621\ds1621.c:56: movx	a, @dptr
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00109$
00130$:
;	genAssign
	mov	dptr,#_dotsi
	mov	a,#0xFF
	movx	@dptr,a
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0xF0
	movx	@dptr,a
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xFF
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00110$
00109$:
;	..\test_menu/..\ds1621\ds1621.c:57: jnb	acc.0, end_key_scan
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x03
	movx	@dptr,a
;	genAssign
	mov	dpl,_t
;	genCall
	lcall	_show_T
00110$:
;	..\test_menu/..\ds1621\ds1621.c:59: jnb	acc.1, end_key_scan
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00131$
00131$:
;	genIfxJump
	jc	00132$
	ljmp	00104$
00132$:
;	genCall
	lcall	_lights_off
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
00111$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00133$
00133$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00111$
00134$:
;	genRet
;	..\test_menu/..\ds1621\ds1621.c:61: jnb	acc.2, end_key_scan
00116$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_i'
;------------------------------------------------------------
;index                     Allocated to registers 
;temp                      Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\testi\testi.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function test_i
;	-----------------------------------------
_test_i:
;	..\test_menu/..\testi\testi.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
00101$:
;	..\test_menu/..\testi\testi.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
;	..\test_menu/..\testi\testi.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_dotsi
	movx  @dptr,a
;	..\test_menu/..\testi\testi.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	mov	r2,#0x00
;	genAssign
	mov	r3,#0x00
00104$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x10,00125$
00125$:
;	genIfx
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
; Peephole 128   jump optimization
	jnc  00107$
00126$:
;	..\test_menu/..\testi\testi.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genCall
	push	ar2
	push	ar3
	lcall	_key_scan
	mov	r4,dpl
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r4,#0x0C,00127$
00127$:
;	genIfx
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
; Peephole 129   jump optimization
	jc   00107$
00128$:
;	..\test_menu/..\testi\testi.c:13: 
;	genAssign
	mov	dptr,#_lefti
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	mov	dptr,#_righti
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genPlus
	mov	a,#0x11
	add	a,ar2
	mov	r2,a
;	..\test_menu/..\testi\testi.c:16: 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	push	ar4
	lcall	_delay16
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\testi\testi.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genPlus
;	genPlusIncr
	inc	r3
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00107$:
;	..\test_menu/..\testi\testi.c:19: 
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xFF
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	mov	r2,#0x10
;	..\test_menu/..\testi\testi.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genAssign
	mov	r3,#0x00
00110$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x04,00129$
00129$:
;	genIfx
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
; Peephole 128   jump optimization
	jnc  00113$
00130$:
;	..\test_menu/..\testi\testi.c:24: 
;	genCall
	push	ar2
	push	ar3
	lcall	_key_scan
	mov	r4,dpl
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r4,#0x0C,00131$
00131$:
;	genIfx
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
; Peephole 129   jump optimization
	jc   00113$
00132$:
;	..\test_menu/..\testi\testi.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	mov	dptr,#_dotsi
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:27: 
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0xFF
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:28: #define KB_BASE 0x9000
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xFF
	movx	@dptr,a
;	..\test_menu/..\testi\testi.c:29: 
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	add	a,acc
	mov	r2,a
;	..\test_menu/..\testi\testi.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	push	ar4
	lcall	_delay16
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\testi\testi.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genPlus
;	genPlusIncr
	inc	r3
; Peephole 132   changed ljmp to sjmp
	sjmp 00110$
00113$:
;	..\test_menu/..\testi\testi.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genCall
	push	ar2
	lcall	_key_scan
	mov	r3,dpl
	pop	ar2
;	genCmpEq
	cjne	r3,#0x0C,00133$
	ljmp	00101$
00133$:
00116$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'read_at24_group'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\at24\at24.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function read_at24_group
;	-----------------------------------------
_read_at24_group:
;	..\test_menu/..\at24\at24.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xA0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	genCall
	lcall	_i2c_stop
;	..\test_menu/..\at24\at24.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genCall
	lcall	_i2c_start
;	..\test_menu/..\at24\at24.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genCall
	mov	dpl,#0xA0
	lcall	_i2c_wb
;	genRet
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_at24'
;------------------------------------------------------------
;acknowledge               Allocated to registers r4 
;i                         Allocated to in memory with name '_test_at24_i_1_1'
;result                    Allocated to in memory with name '_test_at24_result_1_1'
;testbyte                  Allocated to in memory with name '_test_at24_testbyte_1_1'
;volume                    Allocated to in memory with name '_test_at24_volume_1_1'
;adress                    Allocated to registers r2 r3 
;shifted_one               Allocated to in memory with name '_test_at24_shifted_one_1_1'
;------------------------------------------------------------
;	..\test_menu/..\at24\at24.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function test_at24
;	-----------------------------------------
_test_at24:
;	..\test_menu/..\at24\at24.c:13: 
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	r2,#0x3F
;	..\test_menu/..\at24\at24.c:16: 
;	genAssign
; Peephole 3.b   changed mov to clr
	clr  a
	mov  r3,a
	mov	(_test_at24_shifted_one_1_1 + 1),a
	mov	_test_at24_shifted_one_1_1,#0x40
;	..\test_menu/..\at24\at24.c:17: xdata at 0xf000 unsigned char pDAC;
;	genCall
	push	ar2
	push	ar3
	lcall	_read_at24_group
	mov	a,dpl
	pop	ar3
	pop	ar2
;	genIfx
;	genIfxJump
	jz	00137$
	ljmp	00135$
00137$:
;	..\test_menu/..\at24\at24.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	_test_at24_volume_1_1,#0x00
00107$:
;	..\test_menu/..\at24\at24.c:19: 
;	genOr
	mov	a,_test_at24_shifted_one_1_1
	orl	ar2,a
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	orl	ar3,a
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	xch	a,_test_at24_shifted_one_1_1
	add	a,acc
	xch	a,_test_at24_shifted_one_1_1
	rlc	a
	mov	(_test_at24_shifted_one_1_1 + 1),a
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar7,r3
	mov	r0,#0x00
;	genCast
	mov	ar1,r2
;	genCast
	mov	ar6,r1
	mov	a,r1
	rlc	a
	subb	a,acc
	mov	r4,a
;	genXor
	mov	a,r6
	xrl	ar7,a
	mov	a,r4
	xrl	ar0,a
;	genCast
	mov	_test_at24_testbyte_1_1,r7
;	..\test_menu/..\at24\at24.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genCall
	push	ar2
	push	ar3
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar3
	pop	ar2
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar5,r2
	mov	a,r3
	mov	c,acc.7
	xch	a,r5
	rlc	a
	xch	a,r5
	rlc	a
	xch	a,r5
	anl	a,#0x01
	mov	r6,a
;	genAnd
	mov	a,#0x0E
	anl	a,r5
	mov	r7,a
	mov	r0,#0x00
;	genOr
	orl	ar7,#0xA0
;	genCast
	mov	dpl,r7
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genCall
	mov	dpl,_test_at24_testbyte_1_1
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_stop
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:24: 
;	genAssign
	setb	_P1_2
;	..\test_menu/..\at24\at24.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAnd
	mov	a,#0x0E
	anl	a,r5
	mov	r7,a
	mov	r0,#0x00
;	genOr
	orl	ar7,#0xA0
;	genAssign
	mov	_test_at24_i_1_1,#0x00
00102$:
;	..\test_menu/..\at24\at24.c:27: 
;	genPlus
;	genPlusIncr
	inc	_test_at24_i_1_1
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genCast
	mov	dpl,r7
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_wb
	mov	a,dpl
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genAssign
;	..\test_menu/..\at24\at24.c:28: #define KB_BASE 0x9000
;	genIfx
; Peephole 105   removed redundant mov
	mov  r4,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00104$
00138$:
;	genIfx
	mov	a,_test_at24_i_1_1
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00102$
00139$:
00104$:
;	..\test_menu/..\at24\at24.c:29: 
;	genAssign
	clr	_P1_2
;	..\test_menu/..\at24\at24.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_i2c_start
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAnd
	anl	ar5,#0x0E
	mov	r6,#0x00
;	genOr
	orl	ar5,#0xA1
;	genCast
	mov	dpl,r5
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_wb
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	_test_at24_result_1_1,a
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_stop
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genCmpEq
	mov	a,_test_at24_testbyte_1_1
	cjne	a,_test_at24_result_1_1,00140$
	sjmp	00141$
00140$:
	ljmp	00121$
00141$:
;	..\test_menu/..\at24\at24.c:33: 
;	genPlus
;	genPlusIncr
	inc	_test_at24_volume_1_1
;	..\test_menu/..\at24\at24.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genAssign
	mov	dptr,#_righti
	mov	a,_test_at24_i_1_1
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genAssign
	mov	dptr,#_lefti
	mov	a,_test_at24_volume_1_1
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	push	ar2
	push	ar3
	push	ar4
	lcall	_delay16
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genCmpEq
	cjne	r2,#0xFF,00142$
	cjne	r3,#0x07,00142$
	sjmp	00143$
00142$:
	ljmp	00107$
00143$:
	ljmp	00121$
;	..\test_menu/..\at24\at24.c:40: #define DQ P1_2
00135$:
;	genAssign
	mov	_test_at24_volume_1_1,#0x00
00116$:
;	..\test_menu/..\at24\at24.c:41: // One Wire
;	genOr
	mov	a,_test_at24_shifted_one_1_1
	orl	ar2,a
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	orl	ar3,a
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	xch	a,_test_at24_shifted_one_1_1
	add	a,acc
	xch	a,_test_at24_shifted_one_1_1
	rlc	a
	mov	(_test_at24_shifted_one_1_1 + 1),a
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar7,r3
	mov	r0,#0x00
;	genCast
	mov	ar1,r2
;	genCast
	mov	ar6,r1
	mov	a,r1
	rlc	a
	subb	a,acc
	mov	r5,a
;	genXor
	mov	a,r7
	xrl	ar6,a
	mov	a,r0
	xrl	ar5,a
;	genCast
	mov	_test_at24_testbyte_1_1,r6
;	..\test_menu/..\at24\at24.c:43: // I2C
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA0
	push	ar2
	push	ar3
	push	ar4
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
	mov	ar5,r7
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:44: #define SCL P1_1
;	genCall
	mov	dpl,_test_at24_testbyte_1_1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:45: #define SDA P1_0
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_stop
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:46: #define I2CDELAY() delay8(3)
;	genAssign
	setb	_P1_2
;	..\test_menu/..\at24\at24.c:48: 
;	genAssign
	mov	r6,#0x00
00111$:
;	..\test_menu/..\at24\at24.c:49: unsigned char key_scan()
;	genPlus
;	genPlusIncr
	inc	r6
;	..\test_menu/..\at24\at24.c:50: {
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA0
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	mov	a,dpl
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
;	..\test_menu/..\at24\at24.c:51: _asm;
;	genIfx
; Peephole 105   removed redundant mov
	mov  r4,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00113$
00144$:
;	genIfx
	mov	a,r6
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00111$
00145$:
00113$:
;	..\test_menu/..\at24\at24.c:52: mov	r0, #0
;	genAssign
	clr	_P1_2
;	..\test_menu/..\at24\at24.c:53: mov	dptr, #KB_BASE+0xFE
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:54: next_key_line:
;	genCall
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_start
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA1
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar6
	pop	ar3
	pop	ar2
;	genAssign
	mov	_test_at24_result_1_1,a
;	genCall
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_stop
	pop	ar6
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:55: ; check scan line
;	genCmpEq
	mov	a,_test_at24_testbyte_1_1
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne a,_test_at24_result_1_1,00121$
;00146$:
; Peephole 200   removed redundant sjmp
00147$:
;	..\test_menu/..\at24\at24.c:56: movx	a, @dptr
;	genPlus
;	genPlusIncr
	inc	_test_at24_volume_1_1
;	..\test_menu/..\at24\at24.c:57: jnb	acc.0, end_key_scan
;	genAssign
	mov	dptr,#_righti
	mov	a,r6
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:58: inc	r0
;	genAssign
	mov	dptr,#_lefti
	mov	a,_test_at24_volume_1_1
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:59: jnb	acc.1, end_key_scan
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu/..\at24\at24.c:60: inc	r0
;	genCmpEq
	cjne	r2,#0xFF,00148$
	cjne	r3,#0xFF,00148$
	sjmp	00149$
00148$:
	ljmp	00116$
00149$:
00121$:
;	..\test_menu/..\at24\at24.c:63: jnb	acc.3, end_key_scan
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar4,r2
	mov	a,r3
	clr	c
	rrc	a
	xch	a,r4
	rrc	a
	xch	a,r4
	mov	r5,a
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,ar4
	mov	r2,a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,ar5
	mov	r3,a
;	..\test_menu/..\at24\at24.c:64: inc	r0
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	dpl,r2
	mov	a,r3
	mov	c,acc.7
	xch	a,dpl
	rlc	a
	xch	a,dpl
	rlc	a
	xch	a,dpl
	anl	a,#0x01
	mov	dph,a
;	genAssign
	mov	_display_int_PARM_2,#0x00
;	genCall
	lcall	_display_int
;	..\test_menu/..\at24\at24.c:65: ; goto next scan line
;	genAssign
	mov	dptr,#_LED_REG
	mov	a,#0x01
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:66: mov	a, dpl
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
;	..\test_menu/..\at24\at24.c:67: rl	a
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\at24\at24.c:69: cjne	a, #0xF7, next_key_line
00122$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00150$
; Peephole 132   changed ljmp to sjmp
	sjmp 00122$
00150$:
00125$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_accurasy'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\ds1822\ds1822.c:2: xdata at 0xB000 unsigned char righti; // right part of indication
;	-----------------------------------------
;	 function set_accurasy
;	-----------------------------------------
_set_accurasy:
;	..\test_menu/..\ds1822\ds1822.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genCall
	lcall	_ow_reset
;	..\test_menu/..\ds1822\ds1822.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genCall
	mov	dpl,#0xCC
	lcall	_ow_wb
;	..\test_menu/..\ds1822\ds1822.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genCall
	mov	dpl,#0x4E
	lcall	_ow_wb
;	..\test_menu/..\ds1822\ds1822.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genCall
	mov	dpl,#0x00
	lcall	_ow_wb
;	..\test_menu/..\ds1822\ds1822.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genCall
	mov	dpl,#0x00
	lcall	_ow_wb
;	..\test_menu/..\ds1822\ds1822.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCall
	mov	dpl,#0x1F
	lcall	_ow_wb
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'get_T'
;------------------------------------------------------------
;i                         Allocated to registers 
;scratchpad                Allocated to in memory with name '_get_T_scratchpad_1_1'
;temperature               Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\ds1822\ds1822.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	-----------------------------------------
;	 function get_T
;	-----------------------------------------
_get_T:
;	..\test_menu/..\ds1822\ds1822.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	r2,#0x80
;	..\test_menu/..\ds1822\ds1822.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genCall
	push	ar2
	lcall	_ow_reset
	mov	a,dpl
	pop	ar2
;	genIfx
;	genIfxJump
	jz	00117$
	ljmp	00105$
00117$:
;	..\test_menu/..\ds1822\ds1822.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genCall
	mov	dpl,#0xCC
	push	ar2
	lcall	_ow_wb
	pop	ar2
;	..\test_menu/..\ds1822\ds1822.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	mov	dpl,#0x44
	push	ar2
	lcall	_ow_wb
	pop	ar2
;	..\test_menu/..\ds1822\ds1822.c:25: xdata at 0xB000 unsigned char DISPLAYB;
00101$:
;	genCall
	push	ar2
	lcall	_ow_rbit
	mov	a,dpl
	pop	ar2
;	genIfx
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00101$
00118$:
;	..\test_menu/..\ds1822\ds1822.c:27: 
;	genCall
	push	ar2
	lcall	_ow_reset
	pop	ar2
;	..\test_menu/..\ds1822\ds1822.c:28: #define KB_BASE 0x9000
;	genCall
	mov	dpl,#0xCC
	push	ar2
	lcall	_ow_wb
	pop	ar2
;	..\test_menu/..\ds1822\ds1822.c:29: 
;	genCall
	mov	dpl,#0xBE
	push	ar2
	lcall	_ow_wb
	pop	ar2
;	..\test_menu/..\ds1822\ds1822.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genAssign
	mov	r3,#0x00
00106$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x02,00119$
00119$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00109$
00120$:
;	..\test_menu/..\ds1822\ds1822.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genPlus
	mov	a,ar3
	add	a,#_get_T_scratchpad_1_1
	mov	r0,a
;	genCall
	push	ar2
	push	ar3
	push	ar0
	lcall	_ow_rb
	mov	a,dpl
	pop	ar0
	pop	ar3
	pop	ar2
;	genPointerSet
;	genNearPointerSet
	mov	@r0,acc
;	..\test_menu/..\ds1822\ds1822.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genPlus
;	genPlusIncr
	inc	r3
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00109$:
;	..\test_menu/..\ds1822\ds1822.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genPointerGet
;	genNearPointerGet
;	genDataPointerGet
	mov	r3,_get_T_scratchpad_1_1
	mov	r4,(_get_T_scratchpad_1_1 + 1)
;	genRightShift
;	genSignedRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	a,r4
	swap	a
	xch	a,r3
	swap	a
	anl	a,#0x0f
	xrl	a,r3
	xch	a,r3
	anl	a,#0x0f
	xch	a,r3
	xrl	a,r3
	xch	a,r3
	jnb	acc.3,00121$
	orl	a,#0xf0
00121$:
	mov	r4,a
;	genCast
	mov	ar2,r3
00105$:
;	..\test_menu/..\ds1822\ds1822.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genRet
	mov	dpl,r2
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_ds1822'
;------------------------------------------------------------
;sign                      Allocated to registers r3 
;temperature               Allocated to registers r2 
;------------------------------------------------------------
;	..\test_menu/..\ds1822\ds1822.c:38: 
;	-----------------------------------------
;	 function test_ds1822
;	-----------------------------------------
_test_ds1822:
;	..\test_menu/..\ds1822\ds1822.c:43: // I2C
00107$:
;	..\test_menu/..\ds1822\ds1822.c:44: #define SCL P1_1
;	genCall
	lcall	_ow_reset
	mov	a,dpl
;	genIfx
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00105$
00115$:
;	..\test_menu/..\ds1822\ds1822.c:45: #define SDA P1_0
;	genCall
	lcall	_set_accurasy
;	..\test_menu/..\ds1822\ds1822.c:46: #define I2CDELAY() delay8(3)
;	genCall
	lcall	_get_T
	mov	a,dpl
;	genAssign
;	..\test_menu/..\ds1822\ds1822.c:47: // I2C
;	genCmpLt
;	genCmp
; Peephole 105   removed redundant mov
	mov  r2,a
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  acc.7,00102$
00116$:
;	genUminus
	clr	c
	clr	a
	subb	a,r2
	mov	r2,a
;	genAssign
	mov	r3,#0x10
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00102$:
;	..\test_menu/..\ds1822\ds1822.c:48: 
;	genAssign
	mov	r3,#0x00
00103$:
;	..\test_menu/..\ds1822\ds1822.c:49: unsigned char key_scan()
;	genCast
	mov	a,r2
	rlc	a
	subb	a,acc
	mov	r4,a
;	genAssign
	mov	_display_int_PARM_2,r3
;	genCall
	mov	dpl,r2
	mov	dph,r4
	lcall	_display_int
; Peephole 132   changed ljmp to sjmp
	sjmp 00108$
00105$:
;	..\test_menu/..\ds1822\ds1822.c:51: _asm;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0xFF
	movx	@dptr,a
00108$:
;	..\test_menu/..\ds1822\ds1822.c:52: mov	r0, #0
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00117$
; Peephole 132   changed ljmp to sjmp
	sjmp 00107$
00117$:
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'uart_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\test_menu/..\uart\uart.c:13: 
;	-----------------------------------------
;	 function uart_init
;	-----------------------------------------
_uart_init:
;	..\test_menu/..\uart\uart.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	_PCON,#0x80
;	..\test_menu/..\uart\uart.c:16: 
;	genAssign
	mov	_SCON,#0x52
;	..\test_menu/..\uart\uart.c:17: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	_TMOD,#0x22
;	..\test_menu/..\uart\uart.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	_TCON,#0x00
;	..\test_menu/..\uart\uart.c:24: 
;	genAssign
	mov	_TH1,#0xE0
;	..\test_menu/..\uart\uart.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	setb	_TR1
;	..\test_menu/..\uart\uart.c:27: 
;	genAssign
	mov	_TH0,#0xAF
;	..\test_menu/..\uart\uart.c:28: #define KB_BASE 0x9000
;	genAssign
	setb	_TR0
;	..\test_menu/..\uart\uart.c:29: 
;	genAssign
	setb	_ET0
;	..\test_menu/..\uart\uart.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genAssign
	setb	_EA
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_uart'
;------------------------------------------------------------
;channel                   Allocated to registers r2 
;tmp                       Allocated to registers r3 
;------------------------------------------------------------
;	..\test_menu/..\uart\uart.c:41: // One Wire
;	-----------------------------------------
;	 function test_uart
;	-----------------------------------------
_test_uart:
;	..\test_menu/..\uart\uart.c:43: // I2C
;	genAssign
	mov	r2,#0x00
;	..\test_menu/..\uart\uart.c:45: #define SDA P1_0
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\test_menu/..\uart\uart.c:46: #define I2CDELAY() delay8(3)
;	genCall
	push	ar2
	lcall	_uart_init
	pop	ar2
;	..\test_menu/..\uart\uart.c:47: // I2C
;	genAssign
	mov	dptr,#_UC_REG
; Peephole 180   changed mov to clr
;	..\test_menu/..\uart\uart.c:48: 
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_lefti
	movx  @dptr,a
;	..\test_menu/..\uart\uart.c:50: {
;	genCall
	push	ar2
	lcall	_hd_init
	pop	ar2
;	..\test_menu/..\uart\uart.c:51: _asm;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_0
	mov	(_string_to_hd_PARM_3 + 1),#(__str_0 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_string_to_hd
	pop	ar2
;	..\test_menu/..\uart\uart.c:52: mov	r0, #0
;	genCast
	mov	_string_to_hd_PARM_3,#__str_1
	mov	(_string_to_hd_PARM_3 + 1),#(__str_1 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_string_to_hd
	pop	ar2
;	..\test_menu/..\uart\uart.c:53: mov	dptr, #KB_BASE+0xFE
;	genCast
	mov	_string_to_hd_PARM_3,#__str_2
	mov	(_string_to_hd_PARM_3 + 1),#(__str_2 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_string_to_hd
	pop	ar2
;	..\test_menu/..\uart\uart.c:54: next_key_line:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_3
	mov	(_string_to_hd_PARM_3 + 1),#(__str_3 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x03
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_string_to_hd
	pop	ar2
;	..\test_menu/..\uart\uart.c:55: ; check scan line
00101$:
;	..\test_menu/..\uart\uart.c:56: movx	a, @dptr
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P3_3,00105$
00129$:
;	..\test_menu/..\uart\uart.c:57: jnb	acc.0, end_key_scan
;	genPlus
;	genPlusIncr
	inc	r2
;	genCmpGt
;	genCmp
	clr	c
; Peephole 159   avoided xrl during execution
	mov  a,#(0x02 ^ 0x80)
	mov	b,r2
	xrl	b,#0x80
	subb	a,b
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00103$
00130$:
;	genAssign
	mov	r2,#0x00
00103$:
;	..\test_menu/..\uart\uart.c:58: inc	r0
;	genAssign
	mov	dptr,#_UC_REG
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\uart\uart.c:59: jnb	acc.1, end_key_scan
;	genAssign
	mov	dptr,#_lefti
	mov	a,r2
	movx	@dptr,a
;	..\test_menu/..\uart\uart.c:60: inc	r0
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	lcall	_delay16
	pop	ar2
00105$:
;	..\test_menu/..\uart\uart.c:63: jnb	acc.3, end_key_scan
;	genCall
	push	ar2
	lcall	_kb_scan
	mov	r3,dpl
	pop	ar2
;	genPlus
	mov	a,ar3
; Peephole 180   changed mov to clr
;	genPointerGet
;	genCodePointerGet
; Peephole 186   optimized movc sequence
	mov  dptr,#_layout
	movc a,@a+dptr
	mov	r3,a
;	..\test_menu/..\uart\uart.c:64: inc	r0
;	genCmpEq
	cjne	r3,#0x0C,00131$
; Peephole 132   changed ljmp to sjmp
	sjmp 00115$
00131$:
;	..\test_menu/..\uart\uart.c:65: ; goto next scan line
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne r3,#0x0B,00107$
;00132$:
; Peephole 200   removed redundant sjmp
00133$:
;	genRet
	ljmp	00118$
00107$:
;	..\test_menu/..\uart\uart.c:66: mov	a, dpl
;	genCmpEq
	cjne	r2,#0x02,00134$
; Peephole 132   changed ljmp to sjmp
	sjmp 00112$
00134$:
;	..\test_menu/..\uart\uart.c:67: rl	a
;	genPlus
	mov	a,#0x30
	add	a,ar3
	mov	_SBUF,a
;	..\test_menu/..\uart\uart.c:68: mov	dpl, a
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
; Peephole 132   changed ljmp to sjmp
	sjmp 00115$
00112$:
;	..\test_menu/..\uart\uart.c:71: mov	dpl, r0
;	genAssign
	mov	dptr,#_UC_REG
	mov	a,#0x03
	movx	@dptr,a
;	..\test_menu/..\uart\uart.c:72: _endasm;
;	genPlus
	mov	a,#0x30
	add	a,ar3
	mov	_SBUF,a
;	..\test_menu/..\uart\uart.c:73: }
;	genAssign
	clr	_TI
00108$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _TI,00108$
00135$:
;	..\test_menu/..\uart\uart.c:74: 
;	genAssign
	mov	dptr,#_UC_REG
	mov	a,#0x02
	movx	@dptr,a
;	..\test_menu/..\uart\uart.c:75: /*
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu/..\uart\uart.c:76: ��⢥��⢨� ᪠� ���� �����頥���� �㭪樥� key_scan �ᯮ������� ������
;	genAssign
	clr	_RI
00115$:
;	..\test_menu/..\uart\uart.c:80: 3 7 11
;	genIfx
;	genIfxJump
	jb	_RI,00136$
	ljmp	00101$
00136$:
;	..\test_menu/..\uart\uart.c:81: */
;	genAssign
	clr	_RI
;	..\test_menu/..\uart\uart.c:82: 
;	genMinus
	mov	a,_SBUF
	add	a,#0xd0
;	genAssign
; Peephole 100   removed redundant mov
	mov  r3,a
	mov  dptr,#_righti
	movx @dptr,a
;	..\test_menu/..\uart\uart.c:83: //code unsigned char layout [] = {1,4,7,10, 2,5,8,0, 3,6,9,11, 12};
;	genAssign
	clr	a
	mov	(_need_zoom + 1),a
	mov	_need_zoom,#0x64
;	..\test_menu/..\uart\uart.c:85: 1	2	3
	ljmp	00101$
00118$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_menu'
;------------------------------------------------------------
;key                       Allocated to registers r4 
;test_number               Allocated to in memory with name '_test_menu_test_number_1_1'
;index                     Allocated to registers r2 
;number                    Allocated to registers r3 
;------------------------------------------------------------
;	..\test_menu\menu.c:17: xdata at 0xf000 unsigned char pDAC;
;	-----------------------------------------
;	 function test_menu
;	-----------------------------------------
_test_menu:
;	..\test_menu\menu.c:19: 
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_test_menu_test_number_1_1,#0x00
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_test_menu_test_number_1_1 + 0x0001),#0x00
;	genAssign
	mov	r2,#0x00
;	genAssign
	mov	r3,#0x00
;	..\test_menu\menu.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0xFF
	movx	@dptr,a
;	..\test_menu\menu.c:22: xdata at 0xA005 unsigned char EDC_REG;
00101$:
;	genCall
	push	ar2
	push	ar3
	lcall	_key_scan
	mov	r4,dpl
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r4,#0x0C,00194$
00194$:
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00101$
00195$:
;	..\test_menu\menu.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:24: 
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x03
	movx	@dptr,a
;	..\test_menu\menu.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genCall
	push	ar2
	push	ar3
	lcall	_t0_for_zoom_init
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genCall
	push	ar2
	push	ar3
	lcall	_hd_turn_on
	pop	ar3
	pop	ar2
;	genCall
	push	ar2
	push	ar3
	lcall	_hd_init
	pop	ar3
	pop	ar2
;	genCast
	mov	_string_to_hd_PARM_3,#__str_4
	mov	(_string_to_hd_PARM_3 + 1),#(__str_4 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x03
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	lcall	_string_to_hd
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:27: 
00104$:
;	..\test_menu\menu.c:28: #define KB_BASE 0x9000
;	genCall
	push	ar2
	push	ar3
	lcall	_kb_scan
	mov	r4,dpl
	pop	ar3
	pop	ar2
;	genPlus
	mov	a,ar4
; Peephole 180   changed mov to clr
;	genPointerGet
;	genCodePointerGet
; Peephole 186   optimized movc sequence
	mov  dptr,#_layout
	movc a,@a+dptr
	mov	r4,a
;	..\test_menu\menu.c:29: 
;	genCmpEq
	cjne	r4,#0x0C,00196$
	mov	a,#0x01
	sjmp	00197$
00196$:
	clr	a
00197$:
;	genIfx
; Peephole 105   removed redundant mov
	mov  r5,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00106$
00198$:
;	genAssign
	mov	_need_zoom,#0xE8
	mov	(_need_zoom + 1),#0x03
00106$:
;	..\test_menu\menu.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x0B
	subb	a,r4
;	genIfxJump
	jnc	00199$
	ljmp	00153$
00199$:
;	genJumpTab
	mov	a,r4
	add	a,acc
	add	a,r4
	mov	dptr,#00200$
	jmp	@a+dptr
00200$:
	ljmp	00116$
	ljmp	00115$
	ljmp	00114$
	ljmp	00113$
	ljmp	00112$
	ljmp	00111$
	ljmp	00110$
	ljmp	00109$
	ljmp	00108$
	ljmp	00107$
	ljmp	00135$
	ljmp	00136$
;	..\test_menu\menu.c:33: 
00116$:
00115$:
00114$:
00113$:
00112$:
00111$:
00110$:
00109$:
00108$:
00107$:
;	genAssign
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_test_menu_test_number_1_1 + 0x0001),_test_menu_test_number_1_1
;	..\test_menu\menu.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_test_menu_test_number_1_1,r4
;	..\test_menu\menu.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00118$
00201$:
;	genAssign
	mov	r2,#0x00
; Peephole 132   changed ljmp to sjmp
	sjmp 00119$
00118$:
;	..\test_menu\menu.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genAssign
	mov	r2,#0x01
00119$:
;	..\test_menu\menu.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0001 + _test_menu_test_number_1_1
	swap	a
	anl	a,#0xf0
	mov	r6,a
;	genAssign
;	genOr
	mov	a,_test_menu_test_number_1_1
	orl	a,r6
	mov	r3,a
;	..\test_menu\menu.c:38: 
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_hd_init
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:39: // One Wire
;	genCmpEq
	cjne	r3,#0x01,00202$
; Peephole 132   changed ljmp to sjmp
	sjmp 00120$
00202$:
;	genCmpEq
	cjne	r3,#0x02,00203$
	ljmp	00121$
00203$:
;	genCmpEq
	cjne	r3,#0x03,00204$
	ljmp	00122$
00204$:
;	genCmpEq
	cjne	r3,#0x04,00205$
	ljmp	00123$
00205$:
;	genCmpEq
	cjne	r3,#0x05,00206$
	ljmp	00124$
00206$:
;	genCmpEq
	cjne	r3,#0x06,00207$
	ljmp	00125$
00207$:
;	genCmpEq
	cjne	r3,#0x07,00208$
	ljmp	00126$
00208$:
;	genCmpEq
	cjne	r3,#0x08,00209$
	ljmp	00127$
00209$:
;	genCmpEq
	cjne	r3,#0x09,00210$
	ljmp	00128$
00210$:
;	genCmpEq
	cjne	r3,#0x10,00211$
	ljmp	00129$
00211$:
;	genCmpEq
	cjne	r3,#0x11,00212$
	ljmp	00130$
00212$:
;	genCmpEq
	cjne	r3,#0x12,00213$
	ljmp	00131$
00213$:
;	genCmpEq
	cjne	r3,#0x13,00214$
	ljmp	00132$
00214$:
;	genCmpEq
	cjne	r3,#0x14,00215$
	ljmp	00133$
00215$:
	ljmp	00153$
;	..\test_menu\menu.c:42: 
00120$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_5
	mov	(_string_to_hd_PARM_3 + 1),#(__str_5 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:43: // I2C
;	genCast
	mov	_string_to_hd_PARM_3,#__str_6
	mov	(_string_to_hd_PARM_3 + 1),#(__str_6 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:44: #define SCL P1_1
;	genCast
	mov	_string_to_hd_PARM_3,#__str_7
	mov	(_string_to_hd_PARM_3 + 1),#(__str_7 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x04
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:45: #define SDA P1_0
	ljmp	00153$
;	..\test_menu\menu.c:47: // I2C
00121$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_8
	mov	(_string_to_hd_PARM_3 + 1),#(__str_8 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:48: 
;	genCast
	mov	_string_to_hd_PARM_3,#__str_9
	mov	(_string_to_hd_PARM_3 + 1),#(__str_9 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:49: unsigned char key_scan()
;	genCast
	mov	_string_to_hd_PARM_3,#__str_10
	mov	(_string_to_hd_PARM_3 + 1),#(__str_10 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x04
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:50: {
	ljmp	00153$
;	..\test_menu\menu.c:52: mov	r0, #0
00122$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_11
	mov	(_string_to_hd_PARM_3 + 1),#(__str_11 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:53: mov	dptr, #KB_BASE+0xFE
;	genCast
	mov	_string_to_hd_PARM_3,#__str_12
	mov	(_string_to_hd_PARM_3 + 1),#(__str_12 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:54: next_key_line:
	ljmp	00153$
;	..\test_menu\menu.c:56: movx	a, @dptr
00123$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_13
	mov	(_string_to_hd_PARM_3 + 1),#(__str_13 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:57: jnb	acc.0, end_key_scan
;	genCast
	mov	_string_to_hd_PARM_3,#__str_14
	mov	(_string_to_hd_PARM_3 + 1),#(__str_14 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:58: inc	r0
;	genCast
	mov	_string_to_hd_PARM_3,#__str_15
	mov	(_string_to_hd_PARM_3 + 1),#(__str_15 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:59: jnb	acc.1, end_key_scan
	ljmp	00153$
;	..\test_menu\menu.c:61: jnb	acc.2, end_key_scan
00124$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_16
	mov	(_string_to_hd_PARM_3 + 1),#(__str_16 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:62: inc	r0
;	genCast
	mov	_string_to_hd_PARM_3,#__str_17
	mov	(_string_to_hd_PARM_3 + 1),#(__str_17 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:63: jnb	acc.3, end_key_scan
;	genCast
	mov	_string_to_hd_PARM_3,#__str_18
	mov	(_string_to_hd_PARM_3 + 1),#(__str_18 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:64: inc	r0
	ljmp	00153$
;	..\test_menu\menu.c:66: mov	a, dpl
00125$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_19
	mov	(_string_to_hd_PARM_3 + 1),#(__str_19 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:67: rl	a
;	genCast
	mov	_string_to_hd_PARM_3,#__str_20
	mov	(_string_to_hd_PARM_3 + 1),#(__str_20 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:68: mov	dpl, a
;	genCast
	mov	_string_to_hd_PARM_3,#__str_21
	mov	(_string_to_hd_PARM_3 + 1),#(__str_21 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:69: cjne	a, #0xF7, next_key_line
	ljmp	00153$
;	..\test_menu\menu.c:71: mov	dpl, r0
00126$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_22
	mov	(_string_to_hd_PARM_3 + 1),#(__str_22 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:72: _endasm;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_23
	mov	(_string_to_hd_PARM_3 + 1),#(__str_23 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:73: }
;	genCast
	mov	_string_to_hd_PARM_3,#__str_24
	mov	(_string_to_hd_PARM_3 + 1),#(__str_24 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:74: 
;	genCast
	mov	_string_to_hd_PARM_3,#__str_25
	mov	(_string_to_hd_PARM_3 + 1),#(__str_25 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x03
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:75: /*
	ljmp	00153$
;	..\test_menu\menu.c:77: 0 4 8
00127$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_26
	mov	(_string_to_hd_PARM_3 + 1),#(__str_26 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:78: 1 5 9
;	genCast
	mov	_string_to_hd_PARM_3,#__str_27
	mov	(_string_to_hd_PARM_3 + 1),#(__str_27 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:79: 2 6 10
;	genCast
	mov	_string_to_hd_PARM_3,#__str_28
	mov	(_string_to_hd_PARM_3 + 1),#(__str_28 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:80: 3 7 11
	ljmp	00153$
;	..\test_menu\menu.c:82: 
00128$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_29
	mov	(_string_to_hd_PARM_3 + 1),#(__str_29 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:83: //code unsigned char layout [] = {1,4,7,10, 2,5,8,0, 3,6,9,11, 12};
;	genCast
	mov	_string_to_hd_PARM_3,#__str_30
	mov	(_string_to_hd_PARM_3 + 1),#(__str_30 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:84: /*
;	genCast
	mov	_string_to_hd_PARM_3,#__str_31
	mov	(_string_to_hd_PARM_3 + 1),#(__str_31 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:85: 1	2	3
	ljmp	00153$
;	..\test_menu\menu.c:87: 7	8	9
00129$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_32
	mov	(_string_to_hd_PARM_3 + 1),#(__str_32 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:88: *	0	#
;	genCast
	mov	_string_to_hd_PARM_3,#__str_33
	mov	(_string_to_hd_PARM_3 + 1),#(__str_33 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:91: /*
	ljmp	00153$
;	..\test_menu\menu.c:93: 1	2	3
00130$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_34
	mov	(_string_to_hd_PARM_3 + 1),#(__str_34 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:94: 4	5	6
;	genCast
	mov	_string_to_hd_PARM_3,#__str_35
	mov	(_string_to_hd_PARM_3 + 1),#(__str_35 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:95: 7	8	9
;	genCast
	mov	_string_to_hd_PARM_3,#__str_36
	mov	(_string_to_hd_PARM_3 + 1),#(__str_36 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:96: */
	ljmp	00153$
;	..\test_menu\menu.c:98: {
00131$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_37
	mov	(_string_to_hd_PARM_3 + 1),#(__str_37 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:99: static unsigned char prev_scan;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_38
	mov	(_string_to_hd_PARM_3 + 1),#(__str_38 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:100: static unsigned char timer;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_39
	mov	(_string_to_hd_PARM_3 + 1),#(__str_39 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:101: //  unsigned char key=12;
	ljmp	00153$
;	..\test_menu\menu.c:103: scan = key_scan();
00132$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_40
	mov	(_string_to_hd_PARM_3 + 1),#(__str_40 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:104: if (scan == prev_scan) {
;	genCast
	mov	_string_to_hd_PARM_3,#__str_41
	mov	(_string_to_hd_PARM_3 + 1),#(__str_41 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:105: timer++;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_42
	mov	(_string_to_hd_PARM_3 + 1),#(__str_42 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:106: //    if (timer == 0) key = scan;
	ljmp	00153$
;	..\test_menu\menu.c:108: } else {
00133$:
;	genCast
	mov	_string_to_hd_PARM_3,#__str_43
	mov	(_string_to_hd_PARM_3 + 1),#(__str_43 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:109: timer = 0;
;	genCast
	mov	_string_to_hd_PARM_3,#__str_44
	mov	(_string_to_hd_PARM_3 + 1),#(__str_44 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:110: }
;	genCast
	mov	_string_to_hd_PARM_3,#__str_45
	mov	(_string_to_hd_PARM_3 + 1),#(__str_45 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x02
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_string_to_hd
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:115: void lights_off()
	ljmp	00153$
;	..\test_menu\menu.c:116: {
00135$:
;	genAssign
	mov	r2,#0x00
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_test_menu_test_number_1_1,#0x00
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_test_menu_test_number_1_1 + 0x0001),#0x00
	ljmp	00153$
;	..\test_menu\menu.c:118: pA=0;
00136$:
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x9C)<<8) + 0x40)
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_delay16
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:119: pB=0;
;	genAssign
	clr	_EA
;	..\test_menu\menu.c:120: pC=0xFF;
;	genCmpEq
	cjne	r3,#0x01,00216$
; Peephole 132   changed ljmp to sjmp
	sjmp 00137$
00216$:
;	genCmpEq
	cjne	r3,#0x02,00217$
; Peephole 132   changed ljmp to sjmp
	sjmp 00138$
00217$:
;	genCmpEq
	cjne	r3,#0x03,00218$
	ljmp	00139$
00218$:
;	genCmpEq
	cjne	r3,#0x04,00219$
	ljmp	00140$
00219$:
;	genCmpEq
	cjne	r3,#0x05,00220$
	ljmp	00141$
00220$:
;	genCmpEq
	cjne	r3,#0x06,00221$
	ljmp	00142$
00221$:
;	genCmpEq
	cjne	r3,#0x07,00222$
	ljmp	00143$
00222$:
;	genCmpEq
	cjne	r3,#0x08,00223$
	ljmp	00144$
00223$:
;	genCmpEq
	cjne	r3,#0x09,00224$
	ljmp	00145$
00224$:
;	genCmpEq
	cjne	r3,#0x10,00225$
	ljmp	00146$
00225$:
;	genCmpEq
	cjne	r3,#0x11,00226$
	ljmp	00147$
00226$:
;	genCmpEq
	cjne	r3,#0x12,00227$
	ljmp	00148$
00227$:
;	genCmpEq
	cjne	r3,#0x13,00228$
	ljmp	00149$
00228$:
;	genCmpEq
	cjne	r3,#0x14,00229$
	ljmp	00150$
00229$:
;	genCmpEq
	cjne	r3,#0x99,00230$
	ljmp	00151$
00230$:
	ljmp	00152$
;	..\test_menu\menu.c:122: }
00137$:
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_t0_t1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:123: void display_int(unsigned int value, unsigned char dc_mask)
00138$:
;	genCall
	mov	dpl,#0x01
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_t0_t1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:124: {
00139$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_leds
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:125: static unsigned char buffer[4];
00140$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_e40281
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:126: unsigned char data *ptr = buffer;
00141$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_ta07_11
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:127: unsigned char i, dc = 0;
00142$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_adc
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:128: /* �८�ࠧ������ */
00143$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_knopki
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:129: i = 4; do {
00144$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_dac
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:130: char c;
00145$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_i
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00152$
;	..\test_menu\menu.c:131: c = value % 10;
00146$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_ds1307
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:132: value /= 10;
; Peephole 132   changed ljmp to sjmp
	sjmp 00152$
00147$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_ds1621
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:133: *ptr++ = c;
; Peephole 132   changed ljmp to sjmp
	sjmp 00152$
00148$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_at24
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:134: } while (--i);
; Peephole 132   changed ljmp to sjmp
	sjmp 00152$
00149$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_ds1822
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:135: /* ��襭�� �㫥� */
; Peephole 132   changed ljmp to sjmp
	sjmp 00152$
00150$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_test_uart
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:137: --ptr;
; Peephole 132   changed ljmp to sjmp
	sjmp 00152$
00151$:
;	genRet
;	..\test_menu\menu.c:138: if (!*ptr) dc = (dc << 1) | 1;
; Peephole 132   changed ljmp to sjmp
	sjmp 00156$
00152$:
;	..\test_menu\menu.c:139: else break;
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_t0_for_zoom_init
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:140: } while (--i);
;	genAssign
	mov	r2,#0x00
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	_test_menu_test_number_1_1,#0x00
;	genPointerSet
;	genNearPointerSet
;	genDataPointerSet
	mov	(_test_menu_test_number_1_1 + 0x0001),#0x00
;	genAssign
	mov	_need_zoom,#0xE8
	mov	(_need_zoom + 1),#0x03
;	..\test_menu\menu.c:142: /* �뢮� */
00153$:
;	..\test_menu\menu.c:143: DISPLAY[0] = buffer[3] << 4 | buffer[2];
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_lights_off
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:144: DISPLAY[1] = buffer[1] << 4 | buffer[0];
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x03
	movx	@dptr,a
;	..\test_menu\menu.c:145: }
;	genAssign
	mov	dptr,#_righti
	mov	a,r3
	movx	@dptr,a
;	..\test_menu\menu.c:147: void hd_turn_on()
;	genIfx
	mov	a,r5
;	genIfxJump
	jz	00231$
	ljmp	00104$
00231$:
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	push	ar2
	push	ar3
	push	ar4
	lcall	_delay16
	pop	ar4
	pop	ar3
	pop	ar2
;	..\test_menu\menu.c:148: {
	ljmp	00104$
00156$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hardware_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	boot.c:10: xdata at 0x8000 unsigned char PA_REG;
;	-----------------------------------------
;	 function hardware_init
;	-----------------------------------------
_hardware_init:
;	boot.c:13: 
;	genAssign
	mov	_PCON,#0x80
;	boot.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	mov	_SCON,#0x50
;	boot.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	_TMOD,#0x21
;	boot.c:16: 
;	genAssign
	mov	_TCON,#0x00
;	boot.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	_TH1,#0xFC
;	boot.c:19: 
;	genAssign
	setb	_TR1
;	boot.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genCall
	lcall	_lights_off
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'memory_test'
;------------------------------------------------------------
;------------------------------------------------------------
;	boot.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	-----------------------------------------
;	 function memory_test
;	-----------------------------------------
_memory_test:
;	boot.c:60: inc	r0
;	genInline
;
	        mov dptr, #_DC_REG
	        mov a, #0x1F
	        movx @dptr, a
	        ; write memory
	        mov dptr, #0
  next_byte_load:
	        mov a, dph
	        add a, dpl
	        movx @dptr, a
	        inc dptr
	        mov a, dph
	        cjne a, #0x80, next_byte_load
	        mov dptr, #_DC_REG
	        mov a, #0x3F
	        movx @dptr, a
	        ; verify memory
	        mov dptr, #0
  next_byte_verify:
	        movx a, @dptr
	        mov r0, a
	        mov a, dph
	        add a, dpl
	        cjne a, ar0, exit_test
	        inc dptr
	        mov a, dph
	        cjne a, #0x80, next_byte_verify
  exit_test:
	        mov _memory_size, dpl
	        mov _memory_size+1, dph
;	boot.c:61: jnb	acc.2, end_key_scan
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	dpl,_memory_size
	mov	a,(_memory_size + 1)
	mov	c,acc.7
	xch	a,dpl
	rlc	a
	xch	a,dpl
	rlc	a
	xch	a,dpl
	anl	a,#0x01
	mov	dph,a
;	genAssign
	mov	_display_int_PARM_2,#0x00
;	genCall
	lcall	_display_int
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;addr                      Allocated to in memory with name '_main_addr_1_1'
;timeout                   Allocated to registers r2 r3 
;i_timeout                 Allocated to registers r4 r5 
;checksum                  Allocated to registers r6 
;dc_switch                 Allocated to registers r1 
;c                         Allocated to registers 
;------------------------------------------------------------
;	boot.c:64: inc	r0
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	boot.c:66: mov	a, dpl
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
;	boot.c:67: rl	a
;	genAssign
	mov	r6,#0x00
;	genAssign
	mov	r1,#0x00
;	boot.c:68: mov	dpl, a
00101$:
;	boot.c:69: cjne	a, #0xF7, next_key_line
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_hardware_init
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	boot.c:70: end_key_scan:
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:71: mov	dpl, r0
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_ds1307_init
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	boot.c:72: _endasm;
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_hd_turn_on
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_hd_init
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	boot.c:73: }
;	genCast
	mov	_string_to_hd_PARM_3,#__str_46
	mov	(_string_to_hd_PARM_3 + 1),#(__str_46 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x00
;	genCall
	mov	dpl,#0x03
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_string_to_hd
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
	mov	_string_to_hd_PARM_3,#__str_47
	mov	(_string_to_hd_PARM_3 + 1),#(__str_47 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x01
;	genCall
	mov	dpl,#0x02
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_string_to_hd
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
	mov	_string_to_hd_PARM_3,#__str_48
	mov	(_string_to_hd_PARM_3 + 1),#(__str_48 >> 8)
	mov	(_string_to_hd_PARM_3 + 2),#0x2
;	genAssign
	mov	_string_to_hd_PARM_2,#0x03
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_string_to_hd
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	boot.c:74: 
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_memory_test
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	boot.c:75: /*
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_key_scan
	mov	r0,dpl
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCmpLt
;	genCmp
	cjne	r0,#0x0C,00131$
00131$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00128$
00132$:
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_test_menu
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00101$
00128$:
;	genAssign
	clr	a
	mov	(_main_addr_1_1 + 1),a
	mov	_main_addr_1_1,a
00118$:
;	boot.c:77: 0 4 8
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _RI,00105$
00133$:
;	boot.c:79: 2 6 10
;	genAssign
	mov	r7,_SBUF
;	genAssign
	clr	_RI
;	boot.c:80: 3 7 11
;	genCast
	mov	dpl,_main_addr_1_1
	mov	dph,(_main_addr_1_1 + 1)
;	genPointerSet
;	genFarPointerSet
	mov	a,r7
	movx	@dptr,a
;	boot.c:81: */
;	genPlus
	mov	a,ar7
	add	a,ar6
;	boot.c:82: 
;	genPointerSet
;	genFarPointerSet
; Peephole 100   removed redundant mov
	mov  r6,a
	mov  dptr,#(_DISPLAY + 0x0001)
	movx @dptr,a
;	boot.c:83: //code unsigned char layout [] = {1,4,7,10, 2,5,8,0, 3,6,9,11, 12};
;	genAssign
	mov	dptr,#_DC_REG
	mov	a,#0x83
	movx	@dptr,a
;	boot.c:84: /*
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,_main_addr_1_1
	mov	_main_addr_1_1,a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,(_main_addr_1_1 + 1)
	mov	(_main_addr_1_1 + 1),a
;	boot.c:85: 1	2	3
;	genAssign
	mov	r2,#0x01
	mov	r3,#0x00
00105$:
;	boot.c:87: 7	8	9
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00111$
00134$:
;	boot.c:88: *	0	#
;	genPlus
;	genPlusIncr
	inc	r2
	cjne	r2,#0x00,00135$
	inc	r3
00135$:
;	boot.c:89: */
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 198   optimized misc jump sequence
	cjne r2,#0x30,00111$
	cjne r3,#0x75,00111$
;00136$:
; Peephole 200   removed redundant sjmp
00137$:
;	boot.c:90: code unsigned char layout [] = {10,1,4,7, 0,2,5,8, 11,3,6,9, 12};
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
;	boot.c:91: /*
;	genAssign
	mov	dptr,#_DC_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	boot.c:93: 1	2	3
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne r6,#0x00,00111$
;00138$:
; Peephole 200   removed redundant sjmp
00139$:
;	boot.c:94: 4	5	6
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#_DISPLAY
	mov	a,#0xFF
	movx	@dptr,a
;	boot.c:95: 7	8	9
;	genAssign
	mov	dptr,#_SYS_CTL
	mov	a,#0x01
	movx	@dptr,a
00111$:
;	boot.c:100: static unsigned char timer;
;	genAssign
	mov	ar7,r4
	mov	ar0,r5
;	genPlus
;	genPlusIncr
	inc	r4
	cjne	r4,#0x00,00140$
	inc	r5
00140$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x20
	subb	a,r7
	mov	a,#0x4E
	subb	a,r0
;	genIfxJump
	jc	00141$
	ljmp	00118$
00141$:
;	boot.c:101: //  unsigned char key=12;
;	genAssign
;	boot.c:102: char scan;
;	genCpl
; Peephole 3.c   changed mov to clr
	clr  a
	mov  r4,a
	mov  r5,a
	mov  a,r1
	cpl	a
;	genAssign
;	genIfx
; Peephole 166   removed redundant mov
	mov  r7,a
	mov  ar1,r7 
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00113$
00142$:
;	genAssign
	mov	dptr,#_DC_REG
	mov	a,#0x01
	movx	@dptr,a
	ljmp	00118$
00113$:
;	boot.c:103: scan = key_scan();
;	genAssign
	mov	dptr,#_DC_REG
	mov	a,#0x0F
	movx	@dptr,a
	ljmp	00118$
00120$:
	ret
	.area CSEG    (CODE)
_layout:
	.db #0x0A
	.db #0x01
	.db #0x04
	.db #0x07
	.db #0x00
	.db #0x02
	.db #0x05
	.db #0x08
	.db #0x0B
	.db #0x03
	.db #0x06
	.db #0x09
	.db #0x0C
__str_0:
	.ascii "1200 bps"
	.db 0x00
__str_1:
	.ascii "press INT1"
	.db 0x00
__str_2:
	.ascii "to change"
	.db 0x00
__str_3:
	.ascii "channel"
	.db 0x00
__str_4:
	.ascii "v3.21"
	.db 0x00
__str_5:
	.ascii "Test"
	.db 0x00
__str_6:
	.ascii "generator"
	.db 0x00
__str_7:
	.ascii "T0"
	.db 0x00
__str_8:
	.ascii "Test"
	.db 0x00
__str_9:
	.ascii "generator"
	.db 0x00
__str_10:
	.ascii "T1"
	.db 0x00
__str_11:
	.ascii "Test"
	.db 0x00
__str_12:
	.ascii "leds"
	.db 0x00
__str_13:
	.ascii "Test"
	.db 0x00
__str_14:
	.ascii "dinamic"
	.db 0x00
__str_15:
	.ascii "indication"
	.db 0x00
__str_16:
	.ascii "Test"
	.db 0x00
__str_17:
	.ascii "matrix"
	.db 0x00
__str_18:
	.ascii "indication"
	.db 0x00
__str_19:
	.ascii "Test"
	.db 0x00
__str_20:
	.ascii "AD7801 in"
	.db 0x00
__str_21:
	.ascii "ADC mode"
	.db 0x00
__str_22:
	.ascii "Test"
	.db 0x00
__str_23:
	.ascii "buttons on"
	.db 0x00
__str_24:
	.ascii "P3.2 P3.3"
	.db 0x00
__str_25:
	.ascii "INT0 INT1"
	.db 0x00
__str_26:
	.ascii "Test"
	.db 0x00
__str_27:
	.ascii "AD7801 in"
	.db 0x00
__str_28:
	.ascii "DAC mode"
	.db 0x00
__str_29:
	.ascii "Test"
	.db 0x00
__str_30:
	.ascii "static"
	.db 0x00
__str_31:
	.ascii "indication"
	.db 0x00
__str_32:
	.ascii "Test"
	.db 0x00
__str_33:
	.ascii "RTC DS1307"
	.db 0x00
__str_34:
	.ascii "Test"
	.db 0x00
__str_35:
	.ascii "t"
	.db 0x01
	.ascii "C sensor"
	.db 0x00
__str_36:
	.ascii "DS1621(31)"
	.db 0x00
__str_37:
	.ascii "Test"
	.db 0x00
__str_38:
	.ascii "AT24series"
	.db 0x00
__str_39:
	.ascii "EEPROM"
	.db 0x00
__str_40:
	.ascii "Test"
	.db 0x00
__str_41:
	.ascii "t"
	.db 0x01
	.ascii "C sensor"
	.db 0x00
__str_42:
	.ascii "DS1822"
	.db 0x00
__str_43:
	.ascii "Test UART"
	.db 0x00
__str_44:
	.ascii "RS-232 0&1"
	.db 0x00
__str_45:
	.ascii "RS-485"
	.db 0x00
__str_46:
	.ascii "Open"
	.db 0x00
__str_47:
	.ascii "System"
	.db 0x00
__str_48:
	.ascii "EV8031/AVR"
	.db 0x00
	.area XINIT   (CODE)
