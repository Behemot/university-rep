//#define SAMPLE_QTY 32U
//  xdata unsigned int sample_array[SAMPLE_QTY];

void test_adc()
{
  unsigned char shifted_one, result,sample_index = 0,i;
  unsigned int sample, summ = 0, last, display_counter = 0;
#define SAMPLE_QTY 32U
  xdata unsigned int sample_array[SAMPLE_QTY];
  for (i=0; i<SAMPLE_QTY; i++) sample_array[i] = 0;

  do {
    pDAC = 0; result = 0; shifted_one = _10000000;
    do {
      pDAC = result | shifted_one;
      delay8(10);
      if (P1_7) result |= shifted_one;
      shifted_one = shifted_one >> 1;
    } while (shifted_one);

    sample = result;
    last = sample_array[sample_index];
    sample_array[sample_index]=sample;

    sample_index = ++sample_index % SAMPLE_QTY;


    summ = summ + sample - last;
    
    if (++display_counter == 100) {
      sample = summ / SAMPLE_QTY; display_int(sample,0); display_counter = 0;

//      sample = summ / SAMPLE_QTY; display_int(result,0); display_counter = 0;
    }
    
  } while (key_scan() == 12);

}

