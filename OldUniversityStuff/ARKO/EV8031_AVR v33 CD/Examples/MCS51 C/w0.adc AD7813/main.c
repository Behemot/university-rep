
#include <8051.h>
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\bitdef.h"

int main()
{
  unsigned char sample_index = 0,i;
  unsigned int sample;
  unsigned int summ = 0;
  unsigned int last;
  unsigned int display_counter = 0;

#define SAMPLE_QTY 32U
  xdata unsigned int sample_array[SAMPLE_QTY];

  lefti=0x12;
  righti=0x34;
  new_dotsi=0;

  for (i=0; i<SAMPLE_QTY; i++) sample_array[i] = 0;

#define CONVERSION_START() { P1_1 = 0; P1_1 = 1; }
#define BUSY P3_2

  do {
    CONVERSION_START();

    while (!BUSY);

    sample = pADC << 2; sample |= (pADC >> 6);

    last = sample_array[sample_index];
    sample_array[sample_index]=sample;

    sample_index = ++sample_index % SAMPLE_QTY;

    summ = summ + sample - last;
    
    if (++display_counter == 100) {
      sample = summ / SAMPLE_QTY; display_int(sample,0); display_counter = 0;
    }
    delay8(100);
  }
  while (key_scan() == 12);
}
        