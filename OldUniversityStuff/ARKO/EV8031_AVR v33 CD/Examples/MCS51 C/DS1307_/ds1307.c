#define hour_button P3_3
#define min_button P3_2

typedef struct {
  char sec;
  char min;
  char hour;
} LOG_REC;
LOG_REC time;
unsigned char min_counter, hour_counter;

unsigned char bcd2bin(unsigned char bcd)
{
  return ((bcd>>4)*10 + (bcd & 0x0F));
}

void ds1307_init()
{
  unsigned char s;
  
  i2c_start(); i2c_wb(_11010000); i2c_wb(0);					// dummmy sequence to set reg pointer
  i2c_start(); i2c_wb(_11010001); s=i2c_rb(0) & _01111111; i2c_stop();		// read sec from reg 0
  i2c_start(); i2c_wb(_11010000); i2c_wb(0); i2c_wb(s); i2c_stop();		// write sec value with CH=0 to reg 0
  i2c_start(); i2c_wb(_11010000); i2c_wb(7); i2c_wb(_10010000); i2c_stop();	// Square Wawe Enable, Fsqw=1Hz
}

void get_time()
{
  i2c_start(); i2c_wb(_11010000); i2c_wb(0);	// dummmy sequence to set reg pointer
  i2c_start(); i2c_wb(_11010001);
  time.sec = i2c_rb(1) & _01111111;
  time.min = i2c_rb(1) & _01111111;
  time.hour= i2c_rb(0) & _00111111;
  i2c_stop();

  lefti = time.hour; righti = time.min;

  time.sec = bcd2bin(time.sec);
  time.min = bcd2bin(time.min);
  time.hour = bcd2bin(time.hour);

  if (time.sec & 1) { new_dotsi = _00100000; dotsi = _00100000; }
  else { dotsi=0; new_dotsi=0; }
}

void test_ds1307()
{
  ds1307_init();				// ���樠������ DS1307

  do {
    get_time();				// ����� ���祭�� �६���

    if (hour_button) hour_counter=0;// �஢�ઠ ������ ������ ����ன�� �ᮢ
    else 
    {
      if (!hour_counter)
      {
        if (time.hour < 23) time.hour++;
        else time.hour=0;
        i2c_start(); i2c_wb(_11010000); i2c_wb(2);
        i2c_wb(((time.hour/10)<<4) | (time.hour%10));
        i2c_stop();
      }
      if (++hour_counter > 100) hour_counter = 0;
    }

    if (min_button) min_counter=0;// �஢�ઠ ������ ������ ����ன�� �����
    else
    {
      if (!min_counter)
      {
        if (time.min < 59) time.min++;
        else time.min = 0;
        i2c_start(); i2c_wb(_11010000); i2c_wb(1);
        i2c_wb(((time.min/10)<<4) | (time.min%10));
        i2c_stop();
      }
      if (++min_counter > 100) min_counter = 0;
    }
//    delay16(60000); 
  } while (key_scan() == 12);
}
