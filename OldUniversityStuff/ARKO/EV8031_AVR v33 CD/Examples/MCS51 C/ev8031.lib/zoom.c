unsigned int need_zoom = 0;
void t0_isr () interrupt 2	// interrupt service routine for T0 � 
{ � 
  if (need_zoom) { need_zoom--; P1_6 ^= 1; }
}�

void t0_for_zoom_init()
{
  EA=0;
  TH0 = 0xff-80;
  TMOD = 0x22;	// c/t0-timer, mode 2
  TR0 = 1;
  ET0 = 1;
  EA = 1;
}
