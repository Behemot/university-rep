//////////////////////////////////////////////////////////////////////////////
// OW_RESET - performs a reset on the one-wire bus and returns the presence 
// detect.
unsigned char ow_reset(void)
{
  unsigned char presence;
  DQ = 0;		//pull DQ line low
  delay8(150);		// leave it low for 500us
  DQ = 1;		// then release line high
  delay8(30);		// wait for presence 100us
  presence = DQ;	// get presence signal
  delay8(120);		// wait for end of timeslot 400us
  return(presence);// return presence signal (0=presence, 1 = no part)
}
//////////////////////////////////////////////////////////////////////////////
// ow_rbit - reads a bit from the one-wire bus.
unsigned char ow_rbit(void)
{
  unsigned char value;
  DQ = 0;		// pull DQ low to start timeslot
  DQ = 1;		// then return high
  delay8(2);		// delay 10us from start of timeslot
  value = DQ;		// and read bit
  delay8(20);		// wait for end of timeslot
  return(value);// return value of DQ line
}
//////////////////////////////////////////////////////////////////////////////
// ow_wbit - writes a bit to the one-wire bus.
void ow_wbit(char bitval)
{
  DQ = 0;		// pull DQ low to start timeslot
  if(bitval) DQ =1;	// return DQ high if write 1
  delay8(20);		// hold value till end of timeslot
  DQ = 1;
  delay8(2);
}
//////////////////////////////////////////////////////////////////////////////
// ow_wb - reads a byte from the one-wire bus.
unsigned char ow_rb(void)
{
  unsigned char i,value = 0;
  for (i=0;i<8;i++) {
    value >>=1;
    if (ow_rbit()) value |= _10000000;
  }
  return(value);
}
//////////////////////////////////////////////////////////////////////////////
// ow_wb - writes a byte to the one-wire bus.
void ow_wb(char val)
{
  unsigned char shifted_one=1;
  do {
    ow_wbit(val & shifted_one);
    shifted_one <<= 1;
  } while (shifted_one);
}

// for 7,37MHz argument of of delay8() function is:
// 500us	150
// 100us	30
// 400us	120
// 70us		20
// 10us		2
// 13us		3

