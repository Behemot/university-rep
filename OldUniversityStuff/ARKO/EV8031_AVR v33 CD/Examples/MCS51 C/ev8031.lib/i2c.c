void i2c_start()
{
  SDA=1; SCL=1;	I2CDELAY();
  SDA=0;	I2CDELAY(); 
  SCL=0;	I2CDELAY();  
}

void i2c_stop()
{
  SDA=0;I2CDELAY(); SCL=1; I2CDELAY();
  SDA=1;	I2CDELAY();
  //SCL=0;	I2CDELAY();  
}

unsigned char i2c_wb(unsigned char i2c_data)
{
  unsigned char i=8,acknowledge=0;
  do {
    SDA = i2c_data & 0x80;			// Data OUT
    i2c_data <<= 1;
    I2CDELAY();
    SCL = 1; I2CDELAY(); SCL = 0; I2CDELAY();	// Clock
  }
  while (--i);
  SDA = 1; I2CDELAY(); SCL = 1; I2CDELAY();	// extra Clock for acknowledge
  if (!SDA) acknowledge=1;
  SCL = 0; I2CDELAY();
  return acknowledge;
}

unsigned char i2c_rb(unsigned char acknowledge)
{
  unsigned char i=8,i2c_data=0;
  SDA=1;					// pull up SDA-line
  do {
    i2c_data<<=1;
    SCL = 1; I2CDELAY();
    i2c_data|=SDA;				// Data IN
    SCL = 0; I2CDELAY();
  }
  while (--i);
  if (acknowledge) SDA=0;
  SCL = 1; I2CDELAY(); SCL = 0;	SDA = 1;I2CDELAY();	// extra Clock for acknowledge
  return i2c_data;
}

