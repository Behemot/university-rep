
#include <8051.h>
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\bitdef.h"
#include "e40281.c"

int main()
{
q:
  lefti=0x12;
  righti=0x34;
  new_dotsi=0;
  if (key_scan() < 12) { delay16(20000); test_e40281(); delay16(20000); }
  goto q;
}
        