////////////////////////////////////////////////////////////////////////////////
static void set_accurasy()
{
  ow_reset();
  ow_wb(0xCC);		// skip ROM
  ow_wb(0x4E);		// write scratchpad
  ow_wb(0x00);		// Th
  ow_wb(0x00);		// Tl
//  ow_wb(0x3F);		// configuration (10 bit accurasy, 0.25 degC)
  ow_wb(0x1F);		// configuration (9 bit accurasy, 0.5 degC)
}
////////////////////////////////////////////////////////////////////////////////
// Get temperature from DS1822
static signed char get_T()
{
  unsigned char i;
  unsigned char scratchpad [2];
  signed char temperature = -128;

  if(!ow_reset())
  {
    ow_wb(0xCC);		// skip ROM
    ow_wb(0x44);		//convert T

    while (!ow_rbit());

    ow_reset();
    ow_wb(0xCC);		// skip ROM
    ow_wb(0xBE);		// read scratchpad
    for (i=0; i<2; i++)
    {
      scratchpad[i]=ow_rb();
    }
    temperature = *((signed int*)scratchpad) >> 4;
  }
  return (temperature);
}
static void test_ds1822()
{
  unsigned char sign;
  signed char temperature;

  do {
    if (!ow_reset()) {
      set_accurasy();
      temperature = get_T();
      if (temperature < 0) { temperature = - temperature; sign = _00010000; }
      else sign = 0;
      display_int(temperature,sign);
    }
    else new_dotsi = _11111111;
  } while (key_scan() == 12);
}
