void test_i()
{
  unsigned char index,temp;

start:
  temp=0;
  new_dotsi=0;
  dotsi=0;

  for (index=0; index<16; index++)
  {
    if (key_scan() < 12) break;
    lefti = temp;
    righti= temp;
    temp = temp + 0x11;
    delay16(20000);
  }
  
  lefti=0xff;
  righti=0xff;
  temp=0x10;
  for(index=0; index<4; index++)
  {
    if (key_scan() < 12) break;
    dotsi = temp;
    new_dotsi = temp;
    lefti=0xff;
    righti=0xff;
    temp = temp << 1;
    delay16(20000);
  }
  if (key_scan() == 12) goto start;
}
