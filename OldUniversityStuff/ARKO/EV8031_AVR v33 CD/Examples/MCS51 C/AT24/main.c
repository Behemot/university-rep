#include <8051.h>
#include "..\ev8031.lib\bitdef.h"
#include "..\ev8031.lib\ev8031.c"
#include "..\ev8031.lib\i2c.c"

#include "at24.c"

int main()
{
  lights_off();
q:
  lefti=0x12;
  righti=0x34;
  new_dotsi=0;
  if (key_scan() < 12) { delay16(20000); test_at24(); delay16(20000); }
  goto q;
}
        