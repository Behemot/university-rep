;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Thu Feb 19 17:11:14 2004

;--------------------------------------------------------
	.module main
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _set_cursor_PARM_2
	.globl _recode_table
	.globl _main
	.globl _i2c_rb
	.globl _i2c_wb
	.globl _i2c_stop
	.globl _i2c_start
	.globl _string_to_hd
	.globl _set_cursor
	.globl _hd_init
	.globl _hd_turn_on
	.globl _display_int
	.globl _lights_off
	.globl _kb_scan
	.globl _key_scan
	.globl _delay16
	.globl _UC_REG
	.globl _DISPLAYB
	.globl _SYS_CTL
	.globl _EDC_REG
	.globl _DC_REG
	.globl _DISPLAY
	.globl _pADC
	.globl _pDAC
	.globl _LCD_DATA
	.globl _LCD_CMD
	.globl _PC_REG
	.globl _PB_REG
	.globl _PA_REG
	.globl _pC
	.globl _pB
	.globl _pA
	.globl _vv55RUS
	.globl _LED_REG
	.globl _new_dotsi
	.globl _dotsi
	.globl _righti
	.globl _lefti
	.globl _string_to_hd_PARM_3
	.globl _string_to_hd_PARM_2
	.globl _display_int_PARM_2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_kb_scan_prev_scan_1_1:
	.ds 1
_kb_scan_timer_1_1:
	.ds 1
_display_int_buffer_1_1:
	.ds 4
_display_int_PARM_2::
	.ds 1
_string_to_hd_PARM_2::
	.ds 1
_string_to_hd_PARM_3::
	.ds 3
_test_at24_i_1_1::
	.ds 1
_test_at24_result_1_1::
	.ds 1
_test_at24_testbyte_1_1::
	.ds 1
_test_at24_volume_1_1::
	.ds 1
_test_at24_shifted_one_1_1::
	.ds 2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
_set_cursor_PARM_2::
	.ds 1
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_lefti	=	0xa000
_righti	=	0xb000
_dotsi	=	0xc000
_new_dotsi	=	0xa004
_LED_REG	=	0xa006
_vv55RUS	=	0x8003
_pA	=	0x8000
_pB	=	0x8001
_pC	=	0x8002
_PA_REG	=	0x8000
_PB_REG	=	0x8001
_PC_REG	=	0x8002
_LCD_CMD	=	0x8004
_LCD_DATA	=	0x8005
_pDAC	=	0xf000
_pADC	=	0xe000
_DISPLAY	=	0xa000
_DC_REG	=	0xa004
_EDC_REG	=	0xa005
_SYS_CTL	=	0xa007
_DISPLAYB	=	0xb000
_UC_REG	=	0xc000
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
;	main.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_vv55RUS
	mov	a,#0x80
	movx	@dptr,a
;	main.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_pA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_pB
	mov	a,#0xFF
	movx	@dptr,a
;	main.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	main.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAssign
	mov	dptr,#_UC_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'delay16'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	-----------------------------------------
;	 function delay16
;	-----------------------------------------
_delay16:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	genAssign
00101$:
;	genDjnz
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00105$
	dec	r3
00105$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00101$
00106$:
00102$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'key_scan'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:49: unsigned char key_scan()
;	-----------------------------------------
;	 function key_scan
;	-----------------------------------------
_key_scan:
;	..\ev8031.lib\ev8031.c:72: _endasm;
;	genInline
;
	        mov r0, #0
	        mov dptr, #0x9000 +0xFE
  next_key_line:
	        ; check scan line
	        movx a, @dptr
	        jnb acc.0, end_key_scan
	        inc r0
	        jnb acc.1, end_key_scan
	        inc r0
	        jnb acc.2, end_key_scan
	        inc r0
	        jnb acc.3, end_key_scan
	        inc r0
	        ; goto next scan line
	        mov a, dpl
	        rl a
	        mov dpl, a
	        cjne a, #0xF7, next_key_line
  end_key_scan:
	        mov dpl, r0
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'kb_scan'
;------------------------------------------------------------
;prev_scan                 Allocated to in memory with name '_kb_scan_prev_scan_1_1'
;timer                     Allocated to in memory with name '_kb_scan_timer_1_1'
;scan                      Allocated to registers r2 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:81: char kb_scan()
;	-----------------------------------------
;	 function kb_scan
;	-----------------------------------------
_kb_scan:
;	..\ev8031.lib\ev8031.c:87: scan = key_scan();
;	genCall
	lcall	_key_scan
	mov	a,dpl
;	genAssign
;	..\ev8031.lib\ev8031.c:88: if (scan == prev_scan) {
;	genCmpEq
; Peephole 105   removed redundant mov
	mov  r2,a
	cjne	a,_kb_scan_prev_scan_1_1,00110$
	sjmp	00111$
00110$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00111$:
;	..\ev8031.lib\ev8031.c:89: timer++;
;	genPlus
;	genPlusIncr
	inc	_kb_scan_timer_1_1
;	..\ev8031.lib\ev8031.c:91: if (timer == 0) return (scan);
;	genCmpEq
	mov	a,_kb_scan_timer_1_1
; Peephole 162   removed sjmp by inverse jump logic
	jz   00113$
00112$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00113$:
;	genRet
	mov	dpl,r2
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00104$:
;	..\ev8031.lib\ev8031.c:93: timer = 0;
;	genAssign
	mov	_kb_scan_timer_1_1,#0x00
00105$:
;	..\ev8031.lib\ev8031.c:95: prev_scan = scan;
;	genAssign
	mov	_kb_scan_prev_scan_1_1,r2
;	..\ev8031.lib\ev8031.c:97: return 12;
;	genRet
	mov	dpl,#0x0C
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lights_off'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:99: void lights_off()
;	-----------------------------------------
;	 function lights_off
;	-----------------------------------------
_lights_off:
;	..\ev8031.lib\ev8031.c:101: LED_REG=0;
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
;	..\ev8031.lib\ev8031.c:102: pA=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
;	..\ev8031.lib\ev8031.c:103: pB=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219a removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_pA
	movx  @dptr,a
	mov   dptr,#_pB
	movx  @dptr,a
;	..\ev8031.lib\ev8031.c:104: pC=0xFF;
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:105: new_dotsi=0x0f;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x0F
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display_int'
;------------------------------------------------------------
;buffer                    Allocated to in memory with name '_display_int_buffer_1_1'
;dc_mask                   Allocated to in memory with name '_display_int_PARM_2'
;value                     Allocated to registers r2 r3 
;ptr                       Allocated to registers r4 
;i                         Allocated to registers 
;dc                        Allocated to registers r5 
;c                         Allocated to registers r6 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:107: void display_int(unsigned int value, unsigned char dc_mask)
;	-----------------------------------------
;	 function display_int
;	-----------------------------------------
_display_int:
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	..\ev8031.lib\ev8031.c:110: unsigned char data *ptr = buffer;
;	genAssign
	mov	r4,#_display_int_buffer_1_1
;	..\ev8031.lib\ev8031.c:111: unsigned char i, dc = 0;
;	genAssign
	mov	r5,#0x00
;	..\ev8031.lib\ev8031.c:113: i = 4; do {
;	genAssign
	mov	ar0,r4
;	genAssign
	mov	r4,#0x04
00101$:
;	..\ev8031.lib\ev8031.c:115: c = value % 10;
;	genAssign
	clr	a
	mov	(__moduint_PARM_2 + 1),a
	mov	__moduint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar0
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	pop	ar0
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
;	..\ev8031.lib\ev8031.c:116: value /= 10;
;	genAssign
	clr	a
	mov	(__divuint_PARM_2 + 1),a
	mov	__divuint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar0
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
	pop	ar0
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	r2,a
	mov	r3,b
;	..\ev8031.lib\ev8031.c:117: *ptr++ = c;
;	genPointerSet
;	genNearPointerSet
	mov	@r0,ar6
	inc	r0
;	..\ev8031.lib\ev8031.c:118: } while (--i);
;	genDjnz
	djnz	r4,00116$
	sjmp	00117$
00116$:
	ljmp	00101$
00117$:
;	..\ev8031.lib\ev8031.c:120: i = 3; do {
;	genAssign
	mov	r2,#0x03
;	genAssign
00107$:
;	..\ev8031.lib\ev8031.c:121: --ptr;
;	genMinus
;	genMinusDec
	dec	r0
;	..\ev8031.lib\ev8031.c:122: if (!*ptr) dc = (dc << 1) | 1;
;	genPointerGet
;	genNearPointerGet
	mov	ar3,@r0
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00109$
00118$:
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r5
	add	a,acc
	mov	r3,a
;	genOr
	mov	a,#0x01
	orl	a,r3
	mov	r5,a
;	..\ev8031.lib\ev8031.c:124: } while (--i);
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00107$
00119$:
00120$:
00109$:
;	..\ev8031.lib\ev8031.c:125: DC_REG = dc | dc_mask;
;	genOr
	mov	dptr,#_DC_REG
	mov	a,_display_int_PARM_2
	orl	a,r5
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:127: DISPLAY[0] = buffer[3] << 4 | buffer[2];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0003 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,0x0002 + _display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#_DISPLAY
	mov	a,r2
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:128: DISPLAY[1] = buffer[1] << 4 | buffer[0];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0001 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,_display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#(_DISPLAY + 0x0001)
	mov	a,r2
	movx	@dptr,a
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_turn_on'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:131: void hd_turn_on()
;	-----------------------------------------
;	 function hd_turn_on
;	-----------------------------------------
_hd_turn_on:
;	..\ev8031.lib\ev8031.c:133: delay16(30000); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:134: delay16(450); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x01)<<8) + 0xC2)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:135: delay16(150); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:137: delay16(150); LCD_CMD = _01001000;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x48
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:139: delay16(150); LCD_DATA= _00001100;	//
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:140: delay16(150); LCD_DATA= _00010010;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x12
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:141: delay16(150); LCD_DATA= _00001100;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:142: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:143: delay16(150); LCD_DATA= _00000000;	// ᨬ��� �ࠤ�� \x04
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:144: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:145: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:146: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:149: void hd_init()
;	-----------------------------------------
;	 function hd_init
;	-----------------------------------------
_hd_init:
;	..\ev8031.lib\ev8031.c:151: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:152: delay16(150); LCD_CMD = _00001100;	// display on, cursor off, blinc char at cursor pos
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:153: delay16(150); LCD_CMD = 0x06;		// increment, no shift
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x06
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:154: delay16(150); LCD_CMD = 1;		// clear display & cursor to home
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x01
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:155: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_cursor'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:157: void set_cursor(unsigned char x, unsigned char y)
;	-----------------------------------------
;	 function set_cursor
;	-----------------------------------------
_set_cursor:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\ev8031.c:159: switch (y) {
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,_set_cursor_PARM_2
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00105$
00109$:
;	genJumpTab
	mov	a,_set_cursor_PARM_2
	add	a,acc
	add	a,_set_cursor_PARM_2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	..\ev8031.lib\ev8031.c:160: case 0: y=0;    break;
00101$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x00
;	..\ev8031.lib\ev8031.c:161: case 1: y=0x40; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00102$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x40
;	..\ev8031.lib\ev8031.c:162: case 2: y=0x0A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00103$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x0A
;	..\ev8031.lib\ev8031.c:163: case 3: y=0x4A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00104$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x4A
;	..\ev8031.lib\ev8031.c:164: }
00105$:
;	..\ev8031.lib\ev8031.c:165: y=y+x;
;	genPlus
	mov	a,ar2
	add	a,_set_cursor_PARM_2
;	..\ev8031.lib\ev8031.c:166: LCD_CMD = (_10000000 | y);		// set start position
;	genOr
	mov	dptr,#_LCD_CMD
	orl	a,#0x80
	movx	@dptr,a
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'string_to_hd'
;------------------------------------------------------------
;y                         Allocated to in memory with name '_string_to_hd_PARM_2'
;string                    Allocated to in memory with name '_string_to_hd_PARM_3'
;x                         Allocated to registers 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:181: void string_to_hd(unsigned char x, unsigned char y, char *string)
;	-----------------------------------------
;	 function string_to_hd
;	-----------------------------------------
_string_to_hd:
;	genReceive
;	..\ev8031.lib\ev8031.c:183: set_cursor(x,y);
;	genAssign
	mov	_set_cursor_PARM_2,_string_to_hd_PARM_2
;	genCall
	lcall	_set_cursor
;	..\ev8031.lib\ev8031.c:184: while((y = *string++)) {
;	genAssign
	mov	r2,_string_to_hd_PARM_3
	mov	r3,(_string_to_hd_PARM_3 + 1)
	mov	r4,(_string_to_hd_PARM_3 + 2)
00103$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r5,a
	inc	dptr
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
;	genAssign
	mov	ar6,r5
;	genAssign
	mov	_string_to_hd_PARM_2,r6
;	genIfx
	mov	a,r5
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00106$
00111$:
;	..\ev8031.lib\ev8031.c:185: if (y > 0x7f) y=recode_table[y-0x80];
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x7F
	subb	a,r6
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00102$
00112$:
;	genMinus
	mov	a,r6
	add	a,#0x80
;	genPlus
	add	a,#_recode_table
	mov	dpl,a
	mov	a,#(_recode_table >> 8)
	addc	a,#0x00
	mov	dph,a
;	genPointerGet
;	genCodePointerGet
	clr	a
	movc	a,@a+dptr
	mov	_string_to_hd_PARM_2,a
00102$:
;	..\ev8031.lib\ev8031.c:186: LCD_DATA = y;
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,_string_to_hd_PARM_2
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_start'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function i2c_start
;	-----------------------------------------
_i2c_start:
;	..\ev8031.lib\i2c.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	setb	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	..\ev8031.lib\i2c.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	clr	_P1_0
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_stop'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function i2c_stop
;	-----------------------------------------
_i2c_stop:
;	..\ev8031.lib\i2c.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genAssign
	clr	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00101$
00111$:
00112$:
;	..\ev8031.lib\i2c.c:11: xdata at 0x8001 unsigned char PB_REG;
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00102$
00113$:
00114$:
;	..\ev8031.lib\i2c.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00103$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00103$
00115$:
00116$:
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_wb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	-----------------------------------------
;	 function i2c_wb
;	-----------------------------------------
_i2c_wb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:17: xdata at 0xf000 unsigned char pDAC;
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	r4,#0x08
00103$:
;	..\ev8031.lib\i2c.c:19: 
;	genAnd
	mov	a,r2
	mov	c,acc.7
	mov	_P1_0,c
;	..\ev8031.lib\i2c.c:20: xdata at 0xA000 unsigned char DISPLAY[4];
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r2
	add	a,acc
	mov	r2,a
;	..\ev8031.lib\i2c.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00123$:
00124$:
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00125$:
00126$:
;	..\ev8031.lib\i2c.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00103$
00127$:
00128$:
;	..\ev8031.lib\i2c.c:24: 
;	genAssign
	setb	_P1_0
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00106$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00106$
00129$:
00130$:
;	..\ev8031.lib\i2c.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genIfx
;	genIfxJump
; Peephole 112   removed ljmp by inverse jump logic
	jb   _P1_0,00108$
00131$:
;	genAssign
	mov	r3,#0x01
00108$:
;	..\ev8031.lib\i2c.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r2,#0x03
00109$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00109$
00132$:
00133$:
;	..\ev8031.lib\i2c.c:27: 
;	genRet
	mov	dpl,r3
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'i2c_rb'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\i2c.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	-----------------------------------------
;	 function i2c_rb
;	-----------------------------------------
_i2c_rb:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\i2c.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genAssign
	mov	r3,#0x00
;	..\ev8031.lib\i2c.c:33: 
;	genAssign
	setb	_P1_0
;	..\ev8031.lib\i2c.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genAssign
	mov	r4,#0x08
00103$:
;	..\ev8031.lib\i2c.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r3
	add	a,acc
	mov	r3,a
;	..\ev8031.lib\i2c.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r5,#0x03
00101$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00101$
00123$:
00124$:
;	..\ev8031.lib\i2c.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
	clr	a
	mov	c,_P1_0
	rlc	a
;	genOr
; Peephole 105   removed redundant mov
	mov  r5,a
	orl	ar3,a
;	..\ev8031.lib\i2c.c:38: 
;	genAssign
	clr	_P1_1
;	genAssign
	mov	r5,#0x03
00102$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r5,00102$
00125$:
00126$:
;	..\ev8031.lib\i2c.c:40: #define DQ P1_2
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r4,00103$
00127$:
00128$:
;	..\ev8031.lib\i2c.c:41: // One Wire
;	genIfx
	mov	a,r2
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00107$
00129$:
;	genAssign
	clr	_P1_0
00107$:
;	..\ev8031.lib\i2c.c:42: 
;	genAssign
	setb	_P1_1
;	genAssign
	mov	r2,#0x03
00108$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00108$
00130$:
00131$:
;	genAssign
	clr	_P1_1
;	genAssign
	setb	_P1_0
;	genAssign
	mov	r2,#0x03
00109$:
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00109$
00132$:
00133$:
;	..\ev8031.lib\i2c.c:43: // I2C
;	genRet
	mov	dpl,r3
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'read_at24_group'
;------------------------------------------------------------
;------------------------------------------------------------
;	at24.c:1: xdata at 0xA000 unsigned char lefti; // left part of indication
;	-----------------------------------------
;	 function read_at24_group
;	-----------------------------------------
_read_at24_group:
;	at24.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genCall
	lcall	_i2c_start
;	genCall
	mov	dpl,#0xA0
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	genCall
	mov	dpl,#0x00
	lcall	_i2c_wb
;	genCall
	lcall	_i2c_stop
;	at24.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genCall
	lcall	_i2c_start
;	at24.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genCall
	mov	dpl,#0xA0
	lcall	_i2c_wb
;	genRet
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_at24'
;------------------------------------------------------------
;acknowledge               Allocated to registers r4 
;i                         Allocated to in memory with name '_test_at24_i_1_1'
;result                    Allocated to in memory with name '_test_at24_result_1_1'
;testbyte                  Allocated to in memory with name '_test_at24_testbyte_1_1'
;volume                    Allocated to in memory with name '_test_at24_volume_1_1'
;adress                    Allocated to registers r2 r3 
;shifted_one               Allocated to in memory with name '_test_at24_shifted_one_1_1'
;------------------------------------------------------------
;	at24.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function test_at24
;	-----------------------------------------
_test_at24:
;	at24.c:13: 
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	at24.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	r2,#0x3F
;	at24.c:16: 
;	genAssign
; Peephole 3.b   changed mov to clr
	clr  a
	mov  r3,a
	mov	(_test_at24_shifted_one_1_1 + 1),a
	mov	_test_at24_shifted_one_1_1,#0x40
;	at24.c:17: xdata at 0xf000 unsigned char pDAC;
;	genCall
	push	ar2
	push	ar3
	lcall	_read_at24_group
	mov	a,dpl
	pop	ar3
	pop	ar2
;	genIfx
;	genIfxJump
	jz	00137$
	ljmp	00135$
00137$:
;	at24.c:18: xdata at 0xe000 unsigned char pADC;
;	genAssign
	mov	_test_at24_volume_1_1,#0x00
00107$:
;	at24.c:19: 
;	genOr
	mov	a,_test_at24_shifted_one_1_1
	orl	ar2,a
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	orl	ar3,a
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	xch	a,_test_at24_shifted_one_1_1
	add	a,acc
	xch	a,_test_at24_shifted_one_1_1
	rlc	a
	mov	(_test_at24_shifted_one_1_1 + 1),a
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar7,r3
	mov	r0,#0x00
;	genCast
	mov	ar1,r2
;	genCast
	mov	ar6,r1
	mov	a,r1
	rlc	a
	subb	a,acc
	mov	r4,a
;	genXor
	mov	a,r6
	xrl	ar7,a
	mov	a,r4
	xrl	ar0,a
;	genCast
	mov	_test_at24_testbyte_1_1,r7
;	at24.c:21: xdata at 0xA004 unsigned char DC_REG;
;	genCall
	push	ar2
	push	ar3
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar3
	pop	ar2
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar5,r2
	mov	a,r3
	mov	c,acc.7
	xch	a,r5
	rlc	a
	xch	a,r5
	rlc	a
	xch	a,r5
	anl	a,#0x01
	mov	r6,a
;	genAnd
	mov	a,#0x0E
	anl	a,r5
	mov	r7,a
	mov	r0,#0x00
;	genOr
	orl	ar7,#0xA0
;	genCast
	mov	dpl,r7
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	at24.c:22: xdata at 0xA005 unsigned char EDC_REG;
;	genCall
	mov	dpl,_test_at24_testbyte_1_1
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	at24.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_stop
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	at24.c:24: 
;	genAssign
	setb	_P1_2
;	at24.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAnd
	mov	a,#0x0E
	anl	a,r5
	mov	r7,a
	mov	r0,#0x00
;	genOr
	orl	ar7,#0xA0
;	genAssign
	mov	_test_at24_i_1_1,#0x00
00102$:
;	at24.c:27: 
;	genPlus
;	genPlusIncr
	inc	_test_at24_i_1_1
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genCast
	mov	dpl,r7
;	genCall
	push	ar2
	push	ar3
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_wb
	mov	a,dpl
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar3
	pop	ar2
;	genAssign
;	at24.c:28: #define KB_BASE 0x9000
;	genIfx
; Peephole 105   removed redundant mov
	mov  r4,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00104$
00138$:
;	genIfx
	mov	a,_test_at24_i_1_1
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00102$
00139$:
00104$:
;	at24.c:29: 
;	genAssign
	clr	_P1_2
;	at24.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_i2c_start
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAnd
	anl	ar5,#0x0E
	mov	r6,#0x00
;	genOr
	orl	ar5,#0xA1
;	genCast
	mov	dpl,r5
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_wb
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	_test_at24_result_1_1,a
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_i2c_stop
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genCmpEq
	mov	a,_test_at24_testbyte_1_1
	cjne	a,_test_at24_result_1_1,00140$
	sjmp	00141$
00140$:
	ljmp	00121$
00141$:
;	at24.c:33: 
;	genPlus
;	genPlusIncr
	inc	_test_at24_volume_1_1
;	at24.c:34: #define delay8(n) { unsigned char i=n; do { } while(--i); }
;	genAssign
	mov	dptr,#_righti
	mov	a,_test_at24_i_1_1
	movx	@dptr,a
;	at24.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	genAssign
	mov	dptr,#_lefti
	mov	a,_test_at24_volume_1_1
	movx	@dptr,a
;	at24.c:36: //void delay8(unsigned char i) { do { } while(--i); }
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	push	ar2
	push	ar3
	push	ar4
	lcall	_delay16
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genCmpEq
	cjne	r2,#0xFF,00142$
	cjne	r3,#0x07,00142$
	sjmp	00143$
00142$:
	ljmp	00107$
00143$:
	ljmp	00121$
;	at24.c:40: #define DQ P1_2
00135$:
;	genAssign
	mov	_test_at24_volume_1_1,#0x00
00116$:
;	at24.c:41: // One Wire
;	genOr
	mov	a,_test_at24_shifted_one_1_1
	orl	ar2,a
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	orl	ar3,a
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,(_test_at24_shifted_one_1_1 + 1)
	xch	a,_test_at24_shifted_one_1_1
	add	a,acc
	xch	a,_test_at24_shifted_one_1_1
	rlc	a
	mov	(_test_at24_shifted_one_1_1 + 1),a
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar7,r3
	mov	r0,#0x00
;	genCast
	mov	ar1,r2
;	genCast
	mov	ar6,r1
	mov	a,r1
	rlc	a
	subb	a,acc
	mov	r5,a
;	genXor
	mov	a,r7
	xrl	ar6,a
	mov	a,r0
	xrl	ar5,a
;	genCast
	mov	_test_at24_testbyte_1_1,r6
;	at24.c:43: // I2C
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA0
	push	ar2
	push	ar3
	push	ar4
	push	ar7
	push	ar0
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
	mov	ar5,r7
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:44: #define SCL P1_1
;	genCall
	mov	dpl,_test_at24_testbyte_1_1
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:45: #define SDA P1_0
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar1
	lcall	_i2c_stop
	pop	ar1
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	at24.c:46: #define I2CDELAY() delay8(3)
;	genAssign
	setb	_P1_2
;	at24.c:48: 
;	genAssign
	mov	r6,#0x00
00111$:
;	at24.c:49: unsigned char key_scan()
;	genPlus
;	genPlusIncr
	inc	r6
;	at24.c:50: {
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_start
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA0
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar1
	lcall	_i2c_wb
	mov	a,dpl
	pop	ar1
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
;	at24.c:51: _asm;
;	genIfx
; Peephole 105   removed redundant mov
	mov  r4,a
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00113$
00144$:
;	genIfx
	mov	a,r6
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00111$
00145$:
00113$:
;	at24.c:52: mov	r0, #0
;	genAssign
	clr	_P1_2
;	at24.c:53: mov	dptr, #KB_BASE+0xFE
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar6
	push	ar1
	lcall	_i2c_wb
	pop	ar1
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,r1
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar3
	pop	ar2
;	at24.c:54: next_key_line:
;	genCall
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_start
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0xA1
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_wb
	pop	ar6
	pop	ar3
	pop	ar2
;	genCall
	mov	dpl,#0x00
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_rb
	mov	a,dpl
	pop	ar6
	pop	ar3
	pop	ar2
;	genAssign
	mov	_test_at24_result_1_1,a
;	genCall
	push	ar2
	push	ar3
	push	ar6
	lcall	_i2c_stop
	pop	ar6
	pop	ar3
	pop	ar2
;	at24.c:55: ; check scan line
;	genCmpEq
	mov	a,_test_at24_testbyte_1_1
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne a,_test_at24_result_1_1,00121$
;00146$:
; Peephole 200   removed redundant sjmp
00147$:
;	at24.c:56: movx	a, @dptr
;	genPlus
;	genPlusIncr
	inc	_test_at24_volume_1_1
;	at24.c:57: jnb	acc.0, end_key_scan
;	genAssign
	mov	dptr,#_righti
	mov	a,r6
	movx	@dptr,a
;	at24.c:58: inc	r0
;	genAssign
	mov	dptr,#_lefti
	mov	a,_test_at24_volume_1_1
	movx	@dptr,a
;	at24.c:59: jnb	acc.1, end_key_scan
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	push	ar2
	push	ar3
	lcall	_delay16
	pop	ar3
	pop	ar2
;	at24.c:60: inc	r0
;	genCmpEq
	cjne	r2,#0xFF,00148$
	cjne	r3,#0xFF,00148$
	sjmp	00149$
00148$:
	ljmp	00116$
00149$:
00121$:
;	at24.c:63: jnb	acc.3, end_key_scan
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	ar4,r2
	mov	a,r3
	clr	c
	rrc	a
	xch	a,r4
	rrc	a
	xch	a,r4
	mov	r5,a
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,ar4
	mov	r2,a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,ar5
	mov	r3,a
;	at24.c:64: inc	r0
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	dpl,r2
	mov	a,r3
	mov	c,acc.7
	xch	a,dpl
	rlc	a
	xch	a,dpl
	rlc	a
	xch	a,dpl
	anl	a,#0x01
	mov	dph,a
;	genAssign
	mov	_display_int_PARM_2,#0x00
;	genCall
	lcall	_display_int
;	at24.c:65: ; goto next scan line
;	genAssign
	mov	dptr,#_LED_REG
	mov	a,#0x01
	movx	@dptr,a
;	at24.c:66: mov	a, dpl
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x03)<<8) + 0xE8)
	lcall	_delay16
;	at24.c:67: rl	a
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	at24.c:69: cjne	a, #0xF7, next_key_line
00122$:
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00150$
; Peephole 132   changed ljmp to sjmp
	sjmp 00122$
00150$:
00125$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:10: xdata at 0x8000 unsigned char PA_REG;
;	genCall
	lcall	_lights_off
;	main.c:11: xdata at 0x8001 unsigned char PB_REG;
00101$:
;	main.c:12: xdata at 0x8002 unsigned char PC_REG;
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x12
	movx	@dptr,a
;	main.c:13: 
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x34
	movx	@dptr,a
;	main.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00108$
00108$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00101$
00109$:
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	genCall
	lcall	_test_at24
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	main.c:16: 
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
	.area CSEG    (CODE)
_recode_table:
	.db #0x41
	.db #0xA0
	.db #0x42
	.db #0xA1
	.db #0xE0
	.db #0x45
	.db #0xA3
	.db #0xA4
	.db #0xA5
	.db #0xA6
	.db #0x4B
	.db #0xA7
	.db #0x4D
	.db #0x48
	.db #0x4F
	.db #0xA8
	.db #0x50
	.db #0x43
	.db #0x54
	.db #0xA9
	.db #0xAA
	.db #0x58
	.db #0xE1
	.db #0xAB
	.db #0xAC
	.db #0xE2
	.db #0xAD
	.db #0xAE
	.db #0x62
	.db #0xAF
	.db #0xB0
	.db #0xB1
	.db #0x61
	.db #0xB2
	.db #0xB3
	.db #0xB4
	.db #0xE3
	.db #0x65
	.db #0xB6
	.db #0xB7
	.db #0xB8
	.db #0xB9
	.db #0xBA
	.db #0xBB
	.db #0xBC
	.db #0xBD
	.db #0x6F
	.db #0xBE
	.db #0xB0
	.db #0xB1
	.db #0xB2
	.db #0xB3
	.db #0xB4
	.db #0xB5
	.db #0xB6
	.db #0xB7
	.db #0xB8
	.db #0xB9
	.db #0xBA
	.db #0xBB
	.db #0xBC
	.db #0xBD
	.db #0xBE
	.db #0xBF
	.db #0xC0
	.db #0xC1
	.db #0xC2
	.db #0xC3
	.db #0xC4
	.db #0xC5
	.db #0xC6
	.db #0xC7
	.db #0xC8
	.db #0xC9
	.db #0xCA
	.db #0xCB
	.db #0xCC
	.db #0xCD
	.db #0xCE
	.db #0xCF
	.db #0xD0
	.db #0xD1
	.db #0xD2
	.db #0xD3
	.db #0xD4
	.db #0xD5
	.db #0xD6
	.db #0xD7
	.db #0xD8
	.db #0xD9
	.db #0xDA
	.db #0xDB
	.db #0xDC
	.db #0xDD
	.db #0xDE
	.db #0xDF
	.db #0x70
	.db #0x63
	.db #0xBF
	.db #0x79
	.db #0xE4
	.db #0x78
	.db #0xE5
	.db #0xC0
	.db #0xC1
	.db #0xE6
	.db #0xC2
	.db #0xC3
	.db #0xC4
	.db #0xC5
	.db #0xC6
	.db #0xC7
	.db #0xF0
	.db #0xF1
	.db #0xF2
	.db #0xF3
	.db #0xF4
	.db #0xF5
	.db #0xF6
	.db #0xF7
	.db #0xF8
	.db #0xF9
	.db #0xFA
	.db #0xFB
	.db #0xFC
	.db #0xFD
	.db #0xFE
	.db #0xFF
	.area XINIT   (CODE)
