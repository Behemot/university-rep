static unsigned char read_at24_group()
{
  i2c_start(); i2c_wb(_10100000); i2c_wb(0); i2c_wb(0); i2c_stop();
  i2c_start();
  return (i2c_wb(_10100000));// return acknowledge (NO acknowledge if at2401/02/04/08/16 )
}

static void test_at24()
{
  unsigned char acknowledge, i, result, testbyte, volume;
  unsigned int adress,shifted_one;
  
  new_dotsi=0;
  volume=0;
  adress      = 0x003F;
  shifted_one = 0x0040;
  if (!read_at24_group()) {		// for at2401/02/04/08/16 (one byte for word adress)
    do {
      adress |= shifted_one; shifted_one = shifted_one << 1; testbyte = (adress >> 8) ^ (char)adress;

      i2c_start(); i2c_wb(_10100000 | ((adress >> 7) & _00001110)); i2c_wb((char)adress);
      i2c_wb(testbyte);
      i2c_stop();
      P1_2=1;
      i=0;
      do { 
        i++; i2c_start(); acknowledge = i2c_wb(_10100000 | ((adress >> 7) & _00001110));
      } while (!acknowledge && i);
      P1_2=0;
      i2c_wb((char)adress);
      i2c_start(); i2c_wb(_10100001 | ((adress >> 7) & _00001110)); result = i2c_rb(0); i2c_stop();
      if (testbyte != result) break;
      volume++;
      righti = i;
      lefti  = volume;
      delay16(30000);
    } while (adress != 0x7ff);
  }
  else {				// for at2432/64/128/256 (two bytes for word adress)
    do {
      adress |= shifted_one; shifted_one = shifted_one << 1; testbyte = (adress >> 8) ^ (char)adress;

      i2c_start(); i2c_wb(_10100000); i2c_wb(adress >> 8); i2c_wb((char)adress);
      i2c_wb(testbyte);
      i2c_stop();
      P1_2=1;
      i=0;
      do { 
        i++;
        i2c_start(); acknowledge = i2c_wb(_10100000);
      } while (!acknowledge && i);
      P1_2=0;
      i2c_wb(adress >> 8);i2c_wb((char)adress);
      i2c_start(); i2c_wb(_10100001); result = i2c_rb(0); i2c_stop();
      if (testbyte != result) break;
      volume++;
      righti = i;
      lefti  = volume;
      delay16(30000);
    } while (adress != 0xffff);
  }

  adress = (adress >> 1) + 1;
  display_int(adress >> 7,0);
  LED_REG=1;
  delay16(1000);
  LED_REG=0;

  while (key_scan() == 12);
}
