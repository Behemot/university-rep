$nolist
$include(c:\asm51\mod51)
$list
;�ணࠬ�� �஢�ન ��⢥७���� ᥬ�ᥣ���⭮�� �������� �40281
;� - ����� ��� �⮡ࠦ���� (�������)
;R7 - ���� ��������
;R0 - �஬��. ��࠭����, ����প�
;R1,R2 - ����প�

        A55     equ     8000h
        B55     equ     8001h
        C55     equ     8002h
        RUS55   equ     8003h
        UW      equ     80h           ;A,B,C=>

        ORG     0000

        MOV     DPTR,#RUS55
        MOV     A,#UW
        MOVX    @DPTR,A         ;���樠������ ��55
M1:
        MOV     A,#55h
        MOV     DPH, #0A0h
        MOVX    @DPTR, A
        MOV     DPH, #0B0h
        MOVX    @DPTR, A        ;�⮡ࠦ��� �� ��������� 55

        MOV     A,#01h         ;����� �� �뢮�
        MOV     R7,#00h         ;����

GO:
        CALL    OUT
        RL      A
        CJNE    A,#01h,GO
        MOV     R0,A            ;�஬��. ��࠭����
        MOV     A,R7
        INC     A
        CJNE    A,#00000100b,NOT_OWF
        CLR     A
NOT_OWF:
        MOV     R7,A
        MOV     A,R0
        JMP     GO
OUT:
        MOV     DPTR,#B55
        MOVX    @DPTR,A

        MOV     DPTR,#C55
        MOV     R0,A            ;�஬��. ��࠭����
        MOV     A,R7
        MOVX    @DPTR,A
        MOV     A,R0

        MOV     R0,#02h
MM:     MOV     R1,#0FFh
M:      MOV     R2,#5Fh
        DJNZ    R2,$
        DJNZ    R1,M
        DJNZ    R0,MM
        RET

        END