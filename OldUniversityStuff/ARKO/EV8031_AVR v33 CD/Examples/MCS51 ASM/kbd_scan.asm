$nolist
$include(c:\asm51\mod51)
$list

	org	0

  	mov	a, #0ch
  	mov	dptr, #0A000h
  	movx	@dptr, a
  	mov	dptr, #0B000h
  	movx	@dptr, a

begin:
  	mov	dptr, #09006h
  	movx	a, @dptr
  	anl	a, #0fh
  	swap	a
  	mov	r0, a


  	mov	dptr, #09005h
  	movx	a, @dptr
  	anl	a, #0fh
  	orl	a, r0
  	
  	mov	dptr, #0A000h
  	movx	@dptr, a





  	mov	dptr, #09003h
  	movx	a, @dptr
  	anl	a, #0fh
  	swap	a

  	mov	dptr, #0B000h
  	movx	@dptr, a

	jmp	begin
  	
  	end