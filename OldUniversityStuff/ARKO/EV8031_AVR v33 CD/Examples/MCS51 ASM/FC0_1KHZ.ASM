$nolist
$include(c:\asm51\mod51)
$list

        mode    equ     15h ;T1 count, T0 timer
        str     equ     50h

        ORG     0000h

        MOV     A,#00000100b
        MOV     DPTR, #0A004h
        MOVX    @DPTR, A        ;������� �������� 3(�����筠� �窠)

BEG:
        MOV     TL0,#00h
        MOV     TH0,#00h

        MOV     TL1,#0BEh
        MOV     TH1,#0E8h

        MOV     TMOD,#MODE
        MOV     TCON,#STR

        JNB     TF1,$
        CLR     TR1
        CLR     TR0

        MOV     R0,TL0
        MOV     R1,TH0

        CALL    IND
        MOV     R3,#0FFh
M:      MOV     R4,#0FFh
        DJNZ    R4,$
        DJNZ    R3,M
        JMP     BEG

IND:
        JMP  M1

;               0    1  2   3   4   5   6   7   8   9
L:      DB      01h,02h,04h,08h,16h,32h,64h,28h,56h,12h
HI:     DB      00h,00h,00h,00h,00h,00h,00h,01h,02h,05h

M1:
        MOV     R2,#00h
        MOV     R6,#00h
        MOV     R7,#00h
NACHALO_L:
        MOV     A,R0
        JNB     ACC.0,AGAIN_L
        MOV     A,R2
        MOV     DPTR,#L
        MOVC    A,@A+DPTR
        ADD     A,R6
        DA      A
        MOV     R6,A
        JNB     PSW.7,PLUS_NEXT_L
        MOV     A,R7
        ADD     A,#01H
        DA      A
        MOV     R7,A
PLUS_NEXT_L:
        MOV     A,R2
        MOV     DPTR,#HI
        MOVC    A,@A+DPTR
        ADD     A,R7
        DA      A
        MOV     R7,A
AGAIN_L:
        MOV     A,R0
        RRC     A
        MOV     R0,A
        INC     R2
        CJNE    R2,#08h,NACHALO_L

NACHALO_HI:
        MOV     A,R1
        JNB     ACC.0,AGAIN_HI
        MOV     A,R2
        MOV     DPTR,#L
        MOVC    A,@A+DPTR
        ADD     A,R6
        DA      A
        MOV     R6,A
        JNB     PSW.7,PLUS_NEXT_HI
        MOV     A,R7
        ADD     A,#01H
        DA      A
        MOV     R7,A
PLUS_NEXT_HI:
        MOV     A,R2
        MOV     DPTR,#HI
        MOVC    A,@A+DPTR
        ADD     A,R7
        DA      A
        MOV     R7,A
AGAIN_HI:
        MOV     A,R1
        RRC     A
        MOV     R1,A
        INC     R2
        CJNE    R2,#0Ah,NACHALO_HI

        MOV     A,R7
        SWAP    A
        MOV     R7,A
        MOV     A,R6
        SWAP    A
        ANL     A,#00001111b
        ORL     A,R7
        MOV     R7,A
        MOV     A,R6
        ANL     A,#00001111b
        ORL     A,#11110000b
        MOV     R6,A

        MOV     A,R6
        MOV     DPTR,#0B000h          ;�뢮� �� ��������
        MOVX    @DPTR,A               ;����襩 ��� ����

        MOV     A,R7
        MOV     DPTR,#0A000h          ;�뢮� �� ��������
        MOVX    @DPTR,A               ;���襩 ��� ����
        RET

        END