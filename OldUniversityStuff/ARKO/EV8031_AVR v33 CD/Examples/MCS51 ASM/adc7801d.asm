$nolist
$include(c:\asm51\mod51)
$list
;�ணࠬ�� ��������� ��஢��� �८�ࠧ������

        pDAC	equ	0f000h

        ORG     0000

CONV_START:
        MOV	R1,#00h
        SETB	P1.7
        MOV	R0,#00h
NEXT_STEP:
        MOV	DPTR,#pDAC
        MOV	A,R0
        MOVX	@DPTR,A
        CALL	delay
        JB	P1.7,CODE_PROCESSING
SHOWTIME:
	CALL	IND_DEC
	JMP	CONV_START
CODE_PROCESSING:
	INC	R0
	JMP	NEXT_STEP




delay:
        MOV     R3,#01h
M:      MOV     R4,#0FFh
        DJNZ    R4,$
        DJNZ    R3,M
        RET

IND_DEC:	; convert & show 10bit hex to bcd
        JMP  MM1

;               0   1   2   3   4   5   6   7   8   9
L:      DB      01h,02h,04h,08h,16h,32h,64h,28h,56h,12h
HI:     DB      00h,00h,00h,00h,00h,00h,00h,01h,02h,05h

MM1:
        MOV     R2,#00h
        MOV     R6,#00h
        MOV     R7,#00h
NACHALO_L:
        MOV     A,R0
        JNB     ACC.0,AGAIN_L
        MOV     A,R2
        MOV     DPTR,#L
        MOVC    A,@A+DPTR
        ADD     A,R6
        DA      A
        MOV     R6,A
        JNB     PSW.7,PLUS_NEXT_L
        MOV     A,R7
        ADD     A,#01H
        DA      A
        MOV     R7,A
PLUS_NEXT_L:
        MOV     A,R2
        MOV     DPTR,#HI
        MOVC    A,@A+DPTR
        ADD     A,R7
        DA      A
        MOV     R7,A
AGAIN_L:
        MOV     A,R0
        RRC     A
        MOV     R0,A
        INC     R2
        CJNE    R2,#08h,NACHALO_L

NACHALO_HI:
        MOV     A,R1
        JNB     ACC.0,AGAIN_HI
        MOV     A,R2
        MOV     DPTR,#L
        MOVC    A,@A+DPTR
        ADD     A,R6
        DA      A
        MOV     R6,A
        JNB     PSW.7,PLUS_NEXT_HI
        MOV     A,R7
        ADD     A,#01H
        DA      A
        MOV     R7,A
PLUS_NEXT_HI:
        MOV     A,R2
        MOV     DPTR,#HI
        MOVC    A,@A+DPTR
        ADD     A,R7
        DA      A
        MOV     R7,A
AGAIN_HI:
        MOV     A,R1
        RRC     A
        MOV     R1,A
        INC     R2
        CJNE    R2,#0Ah,NACHALO_HI

        MOV     A,R6
        MOV     DPTR,#0B000h          ;�뢮� �� ��������
        MOVX    @DPTR,A               ;����襩 ��� ����

        MOV     A,R7
        MOV     DPTR,#0A000h          ;�뢮� �� ��������
        MOVX    @DPTR,A               ;���襩 ��� ����
        RET

        END
