$nolist
$include(c:\asm51\mod51)
$list

	org	0

  	mov	a, #0ch
  	mov	dptr, #0A000h
  	movx	@dptr, a
  	mov	dptr, #0B000h
  	movx	@dptr, a

begin:
  	mov	r0, #0
  	mov	dptr, #090FEh
  next_key_line:
  	; check scan line
  	movx	a, @dptr
  	jnb	ACC.0, end_key_scan
  	inc	r0
  	jnb	ACC.1, end_key_scan
  	inc	r0
  	jnb	ACC.2, end_key_scan
  	inc	r0
  	jnb	acc.3, end_key_scan
  	inc	r0
	; goto next scan line
	mov	a, dpl
	rl	a
	mov	dpl, a
	cjne	a, #0F7h, next_key_line
  end_key_scan:
	
        mov	a, #0ch
        subb	a, r0

        jz	begin

  	mov	a, r0
  	mov	dptr, #0A000h
  	movx	@dptr, a
  	mov	dptr, #0B000h
  	movx	@dptr, a
	jmp	begin
  	
  	end