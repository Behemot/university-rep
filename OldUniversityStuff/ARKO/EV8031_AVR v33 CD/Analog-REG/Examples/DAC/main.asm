;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Tue Aug 02 18:25:31 2005

;--------------------------------------------------------
	.module main
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _set_cursor_PARM_2
	.globl _ncoderLUT
	.globl _layout
	.globl _main
	.globl _test_dac
	.globl _t0_init
	.globl _t0_isr
	.globl _ncoder_scan
	.globl _string_to_hd
	.globl _set_cursor
	.globl _hd_init
	.globl _hd_turn_on
	.globl _display_int
	.globl _lights_off
	.globl _kb_scan
	.globl _key_scan
	.globl _delay16
	.globl _UC_REG
	.globl _DISPLAYB
	.globl _SYS_CTL
	.globl _EDC_REG
	.globl _DC_REG
	.globl _DISPLAY
	.globl _pADC
	.globl _pDAC
	.globl _LCD_DATA
	.globl _LCD_CMD
	.globl _PC_REG
	.globl _PB_REG
	.globl _PA_REG
	.globl _pC
	.globl _pB
	.globl _pA
	.globl _vv55RUS
	.globl _LED_REG
	.globl _new_dotsi
	.globl _dotsi
	.globl _righti
	.globl _lefti
	.globl _dac_value
	.globl _string_to_hd_PARM_3
	.globl _string_to_hd_PARM_2
	.globl _display_int_PARM_2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_kb_scan_prev_scan_1_1:
	.ds 1
_kb_scan_timer_1_1:
	.ds 1
_display_int_buffer_1_1:
	.ds 4
_display_int_PARM_2::
	.ds 1
_string_to_hd_PARM_2::
	.ds 1
_string_to_hd_PARM_3::
	.ds 3
_dac_value::
	.ds 2
_ncoder_scan_state_1_1:
	.ds 1
_t0_isr_state_1_1:
	.ds 1
_t0_isr_pwm_cntr_1_1:
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
_set_cursor_PARM_2::
	.ds 1
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_lefti	=	0xa000
_righti	=	0xb000
_dotsi	=	0xc000
_new_dotsi	=	0xa004
_LED_REG	=	0xa006
_vv55RUS	=	0x8003
_pA	=	0x8000
_pB	=	0x8001
_pC	=	0x8002
_PA_REG	=	0x8000
_PB_REG	=	0x8001
_PC_REG	=	0x8002
_LCD_CMD	=	0x8004
_LCD_DATA	=	0x8005
_pDAC	=	0xf000
_pADC	=	0xe000
_DISPLAY	=	0xa000
_DC_REG	=	0xa004
_EDC_REG	=	0xa005
_SYS_CTL	=	0xa007
_DISPLAYB	=	0xb000
_UC_REG	=	0xc000
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_t0_isr
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
;------------------------------------------------------------
;Allocation info for local variables in function 'ncoder_scan'
;------------------------------------------------------------
;state                     Allocated to in memory with name '_ncoder_scan_state_1_1'
;------------------------------------------------------------
;	dac.c:25: xdata at 0xB000 unsigned char DISPLAYB;
;	genAssign
	mov	_ncoder_scan_state_1_1,#0x00
;------------------------------------------------------------
;Allocation info for local variables in function 't0_isr'
;------------------------------------------------------------
;state                     Allocated to in memory with name '_t0_isr_state_1_1'
;pwm_cntr                  Allocated to in memory with name '_t0_isr_pwm_cntr_1_1'
;answer                    Allocated to registers r2 
;------------------------------------------------------------
;	dac.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	genAssign
	mov	_t0_isr_state_1_1,#0x00
;	genAssign
	mov	_t0_isr_pwm_cntr_1_1,#0x00
;	main.c:3: xdata at 0xC000 unsigned char dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:4: xdata at 0xA004 unsigned char new_dotsi = 0; // dots of indication
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:5: xdata at 0xA006 unsigned char LED_REG = 0; // leds on digital board
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:6: xdata at 0x8003 unsigned char vv55RUS = 0x80; // CWR of 580vv55
;	genAssign
	mov	dptr,#_vv55RUS
	mov	a,#0x80
	movx	@dptr,a
;	main.c:7: xdata at 0x8000 unsigned char pA = 0x00; // port A of 580vv55
;	genAssign
	mov	dptr,#_pA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:8: xdata at 0x8001 unsigned char pB = 0xff; // port B of 580vv55
;	genAssign
	mov	dptr,#_pB
	mov	a,#0xFF
	movx	@dptr,a
;	main.c:9: xdata at 0x8002 unsigned char pC = 0xff; // port C of 580vv55
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	main.c:26: xdata at 0xC000 unsigned char UC_REG = 0;
;	genAssign
	mov	dptr,#_UC_REG
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'delay16'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:37: void delay16(unsigned int i) { do { } while(--i); }
;	-----------------------------------------
;	 function delay16
;	-----------------------------------------
_delay16:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	genAssign
00101$:
;	genDjnz
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00105$
	dec	r3
00105$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00101$
00106$:
00102$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'key_scan'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:49: unsigned char key_scan()
;	-----------------------------------------
;	 function key_scan
;	-----------------------------------------
_key_scan:
;	..\ev8031.lib\ev8031.c:72: _endasm;
;	genInline
;
	        mov r0, #0
	        mov dptr, #0x9000 +0xFE
  next_key_line:
	        ; check scan line
	        movx a, @dptr
	        jnb acc.0, end_key_scan
	        inc r0
	        jnb acc.1, end_key_scan
	        inc r0
	        jnb acc.2, end_key_scan
	        inc r0
	        jnb acc.3, end_key_scan
	        inc r0
	        ; goto next scan line
	        mov a, dpl
	        rl a
	        mov dpl, a
	        cjne a, #0xF7, next_key_line
  end_key_scan:
	        mov dpl, r0
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'kb_scan'
;------------------------------------------------------------
;prev_scan                 Allocated to in memory with name '_kb_scan_prev_scan_1_1'
;timer                     Allocated to in memory with name '_kb_scan_timer_1_1'
;scan                      Allocated to registers r2 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:97: char kb_scan()
;	-----------------------------------------
;	 function kb_scan
;	-----------------------------------------
_kb_scan:
;	..\ev8031.lib\ev8031.c:103: scan = key_scan();
;	genCall
	lcall	_key_scan
	mov	a,dpl
;	genAssign
;	..\ev8031.lib\ev8031.c:104: if (scan == prev_scan) {
;	genCmpEq
; Peephole 105   removed redundant mov
	mov  r2,a
	cjne	a,_kb_scan_prev_scan_1_1,00110$
	sjmp	00111$
00110$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00111$:
;	..\ev8031.lib\ev8031.c:105: timer++;
;	genPlus
;	genPlusIncr
	inc	_kb_scan_timer_1_1
;	..\ev8031.lib\ev8031.c:107: if (timer == 0) return (scan);
;	genCmpEq
	mov	a,_kb_scan_timer_1_1
; Peephole 162   removed sjmp by inverse jump logic
	jz   00113$
00112$:
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00113$:
;	genRet
	mov	dpl,r2
; Peephole 132   changed ljmp to sjmp
	sjmp 00106$
00104$:
;	..\ev8031.lib\ev8031.c:109: timer = 0;
;	genAssign
	mov	_kb_scan_timer_1_1,#0x00
00105$:
;	..\ev8031.lib\ev8031.c:111: prev_scan = scan;
;	genAssign
	mov	_kb_scan_prev_scan_1_1,r2
;	..\ev8031.lib\ev8031.c:113: return 12;
;	genRet
	mov	dpl,#0x0C
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lights_off'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:115: void lights_off()
;	-----------------------------------------
;	 function lights_off
;	-----------------------------------------
_lights_off:
;	..\ev8031.lib\ev8031.c:117: LED_REG=0;
;	genAssign
	mov	dptr,#_LED_REG
; Peephole 180   changed mov to clr
;	..\ev8031.lib\ev8031.c:118: pA=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219 removed redundant clear
;	..\ev8031.lib\ev8031.c:119: pB=0;
;	genAssign
; Peephole 180   changed mov to clr
; Peephole 219a removed redundant clear
	clr   a
	movx  @dptr,a
	mov   dptr,#_pA
	movx  @dptr,a
	mov   dptr,#_pB
	movx  @dptr,a
;	..\ev8031.lib\ev8031.c:120: pC=0xFF;
;	genAssign
	mov	dptr,#_pC
	mov	a,#0xFF
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:121: new_dotsi=0x0f;
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x0F
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display_int'
;------------------------------------------------------------
;buffer                    Allocated to in memory with name '_display_int_buffer_1_1'
;dc_mask                   Allocated to in memory with name '_display_int_PARM_2'
;value                     Allocated to registers r2 r3 
;ptr                       Allocated to registers r4 
;i                         Allocated to registers 
;dc                        Allocated to registers r5 
;c                         Allocated to registers r6 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:123: void display_int(unsigned int value, unsigned char dc_mask)
;	-----------------------------------------
;	 function display_int
;	-----------------------------------------
_display_int:
;	genReceive
	mov	r2,dpl
	mov	r3,dph
;	..\ev8031.lib\ev8031.c:126: unsigned char data *ptr = buffer;
;	genAssign
	mov	r4,#_display_int_buffer_1_1
;	..\ev8031.lib\ev8031.c:127: unsigned char i, dc = 0;
;	genAssign
	mov	r5,#0x00
;	..\ev8031.lib\ev8031.c:129: i = 4; do {
;	genAssign
	mov	ar0,r4
;	genAssign
	mov	r4,#0x04
00101$:
;	..\ev8031.lib\ev8031.c:131: c = value % 10;
;	genAssign
	clr	a
	mov	(__moduint_PARM_2 + 1),a
	mov	__moduint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar0
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	pop	ar0
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genCast
;	..\ev8031.lib\ev8031.c:132: value /= 10;
;	genAssign
	clr	a
	mov	(__divuint_PARM_2 + 1),a
	mov	__divuint_PARM_2,#0x0A
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar0
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
	pop	ar0
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	r2,a
	mov	r3,b
;	..\ev8031.lib\ev8031.c:133: *ptr++ = c;
;	genPointerSet
;	genNearPointerSet
	mov	@r0,ar6
	inc	r0
;	..\ev8031.lib\ev8031.c:134: } while (--i);
;	genDjnz
	djnz	r4,00116$
	sjmp	00117$
00116$:
	ljmp	00101$
00117$:
;	..\ev8031.lib\ev8031.c:136: i = 3; do {
;	genAssign
	mov	r2,#0x03
;	genAssign
00107$:
;	..\ev8031.lib\ev8031.c:137: --ptr;
;	genMinus
;	genMinusDec
	dec	r0
;	..\ev8031.lib\ev8031.c:138: if (!*ptr) dc = (dc << 1) | 1;
;	genPointerGet
;	genNearPointerGet
	mov	ar3,@r0
;	genIfx
	mov	a,r3
;	genIfxJump
; Peephole 109   removed ljmp by inverse jump logic
	jnz  00109$
00118$:
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,r5
	add	a,acc
	mov	r3,a
;	genOr
	mov	a,#0x01
	orl	a,r3
	mov	r5,a
;	..\ev8031.lib\ev8031.c:140: } while (--i);
;	genDjnz
; Peephole 132   changed ljmp to sjmp
; Peephole 205   optimized misc jump sequence
	djnz r2,00107$
00119$:
00120$:
00109$:
;	..\ev8031.lib\ev8031.c:141: DC_REG = dc | dc_mask;
;	genOr
	mov	dptr,#_DC_REG
	mov	a,_display_int_PARM_2
	orl	a,r5
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:143: DISPLAY[0] = buffer[3] << 4 | buffer[2];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0003 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,0x0002 + _display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#_DISPLAY
	mov	a,r2
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:144: DISPLAY[1] = buffer[1] << 4 | buffer[0];
;	genAssign
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,0x0001 + _display_int_buffer_1_1
	swap	a
	anl	a,#0xf0
	mov	r2,a
;	genAssign
;	genOr
	mov	a,_display_int_buffer_1_1
	orl	ar2,a
;	genPointerSet
;	genFarPointerSet
	mov	dptr,#(_DISPLAY + 0x0001)
	mov	a,r2
	movx	@dptr,a
00110$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_turn_on'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:147: void hd_turn_on()
;	-----------------------------------------
;	 function hd_turn_on
;	-----------------------------------------
_hd_turn_on:
;	..\ev8031.lib\ev8031.c:149: delay16(30000); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x75)<<8) + 0x30)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:150: delay16(450); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x01)<<8) + 0xC2)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:151: delay16(150); LCD_CMD = 0x30;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x30
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:153: delay16(150); LCD_CMD = _01001000;
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x48
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:155: delay16(150); LCD_DATA= _00001100;	//
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:156: delay16(150); LCD_DATA= _00010010;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x12
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:157: delay16(150); LCD_DATA= _00001100;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:158: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:159: delay16(150); LCD_DATA= _00000000;	// ᨬ��� �ࠤ�� \x04
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:160: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:161: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:162: delay16(150); LCD_DATA= _00000000;	//                    
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_DATA
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hd_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:165: void hd_init()
;	-----------------------------------------
;	 function hd_init
;	-----------------------------------------
_hd_init:
;	..\ev8031.lib\ev8031.c:167: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:168: delay16(150); LCD_CMD = _00001100;	// display on, cursor off, blinc char at cursor pos
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x0C
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:169: delay16(150); LCD_CMD = 0x06;		// increment, no shift
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x06
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:170: delay16(150); LCD_CMD = 1;		// clear display & cursor to home
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x01
	movx	@dptr,a
;	..\ev8031.lib\ev8031.c:171: delay16(150); LCD_CMD = _00111000;	// DataBus=8bit,lines=2,font=5x8 
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x96)
	lcall	_delay16
;	genAssign
	mov	dptr,#_LCD_CMD
	mov	a,#0x38
	movx	@dptr,a
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_cursor'
;------------------------------------------------------------
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:173: void set_cursor(unsigned char x, unsigned char y)
;	-----------------------------------------
;	 function set_cursor
;	-----------------------------------------
_set_cursor:
;	genReceive
	mov	r2,dpl
;	..\ev8031.lib\ev8031.c:175: switch (y) {
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,_set_cursor_PARM_2
;	genIfxJump
; Peephole 132   changed ljmp to sjmp
; Peephole 160   removed sjmp by inverse jump logic
	jc   00105$
00109$:
;	genJumpTab
	mov	a,_set_cursor_PARM_2
	add	a,acc
	add	a,_set_cursor_PARM_2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	..\ev8031.lib\ev8031.c:176: case 0: y=0;    break;
00101$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x00
;	..\ev8031.lib\ev8031.c:177: case 1: y=0x40; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00102$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x40
;	..\ev8031.lib\ev8031.c:178: case 2: y=0x0A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00103$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x0A
;	..\ev8031.lib\ev8031.c:179: case 3: y=0x4A; break;
; Peephole 132   changed ljmp to sjmp
	sjmp 00105$
00104$:
;	genAssign
	mov	_set_cursor_PARM_2,#0x4A
;	..\ev8031.lib\ev8031.c:180: }
00105$:
;	..\ev8031.lib\ev8031.c:181: y=y+x;
;	genPlus
	mov	a,ar2
	add	a,_set_cursor_PARM_2
;	..\ev8031.lib\ev8031.c:182: LCD_CMD = (_10000000 | y);		// set start position
;	genOr
	mov	dptr,#_LCD_CMD
	orl	a,#0x80
	movx	@dptr,a
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'string_to_hd'
;------------------------------------------------------------
;y                         Allocated to in memory with name '_string_to_hd_PARM_2'
;string                    Allocated to in memory with name '_string_to_hd_PARM_3'
;x                         Allocated to registers 
;------------------------------------------------------------
;	..\ev8031.lib\ev8031.c:185: void string_to_hd(unsigned char x, unsigned char y, char *string)
;	-----------------------------------------
;	 function string_to_hd
;	-----------------------------------------
_string_to_hd:
;	genReceive
;	..\ev8031.lib\ev8031.c:187: set_cursor(x,y);
;	genAssign
	mov	_set_cursor_PARM_2,_string_to_hd_PARM_2
;	genCall
	lcall	_set_cursor
;	..\ev8031.lib\ev8031.c:188: while((y = *string++)) {
;	genAssign
	mov	r2,_string_to_hd_PARM_3
	mov	r3,(_string_to_hd_PARM_3 + 1)
	mov	r4,(_string_to_hd_PARM_3 + 2)
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r5,a
	inc	dptr
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
;	genAssign
	mov	ar6,r5
;	genIfx
	mov	a,r5
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00104$
00108$:
;	..\ev8031.lib\ev8031.c:190: LCD_DATA = y;
;	genAssign
	mov	dptr,#_LCD_DATA
	mov	a,r6
	movx	@dptr,a
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ncoder_scan'
;------------------------------------------------------------
;state                     Allocated to in memory with name '_ncoder_scan_state_1_1'
;------------------------------------------------------------
;	dac.c:23: xdata at 0xA007 unsigned char SYS_CTL;
;	-----------------------------------------
;	 function ncoder_scan
;	-----------------------------------------
_ncoder_scan:
;	dac.c:28: #define KB_BASE 0x9000
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,_ncoder_scan_state_1_1
	add	a,acc
	add	a,acc
	mov	_ncoder_scan_state_1_1,a
;	dac.c:29: 
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P1_7,00102$
00109$:
;	genOr
	orl	_ncoder_scan_state_1_1,#0x01
00102$:
;	dac.c:30: #define MEM(Addr) (*((xdata unsigned char *)(Addr)))
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P1_4,00104$
00110$:
;	genOr
	orl	_ncoder_scan_state_1_1,#0x02
00104$:
;	dac.c:31: #define HIGH(Value) ((unsigned char)((Value)>>8))
;	genAnd
	anl	_ncoder_scan_state_1_1,#0x0F
;	dac.c:32: #define LOW(Value) ((unsigned char)(Value))
;	genPlus
	mov	a,_ncoder_scan_state_1_1
; Peephole 180   changed mov to clr
;	genPointerGet
;	genCodePointerGet
; Peephole 186   optimized movc sequence
	mov  dptr,#_ncoderLUT
	movc a,@a+dptr
	mov	r2,a
;	dac.c:33: 
;	genRet
	mov	dpl,r2
00105$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 't0_isr'
;------------------------------------------------------------
;state                     Allocated to in memory with name '_t0_isr_state_1_1'
;pwm_cntr                  Allocated to in memory with name '_t0_isr_pwm_cntr_1_1'
;answer                    Allocated to registers r2 
;------------------------------------------------------------
;	dac.c:35: //#define delay16(n) { unsigned int i=n; do { } while(--i); }
;	-----------------------------------------
;	 function t0_isr
;	-----------------------------------------
_t0_isr:
	push	acc
	push	b
	push	dpl
	push	dph
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	psw
	mov	psw,#0x00
;	dac.c:40: #define DQ P1_2
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
	mov	a,_t0_isr_state_1_1
	add	a,acc
	add	a,acc
	mov	_t0_isr_state_1_1,a
;	dac.c:41: // One Wire
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P1_7,00102$
00119$:
;	genOr
	orl	_t0_isr_state_1_1,#0x01
00102$:
;	dac.c:42: 
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _P1_4,00104$
00120$:
;	genOr
	orl	_t0_isr_state_1_1,#0x02
00104$:
;	dac.c:43: // I2C
;	genAnd
	anl	_t0_isr_state_1_1,#0x0F
;	dac.c:44: #define SCL P1_1
;	genPlus
	mov	a,_t0_isr_state_1_1
; Peephole 180   changed mov to clr
;	genPointerGet
;	genCodePointerGet
; Peephole 186   optimized movc sequence
	mov  dptr,#_ncoderLUT
	movc a,@a+dptr
;	dac.c:47: // I2C
;	genCast
; Peephole 105   removed redundant mov
	mov  r2,a
	rlc	a
	subb	a,acc
	mov	r3,a
;	genPlus
	mov	a,ar2
	add	a,_dac_value
	mov	_dac_value,a
	mov	a,ar3
	addc	a,(_dac_value + 1)
;	dac.c:48: 
;	genCmpLt
;	genCmp
; Peephole 105   removed redundant mov
	mov  (_dac_value + 1),a
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  acc.7,00106$
00121$:
;	genAssign
	clr	a
	mov	(_dac_value + 1),a
	mov	_dac_value,a
00106$:
;	dac.c:49: unsigned char key_scan()
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,_dac_value
; Peephole 159   avoided xrl during execution
	mov  a,#(0x00 ^ 0x80)
	mov	b,(_dac_value + 1)
	xrl	b,#0x80
	subb	a,b
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00108$
00122$:
;	genAssign
	clr	a
	mov	(_dac_value + 1),a
	mov	_dac_value,#0xFF
00108$:
;	dac.c:50: {
;	genXor
	cpl	_P1_6
;	dac.c:52: mov	r0, #0
;	genPlus
;	genPlusIncr
	inc	_t0_isr_pwm_cntr_1_1
;	dac.c:53: mov	dptr, #KB_BASE+0xFE
;	genAssign
	mov	r2,_dac_value
	mov	r3,(_dac_value + 1)
;	genCast
	mov	r4,_t0_isr_pwm_cntr_1_1
	mov	r5,#0x00
;	genCmpGt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00110$
00123$:
;	genAssign
	clr	_P1_0
; Peephole 132   changed ljmp to sjmp
	sjmp 00112$
00110$:
;	dac.c:54: next_key_line:
;	genAssign
	setb	_P1_0
00112$:
	pop	psw
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 't0_init'
;------------------------------------------------------------
;------------------------------------------------------------
;	dac.c:58: inc	r0
;	-----------------------------------------
;	 function t0_init
;	-----------------------------------------
_t0_init:
;	dac.c:60: inc	r0
;	genAssign
	clr	_EA
;	dac.c:61: jnb	acc.2, end_key_scan
;	genAssign
	mov	_TH0,#0xAF
;	dac.c:62: inc	r0
;	genAssign
	mov	_TMOD,#0x22
;	dac.c:63: jnb	acc.3, end_key_scan
;	genAssign
	setb	_TR0
;	dac.c:64: inc	r0
;	genAssign
	setb	_ET0
;	dac.c:65: ; goto next scan line
;	genAssign
	setb	_EA
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'test_dac'
;------------------------------------------------------------
;i                         Allocated to registers 
;------------------------------------------------------------
;	dac.c:68: mov	dpl, a
;	-----------------------------------------
;	 function test_dac
;	-----------------------------------------
_test_dac:
;	dac.c:72: _endasm;
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x0D
	movx	@dptr,a
;	genAssign
	mov	dptr,#_righti
	mov	a,#0xAC
	movx	@dptr,a
;	genAssign
	mov	dptr,#_new_dotsi
	mov	a,#0x01
	movx	@dptr,a
;	dac.c:73: }
;	genCall
	lcall	_t0_init
;	dac.c:77: 0 4 8
00102$:
;	dac.c:78: 1 5 9
;	genAssign
	mov	dpl,_dac_value
	mov	dph,(_dac_value + 1)
;	genAssign
	mov	_display_int_PARM_2,#0x00
;	genCall
	lcall	_display_int
;	dac.c:79: 2 6 10
;	genCast
	mov	dptr,#_pDAC
	mov	a,_dac_value
	movx	@dptr,a
;	dac.c:80: 3 7 11
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x00)<<8) + 0x64)
	lcall	_delay16
;	dac.c:81: */
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpEq
	cjne	r2,#0x0C,00108$
; Peephole 132   changed ljmp to sjmp
	sjmp 00102$
00108$:
00105$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:10: xdata at 0x8000 unsigned char PA_REG;
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:12: xdata at 0x8002 unsigned char PC_REG;
00101$:
;	main.c:13: 
;	genAssign
	mov	dptr,#_lefti
	mov	a,#0x12
	movx	@dptr,a
;	main.c:14: xdata at 0x8004 unsigned char LCD_CMD;
;	genAssign
	mov	dptr,#_righti
	mov	a,#0x34
	movx	@dptr,a
;	main.c:15: xdata at 0x8005 unsigned char LCD_DATA;
;	genAssign
	mov	dptr,#_new_dotsi
; Peephole 180   changed mov to clr
	clr  a
	movx	@dptr,a
;	main.c:16: 
;	genCall
	lcall	_key_scan
	mov	r2,dpl
;	genCmpLt
;	genCmp
	cjne	r2,#0x0C,00108$
00108$:
;	genIfxJump
; Peephole 108   removed ljmp by inverse jump logic
	jnc  00101$
00109$:
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	genCall
	lcall	_test_dac
;	genCall
; Peephole 182   used 16 bit load of dptr
	mov  dptr,#(((0x4E)<<8) + 0x20)
	lcall	_delay16
;	main.c:17: xdata at 0xf000 unsigned char pDAC;
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
	.area CSEG    (CODE)
_layout:
	.db #0x01
	.db #0x04
	.db #0x07
	.db #0x0A
	.db #0x02
	.db #0x05
	.db #0x08
	.db #0x00
	.db #0x03
	.db #0x06
	.db #0x09
	.db #0x0B
	.db #0x0C
_ncoderLUT:
	.db #0x00
	.db #0x01
	.db #0xFF
	.db #0x00
	.db #0xFF
	.db #0x00
	.db #0x00
	.db #0x01
	.db #0x01
	.db #0x00
	.db #0x00
	.db #0xFF
	.db #0x00
	.db #0xFF
	.db #0x01
	.db #0x00
	.area XINIT   (CODE)
