code signed char ncoderLUT[] = {
	0,	// 0000 0
	+1,	// 0001 1
	-1,	// 0010 2
	0,	// 0011 3

	-1,	// 0100 4
	0,	// 0101 5
	0,	// 0110 6
	+1,	// 0111 7

	+1,	// 1000 8
	0,	// 1001 9
	0,	// 1010 10
	-1,	// 1011 11

	0,	// 1100 12
	-1,	// 1101 13
	+1,	// 1110 14
	0	// 1111 15
};

signed char ncoder_scan()
{
  static char state=0;
  signed char answer;

  state = (state << 2);
  if (P1_7) state |= _00000001;
  if (P1_4) state |= _00000010;
  state &= _00001111;
  answer = ncoderLUT[state];
  return answer;
}
void t0_isr () interrupt 2	// interrupt service routine for T0 � 
{
  static unsigned char state=0, pwm_cntr=0;
  signed char answer;

  state = (state << 2);
  if (P1_7) state |= _00000001;
  if (P1_4) state |= _00000010;
  state &= _00001111;
  answer = ncoderLUT[state];

//  dac_value += ncoder_scan();
  dac_value += answer;
  if (dac_value < 0) dac_value = 0;
  if (dac_value > 0xff) dac_value = 0xff;
  P1_6 ^= 1;

  pwm_cntr++;
  if (dac_value > pwm_cntr) P1_0 = 0;
  else P1_0 = 1;

}�

void t0_init()
{
  EA=0;
  TH0 = 0xff-80;
  TMOD = 0x22;	// c/t0-timer, mode 2
  TR0 = 1;
  ET0 = 1;
  EA = 1;
}

void test_dac()
{
  unsigned char i = 0;

  lefti = 0x0d; righti = 0xac; new_dotsi = _00000001;
  t0_init();

start:

  do {
    display_int(dac_value,0);
    pDAC = (char)dac_value;
    delay16(100);
  } while (key_scan() == 12);

}
