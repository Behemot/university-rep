ASxxxx Assembler V01.70 + NoICE + SDCC mods + Flat24 Feb-1999  (Intel 8051), page 1.



                              1 ;--------------------------------------------------------
                              2 ; blink.asm - simple ASM example to toggle a port pin
                              3 ;   (LED on TINI board).
                              4 ;
                              5 ;   On a TINI board this toggles really fast, so
                              6 ;   it appears that the LED just dims slightly.
                              7 ;   You may need a scope to detect it.
                              8 ;
                              9 ;  You can use s51(ucsim simulator) to look at the
                             10 ;  blink.ihx(hex-file).  Run like this:
                             11 ;  >S51 blink.ihx
                             12 ;  Then dump code like this:
                             13 ;  s51>dc
                             14 ;--------------------------------------------------------
                             15    .module bootasm
                             16 
                             17 ;--------------------------------------------------------
                             18 ; Stack segment in internal ram 
                             19 ;--------------------------------------------------------
                             20 	.area	SSEG	(DATA)
   0008                      21 __start__stack:
   0008                      22 	.ds	1
                             23 
                             24 	.area	CODE (CODE)
                             25 	
                             26 ;--------------------------------------------------------
                             27 ; global & static initialisations
                             28 ;--------------------------------------------------------
                             29 	.area GSINIT (CODE)
   0000                      30 main:
   0000 C2 AF                31    clr   ea    ; disable ints
   0002 C2 B5                32    clr   p3.5  ; turn LED off
   0004 D2 B5                33    setb  p3.5  ; turn LED on
   0006 02 00 00             34    ljmp  main
