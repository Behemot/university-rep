;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Fri Dec 20 19:27:49 2002

;--------------------------------------------------------
	.module hi
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _my_message
	.globl _main
	.globl _stop
	.globl _tx_str
	.globl _tx_char
	.globl _mem_mapped_hardware
	.globl _a_xdata_byte
	.globl _my_bit
	.globl _a_idata_byte
	.globl _a_data_byte
	.globl _hi_flag
	.globl _timer
	.globl _timer0_irq_proc
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_WDCON	=	0x00d8
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_ET2	=	0x00ad
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_LED_SYS	=	0x00b5
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_2	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_timer::
	.ds 1
_hi_flag::
	.ds 1
_a_data_byte::
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
_a_idata_byte::
	.ds 1
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
_my_bit::
	.ds 1
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_a_xdata_byte::
	.ds 1
_mem_mapped_hardware	=	0x8000
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	ljmp	_timer0_irq_proc
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'timer0_irq_proc'
;------------------------------------------------------------
;------------------------------------------------------------
;	hi.c:52: void timer0_irq_proc(void) interrupt 1 using 2
;	-----------------------------------------
;	 function timer0_irq_proc
;	-----------------------------------------
_timer0_irq_proc:
	ar2 = 0x12
	ar3 = 0x13
	ar4 = 0x14
	ar5 = 0x15
	ar6 = 0x16
	ar7 = 0x17
	ar0 = 0x10
	ar1 = 0x11
	push	acc
	push	b
	push	dpl
	push	dph
	push	psw
	mov	psw,#0x10
;	hi.c:54: if (timer != 0)
;	genCmpEq
	mov	a,_timer
; Peephole 110   removed ljmp by inverse jump logic
	jz  00102$
00107$:
;	hi.c:56: --timer;
;	genMinus
;	genMinusDec
	dec	_timer
; Peephole 132   changed ljmp to sjmp
	sjmp 00103$
00102$:
;	hi.c:60: hi_flag = 1;
;	genAssign
	mov	_hi_flag,#0x01
;	hi.c:61: timer = 250;
;	genAssign
	mov	_timer,#0xFA
00103$:
;	hi.c:64: TR0 = 0; /* Stop Timer 0 counting */
;	genAssign
	clr	_TR0
;	hi.c:65: TH0 = (~(5000)) >> 8;
;	genAssign
	mov	_TH0,#0xEC
;	hi.c:66: TL0 = (~(5000)) & 0xff;
;	genAssign
	mov	_TL0,#0x77
;	hi.c:67: TR0 = 1; /* Start counting again */
;	genAssign
	setb	_TR0
00104$:
	pop	psw
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'tx_char'
;------------------------------------------------------------
;------------------------------------------------------------
;	hi.c:88: void tx_char(char c)
;	-----------------------------------------
;	 function tx_char
;	-----------------------------------------
_tx_char:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	_SBUF,dpl
;	hi.c:91: while (!TI)
00101$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _TI,00101$
00108$:
;	hi.c:93: TI = 0;
;	genAssign
	clr	_TI
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'tx_str'
;------------------------------------------------------------
;str                       Allocated to registers r2 r3 r4 
;------------------------------------------------------------
;	hi.c:99: void tx_str(char *str)
;	-----------------------------------------
;	 function tx_str
;	-----------------------------------------
_tx_str:
;	genReceive
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
;	hi.c:102: while (*str)
;	genAssign
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
; Peephole 105   removed redundant mov
	mov  r5,a
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00104$
00108$:
;	hi.c:103: tx_char(*str++);
;	genPlus
;	genPlusIncr
	inc	r2
	cjne	r2,#0x00,00109$
	inc	r3
00109$:
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_tx_char
	pop	ar4
	pop	ar3
	pop	ar2
; Peephole 132   changed ljmp to sjmp
	sjmp 00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'stop'
;------------------------------------------------------------
;------------------------------------------------------------
;	hi.c:111: void stop(void)
;	-----------------------------------------
;	 function stop
;	-----------------------------------------
_stop:
;	hi.c:117: _endasm;
;	genInline
;
	  mov dptr, #65535;
	  movx a, @dptr;
	  nop;
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	hi.c:123: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	hi.c:125: PCON = 0x80;  /* power control byte, set SMOD bit for serial port */
;	genAssign
	mov	_PCON,#0x80
;	hi.c:126: SCON = 0x50;  /* serial control byte, mode 1, RI active */
;	genAssign
	mov	_SCON,#0x50
;	hi.c:127: TMOD = 0x21;  /* timer control mode, byte operation */
;	genAssign
	mov	_TMOD,#0x21
;	hi.c:128: TCON = 0;     /* timer control register, byte operation */
;	genAssign
	mov	_TCON,#0x00
;	hi.c:130: TH1 = 0xFA;   /* serial reload value, 9,600 baud at 11.0952Mhz */
;	genAssign
	mov	_TH1,#0xFA
;	hi.c:131: TR1 = 1;      /* start serial timer */
;	genAssign
	setb	_TR1
;	hi.c:133: TR0 = 1;      /* start timer0 */
;	genAssign
	setb	_TR0
;	hi.c:134: ET0 = 1;      /* Enable Timer 0 overflow interrupt IE.1 */
;	genAssign
	setb	_ET0
;	hi.c:135: EA = 1;       /* Enable Interrupts */
;	genAssign
	setb	_EA
;	hi.c:137: TI = 0;       /* clear this out */
;	genAssign
	clr	_TI
;	hi.c:138: SBUF = '.';   /* send an initial '.' out serial port */
;	genAssign
	mov	_SBUF,#0x2E
;	hi.c:139: hi_flag = 1;
;	genAssign
	mov	_hi_flag,#0x01
;	hi.c:142: tx_str(my_message);
;	genCall
; Peephole 182a use 16 bit load of DPTR
	mov dptr,#_my_message
	mov	b,#0x02
	lcall	_tx_str
00104$:
;	hi.c:146: if (hi_flag)
;	genIfx
	mov	a,_hi_flag
;	genIfxJump
; Peephole 110   removed ljmp by inverse jump logic
	jz  00102$
00110$:
;	hi.c:148: tx_str("Hi There\n");
;	genCall
; Peephole 182a use 16 bit load of DPTR
	mov dptr,#__str_0
	mov	b,#0x02
	lcall	_tx_str
;	hi.c:149: hi_flag = 0;
;	genAssign
	mov	_hi_flag,#0x00
00102$:
;	hi.c:152: stop();
;	genCall
	lcall	_stop
; Peephole 132   changed ljmp to sjmp
	sjmp 00104$
00106$:
	ret
	.area CSEG    (CODE)
_my_message:
	.ascii "GNU rocks"
	.db 0x0A
	.db 0x00
__str_0:
	.ascii "Hi There"
	.db 0x0A
	.db 0x00
	.area XINIT   (CODE)
