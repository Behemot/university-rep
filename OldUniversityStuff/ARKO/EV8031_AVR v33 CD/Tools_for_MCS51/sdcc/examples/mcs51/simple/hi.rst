                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.3.2 Fri Dec 20 19:27:49 2002
                              4 
                              5 ;--------------------------------------------------------
                              6 	.module hi
                              7 	
                              8 ;--------------------------------------------------------
                              9 ; Public variables in this module
                             10 ;--------------------------------------------------------
                             11 	.globl _my_message
                             12 	.globl _main
                             13 	.globl _stop
                             14 	.globl _tx_str
                             15 	.globl _tx_char
                             16 	.globl _mem_mapped_hardware
                             17 	.globl _a_xdata_byte
                             18 	.globl _my_bit
                             19 	.globl _a_idata_byte
                             20 	.globl _a_data_byte
                             21 	.globl _hi_flag
                             22 	.globl _timer
                             23 	.globl _timer0_irq_proc
                             24 ;--------------------------------------------------------
                             25 ; special function registers
                             26 ;--------------------------------------------------------
                    0080     27 _P0	=	0x0080
                    0081     28 _SP	=	0x0081
                    0082     29 _DPL	=	0x0082
                    0083     30 _DPH	=	0x0083
                    0087     31 _PCON	=	0x0087
                    0088     32 _TCON	=	0x0088
                    0089     33 _TMOD	=	0x0089
                    008A     34 _TL0	=	0x008a
                    008B     35 _TL1	=	0x008b
                    008C     36 _TH0	=	0x008c
                    008D     37 _TH1	=	0x008d
                    0090     38 _P1	=	0x0090
                    0098     39 _SCON	=	0x0098
                    0099     40 _SBUF	=	0x0099
                    00A0     41 _P2	=	0x00a0
                    00A8     42 _IE	=	0x00a8
                    00B0     43 _P3	=	0x00b0
                    00B8     44 _IP	=	0x00b8
                    00D0     45 _PSW	=	0x00d0
                    00E0     46 _ACC	=	0x00e0
                    00F0     47 _B	=	0x00f0
                    00C8     48 _T2CON	=	0x00c8
                    00CA     49 _RCAP2L	=	0x00ca
                    00CB     50 _RCAP2H	=	0x00cb
                    00CC     51 _TL2	=	0x00cc
                    00CD     52 _TH2	=	0x00cd
                    00D8     53 _WDCON	=	0x00d8
                             54 ;--------------------------------------------------------
                             55 ; special function bits 
                             56 ;--------------------------------------------------------
                    0080     57 _P0_0	=	0x0080
                    0081     58 _P0_1	=	0x0081
                    0082     59 _P0_2	=	0x0082
                    0083     60 _P0_3	=	0x0083
                    0084     61 _P0_4	=	0x0084
                    0085     62 _P0_5	=	0x0085
                    0086     63 _P0_6	=	0x0086
                    0087     64 _P0_7	=	0x0087
                    0088     65 _IT0	=	0x0088
                    0089     66 _IE0	=	0x0089
                    008A     67 _IT1	=	0x008a
                    008B     68 _IE1	=	0x008b
                    008C     69 _TR0	=	0x008c
                    008D     70 _TF0	=	0x008d
                    008E     71 _TR1	=	0x008e
                    008F     72 _TF1	=	0x008f
                    0090     73 _P1_0	=	0x0090
                    0091     74 _P1_1	=	0x0091
                    0092     75 _P1_2	=	0x0092
                    0093     76 _P1_3	=	0x0093
                    0094     77 _P1_4	=	0x0094
                    0095     78 _P1_5	=	0x0095
                    0096     79 _P1_6	=	0x0096
                    0097     80 _P1_7	=	0x0097
                    0098     81 _RI	=	0x0098
                    0099     82 _TI	=	0x0099
                    009A     83 _RB8	=	0x009a
                    009B     84 _TB8	=	0x009b
                    009C     85 _REN	=	0x009c
                    009D     86 _SM2	=	0x009d
                    009E     87 _SM1	=	0x009e
                    009F     88 _SM0	=	0x009f
                    00A0     89 _P2_0	=	0x00a0
                    00A1     90 _P2_1	=	0x00a1
                    00A2     91 _P2_2	=	0x00a2
                    00A3     92 _P2_3	=	0x00a3
                    00A4     93 _P2_4	=	0x00a4
                    00A5     94 _P2_5	=	0x00a5
                    00A6     95 _P2_6	=	0x00a6
                    00A7     96 _P2_7	=	0x00a7
                    00A8     97 _EX0	=	0x00a8
                    00A9     98 _ET0	=	0x00a9
                    00AA     99 _EX1	=	0x00aa
                    00AB    100 _ET1	=	0x00ab
                    00AC    101 _ES	=	0x00ac
                    00AF    102 _EA	=	0x00af
                    00B0    103 _P3_0	=	0x00b0
                    00B1    104 _P3_1	=	0x00b1
                    00B2    105 _P3_2	=	0x00b2
                    00B3    106 _P3_3	=	0x00b3
                    00B4    107 _P3_4	=	0x00b4
                    00B5    108 _P3_5	=	0x00b5
                    00B6    109 _P3_6	=	0x00b6
                    00B7    110 _P3_7	=	0x00b7
                    00B0    111 _RXD	=	0x00b0
                    00B1    112 _TXD	=	0x00b1
                    00B2    113 _INT0	=	0x00b2
                    00B3    114 _INT1	=	0x00b3
                    00B4    115 _T0	=	0x00b4
                    00B5    116 _T1	=	0x00b5
                    00B6    117 _WR	=	0x00b6
                    00B7    118 _RD	=	0x00b7
                    00B8    119 _PX0	=	0x00b8
                    00B9    120 _PT0	=	0x00b9
                    00BA    121 _PX1	=	0x00ba
                    00BB    122 _PT1	=	0x00bb
                    00BC    123 _PS	=	0x00bc
                    00D0    124 _P	=	0x00d0
                    00D1    125 _F1	=	0x00d1
                    00D2    126 _OV	=	0x00d2
                    00D3    127 _RS0	=	0x00d3
                    00D4    128 _RS1	=	0x00d4
                    00D5    129 _F0	=	0x00d5
                    00D6    130 _AC	=	0x00d6
                    00D7    131 _CY	=	0x00d7
                    00AD    132 _ET2	=	0x00ad
                    00C8    133 _T2CON_0	=	0x00c8
                    00C9    134 _T2CON_1	=	0x00c9
                    00CA    135 _T2CON_2	=	0x00ca
                    00CB    136 _T2CON_3	=	0x00cb
                    00CC    137 _T2CON_4	=	0x00cc
                    00CD    138 _T2CON_5	=	0x00cd
                    00CE    139 _T2CON_6	=	0x00ce
                    00CF    140 _T2CON_7	=	0x00cf
                    00C8    141 _CP_RL2	=	0x00c8
                    00C9    142 _C_T2	=	0x00c9
                    00CA    143 _TR2	=	0x00ca
                    00CB    144 _EXEN2	=	0x00cb
                    00CC    145 _TCLK	=	0x00cc
                    00CD    146 _RCLK	=	0x00cd
                    00CE    147 _EXF2	=	0x00ce
                    00CF    148 _TF2	=	0x00cf
                    00B5    149 _LED_SYS	=	0x00b5
                            150 ;--------------------------------------------------------
                            151 ; overlayable register banks 
                            152 ;--------------------------------------------------------
                            153 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     154 	.ds 8
                            155 	.area REG_BANK_2	(REL,OVR,DATA)
   0010                     156 	.ds 8
                            157 ;--------------------------------------------------------
                            158 ; internal ram data
                            159 ;--------------------------------------------------------
                            160 	.area DSEG    (DATA)
   0021                     161 _timer::
   0021                     162 	.ds 1
   0022                     163 _hi_flag::
   0022                     164 	.ds 1
   0023                     165 _a_data_byte::
   0023                     166 	.ds 1
                            167 ;--------------------------------------------------------
                            168 ; overlayable items in internal ram 
                            169 ;--------------------------------------------------------
                            170 	.area	OSEG    (OVR,DATA)
                            171 ;--------------------------------------------------------
                            172 ; Stack segment in internal ram 
                            173 ;--------------------------------------------------------
                            174 	.area	SSEG	(DATA)
   0024                     175 __start__stack:
   0024                     176 	.ds	1
                            177 
                            178 ;--------------------------------------------------------
                            179 ; indirectly addressable internal ram data
                            180 ;--------------------------------------------------------
                            181 	.area ISEG    (DATA)
   0080                     182 _a_idata_byte::
   0080                     183 	.ds 1
                            184 ;--------------------------------------------------------
                            185 ; bit data
                            186 ;--------------------------------------------------------
                            187 	.area BSEG    (BIT)
   0000                     188 _my_bit::
   0000                     189 	.ds 1
                            190 ;--------------------------------------------------------
                            191 ; external ram data
                            192 ;--------------------------------------------------------
                            193 	.area XSEG    (XDATA)
   0000                     194 _a_xdata_byte::
   0000                     195 	.ds 1
                    8000    196 _mem_mapped_hardware	=	0x8000
                            197 ;--------------------------------------------------------
                            198 ; external initialized ram data
                            199 ;--------------------------------------------------------
                            200 	.area XISEG   (XDATA)
                            201 ;--------------------------------------------------------
                            202 ; interrupt vector 
                            203 ;--------------------------------------------------------
                            204 	.area CSEG    (CODE)
   0000                     205 __interrupt_vect:
   0000 02 01 24            206 	ljmp	__sdcc_gsinit_startup
   0003 32                  207 	reti
   0004                     208 	.ds	7
   000B 02 00 38            209 	ljmp	_timer0_irq_proc
   000E                     210 	.ds	5
   0013 32                  211 	reti
   0014                     212 	.ds	7
   001B 32                  213 	reti
   001C                     214 	.ds	7
   0023 32                  215 	reti
   0024                     216 	.ds	7
   002B 32                  217 	reti
   002C                     218 	.ds	7
                            219 ;--------------------------------------------------------
                            220 ; global & static initialisations
                            221 ;--------------------------------------------------------
                            222 	.area GSINIT  (CODE)
                            223 	.area GSFINAL (CODE)
                            224 	.area GSINIT  (CODE)
   0124                     225 __sdcc_gsinit_startup:
   0124 75 81 24            226 	mov	sp,#__start__stack
   0127 12 00 F5            227 	lcall	__sdcc_external_startup
   012A E5 82               228 	mov	a,dpl
   012C 60 03               229 	jz	__sdcc_init_data
   012E 02 00 33            230 	ljmp	__sdcc_program_startup
   0131                     231 __sdcc_init_data:
                            232 ;	_mcs51_genXINIT() start
   0131 74 00               233 	mov	a,#l_XINIT
   0133 44 00               234 	orl	a,#l_XINIT>>8
   0135 60 29               235 	jz	00003$
   0137 74 63               236 	mov	a,#s_XINIT
   0139 24 00               237 	add	a,#l_XINIT
   013B F9                  238 	mov	r1,a
   013C 74 01               239 	mov	a,#s_XINIT>>8
   013E 34 00               240 	addc	a,#l_XINIT>>8
   0140 FA                  241 	mov	r2,a
   0141 90 01 63            242 	mov	dptr,#s_XINIT
   0144 78 01               243 	mov	r0,#s_XISEG
   0146 75 A0 00            244 	mov	p2,#(s_XISEG >> 8)
   0149 E4                  245 00001$:	clr	a
   014A 93                  246 	movc	a,@a+dptr
   014B F2                  247 	movx	@r0,a
   014C A3                  248 	inc	dptr
   014D 08                  249 	inc	r0
   014E B8 00 02            250 	cjne	r0,#0,00002$
   0151 05 A0               251 	inc	p2
   0153 E5 82               252 00002$:	mov	a,dpl
   0155 B5 01 F1            253 	cjne	a,ar1,00001$
   0158 E5 83               254 	mov	a,dph
   015A B5 02 EC            255 	cjne	a,ar2,00001$
   015D 75 A0 FF            256 	mov	p2,#0xFF
   0160                     257 00003$:
                            258 ;	_mcs51_genXINIT() end
                            259 	.area GSFINAL (CODE)
   0160 02 00 33            260 	ljmp	__sdcc_program_startup
                            261 ;--------------------------------------------------------
                            262 ; Home
                            263 ;--------------------------------------------------------
                            264 	.area HOME    (CODE)
                            265 	.area CSEG    (CODE)
                            266 ;--------------------------------------------------------
                            267 ; code
                            268 ;--------------------------------------------------------
                            269 	.area CSEG    (CODE)
   0033                     270 __sdcc_program_startup:
   0033 12 00 A2            271 	lcall	_main
                            272 ;	return from main will lock up
   0036 80 FE               273 	sjmp .
                            274 ;------------------------------------------------------------
                            275 ;Allocation info for local variables in function 'timer0_irq_proc'
                            276 ;------------------------------------------------------------
                            277 ;------------------------------------------------------------
                            278 ;	hi.c:52: void timer0_irq_proc(void) interrupt 1 using 2
                            279 ;	-----------------------------------------
                            280 ;	 function timer0_irq_proc
                            281 ;	-----------------------------------------
   0038                     282 _timer0_irq_proc:
                    0012    283 	ar2 = 0x12
                    0013    284 	ar3 = 0x13
                    0014    285 	ar4 = 0x14
                    0015    286 	ar5 = 0x15
                    0016    287 	ar6 = 0x16
                    0017    288 	ar7 = 0x17
                    0010    289 	ar0 = 0x10
                    0011    290 	ar1 = 0x11
   0038 C0 E0               291 	push	acc
   003A C0 F0               292 	push	b
   003C C0 82               293 	push	dpl
   003E C0 83               294 	push	dph
   0040 C0 D0               295 	push	psw
   0042 75 D0 10            296 	mov	psw,#0x10
                            297 ;	hi.c:54: if (timer != 0)
                            298 ;	genCmpEq
   0045 E5 21               299 	mov	a,_timer
                            300 ; Peephole 110   removed ljmp by inverse jump logic
   0047 60 04               301 	jz  00102$
   0049                     302 00107$:
                            303 ;	hi.c:56: --timer;
                            304 ;	genMinus
                            305 ;	genMinusDec
   0049 15 21               306 	dec	_timer
                            307 ; Peephole 132   changed ljmp to sjmp
   004B 80 06               308 	sjmp 00103$
   004D                     309 00102$:
                            310 ;	hi.c:60: hi_flag = 1;
                            311 ;	genAssign
   004D 75 22 01            312 	mov	_hi_flag,#0x01
                            313 ;	hi.c:61: timer = 250;
                            314 ;	genAssign
   0050 75 21 FA            315 	mov	_timer,#0xFA
   0053                     316 00103$:
                            317 ;	hi.c:64: TR0 = 0; /* Stop Timer 0 counting */
                            318 ;	genAssign
   0053 C2 8C               319 	clr	_TR0
                            320 ;	hi.c:65: TH0 = (~(5000)) >> 8;
                            321 ;	genAssign
   0055 75 8C EC            322 	mov	_TH0,#0xEC
                            323 ;	hi.c:66: TL0 = (~(5000)) & 0xff;
                            324 ;	genAssign
   0058 75 8A 77            325 	mov	_TL0,#0x77
                            326 ;	hi.c:67: TR0 = 1; /* Start counting again */
                            327 ;	genAssign
   005B D2 8C               328 	setb	_TR0
   005D                     329 00104$:
   005D D0 D0               330 	pop	psw
   005F D0 83               331 	pop	dph
   0061 D0 82               332 	pop	dpl
   0063 D0 F0               333 	pop	b
   0065 D0 E0               334 	pop	acc
   0067 32                  335 	reti
                            336 ;------------------------------------------------------------
                            337 ;Allocation info for local variables in function 'tx_char'
                            338 ;------------------------------------------------------------
                            339 ;------------------------------------------------------------
                            340 ;	hi.c:88: void tx_char(char c)
                            341 ;	-----------------------------------------
                            342 ;	 function tx_char
                            343 ;	-----------------------------------------
   0068                     344 _tx_char:
                    0002    345 	ar2 = 0x02
                    0003    346 	ar3 = 0x03
                    0004    347 	ar4 = 0x04
                    0005    348 	ar5 = 0x05
                    0006    349 	ar6 = 0x06
                    0007    350 	ar7 = 0x07
                    0000    351 	ar0 = 0x00
                    0001    352 	ar1 = 0x01
                            353 ;	genReceive
   0068 85 82 99            354 	mov	_SBUF,dpl
                            355 ;	hi.c:91: while (!TI)
   006B                     356 00101$:
                            357 ;	genIfx
                            358 ;	genIfxJump
                            359 ; Peephole 111   removed ljmp by inverse jump logic
   006B 30 99 FD            360 	jnb  _TI,00101$
   006E                     361 00108$:
                            362 ;	hi.c:93: TI = 0;
                            363 ;	genAssign
   006E C2 99               364 	clr	_TI
   0070                     365 00104$:
   0070 22                  366 	ret
                            367 ;------------------------------------------------------------
                            368 ;Allocation info for local variables in function 'tx_str'
                            369 ;------------------------------------------------------------
                            370 ;str                       Allocated to registers r2 r3 r4 
                            371 ;------------------------------------------------------------
                            372 ;	hi.c:99: void tx_str(char *str)
                            373 ;	-----------------------------------------
                            374 ;	 function tx_str
                            375 ;	-----------------------------------------
   0071                     376 _tx_str:
                            377 ;	genReceive
   0071 AA 82               378 	mov	r2,dpl
   0073 AB 83               379 	mov	r3,dph
   0075 AC F0               380 	mov	r4,b
                            381 ;	hi.c:102: while (*str)
                            382 ;	genAssign
   0077                     383 00101$:
                            384 ;	genPointerGet
                            385 ;	genGenPointerGet
   0077 8A 82               386 	mov	dpl,r2
   0079 8B 83               387 	mov	dph,r3
   007B 8C F0               388 	mov	b,r4
   007D 12 00 F9            389 	lcall	__gptrget
                            390 ;	genIfx
                            391 ; Peephole 105   removed redundant mov
   0080 FD                  392 	mov  r5,a
                            393 ;	genIfxJump
                            394 ; Peephole 110   removed ljmp by inverse jump logic
   0081 60 18               395 	jz  00104$
   0083                     396 00108$:
                            397 ;	hi.c:103: tx_char(*str++);
                            398 ;	genPlus
                            399 ;	genPlusIncr
   0083 0A                  400 	inc	r2
   0084 BA 00 01            401 	cjne	r2,#0x00,00109$
   0087 0B                  402 	inc	r3
   0088                     403 00109$:
                            404 ;	genCall
   0088 8D 82               405 	mov	dpl,r5
   008A C0 02               406 	push	ar2
   008C C0 03               407 	push	ar3
   008E C0 04               408 	push	ar4
   0090 12 00 68            409 	lcall	_tx_char
   0093 D0 04               410 	pop	ar4
   0095 D0 03               411 	pop	ar3
   0097 D0 02               412 	pop	ar2
                            413 ; Peephole 132   changed ljmp to sjmp
   0099 80 DC               414 	sjmp 00101$
   009B                     415 00104$:
   009B 22                  416 	ret
                            417 ;------------------------------------------------------------
                            418 ;Allocation info for local variables in function 'stop'
                            419 ;------------------------------------------------------------
                            420 ;------------------------------------------------------------
                            421 ;	hi.c:111: void stop(void)
                            422 ;	-----------------------------------------
                            423 ;	 function stop
                            424 ;	-----------------------------------------
   009C                     425 _stop:
                            426 ;	hi.c:117: _endasm;
                            427 ;	genInline
                            428 ;
   009C 90 FF FF            429 	  mov dptr, #65535;
   009F E0                  430 	  movx a, @dptr;
   00A0 00                  431 	  nop;
   00A1                     432 00101$:
   00A1 22                  433 	ret
                            434 ;------------------------------------------------------------
                            435 ;Allocation info for local variables in function 'main'
                            436 ;------------------------------------------------------------
                            437 ;------------------------------------------------------------
                            438 ;	hi.c:123: void main(void)
                            439 ;	-----------------------------------------
                            440 ;	 function main
                            441 ;	-----------------------------------------
   00A2                     442 _main:
                            443 ;	hi.c:125: PCON = 0x80;  /* power control byte, set SMOD bit for serial port */
                            444 ;	genAssign
   00A2 75 87 80            445 	mov	_PCON,#0x80
                            446 ;	hi.c:126: SCON = 0x50;  /* serial control byte, mode 1, RI active */
                            447 ;	genAssign
   00A5 75 98 50            448 	mov	_SCON,#0x50
                            449 ;	hi.c:127: TMOD = 0x21;  /* timer control mode, byte operation */
                            450 ;	genAssign
   00A8 75 89 21            451 	mov	_TMOD,#0x21
                            452 ;	hi.c:128: TCON = 0;     /* timer control register, byte operation */
                            453 ;	genAssign
   00AB 75 88 00            454 	mov	_TCON,#0x00
                            455 ;	hi.c:130: TH1 = 0xFA;   /* serial reload value, 9,600 baud at 11.0952Mhz */
                            456 ;	genAssign
   00AE 75 8D FA            457 	mov	_TH1,#0xFA
                            458 ;	hi.c:131: TR1 = 1;      /* start serial timer */
                            459 ;	genAssign
   00B1 D2 8E               460 	setb	_TR1
                            461 ;	hi.c:133: TR0 = 1;      /* start timer0 */
                            462 ;	genAssign
   00B3 D2 8C               463 	setb	_TR0
                            464 ;	hi.c:134: ET0 = 1;      /* Enable Timer 0 overflow interrupt IE.1 */
                            465 ;	genAssign
   00B5 D2 A9               466 	setb	_ET0
                            467 ;	hi.c:135: EA = 1;       /* Enable Interrupts */
                            468 ;	genAssign
   00B7 D2 AF               469 	setb	_EA
                            470 ;	hi.c:137: TI = 0;       /* clear this out */
                            471 ;	genAssign
   00B9 C2 99               472 	clr	_TI
                            473 ;	hi.c:138: SBUF = '.';   /* send an initial '.' out serial port */
                            474 ;	genAssign
   00BB 75 99 2E            475 	mov	_SBUF,#0x2E
                            476 ;	hi.c:139: hi_flag = 1;
                            477 ;	genAssign
   00BE 75 22 01            478 	mov	_hi_flag,#0x01
                            479 ;	hi.c:142: tx_str(my_message);
                            480 ;	genCall
                            481 ; Peephole 182a use 16 bit load of DPTR
   00C1 90 00 E0            482 	mov dptr,#_my_message
   00C4 75 F0 02            483 	mov	b,#0x02
   00C7 12 00 71            484 	lcall	_tx_str
   00CA                     485 00104$:
                            486 ;	hi.c:146: if (hi_flag)
                            487 ;	genIfx
   00CA E5 22               488 	mov	a,_hi_flag
                            489 ;	genIfxJump
                            490 ; Peephole 110   removed ljmp by inverse jump logic
   00CC 60 0C               491 	jz  00102$
   00CE                     492 00110$:
                            493 ;	hi.c:148: tx_str("Hi There\n");
                            494 ;	genCall
                            495 ; Peephole 182a use 16 bit load of DPTR
   00CE 90 00 EB            496 	mov dptr,#__str_0
   00D1 75 F0 02            497 	mov	b,#0x02
   00D4 12 00 71            498 	lcall	_tx_str
                            499 ;	hi.c:149: hi_flag = 0;
                            500 ;	genAssign
   00D7 75 22 00            501 	mov	_hi_flag,#0x00
   00DA                     502 00102$:
                            503 ;	hi.c:152: stop();
                            504 ;	genCall
   00DA 12 00 9C            505 	lcall	_stop
                            506 ; Peephole 132   changed ljmp to sjmp
   00DD 80 EB               507 	sjmp 00104$
   00DF                     508 00106$:
   00DF 22                  509 	ret
                            510 	.area CSEG    (CODE)
   00E0                     511 _my_message:
   00E0 47 4E 55 20 72 6F   512 	.ascii "GNU rocks"
        63 6B 73
   00E9 0A                  513 	.db 0x0A
   00EA 00                  514 	.db 0x00
   00EB                     515 __str_0:
   00EB 48 69 20 54 68 65   516 	.ascii "Hi There"
        72 65
   00F3 0A                  517 	.db 0x0A
   00F4 00                  518 	.db 0x00
                            519 	.area XINIT   (CODE)
