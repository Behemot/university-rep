                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.3.2 Fri Dec 20 19:26:24 2002
                              4 
                              5 ;--------------------------------------------------------
                              6 	.module hwinit
                              7 	
                              8 ;--------------------------------------------------------
                              9 ; Public variables in this module
                             10 ;--------------------------------------------------------
                             11 	.globl _putchar
                             12 	.globl _getchar
                             13 	.globl __sdcc_external_startup
                             14 	.globl _ClockIrqHandler
                             15 	.globl _ClockTicks
                             16 ;--------------------------------------------------------
                             17 ; special function registers
                             18 ;--------------------------------------------------------
                    0080     19 _P0	=	0x0080
                    0081     20 _SP	=	0x0081
                    0082     21 _DPL	=	0x0082
                    0083     22 _DPH	=	0x0083
                    0087     23 _PCON	=	0x0087
                    0088     24 _TCON	=	0x0088
                    0089     25 _TMOD	=	0x0089
                    008A     26 _TL0	=	0x008a
                    008B     27 _TL1	=	0x008b
                    008C     28 _TH0	=	0x008c
                    008D     29 _TH1	=	0x008d
                    0090     30 _P1	=	0x0090
                    0098     31 _SCON	=	0x0098
                    0099     32 _SBUF	=	0x0099
                    00A0     33 _P2	=	0x00a0
                    00A8     34 _IE	=	0x00a8
                    00B0     35 _P3	=	0x00b0
                    00B8     36 _IP	=	0x00b8
                    00D0     37 _PSW	=	0x00d0
                    00E0     38 _ACC	=	0x00e0
                    00F0     39 _B	=	0x00f0
                             40 ;--------------------------------------------------------
                             41 ; special function bits 
                             42 ;--------------------------------------------------------
                    0080     43 _P0_0	=	0x0080
                    0081     44 _P0_1	=	0x0081
                    0082     45 _P0_2	=	0x0082
                    0083     46 _P0_3	=	0x0083
                    0084     47 _P0_4	=	0x0084
                    0085     48 _P0_5	=	0x0085
                    0086     49 _P0_6	=	0x0086
                    0087     50 _P0_7	=	0x0087
                    0088     51 _IT0	=	0x0088
                    0089     52 _IE0	=	0x0089
                    008A     53 _IT1	=	0x008a
                    008B     54 _IE1	=	0x008b
                    008C     55 _TR0	=	0x008c
                    008D     56 _TF0	=	0x008d
                    008E     57 _TR1	=	0x008e
                    008F     58 _TF1	=	0x008f
                    0090     59 _P1_0	=	0x0090
                    0091     60 _P1_1	=	0x0091
                    0092     61 _P1_2	=	0x0092
                    0093     62 _P1_3	=	0x0093
                    0094     63 _P1_4	=	0x0094
                    0095     64 _P1_5	=	0x0095
                    0096     65 _P1_6	=	0x0096
                    0097     66 _P1_7	=	0x0097
                    0098     67 _RI	=	0x0098
                    0099     68 _TI	=	0x0099
                    009A     69 _RB8	=	0x009a
                    009B     70 _TB8	=	0x009b
                    009C     71 _REN	=	0x009c
                    009D     72 _SM2	=	0x009d
                    009E     73 _SM1	=	0x009e
                    009F     74 _SM0	=	0x009f
                    00A0     75 _P2_0	=	0x00a0
                    00A1     76 _P2_1	=	0x00a1
                    00A2     77 _P2_2	=	0x00a2
                    00A3     78 _P2_3	=	0x00a3
                    00A4     79 _P2_4	=	0x00a4
                    00A5     80 _P2_5	=	0x00a5
                    00A6     81 _P2_6	=	0x00a6
                    00A7     82 _P2_7	=	0x00a7
                    00A8     83 _EX0	=	0x00a8
                    00A9     84 _ET0	=	0x00a9
                    00AA     85 _EX1	=	0x00aa
                    00AB     86 _ET1	=	0x00ab
                    00AC     87 _ES	=	0x00ac
                    00AF     88 _EA	=	0x00af
                    00B0     89 _P3_0	=	0x00b0
                    00B1     90 _P3_1	=	0x00b1
                    00B2     91 _P3_2	=	0x00b2
                    00B3     92 _P3_3	=	0x00b3
                    00B4     93 _P3_4	=	0x00b4
                    00B5     94 _P3_5	=	0x00b5
                    00B6     95 _P3_6	=	0x00b6
                    00B7     96 _P3_7	=	0x00b7
                    00B0     97 _RXD	=	0x00b0
                    00B1     98 _TXD	=	0x00b1
                    00B2     99 _INT0	=	0x00b2
                    00B3    100 _INT1	=	0x00b3
                    00B4    101 _T0	=	0x00b4
                    00B5    102 _T1	=	0x00b5
                    00B6    103 _WR	=	0x00b6
                    00B7    104 _RD	=	0x00b7
                    00B8    105 _PX0	=	0x00b8
                    00B9    106 _PT0	=	0x00b9
                    00BA    107 _PX1	=	0x00ba
                    00BB    108 _PT1	=	0x00bb
                    00BC    109 _PS	=	0x00bc
                    00D0    110 _P	=	0x00d0
                    00D1    111 _F1	=	0x00d1
                    00D2    112 _OV	=	0x00d2
                    00D3    113 _RS0	=	0x00d3
                    00D4    114 _RS1	=	0x00d4
                    00D5    115 _F0	=	0x00d5
                    00D6    116 _AC	=	0x00d6
                    00D7    117 _CY	=	0x00d7
                            118 ;--------------------------------------------------------
                            119 ; overlayable register banks 
                            120 ;--------------------------------------------------------
                            121 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     122 	.ds 8
                            123 	.area REG_BANK_3	(REL,OVR,DATA)
   0018                     124 	.ds 8
                            125 ;--------------------------------------------------------
                            126 ; internal ram data
                            127 ;--------------------------------------------------------
                            128 	.area DSEG    (DATA)
   0028                     129 _milliSeconds:
   0028                     130 	.ds 4
                            131 ;--------------------------------------------------------
                            132 ; overlayable items in internal ram 
                            133 ;--------------------------------------------------------
                            134 	.area	OSEG    (OVR,DATA)
                            135 	.area	OSEG    (OVR,DATA)
                            136 ;--------------------------------------------------------
                            137 ; indirectly addressable internal ram data
                            138 ;--------------------------------------------------------
                            139 	.area ISEG    (DATA)
                            140 ;--------------------------------------------------------
                            141 ; bit data
                            142 ;--------------------------------------------------------
                            143 	.area BSEG    (BIT)
                            144 ;--------------------------------------------------------
                            145 ; external ram data
                            146 ;--------------------------------------------------------
                            147 	.area XSEG    (XDATA)
                            148 ;--------------------------------------------------------
                            149 ; external initialized ram data
                            150 ;--------------------------------------------------------
                            151 	.area XISEG   (XDATA)
                            152 ;--------------------------------------------------------
                            153 ; global & static initialisations
                            154 ;--------------------------------------------------------
                            155 	.area GSINIT  (CODE)
                            156 	.area GSFINAL (CODE)
                            157 	.area GSINIT  (CODE)
                            158 ;--------------------------------------------------------
                            159 ; Home
                            160 ;--------------------------------------------------------
                            161 	.area HOME    (CODE)
                            162 	.area CSEG    (CODE)
                            163 ;--------------------------------------------------------
                            164 ; code
                            165 ;--------------------------------------------------------
                            166 	.area CSEG    (CODE)
                            167 ;------------------------------------------------------------
                            168 ;Allocation info for local variables in function 'ClockIrqHandler'
                            169 ;------------------------------------------------------------
                            170 ;------------------------------------------------------------
                            171 ;	hwinit.c:13: void ClockIrqHandler (void) interrupt 1 using 3 {
                            172 ;	-----------------------------------------
                            173 ;	 function ClockIrqHandler
                            174 ;	-----------------------------------------
   01F1                     175 _ClockIrqHandler:
                    001A    176 	ar2 = 0x1a
                    001B    177 	ar3 = 0x1b
                    001C    178 	ar4 = 0x1c
                    001D    179 	ar5 = 0x1d
                    001E    180 	ar6 = 0x1e
                    001F    181 	ar7 = 0x1f
                    0018    182 	ar0 = 0x18
                    0019    183 	ar1 = 0x19
   01F1 C0 E0               184 	push	acc
   01F3 C0 F0               185 	push	b
   01F5 C0 82               186 	push	dpl
   01F7 C0 83               187 	push	dph
   01F9 C0 D0               188 	push	psw
   01FB 75 D0 18            189 	mov	psw,#0x18
                            190 ;	hwinit.c:14: TL0 = TIMER0_RELOAD_VALUE&0xff;
                            191 ;	genAssign
   01FE 75 8A 67            192 	mov	_TL0,#0x67
                            193 ;	hwinit.c:15: TH0 = TIMER0_RELOAD_VALUE>>8;
                            194 ;	genAssign
   0201 75 8C FC            195 	mov	_TH0,#0xFC
                            196 ;	hwinit.c:16: milliSeconds++;
                            197 ;	genPlus
                            198 ;	genPlusIncr
   0204 74 01               199 	mov	a,#0x01
   0206 25 28               200 	add	a,_milliSeconds
   0208 F5 28               201 	mov	_milliSeconds,a
                            202 ; Peephole 180   changed mov to clr
   020A E4                  203 	clr  a
   020B 35 29               204 	addc	a,(_milliSeconds + 1)
   020D F5 29               205 	mov	(_milliSeconds + 1),a
                            206 ; Peephole 180   changed mov to clr
   020F E4                  207 	clr  a
   0210 35 2A               208 	addc	a,(_milliSeconds + 2)
   0212 F5 2A               209 	mov	(_milliSeconds + 2),a
                            210 ; Peephole 180   changed mov to clr
   0214 E4                  211 	clr  a
   0215 35 2B               212 	addc	a,(_milliSeconds + 3)
   0217 F5 2B               213 	mov	(_milliSeconds + 3),a
   0219                     214 00101$:
   0219 D0 D0               215 	pop	psw
   021B D0 83               216 	pop	dph
   021D D0 82               217 	pop	dpl
   021F D0 F0               218 	pop	b
   0221 D0 E0               219 	pop	acc
   0223 32                  220 	reti
                            221 ;------------------------------------------------------------
                            222 ;Allocation info for local variables in function 'ClockTicks'
                            223 ;------------------------------------------------------------
                            224 ;------------------------------------------------------------
                            225 ;	hwinit.c:20: unsigned long ClockTicks(void) {
                            226 ;	-----------------------------------------
                            227 ;	 function ClockTicks
                            228 ;	-----------------------------------------
   0224                     229 _ClockTicks:
                    0002    230 	ar2 = 0x02
                    0003    231 	ar3 = 0x03
                    0004    232 	ar4 = 0x04
                    0005    233 	ar5 = 0x05
                    0006    234 	ar6 = 0x06
                    0007    235 	ar7 = 0x07
                    0000    236 	ar0 = 0x00
                    0001    237 	ar1 = 0x01
                            238 ;	hwinit.c:22: ET0=0;
                            239 ;	genAssign
   0224 C2 A9               240 	clr	_ET0
                            241 ;	hwinit.c:23: ms=milliSeconds;
                            242 ;	genAssign
   0226 AA 28               243 	mov	r2,_milliSeconds
   0228 AB 29               244 	mov	r3,(_milliSeconds + 1)
   022A AC 2A               245 	mov	r4,(_milliSeconds + 2)
   022C AD 2B               246 	mov	r5,(_milliSeconds + 3)
                            247 ;	hwinit.c:24: ET0=1;
                            248 ;	genAssign
   022E D2 A9               249 	setb	_ET0
                            250 ;	hwinit.c:25: return ms;
                            251 ;	genRet
   0230 8A 82               252 	mov	dpl,r2
   0232 8B 83               253 	mov	dph,r3
   0234 8C F0               254 	mov	b,r4
   0236 ED                  255 	mov	a,r5
   0237                     256 00101$:
   0237 22                  257 	ret
                            258 ;------------------------------------------------------------
                            259 ;Allocation info for local variables in function '_sdcc_external_startup'
                            260 ;------------------------------------------------------------
                            261 ;------------------------------------------------------------
                            262 ;	hwinit.c:28: unsigned char _sdcc_external_startup() {
                            263 ;	-----------------------------------------
                            264 ;	 function _sdcc_external_startup
                            265 ;	-----------------------------------------
   0238                     266 __sdcc_external_startup:
                            267 ;	hwinit.c:30: TR0=0; // stop timer 0
                            268 ;	genAssign
   0238 C2 8C               269 	clr	_TR0
                            270 ;	hwinit.c:31: TMOD =(TMOD&0xf0)|0x01; // T0=16bit timer
                            271 ;	genAnd
   023A 74 F0               272 	mov	a,#0xF0
   023C 55 89               273 	anl	a,_TMOD
                            274 ;	genOr
   023E 44 01               275 	orl	a,#0x01
   0240 F5 89               276 	mov	_TMOD,a
                            277 ;	hwinit.c:33: TL0 = -TIMER0_RELOAD_VALUE&0xff;
                            278 ;	genAssign
   0242 75 8A 99            279 	mov	_TL0,#0x99
                            280 ;	hwinit.c:34: TH0 = -TIMER0_RELOAD_VALUE>>8;
                            281 ;	genAssign
   0245 75 8C 03            282 	mov	_TH0,#0x03
                            283 ;	hwinit.c:35: milliSeconds=0; // reset system time
                            284 ;	genAssign
   0248 E4                  285 	clr	a
   0249 F5 2B               286 	mov	(_milliSeconds + 3),a
   024B F5 2A               287 	mov	(_milliSeconds + 2),a
   024D F5 29               288 	mov	(_milliSeconds + 1),a
   024F F5 28               289 	mov	_milliSeconds,a
                            290 ;	hwinit.c:36: TR0=1; // start timer 0
                            291 ;	genAssign
   0251 D2 8C               292 	setb	_TR0
                            293 ;	hwinit.c:37: ET0=1; // enable timer 0 interrupt
                            294 ;	genAssign
   0253 D2 A9               295 	setb	_ET0
                            296 ;	hwinit.c:40: TR1=0; // stop timer 1
                            297 ;	genAssign
   0255 C2 8E               298 	clr	_TR1
                            299 ;	hwinit.c:41: TMOD = (TMOD&0x0f)|0x20; // T1=8bit autoreload timer
                            300 ;	genAnd
   0257 74 0F               301 	mov	a,#0x0F
   0259 55 89               302 	anl	a,_TMOD
                            303 ;	genOr
   025B 44 20               304 	orl	a,#0x20
   025D F5 89               305 	mov	_TMOD,a
                            306 ;	hwinit.c:43: PCON |= 0x80; // SMOD=1: double baudrate
                            307 ;	genOr
   025F 43 87 80            308 	orl	_PCON,#0x80
                            309 ;	hwinit.c:44: TH1=TL1=TIMER1_RELOAD_VALUE;
                            310 ;	genAssign
   0262 75 8B FD            311 	mov	_TL1,#0xFD
                            312 ;	genAssign
   0265 75 8D FD            313 	mov	_TH1,#0xFD
                            314 ;	hwinit.c:45: TR1=1; // start timer 1
                            315 ;	genAssign
   0268 D2 8E               316 	setb	_TR1
                            317 ;	hwinit.c:48: SCON=0x52; // mode 1, ren, txrdy, rxempty
                            318 ;	genAssign
   026A 75 98 52            319 	mov	_SCON,#0x52
                            320 ;	hwinit.c:50: EA=1; // enable global interrupt
                            321 ;	genAssign
   026D D2 AF               322 	setb	_EA
                            323 ;	hwinit.c:52: return 0;
                            324 ;	genRet
   026F 75 82 00            325 	mov	dpl,#0x00
   0272                     326 00101$:
   0272 22                  327 	ret
                            328 ;------------------------------------------------------------
                            329 ;Allocation info for local variables in function 'getchar'
                            330 ;------------------------------------------------------------
                            331 ;------------------------------------------------------------
                            332 ;	hwinit.c:55: char getchar() {
                            333 ;	-----------------------------------------
                            334 ;	 function getchar
                            335 ;	-----------------------------------------
   0273                     336 _getchar:
                            337 ;	hwinit.c:57: while (!RI)
   0273                     338 00101$:
                            339 ;	genIfx
                            340 ;	genIfxJump
                            341 ; Peephole 111   removed ljmp by inverse jump logic
   0273 30 98 FD            342 	jnb  _RI,00101$
   0276                     343 00108$:
                            344 ;	hwinit.c:59: RI=0;
                            345 ;	genAssign
   0276 C2 98               346 	clr	_RI
                            347 ;	hwinit.c:60: c=SBUF;
                            348 ;	genAssign
   0278 85 99 82            349 	mov	dpl,_SBUF
                            350 ;	hwinit.c:61: return c;
                            351 ;	genRet
   027B                     352 00104$:
   027B 22                  353 	ret
                            354 ;------------------------------------------------------------
                            355 ;Allocation info for local variables in function 'putchar'
                            356 ;------------------------------------------------------------
                            357 ;c                         Allocated to registers r2 
                            358 ;------------------------------------------------------------
                            359 ;	hwinit.c:64: void putchar(char c) {
                            360 ;	-----------------------------------------
                            361 ;	 function putchar
                            362 ;	-----------------------------------------
   027C                     363 _putchar:
                            364 ;	genReceive
   027C AA 82               365 	mov	r2,dpl
                            366 ;	hwinit.c:65: while (!TI)
   027E                     367 00101$:
                            368 ;	genIfx
                            369 ;	genIfxJump
                            370 ; Peephole 111   removed ljmp by inverse jump logic
   027E 30 99 FD            371 	jnb  _TI,00101$
   0281                     372 00111$:
                            373 ;	hwinit.c:67: TI=0;
                            374 ;	genAssign
   0281 C2 99               375 	clr	_TI
                            376 ;	hwinit.c:68: SBUF=c;
                            377 ;	genAssign
   0283 8A 99               378 	mov	_SBUF,r2
                            379 ;	hwinit.c:69: if (c=='\n') {
                            380 ;	genCmpEq
                            381 ; Peephole 132   changed ljmp to sjmp
                            382 ; Peephole 199   optimized misc jump sequence
   0285 BA 0A 06            383 	cjne r2,#0x0A,00106$
                            384 ;00112$:
                            385 ; Peephole 200   removed redundant sjmp
   0288                     386 00113$:
                            387 ;	hwinit.c:70: putchar('\r');
                            388 ;	genCall
   0288 75 82 0D            389 	mov	dpl,#0x0D
   028B 12 02 7C            390 	lcall	_putchar
   028E                     391 00106$:
   028E 22                  392 	ret
                            393 	.area CSEG    (CODE)
                            394 	.area XINIT   (CODE)
