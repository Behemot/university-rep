                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.3.2 Fri Dec 20 19:26:24 2002
                              4 
                              5 ;--------------------------------------------------------
                              6 	.module clock
                              7 	
                              8 ;--------------------------------------------------------
                              9 ; Public variables in this module
                             10 ;--------------------------------------------------------
                             11 	.globl _main
                             12 ;--------------------------------------------------------
                             13 ; special function registers
                             14 ;--------------------------------------------------------
                    0080     15 _P0	=	0x0080
                    0081     16 _SP	=	0x0081
                    0082     17 _DPL	=	0x0082
                    0083     18 _DPH	=	0x0083
                    0087     19 _PCON	=	0x0087
                    0088     20 _TCON	=	0x0088
                    0089     21 _TMOD	=	0x0089
                    008A     22 _TL0	=	0x008a
                    008B     23 _TL1	=	0x008b
                    008C     24 _TH0	=	0x008c
                    008D     25 _TH1	=	0x008d
                    0090     26 _P1	=	0x0090
                    0098     27 _SCON	=	0x0098
                    0099     28 _SBUF	=	0x0099
                    00A0     29 _P2	=	0x00a0
                    00A8     30 _IE	=	0x00a8
                    00B0     31 _P3	=	0x00b0
                    00B8     32 _IP	=	0x00b8
                    00D0     33 _PSW	=	0x00d0
                    00E0     34 _ACC	=	0x00e0
                    00F0     35 _B	=	0x00f0
                             36 ;--------------------------------------------------------
                             37 ; special function bits 
                             38 ;--------------------------------------------------------
                    0080     39 _P0_0	=	0x0080
                    0081     40 _P0_1	=	0x0081
                    0082     41 _P0_2	=	0x0082
                    0083     42 _P0_3	=	0x0083
                    0084     43 _P0_4	=	0x0084
                    0085     44 _P0_5	=	0x0085
                    0086     45 _P0_6	=	0x0086
                    0087     46 _P0_7	=	0x0087
                    0088     47 _IT0	=	0x0088
                    0089     48 _IE0	=	0x0089
                    008A     49 _IT1	=	0x008a
                    008B     50 _IE1	=	0x008b
                    008C     51 _TR0	=	0x008c
                    008D     52 _TF0	=	0x008d
                    008E     53 _TR1	=	0x008e
                    008F     54 _TF1	=	0x008f
                    0090     55 _P1_0	=	0x0090
                    0091     56 _P1_1	=	0x0091
                    0092     57 _P1_2	=	0x0092
                    0093     58 _P1_3	=	0x0093
                    0094     59 _P1_4	=	0x0094
                    0095     60 _P1_5	=	0x0095
                    0096     61 _P1_6	=	0x0096
                    0097     62 _P1_7	=	0x0097
                    0098     63 _RI	=	0x0098
                    0099     64 _TI	=	0x0099
                    009A     65 _RB8	=	0x009a
                    009B     66 _TB8	=	0x009b
                    009C     67 _REN	=	0x009c
                    009D     68 _SM2	=	0x009d
                    009E     69 _SM1	=	0x009e
                    009F     70 _SM0	=	0x009f
                    00A0     71 _P2_0	=	0x00a0
                    00A1     72 _P2_1	=	0x00a1
                    00A2     73 _P2_2	=	0x00a2
                    00A3     74 _P2_3	=	0x00a3
                    00A4     75 _P2_4	=	0x00a4
                    00A5     76 _P2_5	=	0x00a5
                    00A6     77 _P2_6	=	0x00a6
                    00A7     78 _P2_7	=	0x00a7
                    00A8     79 _EX0	=	0x00a8
                    00A9     80 _ET0	=	0x00a9
                    00AA     81 _EX1	=	0x00aa
                    00AB     82 _ET1	=	0x00ab
                    00AC     83 _ES	=	0x00ac
                    00AF     84 _EA	=	0x00af
                    00B0     85 _P3_0	=	0x00b0
                    00B1     86 _P3_1	=	0x00b1
                    00B2     87 _P3_2	=	0x00b2
                    00B3     88 _P3_3	=	0x00b3
                    00B4     89 _P3_4	=	0x00b4
                    00B5     90 _P3_5	=	0x00b5
                    00B6     91 _P3_6	=	0x00b6
                    00B7     92 _P3_7	=	0x00b7
                    00B0     93 _RXD	=	0x00b0
                    00B1     94 _TXD	=	0x00b1
                    00B2     95 _INT0	=	0x00b2
                    00B3     96 _INT1	=	0x00b3
                    00B4     97 _T0	=	0x00b4
                    00B5     98 _T1	=	0x00b5
                    00B6     99 _WR	=	0x00b6
                    00B7    100 _RD	=	0x00b7
                    00B8    101 _PX0	=	0x00b8
                    00B9    102 _PT0	=	0x00b9
                    00BA    103 _PX1	=	0x00ba
                    00BB    104 _PT1	=	0x00bb
                    00BC    105 _PS	=	0x00bc
                    00D0    106 _P	=	0x00d0
                    00D1    107 _F1	=	0x00d1
                    00D2    108 _OV	=	0x00d2
                    00D3    109 _RS0	=	0x00d3
                    00D4    110 _RS1	=	0x00d4
                    00D5    111 _F0	=	0x00d5
                    00D6    112 _AC	=	0x00d6
                    00D7    113 _CY	=	0x00d7
                            114 ;--------------------------------------------------------
                            115 ; overlayable register banks 
                            116 ;--------------------------------------------------------
                            117 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     118 	.ds 8
                            119 	.area REG_BANK_3	(REL,OVR,DATA)
   0018                     120 	.ds 8
                            121 ;--------------------------------------------------------
                            122 ; internal ram data
                            123 ;--------------------------------------------------------
                            124 	.area DSEG    (DATA)
   0022                     125 _main_ms_1_1::
   0022                     126 	.ds 4
   0026                     127 _main_oldseconds_1_1::
   0026                     128 	.ds 2
                            129 ;--------------------------------------------------------
                            130 ; overlayable items in internal ram 
                            131 ;--------------------------------------------------------
                            132 	.area OSEG    (OVR,DATA)
                            133 ;--------------------------------------------------------
                            134 ; Stack segment in internal ram 
                            135 ;--------------------------------------------------------
                            136 	.area	SSEG	(DATA)
   0043                     137 __start__stack:
   0043                     138 	.ds	1
                            139 
                            140 ;--------------------------------------------------------
                            141 ; indirectly addressable internal ram data
                            142 ;--------------------------------------------------------
                            143 	.area ISEG    (DATA)
                            144 ;--------------------------------------------------------
                            145 ; bit data
                            146 ;--------------------------------------------------------
                            147 	.area BSEG    (BIT)
                            148 ;--------------------------------------------------------
                            149 ; external ram data
                            150 ;--------------------------------------------------------
                            151 	.area XSEG    (XDATA)
                            152 ;--------------------------------------------------------
                            153 ; external initialized ram data
                            154 ;--------------------------------------------------------
                            155 	.area XISEG   (XDATA)
                            156 ;--------------------------------------------------------
                            157 ; interrupt vector 
                            158 ;--------------------------------------------------------
                            159 	.area CSEG    (CODE)
   0000                     160 __interrupt_vect:
   0000 02 0B 8A            161 	ljmp	__sdcc_gsinit_startup
   0003 32                  162 	reti
   0004                     163 	.ds	7
   000B 02 01 F1            164 	ljmp	_ClockIrqHandler
   000E                     165 	.ds	5
   0013 32                  166 	reti
   0014                     167 	.ds	7
   001B 32                  168 	reti
   001C                     169 	.ds	7
   0023 32                  170 	reti
   0024                     171 	.ds	7
   002B 32                  172 	reti
   002C                     173 	.ds	7
                            174 ;--------------------------------------------------------
                            175 ; global & static initialisations
                            176 ;--------------------------------------------------------
                            177 	.area GSINIT  (CODE)
                            178 	.area GSFINAL (CODE)
                            179 	.area GSINIT  (CODE)
   0B8A                     180 __sdcc_gsinit_startup:
   0B8A 75 81 43            181 	mov	sp,#__start__stack
   0B8D 12 02 38            182 	lcall	__sdcc_external_startup
   0B90 E5 82               183 	mov	a,dpl
   0B92 60 03               184 	jz	__sdcc_init_data
   0B94 02 00 33            185 	ljmp	__sdcc_program_startup
   0B97                     186 __sdcc_init_data:
                            187 ;	_mcs51_genXINIT() start
   0B97 74 00               188 	mov	a,#l_XINIT
   0B99 44 00               189 	orl	a,#l_XINIT>>8
   0B9B 60 29               190 	jz	00003$
   0B9D 74 C9               191 	mov	a,#s_XINIT
   0B9F 24 00               192 	add	a,#l_XINIT
   0BA1 F9                  193 	mov	r1,a
   0BA2 74 0B               194 	mov	a,#s_XINIT>>8
   0BA4 34 00               195 	addc	a,#l_XINIT>>8
   0BA6 FA                  196 	mov	r2,a
   0BA7 90 0B C9            197 	mov	dptr,#s_XINIT
   0BAA 78 00               198 	mov	r0,#s_XISEG
   0BAC 75 A0 40            199 	mov	p2,#(s_XISEG >> 8)
   0BAF E4                  200 00001$:	clr	a
   0BB0 93                  201 	movc	a,@a+dptr
   0BB1 F2                  202 	movx	@r0,a
   0BB2 A3                  203 	inc	dptr
   0BB3 08                  204 	inc	r0
   0BB4 B8 00 02            205 	cjne	r0,#0,00002$
   0BB7 05 A0               206 	inc	p2
   0BB9 E5 82               207 00002$:	mov	a,dpl
   0BBB B5 01 F1            208 	cjne	a,ar1,00001$
   0BBE E5 83               209 	mov	a,dph
   0BC0 B5 02 EC            210 	cjne	a,ar2,00001$
   0BC3 75 A0 FF            211 	mov	p2,#0xFF
   0BC6                     212 00003$:
                            213 ;	_mcs51_genXINIT() end
                            214 	.area GSFINAL (CODE)
   0BC6 02 00 33            215 	ljmp	__sdcc_program_startup
                            216 ;--------------------------------------------------------
                            217 ; Home
                            218 ;--------------------------------------------------------
                            219 	.area HOME    (CODE)
                            220 	.area CSEG    (CODE)
                            221 ;--------------------------------------------------------
                            222 ; code
                            223 ;--------------------------------------------------------
                            224 	.area CSEG    (CODE)
   0033                     225 __sdcc_program_startup:
   0033 12 00 38            226 	lcall	_main
                            227 ;	return from main will lock up
   0036 80 FE               228 	sjmp .
                            229 ;------------------------------------------------------------
                            230 ;Allocation info for local variables in function 'main'
                            231 ;------------------------------------------------------------
                            232 ;ms                        Allocated to in memory with name '_main_ms_1_1'
                            233 ;seconds                   Allocated to registers r2 r3 
                            234 ;oldseconds                Allocated to in memory with name '_main_oldseconds_1_1'
                            235 ;------------------------------------------------------------
                            236 ;	clock.c:6: void main(void) {
                            237 ;	-----------------------------------------
                            238 ;	 function main
                            239 ;	-----------------------------------------
   0038                     240 _main:
                    0002    241 	ar2 = 0x02
                    0003    242 	ar3 = 0x03
                    0004    243 	ar4 = 0x04
                    0005    244 	ar5 = 0x05
                    0006    245 	ar6 = 0x06
                    0007    246 	ar7 = 0x07
                    0000    247 	ar0 = 0x00
                    0001    248 	ar1 = 0x01
                            249 ;	clock.c:8: unsigned int seconds, oldseconds=ClockTicks()/1000;
                            250 ;	genCall
   0038 12 02 24            251 	lcall	_ClockTicks
   003B AA 82               252 	mov	r2,dpl
   003D AB 83               253 	mov	r3,dph
   003F AC F0               254 	mov	r4,b
   0041 FD                  255 	mov	r5,a
                            256 ;	genAssign
   0042 75 3F E8            257 	mov	__divulong_PARM_2,#0xE8
   0045 75 40 03            258 	mov	(__divulong_PARM_2 + 1),#0x03
   0048 75 41 00            259 	mov	(__divulong_PARM_2 + 2),#0x00
   004B 75 42 00            260 	mov	(__divulong_PARM_2 + 3),#0x00
                            261 ;	genCall
   004E 8A 82               262 	mov	dpl,r2
   0050 8B 83               263 	mov	dph,r3
   0052 8C F0               264 	mov	b,r4
   0054 ED                  265 	mov	a,r5
   0055 12 02 B8            266 	lcall	__divulong
   0058 AA 82               267 	mov	r2,dpl
   005A AB 83               268 	mov	r3,dph
   005C AC F0               269 	mov	r4,b
   005E FD                  270 	mov	r5,a
                            271 ;	genCast
   005F 8A 26               272 	mov	_main_oldseconds_1_1,r2
   0061 8B 27               273 	mov	(_main_oldseconds_1_1 + 1),r3
                            274 ;	clock.c:10: printf ("Example using the core timer to generate seconds.\n");
                            275 ;	genIpush
   0063 74 A4               276 	mov	a,#__str_0
   0065 C0 E0               277 	push	acc
   0067 74 01               278 	mov	a,#(__str_0 >> 8)
   0069 C0 E0               279 	push	acc
   006B 74 02               280 	mov	a,#0x02
   006D C0 E0               281 	push	acc
                            282 ;	genCall
   006F 12 03 6A            283 	lcall	_printf
   0072 15 81               284 	dec	sp
   0074 15 81               285 	dec	sp
   0076 15 81               286 	dec	sp
                            287 ;	clock.c:12: while (1) {
   0078                     288 00106$:
                            289 ;	clock.c:13: ms=ClockTicks();
                            290 ;	genCall
   0078 12 02 24            291 	lcall	_ClockTicks
   007B AC 82               292 	mov	r4,dpl
   007D AD 83               293 	mov	r5,dph
   007F AE F0               294 	mov	r6,b
   0081 FF                  295 	mov	r7,a
                            296 ;	genAssign
   0082 8C 22               297 	mov	_main_ms_1_1,r4
   0084 8D 23               298 	mov	(_main_ms_1_1 + 1),r5
   0086 8E 24               299 	mov	(_main_ms_1_1 + 2),r6
   0088 8F 25               300 	mov	(_main_ms_1_1 + 3),r7
                            301 ;	clock.c:14: seconds=ms/1000;
                            302 ;	genAssign
   008A 75 3F E8            303 	mov	__divulong_PARM_2,#0xE8
   008D 75 40 03            304 	mov	(__divulong_PARM_2 + 1),#0x03
   0090 75 41 00            305 	mov	(__divulong_PARM_2 + 2),#0x00
   0093 75 42 00            306 	mov	(__divulong_PARM_2 + 3),#0x00
                            307 ;	genCall
   0096 85 22 82            308 	mov	dpl,_main_ms_1_1
   0099 85 23 83            309 	mov	dph,(_main_ms_1_1 + 1)
   009C 85 24 F0            310 	mov	b,(_main_ms_1_1 + 2)
   009F E5 25               311 	mov	a,(_main_ms_1_1 + 3)
   00A1 12 02 B8            312 	lcall	__divulong
   00A4 A8 82               313 	mov	r0,dpl
   00A6 A9 83               314 	mov	r1,dph
   00A8 AA F0               315 	mov	r2,b
   00AA FB                  316 	mov	r3,a
                            317 ;	genCast
   00AB 88 02               318 	mov	ar2,r0
   00AD 89 03               319 	mov	ar3,r1
                            320 ;	clock.c:15: if (oldseconds!=seconds) {
                            321 ;	genCmpEq
   00AF E5 26               322 	mov	a,_main_oldseconds_1_1
   00B1 B5 02 08            323 	cjne	a,ar2,00113$
   00B4 E5 27               324 	mov	a,(_main_oldseconds_1_1 + 1)
   00B6 B5 03 03            325 	cjne	a,ar3,00113$
   00B9 02 01 6D            326 	ljmp	00102$
   00BC                     327 00113$:
                            328 ;	clock.c:16: oldseconds=seconds;
                            329 ;	genAssign
   00BC 8A 26               330 	mov	_main_oldseconds_1_1,r2
   00BE 8B 27               331 	mov	(_main_oldseconds_1_1 + 1),r3
                            332 ;	clock.c:19: (int)seconds%60, ms);
                            333 ;	genAssign
   00C0 8A 00               334 	mov	ar0,r2
   00C2 8B 01               335 	mov	ar1,r3
                            336 ;	genAssign
   00C4 E4                  337 	clr	a
   00C5 F5 40               338 	mov	(__modsint_PARM_2 + 1),a
   00C7 75 3F 3C            339 	mov	__modsint_PARM_2,#0x3C
                            340 ;	clock.c:18: (int)seconds/3600, (int)(seconds/60)%60, 
                            341 ;	genCall
   00CA 88 82               342 	mov	dpl,r0
   00CC 89 83               343 	mov	dph,r1
   00CE C0 02               344 	push	ar2
   00D0 C0 03               345 	push	ar3
   00D2 12 0A 64            346 	lcall	__modsint
   00D5 A8 82               347 	mov	r0,dpl
   00D7 A9 83               348 	mov	r1,dph
   00D9 D0 03               349 	pop	ar3
   00DB D0 02               350 	pop	ar2
                            351 ;	genAssign
   00DD E4                  352 	clr	a
   00DE F5 40               353 	mov	(__divuint_PARM_2 + 1),a
   00E0 75 3F 3C            354 	mov	__divuint_PARM_2,#0x3C
                            355 ;	genCall
   00E3 8A 82               356 	mov	dpl,r2
   00E5 8B 83               357 	mov	dph,r3
   00E7 C0 02               358 	push	ar2
   00E9 C0 03               359 	push	ar3
   00EB C0 00               360 	push	ar0
   00ED C0 01               361 	push	ar1
   00EF 12 02 8F            362 	lcall	__divuint
   00F2 E5 82               363 	mov	a,dpl
   00F4 85 83 F0            364 	mov	b,dph
   00F7 D0 01               365 	pop	ar1
   00F9 D0 00               366 	pop	ar0
   00FB D0 03               367 	pop	ar3
   00FD D0 02               368 	pop	ar2
                            369 ;	genAssign
   00FF FC                  370 	mov	r4,a
   0100 AD F0               371 	mov	r5,b
                            372 ;	genAssign
   0102 E4                  373 	clr	a
   0103 F5 40               374 	mov	(__modsint_PARM_2 + 1),a
   0105 75 3F 3C            375 	mov	__modsint_PARM_2,#0x3C
                            376 ;	genCall
   0108 8C 82               377 	mov	dpl,r4
   010A 8D 83               378 	mov	dph,r5
   010C C0 02               379 	push	ar2
   010E C0 03               380 	push	ar3
   0110 C0 00               381 	push	ar0
   0112 C0 01               382 	push	ar1
   0114 12 0A 64            383 	lcall	__modsint
   0117 AC 82               384 	mov	r4,dpl
   0119 AD 83               385 	mov	r5,dph
   011B D0 01               386 	pop	ar1
   011D D0 00               387 	pop	ar0
   011F D0 03               388 	pop	ar3
   0121 D0 02               389 	pop	ar2
                            390 ;	genAssign
                            391 ;	genAssign
   0123 75 3F 10            392 	mov	__divsint_PARM_2,#0x10
   0126 75 40 0E            393 	mov	(__divsint_PARM_2 + 1),#0x0E
                            394 ;	clock.c:17: printf ("%02d:%02d.%02d %ld\n", 
                            395 ;	genCall
   0129 8A 82               396 	mov	dpl,r2
   012B 8B 83               397 	mov	dph,r3
   012D C0 04               398 	push	ar4
   012F C0 05               399 	push	ar5
   0131 C0 00               400 	push	ar0
   0133 C0 01               401 	push	ar1
   0135 12 0A 9C            402 	lcall	__divsint
   0138 AA 82               403 	mov	r2,dpl
   013A AB 83               404 	mov	r3,dph
   013C D0 01               405 	pop	ar1
   013E D0 00               406 	pop	ar0
   0140 D0 05               407 	pop	ar5
   0142 D0 04               408 	pop	ar4
                            409 ;	genIpush
   0144 C0 22               410 	push	_main_ms_1_1
   0146 C0 23               411 	push	(_main_ms_1_1 + 1)
   0148 C0 24               412 	push	(_main_ms_1_1 + 2)
   014A C0 25               413 	push	(_main_ms_1_1 + 3)
                            414 ;	genIpush
   014C C0 00               415 	push	ar0
   014E C0 01               416 	push	ar1
                            417 ;	genIpush
   0150 C0 04               418 	push	ar4
   0152 C0 05               419 	push	ar5
                            420 ;	genIpush
   0154 C0 02               421 	push	ar2
   0156 C0 03               422 	push	ar3
                            423 ;	genIpush
   0158 74 D7               424 	mov	a,#__str_1
   015A C0 E0               425 	push	acc
   015C 74 01               426 	mov	a,#(__str_1 >> 8)
   015E C0 E0               427 	push	acc
   0160 74 02               428 	mov	a,#0x02
   0162 C0 E0               429 	push	acc
                            430 ;	genCall
   0164 12 03 6A            431 	lcall	_printf
   0167 E5 81               432 	mov	a,sp
   0169 24 F3               433 	add	a,#0xf3
   016B F5 81               434 	mov	sp,a
   016D                     435 00102$:
                            436 ;	clock.c:21: if (RI) {
                            437 ;	genIfx
                            438 ;	genIfxJump
   016D 20 98 03            439 	jb	_RI,00114$
   0170 02 00 78            440 	ljmp	00106$
   0173                     441 00114$:
                            442 ;	clock.c:22: putchar(getchar());
                            443 ;	genCall
   0173 12 02 73            444 	lcall	_getchar
                            445 ;	genCall
   0176 12 02 7C            446 	lcall	_putchar
                            447 ;	clock.c:23: printf("%ld\n\r", ClockTicks());
                            448 ;	genCall
   0179 12 02 24            449 	lcall	_ClockTicks
   017C AA 82               450 	mov	r2,dpl
   017E AB 83               451 	mov	r3,dph
   0180 AC F0               452 	mov	r4,b
   0182 FD                  453 	mov	r5,a
                            454 ;	genIpush
   0183 C0 02               455 	push	ar2
   0185 C0 03               456 	push	ar3
   0187 C0 04               457 	push	ar4
   0189 C0 05               458 	push	ar5
                            459 ;	genIpush
   018B 74 EB               460 	mov	a,#__str_2
   018D C0 E0               461 	push	acc
   018F 74 01               462 	mov	a,#(__str_2 >> 8)
   0191 C0 E0               463 	push	acc
   0193 74 02               464 	mov	a,#0x02
   0195 C0 E0               465 	push	acc
                            466 ;	genCall
   0197 12 03 6A            467 	lcall	_printf
   019A E5 81               468 	mov	a,sp
   019C 24 F9               469 	add	a,#0xf9
   019E F5 81               470 	mov	sp,a
   01A0 02 00 78            471 	ljmp	00106$
   01A3                     472 00108$:
   01A3 22                  473 	ret
                            474 	.area CSEG    (CODE)
   01A4                     475 __str_0:
   01A4 45 78 61 6D 70 6C   476 	.ascii "Example using the core timer to generate seconds."
        65 20 75 73 69 6E
        67 20 74 68 65 20
        63 6F 72 65 20 74
        69 6D 65 72 20 74
        6F 20 67 65 6E 65
        72 61 74 65 20 73
        65 63 6F 6E 64 73
        2E
   01D5 0A                  477 	.db 0x0A
   01D6 00                  478 	.db 0x00
   01D7                     479 __str_1:
   01D7 25 30 32 64 3A 25   480 	.ascii "%02d:%02d.%02d %ld"
        30 32 64 2E 25 30
        32 64 20 25 6C 64
   01E9 0A                  481 	.db 0x0A
   01EA 00                  482 	.db 0x00
   01EB                     483 __str_2:
   01EB 25 6C 64            484 	.ascii "%ld"
   01EE 0A                  485 	.db 0x0A
   01EF 0D                  486 	.db 0x0D
   01F0 00                  487 	.db 0x00
                            488 	.area XINIT   (CODE)
