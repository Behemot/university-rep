;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Fri Dec 20 19:26:24 2002

;--------------------------------------------------------
	.module clock
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_3	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_main_ms_1_1::
	.ds 4
_main_oldseconds_1_1::
	.ds 2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area CSEG    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	ljmp	_ClockIrqHandler
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
__sdcc_gsinit_startup:
	mov	sp,#__start__stack
	lcall	__sdcc_external_startup
	mov	a,dpl
	jz	__sdcc_init_data
	ljmp	__sdcc_program_startup
__sdcc_init_data:
;	_mcs51_genXINIT() start
	mov	a,#l_XINIT
	orl	a,#l_XINIT>>8
	jz	00003$
	mov	a,#s_XINIT
	add	a,#l_XINIT
	mov	r1,a
	mov	a,#s_XINIT>>8
	addc	a,#l_XINIT>>8
	mov	r2,a
	mov	dptr,#s_XINIT
	mov	r0,#s_XISEG
	mov	p2,#(s_XISEG >> 8)
00001$:	clr	a
	movc	a,@a+dptr
	movx	@r0,a
	inc	dptr
	inc	r0
	cjne	r0,#0,00002$
	inc	p2
00002$:	mov	a,dpl
	cjne	a,ar1,00001$
	mov	a,dph
	cjne	a,ar2,00001$
	mov	p2,#0xFF
00003$:
;	_mcs51_genXINIT() end
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;ms                        Allocated to in memory with name '_main_ms_1_1'
;seconds                   Allocated to registers r2 r3 
;oldseconds                Allocated to in memory with name '_main_oldseconds_1_1'
;------------------------------------------------------------
;	clock.c:6: void main(void) {
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	clock.c:8: unsigned int seconds, oldseconds=ClockTicks()/1000;
;	genCall
	lcall	_ClockTicks
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r5,a
;	genAssign
	mov	__divulong_PARM_2,#0xE8
	mov	(__divulong_PARM_2 + 1),#0x03
	mov	(__divulong_PARM_2 + 2),#0x00
	mov	(__divulong_PARM_2 + 3),#0x00
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r5
	lcall	__divulong
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r5,a
;	genCast
	mov	_main_oldseconds_1_1,r2
	mov	(_main_oldseconds_1_1 + 1),r3
;	clock.c:10: printf ("Example using the core timer to generate seconds.\n");
;	genIpush
	mov	a,#__str_0
	push	acc
	mov	a,#(__str_0 >> 8)
	push	acc
	mov	a,#0x02
	push	acc
;	genCall
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
;	clock.c:12: while (1) {
00106$:
;	clock.c:13: ms=ClockTicks();
;	genCall
	lcall	_ClockTicks
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	genAssign
	mov	_main_ms_1_1,r4
	mov	(_main_ms_1_1 + 1),r5
	mov	(_main_ms_1_1 + 2),r6
	mov	(_main_ms_1_1 + 3),r7
;	clock.c:14: seconds=ms/1000;
;	genAssign
	mov	__divulong_PARM_2,#0xE8
	mov	(__divulong_PARM_2 + 1),#0x03
	mov	(__divulong_PARM_2 + 2),#0x00
	mov	(__divulong_PARM_2 + 3),#0x00
;	genCall
	mov	dpl,_main_ms_1_1
	mov	dph,(_main_ms_1_1 + 1)
	mov	b,(_main_ms_1_1 + 2)
	mov	a,(_main_ms_1_1 + 3)
	lcall	__divulong
	mov	r0,dpl
	mov	r1,dph
	mov	r2,b
	mov	r3,a
;	genCast
	mov	ar2,r0
	mov	ar3,r1
;	clock.c:15: if (oldseconds!=seconds) {
;	genCmpEq
	mov	a,_main_oldseconds_1_1
	cjne	a,ar2,00113$
	mov	a,(_main_oldseconds_1_1 + 1)
	cjne	a,ar3,00113$
	ljmp	00102$
00113$:
;	clock.c:16: oldseconds=seconds;
;	genAssign
	mov	_main_oldseconds_1_1,r2
	mov	(_main_oldseconds_1_1 + 1),r3
;	clock.c:19: (int)seconds%60, ms);
;	genAssign
	mov	ar0,r2
	mov	ar1,r3
;	genAssign
	clr	a
	mov	(__modsint_PARM_2 + 1),a
	mov	__modsint_PARM_2,#0x3C
;	clock.c:18: (int)seconds/3600, (int)(seconds/60)%60, 
;	genCall
	mov	dpl,r0
	mov	dph,r1
	push	ar2
	push	ar3
	lcall	__modsint
	mov	r0,dpl
	mov	r1,dph
	pop	ar3
	pop	ar2
;	genAssign
	clr	a
	mov	(__divuint_PARM_2 + 1),a
	mov	__divuint_PARM_2,#0x3C
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	push	ar0
	push	ar1
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
	pop	ar1
	pop	ar0
	pop	ar3
	pop	ar2
;	genAssign
	mov	r4,a
	mov	r5,b
;	genAssign
	clr	a
	mov	(__modsint_PARM_2 + 1),a
	mov	__modsint_PARM_2,#0x3C
;	genCall
	mov	dpl,r4
	mov	dph,r5
	push	ar2
	push	ar3
	push	ar0
	push	ar1
	lcall	__modsint
	mov	r4,dpl
	mov	r5,dph
	pop	ar1
	pop	ar0
	pop	ar3
	pop	ar2
;	genAssign
;	genAssign
	mov	__divsint_PARM_2,#0x10
	mov	(__divsint_PARM_2 + 1),#0x0E
;	clock.c:17: printf ("%02d:%02d.%02d %ld\n", 
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar4
	push	ar5
	push	ar0
	push	ar1
	lcall	__divsint
	mov	r2,dpl
	mov	r3,dph
	pop	ar1
	pop	ar0
	pop	ar5
	pop	ar4
;	genIpush
	push	_main_ms_1_1
	push	(_main_ms_1_1 + 1)
	push	(_main_ms_1_1 + 2)
	push	(_main_ms_1_1 + 3)
;	genIpush
	push	ar0
	push	ar1
;	genIpush
	push	ar4
	push	ar5
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_1
	push	acc
	mov	a,#(__str_1 >> 8)
	push	acc
	mov	a,#0x02
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf3
	mov	sp,a
00102$:
;	clock.c:21: if (RI) {
;	genIfx
;	genIfxJump
	jb	_RI,00114$
	ljmp	00106$
00114$:
;	clock.c:22: putchar(getchar());
;	genCall
	lcall	_getchar
;	genCall
	lcall	_putchar
;	clock.c:23: printf("%ld\n\r", ClockTicks());
;	genCall
	lcall	_ClockTicks
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r5,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_2
	push	acc
	mov	a,#(__str_2 >> 8)
	push	acc
	mov	a,#0x02
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf9
	mov	sp,a
	ljmp	00106$
00108$:
	ret
	.area CSEG    (CODE)
__str_0:
	.ascii "Example using the core timer to generate seconds."
	.db 0x0A
	.db 0x00
__str_1:
	.ascii "%02d:%02d.%02d %ld"
	.db 0x0A
	.db 0x00
__str_2:
	.ascii "%ld"
	.db 0x0A
	.db 0x0D
	.db 0x00
	.area XINIT   (CODE)
