;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.3.2 Fri Dec 20 19:26:24 2002

;--------------------------------------------------------
	.module hwinit
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _putchar
	.globl _getchar
	.globl __sdcc_external_startup
	.globl _ClockIrqHandler
	.globl _ClockTicks
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits 
;--------------------------------------------------------
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks 
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_3	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_milliSeconds:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'ClockIrqHandler'
;------------------------------------------------------------
;------------------------------------------------------------
;	hwinit.c:13: void ClockIrqHandler (void) interrupt 1 using 3 {
;	-----------------------------------------
;	 function ClockIrqHandler
;	-----------------------------------------
_ClockIrqHandler:
	ar2 = 0x1a
	ar3 = 0x1b
	ar4 = 0x1c
	ar5 = 0x1d
	ar6 = 0x1e
	ar7 = 0x1f
	ar0 = 0x18
	ar1 = 0x19
	push	acc
	push	b
	push	dpl
	push	dph
	push	psw
	mov	psw,#0x18
;	hwinit.c:14: TL0 = TIMER0_RELOAD_VALUE&0xff;
;	genAssign
	mov	_TL0,#0x67
;	hwinit.c:15: TH0 = TIMER0_RELOAD_VALUE>>8;
;	genAssign
	mov	_TH0,#0xFC
;	hwinit.c:16: milliSeconds++;
;	genPlus
;	genPlusIncr
	mov	a,#0x01
	add	a,_milliSeconds
	mov	_milliSeconds,a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,(_milliSeconds + 1)
	mov	(_milliSeconds + 1),a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,(_milliSeconds + 2)
	mov	(_milliSeconds + 2),a
; Peephole 180   changed mov to clr
	clr  a
	addc	a,(_milliSeconds + 3)
	mov	(_milliSeconds + 3),a
00101$:
	pop	psw
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'ClockTicks'
;------------------------------------------------------------
;------------------------------------------------------------
;	hwinit.c:20: unsigned long ClockTicks(void) {
;	-----------------------------------------
;	 function ClockTicks
;	-----------------------------------------
_ClockTicks:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	hwinit.c:22: ET0=0;
;	genAssign
	clr	_ET0
;	hwinit.c:23: ms=milliSeconds;
;	genAssign
	mov	r2,_milliSeconds
	mov	r3,(_milliSeconds + 1)
	mov	r4,(_milliSeconds + 2)
	mov	r5,(_milliSeconds + 3)
;	hwinit.c:24: ET0=1;
;	genAssign
	setb	_ET0
;	hwinit.c:25: return ms;
;	genRet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r5
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	hwinit.c:28: unsigned char _sdcc_external_startup() {
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
;	hwinit.c:30: TR0=0; // stop timer 0
;	genAssign
	clr	_TR0
;	hwinit.c:31: TMOD =(TMOD&0xf0)|0x01; // T0=16bit timer
;	genAnd
	mov	a,#0xF0
	anl	a,_TMOD
;	genOr
	orl	a,#0x01
	mov	_TMOD,a
;	hwinit.c:33: TL0 = -TIMER0_RELOAD_VALUE&0xff;
;	genAssign
	mov	_TL0,#0x99
;	hwinit.c:34: TH0 = -TIMER0_RELOAD_VALUE>>8;
;	genAssign
	mov	_TH0,#0x03
;	hwinit.c:35: milliSeconds=0; // reset system time
;	genAssign
	clr	a
	mov	(_milliSeconds + 3),a
	mov	(_milliSeconds + 2),a
	mov	(_milliSeconds + 1),a
	mov	_milliSeconds,a
;	hwinit.c:36: TR0=1; // start timer 0
;	genAssign
	setb	_TR0
;	hwinit.c:37: ET0=1; // enable timer 0 interrupt
;	genAssign
	setb	_ET0
;	hwinit.c:40: TR1=0; // stop timer 1
;	genAssign
	clr	_TR1
;	hwinit.c:41: TMOD = (TMOD&0x0f)|0x20; // T1=8bit autoreload timer
;	genAnd
	mov	a,#0x0F
	anl	a,_TMOD
;	genOr
	orl	a,#0x20
	mov	_TMOD,a
;	hwinit.c:43: PCON |= 0x80; // SMOD=1: double baudrate
;	genOr
	orl	_PCON,#0x80
;	hwinit.c:44: TH1=TL1=TIMER1_RELOAD_VALUE;
;	genAssign
	mov	_TL1,#0xFD
;	genAssign
	mov	_TH1,#0xFD
;	hwinit.c:45: TR1=1; // start timer 1
;	genAssign
	setb	_TR1
;	hwinit.c:48: SCON=0x52; // mode 1, ren, txrdy, rxempty
;	genAssign
	mov	_SCON,#0x52
;	hwinit.c:50: EA=1; // enable global interrupt
;	genAssign
	setb	_EA
;	hwinit.c:52: return 0;
;	genRet
	mov	dpl,#0x00
00101$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;------------------------------------------------------------
;	hwinit.c:55: char getchar() {
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	hwinit.c:57: while (!RI)
00101$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _RI,00101$
00108$:
;	hwinit.c:59: RI=0;
;	genAssign
	clr	_RI
;	hwinit.c:60: c=SBUF;
;	genAssign
	mov	dpl,_SBUF
;	hwinit.c:61: return c;
;	genRet
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;c                         Allocated to registers r2 
;------------------------------------------------------------
;	hwinit.c:64: void putchar(char c) {
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	r2,dpl
;	hwinit.c:65: while (!TI)
00101$:
;	genIfx
;	genIfxJump
; Peephole 111   removed ljmp by inverse jump logic
	jnb  _TI,00101$
00111$:
;	hwinit.c:67: TI=0;
;	genAssign
	clr	_TI
;	hwinit.c:68: SBUF=c;
;	genAssign
	mov	_SBUF,r2
;	hwinit.c:69: if (c=='\n') {
;	genCmpEq
; Peephole 132   changed ljmp to sjmp
; Peephole 199   optimized misc jump sequence
	cjne r2,#0x0A,00106$
;00112$:
; Peephole 200   removed redundant sjmp
00113$:
;	hwinit.c:70: putchar('\r');
;	genCall
	mov	dpl,#0x0D
	lcall	_putchar
00106$:
	ret
	.area CSEG    (CODE)
	.area XINIT   (CODE)
