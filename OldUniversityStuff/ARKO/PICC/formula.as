opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 5239"

opt pagewidth 120

	opt lm

	processor	16C84
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
;BANK0:	_main->___awdiv
;BANK0:	___awdiv->___wmul
	FNCALL	_main,___wmul
	FNCALL	_main,___awdiv
	FNROOT	_main
	global	_EEADR
psect	text17,local,class=CODE,delta=2
global __ptext17
__ptext17:
_EEADR  equ     9
	global	_EEDATA
_EEDATA  equ     8
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RTCC
_RTCC  equ     1
	global	_STATUS
_STATUS  equ     3
	global	_TMR0
_TMR0  equ     1
	global	_CARRY
_CARRY  equ     24
	global	_DC
_DC  equ     25
	global	_EEIE
_EEIE  equ     94
	global	_GIE
_GIE  equ     95
	global	_INT
_INT  equ     48
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_PD
_PD  equ     27
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RP0
_RP0  equ     29
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_TO
_TO  equ     28
	global	_ZERO
_ZERO  equ     26
	global	_EECON1
_EECON1  equ     136
	global	_EECON2
_EECON2  equ     137
	global	_OPTION
_OPTION  equ     129
	global	_R1
_R1  equ     141
	global	_R2
_R2  equ     142
	global	_R3
_R3  equ     143
	global	_R4
_R4  equ     144
	global	_R5
_R5  equ     145
	global	_R6
_R6  equ     146
	global	_R7
_R7  equ     147
	global	_R8
_R8  equ     148
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_EEIF
_EEIF  equ     1092
	global	_INTEDG
_INTEDG  equ     1038
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RD
_RD  equ     1088
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_WR
_WR  equ     1089
	global	_WREN
_WREN  equ     1090
	global	_WRERR
_WRERR  equ     1091
	file	"formula.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??___wmul
??___wmul: ;@ 0x0
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x0
	ds	2
	global	?___wmul
?___wmul: ;@ 0x2
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x2
	ds	2
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x4
	ds	2
	global	??___awdiv
??___awdiv: ;@ 0x6
	ds	1
	global	___awdiv@counter
___awdiv@counter:	; 1 bytes @ 0x7
	ds	1
	global	___awdiv@sign
___awdiv@sign:	; 1 bytes @ 0x8
	ds	1
	global	___awdiv@quotient
___awdiv@quotient:	; 2 bytes @ 0x9
	ds	2
	global	?___awdiv
?___awdiv: ;@ 0xB
	global	___awdiv@dividend
___awdiv@dividend:	; 2 bytes @ 0xB
	ds	2
	global	___awdiv@divisor
___awdiv@divisor:	; 2 bytes @ 0xD
	ds	2
	global	??_main
??_main: ;@ 0xF
	ds	4
	global	?_main
?_main: ;@ 0x13
;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON           0      0       0
; BANK0           34     19      19


;Pointer list with targets:

;?___awdiv	int  size(1); Largest target is 0
;?___wmul	unsigned int  size(1); Largest target is 0


;Main: autosize = 0, tempsize = 4, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                                4    0  266   0.00
;                                   15 BANK0    4
;             ___wmul
;            ___awdiv
;  ___awdiv                                           5    4  198   0.00
;                                    6 BANK0    9
;             ___wmul (ARG)
;  ___wmul                                            2    4   68   0.00
;                                    0 BANK0    6
; Estimated maximum call depth 1
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;COMMON               0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;BITCOMMON            0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR0              0      0       0       1        0.0%
;SFR1                 0      0       0       2        0.0%
;BITSFR1              0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BITBANK0            22      0       0       3        0.0%
;BANK0               22     13      13       4       55.9%
;ABS                  0      0       5       5        0.0%
;DATA                 0      0       5       6        0.0%
;EEDATA              40      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 4 in file "formula.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       4
;      Temp:     4
;      Total:    4
; This function calls:
;		___wmul
;		___awdiv
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"formula.c"
	line	4
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;formula.c: 4: VOID main(VOID){
	
_main:	
	opt stack 8
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	5
	
l30000220:	
;formula.c: 5: R2=3;
	movlw	(03h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(142)^080h	;volatile
	line	6
;formula.c: 6: R7=4;
	movlw	(04h)
	movwf	(147)^080h	;volatile
	line	7
;formula.c: 7: R6=1;
	movlw	(01h)
	movwf	(146)^080h	;volatile
	
l30000221:	
	line	9
;formula.c: 9: R1=((R2*R7)/4);
	movf	(142)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	movwf	(??_main+0+0)
	clrf	(??_main+0+0+1)
	movf	0+(??_main+0+0),w
	movwf	(?___wmul)
	movf	1+(??_main+0+0),w
	movwf	(?___wmul+1)
	bsf	status, 5	;RP0=1, select bank1
	movf	(147)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	movwf	(??_main+2+0)
	clrf	(??_main+2+0+1)
	movf	0+(??_main+2+0),w
	movwf	0+(?___wmul)+02h
	movf	1+(??_main+2+0),w
	movwf	1+(?___wmul)+02h
	fcall	___wmul
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(1+(?___wmul)),w
	clrf	(?___awdiv+1)
	addwf	(?___awdiv+1)
	movf	(0+(?___wmul)),w
	clrf	(?___awdiv)
	addwf	(?___awdiv)

	movlw	low(04h)
	movwf	0+(?___awdiv)+02h
	movlw	high(04h)
	movwf	(0+(?___awdiv)+02h)+1
	fcall	___awdiv
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(0+(?___awdiv)),w
	bsf	status, 5	;RP0=1, select bank1
	movwf	(141)^080h	;volatile
	
l30000222:	
	line	10
;formula.c: 10: R3=((R5+6)/8);
	movf	(145)^080h,w	;volatile
	addlw	low(06h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(?___awdiv)
	movlw	high(06h)
	skipnc
	movlw	high(06h)+1
	movwf	((?___awdiv))+1
	movlw	low(08h)
	movwf	0+(?___awdiv)+02h
	movlw	high(08h)
	movwf	(0+(?___awdiv)+02h)+1
	fcall	___awdiv
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(0+(?___awdiv)),w
	bsf	status, 5	;RP0=1, select bank1
	movwf	(143)^080h	;volatile
	
l30000223:	
	line	11
;formula.c: 11: R4=R1&R3;
	movf	(141)^080h,w	;volatile
	andwf	(143)^080h,w	;volatile
	movwf	(144)^080h	;volatile
	
l1:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	12
	signat	_main,88
	global	___awdiv
psect	text18,local,class=CODE,delta=2
global __ptext18
__ptext18:

; *************** function ___awdiv *****************
; Defined at:
;		line 5 in file "/home/anthony/hitech/picc/sources/awdiv.c"
; Parameters:    Size  Location     Type
;  dividend        2   11[BANK0 ] int 
;  divisor         2   13[BANK0 ] int 
; Auto vars:     Size  Location     Type
;  quotient        2    9[BANK0 ] int 
;  sign            1    8[BANK0 ] unsigned char 
;  counter         1    7[BANK0 ] unsigned char 
; Return value:  Size  Location     Type
;                  2   11[BANK0 ] int 
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       9
;      Temp:     1
;      Total:    9
; This function calls:
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text18
	file	"/home/anthony/hitech/picc/sources/awdiv.c"
	line	5
	global	__size_of___awdiv
	__size_of___awdiv	equ	__end_of___awdiv-___awdiv
	
___awdiv:	
	opt stack 7
; Regs used in ___awdiv: [wreg+status,2+status,0]
	line	9
	
l30000192:	
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	movwf	(___awdiv@sign)
	
l30000193:	
	line	10
	btfss	(___awdiv@divisor+1),7
	goto	u51
	goto	u50
u51:
	goto	l30000195
u50:
	
l30000194:	
	line	11
	comf	(___awdiv@divisor),f
	comf	(___awdiv@divisor+1),f
	incf	(___awdiv@divisor),f
	skipnz
	incf	(___awdiv@divisor+1),f
	line	12
	clrf	(___awdiv@sign)
	bsf	status,0
	rlf	(___awdiv@sign),f
	
l30000195:	
	line	14
	btfss	(___awdiv@dividend+1),7
	goto	u61
	goto	u60
u61:
	goto	l153
u60:
	
l30000196:	
	line	15
	comf	(___awdiv@dividend),f
	comf	(___awdiv@dividend+1),f
	incf	(___awdiv@dividend),f
	skipnz
	incf	(___awdiv@dividend+1),f
	
l30000197:	
	line	16
	movlw	(01h)
	movwf	(??___awdiv+0+0)
	movf	(??___awdiv+0+0),w
	xorwf	(___awdiv@sign),f
	
l153:	
	line	18
	movlw	low(0)
	movwf	(___awdiv@quotient)
	movlw	high(0)
	movwf	((___awdiv@quotient))+1
	line	19
	movf	(___awdiv@divisor+1),w
	iorwf	(___awdiv@divisor),w
	skipnz
	goto	u71
	goto	u70
u71:
	goto	l30000207
u70:
	
l30000198:	
	line	20
	clrf	(___awdiv@counter)
	bsf	status,0
	rlf	(___awdiv@counter),f
	goto	l30000201
	
l30000199:	
	line	22
	movlw	01h
u85:
	clrc
	rlf	(___awdiv@divisor),f
	rlf	(___awdiv@divisor+1),f
	addlw	-1
	skipz
	goto	u85
	
l30000200:	
	line	23
	movlw	(01h)
	movwf	(??___awdiv+0+0)
	movf	(??___awdiv+0+0),w
	addwf	(___awdiv@counter),f
	
l30000201:	
	line	21
	btfss	(___awdiv@divisor+1),(15)&7
	goto	u91
	goto	u90
u91:
	goto	l30000199
u90:
	
l30000202:	
	line	26
	movlw	01h
u105:
	clrc
	rlf	(___awdiv@quotient),f
	rlf	(___awdiv@quotient+1),f
	addlw	-1
	skipz
	goto	u105
	line	27
	movf	(___awdiv@divisor+1),w
	subwf	(___awdiv@dividend+1),w
	skipz
	goto	u115
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),w
u115:
	skipc
	goto	u111
	goto	u110
u111:
	goto	l30000205
u110:
	
l30000203:	
	line	28
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),f
	movf	(___awdiv@divisor+1),w
	skipc
	decf	(___awdiv@dividend+1),f
	subwf	(___awdiv@dividend+1),f
	
l30000204:	
	line	29
	bsf	(___awdiv@quotient)+(0/8),(0)&7
	
l30000205:	
	line	31
	movlw	01h
u125:
	clrc
	rrf	(___awdiv@divisor+1),f
	rrf	(___awdiv@divisor),f
	addlw	-1
	skipz
	goto	u125
	
l30000206:	
	line	32
	movlw	low(01h)
	subwf	(___awdiv@counter),f
	btfss	status,2
	goto	u131
	goto	u130
u131:
	goto	l30000202
u130:
	
l30000207:	
	line	34
	movf	(___awdiv@sign),w
	skipz
	goto	u140
	goto	l30000209
u140:
	
l30000208:	
	line	35
	comf	(___awdiv@quotient),f
	comf	(___awdiv@quotient+1),f
	incf	(___awdiv@quotient),f
	skipnz
	incf	(___awdiv@quotient+1),f
	
l30000209:	
	line	36
	movf	(___awdiv@quotient+1),w
	clrf	(?___awdiv+1)
	addwf	(?___awdiv+1)
	movf	(___awdiv@quotient),w
	clrf	(?___awdiv)
	addwf	(?___awdiv)

	
l151:	
	return
	opt stack 0
GLOBAL	__end_of___awdiv
	__end_of___awdiv:
; =============== function ___awdiv ends ============

psect	text19,local,class=CODE,delta=2
global __ptext19
__ptext19:
	line	37
	signat	___awdiv,8314
	global	___wmul

; *************** function ___wmul *****************
; Defined at:
;		line 3 in file "/home/anthony/hitech/picc/sources/wmul.c"
; Parameters:    Size  Location     Type
;  multiplier      2    2[BANK0 ] unsigned int 
;  multiplicand    2    4[BANK0 ] unsigned int 
; Auto vars:     Size  Location     Type
;  product         2    0[BANK0 ] unsigned int 
; Return value:  Size  Location     Type
;                  2    2[BANK0 ] unsigned int 
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       6
;      Temp:     0
;      Total:    6
; This function calls:
;		Nothing
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text19
	file	"/home/anthony/hitech/picc/sources/wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt stack 7
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l30000214:	
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(___wmul@product)
	movlw	high(0)
	movwf	((___wmul@product))+1
	
l30000215:	
	line	7
	btfss	(___wmul@multiplier),(0)&7
	goto	u151
	goto	u150
u151:
	goto	l11
u150:
	
l30000216:	
	line	8
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	skipnc
	incf	(___wmul@product+1),f
	movf	(___wmul@multiplicand+1),w
	addwf	(___wmul@product+1),f
	
l11:	
	line	9
	movlw	01h
u165:
	clrc
	rlf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	addlw	-1
	skipz
	goto	u165
	
l30000217:	
	line	10
	movlw	01h
u175:
	clrc
	rrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	addlw	-1
	skipz
	goto	u175
	line	11
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u181
	goto	u180
u181:
	goto	l30000215
u180:
	
l30000218:	
	line	12
	movf	(___wmul@product+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(___wmul@product),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	
l7:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
; =============== function ___wmul ends ============

psect	text20,local,class=CODE,delta=2
global __ptext20
__ptext20:
	line	13
	signat	___wmul,8314
	end
