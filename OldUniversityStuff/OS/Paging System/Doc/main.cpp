#include "paging_system.h"
#include <pthread.h>
#define PAGE_SIZE 4096

int num_of_processes=100;
int max_pages=20;
int work_set_size=10;
int num_of_frames=1000;
int event_number=10000;
int total_time=0;

int main(int argc,char *argv[])
{
    if(argc==6)
    {
        num_of_processes=atoi(argv[1]);
        max_pages=atoi(argv[2]);
        work_set_size=atoi(argv[3]);
        num_of_frames=atoi(argv[4]);
        event_number=atoi(argv[5]);
    }
    int num_of_pages[num_of_processes];
    int active_page[num_of_processes];
    int work_sets[num_of_processes][work_set_size];
    
    

    for(int i=0;i<num_of_processes;i++)
    {
        num_of_pages[i]=rand()%max_pages+1;
        active_page[i]=0;
        for(int j=0;j<work_set_size;j++){
            work_sets[i][j]=rand()%num_of_pages[i]+1;
        }
    }    
    initialize_paging(0,num_of_frames*PAGE_SIZE);
 
    for(int i=0;i<event_number;i++)
    {
        int cur_proc=rand()%num_of_processes;
        int process=rand()%num_of_processes;
        int operation_type=rand()%100;
        bool access_mode;
        if(operation_type<=90)
        {
            active_page[cur_proc]=work_sets[cur_proc][rand()%work_set_size];    
        }
        
        else
        {
            active_page[cur_proc]=rand()%num_of_pages[cur_proc];
        }
        int event=rand()%100;
        if(event<=50)
        {
            access_mode=false;
        }
        else
        {
            access_mode=true;
        }
        
        getPage(active_page[cur_proc],cur_proc,access_mode);
        total_time+=4;
        if(event_number%100==0)
        {
            for(int i=0;i<num_of_processes;i++)
            {
                work_sets[i][rand()%work_set_size]=rand()%num_of_pages[i];
            }
        }
        if((int)total_time%1000==0){
            cout<<total_time<<endl;
            cout<<"Memory state: "<< physical_pages_used<< "/"<<physical_pages_num<<endl;
            cout<<"Page misses per second:" << ((double)page_misses)/((double)total_time)*1000.0<<endl;
            cout<<"Misses percentage:" << ((double)page_misses)/i<<endl;
            
        }
        usleep(4000);
    }
  
    
    
    
}
