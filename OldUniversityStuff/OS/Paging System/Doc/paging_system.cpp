#include "paging_system.h"
#define PAGE_SIZE 4096
#define REFERENCE_TIMEOUT 20000000

size_t start_addr;
size_t physical_pages_num;//number of actual physical memory pages
size_t physical_pages_used;//number of physical pages currently used
page_table_row* paging_table; //Inverse paging table
multimap <size_t,size_t> anchors; //Anchors to determine if the virtual page was created and where it is kept
long int old_time;//Old time variable for reference accounting.
page_table_row* free_pages;//Free physical pages that are availible.
page_table_row* replacement_pointer;//Pointer for replacement algorithm
int page_misses;

void initialize_paging(size_t start_address, size_t end_address)//This initializes paging table and auxiliary stuff
{
    page_misses=0;
    start_addr=start_address;
    physical_pages_num=(end_address-start_address)/PAGE_SIZE;//Counting amount of physical pages availible
    physical_pages_used=0;
    paging_table=(page_table_row*)malloc(sizeof(page_table_row)*physical_pages_num);//Designate memory for table (it can also be swapped)
    for(int i=0;i<physical_pages_num;i++)//Initially all the pages are availible
    {
        paging_table[i].pid=-1;
        paging_table[i].physical_address=start_address+i*PAGE_SIZE;
        
    }
    free_pages=paging_table;//Free pages setup
    for(int i=0;i<physical_pages_num-1;i++)
    {
        paging_table[i].next_frame=&paging_table[i+1];
    }
    replacement_pointer=(page_table_row*)malloc(sizeof(page_table_row*));
    replacement_pointer=0;
}

size_t getFromSwap(size_t virtual_page,size_t pid)//Swaps out a virtual page of a designated process,returns physical address of swapped-out page
{
    page_misses++;   
    
    if(physical_pages_used<physical_pages_num)//If there are free physical pages - we take one of them
    {
        page_table_row* new_page=free_pages;
        
            free_pages=free_pages->next_frame;
        physical_pages_used++;
        new_page->pid=pid;
        anchors.insert(pair<size_t,size_t>(virtual_page,(new_page->physical_address-start_addr)/PAGE_SIZE));
        if(replacement_pointer==0)
        {
            replacement_pointer=new_page;
            replacement_pointer->next_frame=replacement_pointer;
            replacement_pointer->prev_frame=replacement_pointer;
        }
        else
        {
            page_table_row* prev=replacement_pointer->prev_frame;
            prev->next_frame=new_page;
            new_page->prev_frame=prev;
            new_page->next_frame=replacement_pointer;
            replacement_pointer->prev_frame=new_page;
            
        }
        return new_page->physical_address;
    }
    else//if there are no free pages -we need to chose one to put into swap
    {
    
        bool fetched=false;
        while(!fetched)
        {
            if (replacement_pointer->referenced<REFERENCE_TIMEOUT)
            {
                replacement_pointer->referenced+=REFERENCE_TIMEOUT;
                replacement_pointer=replacement_pointer->next_frame;    
            }
            else
            {
                fetched=true;
                multimap<size_t,size_t>:: iterator it;
                size_t virt_addr;
                for(it=anchors.begin();it!=anchors.end();it++)
                {
                    if (it->second==(replacement_pointer->physical_address-start_addr)/PAGE_SIZE)
                    {
                        virt_addr=it->first;
                    }
                }
                swapPage(virt_addr,replacement_pointer);
                
                replacement_pointer->pid=pid;
                replacement_pointer->modified=false;
                replacement_pointer->referenced=0;
                page_table_row* result_page=replacement_pointer;
                replacement_pointer=replacement_pointer->next_frame;
                anchors.insert(pair<size_t,size_t>(virtual_page,(result_page->physical_address-start_addr)/PAGE_SIZE));
                return result_page->physical_address;
            }
        }   
    }
    
}


void swapPage(size_t virtual_page,page_table_row* phys_page)//Swaps a physical page
{
    //!!!YOU NEED TO ACTUALLY SWAP THE PAGE HERE, BUT WE WONT DO THAT AS IT IS A DEMONSTRATION!!!
    pair<multimap<size_t,size_t>::iterator,multimap<size_t,size_t>::iterator> pages;//range for virtual pages
    pages=anchors.equal_range(virtual_page);//Find all the virtual pages with given number
    multimap<size_t,size_t>::iterator it;
    for(it=pages.first;it!=pages.second;it++)
    {
       if(paging_table[it->second].pid==phys_page->pid)//When we find needed physical page in memory
       {
            anchors.erase(it);
            phys_page->pid=-1;
            break;
       }
    } 
    
}
void releasePage(size_t virtual_page,size_t pid)//Releases page when the client program is finished
{
        
    pair<multimap<size_t,size_t>::iterator,multimap<size_t,size_t>::iterator> pages;//range for virtual pages
    pages=anchors.equal_range(virtual_page);//Find all the virtual pages with given number
    multimap<size_t,size_t>::iterator it;
    for(it=pages.first;it!=pages.second;it++)
    {
       if(paging_table[it->second].pid==pid)//When we find needed physical page in memory
       {
            anchors.erase(it);
            paging_table[it->second].pid==-1;
            paging_table[it->second].next_frame=free_pages;
            free_pages=&paging_table[it->second];
            replacement_pointer->next_frame->prev_frame=replacement_pointer->prev_frame;
            replacement_pointer->prev_frame->next_frame=replacement_pointer->next_frame;
            if(replacement_pointer==&paging_table[it->second])
            {
                if(replacement_pointer->next_frame!=replacement_pointer)
                {
                    replacement_pointer=replacement_pointer->next_frame;
                }
                else
                {
                    replacement_pointer=0;
                }
            }
            
       }
    }
    physical_pages_used--; 
    
}
size_t getPage(size_t virtual_page,size_t pid,bool access_mode)//Client interface for obtaining pages. Access mode: false - read, true - write.
{
    size_t result_address;
    bool in_memory=false;
    //Trying to find the page in the memory
    pair<multimap<size_t,size_t>::iterator,multimap<size_t,size_t>::iterator> pages;//range for virtual pages
    pages=anchors.equal_range(virtual_page);//Find all the virtual pages with given number
    if(anchors.count(virtual_page)!=0&&pages.first->first==virtual_page) //If we found at least some pages with given number
    {
        multimap<size_t,size_t>::iterator it;
        for(it=pages.first;it!=pages.second;it++)
        {
             
            if(paging_table[it->second].pid==pid)//If we actually found needed page in memory
            {
                   
        
                paging_table[it->second].referenced=0;
                if(access_mode)
                {
                    paging_table[it->second].modified=true;
                }
                result_address=paging_table[it->second].physical_address;
                in_memory=true;
                break;
            }
        }
        
    }
    
    //If we didnt get lucky with the memory - we have to swap out the page.
    if(!in_memory)
    {
 
        result_address=getFromSwap(virtual_page,pid);
        paging_table[(result_address-start_addr)/PAGE_SIZE].referenced=0;
        if(access_mode)
        {
            paging_table[(result_address-start_addr)/PAGE_SIZE].modified=true;
        }
    }
    
    //--------------------------------------------------------------------
    timespec new_time; //Here we add idle time to all page reference time so that we know which were referenced in the last meaningful period
    clock_gettime(CLOCK_MONOTONIC,&new_time);
    long int difference=new_time.tv_nsec-old_time;
    old_time=new_time.tv_nsec;
    for(int i=0;i<physical_pages_num;i++)
    {
        paging_table[i].referenced+=difference;
    }
    return result_address;      
}
