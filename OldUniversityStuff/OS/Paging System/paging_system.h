#include <map>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using std::cout;
using std::cin;
using std::endl; 
using std::multimap;
using std::pair;

struct page_table_row{
    page_table_row* next_frame;//Pointer to the next table row, used in page replacement algorithm
    page_table_row* prev_frame;//Previous frame pointer for the same reason
    size_t physical_address;
    size_t pid;//process ID of the process that currently uses this page, -1 if noone uses it
    long int referenced;//how much time ago the page was referenced (in nanoseconds).
    bool modified;//if the page was modified since last swap in
};

extern size_t start_addr;//start of physical memory sector
extern size_t physical_pages_num;//number of actual physical memory pages
extern size_t physical_pages_used;//number of physical pages currently used
extern page_table_row* paging_table; //Inverse paging table
extern multimap<size_t,size_t> anchors; //Anchors to determine if the virtual page was created and where it is kept
extern long int old_time;//Old time variable for reference accounting.
extern page_table_row* free_pages; //Free physical pages that are availible.
extern page_table_row* replacement_pointer;//Pointer for replacement algorithm
extern int page_misses;

void initialize_paging(size_t start_address,size_t end_address);//This initializes paging table and auxiliary stuff

size_t getFromSwap(size_t virtual_page,size_t pid);//Swaps out a virtual page of a designated process

void swapPage(size_t virtual_page,page_table_row* phys_page);//Swaps a physical page

void releasePage(size_t virtual_page,size_t pid);//Releases page when the client program is finished

size_t getPage(size_t virtual_page,size_t pid,bool access_mode);//Client interface for obtaining pages. Access mode: false - read, true - write.
