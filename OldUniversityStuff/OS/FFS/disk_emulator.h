#ifndef DISK_EMULATOR_H
#define DISK_EMULATOR_H
#include <fstream>
#include <string>
using std::fstream;
using std::string;

class block_device
{
    size_t size;
    size_t block_size;
    size_t page_size;
    string device_name;
    string counters_name;
    fstream device;
    fstream counters;
    public:
    block_device(size_t s, size_t blocksize,size_t pagesize);
    int init();
    int open();
    int close();
    int write(size_t position, char* data, size_t blocksize);        
    int read(size_t position, char* data,size_t blocksize);
    int erase(size_t position);
    int get_size();
    int get_block_size();
    int get_page_size();  
    
};
#endif
