#include "disk_emulator.h"
#include <stdlib.h>
#include <iostream>
using std::ios;
using std::cout;
using std::endl;
#define MAX_REWRITES 10
block_device::block_device(size_t s, size_t blocksize,size_t pagesize)
{
    size=s;
    block_size=blocksize;
    page_size=pagesize;
    device_name="test.bd";
    counters_name="counters.bd";
    
};

int block_device::init()
{
    open();
    cout<<"Block device created:"<<endl;
    cout<<"Size: "<<size<<endl;
    cout<<"Block size: "<<block_size<<endl;
    cout<<"Page size: "<<page_size<<endl;
    cout<<"Number of blocks: "<<size/block_size<<endl;
    if(device.is_open()){
        char* dump=(char*)malloc(sizeof(char)*block_size);
        for(int i=0;i<block_size;i++)
        {
            dump[i]=1;
        }
        char temp=0;
        for(int i=0;i<size/block_size;i++)
        {
            device.write(dump,block_size);
            counters.write(&temp,1);
        }
        free(dump);
        
    }
    close();
        
}

int block_device::open()
{
    device.open(device_name.c_str(),fstream::in|fstream::out|fstream::binary);
    counters.open(counters_name.c_str(),fstream::in|fstream::out|fstream::binary);
    if(device.is_open()&&counters.is_open())
    {
        return 0;
    }
    else
    {
        return 1;
    }
};
int block_device::close()
{
    device.close();
    counters.close();
    if(device.is_open()||counters.is_open())
    {
        return 1;
    }
    else
    {
        return 0;
    }
};
int block_device::write(size_t position, char* data, size_t blocksize)
{
    if(!device.is_open())
    {
        return 1;
    }
    device.seekp(position,ios::beg);
    device.write(data,blocksize);
    int page_num=blocksize/page_size;
    bool successful=true;
    for(int i=0;i<page_num;i++)
    {
        counters.seekg(position/page_size+i,ios::beg);
        char temp;
        counters.read(&temp,1);
        temp++;
        counters.seekp(position/page_size+i,ios::beg);
        counters.write(&temp,1);
        if(temp>MAX_REWRITES)
        {
            successful=false;
        }
    }
    if (successful)
    {
        return 0;
    }
    else
    {
        return 1;
    }    
};        
int block_device::read(size_t position, char* data,size_t blocksize)
{
    if(device.is_open()){
        device.seekg(position);
        device.read(data,blocksize);
        return 0;
    }
    else
    {
        return 1;
    }
};
int block_device::erase(size_t position)
{
    if(device.is_open()){
        char* dump=(char*)malloc(sizeof(char)*block_size);
        for(int i=0;i<block_size;i++)
        {
            dump[i]=1;
        }
        device.seekp(position-position%block_size);
        device.write(dump,block_size);
        int page_num=block_size/page_size;
        for(int i=0;i<page_num;i++)
        {
            char temp=0;
            counters.seekp(position/page_size+i,ios::beg);
            counters.write(&temp,1);
        }
        delete(dump);
      return 0;
    }
    else
    {
        return 1;
    }
};
int block_device::get_size()
{
    return size;
};
int block_device::get_block_size()
{
    return block_size;
};
int block_device::get_page_size()
{
    return page_size;
};
