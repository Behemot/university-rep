#ifndef FFS_H
#define FFS_H
#include "disk_emulator.h"
#include <stdlib.h>
#include <string>
#include <map>
#include <vector>
using std::string;
using std::map;
using std::multimap;
using std::pair;
using std::vector;
#define MIN_FREE_BLOCKS 2
 
struct node_header//Common header for all the nodes on drive
{
    int node_type:3;//0 - inode, 1 - dirent node, 2 - clean marker node, 3 - end of block node
    int compression_type:1;//0 - no compression, 1 - zero-pseudocompression.
    int version;
};

struct raw_node_header//Header for inode-concerning nodes on drive
{
    int parent_id;
    int command_type;//0 - data change, 1- truncate
    size_t displacement;//if it's a truncate command - new size, otherwise - displacement for data
    size_t data_length;
};

struct direntry_header//Header for directory entry node on drive
{
    int entry_id;//direntry id
    int target_id;//linked inode id
    int root_id;//id of inode through where the link will be created
    size_t data_length;//Size of entry name
};

struct dirent_cache;


struct raw_node_ref//Info about physical nodes we need to hold in memory
{
    bool obsolete:1;
    union nodetype{//That's linked list to connects physical nodes of one inode.
        raw_node_ref* next_in_inode;
        dirent_cache* dirent;
    } node_type;
    raw_node_ref* next_in_phys;//Next node in physical page.
    size_t phys_offset;
    size_t total_length;
    
};

struct dirent_cache//Info about directory entries we'll keep in memory
{
    int version;
    raw_node_ref* phys_node;
};


struct inode_cache//Info about inodes placed in memory.
{
    raw_node_ref* nodes;//Physical nodes cache that reffers to this inode
    int cur_version;//current inode version.
    int cur_size;//current file size
};


struct block//Block structure to put in lists
{
   raw_node_ref* first_node;//first node in the block
   block* next_block;//next block on the list
   int free_space;
   size_t offset;
};


struct block_statistics
{
    int state;//0 - free, 1 - clean, 2 - dirty
    int free_space;    
};

class ffs
{
    int free_blocks;
    int garbage_collects;
    block_device *flash_drive;//flash drive emulator
    int next_inode_num; //inode numbers that are never repeated
    int next_dirent_num; //direntries numbers that are never repeated
    block* free_list; 
    block* clean_list;
    block* dirty_list;
    block* buffer_block;
    char* small_block_buffer;
    int buffer_block_offset;
    
    map<int,struct inode_cache*> inode_cache_map;//key is inode id
    map<int,map<int,map<int,struct dirent_cache*>*>* > dirent_cache_map;//keys are target inode id,root id,direntry id
    bool has_obsoletes;
    public:
    ffs(block_device* drive);
    void format();
    void mount();
    void unmount();
    int create_inode(string name,int root_id);
    int create_link(int inode, string alias_name,int root_id);
    int write_data(int inode,char* data, size_t data_size,size_t displacement);
    int read_data(int inode,char* data,size_t data_size,size_t displacement);
    int truncate(int inode, size_t trunc_size);
    int get_inode_num(string file_name,int root_id);
    int delete_link(string file_name,int root_id);
    vector<string> list_files(int root_id);
    int get_file_size(int inode);
    int get_file_version(int inode);
    block_statistics* get_block_stat(int block_num);
    private:
    int garbage_collect();
    int write_node(raw_node_ref* raw_ref,char* buffer);
    dirent_cache* get_direntry(string name,int root_id);
    void clear_orphaned_inodes();
    void sort_dirty_blocks();
    void sort_obsolete_nodes(inode_cache* it);
    block* find_place(int size);
};

#endif
