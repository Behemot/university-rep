#include "ffs.h"
#include <iostream>
#include <string.h>
using std::cout;
using std::endl;



ffs::ffs(block_device* drive)
{
    flash_drive=drive;
};
void ffs::format()
{
    flash_drive->open();
    node_header clean_mark={2,0,0};
    for(int i=1;i<flash_drive->get_size()/flash_drive->get_block_size();i++)
    {
        flash_drive->write(i*flash_drive->get_block_size(),(char*)&clean_mark,sizeof(node_header));
    }
    int inode_num=1;
    int dirent_num=0;
    flash_drive->write(0,(char*)&inode_num,sizeof(int));
    flash_drive->write(sizeof(int),(char*)&dirent_num,sizeof(int));
    flash_drive->close();
}
void ffs::mount()
{
    int free_count=0;
    int full_count=0;
    clean_list=NULL;
    dirty_list=NULL;
    free_list=NULL;
    small_block_buffer=(char*)malloc(flash_drive->get_block_size());
    garbage_collects=0;
    free_blocks=0;   
    flash_drive->open();
    block* all_blocks=NULL;
    //We reserve the first block for some data of our own, namely next inode number, possibly something else
    char* work_buffer=(char*)malloc(sizeof(int));
    flash_drive->read(0,work_buffer,sizeof(int));
    next_inode_num=*((int*)work_buffer);    
    flash_drive->read(sizeof(int),work_buffer,sizeof(int));
    next_dirent_num=*((int*)work_buffer);    
    free(work_buffer);
    //We need to form all the block reference structures plus inodes and direntries structures
    for(int i=1;i<flash_drive->get_size()/flash_drive->get_block_size();i++)
    {
        int disp=0;
        char* buffer=(char*)malloc(sizeof(char)*flash_drive->get_block_size());//create a buffer to read whole block
        flash_drive->read(i*flash_drive->get_block_size(),buffer,flash_drive->get_block_size());
        node_header* node_marker=(node_header*)buffer;
        block* temp_block=(block*)malloc(sizeof(struct block));    
        temp_block->offset=i*flash_drive->get_block_size();    
        if (node_marker->node_type==2)// If there's a clean block marker - just dump it into clean list
        {
            free_count++;
            free_blocks++;
            temp_block->free_space=flash_drive->get_block_size();
            temp_block->first_node=NULL;
            temp_block->next_block=free_list;
            free_list=temp_block;
        }
        else
        {
            full_count++;
            temp_block->free_space=flash_drive->get_block_size();
            temp_block->first_node=NULL;
            bool end_block=false;
            raw_node_ref* prev_node;
            while(disp<(flash_drive->get_block_size()-sizeof(node_header))&&end_block==false)
            {
                node_marker=(node_header*)((char*)buffer+disp);
                
                raw_node_ref* node=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
                node->next_in_phys=NULL;                
                switch(node_marker->node_type)
                {
                    case 0://if it's inode record
                    {
                        raw_node_header* raw_header=(raw_node_header*)((char*)node_marker+sizeof(struct node_header));
                        node->phys_offset=i*flash_drive->get_block_size()+disp;
                        node->obsolete=false;
                        if(node_marker->compression_type==0)
                        {
                            node->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header)+raw_header->data_length;
                        }
                        else
                        {
                            node->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
                        }
                        if(disp!=0)
                        {
                            prev_node->next_in_phys=node;
                        }
                        else
                        {
                            temp_block->first_node=node;
                        }
                        map<int, struct inode_cache*>::iterator it=inode_cache_map.find(raw_header->parent_id);
                        inode_cache* inode;
                        if(it==inode_cache_map.end())//If there's no appropriate inode_cache allocated - create one
                        {
                            inode=(inode_cache*)malloc(sizeof(struct inode_cache));    
                            inode->cur_version=node_marker->version;
                            inode->nodes=node;
                            node->node_type.next_in_inode=NULL;
                            inode_cache_map.insert(pair<int,struct inode_cache*>(raw_header->parent_id,inode));
                            
                        }
                        else//otherwise - just hook up another raw_node to it and change version if nessesary
                        {
                            inode=it->second;
                            node->node_type.next_in_inode=inode->nodes;
                            inode->nodes=node;
                            if (node_marker->version>inode->cur_version)inode->cur_version=node_marker->version;
                        };
                        temp_block->free_space-=node->total_length;   //dont forget about free space!
                    };break;
                    case 1://if it's direntry node
                    {
                        
                        direntry_header* dir_header=(direntry_header*)((char*)node_marker+sizeof(struct node_header));
                        node->phys_offset=i*flash_drive->get_block_size()+disp;
                        node->obsolete=false;
                        node->total_length=sizeof(struct node_header)+sizeof(struct direntry_header)+dir_header->data_length;
                        if(disp!=0)
                        {
                            prev_node->next_in_phys=node;
                        }
                        else
                        {
                            temp_block->first_node=node;
                        }
                        temp_block->free_space-=node->total_length;
                        dirent_cache* dirent=(dirent_cache*)malloc(sizeof(dirent_cache));
                        dirent->phys_node=node;
                        dirent->version=node_marker->version;
                        node->node_type.dirent=dirent;
                        map<int,map<int,map<int, dirent_cache*>*>*>::iterator it=dirent_cache_map.find(dir_header->target_id);
                        map<int,map<int,dirent_cache*>*>* second;
                        if(it==dirent_cache_map.end())
                        {
                            second= new map<int,map<int,dirent_cache*>*>();
                            dirent_cache_map.insert(pair<int,map<int,map<int,dirent_cache*>*>* >(dir_header->target_id,second));
                        }
                        else
                        {
                            second==it->second;
                        }
                        map<int,map<int,dirent_cache*>*>::iterator it2=second->find(dir_header->root_id);
                        map<int,dirent_cache*>* third;
                        if(it2==second->end())
                        {
                            third=new map<int,dirent_cache*>();
                            second->insert(pair<int,map<int,dirent_cache*>*>(dir_header->root_id,third));
                        }
                        else
                        {
                            third=it2->second;
                        }
                        map<int,dirent_cache*>::iterator it3=third->find(dir_header->entry_id);
                        if(it3==third->end())
                        {
                            third->insert(pair<int,dirent_cache*>(dir_header->entry_id,dirent));
                        
                        }
                        else
                        {
                            
                            if(it3->second->version<dirent->version)
                            {
                                it3->second=dirent;
                            }
                        }
                        if(disp!=0)
                        {
                            prev_node->next_in_phys=node;
                        }
                        else
                        {
                            temp_block->first_node=node;
                        }
                    };break;
                    case 3://if it's block termination node - throw the node into clean list (we'll sort it out later)
                    {
                        temp_block->next_block=clean_list;
                        clean_list=temp_block;
                        disp+=flash_drive->get_block_size();
                    };break;
                }
                
                prev_node=node;
                disp+=node->total_length;
            }
        }
        free(buffer);
    }
    //Clean void direntries
    for(map<int,map<int,map<int,struct dirent_cache*>*>* >::iterator it=dirent_cache_map.begin();it!=dirent_cache_map.end();it++)
    {
        for(map<int,map<int,struct dirent_cache*>*>::iterator it2=it->second->begin();it2!=it->second->end();it2++)
        {
            for(map<int,struct dirent_cache*>::iterator it3=it2->second->begin();it3!=it2->second->end();it3++)
            {
                    
                if(it3->second->phys_node->total_length==sizeof(struct node_header)+sizeof(struct direntry_header))
                {
                    it2->second->erase(it3->first);
                }
            }
        }
    }
    
    //Now we're gonna detect obsolete nodes, sort out block lists, set links for inodes and delete possible orphaned inodes and links.
    for(map<int,struct inode_cache*>::iterator it=inode_cache_map.begin();it!=inode_cache_map.end();it++)//Sort out all the obsolete raw_nodes from inodes
    {
        sort_obsolete_nodes(it->second);
    }
    //Adding root directory
    inode_cache* new_inode_cache=(inode_cache*)malloc(sizeof(struct inode_cache));
    raw_node_ref* new_raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
    new_raw_ref->obsolete=false;
    new_raw_ref->node_type.next_in_inode=NULL;
    new_raw_ref->phys_offset=-1;
    new_raw_ref->total_length=-1;
    new_inode_cache->nodes=new_raw_ref;
    new_inode_cache->cur_version=0;
    new_inode_cache->cur_size=0;
    inode_cache_map.insert(pair<int,inode_cache*>(0,new_inode_cache));
    
    
    //Deleting orphaned inodes without any links to them
    clear_orphaned_inodes(); 
    //Sort out dirty blocks
    sort_dirty_blocks();
};


void ffs::unmount()
{
    flash_drive->write(0,(char*)&next_inode_num,sizeof(int));    
    flash_drive->close();
    free_list=NULL;
    clean_list=NULL;
    dirty_list=NULL;
    inode_cache_map.clear();
    dirent_cache_map.clear();
};


int ffs::create_inode(string name,int root_id)
{
    char* buffer=(char*)malloc(sizeof(node_header)+sizeof(raw_node_header));
    node_header* new_node=(node_header*)buffer;
    raw_node_header* new_raw_node=(raw_node_header*)(buffer+sizeof(node_header));
    new_node->node_type=0;
    new_node->version=0;
    new_node->compression_type=0;
    new_raw_node->parent_id=next_inode_num;
    new_raw_node->command_type=0;
    new_raw_node->displacement=0;
    new_raw_node->data_length=0;
    inode_cache* new_inode_cache=(inode_cache*)malloc(sizeof(struct inode_cache));
    raw_node_ref* new_raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
    new_raw_ref->obsolete=false;
    new_raw_ref->node_type.next_in_inode=NULL;
    new_raw_ref->total_length=sizeof(node_header)+sizeof(raw_node_header);
    new_inode_cache->nodes=new_raw_ref;
    new_inode_cache->cur_version=0;
    new_inode_cache->cur_size=0;
    inode_cache_map.insert(pair<int,inode_cache*>(next_inode_num,new_inode_cache));
    write_node(new_raw_ref,buffer);
    this->create_link(next_inode_num,name,root_id);
    next_inode_num++;
    free(buffer);
};
int ffs::create_link(int inode, string alias_name,int root_id)
{
    int ret=0;
    char* write_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct direntry_header)+alias_name.size()+1);
    node_header* new_node=(node_header*)write_buf;
    new_node->node_type=1;
    new_node->compression_type=0;
    new_node->version=0;
    direntry_header* new_dirent=(direntry_header*)(write_buf+sizeof(struct node_header));
    new_dirent->entry_id=next_dirent_num;
    next_dirent_num++;
    new_dirent->target_id=inode;
    new_dirent->root_id=root_id;
    new_dirent->data_length=alias_name.size()+1;
    char* alias_start=write_buf+sizeof(struct direntry_header)+sizeof(struct node_header);
    memcpy(alias_start,alias_name.c_str(),alias_name.size()+1);
    dirent_cache* new_dirent_cache=(dirent_cache*)malloc(sizeof(struct dirent_cache));
    new_dirent_cache->version=0;
    raw_node_ref* new_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
    new_ref->obsolete=false;
    new_ref->node_type.dirent=new_dirent_cache;
    new_ref->total_length=sizeof(struct node_header)+sizeof(struct direntry_header)+alias_name.size()+1;
    new_dirent_cache->phys_node=new_ref;
    map<int,map<int,map<int,struct dirent_cache*>*>* >::iterator it=dirent_cache_map.find(inode);
    map<int,map<int,struct dirent_cache*>*>* second_layer;
    if(it!=dirent_cache_map.end())
    {
        second_layer=it->second;
    }
    else
    {
        second_layer=new map<int,map<int,struct dirent_cache*>*>();
        dirent_cache_map.insert(pair<int,map<int,map<int,struct dirent_cache*>*>*>(inode,second_layer));
    }
    map<int,map<int,struct dirent_cache*>*>::iterator it2=second_layer->find(root_id);   
    map<int,struct dirent_cache*>* third_layer;
    if(it2!=second_layer->end())
    {
        third_layer=it2->second;
    }
    else
    {
        third_layer=new map<int,struct dirent_cache*>();
        second_layer->insert(pair<int,map<int,struct dirent_cache*>*>(root_id,third_layer));
    }
    third_layer->insert(pair<int,struct dirent_cache*>(new_dirent->entry_id,new_dirent_cache));
    ret=write_node(new_ref,write_buf);
    free(write_buf);
    return ret;
};


int ffs::write_data(int inode,char* data, size_t data_size,size_t displacement)
{
    int ret=0;
    if (data_size<(flash_drive->get_block_size()-sizeof(struct node_header)-sizeof(struct raw_node_header)))
    {
        char* buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header)+data_size);
        node_header* head=(node_header*)buf;
        raw_node_header* raw=(raw_node_header*)(buf+sizeof(struct node_header));
        memcpy(buf+sizeof(struct node_header)+sizeof(struct raw_node_header),data,data_size);
        raw_node_ref* raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
        inode_cache* inode_ref=inode_cache_map.find(inode)->second;
        
        head->node_type=0;
        head->compression_type=0;
        inode_ref->cur_version++;
        head->version=inode_ref->cur_version;
        raw->parent_id=inode;
        raw->command_type=0;
        raw->displacement=displacement;
        raw->data_length=data_size;
        
        raw_ref->obsolete=false;
        raw_ref->node_type.next_in_inode=inode_ref->nodes;
        inode_ref->nodes=raw_ref;    
        raw_ref->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header)+data_size;
        ret=write_node(raw_ref,buf);
        free(buf);
    }
    else
    {
        int loc_disp=0;
        int delta=flash_drive->get_block_size()-sizeof(struct node_header)-sizeof(struct raw_node_header);
        while(loc_disp<delta);
        {
            int buf_size;
            if(data_size-loc_disp>delta)
            {
                buf_size=delta;
            }
            else
            {
                buf_size=data_size-loc_disp;
            }
            char* buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header)+buf_size);
            node_header* head=(node_header*)buf;
            raw_node_header* raw=(raw_node_header*)(buf+sizeof(struct node_header));
            memcpy(buf+sizeof(struct node_header)+sizeof(struct raw_node_header),data+loc_disp,buf_size);
            raw_node_ref* raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
            inode_cache* inode_ref=inode_cache_map.find(inode)->second;
            
            head->node_type=0;
            head->compression_type=0;
            inode_ref->cur_version++;
            head->version=inode_ref->cur_version;
            raw->parent_id=inode;
            raw->command_type=0;
            raw->displacement=displacement+loc_disp;
            raw->data_length=buf_size;
        
            raw_ref->obsolete=false;
            raw_ref->node_type.next_in_inode=inode_ref->nodes;
            inode_ref->nodes=raw_ref;    
            raw_ref->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header)+buf_size;
            ret=write_node(raw_ref,buf);
            if(ret=1)
            {
                return ret;
            }
            free(buf);
            loc_disp+=delta;
        }
    }
    
    inode_cache* inode_ref=inode_cache_map.find(inode)->second;
    
    if(inode_ref->cur_size<displacement)
    {
        char* buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header));
        node_header* head=(node_header*)buf;
        raw_node_header* raw=(raw_node_header*)(buf+sizeof(struct node_header));
        raw_node_ref* raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
        inode_cache* inode_ref=inode_cache_map.find(inode)->second;
        
        head->node_type=0;
        head->compression_type=1;
        inode_ref->cur_version++;
        head->version=inode_ref->cur_version;
        raw->parent_id=inode;
        raw->command_type=0;
        raw->displacement=inode_ref->cur_size;
        raw->data_length=displacement-inode_ref->cur_size;
        raw_ref->obsolete=false;
        raw_ref->node_type.next_in_inode=inode_ref->nodes;
        inode_ref->nodes=raw_ref;    
        raw_ref->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
        ret=write_node(raw_ref,buf);
        free(buf);
    }
    sort_obsolete_nodes(inode_ref);    
    return ret;
};


int ffs::read_data(int inode,char* data, size_t data_size,size_t displacement)
{
    inode_cache* inode_ref=inode_cache_map.find(inode)->second;
    if(displacement>inode_ref->cur_size)return 1;
    raw_node_ref* raw_ref=inode_ref->nodes;
    while(raw_ref!=NULL)
    {
        char* buf=(char*)malloc(raw_ref->total_length);
        flash_drive->read(raw_ref->phys_offset,buf,raw_ref->total_length);
        node_header* node=(node_header*)buf;
        raw_node_header* raw=(raw_node_header*)(buf+sizeof(struct node_header));
        if(raw->command_type==0)
        {
            if((displacement>=raw->displacement)&&((displacement+data_size)<=(raw->displacement+raw->data_length)))
            {
                
                int loc_disp=displacement-raw->displacement;
                int size=data_size;
                
                if(node->compression_type==0)
                {
                    memcpy(data,buf+sizeof(struct node_header)+sizeof(struct raw_node_header)+loc_disp,size);
                }
                else
                {
                    for(int i=0;i<size;i++)
                    {
                        data[i]=0;
                    }
                }
            }
            else if((displacement<=raw->displacement)&&(displacement+data_size>=raw->displacement+raw->data_length))
            {
                
                int loc_disp=raw->displacement-displacement;
                int size=raw->data_length;
                if(node->compression_type==0)
                {
                    memcpy(data+loc_disp,buf+sizeof(struct node_header)+sizeof(struct raw_node_header),size);
                }
                else
                {
                    for(int i=loc_disp;i<loc_disp+size;i++)
                    {
                        data[i]=0;
                    }
                }
                
            }
            else if((raw->displacement<displacement+data_size)&&(raw->displacement>=displacement))       
            {
                int loc_disp=raw->displacement-displacement;
                int size=displacement+data_size-raw->displacement;
                if(node->compression_type==0)
                {
                    memcpy(data+loc_disp,buf+sizeof(struct node_header)+sizeof(struct raw_node_header),size);
                }
                else
                {
                    for(int i=loc_disp;i<loc_disp+size;i++)
                    {
                        data[i]=0;
                    }
                }
            }
            else if((raw->displacement+raw->data_length>displacement)&&(raw->displacement+raw->data_length<=displacement+data_size))
            {
                int loc_disp=displacement-raw->displacement;
                int size=raw->displacement+raw->data_length-displacement;
                if(node->compression_type==0)
                {
                    memcpy(data,buf+sizeof(struct node_header)+sizeof(struct raw_node_header)+loc_disp,size);
                }
                else
                {
                    for(int i=0;i<size;i++)
                    {
                        data[i]=0;
                    }
                }
            }
        }
        free(buf);
        raw_ref=raw_ref->node_type.next_in_inode;
    }
    return 0;    
};


int ffs::truncate(int inode, size_t trunc_size)
{
    int ret=0;
    inode_cache* inode_ref=inode_cache_map.find(inode)->second;//Generate and add truncate node
    char* write_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header));
    node_header* new_node=(node_header*)write_buf;
    new_node->node_type=0;
    new_node->compression_type=0;
    new_node->version=inode_ref->cur_version+1;
    inode_ref->cur_version++;
    raw_node_header* new_raw_node=(raw_node_header*)(write_buf+sizeof(struct node_header)); 
    new_raw_node->parent_id=inode;
    new_raw_node->command_type=1;
    new_raw_node->displacement=trunc_size;
    new_raw_node->data_length=0;
    raw_node_ref* new_raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
    new_raw_ref->obsolete=false;
    new_raw_ref->node_type.next_in_inode=inode_ref->nodes;
    inode_ref->nodes=new_raw_ref;
    new_raw_ref->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
    int prev_size=inode_ref->cur_size;
    ret=write_node(new_raw_ref,write_buf);
    if(ret==1)return 1;
    
    free(write_buf);
    if(prev_size<trunc_size)//if truncate size is larger than previous size - add zeroes space to cover 
    {
        char* write_buf2=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header));
        
        new_node=(node_header*)write_buf2;
        new_node->node_type=0;
        new_node->compression_type=1;
        new_node->version=inode_ref->cur_version+1;
        inode_ref->cur_version++;
        raw_node_header* new_raw_node2=(raw_node_header*)(write_buf2+sizeof(struct node_header)); 
        new_raw_node2->parent_id=inode;
        new_raw_node2->command_type=0;
        new_raw_node2->displacement=prev_size;
        new_raw_node2->data_length=trunc_size-prev_size;
        raw_node_ref* zero_raw_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
        zero_raw_ref->obsolete=false;
        zero_raw_ref->node_type.next_in_inode=inode_ref->nodes;
        zero_raw_ref->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
        zero_raw_ref->node_type.next_in_inode=inode_ref->nodes;
        inode_ref->nodes=zero_raw_ref;
        ret=write_node(zero_raw_ref,write_buf2);
        if(ret==1)return 1;
        free(write_buf2);
    
    }
    sort_obsolete_nodes(inode_ref);    
    return 0;
};

int ffs::get_inode_num(string file_name,int root_id)
{
    for(map<int,map<int,map<int,struct dirent_cache*>*>* >::iterator it=dirent_cache_map.begin();it!=dirent_cache_map.end();it++)
    {
        map<int,map<int,struct dirent_cache*>*>::iterator it2=it->second->find(root_id);
        if(it2!=it->second->end())
        {
            for(map<int,struct dirent_cache*>::iterator it3=it2->second->begin();it3!=it2->second->end();it3++)
            {
                char* read_buf=(char*)malloc(it3->second->phys_node->total_length);
                flash_drive->read(it3->second->phys_node->phys_offset,read_buf,it3->second->phys_node->total_length);
                char* alias_ptr=read_buf+sizeof(struct node_header)+sizeof(struct direntry_header);
                if(memcmp(alias_ptr,file_name.c_str(),it3->second->phys_node->total_length-sizeof(struct node_header)-sizeof(struct direntry_header))==0)
                {   
                    free(read_buf);
                    return it->first;
                }
                free(read_buf);
            }
        }
    }
    return -1;
    
};


int ffs::delete_link(string file_name,int root_id)
{
    int ret=0;
    int del_ver;
    int del_id;
    int del_inode_targ;
    bool deleted=false;
    for(map<int,map<int,map<int,struct dirent_cache*>*>* >::iterator it=dirent_cache_map.begin();it!=dirent_cache_map.end();it++)
    {
        map<int,map<int,struct dirent_cache*>*>::iterator it2=it->second->find(root_id);
        if(it2!=it->second->end())
        {
            for(map<int,struct dirent_cache*>::iterator it3=it2->second->begin();it3!=it2->second->end();it3++)
            {
                char* read_buf=(char*)malloc(it3->second->phys_node->total_length);
                flash_drive->read(it3->second->phys_node->phys_offset,read_buf,it3->second->phys_node->total_length);
                char* alias_ptr=read_buf+sizeof(struct node_header)+sizeof(struct direntry_header);
                if(memcmp(alias_ptr,file_name.c_str(),file_name.size())==0)
                {
                    del_id=it3->first;
                    del_inode_targ=it->first;
                    del_ver=it3->second->version;
                    it2->second->erase(it3->first);
                    if(it2->second->size()==0)
                    {
                        it->second->erase(it2->first);
                    }
                    if(it->second->size()==0){
                        dirent_cache_map.erase(it->first);
                    }                    
                    deleted=true;
                    break;
                }
                free(read_buf);
            }
        }
        if(deleted)break;
    }
    if(!deleted)return 1;
    char* write_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct direntry_header));
    node_header* new_node=(node_header*)write_buf;
    new_node->node_type=1;
    new_node->compression_type=0;
    new_node->version=del_ver+1;
    direntry_header* new_dirent=(direntry_header*)(write_buf+sizeof(struct node_header));
    new_dirent->entry_id=del_id;
    new_dirent->target_id=del_inode_targ;
    new_dirent->root_id=root_id;
    new_dirent->data_length=0;
    dirent_cache* new_dirent_cache=(dirent_cache*)malloc(sizeof(struct dirent_cache));
    new_dirent_cache->version=del_ver+1;
    raw_node_ref* new_ref=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
    new_ref->obsolete=false;
    new_ref->node_type.dirent=new_dirent_cache;
    new_ref->total_length=sizeof(struct node_header)+sizeof(struct direntry_header);
    new_dirent_cache->phys_node=new_ref;
    ret=write_node(new_ref,write_buf);
    clear_orphaned_inodes();
    free(write_buf);
    return ret;
    
};


int ffs::write_node(raw_node_ref* raw_ref,char* buffer)
{
    block* land_block=find_place(raw_ref->total_length);            
    if(land_block==NULL)
    {
        int garbage_res=garbage_collect();    
        while(land_block==NULL&&garbage_res!=1)
        {
            land_block=find_place(raw_ref->total_length);
            garbage_res=garbage_collect();
        }
        if(garbage_res==1&&land_block==NULL)
        {
            return 1;
        }
    }
   
    raw_node_ref* marker=land_block->first_node;
    if(marker!=NULL)
    {
        //while(marker->next_in_phys!=NULL)
        //{
        //    marker=marker->next_in_phys;
        //}
        //raw_ref->phys_offset=marker->phys_offset+marker->total_length;
        raw_ref->phys_offset=land_block->offset+flash_drive->get_block_size()-land_block->free_space;
    }
    else
    {
        raw_ref->phys_offset=land_block->offset;   
    }
    raw_ref->next_in_phys=land_block->first_node;
    land_block->first_node=raw_ref;
    flash_drive->write(raw_ref->phys_offset,buffer,raw_ref->total_length); 
    land_block->free_space-=raw_ref->total_length;
    if(land_block->free_space>=sizeof(struct node_header))
    {
        node_header eob;
        eob.node_type=3;
        eob.compression_type=0;
        eob.version=0;
        flash_drive->write(raw_ref->phys_offset+raw_ref->total_length,(char*)&eob,sizeof(struct node_header));
    }
    return 0;    
}




void ffs::clear_orphaned_inodes()
{
    int c=0;
    bool orphans_deleted=false;
    vector<int> deleted;
    while(!orphans_deleted)
    {
        orphans_deleted=true;
        for(map<int,struct inode_cache*>::iterator it=inode_cache_map.begin();it!=inode_cache_map.end();it++)//clean up inodes without links
        {
            if(dirent_cache_map.count(it->first)==0&&(it->first!=0))//but not root directory;
            {
                inode_cache* orphan_node=it->second;
                raw_node_ref* ref=orphan_node->nodes;
                while(ref!=NULL)
                {
                    c++;
                    ref->obsolete=true;
                    ref=ref->node_type.next_in_inode;
                }
                inode_cache_map.erase(it);
                orphans_deleted=false;
                deleted.push_back(it->first);
            }
        }
        for(int i=0;i<deleted.size();i++)//clean up all rootless links
        {
            for(map<int,map<int,map<int,struct dirent_cache*>*>*>::iterator it=dirent_cache_map.begin();it!=dirent_cache_map.end();it++)
            {
                if(it->second->count(deleted.at(i))>0)
                {
                    it->second->erase(deleted.at(i));
                }   
            }
        }
        deleted.clear();
    }
    has_obsoletes=true;
};


block* ffs::find_place(int size)
{
    bool found=false;
    block* land_block=clean_list;   
    while(land_block!=NULL){
        if(land_block->free_space>size)
        {
            found=true;
            break;
            
        }
        land_block=land_block->next_block;
    }
    if(!found)
    {
        land_block=dirty_list;   
        while(land_block!=NULL){
            if(land_block->free_space>size)
            {
                found==true;
                has_obsoletes=true;
                break;
            }
            land_block=land_block->next_block;
        }
    }
    if(!found&&free_blocks>2)
    {
        free_blocks--;
        land_block=free_list;
        free_list=land_block->next_block;
        land_block->next_block=clean_list;
        clean_list=land_block;
        block* prev_block;   
        found=true;
    }
    if (found)
    {
        return land_block;
    }
    else
    {
        return NULL;
    }
}

void ffs::sort_dirty_blocks()
{
    if(clean_list!=NULL)
    {
        block* temp_clean_list=NULL;
        block* marker=clean_list;
    
        while(marker!=NULL)
        {
            block* temp_next=marker->next_block;
            raw_node_ref* marker2=marker->first_node;
            bool dirty=false;
            
            while(marker2!=NULL)
            {    
                if (marker2->obsolete)
                {
                    marker->next_block=dirty_list;
                    dirty_list=marker;
                    dirty=true;
                    break;
                }
                marker2=marker2->next_in_phys;
            }
            if(!dirty)
            {
                marker->next_block=temp_clean_list;
                temp_clean_list=marker;
            }
            marker=temp_next;
        }
        clean_list=temp_clean_list;
    }
}


void ffs::sort_obsolete_nodes(inode_cache* it)
{
    
    bool finished=false;
    while(!finished)
    {
    
    finished=true;
    raw_node_ref* valid=NULL;
    raw_node_ref* current=it->nodes;
    vector<int> versions,types,start,end;
    vector<raw_node_ref*> node_refs;
    while (current!=NULL){
        
        node_header* node=(node_header*)malloc(sizeof(struct node_header));
        raw_node_header* raw=(raw_node_header*)malloc(sizeof(struct raw_node_header));
        flash_drive->read(current->phys_offset,(char*)node,sizeof(struct node_header));
        flash_drive->read(current->phys_offset+sizeof(node_header),(char*)raw,sizeof(struct raw_node_header));
        versions.push_back(node->version);
        start.push_back(raw->displacement);
        end.push_back(raw->displacement+raw->data_length);    
        types.push_back(raw->command_type);
        raw_node_ref* temp_ref=current;
        node_refs.push_back(temp_ref);
        current=current->node_type.next_in_inode;
    }
    current=it->nodes;
    int i=0;
    while(current!=NULL)
    {   
        raw_node_ref* temp=current->node_type.next_in_inode;
        bool invalid=false;
        for(int j=0;j<versions.size();j++)
        {
            if(i!=j&&(versions.at(i)<versions.at(j)))//as long as it's not the same node and the version is higher
            {
                if(types.at(i)==0&&types.at(j)==0)//if older node intersects newer node - it's deemed obsolete
                {
                    if(start.at(i)<end.at(j)&&end.at(i)>end.at(j))//chip right
                    {
                
                        char* new_node_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header)+end.at(i)-end.at(j));
                        char* old_node_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header));
                        int disp=node_refs.at(i)->phys_offset+sizeof(struct node_header)+sizeof(struct raw_node_header)+end.at(j)-start.at(i);
                        flash_drive->read(node_refs.at(i)->phys_offset,old_node_buf,sizeof(struct node_header)+sizeof(struct raw_node_header));
                        node_header* old_head=(node_header*)old_node_buf;
                        node_header* head=(node_header*)new_node_buf;
                        head->version=old_head->version;
                        head->compression_type=old_head->compression_type;
                        raw_node_header* old_raw=(raw_node_header*)(old_node_buf+sizeof(struct node_header));
                        raw_node_header* raw_ref=(raw_node_header*)(new_node_buf+sizeof(struct node_header));
                        raw_ref->parent_id=old_raw->parent_id;
                        raw_ref->command_type=0;
                        raw_ref->displacement=end.at(j);
                        raw_ref->data_length=end.at(i)-end.at(j);
                        
                        raw_node_ref* raw_cache=(raw_node_ref*)malloc(sizeof(struct raw_node_ref));
                        raw_cache->obsolete=false;
                        raw_cache->node_type.next_in_inode=valid;
                        valid=raw_cache;
                        
                        if(old_head->compression_type==0)
                        {
                            flash_drive->read(disp,new_node_buf+sizeof(struct node_header)+sizeof(struct raw_node_header),end.at(i)-end.at(j));
                            raw_cache->total_length=sizeof(struct node_header)+sizeof(struct raw_node_ref)+end.at(i)-end.at(j);
                        }
                        else
                        {
                            raw_cache->total_length=sizeof(struct node_header)+sizeof(struct raw_node_ref);
                        }
                        write_node(raw_cache,new_node_buf);  
                        free(old_node_buf);
                        free(new_node_buf);
                        current->obsolete=true;
                        invalid=true;
                        finished=false;
                        
                    }
                    if(start.at(i)<start.at(j)&&end.at(i)>start.at(j))//chip left 
                    {
                    cout<<"OBSOLETED"<<endl;
                
                        char* new_node_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i));
                        int disp=node_refs.at(i)->phys_offset;
                        flash_drive->read(disp,new_node_buf,sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i));
                        node_header* head=(node_header*)new_node_buf;
                        raw_node_header* raw_head=(raw_node_header*)(new_node_buf+sizeof(struct node_header));
                        raw_head->data_length=start.at(j)-start.at(i);
                        if(head->compression_type==0)
                        {
                            node_refs.at(i)->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i);
                        }
                        else
                        {
                            node_refs.at(i)->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
                        }
                        write_node(node_refs.at(i),new_node_buf);
                        free(new_node_buf);
                        current->obsolete=false;
                        invalid=false;
                        finished=false;
                    }
                    if(start.at(i)>=start.at(j)&&end.at(i)<=end.at(j))
                    {
                        current->obsolete=true;
                        invalid=true;
                        break;
                    }
                }
                else if(types.at(j)==2)//If we found newer delete command - all previous nodes are obsolete
                {
                   current->obsolete=true;
                   invalid=true;
                   break;
                }
                else if (types.at(i)==1&&types.at(j)==1)//Older truncate is obsoleted by newer
                {
                   current->obsolete=true;
                   invalid=true;
                   break;
                }
                else if(types.at(j)==1&&types.at(i)==0&&(end.at(i)>start.at(j)))//If node is further than truncated space - it's deemed obsolete
                {
                    if(start.at(i)<start.at(j))
                    {
                        char* new_node_buf=(char*)malloc(sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i));
                        int disp=node_refs.at(i)->phys_offset;
                        flash_drive->read(disp,new_node_buf,sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i));
                        node_header* head=(node_header*)new_node_buf;
                        raw_node_header* raw_head=(raw_node_header*)(new_node_buf+sizeof(struct node_header));
                        raw_head->data_length=start.at(j)-start.at(i);
                        if(head->compression_type==0)
                        {
                            node_refs.at(i)->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header)+start.at(j)-start.at(i);
                        }
                        else
                        {
                            node_refs.at(i)->total_length=sizeof(struct node_header)+sizeof(struct raw_node_header);
                        }
                        write_node(node_refs.at(i),new_node_buf);
                        free(new_node_buf);
                        finished=false;
                    }
                    else
                    {
                        current->obsolete=true;
                        invalid=true;
                        break;    
                    }
                   
                }
                else if(types.at(i)==2)//If there's any command after delete command - delete is deemed obsolete
                {
                   current->obsolete=true;
                   invalid=true;
                   break;
                }
           }
        }
        if(!invalid)
        {
            current->node_type.next_in_inode=valid;
            valid=current;
        }
        current=temp;
        i++;
   }
   it->nodes=valid;
   
   }
   raw_node_ref* current=it->nodes;
   it->cur_size=0;
   while (current!=NULL){
        node_header* node=(node_header*)malloc(sizeof(struct node_header));
        raw_node_header* raw=(raw_node_header*)malloc(sizeof(struct raw_node_header));
        flash_drive->read(current->phys_offset,(char*)node,sizeof(struct node_header));
        flash_drive->read(current->phys_offset+sizeof(node_header),(char*)raw,sizeof(struct raw_node_header));
        if(raw->command_type==0)
        {
            
            if (it->cur_size<raw->displacement+raw->data_length)
            {
                it->cur_size=raw->displacement+raw->data_length;
            }
        }
        else if(raw->command_type==1)
        {
            
            if(it->cur_size<raw->displacement)
            {
                it->cur_size=raw->displacement;
       
            }
        }
        current=current->node_type.next_in_inode;
   }
   has_obsoletes=true; 
}



int ffs::garbage_collect()
{
    if(has_obsoletes)sort_dirty_blocks();//if there are obsolete nodes in clean list - we need to sort it
    has_obsoletes=false;
    int old_free=free_blocks;
    block* garbage_list;
    if(garbage_collects%100==0&&clean_list!=NULL)//Once in a while we need to fuss-up the static data
    {
        garbage_collects=0;
        block* clean_temp=clean_list;
        clean_list=clean_list->next_block;
        block* free_temp=free_list;
        free_list=free_temp->next_block;
        size_t clean_disp=clean_temp->offset;//clean block offset
        size_t free_disp=free_temp->offset;//free block offset
        char* buffer=(char*)malloc(flash_drive->get_block_size());
        flash_drive->read(clean_disp,buffer,flash_drive->get_block_size());//read full clean block
        flash_drive->write(free_disp,buffer,flash_drive->get_block_size());//dump it in the free block
        free_temp->offset=clean_disp;//change offset in free block
        raw_node_ref* mark=clean_temp->first_node;
        while(mark!=NULL)//change offsets in clean block
        {
            mark->phys_offset=free_disp;
            free_disp+=mark->total_length;
            mark=mark->next_in_phys;    
        }
        
        node_header clean_marker;
        clean_marker.node_type=2;
        clean_marker.version=0;
        clean_marker.compression_type=0;
        flash_drive->erase(clean_disp);    
        flash_drive->write(clean_disp,(char*)&clean_marker,sizeof(struct node_header));
        //dump new block in the ends of respective lists
        block* block_mark=clean_list;
        while(block_mark->next_block!=NULL)
        {
            block_mark=block_mark->next_block;
        }
        block_mark->next_block=clean_temp;
        clean_temp->next_block=NULL;
        
        block_mark=free_list;
        while(block_mark->next_block!=NULL)
        {
            block_mark=block_mark->next_block;
        }
        block_mark->next_block=free_temp;
        free_temp->next_block=NULL;
        free(buffer);
    }
    garbage_collects++;
    //God save me if this thing is bugged
    if(dirty_list==NULL)return 1;//If we've got nothing to collect, we're out of memory
    block* dirty_temp=dirty_list;
    dirty_list=dirty_list->next_block;
    block* free_temp=free_list;    
    free_list=free_list->next_block;
    char* buffer=(char*)malloc(flash_drive->get_block_size());   //buffer where we will copy original block
    char* buffer2=(char*)malloc(flash_drive->get_block_size()); //buffer that will contain generated clean block
    size_t dirty_disp=dirty_temp->offset; //dirty block offset
    size_t free_disp=free_temp->offset;   //free block offset
    size_t disp=0; //offset in clean buffer
    flash_drive->read(dirty_disp,buffer,flash_drive->get_block_size()); //get original block
    bool free=false;
    raw_node_ref* cur_mark=dirty_temp->first_node; // We need to sort out obsolete nodes
    raw_node_ref* prev_mark;
    
    while(cur_mark->obsolete==true)//skip a few first ones to settle in
    {
        dirty_temp->free_space+=cur_mark->total_length;
        cur_mark=cur_mark->next_in_phys;
        dirty_temp->first_node=cur_mark;
           
    }
    
    if(cur_mark==NULL)//if we're lucky enough - we've cleared the whole block
    {
        cout<<"FREED"<<endl;
        free=true;
    }
    else//if not - we're carrying on
    {
        prev_mark=cur_mark;
    }
    
    
    if(free)//if both blocks are free
    {
        cout<<"TEST"<<endl;
        free_blocks++;
        dirty_temp->first_node=NULL;
        dirty_temp->free_space=flash_drive->get_block_size();
        block* block_mark=free_list;
        flash_drive->erase(dirty_disp);//erase previous dirty block
        node_header* clean_marker=(node_header*)malloc(sizeof(struct node_header));
        clean_marker->node_type=2;
        clean_marker->version=0;
        clean_marker->compression_type=0;
        flash_drive->write(dirty_disp,(char*)clean_marker,sizeof(struct node_header));//dump the clean marker there
        
        while(block_mark->next_block!=NULL)
        {
            block_mark=block_mark->next_block;
        }
        block_mark->next_block=free_temp;
        free_temp->next_block=dirty_temp;
        dirty_temp->next_block=NULL;
        
    }
    else//else - we need to finish cleaning the block
    {
        while(cur_mark!=NULL)//clean the obsolete nodes
        {
            if(cur_mark->obsolete==false)
            {
                memcpy((void*)(buffer2+disp),(void*)(buffer+cur_mark->phys_offset-dirty_disp),cur_mark->total_length);
                cur_mark->phys_offset=free_disp+disp;
                disp+=cur_mark->total_length;
                prev_mark=cur_mark;
            }
            else
            {
                dirty_temp->free_space+=cur_mark->total_length;
                prev_mark->next_in_phys=cur_mark->next_in_phys;
            }        
            cur_mark=cur_mark->next_in_phys;
        }
    
        flash_drive->write(free_disp,buffer2,flash_drive->get_block_size());//dump the clean buffer to the new clean block
        flash_drive->erase(dirty_disp);//erase previous dirty block
        node_header* clean_marker=(node_header*)malloc(sizeof(struct node_header));
        clean_marker->node_type=2;
        clean_marker->version=0;
        clean_marker->compression_type=0;
        flash_drive->write(dirty_disp,(char*)clean_marker,sizeof(struct node_header));
        free_temp->offset=dirty_disp;
        block* block_mark=clean_list;//hook them up to ends of the lists
        if(block_mark!=NULL){
            while(block_mark->next_block!=NULL)
            {
                block_mark=block_mark->next_block;
            }
            block_mark->next_block=dirty_temp;
            dirty_temp->next_block=NULL;
        }
        else{
            clean_list=dirty_temp;
            dirty_temp->next_block=NULL;
        }
        block_mark=free_list;
        while(block_mark->next_block!=NULL)
        {
            block_mark=block_mark->next_block;
        }
        block_mark->next_block=free_temp;
        free_temp->next_block=NULL;
        free_temp->free_space=flash_drive->get_block_size();
    }
    
    delete(buffer);
    delete(buffer2);
    
    return 0;
};


vector<string> ffs::list_files(int root_id)
{
    vector<string> result;
    for(map<int,map<int,map<int,struct dirent_cache*>*>* >::iterator it=dirent_cache_map.begin();it!=dirent_cache_map.end();it++)
    {
        map<int,map<int,struct dirent_cache*>*>::iterator it2=it->second->find(root_id);
        
        if(it2!=it->second->end())
        {
            for(map<int,struct dirent_cache*>::iterator it3=it2->second->begin();it3!=it2->second->end();it3++)
            {
                char* read_buf=(char*)malloc(it3->second->phys_node->total_length);
                flash_drive->read(it3->second->phys_node->phys_offset,read_buf,it3->second->phys_node->total_length);
                char* alias_ptr=read_buf+sizeof(struct node_header)+sizeof(struct direntry_header);
                string temp=alias_ptr;
                result.push_back(temp);
            }
        }
        
    }
    return result;        
}
int ffs::get_file_size(int inode)
{
    int size=inode_cache_map.find(inode)->second->cur_size;
};
int ffs::get_file_version(int inode)
{
    int size=inode_cache_map.find(inode)->second->cur_version;
};

block_statistics* ffs::get_block_stat(int block_num)
{
    block_statistics* stat=(block_statistics*)malloc(sizeof(struct block_statistics));
    size_t offset=block_num*flash_drive->get_block_size();    
    bool found=false;
    block* marker=free_list;
    while(marker!=NULL)
    {
        if(marker->offset==offset)
        {
            stat->state=0;
            stat->free_space=marker->free_space;
            found=true;
            break;   
        }
        marker=marker->next_block;
    }
    if(!found)
    {
        marker=clean_list;
        while(marker!=NULL)
        {
            if(marker->offset==offset)
            {
                stat->state=1;
                stat->free_space=marker->free_space;
                found=true;
                break;   
            }
            marker=marker->next_block;
        }
    }
    if(!found)
    {
        marker=clean_list;
        while(marker!=NULL)
        {
            if(marker->offset==offset)
            {
                stat->state=2;
                stat->free_space=marker->free_space;
                found=true;
                break;   
            }
            marker=marker->next_block;
        }
    }
    return stat;
};
    
    

    
