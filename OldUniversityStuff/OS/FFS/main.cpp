#include <iostream>
#include "disk_emulator.h"
#include "ffs.h"
#define BLOCK_SIZE 8096
using std::cin;
using std::cout;
using std::endl;
int main(int argc, char *argv[])
{
    int current_directory=0;
    block_device* bd=new block_device(BLOCK_SIZE*4,BLOCK_SIZE,512);
    ffs* file_system=new ffs(bd);
    bool isFinished=false;
	string ans;
	cout << "init,format,mount,unmount,create,link,unlink,open,read,write,writeblock,truncate,ls,cd,blockstat,filestat,exit" << endl;
	vector<int> path;		
	while(!isFinished){
		cin >> ans;
		if (ans == "exit")
		{
			isFinished=true;	
		}
		else if (ans == "init") 
		{
	        bd->init();    
	    }
		else if (ans == "format") 
		{
			file_system->format();
		}
		else if (ans == "mount") 
		{
            file_system->mount();
		}
		else if (ans == "unmount") 
		{
		    file_system->unmount();
		}
		else if (ans == "create")
		{
		    string name;
		    cin>>name;
		    int res=file_system->create_inode(name,current_directory);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		}
		else if (ans == "ls")
		{
		    vector<string> file_list=file_system->list_files(current_directory);
		    cout<<"Entries found: "<<file_list.size()<<endl;
		    for(int i=0;i<file_list.size();i++)
		    {
		        cout<<file_list.at(i)<<endl;
		    }
		}
		else if (ans =="cd")
		{
		    string name;
		    cin>>name;
		    if(name=="..")
		    {
		        if (path.size()>0)
		        {
		            current_directory=path.back();
		            path.pop_back();
		        }
		        else
		        {
		            current_directory=0;
		        }
		    }
		    else
		    {
		        int num=file_system->get_inode_num(name,current_directory);
		        path.push_back(current_directory);
		        current_directory=num;
		    }
		}
		else if (ans == "open")
		{
		    string name;
		    cin>>name;
		    int num=file_system->get_inode_num(name,current_directory);
		    cout<<"Inode number: "<<num<<endl;
		}
		else if (ans == "link")
		{
		    int inode_id;
		    cin>>inode_id;
		    string new_name;
		    cin>>new_name;
		    
		    int res=file_system->create_link(inode_id,new_name,current_directory);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		}
		else if (ans == "unlink")
		{
		    string name;
		    cin>>name;
		    int res=file_system->delete_link(name,current_directory);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		}
		else if (ans == "truncate")
		{
		    string new_name;
		    cin>>new_name;
		    int new_size;
		    cin>>new_size;
		    int inode;
		    inode=file_system->get_inode_num(new_name,current_directory);
		    int res=file_system->truncate(inode,new_size);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		    
		}
		else if (ans == "filestat")
		{
		    string new_name;
		    cin>>new_name;
		    
		    int inode;
		    inode=file_system->get_inode_num(new_name,current_directory);
		    cout<<"Size: "<<file_system->get_file_size(inode)<<endl;
		    cout<<"Version: "<<file_system->get_file_version(inode)<<endl;
		}
		else if (ans == "write")
		{
		    int inode;
		    cin>>inode;
		    int disp;
		    cin>>disp;
		    string data;
		    cin>>data;
		    char* buf=(char*)data.c_str();
		    int res=file_system->write_data(inode,buf,data.size(),disp);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		
		}
		else if (ans == "writeblock")
		{
		    int inode;
		    cin>>inode;
		    int disp;
		    cin>>disp;
		    int count;
		    cin>>count;
		    string data;
		    cin>>data;
		    char* buf=(char*)data.c_str();
		    for(int i=0;i<count;i++){
		        int res=file_system->write_data(inode,buf,data.size(),disp+i*data.size());
		        //cout<<i<<endl;
		        if (res==1)
		        {
		            
		            cout<<"Out of memory"<<endl;
		        }
		    }
		
		}
		else if (ans == "read")
		{
		    int inode;
		    cin>>inode;
		    int disp;
		    cin>>disp;
		    int size;
		    cin>>size;
		    char* buf=(char*)malloc(size);
		    
		    int res=file_system->read_data(inode,buf,size,disp);
		    if (res==1)
		    {
		        cout<<"Out of memory"<<endl;
		    }
		    else
            {
                buf[size]=NULL;
                string s=buf;
                cout<<s<<endl;    
            }
		}
		else if (ans == "blockstat")
		{
		    int num;
		    cin>>num;
		    block_statistics* stat=file_system->get_block_stat(num);
		    cout<<"Block #"<<num<<".State:";
		    if(stat->state==0)
		    {
		        cout<<"free.";
		    }
		    else if(stat->state==1)
		    {
		        cout<<"clean.";
		    }
		    else
		    {
		        cout<<"dirty.";
		    }
		    cout<<" Free space: "<< stat->free_space<<endl;
		}
		else {
			cout << "invalid input" << endl;
		}
		
	}
    return 0;
}
