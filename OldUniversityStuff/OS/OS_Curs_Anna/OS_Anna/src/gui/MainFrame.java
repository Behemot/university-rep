package gui;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.MouseInputAdapter;


import data.GraphConverter;
import data.MHscheduler;
import data.Serializer;

import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.event.MouseEvent;
import java.io.File;
import data.Node;
import java.util.ArrayList;
public class MainFrame extends JFrame{
	private static MainFrame frame_=new MainFrame();
	private GraphEd graphEd_=new GraphEd();
	private JButton convertButton_=new JButton("Convert graph");
	private JButton scheduleButton_=new JButton("Start scheduling");
	private JButton saveButton_=new JButton("Save");
	private JButton loadButton_=new JButton("Load");
	private JTextField procNum_=new JTextField();
	private Serializer serializer_=new Serializer();
	private ArrayList<Node> nodes_;
	private MainFrame(){
		this.setSize(640,480);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		JTabbedPane tabs_=new JTabbedPane();
		tabs_.addTab("Editor", new JScrollPane(graphEd_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		tabs_.addTab("Parameters",new JPanel().add(procNum_));
		JPanel buttonPanel=new JPanel();
		buttonPanel.add(convertButton_);
		buttonPanel.add(scheduleButton_);
		buttonPanel.add(saveButton_);
		buttonPanel.add(loadButton_);
		convertButton_.addMouseListener(new MouseInputAdapter(){
			@Override
			public void mousePressed(MouseEvent E){
				MainFrame.getInstance().convert();
			}
		});
		scheduleButton_.addMouseListener(new MouseInputAdapter(){
			@Override
			public void mousePressed(MouseEvent E){
				MainFrame.getInstance().startScheduling();
			}
		});
		saveButton_.addMouseListener(new MouseInputAdapter(){
			@Override
			public void mousePressed(MouseEvent E){
				MainFrame.getInstance().save();
			}
		});
		loadButton_.addMouseListener(new MouseInputAdapter(){
			@Override
			public void mousePressed(MouseEvent E){
				MainFrame.getInstance().load();
			}
		});
		GroupLayout g=new GroupLayout(this.getContentPane());
		g.setVerticalGroup(g.createSequentialGroup()
				.addComponent(buttonPanel)
				.addComponent(tabs_));
		g.setHorizontalGroup(g.createParallelGroup()
				.addComponent(buttonPanel)
				.addComponent(tabs_));
		buttonPanel.setMaximumSize(new Dimension(this.getWidth(),this.getHeight()/10));
		this.getContentPane().add(buttonPanel);
		this.getContentPane().add(tabs_);
		this.getContentPane().setLayout(g);
		this.setVisible(true);
	}
	public static MainFrame getInstance(){
		return frame_;
	}
	public void convert(){
		nodes_=GraphConverter.convert(graphEd_.getGraph()); 
	}
	public void startScheduling(){
		MHscheduler scheduler=new MHscheduler(nodes_);
		scheduler.createSchedule();
		scheduler.generateReport();
	}
	public void save(){
		File S;
		FileDialog SDialog = new FileDialog(this);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			serializer_.serialize(S, nodes_);
		}
	}
	public void load(){
		JOptionPane ConfirmPane = new JOptionPane(null,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		ConfirmPane
				.setMessage("Do you want to save current chart before loading new one?");
		JDialog dialog = ConfirmPane.createDialog(this, "Save");
		dialog.setVisible(true);
		try {
			if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.CANCEL_OPTION) {
			} else {
				if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.YES_OPTION) {
					this.save();
				}
				FileDialog FDialog = new FileDialog(this, "Open");
				FDialog.setMode(FileDialog.LOAD);
				FDialog.setVisible(true);
				if (FDialog.getFile() != null) {
					File F = new File(FDialog.getFile());
					nodes_ = serializer_.deserialize(F);
					this.repaint();
				} else {
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
					ErrorPane.setMessage("Incorrect filename");
					JDialog errorDialog = ErrorPane.createDialog(this,
							"Error");
					errorDialog.setVisible(true);
				}
			}

		} catch (NullPointerException NP) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
			ErrorPane.setMessage("Critical failure while loading.");
			JDialog errorDialog = ErrorPane.createDialog(this, "Error");
			errorDialog.setVisible(true);
			
		}
	}
}
