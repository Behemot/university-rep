package data;
import java.util.ArrayList;
import java.io.Serializable;

public class Node implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3388768794943281957L;
	private int id_;
	private int sl_;
	private int weight_;
	private ArrayList<Connection> in_=new ArrayList<Connection>();
	private ArrayList<Connection> out_=new ArrayList<Connection>(); 
	public Node(int weight,int id){
		weight_=weight;
		id_=id;
	}
	public void setSL(int sl){
		sl_=sl;
	}
	public void addParent(Connection c){
		in_.add(c);
	}
	public void addChild(Connection c){
		out_.add(c);
	}
	public ArrayList<Connection> getParents(){
		return in_;
	}
	public ArrayList<Connection> getChildren(){
		return out_;
	}
	public int getWeight(){
		return weight_;
	}
	public int getSL(){
		return sl_;
	}
	public int getID(){
		return id_;
	}
}
