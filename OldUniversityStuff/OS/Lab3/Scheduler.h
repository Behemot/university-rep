#include <time.h>
#include <limits.h>
#include <stdlib.h>
#include <vector>
//That's the class that represent the task and manages all thing considering timing and stuff.

using namespace std;

//Time quantum for the cycling
const int quantum=600000;//600 microseconds=600000 nanoseconds
//Maximum task priority
const int max_priority=10;
//Number of cycles the task remains in the first queue
const int first_queue_cycles=3;
//Number of cycles the task remains in the second queue
const int second_queue_cycles=6;
//Number of cycles the task remains in the third queue
const int third_queue_cycles=20;


class incoming_task{
    int cycles_left;
    int time_needed;
    int priority;
    
    
    public:
        //Basic constructor
        incoming_task(int time,int prior){
            cycles_left=first_queue_cycles;
            priority=prior;
            time_needed=time;
        }
        //Getters and setters
               
        void set_cycles_left(int left){
            cycles_left=left;
        }
        
        int get_priority(){
            return priority;
        }
        
        void set_priority(int prior){
            priority=prior;
        }
        
        int get_time_needed(){
            return time_needed;
        }
        
        //Other stuff
        
        bool cycle(){
            cycles_left-=1;
            time_needed-=quantum;
            if(cycles_left){
                return true;
            }
            else{
                return false;
            }
        }
        
        
        
        
        
            
};


extern vector<incoming_task*> queue1,queue2,queue3;

