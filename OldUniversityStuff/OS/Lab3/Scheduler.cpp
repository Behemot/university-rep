#include "Scheduler.h"
//Scheduler queues
vector<incoming_task*> queue1,queue2,queue3;
vector<incoming_task*>::iterator it;
//Timer structures
timespec b_time,a_time;
//Waiting routine
void wait_time(int time){
    clock_gettime(CLOCK_MONOTONIC,&a_time);
    clock_gettime(CLOCK_MONOTONIC,&b_time);
    while((b_time.tv_nsec-a_time.tv_nsec)<time){
        clock_gettime(CLOCK_MONOTONIC,&b_time);
    }
}

//One quantum cycle
void cycle(){
    int time;//That's the time we'll be waiting this cycle
    //If there's something in the first queue
    
    if(!queue1.empty()){
        incoming_task* buf = queue1.at(0);
        it=queue1.begin();
        time=(*queue1.at(0)).get_time_needed();
        //If we're not out of cycle's - push to the end of the line
        if((*queue1.at(0)).cycle()){
            queue1.erase(it);
            queue1.push_back(buf);
        }
        else{//else push to the end of the next queue
            queue1.erase(it);
            (*buf).set_cycles_left(second_queue_cycles);
            queue2.push_back(buf);
        }
    }
    else if (!queue2.empty()){//If there's something in the second queue
        incoming_task* buf = queue2.at(0);
        it=queue2.begin();
        time=(*queue2.at(0)).get_time_needed();
        //If we're not out of cycle's - push to the end of the line
        if((*queue2.at(0)).cycle()){
            queue2.erase(it);
            queue2.push_back(buf);
        }
        else{//else push to the end of the next queue
            queue2.erase(it);
            (*buf).set_cycles_left(third_queue_cycles);
            queue3.push_back(buf);
        }
    }
    else if (!queue3.empty()){//If there's something in the third queue
        incoming_task* buf = queue3.at(0);
        it=queue3.begin();
        time=(*queue3.at(0)).get_time_needed();
        //If we're not out of cycle's - push to the end of the line
        if((*queue3.at(0)).cycle()){
            queue3.erase(it);
            queue3.push_back(buf);
        }
        else{//else - nowhere to push so just throw out
            queue3.erase(it);
        }
        
    }
    //If there's less time than quantum - wait that long, else wait quantum time
    if(time<quantum){
        wait_time(time);
    }
    else{
        wait_time(quantum);
    }
}

