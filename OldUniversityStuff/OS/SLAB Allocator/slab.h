#include <stdlib.h>

//Page descriptor structure
struct page_descriptor {
    char state; // page state 
                // 0 -free;
                // 1 -fractioned;
                // 2 -head of multipage block;
                
    size_t fraction_size; //size of fraction block;
    struct block_descriptor* first_free_block; //first free block address;
    size_t num_of_next_pages; //number of pages of the same type (multipage mode or free pages);
    struct page_descriptor* next_free_space; // next free page spaces
    
};

//Block descriptor structure
struct block_descriptor {
    block_descriptor* next_free_block; //address of the next free block in the page 
};


//Variables and constants
const size_t totalmem_size=1048576; //Total memory size
const size_t page_size=4096; //Page size
extern char* memory_start; //Memory beginning adress
const size_t page_descriptor_size=sizeof(struct page_descriptor);//Page descriptor size
const size_t block_descriptor_size=sizeof(struct block_descriptor);//Block descriptor size

//Function headers
void* mem_alloc(size_t size);//Memory allocation routine
void* mem_realloc(void* adress,size_t size);//Memory reallocation routine
void mem_free(void* adress);//Memory discharge routine
void mem_init();

