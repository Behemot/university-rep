#include "Auxiliary.h"
#include "slab.h"
#include <iostream>
#include <string>
#include <vector>
using std::string;
using std::endl;
using std::cin;
using std::cout;
using std::vector;

//Prints memory state
void mem_dump(){
    page_descriptor* ptr=(page_descriptor*)memory_start; 
    cout<<"----------------------------------------------"<<endl;
    size_t num=0;
    while((char*)ptr<(memory_start+totalmem_size-page_size)){//mark all pages as clear
        switch (ptr->state){
            case 0: {cout<<"Free , "; num=ptr->num_of_next_pages;};break;
            case 1: {cout<<"Frac , "; num=1;} break;
            case 2: {cout<<"Mult , "; num=ptr->num_of_next_pages;}; break;
        }
        cout<<"Frac size: "<< ptr->fraction_size<<" , ";
        cout<<"Num_page: "<< ptr->num_of_next_pages<<endl;
        ptr=(page_descriptor*)((char*)ptr+(num)*page_size);
    }
    cout<<"----------------------------------------------"<<endl;
    
}
//Command promts
void command_promts(){
	bool isFinished=false;
	string ans;
	vector<void *> ptrs;
	cout << "allocate, free, exit, dump,reallocate,test" << endl;
			
	while(!isFinished){
		cin >> ans;
		if (ans == "exit"){
			isFinished=true;	
		}
		else if (ans == "allocate") {
			size_t s;
			cin >> s;
			void* ptr=mem_alloc(s);
			ptrs.push_back(ptr);
		}
		else if (ans == "free") {
			size_t i;
			cin >> i;
			mem_free(ptrs.at(i));
			ptrs.at(i) = 0;
		}
		else if (ans == "reallocate") {
			size_t i;
			size_t s;
			cin >> i;
			cin >> s;
			void* ptr=mem_realloc(ptrs.at(i),s);
			ptrs.at(i) = ptr;
		}
		else if (ans == "dump") {
			mem_dump();
		}
		else if (ans == "test") {
			test();
		}
		else {
			cout << "invalid input" << endl;
		}
	}
}

void test(){
	char* ptrs[100];
	char checksum[100];
	for (int i=0;i<100;i++){
		ptrs[i]=NULL;	
	}
	//srandom(getpid());
	for(int j=0;j<10000;j++){
			int i=j%100;
			if (ptrs[i]==NULL){
				size_t s=rand()%100+20;
				ptrs[i]=(char*)mem_alloc(s);
				
				if (ptrs[i]!=NULL){
					char check=rand()%100+'0';				
					
					//*(ptrs[i])=check;
					//checksum[i]=check;
				}
							
			}
			else{
				
				if (*(ptrs[i])!=checksum[i]){
					//cout<<"Checksum fail "<< i << " " << *(ptrs[i])<< " " << checksum[i]<<  endl;				
				}
							
		        mem_free(ptrs[i]);
				ptrs[i]=NULL;
							
			}
	}
	
}


