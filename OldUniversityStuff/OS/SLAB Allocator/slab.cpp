#include "slab.h"
#include <math.h>
#include <vector>
#include <map>
#include <iostream>
using std::map;
using std::vector;
using std::pair;
using std::make_pair;
using std::cout;
using std::endl;


char* memory_start=NULL;
map<size_t,vector<page_descriptor*> > blocktype_hash;// hashmap for free pages with blocks of given type
map<size_t,vector<page_descriptor*> >:: iterator blocktype_iter;//hashmap iterator
page_descriptor* first_free_page; //first free page descriptor

void fracture_page(page_descriptor* page, size_t block_size);
void add_free_pages(page_descriptor* page,int N);

//Memory allocation routine
void* mem_alloc(size_t size){
    //cout<<"alloc"<<endl;
    //Marshal size to block type;
    size_t blck_size=pow(2,(int)(log2(size)+1));
    if (blck_size<block_descriptor_size){blck_size=block_descriptor_size;};//block should be at least as big as block descriptor;
    
    //if block is less than half page size
    if (blck_size<=((page_size-page_descriptor_size)/2)){
        blocktype_iter=blocktype_hash.find(blck_size); //Try to find neccessary block size in cache;
        //if found - try to allocate block;
        if (blocktype_iter!=blocktype_hash.end()){
           if(!(blocktype_iter->second).empty()){ //if there is a page with free block of needed size;
                page_descriptor* page=(blocktype_iter->second).back();//get it
                block_descriptor* block=page->first_free_block;// Grab a block
                page->first_free_block=block->next_free_block;
                page->num_of_next_pages+=1;
                page->state=1;
                if ((page->num_of_next_pages)>((page_size-page_descriptor_size)/page->fraction_size)){
                    cout<<"Error"<<(size_t)page+'0'<<endl;
                }
                if (page->first_free_block==NULL){
                (blocktype_iter->second).pop_back();
                };//if page is full - kick it out of slab
                return (void*)block;
            }
            else{ //else try to create a new page with needed block size;
                cout<<"Trying to create a new page"<<(blocktype_iter->second).size()<<endl;
                block_descriptor* block;
                if(first_free_page!=NULL){//if there is a free page availible
                    page_descriptor* new_page=first_free_page;//grab first free page
                    if (first_free_page->num_of_next_pages==1){//if it's a single page - push the next free space up
                        first_free_page=first_free_page->next_free_space;
                    }
                    else{ // if no - chip away leftmost page from the space and move the space head to the first block on the right
                        page_descriptor* next_free_page=(page_descriptor*)((char*)first_free_page+page_size);
                        next_free_page->next_free_space=first_free_page->next_free_space;
                        next_free_page->num_of_next_pages=first_free_page->num_of_next_pages-1;
                        first_free_page=next_free_page;
                    }
                    fracture_page(new_page,blck_size); // fracture page into needed pieces
                    blocktype_iter=blocktype_hash.find(blck_size);
                    (blocktype_iter->second).push_back(new_page);
                    block=new_page->first_free_block;
                    new_page->first_free_block=block->next_free_block;
                    new_page->num_of_next_pages+=1;
                }
                else{//if not, try to find larger block recursively
                    
                    block_descriptor* block=NULL;
                    while (blck_size<=((page_size-page_descriptor_size)/2)){
                    blck_size=2*blck_size;
                    blocktype_iter=blocktype_hash.find(blck_size); //Try to find neccessary block size in cache;
                    //if found - try to allocate block;
                    if ((blocktype_iter!=blocktype_hash.end())&&(!(blocktype_iter->second).empty())){
                            page_descriptor* page=(blocktype_iter->second).back();//get it
                            block_descriptor* block=page->first_free_block;// Grab a block
                            page->first_free_block=block->next_free_block;
                            page->num_of_next_pages+=1;
                            if ((page->num_of_next_pages)>((page_size-page_descriptor_size)/page->fraction_size)){
                            cout<<"Error"<< (size_t)page+'0'<<endl;
                            }
                            if (page->first_free_block==NULL){(blocktype_iter->second).pop_back();};//if page is full - kick it out of slab
                    }
                    }
                
                    return (void*)block;
                    
                }
                return (void*) block;
            }               
        }	
        else {  //if not found - try to make one
            block_descriptor* block;
            if(first_free_page!=NULL){//if there is a free page availible
                vector<page_descriptor*> vec;
                pair<size_t,vector<page_descriptor*> > t=make_pair(blck_size,vec); //add block size to hashmap
                blocktype_hash.insert(t);
                page_descriptor* new_page=first_free_page;//grab first free page
                if (first_free_page->num_of_next_pages==1){//if it's a single page - push the next free space up
                    first_free_page=first_free_page->next_free_space;
                }
                else{ // if no - chip away leftmost page from the space and move the space head to the first block on the right
                    page_descriptor* next_free_page=(page_descriptor*)((char*)first_free_page+page_size);
                    next_free_page->next_free_space=first_free_page->next_free_space;
                    next_free_page->num_of_next_pages=first_free_page->num_of_next_pages-1;
                    first_free_page=next_free_page;
                }
                fracture_page(new_page,blck_size); // fracture page into needed pieces
                blocktype_iter=blocktype_hash.find(blck_size);
                (blocktype_iter->second).push_back(new_page);
                block=new_page->first_free_block;
                new_page->first_free_block=block->next_free_block;
                new_page->num_of_next_pages+=1;
                return block;
             }
             else{//if not, try to find larger block recursively
                block_descriptor* block=NULL;
                while (blck_size<=((page_size-page_descriptor_size)/2)){
                    blck_size=2*blck_size;
                    blocktype_iter=blocktype_hash.find(blck_size); //Try to find neccessary block size in cache;
                    //if found - try to allocate block;
                    if ((blocktype_iter!=blocktype_hash.end())&&(!(blocktype_iter->second).empty())){
                            page_descriptor* page=(blocktype_iter->second).back();//get it
                            block_descriptor* block=page->first_free_block;// Grab a block
                            page->num_of_next_pages+=1;
                            page->first_free_block=block->next_free_block;
                            if ((page->num_of_next_pages)>((page_size-page_descriptor_size)/page->fraction_size)){
                            cout<<"Error"<<(size_t)page+'0'<<endl;
                            }   
                            if (page->first_free_block==NULL){(blocktype_iter->second).pop_back();};//if page is full - kick it out of slab
                    }
                }
                return (void*)block;
                
              }
        
             }
             
    }    
    else{//if block is  larger than half page size
       size_t num_pages=(size+page_descriptor_size)/page_size;//determine how many pages are needed;
       if ((size+page_descriptor_size)%page_size!=0){num_pages++;};
       page_descriptor* page=first_free_page;
       page_descriptor* prev_page=NULL;
       while(page){//search for suitable space
          if (page->num_of_next_pages>=num_pages){//if found one - take it
                if (page->num_of_next_pages==num_pages){//if it's exact match
                    if(prev_page){//and there is previous space
                        prev_page->next_free_space=page->next_free_space; //cut it from the list
                            //and switch to multipage mode
                    }
                    else{
                        first_free_page=page->next_free_space;
                    }
                    page->state=2;
                    page_descriptor* temp=page;
                    
                }
                else{//else if it's larger
                    page_descriptor* next_page=(page_descriptor*)((char*)page+num_pages*page_size);//cut it
                    next_page->next_free_space=page->next_free_space;//Connect to the right part part of the list;
                    next_page->num_of_next_pages=page->num_of_next_pages-num_pages;//determine new free space size
                    next_page->state=0;
                    page->num_of_next_pages=num_pages;
                    page->state=2;//designate pages as multipage busy block;
                    //left join to the list
                    if(prev_page){//if there is a previous page
                        prev_page->next_free_space=next_page;
                    }
                    else{
                        first_free_page=next_page;
                    }
                }
               break;
          }
          prev_page=page;//else go to next space
          page=page->next_free_space;  
       }
       return (void*)(page+page_descriptor_size);
    }
    return NULL;
}


void* mem_realloc(void* adress,size_t size){//Memory reallocation routine
    void* new_addr=mem_alloc(size);
    if(new_addr!=NULL){
        mem_free(adress);
    }
    
    return new_addr;
};


void mem_free(void* adress){//Memory discharge routine
    size_t offset=(size_t)adress%page_size;
    //page_descriptor* page_desc=(page_descriptor*)((size_t)adress & ~(page_size - 1));//get page descriptor
    page_descriptor* page_desc=(page_descriptor*)((size_t)adress-offset);
    if ((page_desc->state)==1){// if it's fractured
       block_descriptor* block=(block_descriptor*)adress;//get block descriptor for freed block
       block_descriptor* block2=page_desc->first_free_block;// get first free block of the page
       block->next_free_block=block2;//hook up freed block to the head of the list
       page_desc->first_free_block=block;
       page_desc->num_of_next_pages-=1;//decrease counter
       if (page_desc->num_of_next_pages==0){// if the page is empty
            page_desc->state=0;//mark it as empty
            page_desc->num_of_next_pages=1;
            size_t frac_size=page_desc->fraction_size;//and kick it out of slab
            blocktype_iter=blocktype_hash.find(frac_size);
            vector<page_descriptor*> vec=blocktype_iter->second;
            for(int i=0;i<vec.size();i++){
                if(vec.at(i)==page_desc){
                    cout<<"Slab erased"<<endl;
                    (blocktype_iter->second).erase((blocktype_iter->second).begin()+i);
                    break;
                }
            }
            add_free_pages(page_desc,1);//add the page to free pages list
        }
        else{//check if page is in appropriate slab
            size_t frac_size=page_desc->fraction_size;
            blocktype_iter=blocktype_hash.find(frac_size);
            vector<page_descriptor*> vec=blocktype_iter->second;
            bool isPresent=false;
            for (int i=0;i<vec.size();i++){
                if (vec.at(i)==page_desc){
                    isPresent=true;
                    break;
                }
            }
            if(!isPresent){(blocktype_iter->second).push_back(page_desc); cout<<"Returned to slab"<<vec.size()<<endl;};
        }
    }
    else{//if it's multipage;
       
        page_descriptor* ptr=page_desc;
        int num_pages=ptr->num_of_next_pages;//get length of space
        ptr->state=0;
        for(int i=0;i<num_pages;i++){//mark free all pages
            
            ptr=(page_descriptor*)((char*)ptr+page_size);
        }
        add_free_pages(page_desc,num_pages);//add pages to free list
    }
        


};



void mem_init(){
    char *raw_ptr = (char*)malloc(totalmem_size)+page_size-1;//Bad voodoo magic to set the memory start to physical page start
    intptr_t rounded = ((intptr_t)raw_ptr) & ~(page_size-1);
    memory_start=(char*) rounded;
    
    //divide into pages
    page_descriptor* ptr=(page_descriptor*)memory_start; 
    first_free_page=ptr;//all the memory is one big free space at this point
    while((char*)ptr<(memory_start+totalmem_size-page_size)){//mark all pages as clear
        ptr->state=0;
        ptr=(page_descriptor*)((char*)ptr+page_size);
    }
    first_free_page->num_of_next_pages=(totalmem_size-page_size)/page_size;//make the first page a head of space and calculate number of pages
    first_free_page->next_free_space=NULL;
};


//Fractures page into small blocks
void fracture_page(page_descriptor* page, size_t block_size){
    page->state=1;//Turn on the fractured state;
    page->fraction_size=block_size;//Designate the fraction_size;
    page->num_of_next_pages=0;//counter to determine if page's empty;
    block_descriptor* block=(block_descriptor*)((char*)page+page_descriptor_size);//step over to actuall block
    block_descriptor* prev_block=NULL;
    page->first_free_block=block;//set up first free block
    for (int i=0;i<page_size/block_size;i++){
        if (prev_block){
            prev_block->next_free_block=block;
        }
        prev_block=block;
        block=(block_descriptor*)((char*)block+block_size);
    }
    block->next_free_block=NULL;//trailing element is NULL
}



//Adds N pages after given one to free pages list
void add_free_pages(page_descriptor* page,int N){
    page_descriptor* ptr=first_free_page;//get pointer to free pages list
    page_descriptor* prev_ptr=NULL;//previous element to the list
    while((ptr<page)&&(ptr!=NULL)){//determine position of space in the list
        prev_ptr=ptr;
        ptr=ptr->next_free_space;
    }
    if(ptr==NULL){//if it's further than tail of the list
            if(first_free_page==NULL){
                first_free_page=page;
                page->num_of_next_pages=N;
                page->next_free_space=NULL;    
            }
            else{
                page_descriptor* temp=(page_descriptor*)((char*)prev_ptr+(prev_ptr->num_of_next_pages)*page_size);//get the tail of previous space
                if(temp==page){//if the space is immediately after the tail space - just hook it up
                    prev_ptr->num_of_next_pages+=N;    
                } 
                else{//if it's not immediately after - make a new item on the list
                    prev_ptr->next_free_space=page;
                    page->num_of_next_pages=N;
                    page->next_free_space=NULL;
                }
            }
    }// if it's in the middle of the list
    else{
        page->next_free_space=ptr;
        page->num_of_next_pages=N;
        if (prev_ptr!=NULL){
            page_descriptor* temp=(page_descriptor*)((char*)prev_ptr+(prev_ptr->num_of_next_pages)*page_size);//get the tail of previous space
            prev_ptr->next_free_space=page;
            if(temp==page){//if the space is immediately after the prev space - just hook it up
                prev_ptr->num_of_next_pages+=N; 
                page=prev_ptr;   
            } 
            else{//if it's not immediatly after - make a new item on the list
                prev_ptr->next_free_space=page;
                page->num_of_next_pages=N;
                page->next_free_space=ptr;
            }
            temp=(page_descriptor*)((char*)page+(page->num_of_next_pages)*page_size);
            if (temp==ptr){
                page->num_of_next_pages=prev_ptr->num_of_next_pages+ptr->num_of_next_pages;
                page->next_free_space=ptr->next_free_space;
            }
            
        }
        else{
            first_free_page=page;
            page_descriptor* temp=(page_descriptor*)((char*)page+(page->num_of_next_pages)*page_size);//get the tail
            if (temp==ptr){
                page->num_of_next_pages=page->num_of_next_pages+ptr->num_of_next_pages;
                page->next_free_space=ptr->next_free_space;
            }
            else{
                page->next_free_space=ptr;
                page->num_of_next_pages=N;
            }
            
        }
    }         
}


