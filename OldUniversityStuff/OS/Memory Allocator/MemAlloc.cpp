#include "MemAlloc.h"

//Variables

char freeMem[mem_size]; //Array to reserve total memory
char* sectorStart=&freeMem[0]; //Sector beginning adress


//Memory allocation routine
void* mem_alloc(size_t size){
	header* ptr=(header*)sectorStart;
	header* ptr1;
	header* ptr2;
	// Trimming
	size_t needed_size=size+header_size;
	needed_size=needed_size%trimming_size;
	if (needed_size!=0){
		needed_size=size+4-needed_size;
	}
	else{
		needed_size=size;
	}

	while ((char*) ptr<((char*)sectorStart+mem_size)){ //While not out of memory sector
		//if found suitable block
		if (((*ptr).size>=(needed_size))&&((*ptr).isFree==true)){
			if((*ptr).size>(needed_size+header_size)){
				ptr1=(header*)((char*)ptr+needed_size+header_size);
				ptr2=(header*)((char*)ptr+ptr->size+header_size);			
				size_t old_size=(*ptr).size;
				(*ptr).size=needed_size;
				(*ptr1).size=old_size-needed_size-header_size;
				(*ptr1).prevSize=(*ptr).size;
				if ((char*)ptr2<((char*)sectorStart+mem_size)){
					(*ptr2).prevSize=(*ptr1).size;
				}
				(*ptr1).isFree=true;
				(*ptr).isFree=false;
			}
			else{
				(*ptr).isFree=false;			
			}			
			break;				
		}
		//else - go to next free block
		else{
			ptr=(header*)((char*)ptr+(*ptr).size+header_size);		
		}	
	};
	
	if((char*)ptr>=(char*)sectorStart+mem_size){
		return 0;
	}
	else{
		return (void*)((char*)ptr+header_size);	
	}
};

//Memory reallocation routine
void* mem_realloc(void* address,size_t size){
	bool isNew_=false;	
	char* old_address=(char*)address-header_size;
	void* new_address=mem_alloc(size);
	if(	new_address!=0){
		old_address=(char*)old_address+header_size;
		mem_free(old_address);
		isNew_=true;
	}
	if(isNew_){
		return new_address;
	}
	else{
		return 0;	
	}
};


//Memory free routine
void mem_free(void* address){
	header* ptr=(header*)((char*)address-header_size);
	header* ptr1=(header*)((char*)ptr+(*ptr).size+header_size);
	header* ptr2=(header*)((char*)ptr-(*ptr).prevSize-header_size);
	(*ptr).isFree=true;
	if ((*ptr1).isFree==true){
		header* ptr3=ptr1+(*ptr1).size+header_size;		
		size_t upd_size=(*ptr).size+(*ptr1).size+header_size;
		if ((char*)ptr3<(sectorStart+mem_size)){							
			(*ptr3).prevSize=upd_size;
		}
		(*ptr).size=upd_size;
	}

	if(((*ptr2).isFree==true)&&((*ptr).prevSize!=0)){
		size_t upd_size=(*ptr2).size+header_size+(*ptr).size;		
		header* ptr3=(header*)((char*)ptr+(*ptr).size+header_size);
		if ((char*)(ptr3)<(char*)(sectorStart+mem_size)){							
			(*ptr3).prevSize=upd_size;
		}
		(*ptr2).size=upd_size;	
	}

};

//Allocator initialization
void mem_init(){
	header* start=(header*) sectorStart;
	(*start).size=mem_size-header_size;
	(*start).prevSize=0;
	(*start).isFree=true;
};
