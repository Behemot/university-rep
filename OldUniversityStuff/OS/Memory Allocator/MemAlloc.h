#include <stdlib.h>
//Block header structure
struct header{
	bool isFree; // Is block free
	size_t size; // Block size
	size_t prevSize ; // Previous block size
};

//Function headers

void* mem_alloc(size_t size);//Memory allocation routine
void* mem_realloc(void* adress,size_t size);//Memory reallocation routine
void mem_free(void* adress);//Memory discharge routine
void mem_init();

//Variables
const size_t mem_size=1024; //Total memory size
const size_t header_size=sizeof(struct header); //Block header size
const size_t trimming_size=4; //Block size to trim to
extern char freeMem[mem_size]; //Array to reserve total memory
extern char* sectorStart; //Sector beginning adress

