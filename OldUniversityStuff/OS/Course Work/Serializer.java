package data;

import gui.GraphEd;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.jgraph.JGraph;

public class Serializer {
	/**
	 * Method that serializes chart to file.
	 * 
	 * @param outFile
	 *            target file.
	 * @param c
	 *            chart to be serialized.
	 * @return true if serialization was successful, false otherwise.
	 */
	public boolean serialize(File outFile, ArrayList<Node> c) {

		try {
			if (outFile.createNewFile() == false) {
				outFile.delete();
				outFile.createNewFile();
			}
		} catch (SecurityException SE) {
			System.out.println("Unable to access disk");
			return false;
		} catch (IOException IOe) {
			System.out
					.println("IO Exception while creating serialization file");
			return false;
		}

		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(outFile));
			out.writeObject(c);
			out.close();
		} catch (IOException IOe) {
			System.out.println("IO Exception while serializing " );
			IOe.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Method that deserializes NetManager object from file.
	 * 
	 * @param outFile
	 *            deserialization file;
	 * @return Chart instance or null if deserialization failed.
	 */
	public ArrayList<Node> deserialize(File outFile) {
		ArrayList<Node> net_;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					outFile));
			net_ = (ArrayList<Node>) in.readObject();
			System.out.println("Loaded "+net_.size());
			in.close();
		} catch (FileNotFoundException FileNotFound) {
			System.out.println("Temporary file not found");
			return null;
		} catch (IOException IOe) {
			System.out.println("Unable to restore data");
			return null;
		} catch (ClassNotFoundException ClassNotFound) {
			System.out.println("Wrong file");
			return null;
		}

		return net_;

	}
	
}
