package data;
import java.io.Serializable;
public class Connection implements Serializable{
	private Node node_;
	private int weight_;
	public Connection(Node node,int weight){
		node_=node;
		weight_=weight;
	}
	public int getWeight(){
		return weight_;
	}
	public Node getNode(){
		return node_;
	}
}
