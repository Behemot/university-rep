package data;
import java.util.ArrayList;

public class MHscheduler {
	private ArrayList<Node> nodes_;
	private ArrayList<Node> entryNodes_=new ArrayList<Node>();
	private ArrayList<Node> exitNodes_=new ArrayList<Node>();
	private ArrayList<Node> readyNodes_=new ArrayList<Node>();
	private boolean[] finished_;
	private int[] procNextAvailible_;
	private int[] num_of_proc_;
	private int[] start_time_;
	private int[] end_time_;
	public MHscheduler(ArrayList<Node> nodes,int numProc){
		nodes_=nodes;
		for(int i=0;i<nodes_.size();i++){
			if(nodes_.get(i).getParents().size()==0){
				entryNodes_.add(nodes_.get(i));
				readyNodes_.add(nodes_.get(i));
			}else if(nodes_.get(i).getChildren().size()==0){
				exitNodes_.add(nodes_.get(i));
			}
		}
		procNextAvailible_=new int[numProc];
		for(int i=0;i<numProc;i++){
			procNextAvailible_[i]=0;
		}
	}
	public void createSchedule(){
		start_time_=new int[nodes_.size()];
		end_time_=new int[nodes_.size()];
		num_of_proc_=new int[nodes_.size()];
		finished_=new boolean[nodes_.size()];
		
		for(int i=0;i<procNextAvailible_.length;i++){
			procNextAvailible_[i]=0;
		}
		for(int i=0;i<nodes_.size();i++){
			start_time_[i]=0;
			end_time_[i]=0;
			finished_[i]=false;
			nodes_.get(i).setSL(getSL(nodes_.get(i)));
		}
		while(readyNodes_.size()!=0){
			sortReadyNodes();
			Node cur=readyNodes_.get(0);
			System.out.println("Node: "+cur.getID());
			readyNodes_.remove(0);
			ArrayList<Connection> parents=cur.getParents();
			int edgeProc=procNextAvailible_.length-1;
			for(int i=0;i<parents.size();i++){
				if(num_of_proc_[nodes_.indexOf(parents.get(i).getNode())]<=edgeProc){
					edgeProc=num_of_proc_[nodes_.indexOf(parents.get(i).getNode())];
				}
			}
			int proc=0;
			int time=Integer.MAX_VALUE;
			for(int i=0;i<=edgeProc;i++){
				int est_time=procNextAvailible_[i];
				for(int j=0;j<parents.size();j++){
					int est=end_time_[nodes_.indexOf(parents.get(j).getNode())]+getRouteEstimate(i,num_of_proc_[nodes_.indexOf(parents.get(j).getNode())],parents.get(j).getWeight());
					if(est>=est_time){
						est_time=est;
						
					}
				}
				if(est_time<=time){
					time=est_time;
					proc=i;
				}
			}
			finished_[nodes_.indexOf(cur)]=true;
			start_time_[nodes_.indexOf(cur)]=time;
			end_time_[nodes_.indexOf(cur)]=time+cur.getWeight();
			procNextAvailible_[proc]=time+cur.getWeight();
			num_of_proc_[nodes_.indexOf(cur)]=proc;
			ArrayList<Connection> children=cur.getChildren();
			
			for(int i=0;i<children.size();i++){
				boolean ready=true;
				ArrayList<Connection> p2=children.get(i).getNode().getParents();
				for(int j=0;j<p2.size();j++){
					if(!finished_[nodes_.indexOf(p2.get(j).getNode())]){
						ready=false;
					}
				}
				if(ready==true){
					readyNodes_.add(children.get(i).getNode());
				}
				
			}
		}
		
		for(int i=0;i<nodes_.size();i++){
			System.out.println(nodes_.get(i).getID()+" & "+num_of_proc_[i]+" & "+start_time_[i]+" & "+end_time_[i]+" & "+nodes_.get(i).getSL()+" \\\\ \\hline");
		}
		
	}
	
	private void sortReadyNodes(){
		ArrayList<Node> temp=new ArrayList<Node>();
		while(readyNodes_.size()!=0){
			int max_priority=0;
			int pos=0;
			for(int i=0;i<readyNodes_.size();i++){
				if(readyNodes_.get(i).getSL()>=max_priority){
					max_priority=readyNodes_.get(i).getSL();
					pos=i;
				}
			}
			temp.add(readyNodes_.get(pos));
			readyNodes_.remove(pos);
		}
		readyNodes_=temp;
	}
	
	private int getSL(Node N){
		if(exitNodes_.contains(N)==true){
			return N.getWeight();
		}else{
			ArrayList<Connection> children_=N.getChildren();
			int res=0;
			for(int i=0;i<children_.size();i++){
				int temp=getSL(children_.get(i).getNode()); 
				if(temp>res)res=temp;
			}
			return res+N.getWeight();
		}
	}
	public int getRouteEstimate(int start, int end,int size){
		System.out.println("Start: "+start+"; End: "+end+"; Size: "+size+"; Length: "+Math.abs(start-end)*size);
			return Math.abs(start-end)*size;//TODO fix
	}
	
	public void generateReport(){
		ArrayList<ArrayList<Integer>> timelines_=new ArrayList<ArrayList<Integer>>();
		int timeLength=0;
		for(int i=0;i<end_time_.length;i++){
			if(end_time_[i]>timeLength)timeLength=end_time_[i];
		}
		for(int i=0;i<procNextAvailible_.length;i++){
			ArrayList<Integer> temp=new ArrayList<Integer>();
			timelines_.add(temp);
			for(int j=0;j<timeLength;j++){
				temp.add(-1);
			}
		}
		for(int i=0;i<end_time_.length;i++){
			for(int j=start_time_[i];j<end_time_[i];j++){
				timelines_.get(num_of_proc_[i]).set(j,nodes_.get(i).getID());
			}
		}
		System.out.println("& ");
		for(int i=0;i<procNextAvailible_.length;i++){
			System.out.print("P"+i+"&");
		}
		System.out.println();
		for(int i=0;i<timeLength;i++){
			System.out.print(i);
			for(int j=0;j<procNextAvailible_.length;j++){
				System.out.print("&"+timelines_.get(j).get(i));
			}
			System.out.println("\\\\ \\hline");
		}
	}
	
}
