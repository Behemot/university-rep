#include "core.h"
#include <iostream>

namespace email{

const int char_classes_size=7;
const int email_state_num=6;
extern string char_classes [char_classes_size-1];
extern trans_table_cell email_trans_table[email_state_num][char_classes_size];
extern vector<vector<trans_table_cell> > email_table;
extern vector<string> char_classes_vec;

/*Some initializations needed for proper transition tables and characher types */
void init_environment();


/*Error action*/
class error_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Whitespace and newlines related automat actions*/
/* If whitespaces or newlines continue*/
class space_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* If there's a letter or a number*/
class endspace_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Action that adds char to the context*/
class accumulate_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Action that start a new part of the context */
class newtoken_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Action that prints and dumps the context*/
class dump_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};





}
