#include "email.h"
using std::cout;
using std::endl;

#define ERROR(state) {state,new error_action()}
#define SPACE(state) {state,new space_action()}
#define END_SPACE(state) {state,new endspace_action()}
#define ACCUMULATE(state) {state, new accumulate_action()}
#define NEW_TOKEN(state) {state, new newtoken_action()}
#define DUMP(state) {state, new dump_action()}



namespace email
{



string char_classes [char_classes_size-1]={"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBM1234567890","_","@","-",".","\n "};
vector<string> char_classes_vec;

trans_table_cell email_trans_table[email_state_num][char_classes_size]=
{
  //State  [a-zA-Z0-9] [_] [@] [-] [.] [\n" "] [Banned symbols]  
  /* 0 */{ END_SPACE(1),ERROR(0),ERROR(0),ERROR(0),ERROR(0),SPACE(0),ERROR(0) },
  /* 1 */{ ACCUMULATE(1),ACCUMULATE(2),NEW_TOKEN(3),ERROR(1),ERROR(1),ERROR(1),ERROR(1) },
  /* 2 */{ ACCUMULATE(1),ACCUMULATE(2),ERROR(2),ERROR(2), ERROR(2),ERROR(2),ERROR(2) },
  /* 3 */{ ACCUMULATE(4),ERROR(3),ERROR(3),ERROR(3), ERROR(3),ERROR(3),ERROR(3) },
  /* 4 */{ ACCUMULATE(4),ERROR(4),ERROR(4),ACCUMULATE(5), NEW_TOKEN(4),DUMP(0),ERROR(4) },
  /* 5 */{ ACCUMULATE(4),ERROR(5),ERROR(5),ACCUMULATE(5), ERROR(5),ERROR(5),ERROR(5) },

};
vector<vector<trans_table_cell> > email_table;

void init_environment()
{
    for(int i=0;i<char_classes_size-1;i++)
    {
        char_classes_vec.push_back(char_classes[i]);
    }
    for(int i=0;i<email_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<char_classes_size;j++)
        {
            temp.push_back(email_trans_table[i][j]);
        }
        email_table.push_back(temp);
    }
    
} 



task_context* error_action::doWork(task_context* context,char c)
{
    /*if it's EOF then dump the context and get out*/
    if ((int)c==4){
        vector<string> outp=context->getAllContext();
        
        if (outp.size()>=2){
            cout<<"Name: ";
            cout<<outp.at(0)<<" ; ";
            cout<<"Domain:";
            for(int i=1;i<outp.size()-1;i++){
                cout<< outp.at(i)<<".";
            }
            cout<< outp.at(outp.size()-1)<<endl;
        }
        
    }
    else{
            cout<<"Error: "<<context->getLastContext()<<c<<endl;
    }

    return context;    
};
task_context* space_action::doWork(task_context* context,char c)
{
    return context;
};
task_context* endspace_action::doWork(task_context* context,char c)
{
    string s;
    s.append(1,c);
    context->addContext(s);
    return context;
};

task_context* accumulate_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext().append(1,c);
    context->setLastContext(s);
    return context;
}
task_context* newtoken_action::doWork(task_context* context,char c)
{
    string s="";
    context->addContext(s);
    return context;
}
task_context* dump_action::doWork(task_context* context,char c)
{
    vector<string> outp=context->getAllContext();
    if (outp.size()>=2){
        cout<<"Name: ";
        cout<<outp.at(0)<<" ; ";
        cout<<"Domain:";
        for(int i=1;i<outp.size()-1;i++){
            cout<< outp.at(i)<<".";
        }
        cout<< outp.at(outp.size()-1)<<endl;
    }
    context->clear();
    
    return context;   
}


}
