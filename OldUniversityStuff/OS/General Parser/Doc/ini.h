#include "core.h"
#include <iostream>

namespace ini{

const int parameter_chartypes_num=11;
const int whitespace_chartypes_num=2;
const int delimitedcomment1_chartypes_num=3;
const int delimitedcomment2_chartypes_num=3;
const int linecomment_chartypes_num=2;
const int section_chartypes_num=2;
const int string_chartypes_num=6;

const int parameter_state_num=4;
const int whitespace_state_num=1;
const int delimitedcomment1_state_num=3;
const int delimitedcomment2_state_num=3;
const int linecomment_state_num=1;
const int section_state_num=1;
const int string_state_num=3;

extern string parameter_chartypes[parameter_chartypes_num-1];
extern string whitespace_chartypes[whitespace_chartypes_num-1];
extern string delimitedcomment1_chartypes[delimitedcomment1_chartypes_num-1];
extern string delimitedcomment2_chartypes[delimitedcomment2_chartypes_num-1];
extern string linecomment_chartypes[linecomment_chartypes_num-1];
extern string section_chartypes[section_chartypes_num-1];
extern string string_chartypes[string_chartypes_num-1];


extern vector<string> parameter_chartypes_vec;
extern vector<string> whitespace_chartypes_vec;
extern vector<string> delimitedcomment1_chartypes_vec;
extern vector<string> delimitedcomment2_chartypes_vec;
extern vector<string> linecomment_chartypes_vec;
extern vector<string> section_chartypes_vec;
extern vector<string> string_chartypes_vec;
 
extern trans_table_cell parameter_trans_table[parameter_state_num][parameter_chartypes_num];
extern trans_table_cell whitespace_trans_table[whitespace_state_num][whitespace_chartypes_num];
extern trans_table_cell delimitedcomment1_trans_table[delimitedcomment1_state_num][delimitedcomment1_chartypes_num];
extern trans_table_cell delimitedcomment2_trans_table[delimitedcomment2_state_num][delimitedcomment2_chartypes_num];
extern trans_table_cell linecomment_trans_table[linecomment_state_num][linecomment_chartypes_num];
extern trans_table_cell section_trans_table[section_state_num][section_chartypes_num];
extern trans_table_cell string_trans_table[string_state_num][string_chartypes_num];

extern vector<vector<trans_table_cell> > parameter_table;
extern vector<vector<trans_table_cell> > whitespace_table;
extern vector<vector<trans_table_cell> > delimitedcomment1_table;
extern vector<vector<trans_table_cell> > delimitedcomment2_table;
extern vector<vector<trans_table_cell> > linecomment_table;
extern vector<vector<trans_table_cell> > section_table;
extern vector<vector<trans_table_cell> > string_table;




/*Some initializations needed for proper transition tables and characher types */

void init_environment();

/* Actions */
/* Parameter machine */
/* Whitespaces start */
class ToWhitespace_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Line Comment starts */
class LineComment_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Delimited comment near parameter name starts*/
class DelimitedComment1_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Delimited comment near value starts */
class DelimitedComment2_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Section start*/
class SectionStart_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Section end*/
class EndSection_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Formated string starts*/
class StringStart_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Accumulate token*/
class Accumulate_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* New token*/
class NewToken_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Parameter End*/
class EndParam_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};


/* Whitespace machine*/
/* If spaces or \n continue*/
class Space_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* If spaces end*/
class EndSpace_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Section machine*/
/* Starting to parse tokens inside section*/
class InSection_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Finializes section */ 
class FinalizeSection_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Line comments machine*/
/* Line comment ends */
class EndLineComment_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Delimited comments machines*/
/* Cant find * after / - error version */
class NonComment_error_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Cant find * after / - return version */
class NonComment_return_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Delimited comment ends*/
class EndDelimitedComment_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};


/* String machine */
/* Whitespace or newline in sting */
class StringSpace_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Continual spaces in string*/
class StringCSpace_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* Escape character*/
class EscapeCharacter_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Escape backslash*/
class EscapeBackslash_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Escape quotes*/
class EscapeQuotes_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Escape Tab*/
class EscapeTab_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};
/* Escape newline */
class EscapeNewline_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};

/* End string */
class EndString_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};





/* General actions */
class Error_action:public basic_action
{
    public:
    task_context* doWork(task_context* context,char c);
};


}
