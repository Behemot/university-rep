#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
using std::string;
using std::vector;



/*That's a forward declaration stub for some compatibility*/
class finite_state_machine;
class task_context;
extern vector<task_context*> stack;


/* Context to save nessesary string for further operations */
class task_context{
    
    vector<string> cont_strings;
    finite_state_machine* current_machine;
    public:
    
    task_context(){
        cont_strings=*(new vector<string>());
    }
    
    string getLastContext()
    {
        if (cont_strings.size()>0)
        {
            return cont_strings.back();
        }
        else
        {
            string s("");
            cont_strings.push_back(s);
            return s;
        }
    }
    
    vector<string> getAllContext()
    {
        return cont_strings;
    }
    
    void setLastContext(string S)
    {
        if(cont_strings.size()>0){
            cont_strings.pop_back();
        }
        cont_strings.push_back(S);
    }
    
    void addContext(string S)
    {
        cont_strings.push_back(S);
    }
    
    finite_state_machine* getCurrentMachine()
    {
        return current_machine;
    }
    
    void setCurrentMachine(finite_state_machine* fsm)
    {
        current_machine=fsm;
    }
    void clear()
    {
        cont_strings.clear();
    }
   
};
/* Parent class serving as an interface for actions */ 
class basic_action
{
    public:
    virtual task_context* doWork(task_context* context,char c)=0;
};
/*Transition table cell unit */
struct trans_table_cell 
{
    int next_state;
    basic_action* action;
     
};

/* Finite state machine class*/
class finite_state_machine
{

    int state;
    int charmap[256];
    task_context* context;
    vector<vector<trans_table_cell> >* transition_table;
    public:
    finite_state_machine(vector<vector<trans_table_cell> >* t,vector<string>* char_classes)
    {
       state=0;
       transition_table=t;
       for(int i=0;i<256;i++)
        {
            charmap[i]=char_classes->size();
        }
        for(int i=0;i<char_classes->size();i++)
        {
            for(int j=0;j<char_classes->at(i).length();j++)
            {
                charmap[(int)char_classes->at(i).at(j)]=i;
            }
        }
       
    };
    
    task_context* step(char c){
        context=transition_table->at(state).at(charmap[(int)c]).action->doWork(context,c);
        state=transition_table->at(state).at(charmap[(int)c]).next_state;
        return context;
    };
    
    void set_context(task_context* t)
    {
        context=t;
    };
    
    task_context* get_context(task_context* t)
    {
       return context;
    };   

};





