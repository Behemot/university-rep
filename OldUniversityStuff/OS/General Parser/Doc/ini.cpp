#include "ini.h"
using std::cout;
using std::endl;

#define TO_WHITESPACE(state) {state, new ToWhitespace_action()}
#define LINE_COMMENT(state) {state, new LineComment_action()}
#define DELIMITED_COMMENT1(state) {state, new DelimitedComment1_action()}
#define DELIMITED_COMMENT2(state) {state, new DelimitedComment2_action()}
#define SECTION_START(state) {state, new SectionStart_action()}
#define END_SECTION(state) {state, new EndSection_action()}
#define START_STRING(state) {state, new StringStart_action()}
#define ACCUMULATE(state) {state, new Accumulate_action()}
#define NEW_TOKEN(state) {state, new NewToken_action()}
#define DUMP(state) {state, new EndParam_action()}

#define SPACE(state) {state, new Space_action()}
#define END_SPACE(state) {state, new EndSpace_action()}

#define IN_SECTION(state) {state, new InSection_action()}
#define FINALIZE_SECTION(state) {state, new FinalizeSection_action()}

#define END_LINECOMMENT(state) {state, new EndLineComment_action()}

#define NONCOMMENT_ERROR(state) {state, new NonComment_error_action()}
#define NONCOMMENT_RETURN(state) {state, new NonComment_return_action()}
#define END_DELIMITEDCOMMENT(state) {state, new EndDelimitedComment_action()}

#define STRING_SPACE(state) {state, new StringSpace_action()}
#define STRING_CSPACE(state) {state, new StringCSpace_action()}
#define ESCAPE_CHARACTER(state) {state, new EscapeCharacter_action()}
#define ESCAPE_BACKSLASH(state) {state, new EscapeBackslash_action()}
#define ESCAPE_QUOTES(state) {state, new EscapeQuotes_action()}
#define ESCAPE_TAB(state) {state, new EscapeTab_action()}
#define ESCAPE_NEWLINE(state) {state, new EscapeNewline_action()}
#define END_STRING(state) {state, new EndString_action()}

#define ERROR(state) {state, new Error_action()}





namespace ini
{




string parameter_chartypes[parameter_chartypes_num-1]={"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBM1234567890","_","\n ","#","/","{","}","=","\"",";"};
string whitespace_chartypes[whitespace_chartypes_num-1]={"\n "};
string delimitedcomment1_chartypes[delimitedcomment1_chartypes_num-1]={"/","*"};
string delimitedcomment2_chartypes[delimitedcomment2_chartypes_num-1]={"/","*"};
string linecomment_chartypes[linecomment_chartypes_num-1]={"\n"};
string section_chartypes[section_chartypes_num-1]={"}"};
string string_chartypes[string_chartypes_num-1]={"\n ","\\","\"","n","t"};


vector<string> parameter_chartypes_vec;
vector<string> whitespace_chartypes_vec;
vector<string> delimitedcomment1_chartypes_vec;
vector<string> delimitedcomment2_chartypes_vec;
vector<string> linecomment_chartypes_vec;
vector<string> section_chartypes_vec;
vector<string> string_chartypes_vec;

trans_table_cell parameter_trans_table[parameter_state_num][parameter_chartypes_num]=
{
/* # */ /* a-zA-Z0-9 *//* _ */       /* \n  */        /* # */         /* / */               /* { */          /* } */        /* = */      /* " */         /* ; */  /* Other*/
/* 0 */{ACCUMULATE(1), ERROR(0),     TO_WHITESPACE(0),LINE_COMMENT(0),DELIMITED_COMMENT1(0),ERROR(0),        END_SECTION(0),ERROR(0),    ERROR(0),       ERROR(0),ERROR(0)},
/* 1 */{ACCUMULATE(1), ACCUMULATE(1),TO_WHITESPACE(2),LINE_COMMENT(1),DELIMITED_COMMENT1(1),SECTION_START(1),ERROR(1),      NEW_TOKEN(3),ERROR(1),       ERROR(1),ERROR(1)},
/* 2 */{ERROR(2),      ERROR(2),     TO_WHITESPACE(2),LINE_COMMENT(2),DELIMITED_COMMENT1(2),SECTION_START(1),ERROR(2),      NEW_TOKEN(3),ERROR(2),       ERROR(2),ERROR(2)},
/* 3 */{ACCUMULATE(3), ACCUMULATE(3),ACCUMULATE(3),   LINE_COMMENT(3),DELIMITED_COMMENT2(3),ERROR(3),        ERROR(3),      ERROR(3), START_STRING(3),DUMP(0),ACCUMULATE(3)}

};
trans_table_cell whitespace_trans_table[whitespace_state_num][whitespace_chartypes_num]=
{
/* # */ /* \n */ /* Other */
/* 0 */{SPACE(0),END_SPACE(0)}
};
trans_table_cell delimitedcomment1_trans_table[delimitedcomment1_state_num][delimitedcomment1_chartypes_num]=
{
/* # */ /* / */                    /* * */       /* Other */
/* 0 */ {NONCOMMENT_ERROR(0),    ACCUMULATE(1),NONCOMMENT_ERROR(0)},
/* 1 */ {ACCUMULATE(1),          ACCUMULATE(2),ACCUMULATE(1) },
/* 2 */ {END_DELIMITEDCOMMENT(2),ACCUMULATE(2),ACCUMULATE(1) }

};
trans_table_cell delimitedcomment2_trans_table[delimitedcomment2_state_num][delimitedcomment2_chartypes_num]=
{
/* # */ /* / */                  /* * */       /* Other */
/* 0 */ {NONCOMMENT_RETURN(0),   ACCUMULATE(1),NONCOMMENT_RETURN(0)},
/* 1 */ {ACCUMULATE(1),          ACCUMULATE(2),ACCUMULATE(1) },
/* 2 */ {END_DELIMITEDCOMMENT(2),ACCUMULATE(2),ACCUMULATE(1) }

};
trans_table_cell linecomment_trans_table[linecomment_state_num][linecomment_chartypes_num]=
{
/* # */    /* \n */          /* Other */
/* 0 */{  END_LINECOMMENT(0),ACCUMULATE(0)}
};
trans_table_cell section_trans_table[section_state_num][section_chartypes_num]=
{
/* # */ /* / */             /* Other */
/* 0 */{FINALIZE_SECTION(0),IN_SECTION(0)}
};
trans_table_cell string_trans_table[string_state_num][string_chartypes_num]=
{
/* # */ /* \n  */        /* \ */             /* " */          /* n */           /* t */       /* Other */
/* 0 */{STRING_SPACE(2), ESCAPE_CHARACTER(1),END_STRING(0),   ACCUMULATE(0),    ACCUMULATE(0),ACCUMULATE(0)},
/* 1 */{ERROR(1),        ESCAPE_BACKSLASH(0),ESCAPE_QUOTES(0),ESCAPE_NEWLINE(0),ESCAPE_TAB(0),ERROR(1)},      
/* 2 */{STRING_CSPACE(2),ESCAPE_CHARACTER(1),END_STRING(2),   ACCUMULATE(0),    ACCUMULATE(0),ACCUMULATE(0)}
};


vector<vector<trans_table_cell> > parameter_table;
vector<vector<trans_table_cell> > whitespace_table;
vector<vector<trans_table_cell> > delimitedcomment1_table;
vector<vector<trans_table_cell> > delimitedcomment2_table;
vector<vector<trans_table_cell> > linecomment_table;
vector<vector<trans_table_cell> > section_table;
vector<vector<trans_table_cell> > string_table;




/*Some initializations needed for proper transition tables and characher types */

void init_environment()
{
    for(int i=0;i<parameter_chartypes_num-1;i++)
    {
        parameter_chartypes_vec.push_back(parameter_chartypes[i]);
    }
    for(int i=0;i<parameter_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<parameter_chartypes_num;j++)
        {
            temp.push_back(parameter_trans_table[i][j]);
        }
        parameter_table.push_back(temp);
    }
    
    //-----------------------------------------------------------------
    
    for(int i=0;i<whitespace_chartypes_num-1;i++)
    {
        whitespace_chartypes_vec.push_back(whitespace_chartypes[i]);
    }
    for(int i=0;i<whitespace_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<whitespace_chartypes_num;j++)
        {
            temp.push_back(whitespace_trans_table[i][j]);
        }
        whitespace_table.push_back(temp);
    
    }
    
    
    
    //-----------------------------------------------------------------
    
    for(int i=0;i<delimitedcomment1_chartypes_num-1;i++)
    {
        delimitedcomment1_chartypes_vec.push_back(delimitedcomment1_chartypes[i]);
    }
    for(int i=0;i<delimitedcomment1_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<delimitedcomment1_chartypes_num;j++)
        {
            temp.push_back(delimitedcomment1_trans_table[i][j]);
        }
        delimitedcomment1_table.push_back(temp);
    }
    
    //-----------------------------------------------------------------
    
    for(int i=0;i<delimitedcomment2_chartypes_num-1;i++)
    {
        delimitedcomment2_chartypes_vec.push_back(delimitedcomment2_chartypes[i]);
    }
    for(int i=0;i<delimitedcomment2_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<delimitedcomment2_chartypes_num;j++)
        {
            temp.push_back(delimitedcomment2_trans_table[i][j]);
        }
        delimitedcomment2_table.push_back(temp);
    }
    
    //-----------------------------------------------------------------
    
    for(int i=0;i<linecomment_chartypes_num-1;i++)
    {
        linecomment_chartypes_vec.push_back(linecomment_chartypes[i]);
    }
    for(int i=0;i<linecomment_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<linecomment_chartypes_num;j++)
        {
            temp.push_back(linecomment_trans_table[i][j]);
        }
        linecomment_table.push_back(temp);
    }
    
    //-----------------------------------------------------------------
    
    for(int i=0;i<section_chartypes_num-1;i++)
    {
        section_chartypes_vec.push_back(section_chartypes[i]);
    }
    for(int i=0;i<section_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<section_chartypes_num;j++)

        {
            temp.push_back(section_trans_table[i][j]);
        }
        section_table.push_back(temp);
    }
    
    //-----------------------------------------------------------------
    
    
    for(int i=0;i<string_chartypes_num-1;i++)
    {
        string_chartypes_vec.push_back(string_chartypes[i]);
    }
    for(int i=0;i<string_state_num;i++)
    {
        vector<trans_table_cell> temp;
        for(int j=0;j<string_chartypes_num;j++)
        {
            temp.push_back(string_trans_table[i][j]);
        }
        string_table.push_back(temp);
    }
};



/* Actions */



/* Parameter machine */


/* Whitespaces start */

task_context* ToWhitespace_action::doWork(task_context* context,char c)
{
    task_context* new_context= new task_context();
    finite_state_machine* new_machine=new finite_state_machine(&whitespace_table,&whitespace_chartypes_vec);
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    return new_context;
};

/* Line Comment starts */
task_context* LineComment_action::doWork(task_context* context,char c)
{
    task_context* new_context= new task_context();
    finite_state_machine* new_machine=new finite_state_machine(&linecomment_table,&linecomment_chartypes_vec);
    string s;
    s.append(1,c);
    new_context->addContext(s);
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    return new_context;

};

/* Delimited comment near parameter name starts*/
task_context* DelimitedComment1_action::doWork(task_context* context,char c)
{
    task_context* new_context= new task_context();
    finite_state_machine* new_machine=new finite_state_machine(&delimitedcomment1_table,&delimitedcomment1_chartypes_vec);
    string s;
    s.append(1,c);
    new_context->addContext(s);
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    return new_context;
    
};

/* Delimited comment near value starts */
task_context* DelimitedComment2_action::doWork(task_context* context,char c)
{
    task_context* new_context= new task_context();
    finite_state_machine* new_machine=new finite_state_machine(&delimitedcomment2_table,&delimitedcomment2_chartypes_vec);
    string s;
    s.append(1,c);
    new_context->addContext(s);
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    return new_context;

};

/* Section start*/
task_context* SectionStart_action::doWork(task_context* context,char c)
{
    finite_state_machine* new_machine=new finite_state_machine(&section_table,&section_chartypes_vec);
    cout<<"section: "<<context->getLastContext()<<endl;
    context->setCurrentMachine(new_machine);
    new_machine->set_context(context);
    return context;
};

/* Section end*/
task_context* EndSection_action::doWork(task_context* context,char c)
{
    task_context* prev_context =stack.back();
    stack.pop_back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    task_context* new_context=prev_context->getCurrentMachine()->step(c);
    
    return new_context;
};

/* Formated string starts*/
task_context* StringStart_action::doWork(task_context* context,char c)
{
    finite_state_machine* new_machine=new finite_state_machine(&string_table,&string_chartypes_vec);
    task_context* new_context=new task_context();
    string s;
    s.append(1,c);
    new_context->addContext(s);
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    return new_context;
};

/* Accumulate token*/
task_context* Accumulate_action::doWork(task_context* context,char c)
{
    string s(context->getLastContext());
    s.append(1,c);
    context->setLastContext(s);

    return context;
};
/* New token*/
task_context* NewToken_action::doWork(task_context* pcontext,char c)
{
    string s;
    pcontext->addContext(s);
    return pcontext;
};
/* Parameter End*/
task_context* EndParam_action::doWork(task_context* context,char c)
{
    vector<string> s=context->getAllContext();
    cout<<s.at(0)<<" = "<<s.at(1)<<endl; 
    context->clear();
    return context;
};




/* Whitespace machine*/


/* If spaces or \n continue*/
task_context* Space_action::doWork(task_context* context,char c)
{
    return context;
    
};
/* If spaces end*/
task_context* EndSpace_action::doWork(task_context* context,char c)
{
    task_context* prev_context=stack.back();
    stack.pop_back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    task_context* new_context=prev_context->getCurrentMachine()->step(c);
    return new_context;
};


/* Section machine*/


/* Starting to parse tokens inside section*/
task_context* InSection_action::doWork(task_context* context,char c)
{
    finite_state_machine* new_machine=new finite_state_machine(&parameter_table,&parameter_chartypes_vec);
    task_context* new_context=new task_context();
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    stack.push_back(context);
    new_context=new_machine->step(c);
    return new_context;    
};
/* Finializes section */ 
task_context* FinalizeSection_action::doWork(task_context* context,char c)
{
    finite_state_machine* new_machine=new finite_state_machine(&parameter_table,&parameter_chartypes_vec);
    task_context* new_context=new task_context();
    new_context->setCurrentMachine(new_machine);
    new_machine->set_context(new_context);
    vector<string> allcontext=context->getAllContext();
    cout<<"end section: "<<context->getLastContext()<<endl;
    return new_context;
};



/* Line comments machine*/


/* Line comment ends */
task_context* EndLineComment_action::doWork(task_context* context,char c)
{
    cout<<"Comment:"<<context->getLastContext()<<endl;
    task_context* prev_context=stack.back();
    stack.pop_back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    return prev_context;
};



/* Delimited comments machines*/



/* Cant find * after / - error version */
task_context* NonComment_error_action::doWork(task_context* context,char c)
{
    cout<<"Error, invalid character: "<<context->getLastContext()<<endl;
    task_context* prev_context=stack.back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    
    stack.pop_back();
    return prev_context;
};

/* Cant find * after / - return version */
task_context* NonComment_return_action::doWork(task_context* context,char c)
{
    task_context* prev_context=stack.back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    stack.pop_back();
    string s=prev_context->getLastContext();
    s.append(1,'/');
    s.append(1,c);
    prev_context->setLastContext(s);
    return prev_context;
};

/* Delimited comment ends*/
task_context* EndDelimitedComment_action::doWork(task_context* context,char c)
{
    task_context* prev_context=stack.back();
    stack.pop_back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    cout<<"Comment: "<<context->getLastContext()<<c<<endl;
    return prev_context;

};



/* String machine */


/* Whitespace or newline in sting */
task_context* StringSpace_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext();
    s.append(1,' ');
    context->setLastContext(s);
    return context;
};
/* Continual spaces in string*/
task_context* StringCSpace_action::doWork(task_context* context,char c)
{
    return context;
};
/* Escape character*/
task_context* EscapeCharacter_action::doWork(task_context* context,char c)
{
    return context;
};
/* Escape backslash*/
task_context* EscapeBackslash_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext();
    s.append("\\");
    context->setLastContext(s);
    return context;
    
};
/* Escape quotes*/
task_context* EscapeQuotes_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext();
    s.append("\"");
    context->setLastContext(s);
    return context;

};
/* Escape Tab*/
task_context* EscapeTab_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext();
    s.append("\t");
    context->setLastContext(s);
    return context;

};
/* Escape newline */
task_context* EscapeNewline_action::doWork(task_context* context,char c)
{
    string s=context->getLastContext();
    s.append("\n");
    context->setLastContext(s);
    return context;

};
/* End string */
task_context* EndString_action::doWork(task_context* context,char c)
{
    task_context* prev_context=stack.back();
    prev_context->getCurrentMachine()->set_context(prev_context);
    stack.pop_back();
    string s=prev_context->getLastContext();
    s.append(context->getLastContext());
    s.append(1,c);
    prev_context->setLastContext(s);
    return prev_context;
};



/* General actions */
task_context* Error_action::doWork(task_context* context,char c)
{
    if((int)c==4)
    {
        if(stack.size()>0)
        {
            cout<<"Error. Unexpected end of file."<<endl;           
        }
        else
        {
            vector<string> s=context->getAllContext();
            if(s.size()==0)
            {
            }
            else if(s.size()==1)
            {
                cout<<"Error. Unexpected end of file."<<endl;
            }
            
            else
            {
                cout<<s.at(0)<<" = "<<s.at(1)<<endl; 
            }
        }
               
    }
    else
    {
        cout<<"Error on token. Unexpected character: "<<context->getLastContext()<<c<<endl;
    }
    return context;
};

}
