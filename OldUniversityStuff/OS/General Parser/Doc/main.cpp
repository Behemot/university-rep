//#include "email.h"
#include "ini.h"
#include <fstream>
#include <iostream>

using namespace ini;
using std::ifstream;
using std::cout;
using std::endl;


finite_state_machine* current_machine=new finite_state_machine(&parameter_table,&parameter_chartypes_vec);
task_context* context=new task_context();       

string input_buffer;

string read_input(char* path)
{
    string buffer;
    ifstream inp_file(path);
    if(inp_file.is_open())
    {
        while(!inp_file.eof()){
          string temp;
          getline(inp_file,temp);
          buffer=buffer+temp+"\n";
        }
        inp_file.close();
    }
    return buffer;
}


int main(int argc,char *argv[])
{
    init_environment();
    
    current_machine=new finite_state_machine(&parameter_table,&parameter_chartypes_vec);
  
    input_buffer=read_input("inp.txt");
    context->setCurrentMachine(current_machine);
    current_machine->set_context(context);
    for(int i=0;i<input_buffer.size();i++){
    //    cout<<i<<" " <<input_buffer.at(i)<<endl;
        context=current_machine->step(input_buffer.at(i));
        current_machine=context->getCurrentMachine();
       
    }
    context=current_machine->step((char)4);
}
