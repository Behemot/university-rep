#!/usr/bin/env python

import math;
import sys;
import random;
import pylab;
import numpy;
class Signal:
    N=8;
    n=256;
    Omega=1100;
    disc_frequency=2*Omega;
    dt=1./disc_frequency;
    T_max=dt*n;
    def __init__(self):
        random.seed();
        for i in range(0,10):
            self.A_p=[(random.random()*10.) for i in range(0,self.N)];
            self.Phi_p=[random.random()*2.*math.pi for i in range(0,self.N)];
            self.Omega_p=[random.random()*self.Omega*1. for i in range(0,self.N)];  
        
    def at_t(self,t):
        
        if (t>=0 and t<=self.T_max):
            return sum(self.A_p[i]*math.sin(self.Omega_p[i]*t+self.Phi_p[i]) for i in range(self.N));
        else:
            return 0;
    def getValues(self,tk):
        points=[];
        t_cur=0;
        while(t_cur<tk):
            points.append(self.at_t(t_cur));
            t_cur+=self.dt;
        return points;    
    def getArgs(self,tk):
        points=[];
        t_cur=0;
        while(t_cur<tk):
            points.append(t_cur);
            t_cur+=self.dt;
        return points;
    def getMatExp(self,points=[]):
        if (type(points).__name__=='float' or type(points).__name__=='float64'):
            temp=points;
            points=self.getValues(temp);
        total=0;
        for i in points:
            total+=i;
        return total/len(points);
    def getDispersion(self,tk):
        points=self.getValues(tk);
        Mx=self.getMatExp(points);
        return sum((i-Mx)**2 for i in points)/len(points);
#------------------------------------------
def getCorrelation(x,y,tau):
    if (tau<Signal.dt): tau=Signal.dt;
    tau_points=(int)(tau/Signal.dt);
    Mx=x.getMatExp(Signal.T_max+tau);
    My=y.getMatExp(Signal.T_max);
    x_p=x.getValues(Signal.T_max+tau);
    y_p=y.getValues(Signal.T_max);
    return sum((x_p[i+tau_points]-Mx)*(y_p[i]-My) for i in range(len(y_p)))/len(y_p);
    #return sum((x_p[i+tau_points]-Mx)*(y_p[i]-My) for i in range(100))/100;
       
#
def discrete_plot(args, vals, xlabel, ylabel, filename):
    
    pylab.plot(args, vals);
    pylab.xlabel(xlabel);
    pylab.ylabel(ylabel);
    pylab.grid(True);
    pylab.savefig(filename);
    pylab.clf();
def save_plot(function, name, filename, range_start = 0, range_end = 4.5E4):
    step = (range_end - range_start) / 300.;
    pltime = numpy.arange(range_start, range_end + step, step);
    plvalue = [function(t) for t in pltime];
    pylab.plot(pltime, plvalue);
    pylab.xlabel(u"t");
    pylab.ylabel(name);
    pylab.grid(True);
    pylab.savefig(filename);
    pylab.clf();
#-------------------------------------------------------    
def main():
    X=Signal();
    Y=Signal();
    print("Mat exp X(t): "+(str)(X.getMatExp(Signal.T_max)));
    print("Dispersion X(t): "+(str)(X.getDispersion(Signal.T_max)));
    print("Mat exp Y(t): "+(str)(Y.getMatExp(Signal.T_max)));
    print("Dispersion Y(t): "+(str)(Y.getDispersion(Signal.T_max)));
    discrete_plot(X.getArgs(Signal.T_max),X.getValues(Signal.T_max),u"T",u"X(t)",u"X(t).png");
    discrete_plot(Y.getArgs(Signal.T_max),Y.getValues(Signal.T_max),u"T",u"Y(t)",u"Y(t).png");
    save_plot(lambda t: getCorrelation(X, Y, t), u"C(X, Y)", "corr.png", 0, Signal.T_max);
    save_plot(lambda t: getCorrelation(X, X, t), u"C(X, X)", "acorr.png", 0, Signal.T_max);
#---------------------------------------------------------
main();

