#include <stdio.h>
#include "log_table.h"
#include <math.h>
int n=1;
int main(int argc, char *argv[])
{
    double x=2.718282;
    while(n<40){
        double x_old=x;
        double y_old=1.-x;
        double phi_old=0.;
        double pow=1.;
        for(int i=1;i<=n;i++)
        {
            pow/=2.;
                
            for(int k=0;k<2;k++)
            {
                double x_new;
                double y_new;
                double x_temp=x_old*pow;
                double y_temp=y_old*pow;
                double phi_temp=pow;
                if(y_old>0.)
                {
                    y_new=y_old-x_temp;
                    x_new=x_old+x_temp;       
                    phi_old=phi_old+log_table_1p[i-1];
                }
                else
                {
                    y_new=y_old+x_temp;
                    x_new=x_old-x_temp;
                    phi_old=phi_old+log_table_1m[i-1];
                };
                x_old=x_new;
                y_old=y_new;
            }    
        }
        printf("Result: N = %d; Phi = %f; Delta = %e\n",n,-phi_old,log(x)+phi_old);
        n++;
   }
   return 0; 
}
