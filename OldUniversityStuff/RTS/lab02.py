#!/usr/bin/env python
# -*- coding: utf8 -*-

import random
import math
import pylab
import numpy

class RandomSignal(object):
    N = 64
    omega_max = 1200
    m = 6
    discr_freq = 2 * omega_max
    delta_t = 1. / discr_freq

    def __init__(self):
        self.A = [random.uniform(0, 10) for i in xrange(0, RandomSignal.m)]
        self.phi = [random.uniform(0, 2 * math.pi) for i in xrange(0, RandomSignal.m)]
        self.omega = [random.uniform(0, RandomSignal.omega_max) for i in xrange(0, RandomSignal.m)]
        self.times = [i * RandomSignal.delta_t for i in xrange(0, RandomSignal.N)]
        self.points = [self._at_t(t) for t in self.times]

    def _at_t(self, t):
        if t < 0 or t > self.delta_t * RandomSignal.N:
            return 0
        return sum(self.A[i] * math.sin(self.omega[i] * t + self.phi[i])
            for i in xrange(0, RandomSignal.m))
#        return math.sin(600*t)


def save_plot(function, name, filename, range_start = 0, range_end = 4.5E4):
    step = (range_end - range_start) / 300.
    pltime = numpy.arange(range_start, range_end + step, step)
    plvalue = [function(t) for t in pltime]
    pylab.plot(pltime, plvalue)
    pylab.xlabel(u"t")
    pylab.ylabel(name)
    pylab.grid(True)
#    pylab.show()
    pylab.savefig(filename)
    pylab.clf()


def plot_points_as_steps(args, vals, xlabel, ylabel, filename):
    pylab.plot(args, vals)
    pylab.xlabel(xlabel)
    pylab.ylabel(ylabel)
    pylab.grid(True)
#    pylab.show()
    pylab.savefig(filename)
    pylab.clf()


def expected_value(x):
    return sum(x.points) / x.N


def variance(x):
    E = expected_value(x)
    return sum((v - E)**2 for v in x.points)

def correlation(x, y, tau):
    Ex = expected_value(x)
    Ey = expected_value(y)
    tau_points = int(tau * RandomSignal.discr_freq)
    s = 0.
    for i in xrange(0, RandomSignal.N):
        s += (x._at_t(i * RandomSignal.delta_t) - Ex) * (y._at_t(i * RandomSignal.delta_t - tau) - Ey)

    return s

def main():
    x = RandomSignal()
    y = RandomSignal()
#    for i in xrange(0, x.N):
#        print x.times[i], x.points[i]
    plot_points_as_steps(x.times, x.points, u"t", u"x(t)", "xdiscr.png")
    plot_points_as_steps(y.times, y.points, u"t", u"y(t)", "ydiscr.png")
    print "E(X) = %f" % (expected_value(x))
    print "Var(X) = %f" % (variance(x))
    print "E(y) = %f" % (expected_value(y))
    print "Var(y) = %f" % (variance(y))
    save_plot(lambda t: correlation(x, y, t), u"C(X, Y)", "corr.png", -0.03, 0.03)
    save_plot(lambda t: correlation(x, x, t), u"C(X, X)", "acorr.png", -0.03, 0.03)

if __name__ == '__main__':
    main()

# vim: sw=4:
