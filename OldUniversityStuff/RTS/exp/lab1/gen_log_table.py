#!/usr/bin/python

from math import log

maxiter = 40

print r"""
/* This is an auto-generated file */

#ifndef _LOG_TABLE_H_
#define _LOG_TABLE_H_

static const int maxiter = %d;

/* log(1. + 2**(-i)) */
static double log_table_1p[%d] = {
""" % (maxiter, maxiter)

for i in xrange(1, maxiter + 1):
    print "  %.25f," % (log(1. + 2**(-i)))

print r"""
};

/* log(1. - 2**(-i)) */
static double log_table_1m[%d] = {
""" % (maxiter)

for i in xrange(1, maxiter + 1):
    print "  %.25f," % (log(1. - 2**(-i)))


print r"""
};

#endif
"""
