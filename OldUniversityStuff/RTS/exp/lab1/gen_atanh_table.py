#!/usr/bin/python

from math import atanh, sqrt

maxiter = 40

print r"""
/* This is an auto-generated file */

#ifndef _ATANH_TABLE_H_
#define _ATANH_TABLE_H_

static const int maxiter = %d;

/* atanh(2**(-i)) */
static double atanh_table[] = {
  0.0,
""" % (maxiter)

for i in xrange(1, maxiter + 1):
    print "  %.25f," % (atanh(2.**(-i)))

print r"""
};
"""

i = 1
nextI = 4
scale = 1.0
while i <= maxiter:
    scale *= sqrt(1. - 2.**(-2*i))
    if i == nextI:
        nextI = 3*i + 1
    else:
        i += 1
scale = 1. / scale

print r"""
static double cordic_hyp_scale = %.25f;

#endif
""" % (scale)

