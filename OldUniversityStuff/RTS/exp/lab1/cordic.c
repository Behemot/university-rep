#include <stdio.h>
#include <math.h>

/*
#include "log_table.h"

double cordic_exp_(double x)
{
  double x_p = 1;
  double phi_p = x;
  double pow2 = 0.5;
  double x_n;
  double phi_n;
  int j = 0;
  for(int i = 0; i < maxiter * 2; i++)
  {
    if(phi_p >= 0.)
    {
      phi_n = phi_p - log_table_1p[j];
    }
    else
    {
      phi_n = phi_p - log_table_1m[j];
    }
    if(phi_p >= 0.)
    {
      x_n = x_p + x_p * pow2;
    }
    else
    {
      x_n = x_p - x_p * pow2;
    }

    x_p = x_n;
    phi_p = phi_n;

    if(i % 2 == 1)
    {
      pow2 /= 2.;
      j++;
    }
  }
  return x_p;
}
*/

#include "atanh_table.h"

double cordic_exp(double x)
{
  double x_p = cordic_hyp_scale;
  double y_p = 0.;
  double z_p = x;
  double x_n, y_n, z_n;
  double pow2 = 0.5;
  int nextI = 4;
  int i = 1;
  while(i <= 40)
  {
    if(z_p < 0.)
    {
      x_n = x_p - y_p * pow2;
      y_n = y_p - x_p * pow2;
      z_n = z_p + atanh_table[i];
    }
    else
    {
      x_n = x_p + y_p * pow2;
      y_n = y_p + x_p * pow2;
      z_n = z_p - atanh_table[i];
    }

    x_p = x_n;
    y_p = y_n;
    z_p = z_n;

    if(i == nextI)
    {
      nextI = 3*i + 1;
    }
    else
    {
      pow2 /= 2.;
      i++;
    }
  }
  return x_p + y_p;
}

void exp_test()
{
  for(double x = 0.; x < 1.5; x += 1e-6)
  {
    double e1 = exp(x);
    double e2 = cordic_exp(x);
    double abs_err = e2 - e1;
    double rel_err = abs_err / e1;
    if(fabs(rel_err) >= 1e-12)
    {
      printf("x = %15e e1 = %15e e2 = %15e aerr = %15e rerr = %15e\n", x, e1, e2, abs_err, rel_err);
    }
  }
}

int main()
{
  // exp_test(); return 0;
  for(double x = 0.; x < 1.2; x += 0.025)
  {
    double e1 = exp(x);
    double e2 = cordic_exp(x);
    double abs_err = e2 - e1;
    double rel_err = abs_err / e1;
    printf("x = %13e e1 = %13e e2 = %13e aerr = %13e rerr = %13e\n", x, e1, e2, abs_err, rel_err);
  }
}

