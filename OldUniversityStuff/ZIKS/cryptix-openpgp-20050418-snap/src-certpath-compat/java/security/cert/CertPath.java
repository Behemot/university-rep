/* $Id: CertPath.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.util.List;
import java.util.Iterator;
import java.io.Serializable;
import java.io.ObjectStreamException;

// present before 1.4
import java.security.cert.CertificateEncodingException;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public abstract class CertPath implements Serializable {
    private String type;
    
    protected static class CertPathRep {
        private String type;
        private byte[] data;
        
        /** Creates a CertPathRep with the specified type and encoded form
         *  form of a certification path. */
        protected CertPathRep(String type, byte[] data) {
            this.type = type;
            this.data = data;
        }
        /** Returns a CertPath constructed from the type and data. */
        protected Object readResolve() {
            // TODO: construct new CertPath through CertificateFactory here
            return null;
        }
    }
    
    /** Creates new CertPath */
    protected  CertPath(String type) {
        this.type = type;
    }
    protected Object writeReplace() throws ObjectStreamException {
        try {
            return new CertPathRep(type, getEncoded());
        } catch(CertificateEncodingException e) {
            return null; // is this correct?
        }
    }
    
    public String getType() {
        return type;
    }
    public boolean equals(Object o) {
        CertPath that = (CertPath)o;
        if(type.equals(that.getType()) && 
                getCertificates().equals(that.getCertificates()))
            return true;
        return false;
    }
    public int hashCode() {
        int hash = type.hashCode();
        hash = 31*hash + getCertificates().hashCode();
        return hash;
    }
    public String toString() {
        Iterator i = getCertificates().iterator();
        StringBuffer sb = new StringBuffer();
        while(i.hasNext())
            sb.append(i.next().toString()).append("\n"); // TODO: check if \n ok
        return sb.toString();
    }
    
    public abstract List getCertificates();
    public abstract byte[] getEncoded() throws CertificateEncodingException;
    public abstract byte[] getEncoded(String encoding) throws CertificateEncodingException;
    public abstract Iterator getEncodings();
}
