/* $Id: CertificateFactorySpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Iterator;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public abstract class CertificateFactorySpi {

    /** Creates new CertificateFactorySpi */
    public CertificateFactorySpi() { return; }

    public abstract Certificate engineGenerateCertificate(InputStream in)
    throws CertificateException;
    public abstract Collection engineGenerateCertificates(InputStream in)
    throws CertificateException;
    public abstract CRL engineGenerateCRL(InputStream in)
    throws CRLException;
    public abstract Collection engineGenerateCRLs(InputStream in)
    throws CRLException;
    
    // API new in 1.4, throws UnsupportedOpEx for backwards compat
    public CertPath engineGenerateCertPath(InputStream in)
    throws CertificateException
    {
        throw new UnsupportedOperationException("Not implemented");
    }
    public CertPath engineGenerateCertPath(InputStream in, String encoding)
    throws CertificateException
    {
        throw new UnsupportedOperationException("Not implemented");
    }
    public CertPath engineGenerateCertPath(List certificates)
    {
        throw new UnsupportedOperationException("Not implemented");
    }
    public Iterator engineGetCertPathEncodings()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
    
}
