/* $Id: CertificateFactory.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.security.Provider;
import java.security.Security;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import java.util.Collection;
import java.util.List;
import java.util.Iterator;

import java.io.InputStream;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public class CertificateFactory {
    private CertificateFactorySpi engine;
    private Provider provider;
    private String type;
    
    private static final String propPrefix = "CertificateFactory.";
    
    /** Creates new CertificateFactory */
    protected CertificateFactory(CertificateFactorySpi engine, Provider provider,
        String type) 
    {
        this.engine = engine;
        this.provider = provider;
        this.type = type;
    }

    public final Provider getProvider() { return provider; }
    public final String getType() { return type; }
    
    public static CertificateFactory getInstance(String type) 
    throws CertificateException
    {
        try {

            Object[] o = Support.getImplementation("CertificateFactory", 
                                                   type);
            return new CertificateFactory( (CertificateFactorySpi)o[0],
                                           (Provider)o[1], type);

        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("Type not found");
        }
    }
    public static CertificateFactory getInstance(String type, String provider) 
    throws CertificateException, NoSuchProviderException
    {
        try {

            Object[] o = Support.getImplementation("CertificateFactory", 
                                                   type, provider);
            return new CertificateFactory( (CertificateFactorySpi)o[0],
                                           (Provider)o[1], type);

        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("Type not found");
        }
    }
    public static CertificateFactory getInstance(String type, Provider provider)
    throws CertificateException
    {
        try {

            Object[] o = Support.getImplementation("CertificateFactory", 
                                                   type, provider);
            return new CertificateFactory( (CertificateFactorySpi)o[0],
                                           (Provider)o[1], type);

        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("Type not found");
        }
    }

    
    public final Certificate generateCertificate(InputStream in)
    throws CertificateException
    {
        return engine.engineGenerateCertificate(in);
    }
    public final Iterator getCertPathEncodings()
    {
        return engine.engineGetCertPathEncodings();
    }
    public final CertPath generateCertPath(InputStream in)
    throws CertificateException
    {
        return engine.engineGenerateCertPath(in);
    }
    public final CertPath generateCertPath(InputStream in, String encoding)
    throws CertificateException
    {
        return engine.engineGenerateCertPath(in, encoding);
    }
    public final CertPath generateCertPath(List certificates)
    throws CertificateException
    {
        return engine.engineGenerateCertPath(certificates);
    }
    public final Collection generateCertificates(InputStream in)
    throws CertificateException
    {
        return engine.engineGenerateCertificates(in);
    }
    public final CRL generateCRL(InputStream in)
    throws CRLException
    {
        return engine.engineGenerateCRL(in);
    }
    public final Collection generateCRLs(InputStream in)
    throws CRLException
    {
        return engine.engineGenerateCRLs(in);
    }

}
