/* $Id: CertPathValidator.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.security.Provider;
import java.security.Security;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.InvalidAlgorithmParameterException;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public class CertPathValidator {
    private CertPathValidatorSpi engine;
    private Provider provider;
    private String algorithm;
    
    private static final String propPrefix = "CertPathValidator.";
    
    /** Creates new CertPathValidator */
    public CertPathValidator(CertPathValidatorSpi engine, Provider provider,
        String algorithm) {
            this.engine = engine;
            this.provider = provider;
            this.algorithm = algorithm;
    }

    public static CertPathValidator getInstance(String algorithm) 
    throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("CertPathValidator", 
                                               algorithm);
        return new CertPathValidator( (CertPathValidatorSpi)o[0],
                                      (Provider)o[1], algorithm);
    }
    public static CertPathValidator getInstance(String algorithm, String provider) 
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("CertPathValidator", 
                                               algorithm, provider);
        return new CertPathValidator( (CertPathValidatorSpi)o[0],
                                      (Provider)o[1], algorithm);
    }
    public static CertPathValidator getInstance(String algorithm, Provider provider)
    throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("CertPathValidator", 
                                               algorithm, provider);
        return new CertPathValidator( (CertPathValidatorSpi)o[0],
                                      (Provider)o[1], algorithm);
    }

    public final Provider getProvider() { return provider; }
    public final String getAlgorithm() { return algorithm; }
    
    public final CertPathValidatorResult validate(CertPath path, 
    CertPathParameters params)
    throws CertPathValidatorException, InvalidAlgorithmParameterException
    {
        return engine.engineValidate(path, params);
    }
    
    public static final String getDefaultType()
    {
        String defaultType = Security.getProperty("certpathvalidator.type");
        if(defaultType != null)
            return defaultType;
        
        return "OpenPGP";
    }
}
