/* $Id: CertStore.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.util.Collection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import java.security.Provider;
import java.security.Security;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.InvalidAlgorithmParameterException;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public class CertStore {
    private CertStoreSpi engine;
    private Provider provider;
    private String type;
    private CertStoreParameters params;
    
    private static final String propPrefix = "CertStore.";
    
    /** Creates new CertStore */
    protected CertStore(CertStoreSpi engine, Provider provider, String type,
    CertStoreParameters params) {
        this.engine = engine;
        this.provider = provider;
        this.type = type;
        this.params = params;
    }

    public final Collection getCertificates(CertSelector selector)
    throws CertStoreException
    {
        return engine.engineGetCertificates(selector);
    }
    
    public final Collection getCRLs(CRLSelector selector)
    throws CertStoreException
    {
        return engine.engineGetCRLs(selector);
    }
    
    public static CertStore getInstance(String algorithm, 
    CertStoreParameters params) 
    throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("CertStore", 
                                          algorithm);
        return new CertStore( getSpiInstance((String)o[0], params),
                              (Provider)o[1], algorithm, params);
    }
    public static CertStore getInstance(String algorithm, 
    CertStoreParameters params, String provider) 
    throws NoSuchAlgorithmException, NoSuchProviderException, 
    InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("CertStore", 
                                          algorithm, provider);
        return new CertStore( getSpiInstance((String)o[0], params),
                              (Provider)o[1], algorithm, params);
    }
    public static CertStore getInstance(String algorithm, CertStoreParameters
    params, Provider provider)
    throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("CertStore", 
                                          algorithm, provider);
        return new CertStore( getSpiInstance((String)o[0], params),
                              (Provider)o[1], algorithm, params);
    }
    
    private static CertStoreSpi getSpiInstance(String classname, 
    CertStoreParameters params)
    throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        try {
            Class spiClass = Class.forName(classname);
            Constructor ctor = spiClass.getConstructor(
                new Class[] { CertStoreParameters.class });
            CertStoreSpi spi = (CertStoreSpi)ctor.newInstance(
                new Object[] { params.clone() });
            return spi;
        } catch(ClassNotFoundException e) {
            throw new NoSuchAlgorithmException("Registered class not found");
        } catch(ClassCastException e) {
            throw new NoSuchAlgorithmException("Registered class is not a CertStoreSpi");
        } catch(NoSuchMethodException e) {
            throw new NoSuchAlgorithmException("Registered class does not have the proper constructor");
        } catch(InvocationTargetException e) {
            throw new NoSuchAlgorithmException("Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        } catch(InstantiationException e) {
            throw new NoSuchAlgorithmException("Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        } catch(IllegalAccessException e) {
            throw new NoSuchAlgorithmException("Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        }
    }
}
