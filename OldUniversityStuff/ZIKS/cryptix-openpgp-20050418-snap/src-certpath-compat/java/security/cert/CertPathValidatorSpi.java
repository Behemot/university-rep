/* $Id: CertPathValidatorSpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package java.security.cert;

import java.security.InvalidAlgorithmParameterException;

/**
 *
 * @author  ingo
 * @version $Revision: 1.2 $
 */
public abstract class CertPathValidatorSpi {

    /** Creates new CertPathValidatorSpi */
    public CertPathValidatorSpi() {
    }

    public abstract CertPathValidatorResult engineValidate(CertPath path,
        CertPathParameters params)
    throws CertPathValidatorException, InvalidAlgorithmParameterException;
}
