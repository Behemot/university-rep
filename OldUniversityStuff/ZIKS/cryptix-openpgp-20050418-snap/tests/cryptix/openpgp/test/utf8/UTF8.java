/* $Id: UTF8.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.utf8;


import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.util.*;


public class UTF8 extends TestCase
{
    private static final String FS = System.getProperty("file.separator");
    private final File _testdir;
    

    public UTF8(String name) 
    {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            _testdir = new File("." + FS + "testdata");
        }
        else {
            _testdir = new File(td + FS + "testdata");
        }
    }
    

    protected void setUp() {
    }

    protected void tearDown() {
    }
  
    
    public void testUTF8() 
        throws Exception
    {
        FileInputStream in;
        Collection msgs;
        KeyBundleMessage kbm;
        EncryptedMessage em;
        LiteralMessage lm;
        SignedMessage sm;
        Iterator it;
        KeyBundle prvDHDSS, pubDHDSS;
        MessageFactory mf = MessageFactory.getInstance("OpenPGP");
        

        in = new FileInputStream(new File(_testdir, "PGP8_testkey_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        prvDHDSS = kbm.getKeyBundle();
        kbm = (KeyBundleMessage)it.next();
        pubDHDSS = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "UTF8_armoured.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly armoured signed", sm.verify(pubDHDSS));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed armoured data", "UTF-8 test \u20ac\u00a9\u00bf\r\n", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "UTF8_cleartext.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed cleartext data ", "UTF-8 test \u20ac\u00a9\u00bf\r\n", lm.getTextData());
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly cleartext signed", sm.verify(pubDHDSS));
    }

}
