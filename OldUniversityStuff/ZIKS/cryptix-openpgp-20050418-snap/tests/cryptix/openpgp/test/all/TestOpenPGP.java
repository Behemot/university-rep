/* $Id: TestOpenPGP.java,v 1.5 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.all;

import junit.framework.*;

import java.util.Enumeration;

public class TestOpenPGP extends TestCase
{
	
	
  public TestOpenPGP()
  {
    super("All OpenPGP tests");
  }
 

	public TestSuite suite() {
		TestSuite all = new TestSuite("All OpenPGP tests");
		all.addTestSuite(cryptix.openpgp.test.util.PGPCompareTest.class);
		all.addTestSuite(cryptix.openpgp.test.util.PGPCRCTest.class);
		all.addTestSuite(cryptix.openpgp.test.util.PGPMPITest.class);
		all.addTestSuite(cryptix.openpgp.test.algorithm.PGPAlgorithmFactoryTest.class);
		all.addTestSuite(cryptix.openpgp.test.algorithm.PGPDSATest.class);
		all.addTestSuite(cryptix.openpgp.test.algorithm.PGPZlibTest.class);
		all.addTestSuite(cryptix.openpgp.test.io.PGPInputStreamAdapterTest.class);
		all.addTestSuite(cryptix.openpgp.test.packet.PGPCompressedDataPacketTest.class);
		all.addTestSuite(cryptix.openpgp.test.packet.PGPDummyPacketTest.class);
		all.addTestSuite(cryptix.openpgp.test.pki.PrincipalBuilderTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.AsciiLastLineTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.CryptixOpenPGPTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.ClearSignTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.EmptyKeyRingTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.PGPKeyPairGeneratorTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.PGPLiteralMessageOutputStreamTest.class);
		all.addTestSuite(cryptix.openpgp.test.provider.PGPUserIDPrincipalBuilderTest.class);
		all.addTestSuite(cryptix.openpgp.test.net.HkpTest.class);
		all.addTestSuite(cryptix.openpgp.test.net.PGPKeyServerImplTest.class);
		all.addTestSuite(cryptix.openpgp.test.interop.PGP5Interop.class);
		all.addTestSuite(cryptix.openpgp.test.interop.PGP8Interop.class);
		all.addTestSuite(cryptix.openpgp.test.interop.ZLibEOFTest.class);
		all.addTestSuite(cryptix.openpgp.test.utf8.UTF8.class);
		return all;
	}
	
	
	/**
   	* Main method needed to make a self runnable class
   	* 
   	* @param args This is required for main method
   	*/
  	public static void main(String[] args) {
        java.security.Security.addProvider(
            new cryptix.jce.provider.CryptixCrypto() );
        java.security.Security.addProvider(
            new cryptix.openpgp.provider.CryptixOpenPGP() );

		System.out.println("Testing, this may take a couple of minutes");

            
        System.getProperties().setProperty("CRYPTIX_OPENPGP_TESTDIR", 
                                           "./build/classes/");

  		TestResult result = new TestResult();
    	new TestOpenPGP().suite().run(result);
    	
    	int errors   = result.errorCount();
    	int failures = result.failureCount();
    	int total    = result.runCount();
    	int passes   = total - errors - failures;
    	
    	TestFailure fail;
    	
    	if(!result.wasSuccessful()) {
    		System.out.println("ERRORS WHILE TESTING!!");
    		for(Enumeration e = result.failures();e.hasMoreElements();) {
    			fail = (TestFailure)e.nextElement();
    			System.out.println("****Failure: " + fail);
    			System.out.println();
    		}
    		for(Enumeration e = result.errors();e.hasMoreElements();) {
    			fail = (TestFailure)e.nextElement();
    			System.out.println("**** Errors: " + fail);
    			System.out.println();
    			fail.thrownException().printStackTrace(System.out);
    			System.out.println("*******************************************");
    		}
    	}
    	
    	System.out.println("");
    	System.out.println("Tests run: " + total);
    	System.out.println("Pass: " + passes);
    	System.out.println("Failures: " + failures);
    	System.out.println("Errors: " + errors );
    		
    	if(!result.wasSuccessful() )
    		System.exit(-1);
    	
  	}
  	
}
