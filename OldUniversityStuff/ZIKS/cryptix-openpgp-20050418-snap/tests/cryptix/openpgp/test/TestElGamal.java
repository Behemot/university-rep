/* $Id: TestElGamal.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test;


import cryptix.openpgp.algorithm.PGPElGamal;

import cryptix.openpgp.io.PGPWrapperDataInputStream;
import cryptix.openpgp.io.PGPWrapperDataOutputStream;

import cryptix.openpgp.util.PGPMPI;

import cryptix.test.Testlet;

import java.io.*;
import java.math.BigInteger;
import java.security.SecureRandom;


/**
 * Tests cryptix.openpgp.algorithm.PGPElGamal
 *
 * @author Erwin van der Koogh (erwin@cryptix.org)
 */
public class TestElGamal extends Testlet {
    
    static final String path = "testdata"+File.separator;
    
    public TestElGamal() {
        super("ElGamal");
    }
    
    public void test() throws Exception {

        PGPElGamal elgamal;
        FileInputStream fis;
        FileOutputStream fos;
        ByteArrayOutputStream baos;
        ByteArrayInputStream bais;
        boolean pass;
        
        SecureRandom sr = new SecureRandom();
        
        beginTest("Generate 512");
            elgamal = new PGPElGamal();
            elgamal.generateKeyPair(512, sr);
        passIf(true);

        beginTest("Encrypt 512");
            elgamal.encrypt("Hello World!".getBytes(), sr);
        passIf(true);
        
        beginTest("Encode encrypted data");
            baos = new ByteArrayOutputStream();
            elgamal.encodeEncryptedData(new PGPWrapperDataOutputStream(baos));
        passIf(true);
        
        beginTest("Decode encrypted data");
            bais = new ByteArrayInputStream(baos.toByteArray() );
            elgamal.decodeEncryptedData(new PGPWrapperDataInputStream(bais));
        passIf(bais.available() == 0);
        
        beginTest("Decrypt 512");
            elgamal.decrypt();
        passIf(true);

    }
    
    
}
