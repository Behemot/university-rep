/* $Id: PGPZlibTest.java,v 1.2 2005/03/13 17:46:40 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.algorithm;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

import cryptix.openpgp.io.PGPCompressorInputStream;
import cryptix.openpgp.io.PGPCompressorOutputStream;

import cryptix.openpgp.algorithm.*;

/**
 * PGPInputStreamAdapterTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.io.PGPInputStreamAdapter </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPZlib extends cryptix.openpgp.algorithm.PGPCompressor <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @date $Date: 2005/03/13 17:46:40 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.algorithm.PGPZlib
 */

public class PGPZlibTest extends TestCase
{


  //Private variables
  
  private static final String CLEARTEXT = 
    "Compression algorithm numbers that indicate which algorithms the key " + 
    "holder prefers to use. Like the preferred symmetric algorithms, list is " +
    "ordered. Algorithm numbers are in section 6. If subpacket is not " +
    "included, ZIP is preferred. A zero denotes uncompressed data is " +
    "preferred the key holders software might have no compression software " +
    "in that implementation. This is only found on a self-signature.";
  
  private static final String fs = System.getProperty("file.separator");
  private File testdir;
  
  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPZlibTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp()
  {
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
  }

  /** 
   * Test inflate/deflate cycle using buffers.
   */
  public void testDeflateInflate() throws Exception
  {
  	PGPZlib zlib = new PGPZlib();

    //
    // Compression
    //
    ByteArrayOutputStream compressedOut = new ByteArrayOutputStream();    
    PGPCompressorOutputStream out = zlib.getCompressionStream(compressedOut);   
    byte[] clear = CLEARTEXT.getBytes("ASCII");
    out.write(clear);
    out.finish();
    out.close();

    byte[] compressed = compressedOut.toByteArray();
    assertTrue("No compressed data produced!", compressed.length > 0);

    //
    // Expansion
    //
    ByteArrayInputStream compressedIn = new ByteArrayInputStream(compressed);
    PGPCompressorInputStream in = zlib.getExpansionStream(compressedIn);
    byte[] expanded = new byte[clear.length + 8];
    int len = in.read(expanded);
    assertEquals("Read wrong number of bytes from stream", clear.length, len);
    
    String str = new String(expanded, 0, len, "ASCII");

    assertEquals(str, CLEARTEXT);
  }
  
  /**
   * Test the inflate/deflate cycle using single bytes.
   */
  public void testDeflateInflateAltInterface() throws Exception {
    
    PGPZlib zlib = new PGPZlib();
    //
    // Compression, one byte at a time
    //
    ByteArrayOutputStream compressedOut = new ByteArrayOutputStream();    
    PGPCompressorOutputStream out = zlib.getCompressionStream(compressedOut);   
    byte[] clear = CLEARTEXT.getBytes("ASCII");
    for(int i=0; i < clear.length; ++i) {
      out.write(byteToInt(clear[i]));
    }
    out.finish();
    out.close();

    byte[] compressed = compressedOut.toByteArray();
    assertTrue("No compressed data produced!", compressed.length > 0);

    //
    // Expansion, one byte at a time
    //
    ByteArrayInputStream compressedIn = new ByteArrayInputStream(compressed);
    PGPCompressorInputStream in = zlib.getExpansionStream(compressedIn);
    byte[] expanded = new byte[clear.length + 8];
    int i=0;
    int b=-1;
    while((b = in.read()) >= 0) {
      expanded[i] = (byte)b;
      if (++i >= expanded.length) {
        throw(new Exception("Too much data returned from stream."));
      }
    }  
    
    String str = new String(expanded, 0, i, "ASCII");
    
    assertEquals(str, CLEARTEXT);    
  }


  private static final int byteToInt(byte b) {
    int i = 0;
    if ((b & 0x80) != 0) {
      i |= 0x80;
      b &= 0x7F;
    }
    i |= b;
    return(i);
  }

}
