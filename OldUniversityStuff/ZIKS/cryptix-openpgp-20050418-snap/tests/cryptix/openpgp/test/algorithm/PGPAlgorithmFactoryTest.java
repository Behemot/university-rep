/* $Id: PGPAlgorithmFactoryTest.java,v 1.2 2005/03/13 17:46:40 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.algorithm;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

import cryptix.openpgp.algorithm.*;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPPublicKeyAlgorithm;
import cryptix.openpgp.algorithm.PGPSigner;
import cryptix.openpgp.algorithm.PGPEncryptor;
import cryptix.openpgp.algorithm.PGPCompressor;
import cryptix.openpgp.algorithm.PGPStringToKey;
import java.security.MessageDigest;
import javax.crypto.Cipher;

/**
 * PGPAlgorithmFactoryTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.algorithm.PGPAlgorithmFactory </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPAlgorithmFactory extends java.lang.Object <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:40 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.algorithm.PGPAlgorithmFactory
 */

public class PGPAlgorithmFactoryTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPAlgorithmFactory pgpalgorithmfactory;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPAlgorithmFactoryTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    //pgpalgorithmfactory = new PGPAlgorithmFactory();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    pgpalgorithmfactory = null;
  }

  /**
   * Test the constructor: PGPAlgorithmFactory(int)
   */
  public void testPGPAlgorithmFactory_1() {
  	//Must test for the following parameters!
	int intValues [] = {-1, 0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
  }

  /**
   * Test all default values
   */
  public void testAllDefault() throws Exception{
  	
        PGPAlgorithmFactory factory = 
            new PGPAlgorithmFactory(PGPAlgorithmFactory.INIT_DEFAULT);

        PGPPublicKeyAlgorithm pka;
        PGPCompressor comp;
        PGPStringToKey s2k;
        MessageDigest md;
        Cipher ciph;
        
        
        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(1);
            } catch (RuntimeException re) {
                re.printStackTrace();
            }
        assertTrue((pka instanceof PGPSigner) && (pka instanceof PGPEncryptor));

        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(2);
            } catch (RuntimeException re) {
            }
        assertTrue((pka instanceof PGPEncryptor));

        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(3);
            } catch (RuntimeException re) {
            }
        assertTrue((pka instanceof PGPSigner));

        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(16);
            } catch (RuntimeException re) {
            }
        assertTrue((pka instanceof PGPEncryptor));

        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(17);
            } catch (RuntimeException re) {
            }
        assertTrue((pka instanceof PGPSigner));

        pka = null;
            try {
                pka = factory.getPublicKeyAlgorithm(20);
            } catch (RuntimeException re) {
            }
        assertTrue((pka instanceof PGPSigner) && (pka instanceof PGPEncryptor));

        comp = null;
            try {
                comp = factory.getCompressionAlgorithm(0);
            } catch (RuntimeException re) {
            }
        assertTrue(comp instanceof PGPCompressor);
/*
        comp = null;
        beginTest(" 1: ZIP");
            try {
                comp = factory.getCompressionAlgorithm(1);
            } catch (RuntimeException re) {
            }
        assertTrue(comp instanceof PGPCompressor);
*/
        comp = null;
            try {
                comp = factory.getCompressionAlgorithm(2);
            } catch (RuntimeException re) {
            }
        assertTrue(comp instanceof PGPCompressor);

        s2k = null;
            try {
                s2k = factory.getS2KAlgorithm(0);
            } catch (RuntimeException re) {
            }
        assertTrue(s2k instanceof PGPStringToKey);

        s2k = null;
            try {
                s2k = factory.getS2KAlgorithm(1);
            } catch (RuntimeException re) {
            }
        assertTrue(s2k instanceof PGPStringToKey);

        s2k = null;
            try {
                s2k = factory.getS2KAlgorithm(3);
            } catch (RuntimeException re) {
            }
        assertTrue(s2k instanceof PGPStringToKey);


        md = null;
            try {
                md = factory.getHashAlgorithm(1);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);

        md = null;
            try {
                md = factory.getHashAlgorithm(2);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);

        md = null;
            try {
                md = factory.getHashAlgorithm(3);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);

/*        md = null;
        beginTest(" 4: DW SHA");
            try {
                md = factory.getHashAlgorithm(4);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);
*/
        md = null;
            try {
                md = factory.getHashAlgorithm(5);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);

        md = null;
            try {
                md = factory.getHashAlgorithm(6);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);

/*        md = null;
        beginTest(" 7: HAVAL");
            try {
                md = factory.getHashAlgorithm(7);
            } catch (RuntimeException re) {
            }
        assertTrue(md instanceof MessageDigest);
*/

        ciph = null;
//        beginTest(" 1: IDEA     /CFB");
            try {
                ciph = factory.getCipherAlgorithm(1, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//      beginTest(" 2: TripleDES/CFB");
            try {
                ciph = factory.getCipherAlgorithm(2, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 3: CAST5    /CFB");
            try {
                ciph = factory.getCipherAlgorithm(3, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 4: Blowfish /CFB");
            try {
                ciph = factory.getCipherAlgorithm(4, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

/*        ciph = null;
        beginTest("5: SAFER     /CFB");
            try {
                ciph = factory.getCipherAlgorithm(5, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);
*/
        ciph = null;
//        beginTest(" 7: AES      /CFB");
            try {
                ciph = factory.getCipherAlgorithm(9, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 8: AES      /CFB");
            try {
                ciph = factory.getCipherAlgorithm(9, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 9: AES      /CFB");
            try {
                ciph = factory.getCipherAlgorithm(9, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest("10: Twofish  /CFB");
            try {
                ciph = factory.getCipherAlgorithm(10, "CFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher); 

        ciph = null;
//        beginTest(" 1: IDEA     /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(1, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 2: TripleDES/OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(2, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 3: CAST5    /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(3, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

        ciph = null;
//        beginTest(" 4: Blowfish /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(4, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);

/*        ciph = null;
        beginTest(" 5: SAFER    /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(5, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher);
*/
        ciph = null;
//        beginTest(" 7: AES      /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(7, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher); 

        ciph = null;
//        beginTest(" 8: AES      /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(8, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher); 

        ciph = null;
//        beginTest(" 9: AES      /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(9, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher); 

        ciph = null;
//        beginTest("10: Twofish  /OpenpgpCFB");
            try {
                ciph = factory.getCipherAlgorithm(10, "OpenpgpCFB");
            } catch (RuntimeException re) {
            }
        assertTrue(ciph instanceof Cipher); 
  }


  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPAlgorithmFactoryTest.class) );
  }
}
