/* $Id: PGPDSATest.java,v 1.2 2005/03/13 17:46:40 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.algorithm;

import cryptix.openpgp.algorithm.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.security.SecureRandom;

import cryptix.openpgp.io.PGPWrapperDataInputStream;
import cryptix.openpgp.io.PGPWrapperDataOutputStream;
import cryptix.openpgp.*;

/**
 * PGPDSATest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.algorithm.PGPDSA </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPDSA extends java.lang.Object <br>
 *     PGPDSA implements cryptix.openpgp.algorithm.PGPSigner </p>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:40 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.algorithm.PGPDSA
 */

public class PGPDSATest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPDSA dsa;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPDSATest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR");
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
  	dsa = new PGPDSA();
    dsa.generateKeyPair(512,new SecureRandom("Blirp".getBytes()));
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    dsa = null;
  }

  /**
   * Test method: boolean equals(Object)
   */
  public void testEquals() {
    //Must test for the following parameters!
        
    PGPDSA dsa2 = new PGPDSA();
    dsa2.generateKeyPair(512,new SecureRandom("Blirp".getBytes()));

	PGPDSA dsa3 = new PGPDSA();
    dsa3.generateKeyPair(512,new SecureRandom("NOO!!".getBytes()));
    
    assertTrue("Same Object", dsa.equals(dsa) );
    assertTrue("Different Object, same", dsa.equals(dsa2) );
    assertTrue("Different", !dsa.equals(dsa3) );
    assertTrue("Public clone", !dsa.equals(dsa.clonePublic()) );
    assertTrue("null", !dsa.equals(null) );
    assertTrue("Public clone", !dsa.clonePublic().equals(dsa) );

  }

  /**
   * Test method: int hashCode()
   */
  public void testHashCode() {
  	dsa.hashCode();
  	assertTrue(true);
  }

  /**
   * Test method: String toString()
   */
  public void testToString() {
	String str = dsa.toString();
	assertEquals("DSA", str);
  }

  /**
   * Test method: boolean signatureEquals(PGPSigner)
   */
  public void testSignatureEquals() {
    //Must test for the following parameters!
    //PGPSigner;
	//FIXME should include a test
  }

  /**
   * Test method: void decodePublicData(PGPDataInputStream)
   * decodePublicData throws java.io.IOException
   * decodePublicData throws cryptix.openpgp.PGPDataFormatException
   * decodePublicData throws cryptix.openpgp.PGPFatalDataFormatException
   */
  public void testDecodePublicData() throws Exception{
    //Must test for the following parameters!
    //PGPDataInputStream;
	
	FileInputStream fis = new FileInputStream(new File(testdir, "TestDSA1.dat") );
    fis.skip(9);
    PGPDSA dsa = new PGPDSA();
    dsa.decodePublicData(new PGPWrapperDataInputStream(fis));
    assertTrue(true);
  }

  /**
   * Test method: void decodeSecretData(PGPDataInputStream)
   * decodeSecretData throws java.io.IOException
   * decodeSecretData throws cryptix.openpgp.PGPDataFormatException
   * decodeSecretData throws cryptix.openpgp.PGPFatalDataFormatException
   */
  public void testDecodeSecretData() throws Exception{
    //Must test for the following parameters!
    //PGPDataInputStream;
    
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    dsa.encodeSecretData(new PGPWrapperDataOutputStream(baos));
    baos.close();
    PGPDSA dsa2 = (PGPDSA)dsa.clonePublic();
    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray() );
    dsa2.decodeSecretData(new PGPWrapperDataInputStream(bais));
	assertEquals(dsa, dsa2);
  }

  /**
   * Test method: void encodePublicData(PGPDataOutputStream)
   * encodePublicData throws java.io.IOException
   */
  public void testEncodePublicData() throws Exception {
    //Must test for the following parameters!
    //PGPDataOutputStream;

	FileOutputStream fos = new FileOutputStream(new File(testdir, "TestDSA1.out"));
    dsa.encodePublicData(new PGPWrapperDataOutputStream(fos));
    fos.close();
    assertTrue(true);

  }

  /**
   * Test method: void encodeSecretData(PGPDataOutputStream)
   * encodeSecretData throws java.io.IOException
   */
  public void testEncodeSecretData() throws Exception{
    //Must test for the following parameters!
    //PGPDataOutputStream;
    
    FileOutputStream fos = new FileOutputStream(new File(testdir, "TestDSA1.out"));
    dsa.encodePublicData(new PGPWrapperDataOutputStream(fos));
    fos.close();
    assertTrue(true);

  }

  /**
   * Test method: void forgetSecretData()
   */
  public void testForgetSecretData() {
  	
  	dsa.forgetSecretData();
  	try {
  		dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance());
  		fail("Should have thrown an exception");
  	}
  	catch(IllegalStateException isex) {
  		assertTrue(true);
  	}
  	
  }

  /**
   * Test method: void generateKeyPair(int, SecureRandom)
   */
  public void testGenerateKeyPair() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, 512, 768, 1024, 2048};
	for(int i = 0; i < intValues.length;i++) {
		try {
			dsa = new PGPDSA();
    		dsa.generateKeyPair(intValues[i], new SecureRandom("Blirp".getBytes()));
    		assertTrue(intValues[i] >= 512 && intValues[i] <= 1024);
    	}
    	catch(Exception ex) {
    		assertTrue(intValues[i] < 512 || intValues[i] > 1024);
    	}
    }
    
  }

  /**
   * Test method: cryptix.openpgp.algorithm.PGPPublicKeyAlgorithm clonePublic()
   */
  public void testClonePublic() {
	PGPDSA dsa2 = (PGPDSA)dsa.clonePublic();
	assertTrue(!dsa.equals(dsa2) );
	dsa.forgetSecretData();
	assertTrue(dsa.equals(dsa2) );
  }

  /**
   * Test method: cryptix.openpgp.algorithm.PGPPublicKeyAlgorithm clonePrivate()
   */
  public void testClonePrivate() {
  	PGPDSA dsa2 = (PGPDSA)dsa.clonePrivate();
	assertTrue(dsa.equals(dsa2) );
	dsa.forgetSecretData();
	assertTrue(!dsa.equals(dsa2) );
  }

  /**
   * Test method: void initSign(int, PGPAlgorithmFactory)
   */
  public void testInitSign() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, 2, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for(int i = 0; i < intValues[i];i++) {
    	try {
    		dsa.initSign(intValues[i], PGPAlgorithmFactory.getDefaultInstance() );
  			assertTrue(intValues[i] == 2);
  		}
  		catch(IllegalArgumentException isex) {
  			assertTrue(intValues[i] != 2);
  		}
  	}
  	PGPDSA dsa2 = (PGPDSA)dsa.clonePublic();
  	try {
  		dsa2.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
  		fail("Should have thrown an exception");
  	}
  	catch(IllegalStateException isex) {
  		assertTrue(true);
  	}
  }

  /**
   * Test method: void initVerify(int, PGPAlgorithmFactory)
   */
  public void testInitVerify() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, 2, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for(int i = 0; i < intValues[i];i++) {
    	try {
    		dsa.initVerify(intValues[i], PGPAlgorithmFactory.getDefaultInstance() );
  			assertTrue(intValues[i] == 2);
  		}
  		catch(IllegalStateException isex) {
  			assertTrue(intValues[i] != 2);
  		}
  	}
  }

  /**
   * Test method: void update(byte[])
   */
  public void testUpdate_1() {
    byte[] bytes = {0x01, 0x02, 0x03};
    PGPDSA dsa2 = (PGPDSA)dsa.clonePublic();
    try {
    	dsa.update(bytes);
    	fail("Should have thrown an exception");
    }
    catch(IllegalStateException isex) {
		assertTrue(true);
  	}
  	dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
  	dsa.update(bytes);
  	assertTrue(true);
  	
  	try {
    	dsa2.update(bytes);
    	fail("Should have thrown an exception");
    }
    catch(IllegalStateException isex) {
		assertTrue(true);
  	}
  	
  	dsa2.initVerify(2, PGPAlgorithmFactory.getDefaultInstance() );
  	dsa2.update(bytes);
  }

  /**
   * Test method: void computeSignature()
   */
  public void testComputeSignature() {
	int[] ints = {512, 768, 1024};
	for(int i = 0; i < ints.length;i++) {
		dsa = new PGPDSA();
        dsa.generateKeyPair(ints[i],new SecureRandom("Blirp".getBytes()));
        dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
        dsa.update("Hello World!".getBytes());
        dsa.computeSignature();
        assertTrue(true);
	}        
  }

  /**
   * Test method: boolean verifySignature()
   */
  public void testVerifySignature() throws Exception{
  	
  	PGPDSA dsa2 = new PGPDSA();
  	dsa2.generateKeyPair(768, new SecureRandom("Wrong!!".getBytes()));
  	
  	dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
    dsa.update("Hello World!".getBytes());
    dsa.computeSignature();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    dsa.encodeSignatureData(new PGPWrapperDataOutputStream(baos));
        
    dsa.initVerify(2, PGPAlgorithmFactory.getDefaultInstance() );
    dsa.update("Hello World!".getBytes());
    assertTrue(dsa.verifySignature() );
    
    dsa.initVerify(2, PGPAlgorithmFactory.getDefaultInstance() );
    dsa.update("Wrong text".getBytes() );
    assertTrue(!dsa.verifySignature() );
    
    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    dsa2.decodeSignatureData(new PGPWrapperDataInputStream(bais));
    dsa2.initVerify(2, PGPAlgorithmFactory.getDefaultInstance());
    dsa2.update("Hello World!".getBytes());
    assertTrue(!dsa2.verifySignature() );
    
  }

  /**
   * Test method: void decodeSignatureData(PGPDataInputStream)
   * decodeSignatureData throws java.io.IOException
   * decodeSignatureData throws cryptix.openpgp.PGPDataFormatException
   * decodeSignatureData throws cryptix.openpgp.PGPFatalDataFormatException
   */
  public void testDecodeSignatureData() throws Exception{
    //Must test for the following parameters!
 
   	dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
    dsa.update("Hello World!".getBytes());
    dsa.computeSignature();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    dsa.encodeSignatureData(new PGPWrapperDataOutputStream(baos));
    
    PGPDSA dsa2 = (PGPDSA)dsa.clonePrivate();
    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    dsa2.decodeSignatureData(new PGPWrapperDataInputStream(bais));
    assertTrue(dsa.signatureEquals(dsa2) );

  }

  /**
   * Test method: void encodeSignatureData(PGPDataOutputStream)
   * encodeSignatureData throws java.io.IOException
   */
  public void testEncodeSignatureData() throws IOException {
    //Must test for the following parameters!
    dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance() );
    dsa.update("Hello World!".getBytes());
    dsa.computeSignature();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    dsa.encodeSignatureData(new PGPWrapperDataOutputStream(baos));
    assertTrue(true);
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPDSATest.class) );
  }
}
