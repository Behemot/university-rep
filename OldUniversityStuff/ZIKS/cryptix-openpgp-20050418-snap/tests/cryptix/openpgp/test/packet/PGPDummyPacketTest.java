/* $Id: PGPDummyPacketTest.java,v 1.2 2005/03/13 17:46:58 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.packet;

import cryptix.openpgp.packet.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

/**
 * PGPDummyPacketTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.packet.PGPDummyPacket </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPDummyPacket extends cryptix.openpgp.packet.PGPPacket <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:58 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.packet.PGPDummyPacket
 */

public class PGPDummyPacketTest extends TestCase
{


  //Private variables
  
  static final PGPAlgorithmFactory afactory = new PGPAlgorithmFactory(0);
  
  private static final String fs = System.getProperty("file.separator");
  private static final byte[] full  = {0x01, 0x02, 0x03};
  private PGPDummyPacket pgpdummypacket;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPDummyPacketTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    pgpdummypacket = new PGPUnknownPacket();
    pgpdummypacket.setPacketID((byte)2);
    pgpdummypacket.setPayload(full);
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    pgpdummypacket = null;
  }

  /**
   * Test method: void setPayload(byte[])
   */
  public void testSetPayload() {
    //Must test for the following parameters!
	byte[] nul   = null;
	byte[] empty = new byte[10];
	byte[] zero  = new byte[0];
	byte[][] bytes = {nul, empty, zero, full};
	for(int i = 0; i < bytes.length; i++) {
		try {
			pgpdummypacket.setPayload(bytes[i]);
			assertTrue(bytes[i] != null && bytes[i].length > 0);
		}
		catch(IllegalArgumentException iaex) {
			assertTrue(bytes[i] == null || bytes[i].length <= 0);
		}
		catch(Exception ex) {
			fail("Should fail with an IllegalArgumentException");
		}
	}
	

  }

  /**
   * Test method: boolean equals(Object)
   */
  public void testEquals() {
    //Must test for the following parameters!
    Object nul = null;
    byte[] empty = new byte[10];
    PGPDummyPacket same = new PGPUnknownPacket();
    same.setPacketID((byte)2);
    same.setPayload(full);
    PGPDummyPacket idDiff = new PGPUnknownPacket();
    idDiff.setPacketID((byte)12);
    idDiff.setPayload(full);
    PGPDummyPacket plDiff = new PGPUnknownPacket();
    plDiff.setPacketID((byte)2);
    plDiff.setPayload(new byte[10]);
    
    assertTrue(pgpdummypacket.equals(same) );
    assertTrue(!pgpdummypacket.equals(nul) );
    assertTrue(!pgpdummypacket.equals(idDiff) );
    assertTrue(!pgpdummypacket.equals(plDiff) );

  }

  /**
   * Test method: void decodeBody(PGPPacketDataInputStream, PGPAlgorithmFactory)
   * decodeBody throws java.io.IOException
   * decodeBody throws cryptix.openpgp.PGPFatalDataFormatException
   * decodeBody throws cryptix.openpgp.PGPDataFormatException
   */
  public void testDecodeBody() throws Exception{
  	
	PGPPacketFactory factory = new PGPPacketFactory (
    	PGPPacketFactory.INIT_DEFAULT );
    for (int i=2; i<=14; i++) {
    	factory.unregisterPacket(i);
    }
          
    for (int i=2; i<=14; i++) {
	    factory.registerPacket(i,
    	    "cryptix.openpgp.packet.PGPUnknownPacket");
    }
                         
   	PGPPacket pkt;
   	FileInputStream fis;
        
   	fis = new FileInputStream(new File(testdir, "seckey.dat") );
   	pkt = factory.readPacket(fis,afactory);
    pkt = factory.readPacket(fis,afactory);
    pkt = factory.readPacket(fis,afactory);
    pkt = factory.readPacket(fis,afactory);
    pkt = factory.readPacket(fis,afactory);
    assertTrue(fis.available() == 0);

    fis = new FileInputStream(new File(testdir, "test.pgpsig") );
    pkt = factory.readPacket(fis,afactory);
    assertTrue(fis.available() == 0);
  }

  /**
   * Test method: void encodeBody(PGPPacketDataOutputStream)
   * encodeBody throws java.io.IOException
   */
  public void testEncodeBody() throws Exception{
    //Must test for the following parameters!
    PGPPacketFactory factory = new PGPPacketFactory (
    	PGPPacketFactory.INIT_DEFAULT );
    for (int i=2; i<=14; i++) {
    	factory.unregisterPacket(i);
    }
          
    for (int i=2; i<=14; i++) {
	    factory.registerPacket(i,
    	    "cryptix.openpgp.packet.PGPUnknownPacket");
    }
                         
   	PGPPacket pkt;
   	PGPPacket pkt2;
   	FileInputStream fis;
   	File one = new File(testdir, "test.pgpsig");
   	File two = new File(testdir, "test.pgpsig2");
        
   	fis = new FileInputStream( one );
   	pkt = factory.readPacket(fis,afactory);
    assertTrue(fis.available() == 0);
    
    FileOutputStream fos = new FileOutputStream( two );
    pkt.encodeBody(new PGPPacketDataOutputStream(fos, pkt.getPacketID() ));
    fos.close();
   
    fis = new FileInputStream( two );
   	pkt2 = factory.readPacket(fis,afactory);
   	
   	assertEquals(pkt, pkt2);
	
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPDummyPacketTest.class) );
  }
}
