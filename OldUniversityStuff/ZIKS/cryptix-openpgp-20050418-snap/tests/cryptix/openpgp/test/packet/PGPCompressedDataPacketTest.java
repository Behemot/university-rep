/* $Id: PGPCompressedDataPacketTest.java,v 1.2 2005/03/13 17:46:42 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.packet;


import cryptix.openpgp.packet.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.util.*;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPConstants;

import cryptix.openpgp.io.PGPPacketDataOutputStream;


/**
 * PGPCompressedDataPacketTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.packet.PGPCompressedDataPacket </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPCompressedDataPacket extends cryptix.openpgp.packet.PGPContainerPacket <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:42 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.packet.PGPCompressedDataPacket
 */

public class PGPCompressedDataPacketTest extends TestCase
{


  //Private variables
  
  private static final byte COMPRESSED_DATA_PACKET_ID = 8;
  private static final byte USERID_PACKET_ID = 13;
  
  private static final String fs = System.getProperty("file.separator");
  private PGPCompressedDataPacket packet;
  private Vector memory;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPCompressedDataPacketTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    packet = new PGPCompressedDataPacket();
    packet.setAlgorithmID(PGPConstants.COMP_NOCOMPRESSION);
  }


  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    packet = null;
    memory = null;
  }

  /**
   * Test method: void setAlgorithmID(int)
   */
  public void testSetAlgorithmID() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, 256, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for(int i = 0; i < intValues.length;i++) {
    	try {
    		packet.setAlgorithmID(intValues[i]);
    		assertTrue(intValues[i] >= 0 && intValues[i] <=255);
    	}
    	catch(IllegalArgumentException iaex) {
    		assertTrue(intValues[i] > 255 || intValues[i] < 0);
    	}
    	catch(Exception ex) {
    		fail("Should fail with an IllegalArgumentException");
    	}
    }
  }

  /**
   * Test method: void decodeBody(PGPPacketDataInputStream, PGPAlgorithmFactory)
   * decodeBody throws java.io.IOException
   * decodeBody throws cryptix.openpgp.PGPFatalDataFormatException
   * decodeBody throws cryptix.openpgp.PGPDataFormatException
   */
  public void testDecodeBody() throws Exception {
    //FIXME: Should include all of them
    int[] ints = {PGPConstants.COMP_NOCOMPRESSION, PGPConstants.COMP_ZLIB };
	for(int i = 0; i < ints.length; i++) {
		PGPCompressedDataPacket p = new PGPCompressedDataPacket();
    	p.setAlgorithmID(ints[i]);
    	setUpPacketAndMemory(p);
    	ByteArrayOutputStream buf = new ByteArrayOutputStream();
    	PGPPacketDataOutputStream pout = new PGPPacketDataOutputStream(buf, 
    	    COMPRESSED_DATA_PACKET_ID);
    
    	p.encodeBody(pout);
    	pout.close();  // This step is essential
    	
    	// 
    	// now decode the packet and verify the contents
    	//    
    	byte[] raw = buf.toByteArray();
    	assertTrue(raw.length > 0);

	    ByteArrayInputStream in = new ByteArrayInputStream(raw);
    	PGPPacket p2 = PGPPacketFactory.getDefaultInstance().readPacket(in, 
    	    PGPAlgorithmFactory.getDefaultInstance());
    	assertTrue("Factory returned wrong kind of packet", 
    	    p2 instanceof PGPCompressedDataPacket);

		assertTrue(memory.size() > 0);
	    // the factory should have called decode for us...
    	Enumeration plist = ((PGPCompressedDataPacket) p2).listPackets();
    	while(plist.hasMoreElements()) {
      		PGPPacket sub = (PGPPacket) plist.nextElement();
      		int j = memory.indexOf(sub); // checks for equality too
      		assertTrue("Returned packet contains unknown sub packet!", j >= 0);
      		memory.removeElementAt(j);
    	}
    	assertTrue(memory.size() == 0);   
    }
  }

  /**
   * Test method: void encodeBody(PGPPacketDataOutputStream)
   * encodeBody throws java.io.IOException
   */
  public void testEncodeBody() throws IOException{
  	//FIXME: Should include all of them
    int[] ints = {PGPConstants.COMP_NOCOMPRESSION, PGPConstants.COMP_ZLIB };
	for(int i = 0; i < ints.length; i++) {
		PGPCompressedDataPacket p = new PGPCompressedDataPacket();
    	p.setAlgorithmID(ints[i]);
    	ByteArrayOutputStream buf = new ByteArrayOutputStream();
    	PGPPacketDataOutputStream pout = new PGPPacketDataOutputStream(buf, COMPRESSED_DATA_PACKET_ID);
    
    	p.encodeBody(pout);
    	pout.close();  // This step is essential
    	assertTrue(true);
    }
  }

  private void setUpPacketAndMemory(PGPCompressedDataPacket packet) {

	memory = new Vector();
    PGPPacket sub1 = makePacket("The first octet of the packet header is called the \"Packet Tag.\" It determines the format of the header and denotes the packet contents.  The remainder of the packet header is the length of the packet.");

    PGPPacket sub2 = makePacket("Note that the most significant bit is the left-most bit, called bit 7. A mask for this bit is 0x80 in hexadecimal.");

    PGPPacket sub3 = makePacket("PGP 2.6.x only uses old format packets. Thus, software that interoperates with those versions of PGP must only use old format packets. If interoperability is not an issue, either format may be used.");

    packet.appendPacket(sub1);
    packet.appendPacket(sub2);
    packet.appendPacket(sub3);

    memory.addElement(sub1);
    memory.addElement(sub2);
    memory.addElement(sub3);
  }
  
  private PGPPacket makePacket(String str) {
    PGPUserIDPacket p = new PGPUserIDPacket();
    p.setPacketID(USERID_PACKET_ID);
    p.setValue(str);
    return(p);
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPCompressedDataPacketTest.class) );
  }
}
