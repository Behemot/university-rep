/* $Id: PGPCRCTest.java,v 1.2 2005/03/13 17:47:00 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.util;

import cryptix.openpgp.util.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

/**
 * PGPCRCTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.util.PGPCRC </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPCRC extends java.lang.Object <br>
 *     PGPCRC implements java.util.zip.Checksum </p>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:47:00 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.util.PGPCRC
 */

public class PGPCRCTest extends TestCase
{

  //Private variables
  
  private static final byte[] RANDOM_DATA = 
        "h42aroiuaq oimcuafiu3ncrauy3rvnuwanyia".getBytes();
  private static final int RANDOM_DATA_CRC = 14240528;
  
  private static final byte[] b0 = new byte[0];
  private byte[] b1 = {0x01};  
  private byte[] b2 = {0x02};
  private byte[] b3 = {0x01, 0x02};
  
  private static final String fs = System.getProperty("file.separator");
  private PGPCRC pgpcrc;
  private File testdir;
  

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPCRCTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    pgpcrc = new PGPCRC();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    pgpcrc = null;
  }

  /**
   * Test method: int checksum(byte[])
   */
  public void testChecksum() {
    //Must test for the following parameters!
    //byte[];

	int one = PGPCRC.checksum(b1);
	int two = PGPCRC.checksum(b2);
	int three = PGPCRC.checksum(b3);
	assertTrue(two != three); //check whether it resets
	
	assertEquals(PGPCRC.checksum(RANDOM_DATA), RANDOM_DATA_CRC );
  }

  /**
   * Test method: void update(byte[])
   */
  public void testUpdate_2() {
    //Must test for the following parameters!
    //byte[];

  }

  /**
   * Test method: void update(int)
   */
  public void testUpdate() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for(int i = 0; i < intValues.length;i++) {
    	try {
    		pgpcrc.update(intValues[i]);
    		assertTrue( intValues[i] <= 255 && intValues[i] >= 0 );
    	}
    	catch(IllegalArgumentException iaex) {
    		assertTrue(intValues[i] > 255 || intValues[i] < 0 );
    	}
    }
  }

  /**
   * Test method: void reset()
   */
  public void testReset() {
  	pgpcrc.update(b1);
  	pgpcrc.reset();
  	pgpcrc.update(b2);
  	int one = (int)pgpcrc.getValue();
  	int two = PGPCRC.checksum(b3);
  	
	assertTrue(one != two);
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPCRCTest.class) );
  }
}
