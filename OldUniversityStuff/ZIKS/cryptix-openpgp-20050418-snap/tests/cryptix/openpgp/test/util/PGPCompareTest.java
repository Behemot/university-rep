/* $Id: PGPCompareTest.java,v 1.2 2005/03/13 17:47:00 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.util;

import cryptix.openpgp.util.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.util.*;

/**
 * PGPCompareTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.util.PGPCompare </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPCompare extends java.lang.Object <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:47:00 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.util.PGPCompare
 */

public class PGPCompareTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPCompare pgpcompare;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPCompareTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File("." + fs + "testdata");
    }
    else {
        testdir = new File(td);
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    //pgpcompare = new PGPCompare();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    pgpcompare = null;
  }
  
  /**
   * Test method: boolean equals(long, long)
   */
  public void testEquals_11() {
    //Must test for the following parameters!
    long lValues [] = {-1, 0, 1, Long.MAX_VALUE, Long.MIN_VALUE};
    for(int i = 0; i < lValues.length;i++) {
    	assertTrue(PGPCompare.equals(lValues[i], lValues[i]) );
    	assertTrue(!PGPCompare.equals(lValues[i], lValues[(i+1)%lValues.length]) );
    }
  }

  /**
   * Test method: boolean equals(int, int)
   */
  public void testEquals_10() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
	for(int i = 0; i < intValues.length;i++) {
    	assertTrue(PGPCompare.equals(intValues[i], intValues[i]) );
    	assertTrue(!PGPCompare.equals(intValues[i], intValues[(i+1)%intValues.length]) );
    }
  }

  /**
   * Test method: boolean equals(short, short)
   */
  public void testEquals_9() {
    //Must test for the following parameters!
    short sValues [] = {-1, 0, 1, Short.MAX_VALUE, Short.MIN_VALUE };
	for(int i = 0; i < sValues.length;i++) {
    	assertTrue(PGPCompare.equals(sValues[i], sValues[i]) );
    	assertTrue(!PGPCompare.equals(sValues[i], sValues[(i+1)%sValues.length]) );
    }
  }

  /**
   * Test method: boolean equals(byte, byte)
   */
  public void testEquals_8() {
    //Must test for the following parameters!
    byte bValues [] = { Byte.MAX_VALUE, Byte.MIN_VALUE };
	assertTrue(PGPCompare.equals(bValues[0], bValues[0]) );
	assertTrue(!PGPCompare.equals(bValues[0], bValues[1]) );
	assertTrue(PGPCompare.equals(bValues[1], bValues[1]) );
	assertTrue(!PGPCompare.equals(bValues[1], bValues[0]) );
  }

  /**
   * Test method: boolean equals(boolean, boolean)
   */
  public void testEquals_7() {
    //Must test for the following parameters!
    boolean BValues [] = { true, false };
	assertTrue(PGPCompare.equals(true, true) );
	assertTrue(!PGPCompare.equals(true, false) );
	assertTrue(PGPCompare.equals(false, false) );
  }

  /**
   * Test method: boolean equals(float, float)
   */
  public void testEquals_6() {
    //Must test for the following parameters!
    float fValues [] = {-1f, 0f, 1f, Float.MAX_VALUE, Float.MIN_VALUE};
    float[] special = {Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NaN};
	for(int i = 0; i < fValues.length;i++) {
    	assertTrue(PGPCompare.equals(fValues[i], fValues[i]) );
    	assertTrue(!PGPCompare.equals(fValues[i], fValues[(i+1)%fValues.length]) );
    }
    assertTrue("SPECIAL FLOATS: NEGATIVE INFINITY", PGPCompare.equals(special[0], special[0]) );
    assertTrue("SPECIAL FLOATS: POSITIVE INFINITY", PGPCompare.equals(special[1], special[1]) );
    assertTrue("SPECIAL FLOATS: BOTH", !PGPCompare.equals(special[0], special[1]) );
    assertTrue("SPECIAL FLOATS: NaN", !PGPCompare.equals(special[2], special[2]) );
  }

  /**
   * Test method: boolean equals(double, double)
   */
  public void testEquals_5() {
    //Must test for the following parameters!
    double dValues [] = {-1.0, 0.0, 1.0, Double.MAX_VALUE, Double.MIN_VALUE};
	double[] special = {Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NaN};
	for(int i = 0; i < dValues.length;i++) {
    	assertTrue(PGPCompare.equals(dValues[i], dValues[i]) );
    	assertTrue(!PGPCompare.equals(dValues[i], dValues[(i+1)%dValues.length]) );
    }
    assertTrue("SPECIAL DOUBLE: NEGATIVE INFINITY", PGPCompare.equals(special[0], special[0]) );
    assertTrue("SPECIAL DOUBLE: POSITIVE INFINITY", PGPCompare.equals(special[1], special[1]) );
    assertTrue("SPECIAL DOUBLE: BOTH", !PGPCompare.equals(special[0], special[1]) );
    assertTrue("SPECIAL DOUBLE: NaN", !PGPCompare.equals(special[2], special[2]) );
  }

  /**
   * Test method: boolean equals(char, char)
   */
  public void testEquals_4() {
    //Must test for the following parameters!
    char cValues [] = { ' ', 'a', Character.MAX_VALUE, Character.MIN_VALUE };
	for(int i = 0; i < cValues.length;i++) {
    	assertTrue(PGPCompare.equals(cValues[i], cValues[i]) );
    	assertTrue(!PGPCompare.equals(cValues[i], cValues[(i+1)%cValues.length]) );
    }
  }

  /**
   * Test method: boolean equals(String, String)
   */
  public void testEquals_3() {
    //Must test for the following parameters!
    String str [] = {"\u0000", " "};
    String nul = null;
	for(int i = 0; i < str.length;i++) {
		try {
	    	assertTrue(PGPCompare.equals(str[i], str[i]) );
    		assertTrue(!PGPCompare.equals(str[i], null) );
    		assertTrue(!PGPCompare.equals(null, str[i]) );
    		assertTrue(!PGPCompare.equals(str[i], str[(i+1)%str.length]) );
    	}
    	catch(NullPointerException nex) {
    		fail("Shouldn't throw NullPointerException");
    	}
    }
    try {
    	assertTrue(PGPCompare.equals(nul, nul) );
    }
    catch(NullPointerException nex) {
    	fail("Shouldn't throw NullPointerException");
    }
  }

  /**
   * Test method: boolean equals(Date, Date)
   */
  public void testEquals_2() {
    //Must test for the following parameters!
    Date[] dates = {new Date(), new Date(0), new Date(928734982) };
    Date nul = null;
	for(int i = 0; i < dates.length;i++) {
		try {
    		assertTrue(PGPCompare.equals(dates[i], dates[i]) );
    		assertTrue(!PGPCompare.equals(dates[i], null) );
    		assertTrue(!PGPCompare.equals(null, dates[i]) );
    		assertTrue(!PGPCompare.equals(dates[i], dates[(i+1)%dates.length]) );
    	}
    	catch(NullPointerException nex) {
    		fail("Shouldn't throw a NullPointerException");
    	}
    }
    try {
    	assertTrue(PGPCompare.equals(nul, nul) );
    }
    catch(NullPointerException nex) {
    	fail("Shouldn't throw NullPointerException");
    }
  }

  /**
   * Test method: boolean equals(byte[], byte[])
   */
  public void testEquals_1() {
    //Must test for the following parameters!
    byte[] one = new byte[0];
    byte[] two = new byte[1];
    byte[] three = new byte[16];
    byte[] four  = new byte[12345];
    fillByteArray(three);
    fillByteArray(four);
	byte[][] bytes = {one, two, three, four };
	for(int i = 0; i < bytes.length;i++) {
		try {
    		assertTrue(PGPCompare.equals(bytes[i], bytes[i]) );
    		assertTrue(!PGPCompare.equals(bytes[i], null) );
    		assertTrue(!PGPCompare.equals(null, bytes[i]) );
    		assertTrue(!PGPCompare.equals(bytes[i], bytes[(i+1)%bytes.length]) );
    	}
    	catch(NullPointerException nex) {
    		fail("Shouldn't throw a NullPointerException");
    	}
    }
  }

  /**
   * Test method: boolean equals(Vector, Vector)
   */
  public void testEquals() {
    //Must test for the following parameters!
    Integer test  = new Integer(1);
    Integer test2 = new Integer(1);
    Integer test3 = new Integer(2);
    Vector one   = new Vector();
    Vector two   = new Vector(6);
    Vector three = new Vector();
    Vector four  = new Vector();
    Vector five  = new Vector();
    Vector six   = new Vector();
    three.add(0, test);
    three.add(1, test3);
    four.add(0, test);
    four.add(1, test3);
    five.add(0, test3);
    five.add(1, test);
    six.add(0, test3);
    six.add(1, test2);
    
    Vector[] vectors = {one, two, three, four, five, six };
    
    assertTrue(PGPCompare.equals(one, two) );
    assertTrue(PGPCompare.equals(three, four) );
    assertTrue(!PGPCompare.equals(four, five) );
    assertTrue(PGPCompare.equals(five, six) );

  }

  private void fillByteArray(byte[] bytes) {
  	Random rnd = new Random();
  	for(int i = 0; i < bytes.length;i++) {
  		bytes[i] = (byte)rnd.nextInt();
  	}
  }



  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPCompareTest.class) );
  }
}
