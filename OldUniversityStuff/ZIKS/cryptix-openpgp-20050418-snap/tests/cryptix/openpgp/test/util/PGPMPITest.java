/* $Id: PGPMPITest.java,v 1.2 2005/03/13 17:47:00 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.util;

import cryptix.openpgp.util.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.util.Random;
import java.math.BigInteger;

/**
 * PGPMPITest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.util.PGPMPI </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPMPI extends java.lang.Object <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:47:00 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.util.PGPMPI
 */

public class PGPMPITest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPMPI pgpmpi;
  private File testdir;
  
  private byte[] b0   = {0x00, 0x00};
  private byte[] b1   = {0x00, 0x01, 0x01};
  private byte[] b255 = {0x00, 0x08, 0xFF-256};
  private byte[] b511 = {0x00, 0x09, 0x01, 0xFF-256};
  
  private long[] longs = {0, 1, 255, 511};
  private byte[][] bytes = {b0, b1, b255, b511};
  
  private ByteArrayOutputStream baos;
  private ByteArrayInputStream bais;
  private BigInteger x, y;

 

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPMPITest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File("." + fs + "testdata");
    }
    else {
        testdir = new File(td);
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    //pgpmpi = new PGPMPI();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    pgpmpi = null;
  }

  /**
   * Test method: java.math.BigInteger decode(DataInput)
   * decode throws java.io.IOException
   */
  public void testDecode() throws IOException
  {
    for(int i = 0; i < longs.length;i++) {
    	bais = new ByteArrayInputStream(bytes[i]);
        x = PGPMPI.decode(new DataInputStream(bais));
        assertEquals(x, BigInteger.valueOf(longs[i]));
    }
  }

  /**
   * Test method: void encode(DataOutput, BigInteger)
   * encode throws java.io.IOException
   */
  public void testEncode() throws IOException
  {
    for(int i = 0; i < longs.length;i++) {
    	baos = new ByteArrayOutputStream();
        x = BigInteger.valueOf(longs[i]);
        PGPMPI.encode(new DataOutputStream(baos),x);
        assertTrue(PGPCompare.equals(baos.toByteArray(), bytes[i]) );
    }
  }
  
  /**
   * Test method: both encode/decode with random data
   */
  public void testRandom() throws IOException
  {
  
  	int[] ints = {50, 199, 512};
  	for(int i = 0; i < ints.length;i++) {
  		x = new BigInteger(ints[i],0,new Random());
    	baos = new ByteArrayOutputStream();
    	PGPMPI.encode(new DataOutputStream(baos),x);
 
    	bais = new ByteArrayInputStream(baos.toByteArray());
    	y = PGPMPI.decode(new DataInputStream(bais));
    
    	assertEquals(x, y);
    }
  }
        

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPMPITest.class) );
  }
}
