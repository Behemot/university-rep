/* $Id: PGPInputStreamAdapterTest.java,v 1.2 2005/03/13 17:46:41 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.io;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

import cryptix.openpgp.io.*;
import cryptix.openpgp.util.PGPCompare;

/**
 * PGPInputStreamAdapterTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.io.PGPInputStreamAdapter </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPInputStreamAdapter extends java.io.InputStream <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:41 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.io.PGPInputStreamAdapter
 */

public class PGPInputStreamAdapterTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPInputStreamAdapter pgpinputstreamadapter;
  private File testdir;
  
  private PGPInputStreamAdapter adapter;
  private byte[] input;
  private byte[] buffer;
  

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPInputStreamAdapterTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp()
  {
    input = pseudoPacket();

    ByteArrayInputStream bin = new ByteArrayInputStream(input);
    PGPPacketDataInputStream pin = new PGPPacketDataInputStream(bin, true, 0);
    adapter = new PGPInputStreamAdapter(pin);

    buffer = new byte[input.length - 1];  
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    adapter = null;
  }

  /**
   * Test method: int read()
   * read throws java.io.IOException
   */
  public void testRead_2() throws IOException
  {
    int i=0;
    int b;
    while((b = adapter.read()) >= 0) {
		buffer[i++] = (byte) b;
    }

    assertEquals("Wrong number of bytes returned", buffer.length, i);

    byte[] strippedInput = new byte[input.length - 1];
    System.arraycopy(input, 1, strippedInput, 0, strippedInput.length);

    assertTrue(PGPCompare.equals(buffer, strippedInput));
  }

  /**
   * Test method: int read(byte[])
   * read throws java.io.IOException
   */
  public void testRead_1() throws IOException
  {
    int len = adapter.read(buffer);
    assertEquals("Wrong number of bytes read", buffer.length, len);

    byte[] strippedInput = new byte[input.length - 1];
    System.arraycopy(input, 1, strippedInput, 0, strippedInput.length);

    assertTrue(PGPCompare.equals(buffer, strippedInput));

  }

  /**
   * Test method: int read(byte[], int, int)
   * read throws java.io.IOException
   */
  public void testRead() throws IOException
  {
    int len = adapter.read(buffer, 0, buffer.length);
    assertEquals("Wrong number of bytes read", buffer.length, len);

    byte[] strippedInput = new byte[input.length - 1];
    System.arraycopy(input, 1, strippedInput, 0, strippedInput.length);

    assertTrue(PGPCompare.equals(buffer, strippedInput));

  }

  /**
   * Test method: int available()
   * available throws java.io.IOException
   */
  public void testAvailable() throws IOException {
    assertEquals("It's working now, go implement a proper test", adapter.available(), 0);
  }

  /**
   * Test method: void close()
   * close throws java.io.IOException
   */
  public void testClose() {
	//Not implemented
  }

  /**
   * Test method: void mark(int)
   */
  public void testMark() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
	//not supported
  }

  /**
   * Test method: void reset()
   * reset throws java.io.IOException
   */
  public void testReset() {
	//throws IOException
  }

  /**
   * Test method: boolean markSupported()
   */
  public void testMarkSupported() {
	assertEquals("Marking is now supported, go implement some tests",
		adapter.markSupported(), false);
  }
  
  /*
   * Construct a 200 byte pseudo packet.  Use a single byte to store the
   * packet length.
   */
  private byte[] pseudoPacket() {
    byte[] arr = new byte[200];
    arr[0] = (byte)0xC7; // 199
    for(int i=1; i < arr.length; ++i) {
      arr[i] = 0x0a;
    }
    return(arr);
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPInputStreamAdapterTest.class) );
  }
}
