/* $Id: TestSign.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test;


import cryptix.openpgp.algorithm.PGPDSA;
import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPWrapperDataInputStream;
import cryptix.openpgp.io.PGPWrapperDataOutputStream;

import cryptix.openpgp.util.PGPMPI;

import cryptix.test.Testlet;

import java.io.*;

import java.math.BigInteger;

import java.security.SecureRandom;
import java.security.MessageDigest;


/**
 * Tests signing
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public class TestSign extends Testlet {
    
    public TestSign() {
        super("Sign");
    }
    
    public void test() throws Exception {

        String path = getTestdatadir();
    
        beginTest("Read seckey.dat");
            PGPDSA dsa = new PGPDSA();
            FileInputStream fis = new FileInputStream(path+"seckey.dat");
            fis.read(); // packet type = secret key
            fis.read(); // length 1
            fis.read(); // length 2
            fis.read(); // version = 4
            fis.read(); // creation time 1
            fis.read(); // creation time 2
            fis.read(); // creation time 3
            fis.read(); // creation time 4
            fis.read(); // public key algorithm type = DSA
            dsa.decodePublicData(new PGPWrapperDataInputStream(fis));
            fis.read(); // not encrypted
            dsa.decodeSecretData(new PGPWrapperDataInputStream(fis));
        passIf(true);

        beginTest("Read and hash testdata");
            fis = new FileInputStream(path+"test");
            MessageDigest md = (new PGPAlgorithmFactory()).getHashAlgorithm(2);
            while (fis.available() > 0) {
                md.update((byte)fis.read());
            }
            fis = new FileInputStream(path+"test.pgpsig");
            fis.read(); // packet type = signature
            fis.read(); // length 1
            fis.read(); // length 2
            fis.read(); // version = 4
            fis.read(); // length of hashed data = 5
            md.update((byte)fis.read()); // signature type
            md.update((byte)fis.read()); // date & time 1
            md.update((byte)fis.read()); // date & time 2
            md.update((byte)fis.read()); // date & time 3
            md.update((byte)fis.read()); // date & time 4
            fis.read(); fis.read(); fis.read(); fis.read(); // key ID 1-4
            fis.read(); fis.read(); fis.read(); fis.read(); // key ID 5-8
            fis.read(); // signing algorithm = DSA
            fis.read(); // hash algorithm = SHA-1
            byte[] check = new byte[2];
            fis.read(check);
            debug("check: "+toString(check));
            debug("hash:  "+toString(md.digest()));
            DataInputStream dis = new DataInputStream(fis);
            BigInteger r = PGPMPI.decode(dis);
            BigInteger s = PGPMPI.decode(dis);
            debug("r: "+r.toString(16));
            debug("s: "+s.toString(16));
        passIf(true);

        beginTest("Verify PGP");
            dsa.initVerify(2, PGPAlgorithmFactory.getDefaultInstance());
            fis = new FileInputStream(path+"test");
            while (fis.available() > 0) {
                dsa.update((byte)fis.read());
            }
            fis = new FileInputStream(path+"test.pgpsig");
            fis.read(); // packet type = signature
            fis.read(); // length 1
            fis.read(); // length 2
            fis.read(); // version = 4
            fis.read(); // length of hashed data = 5
            dsa.update((byte)fis.read()); // signature type
            dsa.update((byte)fis.read()); // date & time 1
            dsa.update((byte)fis.read()); // date & time 2
            dsa.update((byte)fis.read()); // date & time 3
            dsa.update((byte)fis.read()); // date & time 4
            fis.read(); fis.read(); fis.read(); fis.read(); // key ID 1-4
            fis.read(); fis.read(); fis.read(); fis.read(); // key ID 5-8
            fis.read(); // signing algorithm = DSA
            fis.read(); // hash algorithm = SHA-1
            fis.read(); // check 1
            fis.read(); // check 2
            dsa.decodeSignatureData(new PGPWrapperDataInputStream(fis));
        passIf(dsa.verifySignature());
            
        beginTest("Sign and write");
            dsa.initSign(2, PGPAlgorithmFactory.getDefaultInstance());
            fis = new FileInputStream(path+"test");
            while (fis.available() > 0) {
                dsa.update((byte)fis.read());
            }
            fis = new FileInputStream(path+"test.pgpsig");
            fis.read(); // packet type = signature
            fis.read(); // length 1
            fis.read(); // length 2
            fis.read(); // version = 4
            fis.read(); // length of hashed data = 5
            dsa.update((byte)fis.read()); // signature type
            dsa.update((byte)fis.read()); // date & time 1
            dsa.update((byte)fis.read()); // date & time 2
            dsa.update((byte)fis.read()); // date & time 3
            dsa.update((byte)fis.read()); // date & time 4

            dsa.computeSignature();

            fis = new FileInputStream(path+"test.pgpsig");
            FileOutputStream fos = new FileOutputStream(path+"test.sig");
            fos.write(fis.read()); // packet type = signature
            fos.write(fis.read()); // length 1
            fos.write(fis.read()); // length 2
            fos.write(fis.read()); // version = 4
            fos.write(fis.read()); // length of hashed data = 5
            fos.write(fis.read()); // signature type
            fos.write(fis.read()); // date & time 1
            fos.write(fis.read()); // date & time 2
            fos.write(fis.read()); // date & time 3
            fos.write(fis.read()); // date & time 4
            fos.write(fis.read()); // key ID 1
            fos.write(fis.read()); // key ID 2
            fos.write(fis.read()); // key ID 3
            fos.write(fis.read()); // key ID 4
            fos.write(fis.read()); // key ID 5
            fos.write(fis.read()); // key ID 6
            fos.write(fis.read()); // key ID 7
            fos.write(fis.read()); // key ID 8
            fos.write(fis.read()); // signing algorithm = DSA
            fos.write(fis.read()); // hash algorithm = SHA-1
            fos.write(fis.read()); // check 1
            fos.write(fis.read()); // check 2
            
            dsa.encodeSignatureData(new PGPWrapperDataOutputStream(fos));
            
        passIf(true);

        beginTest("Verify own");
            dsa.initVerify(2, PGPAlgorithmFactory.getDefaultInstance());
            fis = new FileInputStream(path+"test");
            while (fis.available() > 0) {
                dsa.update((byte)fis.read());
            }
            fis = new FileInputStream(path+"test.pgpsig");
            fis.read(); // packet type = signature
            fis.read(); // length 1
            fis.read(); // length 2
            fis.read(); // version = 4
            fis.read(); // length of hashed data = 5
            dsa.update((byte)fis.read()); // signature type
            dsa.update((byte)fis.read()); // date & time 1
            dsa.update((byte)fis.read()); // date & time 2
            dsa.update((byte)fis.read()); // date & time 3
            dsa.update((byte)fis.read()); // date & time 4
        passIf(dsa.verifySignature());
    }

}
