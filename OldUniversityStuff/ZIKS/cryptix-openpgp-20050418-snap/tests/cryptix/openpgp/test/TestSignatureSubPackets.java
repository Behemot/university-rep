/* $Id: TestSignatureSubPackets.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test;


import cryptix.test.Testlet;
import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.signature.*;
import cryptix.openpgp.io.*;

import java.io.*;
import java.util.Date;


/**
 * Tests all the classes in cryptix.openpgp.signature
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public class TestSignatureSubPackets extends Testlet {
    
    static final String path = "testdata"+File.separator;
    
    public TestSignatureSubPackets() {
        super("SignatureSubPackets");
    }
    

    private PGPSignatureSubPacketFactory factory;

    
    public void writeTest(PGPSignatureSubPacket ssp, byte[] result) 
                                                            throws Exception {
        
        String name = ssp.getClass().getName().substring(29);
        name = name.substring(0,name.length()-2);
        beginTest("Write  "+name);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        ssp.encode(baos);

        String expected = toString(result);
        String reality  = toString(baos.toByteArray());
        
        debug("Expected: "+expected);
        debug("Written:  "+reality);
        
        passIf(expected.equals(reality));
        
    }
    
    public void readTest(PGPSignatureSubPacket ssp, byte[] result) 
                                                            throws Exception {
        
        String name = ssp.getClass().getName().substring(29);
        name = name.substring(0,name.length()-2);
        beginTest("Read   "+name);
        
        ByteArrayInputStream bais = new ByteArrayInputStream(result);
        
        PGPSignatureSubPacket newssp = factory.readPacket(bais);
        
        debug ("Expected PacketID: "+Byte.toString(ssp.getPacketID()));
        debug ("Read     PacketID: "+Byte.toString(newssp.getPacketID()));

        passIf(ssp.equals(newssp));
        
        beginTest("Hash== "+name);
        passIf(ssp.hashCode() == newssp.hashCode());

    }
    
    public void equalsTest(PGPSignatureSubPacket ssp, byte[] result) 
                                                            throws Exception {
        
        String name = ssp.getClass().getName().substring(29);
        name = name.substring(0,name.length()-2);
        beginTest("Equals "+name);
        
        ByteArrayInputStream bais = new ByteArrayInputStream(result);
        
        PGPSignatureSubPacket newssp = factory.readPacket(bais);
        newssp.setPacketID((byte)(newssp.getPacketID()+1));
        
        debug ("PacketID A: "+Byte.toString(ssp.getPacketID()));
        debug ("PacketID B: "+Byte.toString(newssp.getPacketID()));

        passIf(!ssp.equals(newssp));
        
        beginTest("Hash!= "+name);
        passIf(ssp.hashCode() != newssp.hashCode());

    }
    
    public void boundsCheck(byte[] data, boolean shouldthrow, String name) 
                                                            throws Exception {

        beginTest("Bound"+name);
        
        boolean correct;
        
        byte[] data2 = new byte[data.length + 3];
        System.arraycopy(data,0,data2,0,data.length);

        data2[data2.length-3] = 2;
        data2[data2.length-2] = 4;
        data2[data2.length-1] = 0;
        
        ByteArrayInputStream bais = new ByteArrayInputStream(data2);
        
        try {
            PGPSignatureSubPacket newssp = factory.readPacket(bais);
            if (shouldthrow) { correct = false; } else { correct = true; }
        } catch (PGPDataFormatException pdfe) {
            if (shouldthrow) { correct = true; } else { correct = false; }
        }

        passIf(correct);

        beginTest("Cont "+name);
        debug("avail: "+bais.available());
        passIf((factory.readPacket(bais)) instanceof PGPBooleanSP);
        
    }
    
    public void notinitCheck(boolean shouldthrow, PGPSignatureSubPacket ssp)
                                                            throws Exception {

        String name = ssp.getClass().getName().substring(29);
        name = name.substring(0,name.length()-2);
        beginTest("!PktID "+name);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        try {
            ssp.encode(baos);
            debug("No exception!");
            passIf(false);
        } catch (IllegalStateException ise) {
            passIf(true);
        }
        
    
        beginTest("NoInit "+name);
        ssp.setPacketID((byte)1);
        
        try {
            ssp.encode(baos);
            passIf(! shouldthrow);
        } catch (IllegalStateException ise) {
            passIf(shouldthrow);
        }
        
    }
    
                
    public void defaultCheck(byte[] result, PGPSignatureSubPacket ssp) 
                                                            throws Exception {
        
        String name = ssp.getClass().getName().substring(29);
        name = name.substring(0,name.length()-2);
        beginTest("Deflt  "+name);
        
        ssp.setPacketID((byte)1);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        ssp.encode(baos);

        String expected = toString(result);
        String reality  = toString(baos.toByteArray());
        
        debug("Expected: "+expected);
        debug("Written:  "+reality);
        
        passIf(expected.equals(reality));
        
    }
    
    
    public void pktsizeTest(String name, byte[] expect, int size) 
                                                            throws Exception {

        beginTest("Write "+name);
        
        byte[] data = new byte[size-1];
        for (int i=0; i<data.length; i++) data[i] = (byte)146;
        
        byte[] expect2 = new byte[expect.length+1];
        System.arraycopy(expect, 0, expect2, 0, expect.length);
        expect2[expect.length] = (byte)11;
        

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        PGPByteArraySP ssp = new PGPByteArraySP();
        ssp.setValue(data);
        ssp.setPacketID((byte)11);
        ssp.encode(baos);


        byte[] result = baos.toByteArray();
        byte[] result2 = new byte[expect2.length];
        
        System.arraycopy(result, 0, result2, 0, result2.length);
        
        debug("Expected: "+toString(expect2));
        debug("Written:  "+toString(result2));
        
        passIf(isEqual(result2, expect2));
        
        

        beginTest("Read  "+name);

        ByteArrayInputStream bais = new ByteArrayInputStream(result);
            
        PGPByteArraySP newssp = (PGPByteArraySP)factory.readPacket(bais);
        
        
        passIf(newssp.getValue().length == size-1);
        
    }
    
    public void test() throws Exception {


// Generic tests
// ...........................................................................

        beginTest("Create Factory");
            factory = new PGPSignatureSubPacketFactory (
                          PGPSignatureSubPacketFactory.INIT_DEFAULT );
        passIf(true);
        
        beginTest("Parse Big Blob");
            byte[] blob = { 5, 2, 0x11, 0x22, 0x33, 0x44,
                            5, 3, 0x11, 0x22, 0x33, 0x44,
                            2, 4, 1,
                            3, 5, 6, 8,
                            4, 6, 65, 66, 0,
                            2, 7, 0,
                            5, 9, (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF, 
                            4, 11, 1, 2, 3,
                            23, 12, (byte)0xC0, 17,
                                       1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                                      11, 12, 13, 14 ,15, 16, 17, 18, 19, 20,
                            9, 16, 1, 2, 3, 4, 5, 6, 7, 8,
                            11, 20, (byte)0x80, 0, 0, 0, 0, 1, 0, 1, 65, 66,
                            2, 21, 34,
                            6, 22, 1, 45, 35, 12, 14, 
                            2, 23, 0,
                            2, 24, 65,
                            2, 25, 1,
                            6, 26, 65, 66, 67, 68, 69,
                            2, 27, 0x15,
                            5, 28, 65, 66, 67, 68,
                            5, 29, 2, 65, 66, 67,
                            4, 30, 1, 2, 3 };

            ByteArrayInputStream bais = new ByteArrayInputStream(blob);
        
            boolean result = true;
            PGPSignatureSubPacket ssp;
            
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 2);
            result = result && (ssp instanceof PGPDateSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 3);
            result = result && (ssp instanceof PGPDateSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 4);
            result = result && (ssp instanceof PGPBooleanSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 5);
            result = result && (ssp instanceof PGPTrustSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 6);
            result = result && (ssp instanceof PGPNullTerminatedStringSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 7);
            result = result && (ssp instanceof PGPBooleanSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 9);
            result = result && (ssp instanceof PGPDateSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 11);
            result = result && (ssp instanceof PGPByteArraySP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 12);
            result = result && (ssp instanceof PGPRevocationKeySP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 16);
            result = result && (ssp instanceof PGPKeyIDSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 20);
            result = result && (ssp instanceof PGPNotationDataSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 21);
            result = result && (ssp instanceof PGPByteArraySP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 22);
            result = result && (ssp instanceof PGPByteArraySP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 23);
            result = result && (ssp instanceof PGPKeyServerPrefsSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 24);
            result = result && (ssp instanceof PGPStringSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 25);
            result = result && (ssp instanceof PGPBooleanSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 26);
            result = result && (ssp instanceof PGPStringSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 27);
            result = result && (ssp instanceof PGPKeyFlagsSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 28);
            result = result && (ssp instanceof PGPStringSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 29);
            result = result && (ssp instanceof PGPRevocationReasonSP);
            ssp = factory.readPacket(bais);
            debug (Byte.toString(ssp.getPacketID()) + ssp.toString());
            result = result && (ssp.getPacketID() == 30);
            result = result && (ssp instanceof PGPFeaturesSP);
            
        passIf(result);
                


// Packet size tests
// ...........................................................................

        byte[] s191 = {(byte)191};
        pktsizeTest("Small  packet - 191", s191, 191);
        byte[] s192 = {(byte)192, 0};
        pktsizeTest("Medium packet - 192", s192, 192);
        byte[] s193 = {(byte)192, 1};
        pktsizeTest("Medium packet - 193", s193, 193);
        byte[] s16319 = {(byte)254, (byte)255};
        pktsizeTest("Medium packet - 16319", s16319, 16319);
        byte[] s16320 = {(byte)255, 0, 0, 63, (byte)192};
        pktsizeTest("Large  packet - 16320", s16320, 16320);



// Individual packet tests
// ...........................................................................

        // PGPBooleanSP
        
        PGPBooleanSP bool = new PGPBooleanSP();
        bool.setPacketID((byte)4);
        bool.setValue(true);
        byte[] boolresult = {2,4,1};
        
        writeTest(bool, boolresult);
        readTest(bool, boolresult);
        equalsTest(bool, boolresult);
        

        // PGPDateSP
        
        PGPDateSP date = new PGPDateSP();
        date.setPacketID((byte)2);
        date.setValue(new Date(Date.UTC(150,7,17,15,03,51)));
        byte[] dateresult = {5,2,(byte)0x97,(byte)0xA7,(byte)0xDF,(byte)0xD7};
        
        writeTest(date, dateresult);
        readTest(date, dateresult);
        equalsTest(date, dateresult);
        
        
        // PGPFeaturesSP
        
        PGPFeaturesSP features = new PGPFeaturesSP();
        features.setPacketID((byte)30);
        features.setSupportsModificationDetection(true);
        features.setSupportsFeature((byte)2,true);
        features.setSupportsFeature((byte)3,false);
        features.setSupportsFeature((byte)4,true);
        features.setSupportsFeature((byte)4,true);
        features.setSupportsFeature((byte)5,false);
        features.setSupportsFeature((byte)5,false);
        features.setSupportsFeature((byte)6,true);
        features.setSupportsFeature((byte)6,false);
        features.setSupportsFeature((byte)7,false);
        features.setSupportsFeature((byte)7,true);
        features.setSupportsFeature((byte)8,true);
        features.setSupportsFeature((byte)8,false);
        features.setSupportsFeature((byte)8,true);
        features.setSupportsFeature((byte)9,false);
        features.setSupportsFeature((byte)9,true);
        features.setSupportsFeature((byte)9,false);
        byte[] featuresresult = {6, 30, 1, 2, 4, 7, 8};
        
        writeTest(features, featuresresult);
        readTest(features, featuresresult);
        equalsTest(features, featuresresult);


        // PGPKeyFlagsSP
        
        PGPKeyFlagsSP keyflags = new PGPKeyFlagsSP();
        keyflags.setPacketID((byte)27);
        keyflags.setCertify(true);
        keyflags.setEncryptCommunication(true);
        keyflags.setSplitKey(true);
        byte[] keyflagsresult = {2, 27, 0x15};
        
        writeTest(keyflags, keyflagsresult);
        readTest(keyflags, keyflagsresult);
        equalsTest(keyflags, keyflagsresult);
        
        
        // PGPKeyIDSP
        
        PGPKeyIDSP keyid = new PGPKeyIDSP();
        keyid.setPacketID((byte)16);
        byte[] keyidbytes = {1, 2, 3, 4, 5, 6, 7, 8};
        keyid.setValue(keyidbytes);
        byte[] keyidresult = {9, 16, 1, 2, 3, 4, 5, 6, 7, 8};
        
        writeTest(keyid, keyidresult);
        readTest(keyid, keyidresult);
        equalsTest(keyid, keyidresult);
        
        
        // PGPKeyServerPrefsSP
        
        PGPKeyServerPrefsSP keyserverprefs = new PGPKeyServerPrefsSP();
        keyserverprefs.setPacketID((byte)23);
        keyserverprefs.setNoModify(true);
        byte[] keyserverprefsresult = {2, 23, (byte)0x80};
        
        writeTest(keyserverprefs, keyserverprefsresult);
        readTest(keyserverprefs, keyserverprefsresult);
        equalsTest(keyserverprefs, keyserverprefsresult);
        
        
        // PGPNotationDataSP
        
        PGPNotationDataSP notationdata = new PGPNotationDataSP();
        notationdata.setPacketID((byte)20);
        notationdata.setHumanReadable(true);
        notationdata.setNameData("A");
        notationdata.setValueData("B");
        byte[] notationdataresult = {11, 20, (byte)0x80, 0, 0, 0, 
                                     0, 1, 0, 1, 65, 66};
        
        writeTest(notationdata, notationdataresult);
        readTest(notationdata, notationdataresult);
        equalsTest(notationdata, notationdataresult);
        
        
        // PGPByteArraySP
        
        PGPByteArraySP prefsarray = new PGPByteArraySP();
        prefsarray.setPacketID((byte)21);
        byte[] prefs = {1, 2, 3};
        prefsarray.setValue(prefs);
        byte[] prefsarrayresult = {4, 21, 1, 2, 3};
        
        writeTest(prefsarray, prefsarrayresult);
        readTest(prefsarray, prefsarrayresult);
        equalsTest(prefsarray, prefsarrayresult);
        
        
        // PGPNullTerminatedStringSP
        
        PGPNullTerminatedStringSP regularexpression = 
                                                new PGPNullTerminatedStringSP();
        regularexpression.setPacketID((byte)6);
        regularexpression.setValue("AB");
        byte[] regularexpressionresult = {4, 6, 65, 66, 0};
        
        writeTest(regularexpression, regularexpressionresult);
        readTest(regularexpression, regularexpressionresult);
        equalsTest(regularexpression, regularexpressionresult);
        
        
        // PGPRevocationKeySP
        
        PGPRevocationKeySP revocationkey = new PGPRevocationKeySP();
        revocationkey.setPacketID((byte)12);
        revocationkey.setSensitive(true);
        revocationkey.setAlgID((byte)17);
        byte[] key = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        revocationkey.setFingerprint(key);
        byte[] revocationkeyresult = {23, 12, (byte)0xC0, 17,
                                       1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                                      11, 12, 13, 14 ,15, 16, 17, 18, 19, 20};
        
        writeTest(revocationkey, revocationkeyresult);
        readTest(revocationkey, revocationkeyresult);
        equalsTest(revocationkey, revocationkeyresult);
        
        
        // PGPRevocationReasonSP
        
        PGPRevocationReasonSP revocationreason = new PGPRevocationReasonSP();
        revocationreason.setPacketID((byte)29);
        revocationreason.setCode(PGPRevocationReasonSP.RETIRED);
        revocationreason.setValue("ABC");
        byte[] revocationreasonresult = {5, 29, 3, 65, 66, 67};
        
        writeTest(revocationreason, revocationreasonresult);
        readTest(revocationreason, revocationreasonresult);
        equalsTest(revocationreason, revocationreasonresult);
        
        
        // PGPStringSP
        
        PGPStringSP string = new PGPStringSP();
        string.setPacketID((byte)28);
        string.setValue("ABC");
        byte[] stringresult = {4, 28, 65, 66, 67};
        
        writeTest(string, stringresult);
        readTest(string, stringresult);
        equalsTest(string, stringresult);
        
        
        // PGPTrustSP
        
        PGPTrustSP trust = new PGPTrustSP();
        trust.setPacketID((byte)5);
        trust.setDepth((byte)10);
        trust.setAmount(PGPTrustSP.FULL_TRUST);
        byte[] trustresult = {3, 5, 10, 120};
        
        writeTest(trust, trustresult);
        readTest(trust, trustresult);
        equalsTest(trust, trustresult);
        
        
        // PGPUnknownSP
        
        PGPUnknownSP unk = new PGPUnknownSP();
        unk.setPacketID((byte)123);
        byte[] unkpayload = {1, 2, 3};
        unk.setPayload(unkpayload);
        byte[] unkresult = {4, 123, 1, 2, 3};
        
        writeTest(unk, unkresult);
        readTest(unk, unkresult);
        equalsTest(unk, unkresult);
        
        

// Bounds checking tests
// ...........................................................................

        // PGPBooleanSP
        byte[] b1 = {2, 4, 2};
        boundsCheck(b1, true, "1 Boolean");
        byte[] b2 = {1, 4};
        boundsCheck(b2, true, "2 Boolean");
        byte[] b3 = {3, 4, 0, 0};
        boundsCheck(b3, true, "3 Boolean");
        
        // PGPByteArraySP
        byte[] a1 = {1, 11};
        boundsCheck(a1, true, "1 ByteArray");

        // PGPDateSP
        byte[] d1 = {1, 2};
        boundsCheck(d1, true, "1 Date");
        byte[] d2 = {4, 2, 1, 2, 3};
        boundsCheck(d2, true, "2 Date");
        byte[] d3 = {6, 2, 1, 2, 3, 4, 5};
        boundsCheck(d3, true, "3 Date");
        byte[] d4 = {5, 2, 0, 0, 0, 0};
        boundsCheck(d4, false, "4 Date");
        byte[] d5 = {5, 2, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF};
        boundsCheck(d5, false, "5 Date");

        // PGPFeaturesSP
        byte[] e1 = {1, 30};
        boundsCheck(e1, false, "1 Features");

        // PGPKeyFlagsSP
        byte[] f1 = {1, 27};
        boundsCheck(f1, true, "1 KeyFlags");
        byte[] f2 = {11, 27, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        boundsCheck(f2, false, "2 KeyFlags");

        // PGPKeyIDSP
        byte[] i1 = {1, 16};
        boundsCheck(i1, true, "1 KeyID");
        byte[] i2 = {8, 16, 1, 2, 3, 4, 5, 6, 7};
        boundsCheck(i2, true, "2 KeyID");
        byte[] i3 = {10, 16, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        boundsCheck(i3, true, "3 KeyID");

        // PGPKeyServerPrefsSP
        byte[] p1 = {1, 23};
        boundsCheck(p1, true, "1 KeyServerPrefs");
        byte[] p2 = {11, 23, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        boundsCheck(p2, false, "2 KeyServerPrefs");

        // PGPNotationDataSP
        byte[] n1 = {1, 20};
        boundsCheck(n1, true, "1 NotationData");
        byte[] n2 = {5, 20, 1, 2, 3, 4};
        boundsCheck(n2, true, "2 NotationData");
        byte[] n3 = {9, 20, 1, 2, 3, 4, 0, 0, 0, 0};
        boundsCheck(n3, true, "3 NotationData");
        byte[] n4 = {10, 20, 1, 2, 3, 4, 0, 1, 0, 0, 65};
        boundsCheck(n4, true, "4 NotationData");
        byte[] n5 = {10, 20, 1, 2, 3, 4, 0, 0, 0, 1, 65};
        boundsCheck(n5, true, "5 NotationData");
        byte[] n6 = {10, 20, 1, 2, 3, 4, 0, 1, 0, 1, 65};
        boundsCheck(n6, true, "6 NotationData");
        byte[] n7 = {12, 20, 1, 2, 3, 4, 0, 1, 0, 1, 65, 66, 67};
        boundsCheck(n7, true, "7 NotationData");
        byte[] n8 = {11, 20, 1, 2, 3, 4, 0, 1, 0, 1, 65, 66};
        boundsCheck(n8, false, "8 NotationData");

        // PGPNullTerminatedStringSP
        byte[] l1 = {1, 6};
        boundsCheck(l1, true, "1 NullTerminatedString");
        byte[] l2 = {2, 6, 0};
        boundsCheck(l2, true, "2 NullTerminatedString");
        byte[] l3 = {3, 6, 65, 66};
        boundsCheck(l3, true, "3 NullTerminatedString");
        byte[] l4 = {3, 6, 65, 0};
        boundsCheck(l4, false,"4 NullTerminatedString");
        byte[] l5 = {3, 6, 0, 0};
        boundsCheck(l5, true,"5 NullTerminatedString");
        
        // PGPRevocationKeySP
        byte[] k1 = {1, 12};
        boundsCheck(k1, true, "1 RevocationKey");
        byte[] k2 = {22, 12, (byte)0x80, 17,
                                     1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9};
        boundsCheck(k2, true, "2 RevocationKey");
        byte[] k3 = {24, 12, (byte)0x80, 17,
                                     1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1};
        boundsCheck(k3, true, "3 RevocationKey");
        byte[] k4 = {23, 12, (byte)0x40, 17,
                                     1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
        boundsCheck(k4, true, "4 RevocationKey");
        byte[] k5 = {23, 12, (byte)0xC0, 17,
                                     1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
        boundsCheck(k5, false,"5 RevocationKey");
        
        // PGPRevocationReasonSP
        byte[] r1 = {1, 29};
        boundsCheck(r1, true, "1 RevocationReason");
        byte[] r2 = {2, 29, 0};
        boundsCheck(r2, true, "2 RevocationReason");
        
        // PGPStringSP
        byte[] s1 = {1, 28};
        boundsCheck(s1, true, "1 String");
        byte[] s2 = {2, 28, 0};
        boundsCheck(s2, false,"2 String");
        
        // PGPTrustSP
        byte[] t1 = {1, 5};
        boundsCheck(t1, true, "1 Trust");
        byte[] t2 = {2, 5, 0};
        boundsCheck(t2, true, "2 Trust");
        byte[] t3 = {3, 5, 100, 100};
        boundsCheck(t3, false,"3 Trust");
        byte[] t4 = {4, 5, 0, 0, 0};
        boundsCheck(t4, true, "4 Trust");
        
        // PGPUnknownSP
        byte[] u1 = {1, 99};
        boundsCheck(u1, true, "1 Unknown");
        byte[] u2 = {2, 99, 0};
        boundsCheck(u2, false,"2 Unknown");
        


// Uninitialized tests
// ...........................................................................

        notinitCheck(false, new PGPBooleanSP());
        notinitCheck(true,  new PGPByteArraySP());
        notinitCheck(true,  new PGPDateSP());
        notinitCheck(false, new PGPFeaturesSP());
        notinitCheck(false, new PGPKeyFlagsSP());
        notinitCheck(true,  new PGPKeyIDSP());
        notinitCheck(false, new PGPKeyServerPrefsSP());
        notinitCheck(true,  new PGPNotationDataSP());
        notinitCheck(true,  new PGPNullTerminatedStringSP());
        notinitCheck(true,  new PGPRevocationKeySP());
        notinitCheck(true,  new PGPRevocationReasonSP());
        notinitCheck(true,  new PGPStringSP());
        notinitCheck(false, new PGPTrustSP());
        notinitCheck(true,  new PGPUnknownSP());



// Default values tests
// ...........................................................................

        byte[] da = {2, 1, 0};
        defaultCheck(da, new PGPBooleanSP());
        byte[] dg = {1, 1};
        defaultCheck(dg, new PGPFeaturesSP());
        byte[] db = {2, 1, 0};
        defaultCheck(db, new PGPKeyFlagsSP());
        byte[] dc = {2, 1, 0};
        defaultCheck(dc, new PGPKeyServerPrefsSP());
        byte[] dd = {3, 1, 0, 0};
        defaultCheck(dd, new PGPTrustSP());
        
        PGPRevocationKeySP se = new PGPRevocationKeySP();
        byte[] be = {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
        se.setFingerprint(be);
        byte[] de = {23,1,(byte)0x80,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
        defaultCheck(de, se);
        
        PGPRevocationReasonSP sf = new PGPRevocationReasonSP();
        sf.setValue("A");
        byte[] df = {3,1,0,65};
        defaultCheck(df, sf);


// ...........................................................................

        // Needed to split this method into two methods because jikes started
        // to generate invalid code.

        test2();

    }


    public void test2() throws Exception {


// Zero length and null argument tests
// ...........................................................................


        byte[] zerobytes = new byte[0];
        String zerostring = "";


        // PGPByteArraySP
        beginTest("Zero   ByteArray");
        try {
            (new PGPByteArraySP()).setValue(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   ByteArray");
        try {
            (new PGPByteArraySP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        

        // PGPDateSP
        beginTest("Null   Date");
        try {
            (new PGPDateSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        

        // PGPFeaturesSP
        beginTest("Zero   Features");
        try {
            (new PGPFeaturesSP()).setSupportsFeature((byte)0,true);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        

        // PGPKeyFlagsSP
        beginTest("Zero   KeyFlags");
        try {
            (new PGPKeyFlagsSP()).setValue(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   KeyFlags");
        try {
            (new PGPKeyFlagsSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        
        // PGPKeyIDSP
        beginTest("Zero   KeyID");
        try {
            (new PGPKeyIDSP()).setValue(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   KeyID");
        try {
            (new PGPKeyIDSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        
        // PGPKeyServerPrefsSP
        beginTest("Zero   KeyServerPrefs");
        try {
            (new PGPKeyServerPrefsSP()).setValue(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   KeyServerPrefs");
        try {
            (new PGPKeyServerPrefsSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        

        // PGPNotationDataSP
        beginTest("Zero1  NotationData");
        try {
            (new PGPNotationDataSP()).setFlags(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null1  NotationData");
        try {
            (new PGPNotationDataSP()).setFlags(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        
        beginTest("Zero2  NotationData");
        try {
            (new PGPNotationDataSP()).setNameData(zerostring);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null2  NotationData");
        try {
            (new PGPNotationDataSP()).setNameData(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        
        beginTest("Zero3  NotationData");
        try {
            (new PGPNotationDataSP()).setValueData(zerostring);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null3  NotationData");
        try {
            (new PGPNotationDataSP()).setValueData(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        

        // PGPNullTerminatedStringSP
        beginTest("Zero   NullTerminatedString");
        try {
            (new PGPNullTerminatedStringSP()).setValue(zerostring);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   NullTerminatedString");
        try {
            (new PGPNullTerminatedStringSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }

        
        // PGPRevocationKeySP
        beginTest("Zero   RevocationKey");
        try {
            (new PGPRevocationKeySP()).setFingerprint(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   RevocationKey");
        try {
            (new PGPRevocationKeySP()).setFingerprint(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }


        // PGPRevocationReasonSP
        beginTest("Zero   RevocationReason");
        try {
            (new PGPRevocationReasonSP()).setValue(zerostring);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   RevocationReason");
        try {
            (new PGPRevocationReasonSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }

        

        // PGPStringSP
        beginTest("Zero   String");
        try {
            (new PGPStringSP()).setValue(zerostring);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   String");
        try {
            (new PGPStringSP()).setValue(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }

        
        // PGPUnknownSP
        beginTest("Zero   Unknown");
        try {
            (new PGPUnknownSP()).setPayload(zerobytes);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }
        
        beginTest("Null   Unknown");
        try {
            (new PGPUnknownSP()).setPayload(null);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        } catch (NullPointerException iae) {
            passIf(true);
        }
        
        

// Min/max tests
// ...........................................................................


        Date max  = new Date(Date.UTC(206,  1,  7,  6, 28, 15));
                                   //2106 Feb  07  02: 28: 15
        Date max2 = new Date(Date.UTC(206,  1,  7,  6, 28, 16));
                                   //2106 Feb  07  02: 28: 16
        Date min  = new Date(Date.UTC( 69, 11, 31, 23, 59, 59));  
                                   //1969 Dec  31  23: 59: 59
        Date min2 = new Date(Date.UTC( 70,  0,  1,  0,  0,  0));  
                                   //1970 Jan   1   0:  0:  0
        
        PGPDateSP datesp = new PGPDateSP();
        
        beginTest("Max Date  "+max.toGMTString());
        
        try {
            datesp.setValue(max);
            passIf(true);
        } catch (IllegalArgumentException iae) {
            passIf(false);
        }


        beginTest("Max Date  "+max2.toGMTString());
        
        try {
            datesp.setValue(max2);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }


        beginTest("Min Date "+min.toGMTString());
        
        try {
            datesp.setValue(min);
            passIf(false);
        } catch (IllegalArgumentException iae) {
            passIf(true);
        }


        beginTest("Max Date  "+min2.toGMTString());
        
        try {
            datesp.setValue(min2);
            passIf(true);
        } catch (IllegalArgumentException iae) {
            passIf(false);
        }


    }

}
