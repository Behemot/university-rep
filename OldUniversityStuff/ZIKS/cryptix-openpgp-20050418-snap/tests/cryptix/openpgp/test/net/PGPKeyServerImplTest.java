/* $Id: PGPKeyServerImplTest.java,v 1.3 2005/03/13 17:46:42 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.openpgp.test.net;



import cryptix.message.KeyBundleMessage;
import cryptix.openpgp.net.*;
import cryptix.pki.KeyBundle;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import junit.framework.*;
import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.PGPPublicKey;




/**
 * Tests for the cryptix.openpgp.net.PGPKeyServerImpl class.
 *
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.3 $
 */
public class PGPKeyServerImplTest extends TestCase {


  //
  // A real key
  //
  private static final String KEY = 
    "-----BEGIN PGP PUBLIC KEY BLOCK-----\n" +
    "Version: PGPsdk 2.0.1 Copyright (C) 2000 Networks Associates Technology, Inc. All rights reserved.\n" +
    "\n" +
    "mQCNAzEIBQEAAAEEAOLegbUQO3tp609m2gTbsU5qPVXaJ4Y6NTFui1eOShOHKaTk\n" +
    "TvUqJpb3XnFeIKB9/czdvImd8zwecie/wClcZpfriPd4iwAY8FgM7njg3XgUmkHF\n" +
    "ZVy5fUgd1penmIbU+AX1Fif93xzmPInUyO6+1s7qnbbyAOnU6EH+EuC4Ega5AAUR\n" +
    "tChNYXRoaWFzIEtvbGVobWFpbmVuIDxyaXBwZXJAZGF0YXdheS5jb20+iQA/AwUQ\n" +
    "M/NsoxuEHtN2B90aEQKiLACg0V/JVMeXScu2KzLDQH9chSB6XY8AoLHb7kjIt03r\n" +
    "i6PWMOk2iMC1vYcBiQCVAwUQMevEsEH+EuC4Ega5AQH4bQP/d7eT6WsEbXfyepAE\n" +
    "+R0DrfBj116nsr2XfJtI2o2iMD9purlWutScAwEu9dmu4tIYdqh6xim4lol8czew\n" +
    "m88eFwfUV3QDCXKoldNYMgXm9z+WXaHqrfPREXJ4u0H/VMUzoAwKH86nCIY7Yv67\n" +
    "dinXZVPc34geVwGoxfoo2UcMLFW0KE1hdGhpYXMgS29sZWhtYWluZW4gPHJpcHBl\n" +
    "ckBiZXJrc3lzLmNvbT60L01hdGhpYXMgS29sZWhtYWluZW4gPHJpcHBlckByb29t\n" +
    "ZnVsbGFzcGllcy5jb20+iQA/AwUQNBiDjRuEHtN2B90aEQIMLQCeO4oXuRrWU+mh\n" +
    "o5/dX6NalSx4NysAoOTbn57aBeMqhTBKFPERWCCNznyD\n" +
    "=zGSX\n" +
    "-----END PGP PUBLIC KEY BLOCK-----\n";



  //
  // Our own implementation of the PGPKeyServerImpl that always
  // returns the same key.
  //
  private class KServer extends PGPKeyServerImpl {

    private ArrayList list;

    public KServer() throws NoSuchAlgorithmException {
      super("OpenPGP");
      list = new ArrayList();
      list.add(KEY);
    }

    public Iterator fetchRaw(String id) throws KeyServerIOException {
      return(list.iterator());
    }

  }// end private class "KServer"



  public PGPKeyServerImplTest(String name) {
    super(name);
  }


	public void setUp() {
    java.security.Security.addProvider
      (new cryptix.jce.provider.CryptixCrypto() );
    java.security.Security.addProvider
      (new cryptix.openpgp.provider.CryptixOpenPGP() );
  }


  /**
   * Test the fetchById() method.
   */
  public void testFetchById() {
    try {
      PGPKeyServer server = new KServer();
      Collection msgs = server.fetchById("blah");
      assertEquals("Wrong number of keys returned", 1, msgs.size());

      KeyBundleMessage msg = (KeyBundleMessage) msgs.iterator().next();
      KeyBundle key = msg.getKeyBundle();
      
      Iterator it = key.getPublicKeys();
      assertTrue("Missing the public key!", it.hasNext());
      
      PGPPublicKey pubkey = (PGPPublicKey) it.next();
      PGPKeyPacket keypacket = pubkey.getPacket();

      //
      // There must be a way to get the email address and other
      // data out of the public key, but I can't figure it out.
      //
      // -mathias
      //
    }
    catch(Exception e) {
      fail("ERR: " + e);
    }
  }


}//end class
