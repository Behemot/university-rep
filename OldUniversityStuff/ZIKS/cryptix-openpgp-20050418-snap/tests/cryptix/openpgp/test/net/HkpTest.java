/* $Id: HkpTest.java,v 1.2 2005/03/13 17:46:41 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.openpgp.test.net;



import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.ArrayList;
import junit.framework.*;
import cryptix.openpgp.net.*;



/** 
 * Tests for cryptix.openpgp.net.Hkp
 *
 *
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.2 $
 */
public class HkpTest extends TestCase {


	private static final String START_TAG = "-----BEGIN PGP PUBLIC KEY BLOCK-----";
	private static final String END_TAG = "-----END PGP PUBLIC KEY BLOCK-----";


	private TestHkpServer server;



	public HkpTest(String name) {
		super(name);
	}


	public void setUp() {
    java.security.Security.addProvider
      (new cryptix.jce.provider.CryptixCrypto() );
    java.security.Security.addProvider
      (new cryptix.openpgp.provider.CryptixOpenPGP() );

		server = new TestHkpServer();
		server.start();
	}
	

	public void tearDown() {
		server.stop();
	}


	/**
	 * Test the fetchRaw method.
	 */
	public void testFetchRaw() {
		try {
			Hkp fetcher = new Hkp("127.0.0.1", server.getPort());
			
			String KEY = START_TAG + "\n" + "some silly key" + "\n" + END_TAG;
			server.setKey("myid", KEY);

			//
			// the request that we expect Hkp to generate:
			// (flexible in HTTP version though --edwin)
			//
			String REQ0 = "GET /pks/lookup?op=get&search=myid HTTP/1.0";
			String REQ1 = "GET /pks/lookup?op=get&search=myid HTTP/1.1";

			Iterator list = fetcher.fetchRaw("myid");
			assertTrue("No key found!", list.hasNext());
			String key = ((String) list.next()).trim();

			assertTrue("Key mismatch", KEY.equals(key));
			
			String request = server.getRequest();

			assertTrue("Request mismatch", REQ0.equals(server.getRequest())
			                            || REQ1.equals(server.getRequest())); 

			//
			// Now lets do another one with multiple keys.
			//
			String[] KEYS = 
				{
					START_TAG + "\n" + "some silly key1" + "\n" + END_TAG,
					START_TAG + "\n" + "some silly key1" + "\n" + END_TAG,
					START_TAG + "\n" + "some silly key1" + "\n" + END_TAG
				};

			StringBuffer multiKey = new StringBuffer();
			for(int i=0; i < KEYS.length; ++i) {
				multiKey.append(KEYS[i]);
				if (i < (KEYS.length - 1)) {
					multiKey.append("\n\n");
				}
			}

			server.setKey("myid", multiKey.toString());
			list = fetcher.fetchRaw("myid");

			assertTrue("Request mismatch", REQ0.equals(server.getRequest())
			                            || REQ1.equals(server.getRequest())); 

			assertTrue("No key found!", list.hasNext());
			
			ArrayList result = new ArrayList();
			while(list.hasNext()) {
				result.add(((String) list.next()).trim());
			}
			assertEquals("Wrong number of keys returned", KEYS.length, result.size());
			
			for(int i=0; i < KEYS.length; ++i) {
				if (! result.remove(KEYS[i])) {
					fail("Missing key number " + i);
				}
			}
			assertEquals("Logic error!", 0, result.size());
		}
		catch(Exception e) {
			e.printStackTrace();
			fail("ERR: " + e);
		}
	}


}//end class "HkpTest"



//
// -------------------------------------------------------------
// -------------------------------------------------------------
// -------------------------------------------------------------
//                PROTECTED HELPER CLASS FOLLOWS
// -------------------------------------------------------------
// -------------------------------------------------------------
// -------------------------------------------------------------
//



/**
 * A server that pretends to be a keyserver.
 */
class TestHkpServer {


	/**
	 * The Daemon class is the single threaded server socket.
	 */
	private class Daemon implements Runnable {
		
		private boolean keepGoing = true;
		private String keyPacket;
		private int port;
		private String req;
		private boolean running = false;


		/*
		 * Capture the calling thread until daemon is ready.
		 */
		public synchronized void waitUntilReady() {
			if (!running) {
				try {
					this.wait();
				}
				catch(InterruptedException e) {
					;
				}
			}
		}

		/**
		 * Set the key data to return.  This is always returned to the client.
		 */
		public void setKeyPacket(String pkt) {
			keyPacket = pkt;
		}

		/**
		 * Get the port that we are listening on.
		 */
		public int getPort() {
			return(port);
		}

		/** 
		 * Get the last request (first line only)
		 */
		public String getRequest() {
			return(req);
		}		

		/** 
		 * Start the server, runs until someone calls stop()
		 */
		public void run() {
			ServerSocket sock = null;
			try {
				sock = new ServerSocket(0);
				port = sock.getLocalPort();
				sock.setSoTimeout(200);
				notifyReady();
				while(keepGoing) {
					try {
						Socket client = sock.accept();
						setRequest(handle(client));
						client.close();
					}
					catch(InterruptedIOException e) {
						// timeout
					}
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			finally {
				if (sock != null) {
					try {
						sock.close();
					}
					catch(Exception ignore) { }
				}
			}
		}

		/**
		 * Stops the server.
		 */
		public void stop() {
			keepGoing = false;
		}

		/**
		 * tell other threads that the server is ready.
		 */
		private synchronized void notifyReady() {
			this.notifyAll();
			running = true;
		}
		
		private void setRequest(String r) {
			req = r;
		}

		/*
		 * @return the first line of the request.
		 */
		private String handle(Socket s) throws IOException {

			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			OutputStream out = s.getOutputStream();

			ArrayList req = new ArrayList();
			String line;
			while((line = in.readLine()) != null) {
				if (line.trim().length() == 0) {
					break;
				}
				req.add(line);
			}
					 
			out.write("HTTP/1.1 200 OK\n".getBytes());
			out.write(("Content-Length: " + keyPacket.length() + "\n").getBytes());
			out.write("Content-Type: text/html\n\n".getBytes());
			out.write(keyPacket.getBytes());

			in.close();
			out.close();

			return((req.size() > 0) ? (String) req.get(0) : null);
		}

	}//end private class "Daemon"
	



	private Daemon daemon;
	private Thread thread = null;
	private int port;



	/** 
	 * @return the port that the server is running on
	 */
	public int getPort() {
		return(daemon.getPort());
	}

	/**
	 * @return the first line of the last request made to the server.
	 */
	public String getRequest() {
		return(daemon.getRequest());
	}

	/**
	 * Set the key data payload.
	 */
	public void setKey(String id, String key) {
		//
		// 'id' is ignored.
		//
		daemon.setKeyPacket(key);
	}
		

	/**
	 * Start the server.  Blocks until the server is
	 * ready to accept a connection.
	 */
	public void start() {		
		daemon = new Daemon();
		thread = new Thread(daemon);
		thread.setDaemon(true);
		thread.start(); // spawn!
		daemon.waitUntilReady();
	}

	
	/**
	 * Stop the server.
	 */
	public void stop() {
		if (thread != null) {
			daemon.stop();
			try {
				thread.join();
			}
			catch(InterruptedException e) {
			}
			thread = null;
		}
	}

}// end class "TestHkpServer"
