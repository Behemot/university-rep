/* $Id: PrincipalBuilderTest.java,v 1.2 2005/03/13 17:46:58 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.pki;

import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import cryptix.openpgp.provider.CryptixOpenPGP;

/**
 * PrincipalBuilderTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.pki.PrincipalBuilder </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PrincipalBuilder extends java.lang.Object <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:58 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.pki.PrincipalBuilder
 */

public class PrincipalBuilderTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PrincipalBuilder principalbuilder;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PrincipalBuilderTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File("." + fs + "testdata");
    }
    else {
        testdir = new File(td);
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    //principalbuilder = new PrincipalBuilder();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    principalbuilder = null;
  }

  /**
   * Test method: cryptix.pki.PrincipalBuilder getInstance(String)
   * getInstance throws java.security.NoSuchAlgorithmException
   */
  public void testGetInstance_1() throws Exception{
    //Must test for the following parameters!
    String str [] = {null, "\u0000", " "};
    for(int i = 0; i < str.length;i++) {
    	try {
    		PrincipalBuilder builder = PrincipalBuilder.getInstance(str[i]);
    		fail("Should have thrown an exception");
    	}
    	catch(NoSuchAlgorithmException nsaex) {
    		assertTrue(true);
    	}
    }
    PrincipalBuilder builder = PrincipalBuilder.getInstance("OpenPGP/UserID");
    assertTrue(builder instanceof PrincipalBuilder );

  }

  /**
   * Test method: cryptix.pki.PrincipalBuilder getInstance(String, String)
   * getInstance throws java.security.NoSuchAlgorithmException
   * getInstance throws java.security.NoSuchProviderException
   */
  public void testGetInstance() throws Exception {
    //Must test for the following parameters!
    String str [] = {null, "\u0000", " "};
    
    PrincipalBuilder builder = 
    	PrincipalBuilder.getInstance("OpenPGP/UserID","CryptixOpenPGP");
    
    assertTrue(builder instanceof PrincipalBuilder );
    
    PrincipalBuilder builder2 = 
    	PrincipalBuilder.getInstance("OpenPGP/UserID", new CryptixOpenPGP() );
           
    assertTrue(builder2 instanceof PrincipalBuilder );
    
    try {
    	PrincipalBuilder builder3 = 
    		PrincipalBuilder.getInstance("OpenPGP/UserID", "akjsdfkahsdkjaf");
    	fail("Should have thrown an exception");
    }
    catch(NoSuchProviderException nspex) {
    	assertTrue(true);
    }
    
    	

  }

  /**
   * Test method: java.security.Provider getProvider()
   */
  public void testGetProvider() {

  }

  /**
   * Test method: String getFormat()
   */
  public void testGetFormat() {

  }

  /**
   * Test method: java.security.Principal build(Object)
   * build throws cryptix.pki.PrincipalException
   */
  public void testBuild() {
    //Must test for the following parameters!
    Object oValues [] = { null };

  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PrincipalBuilderTest.class) );
  }
}
