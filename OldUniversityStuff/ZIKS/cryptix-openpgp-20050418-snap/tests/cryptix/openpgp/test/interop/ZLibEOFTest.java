/* $Id: ZLibEOFTest.java,v 1.2 2005/03/13 17:46:41 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.interop;


import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.util.*;


public class ZLibEOFTest extends TestCase
{
    private static final String FS = System.getProperty("file.separator");
    private final File _testdir;
    

    public ZLibEOFTest(String name) 
    {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            _testdir = new File("." + FS + "testdata");
        }
        else {
            _testdir = new File(td + FS + "testdata");
        }
    }
    

    protected void setUp() {
    }

    protected void tearDown() {
    }
  
    
    public void testZlibEOF() 
        throws Exception
    {
        FileInputStream in;
        Collection msgs;
        
        MessageFactory mf = MessageFactory.getInstance("OpenPGP");

        in = new FileInputStream(new File(_testdir, "bob-secret.pgp.asc"));
        msgs = mf.generateMessages(in);
        KeyBundleMessage kbm = (KeyBundleMessage)msgs.iterator().next();
        KeyBundle bundle = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "zlib_eof.asc"));
        msgs = mf.generateMessages(in);
        EncryptedMessage em = (EncryptedMessage)msgs.iterator().next();
        in.close();

        Message msg = em.decrypt(bundle, "TestingPassphrase".toCharArray());
        String data = ((LiteralMessage)msg).getTextData();
        assertEquals("Decrypted data: ", "Hello", data);
    }

}
