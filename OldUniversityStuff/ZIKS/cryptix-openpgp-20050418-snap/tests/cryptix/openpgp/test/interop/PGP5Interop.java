/* $Id: PGP5Interop.java,v 1.2 2005/03/13 17:46:41 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.interop;


import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.util.*;


public class PGP5Interop extends TestCase
{
    private static final String FS = System.getProperty("file.separator");
    private final File _testdir;
    

    public PGP5Interop(String name) 
    {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            _testdir = new File("." + FS + "testdata");
        }
        else {
            _testdir = new File(td + FS + "testdata");
        }
    }
    

    protected void setUp() {
    }

    protected void tearDown() {
    }
  
    
    public void testPGP5Interop() 
        throws Exception
    {
        FileInputStream in;
        Collection msgs;
        KeyBundleMessage kbm;
        EncryptedMessage em;
        LiteralMessage lm;
        SignedMessage sm;
        Iterator it;
        KeyBundle prvDHDSS, pubDHDSS, prvRSAleg, pubRSAleg;
        MessageFactory mf = MessageFactory.getInstance("OpenPGP");
        

        in = new FileInputStream(new File(_testdir, "PGP5_testkey_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        pubDHDSS = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "PGP5_testkey_RSA.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        pubRSAleg = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "PGP5_secring.skr"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        prvRSAleg = kbm.getKeyBundle();
        kbm = (KeyBundleMessage)it.next();
        prvDHDSS = kbm.getKeyBundle();
        in.close();

        in = new FileInputStream(new File(_testdir, "PGP5_enc_DHDSS_sig_RSA.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        em = (EncryptedMessage)msgs.iterator().next();
        sm = (SignedMessage)em.decrypt(prvDHDSS, "TestingPassphrase".toCharArray());
        assertTrue("Correctly signed", sm.verify(pubRSAleg));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Decrypted data", "PGP5 Test", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP5_enc_RSA_sig_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        em = (EncryptedMessage)msgs.iterator().next();
        sm = (SignedMessage)em.decrypt(prvRSAleg, "TestingPassphrase".toCharArray());
        assertTrue("Correctly signed", sm.verify(pubDHDSS));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Decrypted data", "PGP5 Test", lm.getTextData());


        in = new FileInputStream(new File(_testdir, "PGP5_cls_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly signed", sm.verify(pubDHDSS));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed data", "PGP5 Test", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP5_cls_RSA.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly signed", sm.verify(pubRSAleg));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed data", "PGP5 Test\r\n", lm.getTextData());

    }

}
