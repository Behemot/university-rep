/* $Id: PGP8Interop.java,v 1.2 2005/03/13 17:46:41 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.interop;


import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.util.*;


public class PGP8Interop extends TestCase
{
    private static final String FS = System.getProperty("file.separator");
    private final File _testdir;
    

    public PGP8Interop(String name) 
    {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            _testdir = new File("." + FS + "testdata");
        }
        else {
            _testdir = new File(td + FS + "testdata");
        }
    }
    

    protected void setUp() {
    }

    protected void tearDown() {
    }
  
    
    public void testPGP8Interop() 
        throws Exception
    {
        FileInputStream in;
        Collection msgs;
        KeyBundleMessage kbm;
        EncryptedMessage em;
        LiteralMessage lm;
        SignedMessage sm;
        Iterator it;
        KeyBundle prvDHDSS, pubDHDSS, prvRSAleg, pubRSAleg, prvRSAnew, pubRSAnew;
        MessageFactory mf = MessageFactory.getInstance("OpenPGP");
        

        in = new FileInputStream(new File(_testdir, "PGP8_testkey_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        prvDHDSS = kbm.getKeyBundle();
        kbm = (KeyBundleMessage)it.next();
        pubDHDSS = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "PGP8_testkey_RSA_legacy.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        prvRSAleg = kbm.getKeyBundle();
        kbm = (KeyBundleMessage)it.next();
        pubRSAleg = kbm.getKeyBundle();
        in.close();
        
        in = new FileInputStream(new File(_testdir, "PGP8_testkey_RSA_new.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        kbm = (KeyBundleMessage)it.next();
        prvRSAnew = kbm.getKeyBundle();
        kbm = (KeyBundleMessage)it.next();
        pubRSAnew = kbm.getKeyBundle();
        in.close();
        
        
        in = new FileInputStream(new File(_testdir, "PGP8_enc_DHDSS_sig_RSAleg.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        em = (EncryptedMessage)msgs.iterator().next();
        sm = (SignedMessage)em.decrypt(prvDHDSS, "TestingPassphrase".toCharArray());
        assertTrue("Correctly signed", sm.verify(pubRSAleg));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Decrypted data", "Hello", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP8_enc_RSAnew_sig_DHDSS.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        em = (EncryptedMessage)msgs.iterator().next();
        sm = (SignedMessage)em.decrypt(prvRSAnew, "TestingPassphrase".toCharArray());
        assertTrue("Correctly signed", sm.verify(pubDHDSS));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Decrypted data", "Hello", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP8_enc_RSAleg_sig_RSAnew.asc"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        em = (EncryptedMessage)msgs.iterator().next();
        sm = (SignedMessage)em.decrypt(prvRSAleg, "TestingPassphrase".toCharArray());
        assertTrue("Correctly signed", sm.verify(pubRSAnew));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Decrypted data", "Hello", lm.getTextData());
        

        in = new FileInputStream(new File(_testdir, "PGP8_sig_DHDSS.pgp"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly signed", sm.verify(pubDHDSS));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed data", "Hello", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP8_sig_RSAnew.pgp"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly signed", sm.verify(pubRSAnew));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed data", "Hello", lm.getTextData());

        in = new FileInputStream(new File(_testdir, "PGP8_sig_RSAleg.pgp"));
        msgs = mf.generateMessages(in);
        it = msgs.iterator();
        sm = (SignedMessage)msgs.iterator().next();
        assertTrue("Correctly signed", sm.verify(pubRSAleg));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("Signed data", "Hello", lm.getTextData());

    }

}
