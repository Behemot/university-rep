/* $Id: AsciiLastLineTest.java,v 1.2 2005/03/13 17:46:58 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;

import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import junit.framework.*;

public class AsciiLastLineTest
    extends TestCase
{
    public AsciiLastLineTest(String name) {
        super(name);
    }

    public void testAsciiLastLine()
        throws Exception
    {
        String data = "This is a test message\r\nThis is another line\r\n";
        
        MessageFactory mf = MessageFactory.getInstance("OpenPGP");
        InputStream in = new ByteArrayInputStream(data.getBytes("UTF-8"));
        LiteralMessage msg = (LiteralMessage)mf.generateMessages(in).iterator().next();

        assertEquals("Last line not removed", data, msg.getTextData());
    }
    
}