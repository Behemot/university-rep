/* $Id: ClearSignTest.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;

import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import java.io.ByteArrayInputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import junit.framework.*;

public class ClearSignTest
    extends TestCase
{
    public ClearSignTest(String name) {
        super(name);
    }

    public void testClearSign()
        throws Exception
    {
        byte[] encoded;
        ByteArrayInputStream in;
        KeyPairGenerator kpg;
        KeyPair kp;
        LiteralMessage lm;
        LiteralMessageBuilder lmb;
        Message msg;
        MessageFactory mf;
        SecureRandom sr = new SecureRandom();
        SignedMessage sm;
        SignedMessageBuilder smb;
        

        KeyBundleFactory kbf = KeyBundleFactory.getInstance("OpenPGP");
        PGPKeyBundle dsaPrvKey = (PGPKeyBundle)kbf.generateEmptyKeyBundle();
        PGPKeyBundle dsaPubKey = (PGPKeyBundle)kbf.generateEmptyKeyBundle();
        PGPKeyBundle rsaPrvKey = (PGPKeyBundle)kbf.generateEmptyKeyBundle();
        PGPKeyBundle rsaPubKey = (PGPKeyBundle)kbf.generateEmptyKeyBundle();

        mf = MessageFactory.getInstance("OpenPGP");


        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/DSA", 
                                           "CryptixOpenPGP");
        kpg.initialize(1024, sr);
        kp = kpg.generateKeyPair();
        
        dsaPubKey.addPublicKey(kp.getPublic());
        dsaPrvKey.addPublicKey(kp.getPublic());
        dsaPrvKey.addPrivateKey(kp.getPrivate(), kp.getPublic(), 
            "TestingPassphrase".toCharArray(), sr);
        
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/RSA", 
                                           "CryptixOpenPGP");
        kpg.initialize(1024, sr);
        kp = kpg.generateKeyPair();
        
        rsaPubKey.addPublicKey(kp.getPublic());
        rsaPrvKey.addPublicKey(kp.getPublic());
        rsaPrvKey.addPrivateKey(kp.getPrivate(), kp.getPublic(), 
            "TestingPassphrase".toCharArray(), sr);
        

        lmb = LiteralMessageBuilder.getInstance("OpenPGP");
        lmb.init("This is a text message \n"+
                 "This is another line\n");
        msg = lmb.build();

        smb = SignedMessageBuilder.getInstance("OpenPGP");
        smb.init(msg);
        smb.addSigner(dsaPrvKey, "TestingPassphrase".toCharArray());
        msg = smb.build();

        msg = new PGPArmouredMessage(msg);
        encoded = msg.getEncoded();
        
        in = new ByteArrayInputStream(encoded);
        msg = (Message)mf.generateMessages(in).iterator().next();

        sm = (SignedMessage)msg;
        assertTrue("DSA Correctly signed", sm.verify(dsaPubKey));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("DSA Signed data", "This is a text message\r\n"+
                     "This is another line\r\n", lm.getTextData());
        

        lmb = LiteralMessageBuilder.getInstance("OpenPGP");
        lmb.init("This is a text message \r\n"+
                 "This is another line\r\n");
        msg = lmb.build();

        smb = SignedMessageBuilder.getInstance("OpenPGP");
        smb.init(msg);
        smb.addSigner(rsaPrvKey, "TestingPassphrase".toCharArray());
        msg = smb.build();

        msg = new PGPArmouredMessage(msg);
        encoded = msg.getEncoded();
        
        in = new ByteArrayInputStream(encoded);
        msg = (Message)mf.generateMessages(in).iterator().next();

        sm = (SignedMessage)msg;
        assertTrue("RSA Correctly signed", sm.verify(rsaPubKey));
        lm = (LiteralMessage)sm.getContents();
        assertEquals("RSA Signed data", "This is a text message\r\n"+
                     "This is another line\r\n", lm.getTextData());
    }
    
}