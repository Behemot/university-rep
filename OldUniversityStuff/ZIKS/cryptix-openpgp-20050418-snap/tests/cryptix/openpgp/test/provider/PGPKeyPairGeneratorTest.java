/* $Id: PGPKeyPairGeneratorTest.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import cryptix.openpgp.provider.*;
import cryptix.openpgp.PGPKey;

/**
 * PGPKeyPairGeneratorTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.provider.PGPKeyPairGenerator </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPKeyPairGenerator extends java.security.KeyPairGeneratorSpi <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:59 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.provider.PGPKeyPairGenerator
 */

public class PGPKeyPairGeneratorTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  public static final int ENCODED_DISPLAY_LEN = 13;
  
  SecureRandom sr = new SecureRandom("Cryptix".getBytes() );  
  private KeyPairGenerator kpg;
  KeyPair kp;
  PublicKey pubk;
  PrivateKey prvk;
  byte[] pubb, prvb, pubx, prvx;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPKeyPairGeneratorTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/DSA",
                                       "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    kpg = null;
    kp = null;
    pubk = null;
    prvk = null;
    pubb = null;
    prvb = null;
    pubx = null;
    prvx = null;
  }

  /**
   * Test method: void initialize(int, SecureRandom)
   */
  public void testInitialize() {
    //Must test for the following parameters!
    int intValues [] = {-1, 0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for(int i = 0; i < intValues.length;i++) {
        try {
            kpg.initialize(intValues[i], sr);
            assertTrue(intValues[i] > 0);
        }
        catch(InvalidParameterException ipex) {
            assertTrue(intValues[i] <= 0);
        }
    }
  }
  
  /**
   * Test Default DSA
   */
  public void testDefaultDSA()
  {
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/DSA",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
                                    
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                          (prvk instanceof PGPKey) &&
                          (pubk.getAlgorithm().equals("OpenPGP/Signing/DSA")) &&
                          (prvk.getAlgorithm().equals("OpenPGP/Signing/DSA")) &&
                          (pubk.getFormat().equals("OpenPGP")) &&
                          (prvk.getFormat().equals("OpenPGP")) );
        
    assertEquals("pub bitlength", 1024, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 1024, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x99) && (prvb[0] == (byte)0x95) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x11) && (prvb[8] == (byte)0x11) &&
                           (pubb[9] == (byte)0x04) && (prvb[9] == (byte)0x04) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );
        
  }
  
  /**
   * Test 786 DSA sign
   */
  public void testDSA786() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/DSA",
                                       "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kpg.initialize(768, sr);
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                          (prvk instanceof PGPKey) &&
                          (pubk.getAlgorithm().equals("OpenPGP/Signing/DSA")) &&
                          (prvk.getAlgorithm().equals("OpenPGP/Signing/DSA")) &&
                          (pubk.getFormat().equals("OpenPGP")) &&
                          (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 768, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 768, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x99) && (prvb[0] == (byte)0x95) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x11) && (prvb[8] == (byte)0x11) &&
                           (pubb[9] == (byte)0x03) && (prvb[9] == (byte)0x03) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test default RSA sign
   */
  public void testRSASign() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/RSA",
                                               "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                          (prvk instanceof PGPKey) &&
                          (pubk.getAlgorithm().equals("OpenPGP/Signing/RSA")) &&
                          (prvk.getAlgorithm().equals("OpenPGP/Signing/RSA")) &&
                          (pubk.getFormat().equals("OpenPGP")) &&
                          (prvk.getFormat().equals("OpenPGP")) );
    
    assertEquals("pub bitlength", 2048, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 2048, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x99) && (prvb[0] == (byte)0x95) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x01) && (prvb[8] == (byte)0x01) &&
                           (pubb[9] == (byte)0x08) && (prvb[9] == (byte)0x08) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test 786 RSA sign
   */
  public void testRSA786Sign() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/RSA",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kpg.initialize(768, sr);
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                          (prvk instanceof PGPKey) &&
                          (pubk.getAlgorithm().equals("OpenPGP/Signing/RSA")) &&
                          (prvk.getAlgorithm().equals("OpenPGP/Signing/RSA")) &&
                          (pubk.getFormat().equals("OpenPGP")) &&
                          (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 768, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 768, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x98) && (prvb[0] == (byte)0x95) &&
                           (pubb[2] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[7] == (byte)0x01) && (prvb[8] == (byte)0x01) &&
                           (pubb[8] == (byte)0x03) && (prvb[9] == (byte)0x03) &&
                           (pubb[9] == (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test default ElGamal sign
   */
  public void testElGamalSign() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/ElGamal",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                      (prvk instanceof PGPKey) &&
                      (pubk.getAlgorithm().equals("OpenPGP/Signing/ElGamal")) &&
                      (prvk.getAlgorithm().equals("OpenPGP/Signing/ElGamal")) &&
                      (pubk.getFormat().equals("OpenPGP")) &&
                      (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 2048, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 2048, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x99) && (prvb[0] == (byte)0x95) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x14) && (prvb[8] == (byte)0x14) &&
                           (pubb[9] == (byte)0x08) && (prvb[9] == (byte)0x08) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test 786 ElGamal sign
   */
  public void testElGamalSign786() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Signing/ElGamal",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kpg.initialize(768, sr);
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                      (prvk instanceof PGPKey) &&
                      (pubk.getAlgorithm().equals("OpenPGP/Signing/ElGamal")) &&
                      (prvk.getAlgorithm().equals("OpenPGP/Signing/ElGamal")) &&
                      (pubk.getFormat().equals("OpenPGP")) &&
                      (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 768, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 768, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0x98) && (prvb[0] == (byte)0x95) &&
                           (pubb[2] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[7] == (byte)0x14) && (prvb[8] == (byte)0x14) &&
                           (pubb[8] == (byte)0x03) && (prvb[9] == (byte)0x03) &&
                           (pubb[9] == (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test default RSA crypt
   */
  public void testRSACrypt() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Encryption/RSA",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("object", (pubk instanceof PGPKey) &&
                       (prvk instanceof PGPKey) &&
                       (pubk.getAlgorithm().equals("OpenPGP/Encryption/RSA")) &&
                       (prvk.getAlgorithm().equals("OpenPGP/Encryption/RSA")) &&
                       (pubk.getFormat().equals("OpenPGP")) &&
                       (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 2048, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 2048, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0xB9) && (prvb[0] == (byte)0x9D) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x01) && (prvb[8] == (byte)0x01) &&
                           (pubb[9] == (byte)0x08) && (prvb[9] == (byte)0x08) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  /**
   * Test 786 RSA crypt
   */
  public void testRSA786Crypt() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Encryption/RSA",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kpg.initialize(768, sr);
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("object", (pubk instanceof PGPKey) &&
                       (prvk instanceof PGPKey) &&
                       (pubk.getAlgorithm().equals("OpenPGP/Encryption/RSA")) &&
                       (prvk.getAlgorithm().equals("OpenPGP/Encryption/RSA")) &&
                       (pubk.getFormat().equals("OpenPGP")) &&
                       (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 768, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 768, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0xB8) && (prvb[0] == (byte)0x9D) &&
                           (pubb[2] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[7] == (byte)0x01) && (prvb[8] == (byte)0x01) &&
                           (pubb[8] == (byte)0x03) && (prvb[9] == (byte)0x03) &&
                           (pubb[9] == (byte)0x00) && (prvb[10]== (byte)0x00) );
  }

  /**
   * Test default ElGamal crypt
   */
  public void testElGamalCrypt() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Encryption/ElGamal",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                   (prvk instanceof PGPKey) &&
                   (pubk.getAlgorithm().equals("OpenPGP/Encryption/ElGamal")) &&
                   (prvk.getAlgorithm().equals("OpenPGP/Encryption/ElGamal")) &&
                   (pubk.getFormat().equals("OpenPGP")) &&
                   (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 2048, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 2048, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0xB9) && (prvb[0] == (byte)0x9D) &&
                           (pubb[3] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[8] == (byte)0x10) && (prvb[8] == (byte)0x10) &&
                           (pubb[9] == (byte)0x08) && (prvb[9] == (byte)0x08) &&
                           (pubb[10]== (byte)0x00) && (prvb[10]== (byte)0x00) );

  }

  /**
   * Test default ElGamal crypt
   */
  public void testElGamal786Crypt() {
    
    try {
        kpg = KeyPairGenerator.getInstance("OpenPGP/Encryption/ElGamal",
                                           "CryptixOpenPGP");
    }
    catch(java.security.GeneralSecurityException gex) {
        fail("Exception was thrown: " + gex);
    }
    kpg.initialize(768, sr);
    kp = kpg.generateKeyPair();
    doStuff(kp);

    assertTrue("objects", (pubk instanceof PGPKey) &&
                   (prvk instanceof PGPKey) &&
                   (pubk.getAlgorithm().equals("OpenPGP/Encryption/ElGamal")) &&
                   (prvk.getAlgorithm().equals("OpenPGP/Encryption/ElGamal")) &&
                   (pubk.getFormat().equals("OpenPGP")) &&
                   (prvk.getFormat().equals("OpenPGP")) );

    assertEquals("pub bitlength", 768, ((PGPKey)pubk).getBitLength());
    assertEquals("sec bitlength", 768, ((PGPKey)prvk).getBitLength());

    assertTrue("encoding", (pubb[0] == (byte)0xB8) && (prvb[0] == (byte)0x9D) &&
                           (pubb[2] == (byte)0x04) && (prvb[3] == (byte)0x04) &&
                           (pubb[7] == (byte)0x10) && (prvb[8] == (byte)0x10) &&
                           (pubb[8] == (byte)0x03) && (prvb[9] == (byte)0x03) &&
                           (pubb[9] == (byte)0x00) && (prvb[10]== (byte)0x00) );
  }
  
  private void doStuff(KeyPair kp) {
    pubk = kp.getPublic();
    prvk = kp.getPrivate();

    pubb = pubk.getEncoded();
    prvb = prvk.getEncoded();
    
    pubx = new byte[ENCODED_DISPLAY_LEN];
    prvx = new byte[ENCODED_DISPLAY_LEN];
    System.arraycopy(pubb,0,pubx,0,pubx.length);
    System.arraycopy(prvb,0,prvx,0,prvx.length);
  }


  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    java.security.Security.addProvider(
      new cryptix.jce.provider.CryptixCrypto() );
    java.security.Security.addProvider(
      new cryptix.openpgp.provider.CryptixOpenPGP() );
    junit.textui.TestRunner.run( new TestSuite(PGPKeyPairGeneratorTest.class) );
  }
}
