/* $Id: PGPUserIDPrincipalBuilderTest.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.security.Principal;

import cryptix.pki.PrincipalException;
import cryptix.openpgp.provider.*;
import cryptix.openpgp.util.PGPCompare;

/**
 * PGPUserIDPrincipalBuilderTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.provider.PGPUserIDPrincipalBuilder </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     PGPUserIDPrincipalBuilder extends cryptix.pki.PrincipalBuilderSpi <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:59 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.provider.PGPUserIDPrincipalBuilder
 */

public class PGPUserIDPrincipalBuilderTest extends TestCase
{


  //Private variables
  private static final String fs = System.getProperty("file.separator");
  private PGPUserIDPrincipalBuilder builder;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public PGPUserIDPrincipalBuilderTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    builder = new PGPUserIDPrincipalBuilder();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    builder = null;
  }

  /**
   * Test method: java.security.Principal engineBuild(Object)
   * engineBuild throws cryptix.pki.PrincipalException
   */
  public void testEngineBuild() {
    //Must test for the following parameters!
    Object oValues [] = { null, new Integer(3), "Edwin Woudt <edwin@cryptix.org>" };
    Principal p;
    try {
    	p = builder.engineBuild(oValues[0]);
    	fail("Should have thrown an exception when trying to use null(1)");
    }
    catch(PrincipalException pex) {
	}
	catch(NullPointerException nex) {
		fail("Should have thrown a NullPointerException when trying to use null(1)");
	}
	
	try {
		String blah = null;
    	p = builder.engineBuild(blah);
    	fail("Should have thrown an exception when trying to use null(2)");
    }
    catch(PrincipalException pex) {
	}
	catch(NullPointerException nex) {
		fail("Should have thrown a NullPointerException when trying to use null(2)");
	}
    
    try {
    	p = builder.engineBuild(oValues[1]);
    	fail("Should have thrown an exception when trying to use an Integer");
    }
    catch(PrincipalException pex) {
	}

    try {
    	p = builder.engineBuild(oValues[2]);
    	assertEquals(oValues[2], p.getName() );
    	byte[] encoded = ((PGPUserIDPrincipal)p).getEncoded();
        byte[] shouldbe = {(byte)0xB4,0x1F,0x45,0x64,0x77,0x69,0x6E,0x20,
                            0x57,0x6F,0x75,0x64,0x74,0x20,0x3C,0x65,0x64,
                            0x77,0x69,0x6E,0x40,0x63,0x72,0x79,0x70,0x74,
                            0x69,0x78,0x2E,0x6F,0x72,0x67,0x3E};
        assertTrue(PGPCompare.equals(encoded,shouldbe));
    }
    catch(PrincipalException pex) {
    	fail("Should have worked");
    }
    
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(PGPUserIDPrincipalBuilderTest.class) );
  }
}
