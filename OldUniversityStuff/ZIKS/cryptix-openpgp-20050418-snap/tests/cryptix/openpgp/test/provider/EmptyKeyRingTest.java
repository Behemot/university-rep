/* $Id: EmptyKeyRingTest.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;


import cryptix.message.*;
import cryptix.openpgp.*;
import cryptix.pki.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.util.*;


public class EmptyKeyRingTest extends TestCase
{
    private static final String FS = System.getProperty("file.separator");
    private final File _testdir;
    

    public EmptyKeyRingTest(String name) 
    {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            _testdir = new File("." + FS + "testdata");
        }
        else {
            _testdir = new File(td + FS + "testdata");
        }
    }
    

    protected void setUp() {
    }

    protected void tearDown() {
    }
  
    
    public void testEmptyKeyRing() 
        throws Exception
    {
        FileInputStream in;
        ExtendedKeyStore kring;
        ExtendedCertStore cring;
        
        cring = (ExtendedCertStore)
            ExtendedCertStore.getInstance("OpenPGP/KeyRing",
            new PGPKeyRingCertStoreParameters(
            new File(_testdir, "empty.ring")));
        assertTrue("CertStore: no exception", true);

        in = new FileInputStream(new File(_testdir, "empty.ring"));
        kring = (ExtendedKeyStore)
            ExtendedKeyStore.getInstance("OpenPGP/KeyRing");
        kring.load(in, null);
        in.close();
        assertTrue("KeyStore: no exception", true);
    }

}
