/* $Id: CryptixOpenPGPTest.java,v 1.2 2005/03/13 17:46:59 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;

import cryptix.openpgp.util.PGPVersion;
import cryptix.openpgp.provider.*;

/**
 * CryptixOpenPGPTest    (Copyright 2001 Cryptix Foundation)
 * 
 * <p> This class performs unit tests on cryptix.openpgp.provider.CryptixOpenPGP </p>
 * 
 * <p> Explanation about the tested class and its responsibilities </p>
 * 
 * <p> Relations:
 *     CryptixOpenPGP extends java.security.Provider <br>
 * 
 * @author Erwin van der Koogh erwin@cryptix.org - Cryptix Foundation
 * @date $Date: 2005/03/13 17:46:59 $
 * @version $Revision: 1.2 $
 * 
 * @see cryptix.openpgp.provider.CryptixOpenPGP
 */

public class CryptixOpenPGPTest extends TestCase
{


  //Private variables
  
  private static final String NAME    = "CryptixOpenPGP";
  private static final String INFO    = "Cryptix OpenPGP Provider";
  private static final double VERSION = PGPVersion.VERSION;

  
  private static final String fs = System.getProperty("file.separator");
  private CryptixOpenPGP cryptixopenpgp;
  private File testdir;

  /**
   * Constructor (needed for JTest)
   * @param name    Name of Object
   */
  public CryptixOpenPGPTest(String name) {
    super(name);
    String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
    if(td == null || "".equals(td) ) {
        testdir = new File(".", "testdata");
    }
    else {
        testdir = new File(td, "testdata");
    }
  }

  /**
   * Used by JUnit (called before each test method)
   */
  protected void setUp() {
    cryptixopenpgp = new CryptixOpenPGP();
  }

  /**
   * Used by JUnit (called after each test method)
   */
  protected void tearDown() {
    cryptixopenpgp = null;
  }

  /**
   * Test Name, Info and Version
   */
  public void testName()
  {
  	assertEquals(cryptixopenpgp.getName(), NAME);
  	assertEquals(cryptixopenpgp.getInfo(), INFO);
  	assertEquals(cryptixopenpgp.getVersion(), VERSION, 0.0000001);
  }

  /**
   * Test the constructor: CryptixOpenPGP()
   */
  public void testCryptixOpenPGP() {
	assertTrue(cryptixopenpgp instanceof CryptixOpenPGP);
  }

  /**
   * Main method needed to make a self runnable class
   * 
   * @param args This is required for main method
   */
  public static void main(String[] args) {
    junit.textui.TestRunner.run( new TestSuite(CryptixOpenPGPTest.class) );
  }
}
