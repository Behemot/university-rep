/* $Id: PGPLiteralMessageOutputStreamTest.java,v 1.1 2005/03/29 11:22:51 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.test.provider;


import cryptix.message.stream.*;
import cryptix.openpgp.util.*;

import java.io.*;
import java.security.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Test class for PGPLiteralMessageOutputStream
 * 
 * @author Edwin Woudt
 * @version $Revision: 1.1 $
 * 
 * @see cryptix.openpgp.provider.PGPLiteralMessageOutputStream
 */

public class PGPLiteralMessageOutputStreamTest extends TestCase
{

    private static final String fs = System.getProperty("file.separator");
    private File testdir;

    public PGPLiteralMessageOutputStreamTest(String name) {
        super(name);
        String td = System.getProperty("CRYPTIX_OPENPGP_TESTDIR"); 
        if(td == null || "".equals(td) ) {
            testdir = new File(".", "testdata");
        } else {
            testdir = new File(td, "testdata");
        }
    }

    protected void setUp() {
    }

    protected void tearDown() {
    }

    private void assertArrayEqual(String msg, byte[] a, byte[] b)
    {
        assertTrue(msg+"\n"+PGPHex.toString(a)+"\n"+PGPHex.toString(b), 
                   PGPCompare.equals(a, b));
    }

    public void testOutputStream() throws Exception
    {
        LiteralMessageOutputStream lmos;
        ByteArrayOutputStream baos;
        SecureRandom sr = new SecureRandom();
        
        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        lmos.write(15);
        lmos.close();
        byte[] exp1 = { (byte)0xCB, (byte)0x07, (byte)0x62, (byte)0x00, 
                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
                        (byte)0x0F };
        assertArrayEqual("test1", exp1, baos.toByteArray());
        
        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        lmos.write(14);
        lmos.write(15);
        lmos.close();
        byte[] exp2 = { (byte)0xCB, (byte)0x08, (byte)0x62, (byte)0x00, 
                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
                        (byte)0x0E, (byte)0x0F };
        assertArrayEqual("test2", exp2, baos.toByteArray());
        
        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        byte[] inp3 = { (byte)5, (byte)6, (byte)7, (byte)8 };
        lmos.write(inp3);
        lmos.close();
        byte[] exp3 = { (byte)0xCB, (byte)0x0A, (byte)0x62, (byte)0x00, 
                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
                        (byte)0x05, (byte)0x06, (byte)0x07, (byte)0x08 };
        assertArrayEqual("test3", exp3, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        lmos.write(inp3, 0, 3);
        lmos.close();
        byte[] exp4 = { (byte)0xCB, (byte)0x09, (byte)0x62, (byte)0x00, 
                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
                        (byte)0x05, (byte)0x06, (byte)0x07 };
        assertArrayEqual("test4", exp4, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        lmos.write(inp3, 2, 2);
        lmos.close();
        byte[] exp5 = { (byte)0xCB, (byte)0x08, (byte)0x62, (byte)0x00, 
                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
                        (byte)0x07, (byte)0x08};
        assertArrayEqual("test5", exp5, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        byte[] inp6 = new byte[8186];
        for (int i=0; i<inp6.length; i++) inp6[i] = (byte)2;
        lmos.write(inp6);
        lmos.close();
        byte[] exp6 = new byte[8186+9];
        for (int i=0; i<exp6.length; i++) exp6[i] = (byte)2;
        exp6[0] = (byte)0xCB; 
        exp6[1] = (byte)0xDF; 
        exp6[2] = (byte)0x40; 
        exp6[3] = (byte)0x62; 
        exp6[4] = (byte)0x00; 
        exp6[5] = (byte)0x00; 
        exp6[6] = (byte)0x00; 
        exp6[7] = (byte)0x00; 
        exp6[8] = (byte)0x00; 
        assertArrayEqual("test6", exp6, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        byte[] inp7 = new byte[8187];
        for (int i=0; i<inp7.length; i++) inp7[i] = (byte)2;
        lmos.write(inp7);
        lmos.close();
        byte[] exp7 = new byte[8187+9];
        for (int i=0; i<exp7.length; i++) exp7[i] = (byte)2;
        exp7[   0] = (byte)0xCB; 
        exp7[   1] = (byte)0xED; 
        exp7[   2] = (byte)0x62; 
        exp7[   3] = (byte)0x00; 
        exp7[   4] = (byte)0x00; 
        exp7[   5] = (byte)0x00; 
        exp7[   6] = (byte)0x00; 
        exp7[   7] = (byte)0x00; 
        exp7[8194] = (byte)0x01; 
        assertArrayEqual("test7", exp7, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        lmos.write(inp7,0,8186);
        lmos.write(inp7,0,8186);
        lmos.write(inp7,0,15);
        lmos.close();
        byte[] exp8 = new byte[1+1+6+8191+1+8191+1+5];
        for (int i=0; i<exp8.length; i++) exp8[i] = (byte)2;
        exp8[    0] = (byte)0xCB; 
        exp8[    1] = (byte)0xED; 
        exp8[    2] = (byte)0x62; 
        exp8[    3] = (byte)0x00; 
        exp8[    4] = (byte)0x00; 
        exp8[    5] = (byte)0x00; 
        exp8[    6] = (byte)0x00; 
        exp8[    7] = (byte)0x00; 
        exp8[ 8194] = (byte)0xED;
        exp8[16387] = (byte)0x09;
        assertArrayEqual("test8", exp8, baos.toByteArray());

        lmos = LiteralMessageOutputStream.getInstance("OpenPGP");
        baos = new ByteArrayOutputStream();
        lmos.init(baos, sr);
        byte[] inp9 = new byte[32768+193];
        for (int i=0; i<inp9.length; i++) inp9[i] = (byte)2;
        lmos.write(inp9);
        lmos.close();
        byte[] exp9 = new byte[32768+193+7+6];
        for (int i=0; i<exp9.length; i++) exp9[i] = (byte)2;
        exp9[    0] = (byte)0xCB; 
        exp9[    1] = (byte)0xED; 
        exp9[    2] = (byte)0x62; 
        exp9[    3] = (byte)0x00; 
        exp9[    4] = (byte)0x00; 
        exp9[    5] = (byte)0x00; 
        exp9[    6] = (byte)0x00; 
        exp9[    7] = (byte)0x00; 
        exp9[ 8194] = (byte)0xED; 
        exp9[16387] = (byte)0xED; 
        exp9[24580] = (byte)0xED; 
        exp9[32773] = (byte)0xC0; 
        exp9[32774] = (byte)0x07; 
        assertArrayEqual("test9", exp9, baos.toByteArray());

    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run( 
            new TestSuite(PGPLiteralMessageOutputStreamTest.class) );
    }
}
