/* $Id: Test.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.test;

import java.io.File;
import java.io.IOException;



/**
 * Calls testlets
 *
 * @author  Edwin Woudt (edwin@cryptix.org)
 * @author  Mathias Kolehmainen (ripper@roomfullaspies.com)
 */
public class Test {

    private static String testdatadir;
    
    public static boolean test(String pack, String test, int level) {
        try {
            Class c = Class.forName("cryptix."+pack+".test.Test"+test);
            Testlet tl = (Testlet)c.newInstance();
            tl.setLevel(level);
            tl.setTestdatadir(testdatadir);
            return tl.run();
        } catch (Exception e) {
            System.out.println("Error initializing class"); e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] argv) {
        java.security.Security.addProvider(
            new cryptix.jce.provider.CryptixCrypto() );
        java.security.Security.addProvider(
            new cryptix.openpgp.provider.CryptixOpenPGP() );

        String pack = "";
        String test = null;
        String option = "";

        try {
            if (argv[0].startsWith("-")) {
                pack = argv[1];
                option = argv[0];
                if (argv.length > 2) test = argv[2];
            } else {
                pack = argv[0];
                if (argv.length > 1) test = argv[1];
            }
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            System.out.println("Use: ");
            System.out.println("  java cryptix.test.Test [option] package [test]");
            System.out.println();
            System.out.println("For example:");
            System.out.println("  java cryptix.test.Test openpgp");
            System.out.println("  java cryptix.test.Test -vvv openpgp ElGamal");
            System.exit(1);
        }
        
        int level = 0;
        if (option.equals("-v")) level = 1;
        if (option.equals("-vv")) level = 2;
        if (option.equals("-vvv")) level = 3;
        
        boolean result = true;
        if (test != null) {
            File f = new File("testdata" + File.separator);
            if (f.list() != null) {
              testdatadir="testdata"+File.separator;
            } else {
              testdatadir="build"+File.separator+"classes"+File.separator+"testdata"+File.separator;
            }            
            result = result && test(pack,test,level);
        } else {
          try {
            String base = System.getProperty("openpgp.test.base", ".");

            File f; String[] classes;
            
            f = new File(base + File.separator + "cryptix" + File.separator + pack + File.separator + "test");
            System.err.println("Looking for tests in " + f.getCanonicalPath());
            classes = f.list();
            if (classes != null) {
              testdatadir="testdata"+File.separator;
              for (int i=0; i<classes.length; i++) {
                if ((classes[i].startsWith("Test")) && (classes[i].endsWith(".class"))) {
                    test=classes[i].substring(4,classes[i].length()-6);
                    result = test(pack,test,level) && result;
                }
              }
            } else {
              testdatadir="build"+File.separator+"classes"+File.separator+"testdata"+File.separator;
              f = new File(base + File.separator + "build" + File.separator + "classes" + File.separator + "cryptix" + File.separator + pack + File.separator + "test");
              System.err.println("Looking for tests in " + f.getCanonicalPath());
              classes = f.list();
              for (int i=0; i<classes.length; i++) {
                if ((classes[i].startsWith("Test")) && (classes[i].endsWith(".class"))) {
                  test=classes[i].substring(4,classes[i].length()-6);
                  result = test(pack,test,level) && result;
                }
              }
            }

          }
          catch(IOException e) {
            e.printStackTrace();
          }
          if (! result) System.exit(1);
        }
    }

}
