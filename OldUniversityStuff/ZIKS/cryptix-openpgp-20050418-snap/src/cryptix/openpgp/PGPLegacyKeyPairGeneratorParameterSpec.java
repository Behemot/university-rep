/* $Id: PGPLegacyKeyPairGeneratorParameterSpec.java,v 1.1 2005/03/13 17:12:53 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import java.security.spec.AlgorithmParameterSpec;


/**
 * Parameters for a legacy KeyPairGenerator
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPLegacyKeyPairGeneratorParameterSpec 
    implements AlgorithmParameterSpec 
{

// Instance variables
//..............................................................................

    /** The keysize */
    private int keysize;
    /** The number of days after creation the key expires  */
    private int expiration;


// Constructor
//..............................................................................

    /**
     * Constructs a PGPLegacyKeyPairGeneratorParameterSpec object, given 
     * a keysize and an expiration date.
     *
     * @param keysize the keysize to use for generating a key, use 0 for the
     *        default value.
     * @param expiration the expiration date for the generated key, in days 
     *        after the creation of the key. Use 0 for keys that do not expire.
     */
    public PGPLegacyKeyPairGeneratorParameterSpec(int keysize, int expiration) 
    {
        this.keysize = keysize;
        this.expiration = expiration;
    }


// Getters
//..............................................................................

    /**
     * Returns the keysize
     */
    public int getKeySize() {
        return keysize;
    }
       
    /**
     * Returns the number of days after creation the key expires
     */
    public int getExpiration() {
        return expiration;
    }
       
}
