/* $Id: PGPTrustSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


 /**
 * Represents a signature subpacket the specifies the amount of trust by the
 * signer in the owner of this key.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>5 = trust signature</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPTrustSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPTrustSP () {}
    


// Constants
// ..........................................................................

    /** Standard value for no trust (=0) */
    public static final byte NO_TRUST      =   0;
    /** Standard value for partial trust (=60) */
    public static final byte PARTIAL_TRUST =  60;
    /** Standard value for full trust (=120) */
    public static final byte FULL_TRUST    = 120;
    
    /** Ordinary signature (=0)*/
    public static final byte NORMAL_SIGNATURE   = 0;
    /** Trusted introducer (=1)*/
    public static final byte TRUSTED_INTRODUCER = 1;
    /** Meta introducer (=2)*/
    public static final byte META_INTRODUCER    = 2;
    
        

// Instance variables
// ..........................................................................

    private byte depth  = NORMAL_SIGNATURE;
    private byte amount = NO_TRUST;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the level for the signature.
     *
     * <p>Most used levels are:</p><ul>
     * <li>NORMAL_SIGNATURE   = 0</li>
     * <li>TRUSTED_INTRODUCER = 1</li>
     * <li>META_INTRODUCER    = 2</li>
     * </ul>
     */
    public void setDepth(byte depth) { this.depth = depth; }


    /**
     * Get the level for the signature.
     *
     * <p>Most used levels are:</p><ul>
     * <li>NORMAL_SIGNATURE   = 0</li>
     * <li>TRUSTED_INTRODUCER = 1</li>
     * <li>META_INTRODUCER    = 2</li>
     * </ul>
     */
    public byte getDepth() { return depth; }


    /**
     * Set the amount of trust
     *
     * <p>The application should use one of these:</p><ul>
     * <li>NO_TRUST      =   0</li>
     * <li>PARTIAL_TRUST =  60</li>
     * <li>FULL_TRUST    = 120</li>
     * </ul>
     */
    public void setAmount(byte amount) { this.amount = amount; }


    /**
     * Get the amount of trust
     *
     * <p>The application should use one of these:</p><ul>
     * <li>NO_TRUST      =   0</li>
     * <li>PARTIAL_TRUST =  60</li>
     * <li>FULL_TRUST    = 120</li>
     * </ul>
     */
    public byte getAmount() { return amount; }
    


// PGPCompare method
// ..........................................................................

    /**
     * PGPCompare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPTrustSP)) {
            return false;
        } else {
            PGPTrustSP that = (PGPTrustSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getDepth(),    that.getDepth()) &&
                   PGPCompare.equals(this.getAmount(),   that.getAmount());
        }
        
    }
    

    
// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        setDepth(in.readByte());
        setAmount(in.readByte());
        
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        out.writeByte(getDepth());
        out.writeByte(getAmount());
        
    }


}
