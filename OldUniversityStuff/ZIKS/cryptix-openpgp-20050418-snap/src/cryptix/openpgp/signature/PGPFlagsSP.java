/* $Id: PGPFlagsSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * Generic superclass for signature subpackets that contain flags
 *
 * <p>Subclasses of this class should implement methods to control the
 * individual flags, using the setFlag and getFlag methods.</p>
 *
 * <p>Note that this class is very flexible in resizing on demand. If a flag is
 * set in the second byte, when only one byte is present, the second byte will 
 * be added and the flag set. The same for getting a flag: if the byte 
 * containing the flag is not available, it is added and filled with zero's.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public abstract class PGPFlagsSP extends PGPSignatureSubPacket {
    

// Instance variables
// ..........................................................................

    private byte[] value;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the byte array value of this packet.
     *
     * <p>Use setFlag to set individual flags</p>
     */
    public void setValue(byte[] value) { 
        if (value.length == 0) {
            throw new IllegalArgumentException("Array size == 0");
        }
        this.value = value; 
    }


    /**
     * Get the byte array value of this packet.
     *
     * <p>Returns null if nothing has been read or set.</p>
     * <p>Use getFlag instead of this method to get individual flags.</p>
     */
    public byte[] getValue() { return value; }


    /**
     * Get the byte array value of this packet with a particular size.
     *
     * <p>If the byte array contained in this packet larger, the rest will be
     * cut off. If it is smaller, bytes containing zero are added.</p>
     * <p>In general you should not use a setValue on the byte array returned
     * by this object, as extra bytes may have been present.</p>
     * <p>Returns a zero filled array if nothing has been read or set.</p>
     * <p>Use getFlag instead of this method to get individual flags.</p>
     *
     * @param size the size in bytes of the result
     */
    public byte[] getValue(int size) {
        byte[] result = new byte[size];
        for (int i=0; i<size; i++) result[i] = 0;
        if (value != null) {
            if (size <= value.length) {
                System.arraycopy(value,0,result,0,size);
            } else {
                System.arraycopy(value,0,result,0,value.length);
            }
        }
        return result;
    }
    
    
    /**
     * Get the flag at a given location and bitmask.
     *
     * @param location the byte number where the flag is located, the first byte
     *                 being 0.
     * @param flag the bitmask that represents the byte, e.g. 0x80 for the most
     *             significant bit.
     * @return true if the bit is set (=1), false if not (=0).
     */
    public boolean getFlag(int location, int flag) {
        return ((getValue(location+1)[location] & flag) != 0);
    }
    

    /**
     * Set the flag at a given location and bitmask.
     *
     * @param location the byte number where the flag is located, the first byte
     *                 being 0.
     * @param flag the bitmask that represents the byte, e.g. 0x80 for the most
     *             significant bit.
     * @param set the new value of the bit, true for 1, flas for 0
     */
    public void setFlag(int location, int flag, boolean set) {
        if (value == null) {
            value = new byte[location+1];
            for (int i=0; i<location+1; i++) value[i] = 0;
        }
        if (value.length < location) value = getValue(location+1);
        if (set) {
            value[location] = (byte)(value[location] | flag);
        } else {
            value[location] = (byte)(value[location] & (0xFF - flag));
        }
    }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPFlagsSP)) {
            return false;
        } else {
            PGPFlagsSP that = (PGPFlagsSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        byte[] value = in.readByteArray();
        
        if (value.length == 0) {
            throw new PGPDataFormatException("No data found.");
        }
        
        setValue(value);
                
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * <p>If no flags have been set (i.e. getValue() returns null), a single
     * zero byte is written.</p>
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if (getValue() != null) {
            out.writeFully(getValue());
        } else {
            out.writeByte((byte)0);
        }
        
    }


}
