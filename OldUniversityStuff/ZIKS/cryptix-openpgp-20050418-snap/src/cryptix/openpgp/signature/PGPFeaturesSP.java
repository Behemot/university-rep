/* $Id: PGPFeaturesSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * Represents a signature subpacket containing a description of what special
 * features an implementation supports.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPFeaturesSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPFeaturesSP () {
        data = new byte[0];
    }



// Constants
// ..........................................................................

    /** Identifier for the modification detection feature */
    public static final byte FEATURE_MODIFICATION_DETECTION = 1;



// Instance variables
// ..........................................................................

    private byte[] data;



// Getters/Setters
// ..........................................................................


    /**
     * Returns if modification detection is supported.
     */
    public boolean supportsModificationDetection() {
        return supportsFeature(FEATURE_MODIFICATION_DETECTION);
    }


    /**
     * Set modification detection support.
     */
    public void setSupportsModificationDetection(boolean support) {
        setSupportsFeature(FEATURE_MODIFICATION_DETECTION, support);
    }
    

    /**
     * Returns if a particular feature is supported.
     */
    public boolean supportsFeature(byte feature) {
        for (int i=0; i<data.length; i++) {
            if (data[i] == feature) return true;
        }
        return false;
    }
    

    /**
     * Sets support for a particular feature.
     */
    public void setSupportsFeature(byte feature, boolean support) {
        if (feature == 0) throw new IllegalArgumentException("feature == 0");
        if (supportsFeature(feature) != support) {
            if (support == true) {
                byte[] temp = new byte[data.length+1];
                System.arraycopy(data,0,temp,0,data.length);
                temp[data.length] = feature;
                data = temp;
            } else {
                for (int i=0; i<data.length; i++) {
                    if (data[i] == feature) {
                        byte[] temp = new byte[data.length-1];
                        System.arraycopy(data,0,temp,0,i);
                        System.arraycopy(data,i+1,temp,i,temp.length-i);
                        data = temp;
                        i = data.length+1;
                    }
                }
            }
        }
    }
    

// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPFeaturesSP)) {
            return false;
        } else {
            PGPFeaturesSP that = (PGPFeaturesSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.data, that.data);
        }
        
    }
    


// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        data = in.readByteArray();

    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        out.writeFully(data);
        
    }


}
