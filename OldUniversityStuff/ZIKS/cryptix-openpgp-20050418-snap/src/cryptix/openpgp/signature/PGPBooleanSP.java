/* $Id: PGPBooleanSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * Represents a signature subpacket containing a boolean value.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>4 = exportable certification</li>
 * <li>7 = revocable</li>
 * <li>25 = primary user id</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPBooleanSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPBooleanSP () {}

    

// Instance variables
// ..........................................................................

    private boolean value = false;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the boolean value of this packet.
     */
    public void setValue(boolean value) { this.value = value; }


    /**
     * Return the boolean value of this packet.
     */
    public boolean getValue() { return value; }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPBooleanSP)) {
            return false;
        } else {
            PGPBooleanSP that = (PGPBooleanSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        byte b = in.readByte();
        
        if (b == 0) {
            setValue(false);
        } else if (b == 1) {
            setValue(true);
        } else {
            throw new PGPDataFormatException("Invalid packet value");
        }        
        
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
            
        if (getValue()) {
            out.writeByte((byte)1);
        } else {
            out.writeByte((byte)0);
        }
        
    }


}
