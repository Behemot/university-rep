/* $Id: PGPStringSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


/**
 * Represents a signature subpacket containing a String.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>24 = preferred key server</li>
 * <li>26 = policy URL</li>
 * <li>28 = signer's user id</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPStringSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPStringSP () {}

    

// Instance variables
// ..........................................................................

    private String value;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the String value of this packet.
     */
    public void setValue(String value) { 
        if (value.length() == 0) {
            throw new IllegalArgumentException("String length == 0");
        }
        this.value = value; 
    }


    /**
     * Get the String value of this packet.
     */
    public String getValue() { return value; }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPStringSP)) {
            return false;
        } else {
            PGPStringSP that = (PGPStringSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        byte[] bytes = in.readByteArray();

        try {

            String value = new String(bytes,"UTF-8");

            if (value.length() == 0) {
                throw new PGPDataFormatException("No data found.");
            }

            setValue(value);
                
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported Encoding: UTF-8");
        }
        
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if (getValue() == null) {
            throw new IllegalStateException("Packet data not initialized");
        }
        
        try {

            byte[] bytes = getValue().getBytes("UTF-8");
            out.writeFully(bytes);
        
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported Encoding: UTF-8");
        }
        
    }

}
