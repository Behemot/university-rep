/* $Id: PGPSignatureSubPacket.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Generic superclass for all Signature Subpackets.
 *
 * <p>Every subclass must implement the decodeBody and encodeBody methods.
 * This class then handles the packaging into a real packet (Basically this
 * means that this class takes care of the type byte and length).
 * <br>Every subclass must also implement the equals method.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public abstract class PGPSignatureSubPacket {


// Instance variables
// ..........................................................................

    /** The packet id, 256 means it has not been set yet. */
    private int packetid = 256;
    /** Critical bit. If true then this an application must know how to parse
      * this packet to accept the signature. */
    private boolean critical = false;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the packet type of this packet.
     *
     * <p>See <a href="http://www.cryptix.org/docs/openpgp.html#x5231">
     * the OpenPGP specification for a list of the possible values.</p>
     */
    public void setPacketID (byte id) { packetid = id; }


    /**
     * Returns the packet type of this packet.
     *
     * <p>See <a href="http://www.cryptix.org/docs/openpgp.html#x5231">
     * the OpenPGP specification for a list of the possible values.</p>
     */
    public byte getPacketID () { return (byte)packetid; }
    
    
    /**
     * Set the critical bit for this packet
     */
    public void setCritical (boolean critical) { this.critical = critical; }


    /**
     * Get the critical bit for this packet
     */
    public boolean getCritical () { return critical; }



// Methods from java.lang.Object
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public abstract boolean equals(Object ssp);
    

    /**
     * Returns the hashcode for this object.
     *
     * <p>While this method is correct, it only uses the packetid and is thus
     * far from optimal. Therefore subclasses should override this method if
     * better Hashtable performance is needed.</p>
     */
    public int hashCode() {
        return getPacketID();
    }
    
    

// Decode methods
// ..........................................................................


    /**
     * Decode the packet.
     *
     * <p>Note that this method does not read the type byte, which is already 
     * done by the factory.</p>
     * <p>This method calls decodeBody.</p>
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    public void decode (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        
        decodeBody (in);
        
        in.close();

    }


    /**
     * Decode the packet body.
     *
     * <p>Implementations of this method must not try to read the type and
     * length information as that has already been read. The length is passed
     * as a parameter.</p>
     * <p>In case something goes wrong and a PGPDataFormatException is thrown,
     * the implementation must attempt to read the remaining bytes according
     * to the length parameter. This is just to make sure that this packet can
     * be ignored by the application and the next packet can be processed
     * without any problems. Implementations do not have to try to save the
     * corrupted data in order to enable a succesful write. </p>
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected abstract void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException;

    

// Encode methods
// ..........................................................................

    /**
     * Encode the packet.
     *
     * <p>Note that this method does write the type byte (unlike the read
     * method that does not read it).</p>
     * <p>This methos calls encodeBody.</p>
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public void encode (OutputStream out) throws IOException {

        if (packetid == 256) {
            throw new IllegalStateException("PacketID not set.");
        }

        PGPSignatureDataOutputStream sdos = 
                           new PGPSignatureDataOutputStream(out, getPacketID());
        
        encodeBody(sdos);
        
        sdos.close();

    }
  

    /**
     * Encode the packet body.
     *
     * <p>Implementations of this method must not try to write the type and
     * length information as this class will handle this in the write method.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected abstract void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException;


}
