/* $Id: PGPNotationDataSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


/**
 * Represents a signature subpacket containing a a "notation" on the signature
 * that the issuer wishes to make. 
 *
 * <p>The notation has a name and a value, each of which are strings. There is
 * also a flag that specifies that the information is human-readable.</p>
 *   
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>20 = notation data</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPNotationDataSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPNotationDataSP () {}

    

// Instance variables
// ..........................................................................

    private byte[] flags;
    private String namedata;
    private String valuedata;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the 4 byte flags.
     *
     * <p>Use the setHumanReadable method for a more easy interface to the only
     * currently existing flag.</p>
     */
    public void setFlags(byte[] flags) {
        if (flags.length != 4)
            throw new IllegalArgumentException("Flags should be 4 bytes");
        this.flags = flags; 
    }


    /**
     * Get the 4 byte flags.
     *
     * <p>Use the getHumanReadable method for a more easy interface to the only
     * currently existing flag.</p>
     * <p>If the flags have not been set or read yet it will create and return
     * a new array of 4 bytes containing zeroes.</p>
     */
    public byte[] getFlags() { 
        if (flags == null) {
            flags = new byte[4];
            flags[0] = flags[1] = flags[2] = flags[3] = 0;
        }
        return flags; 
    }
    

    /**
     * Set if this notation is human-readable and has no meaning to software.
     *
     * <p>This method can safely be called if the flags have not been set or 
     * read yet. In that case the flags will be created, filled up with zeroes.
     * </p>
     */
    public void setHumanReadable(boolean flag) {
        if (flags == null) {
            flags = new byte[4];
            flags[0] = flags[1] = flags[2] = flags[3] = 0;
        }
        if (flag) {
            flags[0] = (byte)(flags[0] | 0x80);
        } else {
            flags[0] = (byte)(flags[0] & (0xFF - 0x80));
        }
    }


    /**
     * Get if this notation is human-readable and has no meaning to software.
     *
     * <p>This method can safely be called if the flags have not been set or 
     * read yet. In that case the flags will be created, filled up with zeroes,
     * and this method will return false (unset). </p>
     */
    public boolean getHumanReadable() { 
        if (flags == null) {
            flags = new byte[4];
            flags[0] = flags[1] = flags[2] = flags[3] = 0;
        }
        return ((flags[0] & 0x80) != 0);
    }
    

    /**
     * Set the name of this notation
     */
    public void setNameData(String namedata) { 
        if (namedata.length() == 0) {
            throw new IllegalArgumentException("String length == 0");
        }
        this.namedata = namedata; 
    }


    /**
     * Get the name of this notation
     */
    public String getNameData() { return namedata; }
    

    /**
     * Set the value of this notation
     */
    public void setValueData(String valuedata) { 
        if (valuedata.length() == 0) {
            throw new IllegalArgumentException("String length == 0");
        }
        this.valuedata = valuedata; 
    }


    /**
     * Get the value of this notation
     */
    public String getValueData() { return valuedata; }



// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPNotationDataSP)) {
            return false;
        } else {
            PGPNotationDataSP that = (PGPNotationDataSP) ssp;
            return PGPCompare.equals(this.getPacketID(),  that.getPacketID()) &&
                   PGPCompare.equals(this.getFlags(),     that.getFlags()) &&
                   PGPCompare.equals(this.getNameData(),  that.getNameData()) &&
                   PGPCompare.equals(this.getValueData(), that.getValueData());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        byte[] flags = new byte[4];
        in.readFully(flags);
        setFlags(flags);

        int m = in.readUnsignedShort();
        int n = in.readUnsignedShort();
        
        if ((m == 0) || (n == 0)) {
            in.readByteArray();
            throw new PGPDataFormatException("String length == 0.");
        }
        
        
        byte[] namebytes = new byte[m];
        byte[] valuebytes = new byte[n];

        in.readFully(namebytes);
        in.readFully(valuebytes);

        try {
            setNameData(new String(namebytes,"UTF-8"));
            setValueData(new String(valuebytes,"UTF-8"));
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported encoding: UTF-8");
        }
                
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if ((namedata == null) || (valuedata == null) || (flags == null)) {
            throw new IllegalStateException("Packet data not initialized");
        }
        
        out.writeFully(getFlags());
        
        try {

            byte[] namebytes = namedata.getBytes("UTF-8");
            byte[] valuebytes = valuedata.getBytes("UTF-8");

            out.writeShort((short)namebytes.length);
            out.writeShort((short)valuebytes.length);
        
            out.writeFully(namebytes);
            out.writeFully(valuebytes);        
            
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported encoding: UTF-8");
        }
        
    }


}
