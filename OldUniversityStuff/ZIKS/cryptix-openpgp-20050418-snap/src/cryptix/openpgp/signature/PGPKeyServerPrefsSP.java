/* $Id: PGPKeyServerPrefsSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


/**
 * Represents a signature subpacket containing a list of binary flags that 
 * indicate preferences that the key holder has about how the key is handled 
 * on a key server.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>23 = key server preferences</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPKeyServerPrefsSP extends PGPFlagsSP {


// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPKeyServerPrefsSP () {}



// Getters/Setters
// ..........................................................................
       
       
    /**
     * Get if the key holder requests that this key only be modified or
     * updated by the key holder or an administrator of the key server.
     */
    public boolean getNoModify()  { return getFlag(0,0x80); } 


    /**
     * Set if the key holder requests that this key only be modified or
     * updated by the key holder or an administrator of the key server.
     */
    public void setNoModify(boolean set) { setFlag(0,0x80,set); }


}
