/* $Id: PGPRevocationKeySP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * Represents a signature subpacket containing a boolean value.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>12 = revocation key</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPRevocationKeySP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPRevocationKeySP () {}

    

// Instance variables
// ..........................................................................

    private byte[] fingerprint;
    private byte revocationclass = (byte)0x80;
    private byte algid = 0;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the fingerprint of the revocation key.
     */
    public void setFingerprint(byte[] fp) { 
        if (fp.length != 20) {
            throw new IllegalArgumentException("Fingerprint must be 20 bytes");
        }
        this.fingerprint = fp; 
    }
    

    /**
     * Get the fingerprint of the revocation key.
     */
    public byte[] getFingerprint() { return fingerprint; }


    /**
     * Set the algorithm ID of the revocation key.
     *
     * <p>This is one of the public key algorithm numbers as defined in 
     * cryptix.openpgp.algorithm.PGPAlgorithmFactory.</p>
     */
    public void setAlgID(byte algid) { this.algid = algid; }


    /**
     * Get the algorithm ID of the revocation key.
     *
     * <p>This is one of the public key algorithm numbers as defined in 
     * cryptix.openpgp.algorithm.PGPAlgorithmFactory.</p>
     */
    public byte getAlgID() { return algid; }
    
    
    /**
     * Set the class of the revocation key.
     *
     * <p>Use the setSensitive method for a more convenient method to access the
     * currently only available flag that can be set.</p>
     * <p>The most significant bit of this byte must be set, otherwise an
     * IllegalArgumentException will be thrown.</p>
     */
    public void setRevocationClass(byte cl) { 
        if ((cl & 0x80) == 0) {
            throw new IllegalArgumentException("Illegal revocation class");
        }
        this.revocationclass = cl; 
    }


    /**
     * Get the class of the revocation key.
     *
     * <p>Use the setSensitive method for a more convenient method to access the
     * currently only available flag that can be set.</p>
     */
    public byte getRevocationClass() { return revocationclass; }
    

    /**
     * Set the sensitivity of the packet.
     *
     * <p>If this flag is set, the keyholder feels this subpacket contains
     * private information that should no be exported to other users.</p>
     */
    public void setSensitive(boolean sensitive) { 
        if (sensitive) {
            revocationclass |= 0x40;
        } else {
            revocationclass &= (0xFF - 0x40);
        }
    }
    

    /**
     * Get the sensitivity of the packet.
     *
     * <p>If this flag is set, the keyholder feels this subpacket contains
     * private information that should no be exported to other users.</p>
     */
    public boolean getSensitive() {
        return ((revocationclass & 0x40) != 0);
    }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPRevocationKeySP)) {
            return false;
        } else {
            PGPRevocationKeySP that = (PGPRevocationKeySP) ssp;
            return PGPCompare.equals(this.getPacketID(), 
                                     that.getPacketID()) &&
                   PGPCompare.equals(this.getFingerprint(),
                                     that.getFingerprint()) &&
                   PGPCompare.equals(this.getRevocationClass(),
                                     that.getRevocationClass()) &&
                   PGPCompare.equals(this.getAlgID(),
                                     that.getAlgID());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        byte b = in.readByte();
        

        if ((b & 0x80) == 0) {  // Oops
            // Read all remaining data
            in.readByteArray();
            // Throw exception
            throw new PGPDataFormatException("Invalid class in revocation "+
                                             "packet");
        }


        setRevocationClass(b);

        setAlgID(in.readByte());
        
        byte[] fp = new byte[20];
        in.readFully(fp);
        setFingerprint(fp);
                
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if ((getFingerprint() == null) || ((getRevocationClass() & 0x80) == 0)){
            throw new IllegalStateException("Packet data not initialized");
        }
        
        out.writeByte(getRevocationClass());
        out.writeByte(getAlgID());
        out.writeFully(getFingerprint());
        
    }

}
