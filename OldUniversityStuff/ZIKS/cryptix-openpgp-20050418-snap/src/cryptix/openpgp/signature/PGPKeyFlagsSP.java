/* $Id: PGPKeyFlagsSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


/**
 * Represents a signature subpacket containing a list of binary flags that hold
 * information about a key. 
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>27 = key flags</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPKeyFlagsSP extends PGPFlagsSP {


// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPKeyFlagsSP () {}



// Getters/Setters
// ..........................................................................
       
       
    /**
     * Get if this key may be used to certify other keys.
     */
    public boolean getCertify()                { return getFlag(0,0x01); } 


    /**
     * Set if this key may be used to certify other keys.
     */
    public void setCertify              (boolean set) { setFlag(0,0x01,set); }


    /**
     * Get if this key may be used to sign data.
     */
    public boolean getSign()                   { return getFlag(0,0x02); } 


    /**
     * Set if this key may be used to sign data.
     */
    public void setSign                 (boolean set) { setFlag(0,0x02,set); }


    /**
     * Get if this key may be used to encrypt communications.
     */
    public boolean getEncryptCommunication()   { return getFlag(0,0x04); } 


    /**
     * Set if this key may be used to encrypt communications.
     */
    public void setEncryptCommunication (boolean set) { setFlag(0,0x04,set); }


    /**
     * Get if this key may be used to encrypt storage.
     */
    public boolean getEncryptStorage()         { return getFlag(0,0x08); } 


    /**
     * Get if this key may be used to encrypt storage.
     */
    public void setEncryptStorage       (boolean set) { setFlag(0,0x08,set); }


    /**
     * Get if the private component of this key may have been split by a
     * secret-sharing mechanism.
     */
    public boolean getSplitKey()               { return getFlag(0,0x10); } 


    /**
     * Set if the private component of this key may have been split by a
     * secret-sharing mechanism.
     */
    public void setSplitKey             (boolean set) { setFlag(0,0x10,set); }


    /**
     * Get if the private component of this key may be in the possession of 
     * more than one person.
     */
    public boolean getMultiplePersons()        { return getFlag(0,0x80); } 


    /**
     * Get if the private component of this key may be in the possession of 
     * more than one person.
     */
    public void setMultiplePersons      (boolean set) { setFlag(0,0x80,set); }

    
}
