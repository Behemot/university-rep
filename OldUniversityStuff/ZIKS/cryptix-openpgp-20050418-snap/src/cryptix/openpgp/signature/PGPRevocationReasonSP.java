/* $Id: PGPRevocationReasonSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


/**
 * Represents a signature subpacket containing a revocation reason.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>29 = reason for revocation</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPRevocationReasonSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPRevocationReasonSP () {}
    


// Constants
// ..........................................................................

    /** No reason specified */
    public static final byte NO_REASON       = 0x00;
    /** Key is superceded */
    public static final byte SUPERCEDED      = 0x01;
    /** Key material has been compromised */
    public static final byte COMPROMISED     = 0x02;
    /** Key is retired and no longer used */
    public static final byte RETIRED         = 0x03;
    /** User ID information is no longer valid */
    public static final byte NO_LONGER_VALID = 0x20;



// Instance variables
// ..........................................................................

    private byte   code  = NO_REASON;
    private String value;
    


// Getters/Setters
// ..........................................................................


    /**
     * Set the reason for revocation.
     */
    public void setValue(String value) { 
        if (value.length() == 0) {
            throw new IllegalArgumentException("String length == 0");
        }
        this.value = value; 
    }


    /**
     * Get the reason for revocation.
     */
    public String getValue() { return value; }
    

    /**
     * Set the revocation code.
     *
     * <p>Should be one of these:</p><ul>
     * <li>NO_REASON</li>
     * <li>SUPERCEDED</li>
     * <li>COMPROMISED</li>
     * <li>RETIRED</li>
     * <li>NO_LONGER_VALID</li>
     * </ul>
     */
    public void setCode(byte code) { this.code = code; }


    /**
     * Get the revocation code.
     *
     * <p>Should be one of these:</p><ul>
     * <li>NO_REASON</li>
     * <li>SUPERCEDED</li>
     * <li>COMPROMISED</li>
     * <li>RETIRED</li>
     * <li>NO_LONGER_VALID</li>
     * </ul>
     */
    public byte getCode() { return code; }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPRevocationReasonSP)) {
            return false;
        } else {
            PGPRevocationReasonSP that = (PGPRevocationReasonSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue()) &&
                   PGPCompare.equals(this.getCode(),  that.getCode());
        }
        
    }
    
    
    
// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        setCode(in.readByte());
        
        byte[] bytes = in.readByteArray();

        if (bytes.length == 0) {
            throw new PGPDataFormatException("String length == 0.");
        }
        
        try {

            String value = new String(bytes,"UTF-8");
            setValue(value);

        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported encoding: UTF-8");
        }
        
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if (getValue() == null) {
            throw new IllegalStateException("Packet data not initialized");
        }
        
        out.writeByte(getCode());
        
        try {

            byte[] bytes = getValue().getBytes("UTF-8");
            out.writeFully(bytes);
        
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("Unsupported encoding: UTF-8");
        }
        
    }

}
