/* $Id: PGPDateSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;

import java.util.Date;


/**
 * Represents a signature subpacket containing a Date value.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>2 = signature creation time</li>
 * <li>3 = signature expiration time</li>
 * <li>9 = key expiration time</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPDateSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPDateSP () {}

    

// Instance variables
// ..........................................................................

    private Date value;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the date value of this packet.
     */
    public void setValue(Date value) { 

        long m = value.getTime();

        if ((m < 0) || (m > (0xFFFFFFFFL * 1000L))) {
            throw new IllegalArgumentException("Date not within acceptable "+
                                               "range");
        }
        
        this.value = value; 
    }


    /**
     * Return the date value of this packet.
     */
    public Date getValue() { return value; }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPDateSP)) {
            return false;
        } else {
            PGPDateSP that = (PGPDateSP) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        long m = in.readUnsignedInt();
        m = m*1000;
        
        setValue(new Date(m));
        
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if (getValue() == null) {
            throw new IllegalStateException("Packet data not initialized");
        }
        
        long m = getValue().getTime();
        m = m/1000;

        int d = (int)m;

        out.writeInt(d);
        
    }


}
