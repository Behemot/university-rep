/* $Id: PGPSignatureSubPacketFactory.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import java.util.Hashtable;
import java.io.InputStream;
import java.io.IOException;


/**
 * A factory that can parse the binary subpacket data and return the right
 * subclasses of PGPSignatureSubPacket.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPSignatureSubPacketFactory {


// Constants
//...........................................................................

    public static final int
        INIT_DEFAULT          = 0x00,
        INIT_EMPTY_ALL        = 0xFF,
        INIT_EMPTY            = INIT_EMPTY_ALL;


// Vars
//...........................................................................

    private Hashtable
        PacketClasses   = new Hashtable();


// Default data
//...........................................................................

    private static final int[] defaultPacketIDs = {
        2, 3, 4, 5, 6, 7, 9, 11, 12, 16, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30
    };
    
    private static final String[] defaultPacketClasses = {
        //   2 = signature creation time
        "cryptix.openpgp.signature.PGPDateSP",
        //   3 = signature expiration time
        "cryptix.openpgp.signature.PGPDateSP",
        //   4 = exportable certification
        "cryptix.openpgp.signature.PGPBooleanSP",
        //   5 = trust signature
        "cryptix.openpgp.signature.PGPTrustSP",
        //   6 = regular expression
        "cryptix.openpgp.signature.PGPNullTerminatedStringSP",
        //   7 = revocable
        "cryptix.openpgp.signature.PGPBooleanSP",
        //   9 = key expiration time
        "cryptix.openpgp.signature.PGPDateSP",
        //   11 = preferred symmetric algorithms
        "cryptix.openpgp.signature.PGPByteArraySP",
        //   12 = revocation key
        "cryptix.openpgp.signature.PGPRevocationKeySP",
        //   16 = issuer key ID
        "cryptix.openpgp.signature.PGPKeyIDSP",
        //   20 = notation data
        "cryptix.openpgp.signature.PGPNotationDataSP",
        //   21 = preferred hash algorithms
        "cryptix.openpgp.signature.PGPByteArraySP",
        //   22 = preferred compression algorithms
        "cryptix.openpgp.signature.PGPByteArraySP",
        //   23 = key server preferences
        "cryptix.openpgp.signature.PGPKeyServerPrefsSP",
        //   24 = preferred key server
        "cryptix.openpgp.signature.PGPStringSP",
        //   25 = primary user id
        "cryptix.openpgp.signature.PGPBooleanSP",
        //   26 = policy URL
        "cryptix.openpgp.signature.PGPStringSP",
        //   27 = key flags
        "cryptix.openpgp.signature.PGPKeyFlagsSP",
        //   28 = signer's user id
        "cryptix.openpgp.signature.PGPStringSP",
        //   29 = reason for revocation
        "cryptix.openpgp.signature.PGPRevocationReasonSP",
        //   30 = features
        "cryptix.openpgp.signature.PGPFeaturesSP",
    };


// Constructors
//...........................................................................

    /**
     * Initialize the factory with the default signature subpackets.
     *
     * <p>The default signature subpackets are those that are specified in the 
     * <a href="http://www.cryptix.org/docs/openpgp.html">OpenPGP specification
     * (RFC2440)</a>.</p>
     */
    public PGPSignatureSubPacketFactory () {
        this(INIT_DEFAULT);
    }


    /**
     * Initialize the factory with or without the default signature subpackets.
     *
     * <p>Depending on the value of init the factory is initialized as follows:
     * </p><ul>
     * <li>INIT_DEFAULT - The default signature subpackets are registered.</li>
     * <li>INIT_EMPTY_ALL - No signature subpackets are registered.</li>
     * <li>INIT_EMPTY - synonym for INIT_EMPTY_ALL.</li>
     * </ul>
     */
    public PGPSignatureSubPacketFactory (int init) {

        if ((init & INIT_EMPTY_ALL)  == 0)
            DefaultPackets();

    }



// Constructor help methods
//...........................................................................

    /** 
     * Register all default packets
     */
    private void DefaultPackets() {
        
        try {

            for (int i=0; i<defaultPacketIDs.length; i++) {
                registerPacket(defaultPacketIDs[i],
                               Class.forName(defaultPacketClasses[i]));
            }
            
        } catch (ClassNotFoundException cnfe) {
            throw new InternalError("Class not found for a default signature "+
                                    "subpacket "+cnfe);
        }
        
    }



// Default instance method
//...........................................................................

    /** Cached default instance */
    private static PGPSignatureSubPacketFactory defaultInstance = null;


    /**
     * Return the default instance of this factory.
     *
     * <p>Useful when you don't need to do any customization.</p>
     * <p>This method returns an instance that has been constructed using the
     * INIT_DEFAULT parameter.</p>
     */
    public static PGPSignatureSubPacketFactory getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = new PGPSignatureSubPacketFactory(INIT_DEFAULT);
        }
        return defaultInstance;
    }



// Register
//...........................................................................

    /**
     * Register a new signature subpacket for a particular packet id.
     *
     * @param id the packet type (0-255).
     * @param pktclass the Class object for a subclass of PGPSignatureSubPacket.
     * 
     */
    public void registerPacket(int id, Class pktclass) {
        PacketClasses.put(new Integer(id), pktclass);
    }



// Unregister
//...........................................................................

    /**
     * Unregister an already registered signature subpacket.
     *
     * <p>This method does nothing if the signature subpacket is not registered.
     * </p>
     *
     * @param id the packet type (0-255) for the signature subpacket you wish to
     *        unregister.
     */
    public void unregisterPacket(int id) {
        PacketClasses.remove(new Integer(id));
    }



// Read method
//...........................................................................

    /**
     * Read and parse one signature subpacket from the input.
     *
     * <p>This method does all the magic parsing to deliver the right subclass
     * of PGPSignatureSubPacket</p>
     * 
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    public PGPSignatureSubPacket readPacket(InputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        PGPSignatureDataInputStream sdin = new PGPSignatureDataInputStream(in);

        int type = sdin.readUnsignedByte();
        boolean critical = (type & 0x80) != 0;
        type = type & 0x7F;

        try {

            PGPSignatureSubPacket packet;
            
            Class cl = (Class)PacketClasses.get(new Integer(type));
            if (cl != null) {
            
                packet = (PGPSignatureSubPacket)cl.newInstance();
                packet.setPacketID((byte)type);
                packet.decode(sdin);
                packet.setCritical(critical);
                return packet;
                
            } else {

                if (critical) {
                    throw new PGPFatalDataFormatException("Unknown critical "+
                                               "subpacket found in signature.");
                }

                // Hashtable returned null, so we don't know which packet to
                // use. We are also sure that it is a non-critical packet.
                // Let's load a placeholder packet.
                packet = new PGPUnknownSP();
                packet.setPacketID((byte)type);
                packet.decode(sdin);
                return packet;

            }


        } catch (InstantiationException ie) {

            throw new RuntimeException("The class for signature subpacket #"+
                                        type+" cannot be loaded. - "+ie);

        } catch (IllegalAccessException iae) {

            throw new RuntimeException("The class for signature subpacket #"+
                                        type+" cannot be loaded. - "+iae);

        }

    }



}
