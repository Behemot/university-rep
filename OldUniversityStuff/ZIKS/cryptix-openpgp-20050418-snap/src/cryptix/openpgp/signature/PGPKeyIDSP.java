/* $Id: PGPKeyIDSP.java,v 1.2 2005/03/13 17:46:39 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.signature;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPSignatureDataInputStream;
import cryptix.openpgp.io.PGPSignatureDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * Represents a signature subpacket containing an 8 byte key ID.
 *
 * <p>This class is used to represent the following default packets:</p><ul>
 * <li>16 = issuer key ID</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPKeyIDSP extends PGPSignatureSubPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPKeyIDSP () {}

    

// Instance variables
// ..........................................................................

    private byte[] value;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set the key ID of this packet.
     */
    public void setValue(byte[] value) { 
        if (value.length != 8)
            throw new IllegalArgumentException("KeyID should be 8 bytes");
        this.value = value; 
    }


    /**
     * Get the key ID of this packet.
     */
    public byte[] getValue() { return value; }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPKeyIDSP)) {
            return false;
        } else {
            PGPKeyIDSP that = (PGPKeyIDSP)ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getValue(), that.getValue());
        }
        
    }
    
    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     */
    protected void decodeBody (PGPSignatureDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        
        byte[] value = new byte[8];
        
        in.readFully(value);
        
        setValue(value);
                
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    protected void encodeBody (PGPSignatureDataOutputStream out) 
        throws IOException
    {
        
        if (getValue() == null) {
            throw new IllegalStateException("Packet data not initialized");
        }
        
        out.writeFully(getValue());
        
    }


}
