/* $Id: PGPKey.java,v 1.5 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPKeyPacket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.Key;


/**
 * OpenPGP Key.
 *
 * @author  Ingo Luetkebohle
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.5 $
 */
public abstract class PGPKey implements Key {


// Instance variables
//.............................................................................


    /** The packet that contains the core part of this key */
    private PGPKeyPacket packet;
    /** The algorithm of this key */
    private String algorithm;



// Constructor
//.............................................................................


    /** 
     * Constructor that takes a packet and a format string.
     *
     * @param pkt the packet that contains the data for this key.
     * @param algorithm the string to be returned by the getAlgorithm() method.
     */
    protected PGPKey (PGPKeyPacket pkt, String algorithm) {
        if (pkt == null) throw new IllegalArgumentException("null argument");
        this.packet = pkt;
        this.algorithm = algorithm;
    }



// Methods from java.lang.Key
//.............................................................................


    /**
     * Returns this key in encoded form.
     *
     * <p>Note: this just returns the key itself, without any userid's or
     * certificates. See KeyBundle for that functionality.</p>
     */
    public byte[] getEncoded() {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            packet.encode(baos);
            return baos.toByteArray();
            
        } catch (IOException ioe) {
            throw new InternalError(
                "IOException in ByteArrayOutputStream - "+ioe);
        }
    }


    /**
     * Returns the format of this key.
     *
     * <p>Always returns "OpenPGP".</p>
     */
    public String getFormat() {
        return "OpenPGP";
    }


    /**
     * Returns the algorithm of this key.
     */
    public String getAlgorithm() {
        return algorithm;
    }



// Methods from java.lang.Object
//.............................................................................


    /**
     * Returns if two PGPKeys are equal
     */
    public boolean equals(Object other) {
        if (other == null) return false;
        if (! (other instanceof PGPKey)) return false;
        return this.packet.equals(((PGPKey)other).packet);
    }

    

// Own methods
//.............................................................................


    /**
     * Return the bitlength of this key
     */
    public int getBitLength() {
        return packet.getAlgorithm().getBitLength();
    }
    
    
    /**
     * Return the contained keypacket 
     *
     * <p>Note: packets are part of the low-level API. Normally you don't need
     * to use this method as other options are available.</p>
     */
    public PGPKeyPacket getPacket() {
        return packet;
    }
    
    
    /**
     * Returns whether this is a legacy key
     */
    public boolean isLegacy() {
        return packet.getVersion() <= 3;
    }
    
}
