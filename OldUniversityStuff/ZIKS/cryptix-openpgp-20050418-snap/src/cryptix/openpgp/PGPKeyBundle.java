/* $Id: PGPKeyBundle.java,v 1.4 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.util.Date;
import java.util.Iterator;


/**
 * An OpenPGP KeyBundle.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @author  Ingo Luetkebohle
 * @version $Revision: 1.4 $
 */
public abstract class PGPKeyBundle extends KeyBundle {


// Constructor
// ..........................................................................

    /**
     * Create a new KeyBundle object with the given type.
     */
    protected PGPKeyBundle(String type)
    {
        super(type);
    }



// Subkey methods
// ..........................................................................


    /**
     * Returns whether this KeyBundle contains a Private Key.
     */
    public abstract boolean containsPrivateKey();


    /**
     * Adds a public subkey to a keybundle, calculating binding signature.
     */
    public abstract boolean addPublicSubKey (PublicKey subkey, 
                                             PrivateKey signkey)
        throws KeyBundleException;


    /**
     * Adds a subkey to a keybundle (borrows binding signature from another
     * keybundle).
     */
    public abstract boolean addPublicSubKey (PublicKey pubsubkey, 
                                             KeyBundle other)
        throws KeyBundleException;


    /**
     * Adds a private subkey to the keybundle, encrypting it with the given
     * passphrase.
     */
    public abstract boolean addPrivateSubKey (PrivateKey privsubkey, 
                                              PublicKey  pubsubkey, 
                                              char[] passphrase, 
                                              SecureRandom sr)
        throws KeyBundleException;
    

    /**
     * Adds a private subkey to the keybundle, without encrypting it.
     */
    public abstract boolean addPrivateSubKey (PrivateKey privsubkey, 
                                              PublicKey  pubsubkey)
        throws KeyBundleException;
    

    /**
     * Removes a public subkey.
     */
    public abstract boolean removePublicSubKey (PublicKey pubsubkey)
        throws KeyBundleException;


    /**
     * Removes the private subkey corresponding to the given public subkey.
     */
    public abstract boolean removePrivateSubKey (PublicKey pubsubkey)
        throws KeyBundleException;


    /**
     * Returns an iterator over all subkeys contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * cryptix.openpgp.PGPPublicKey</p>
     */
    public abstract Iterator getPublicSubKeys();


    /**
     * Return the private key corresponding to the given public key, decrypting
     * it with the given passphrase.
     *
     * <p>Returns null if no private key is available</p>
     */
    public abstract PrivateKey getPrivateSubKey (PublicKey pubsubkey,
                                                    char[] passphrase)
        throws UnrecoverableKeyException;


// Other methods
// ..........................................................................


    /**
     * Returns the expiration date, or null of the keybundle does not expire
     *
     * <p>Note: this method assumes that all self signed certificates in this
     * keybundle are correctly signed and not revoked. It does not verify this. 
     * When getting a key from an untrusted source, these should thus be 
     * checked first.</p>
     */
    public abstract Date getExpirationDate()
        throws KeyBundleException;
    

    /**
     * Returns whether this is a keybundle containing a legacy key
     */
    public abstract boolean isLegacy();
    
    
    // ### TODO: add methods here for:
    // ### - preferences
    // ### - revocation

}
