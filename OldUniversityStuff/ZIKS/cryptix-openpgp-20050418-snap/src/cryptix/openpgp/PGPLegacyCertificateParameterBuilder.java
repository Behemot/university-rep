/* $Id: PGPLegacyCertificateParameterBuilder.java,v 1.1 2005/03/13 17:12:53 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.pki.KeyID;

import java.security.InvalidKeyException;
import java.security.Key;


/**
 * Parameters for a V3 PGP Certificate
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPLegacyCertificateParameterBuilder 
    extends PGPAbstractV3SignatureParameterBuilder 
{

// Constructors
//..............................................................................

    public PGPLegacyCertificateParameterBuilder(KeyID issuerkeyid) 
    {
        super(issuerkeyid, PGPSignatureConstants.TYPE_GENERIC_CERT);
    }

    public PGPLegacyCertificateParameterBuilder(Key issuerkey)
        throws InvalidKeyException
    {
        super(issuerkey, PGPSignatureConstants.TYPE_GENERIC_CERT);
    }

}
