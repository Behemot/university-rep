/* $Id: PGPCertificateParameterBuilder.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.PGPPrincipal;

import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPBooleanSP;
import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyFlagsSP;
import cryptix.openpgp.signature.PGPNotationDataSP;
import cryptix.openpgp.signature.PGPStringSP;
import cryptix.openpgp.signature.PGPTrustSP;

import cryptix.pki.KeyID;

import java.security.InvalidKeyException;
import java.security.Key;

import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;


/**
 * Parameters for a PGP Certificate
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPCertificateParameterBuilder 
    extends PGPAbstractSignatureParameterBuilder 
{

// Constructors
//..............................................................................

    public PGPCertificateParameterBuilder(KeyID issuerkeyid) 
    {
        super(issuerkeyid, PGPSignatureConstants.TYPE_GENERIC_CERT);
    }

    public PGPCertificateParameterBuilder(Key issuerkey)
        throws InvalidKeyException
    {
        super(issuerkey, PGPSignatureConstants.TYPE_GENERIC_CERT);
    }
    

// Setters
//..............................................................................

    /**
     * Set or change the expiration date
     *
     * @param expiration the new expiration date, or null if there is no
     *                   expiration date (the default).
     */
    public void setExpirationDate(Date expiration)
    {
        if (expiration == null) {
            removePacket(PGPSignatureConstants.SSP_SIG_EXPIRATION_TIME);
        } else {
            PGPDateSP pkt = new PGPDateSP();
            pkt.setValue(expiration);
            pkt.setPacketID(PGPSignatureConstants.SSP_SIG_EXPIRATION_TIME);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change exportability
     *
     * @param exportable true if this certificate is exportable (the default) or
     *                   false if it is not.
     */
    public void setExportable(boolean exportable)
    {
        if (exportable) {
            removePacket(PGPSignatureConstants.SSP_EXPORTABLE_CERTIFICATION);
        } else {
            PGPBooleanSP pkt = new PGPBooleanSP();
            pkt.setValue(false);
            pkt.setPacketID(PGPSignatureConstants.SSP_EXPORTABLE_CERTIFICATION);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change trust settings
     *
     * @param level  the depth or level of this certification, with 0 being the
     *               default. Allowed values are between 0 and 255 inclusive.
     * @param amount the trust amount, common values are 0 for no trust (the
     *               default), 60 for partial trust and 120 for complete trust.
     *               Allowed values are between 0 and 255 inclusive.
     * @param regex  a regular expression that limits this trust extension to
     *               signatures made on userids matched by this regular
     *               expression.
     */
    public void setTrust(int level, int amount, String regex)
    {
        if ((level == 0) && (amount == 0)) {
            removePacket(PGPSignatureConstants.SSP_TRUST_SIGNATURE);
        } else {
            PGPTrustSP pkt = new PGPTrustSP();
            pkt.setDepth((byte)level);
            pkt.setAmount((byte)amount);
            pkt.setPacketID(PGPSignatureConstants.SSP_TRUST_SIGNATURE);
            setPacket(pkt);
        }
        if ((regex == null) || (level == 0)) {
            removePacket(PGPSignatureConstants.SSP_REGULAR_EXPRESSION);
        } else {
            PGPStringSP pkt = new PGPStringSP();
            pkt.setValue(regex);
            pkt.setPacketID(PGPSignatureConstants.SSP_REGULAR_EXPRESSION);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change revocability
     *
     * @param revocable true if this certificate can be revoked (the default), 
     *                  false if it cannot be revoked.
     */
    public void setRevocable(boolean revocable) 
    {
        if (revocable) {
            removePacket(PGPSignatureConstants.SSP_REVOCABLE);
        } else {
            PGPBooleanSP pkt = new PGPBooleanSP();
            pkt.setValue(false);
            pkt.setPacketID(PGPSignatureConstants.SSP_REVOCABLE);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change notation data
     *
     * @param humanreadable   human readable notation data
     * @param machinereadable machine readable notation data
     */
    public void setNotationData(Properties humanreadable, 
                                Properties machinereadable)
    {
        // remove all old packets
        while (removePacket(PGPSignatureConstants.SSP_NOTATIONDATA) != null) {}
        
        // add human readable notation data
        if (humanreadable != null) {
            Enumeration em = humanreadable.propertyNames();
            while (em.hasMoreElements()) {
                String name = (String)em.nextElement();
                String value = humanreadable.getProperty(name);
                PGPNotationDataSP pkt = new PGPNotationDataSP();
                pkt.setNameData(name);
                pkt.setValueData(value);
                pkt.setHumanReadable(true);
                pkt.setPacketID(PGPSignatureConstants.SSP_NOTATIONDATA);
                addPacket(pkt);
            }
        } 
        
        //add machine readable notation data
        if (machinereadable != null) {
            Enumeration em = machinereadable.propertyNames();
            while (em.hasMoreElements()) {
                String name = (String)em.nextElement();
                String value = machinereadable.getProperty(name);
                PGPNotationDataSP pkt = new PGPNotationDataSP();
                pkt.setNameData(name);
                pkt.setValueData(value);
                pkt.setHumanReadable(false);
                pkt.setPacketID(PGPSignatureConstants.SSP_NOTATIONDATA);
                addPacket(pkt);
            }
        }
    }
    
    /**
     * Set or change the policy URL
     *
     * @param policyurl the URL of a deocument describing the policy the
     *                  certificate was issued under.
     */
    public void setPolicyURL(String policyurl)
    {
        if (policyurl == null) {
            removePacket(PGPSignatureConstants.SSP_POLICY_URL);
        } else {
            PGPStringSP pkt = new PGPStringSP();
            pkt.setValue(policyurl);
            pkt.setPacketID(PGPSignatureConstants.SSP_POLICY_URL);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change keyflags
     *
     * @param certification        true if the certified key can be used for
     *                             certifying other keys (the default), false
     *                             otherwise
     * @param signdata             true if the certified key can be used for
     *                             signing data (the default), false otherwise
     * @param encryptcommunication true if the certified key can be used for
     *                             encryption of communication (the default),
     *                             false otherwise
     * @param encryptstorage       true if the certified key can be used for
     *                             encryption of storage (the default), false
     *                             otherwise
     */
    public void setKeyFlags(boolean certification, boolean signdata,
                            boolean encryptcommunication, 
                            boolean encryptstorage)
    {
        if (certification && signdata && encryptcommunication && encryptstorage)
        {
            removePacket(PGPSignatureConstants.SSP_KEY_FLAGS);
        } else {
            PGPKeyFlagsSP pkt = new PGPKeyFlagsSP();
            pkt.setCertify(certification);
            pkt.setSign(signdata);
            pkt.setEncryptCommunication(encryptcommunication);
            pkt.setEncryptStorage(encryptstorage);
            pkt.setPacketID(PGPSignatureConstants.SSP_KEY_FLAGS);
            setPacket(pkt);
        }
    }
    
    /**
     * Set or change the issuing user id
     *
     * @param issueruserid the principal that issues this certificate
     */
    public void setIssuerUserID(PGPPrincipal issueruserid) {
        if (issueruserid == null) {
            removePacket(PGPSignatureConstants.SSP_SIGNER_USERID);
        } else {
            PGPStringSP pkt = new PGPStringSP();
            pkt.setValue(issueruserid.getName());
            pkt.setPacketID(PGPSignatureConstants.SSP_SIGNER_USERID);
            setPacket(pkt);
        }
    }
}
