/* $Id: PGPRSA.java,v 1.7 2005/03/29 11:22:48 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import cryptix.openpgp.util.PGPMPI;

import java.io.IOException;

import java.math.BigInteger;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

import java.security.interfaces.RSAPublicKey;
import java.security.interfaces.RSAPrivateCrtKey;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;


/**
 * RSA wrapper
 *
 * <p>This class contains the key and has methods to do the signing and 
 * encryption operations with this key.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
 
public class PGPRSA implements PGPSigner, PGPEncryptor {
    
// Vars
//...........................................................................

    private BigInteger 
        n  = null,  // PublicKey
        e  = null,  // PublicKey
        d  = null,  // PrivateKey
        p  = null,  // PrivateKey
        q  = null,  // PrivateKey
        u  = null,  // PrivateKey
        pp = null,  // PrivateKey, calculated as (p-1) mod d
        qq = null,  // PrivateKey, calculated as (q-1) mod d
        s  = null,  // Signature
        m  = null;  // Message

    private Signature sig;
    
    private Cipher cipher;


// Constructor
//...........................................................................

    /**
     * Empty constructor
     */
    public PGPRSA () {}


    /**
     * Constructor for clonePublic
     */
    PGPRSA (BigInteger n, BigInteger e) {
        this.n = n;
        this.e = e;
    }


    /**
     * Constructor for clonePrivate
     */
    PGPRSA (BigInteger n, BigInteger e, BigInteger d, BigInteger p, 
            BigInteger q, BigInteger pp, BigInteger qq, BigInteger u) 
    {
        this.n  = n;
        this.e  = e;
        this.d  = d;
        this.p  = p;
        this.q  = q;
        this.pp = pp;
        this.qq = qq;
        this.u  = u;
    }



// Methods from java.lang.Object
//...........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object obj) {
    
        if (obj instanceof PGPRSA) {
        
            PGPRSA objx = (PGPRSA)obj;
            return objx.equals(n, e, d, p, q, pp, qq, u);
        
        } else {
        
            return false;
            
        }
        
    }
    
    
    /**
     * Compare help method.
     */
    /* package */ boolean equals(BigInteger n, BigInteger e, BigInteger d,
        BigInteger p, BigInteger q, BigInteger pp, BigInteger qq, BigInteger u)
    {

        return compare(this.n,n)
            && compare(this.e,e)
            && compare(this.d,d)
            && compare(this.p,p)
            && compare(this.q,q)
            && compare(this.pp,pp)
            && compare(this.qq,qq)
            && compare(this.u,u);

    }
    
    /**
     * Compare help method.
     */
    private boolean compare(BigInteger a, BigInteger b) {
    
        if (a == null) {
            return (b == null);
        } else {
            if (b == null) return false;
            return a.equals(b);
        }
        
    }
    

    /**
     * Returns the hashcode for this object.
     */
    public int hashCode() {
    
        int hash = 0;

        if (n != null) hash ^= n.hashCode();
        if (e != null) hash ^= e.hashCode();
        if (d != null) hash ^= d.hashCode();

        return hash;
        
    }


    /**
     * Return a string representing the algorithm.
     */
    public String toString() { return "RSA"; }



// Signature compare methods
//...........................................................................

    /** 
     * Compare signatures
     */
    public boolean signatureEquals(PGPSigner obj) {

        if (obj instanceof PGPRSA) {
        
            PGPRSA objx = (PGPRSA)obj;
            
            return objx.signatureEquals(s);
        
        } else {
        
            return false;
            
        }
        
    }


    /**
     * Compare help method.
     */
    /* package */ boolean signatureEquals(BigInteger s)
    {

        return compare(this.s,s);

    }
    


// Methods from PGPPublicKeyAlgorithm
//...........................................................................

    /**
     * Read the public part of the key from a DataInput
     */
    public void decodePublicData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        n = in.readMPI();
        e = in.readMPI();
    }
    

    /**
     * Read the secret part of the key from a DataInput
     */
    public void decodeSecretData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        d = in.readMPI();
        p = in.readMPI();
        q = in.readMPI();
        u = in.readMPI();
        pp = d.mod(p.subtract(BigInteger.valueOf(0x1)));
        qq = d.mod(q.subtract(BigInteger.valueOf(0x1)));
    }
    

    /**
     * Write the public part of the key to a DataOutput
     */
    public void encodePublicData(PGPDataOutputStream out)
        throws IOException 
    {
        out.writeMPI(n);
        out.writeMPI(e);
    }
    

    /**
     * Write the secret part of the key to a DataOutput
     */
    public void encodeSecretData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(d);
        out.writeMPI(p);
        out.writeMPI(q);
        out.writeMPI(u);
    }
    

    /**
     * Helper method needed to ease the calculation the V3 fingerprint.
     *
     * <p>See cryptix.openpgp.PGPKeyID</p>
     */
    public byte[] encodeV3FingerprintData() 
    {
        byte[] modbytes = n.toByteArray();
        byte[] expbytes = e.toByteArray();
        int modlen = modbytes.length;
        int explen = expbytes.length;
        if (modbytes[0] == 0) modlen--;
        if (expbytes[0] == 0) explen--;

        byte[] temp = new byte[modlen+explen];
        System.arraycopy(modbytes, modbytes.length-modlen, temp, 0, modlen);
        System.arraycopy(expbytes, expbytes.length-explen, temp, modlen,explen);

        return temp;
    }
    

    /**
     * Helper method needed to ease the calculation the V3 keyID.
     *
     * <p>See cryptix.openpgp.PGPKeyID</p>
     */
    public byte[] encodeV3KeyIDData() 
    {
        byte[] bytes = n.toByteArray();
        byte[] temp = new byte[8];
        System.arraycopy(bytes, bytes.length-8, temp, 0, 8);
        return temp;
    }


    /**
     * Forget the secret data
     *
     * <p>If no secret data is present, nothing happens.</p>
     *
     * <p>Note that this method cannot really make sure the secret data is not
     * present anymore in system memory and/or system swap space.</p>
     */
    public void forgetSecretData() {
        d  = null;
        p  = null;
        q  = null;
        pp = null;
        qq = null;
        u  = null;
    }
    
    
    /**
     * Generate a keypair for a given keysize.
     *
     * <p>Use writePublicData and writeSecretData to save the key
     */
    public void generateKeyPair(int keysize, SecureRandom sr) {
        KeyPairGenerator kpg;
        try {
            //### FIXME: Should allow for other providers
            kpg = KeyPairGenerator.getInstance("RSA", "CryptixCrypto");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("Cannot find RSA implementation");
        } catch (NoSuchProviderException nspe) {
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
        kpg.initialize(keysize,sr);
        KeyPair kp = kpg.generateKeyPair();
        n  = ((RSAPublicKey)     kp.getPublic() ).getModulus();
        e  = ((RSAPublicKey)     kp.getPublic() ).getPublicExponent();
        d  = ((RSAPrivateCrtKey) kp.getPrivate()).getPrivateExponent();
        p  = ((RSAPrivateCrtKey) kp.getPrivate()).getPrimeP();
        q  = ((RSAPrivateCrtKey) kp.getPrivate()).getPrimeQ();
        pp = ((RSAPrivateCrtKey) kp.getPrivate()).getPrimeExponentP();
        qq = ((RSAPrivateCrtKey) kp.getPrivate()).getPrimeExponentQ();
        u  = ((RSAPrivateCrtKey) kp.getPrivate()).getCrtCoefficient();

        // pgp spec requires that p < q, so swap if needed
        if (p.compareTo(q) == 1) {
            BigInteger t;
            t = p;
            p = q;
            q = t;
            t  = pp;
            pp = qq;
            qq = t;
        }
        
        // Recalculate u, to make sure it is p.modInverse(q), instead of
        // q.modInverse(p), which the JCE generates. If p and q are swapped
        // in the previous step u would be correct, but we do not take any
        // chances and calculate it anyway.
        u  = p.modInverse(q);
    }


    /**
     * Return the bitlength of the contained key.
     */
    public int getBitLength() { return n.bitLength(); }


    /**
     * Create a copy of this object containing only the Public parts
     * of the key.
     */
    public PGPPublicKeyAlgorithm clonePublic() {
        return new PGPRSA(n,e);
    }


    /**
     * Create a copy of this object containing the Public & Private parts
     * of the key.
     *
     * <p>The clone will not contain any state with regard to encrypted data
     * and signatures.</p>
     */
    public PGPPublicKeyAlgorithm clonePrivate() {
        return new PGPRSA(n,e,d,p,q,pp,qq,u);
    }



// Methods from PGPSigner
//...........................................................................

    /** 
     * Initializes for signing
     *
     * <p>Use update to add data and computeSignature to compute the signature
     */
    public void initSign(int hashID, PGPAlgorithmFactory factory) {
        try {
            String h = factory.getHashAlgorithmString(hashID);
            //### FIXME: Should allow for other providers
            sig = Signature.getInstance(h + "withRSA", "CryptixCrypto");
            sig.initSign(new PGPRSAPrivateCrtKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("RSA algorithm not found.");
        } catch (NoSuchProviderException nspe) {
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    

    /** 
     * Initializes for verifying
     *
     * <p>Use update to add data and verifySignature to verify the signature
     */
    public void initVerify(int hashID, PGPAlgorithmFactory factory) {
        try {
            String h = factory.getHashAlgorithmString(hashID);
            //### FIXME: Should allow for other providers
            sig = Signature.getInstance(h + "withRSA", "CryptixCrypto");
            sig.initVerify(new PGPRSAPublicKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("RSA algorithm not found.");
        } catch (NoSuchProviderException nspe) {
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    
    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data) {
        try {
            sig.update(data);
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    
    
    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data, int offset, int len) {
        try {
            sig.update(data, offset, len);
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    
    
    /**
     * Calculate the signature
     *
     * <p>Use writeSignatureData to output it to a stream.
     */
    public void computeSignature() {
        byte[] sigdata;
        
        try { 
            sigdata = sig.sign();
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
        
        s = new BigInteger(1, sigdata);
    }
    
    
    /**
     * Verify the signature
     *
     * @return true if the signature is correct
     */
    public boolean verifySignature() {
        byte[] sigdata = PGPMPI.toFixedLenByteArray(this.s,(n.bitLength()+7)/8);
        try {
            return sig.verify(sigdata);
        } catch (SignatureException se) {
            throw new IllegalStateException("SignatureException");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    

    /**
     * Read the signature in PGP compatible format
     */
    public void decodeSignatureData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        s = in.readMPI();
    }
    
    /**
     * Write the signature in PGP compatible format
     */
    public void encodeSignatureData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(s);
    }
    


// Methods from PGPEncryptor
//...........................................................................

    /** 
     * Encrypts the byte[].
     *
     * @param byte[] data The data to encrypt.
     */
    public void encrypt(byte[] data, SecureRandom sr) {
        try {
            //### FIXME: Should allow for other providers
            cipher = Cipher.getInstance("RSA/ECB/PKCS#1", "CryptixCrypto");
            cipher.init(Cipher.ENCRYPT_MODE, new PGPRSAPublicKey(), sr);
                
            byte[] encrypted = cipher.doFinal(data);
            m = new BigInteger(1, encrypted );

        } catch(SecurityException se) {
            // Braindamaged SUN restrictions
            se.printStackTrace(System.err);
            throw new RuntimeException("Not allowed to use RSA. " + 
                "Perhaps you need to download the Unlimited Strength " +
                "Jurisdiction Policy Files from the Sun website? " + se);
        } catch(InvalidKeyException ivk) {
            // We construct the keys ourselves, so this shouldn't happen.
            ivk.printStackTrace();
            throw new InternalError("Key was invalid:");
        } catch(IllegalBlockSizeException ibs) {
            // Because we use padding, this should not happen.
            ibs.printStackTrace();
            throw new InternalError("Illegal blocksize");
        } catch(BadPaddingException bp) {
            // This exception is not thrown in encryption mode.
            bp.printStackTrace();
            throw new InternalError("Bad padding");
        } catch(NoSuchAlgorithmException nsae) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsae.printStackTrace();
            throw new RuntimeException("RSA algorithm not found. "+
                                       "Provider problem?");
        } catch(NoSuchPaddingException nsp) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsp.printStackTrace();
            throw new RuntimeException("Couldn't use PKCS#1 padding. "+
                                       "Provider problem?");
        } catch(NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    /** 
     * Decrypts the byte[]
     *
     * @return byte[] The decrypted data.
     */
    public byte[] decrypt() throws PGPDecryptionFailedException {
        try {
            //### FIXME: Should allow for other providers
            cipher = Cipher.getInstance("RSA/ECB/PKCS#1", "CryptixCrypto");
            cipher.init(Cipher.DECRYPT_MODE, new PGPRSAPrivateCrtKey() );

            /*
             * Our BigInteger (this.m) may have too little or too many
             * bytes in it's toByteArray representation. Too little because 
             * superfluous zero bytes are stripped. Too many because toByte[]
             * always emits a sign-bit.
             *
             * We solve this by using toFixedLengByteArray to convert this.m
             * to a byte[] of the correct, fixed size.
             */
            int cipherBlockSize = cipher.getBlockSize();
            byte[] ct = PGPMPI.toFixedLenByteArray(this.m, cipherBlockSize);

            return cipher.doFinal( ct );

        } catch(SecurityException se) {
            // Braindamaged SUN restrictions
            se.printStackTrace();
            throw new RuntimeException("Not allowed to use RSA. " + 
                "Perhaps you need to download the Unlimited Strength " +
                "Jurisdiction Policy Files from the Sun website? " + se);
        } catch(IllegalArgumentException iae) {
            // PGPMPI.toFixedLenByteArray throws this one when m is negative
            // (won't happen) or when m is larger than cipherBlockSize (this
            // will happen when we used the wrong key and the right key is
            // larger than this one.
            throw new PGPDecryptionFailedException("Data larger than key. "+
                                                   "Wrong key used?");
        } catch(BadPaddingException bp) {
            // Decrypted data is not something that looks like a valid PKCS#1
            // padded block. So obviously the wrong key was used.
            throw new PGPDecryptionFailedException("Decrypted data != PKCS#1. "+
                                                   "Wrong key used?");
        } catch(IllegalBlockSizeException ibs) {
            // The use of the toFixedLenByteArray method makes sure that this
            // error can't happen.
            ibs.printStackTrace();
            throw new InternalError("Illegal blocksize");
        } catch(InvalidKeyException ivk) {
            // We construct the keys ourself, so this should not happen.
            ivk.printStackTrace();
            throw new InternalError("Key was invalid");
        } catch(NoSuchAlgorithmException nsae) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsae.printStackTrace();
            throw new RuntimeException("RSA algorithm not found. "+
                                       "Provider problem?");
        } catch(NoSuchPaddingException nsp) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsp.printStackTrace();
            throw new RuntimeException("Couldn't use PKCS#1 padding. "+
                                       "Provider problem?");
        } catch(NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    /**
     * Read the encrypted data in PGP compatible format
     */
    public void decodeEncryptedData(PGPDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        m = in.readMPI();
    }
    
    
    /** 
     * Write the encrypted in PGP compatible format
     */
    public void encodeEncryptedData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(m);
    }



// Inner key classes
//...........................................................................


    private class PGPRSAPublicKey implements RSAPublicKey {
        public BigInteger getModulus()         { return n; }
        public BigInteger getPublicExponent()  { return e; }
        public String getAlgorithm()           { return "RSA"; }
        public String getFormat()              { return null; }
        public byte[] getEncoded()             { return null; }
    }
    
    private class PGPRSAPrivateCrtKey implements RSAPrivateCrtKey {
        public BigInteger getModulus()         { return n; }
        public BigInteger getPrivateExponent() { return d; }
        public BigInteger getPublicExponent()  { return e; }
        // note that P and Q are swapped because the CRT algorithm implemented
        // in the JCE uses q^-1 mod p as the crt coefficient, while the PGP 
        // spec delivers p^-1 mod q.
        public BigInteger getPrimeP()          { return q; }
        public BigInteger getPrimeQ()          { return p; }
        public BigInteger getPrimeExponentP()  { return qq; }
        public BigInteger getPrimeExponentQ()  { return pp; }
        public BigInteger getCrtCoefficient()  { return u; }
        public String getAlgorithm()           { return "RSA"; }
        public String getFormat()              { return null; }
        public byte[] getEncoded()             { return null; }
    }
    
        
// Key material retrieval methods
//...........................................................................

    public RSAPublicKey getRSAPublicKey() {
        return new PGPRSAPublicKey();
    }
    
    public RSAPrivateCrtKey getRSAPrivateCrtKey() {
        return new PGPRSAPrivateCrtKey();
    }

}
