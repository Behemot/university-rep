/* $Id: PGPDSA.java,v 1.4 2005/03/29 11:22:48 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import cryptix.openpgp.util.PGPDSAElGamalSignatureParser;
import cryptix.openpgp.util.PGPMPI;

import java.io.IOException;

import java.math.BigInteger;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAKey;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.DSAPrivateKey;


/**
 * DSA wrapper
 *
 * <p>This class contains the key and has methods to do the signing operations
 * with this key.</p>
 *
 * @author Edwin Woudt            (edwin@cryptix.org)
 * @author Jeroen C. van Gelderen (gelderen@cryptix.org)
 * @version $Revision: 1.4 $
 */
public class PGPDSA implements PGPSigner {
    

// Vars
//...........................................................................

    private BigInteger 
        p = null,  // Param
        q = null,  // Param
        g = null,  // Param
        x = null,  // PrivateKey
        y = null,  // PublicKey
        r = null,  // Signature
        s = null;  // Signature
    
    private Signature sig;
    private MessageDigest md;
    private boolean raw = false;
    

// Constructors
//...........................................................................

    /**
     * General constructor
     */
    public PGPDSA ()
    {
        initSigObject();
    }
    

    /**
     * Constructor for clonePublic
     */
    PGPDSA (BigInteger p, BigInteger q, BigInteger g, BigInteger y)
    {
        initSigObject();

        this.p = p;
        this.q = q;
        this.g = g;
        this.y = y;
    }


    /**
     * Constructor for clonePrivate
     */
    PGPDSA (BigInteger p, BigInteger q, BigInteger g, 
            BigInteger x, BigInteger y) 
    {
        initSigObject();

        this.p = p;
        this.q = q;
        this.g = g;
        this.x = x;
        this.y = y;
    }

    /** Helper method for constructors to construct the Signature object **/
    private void initSigObject()
    {
        // ### FIXME: This code has been disabled, because the RawDSA algorithm
        // ### seems to provide invalid results. 
        /* try {
            sig = Signature.getInstance("RawDSA");
            raw = true;
        } catch(NoSuchAlgorithmException nsae) {
            // ignore, try the regular DSA
        } */

        try {
            sig = Signature.getInstance("DSA");
        } catch(NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new RuntimeException("DSA algorithm not found");
        }
    }


// Methods from java.lang.Object
//...........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object obj) {
    
        if (obj instanceof PGPDSA) {
        
            PGPDSA objx = (PGPDSA)obj;
            
            return objx.equals(p, q, g, x, y);
        
        } else {
        
            return false;
            
        }
        
    }
    
    
    /**
     * Compare help method.
     */
    /* package */ boolean equals(BigInteger p, BigInteger q, BigInteger g,
                                 BigInteger x, BigInteger y)
    {

        return compare(this.p,p)
            && compare(this.q,q)
            && compare(this.g,g)
            && compare(this.x,x)
            && compare(this.y,y);

    }
    
    /**
     * Compare help method.
     */
    private boolean compare(BigInteger a, BigInteger b) {
    
        if (a == null) {
            return (b == null);
        } else {
            if (b == null) return false;
            return a.equals(b);
        }
        
    }
    

    /**
     * Returns the hashcode for this object.
     */
    public int hashCode() {
    
        int hash = 0;

        if (p != null) hash ^= p.hashCode();
        if (q != null) hash ^= q.hashCode();
        if (g != null) hash ^= g.hashCode();
        if (x != null) hash ^= x.hashCode();
        if (y != null) hash ^= y.hashCode();

        return hash;
        
    }
    
    
    /**
     * Return a string representing the algorithm.
     */
    public String toString() { return "DSA"; }



// Signature compare methods
//...........................................................................

    /** 
     * Compare signatures
     */
    public boolean signatureEquals(PGPSigner obj) {

        if (obj instanceof PGPDSA) {
        
            PGPDSA objx = (PGPDSA)obj;
            
            return objx.signatureEquals(r, s);
        
        } else {
        
            return false;
            
        }
        
    }


    /**
     * Compare help method.
     */
    /* package */ boolean signatureEquals(BigInteger r, BigInteger s)
    {

        return compare(this.r,r)
            && compare(this.s,s);

    }
    


// Methods from PGPPublicKeyAlgorithm
//...........................................................................

    /**
     * Read the public part of the key from a DataInput
     */
    public void decodePublicData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        p = in.readMPI();
        q = in.readMPI();
        g = in.readMPI();
        y = in.readMPI();
    }
    

    /**
     * Read the secret part of the key from a DataInput
     */
    public void decodeSecretData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        x = in.readMPI();
    }
    

    /**
     * Write the public part of the key to a DataOutput
     */
    public void encodePublicData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(p);
        out.writeMPI(q);
        out.writeMPI(g);
        out.writeMPI(y);
    }
    

    /**
     * Write the secret part of the key to a DataOutput
     */
    public void encodeSecretData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(x);
    }
    

    /**
     * Forget the secret data
     *
     * <p>If no secret data is present, nothing happens.</p>
     *
     * <p>Note that this method cannot really make sure the secret data is not
     * present anymore in system memory and/or system swap space.</p>
     */
    public void forgetSecretData() {
        x = null;
    }
    
    
    /**
     * Generate a keypair for a given keysize.
     *
     * <p>Use writePublicData and writeSecretData to save the key
     */
    public void generateKeyPair(int keysize, SecureRandom sr) {
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("DSA");
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new RuntimeException("Cannot find DSA implementation");
        }
        kpg.initialize(keysize,sr);
        KeyPair kp = kpg.generateKeyPair();
        p = ((DSAPublicKey) kp.getPublic() ).getParams().getP();
        q = ((DSAPublicKey) kp.getPublic() ).getParams().getQ();
        g = ((DSAPublicKey) kp.getPublic() ).getParams().getG();
        y = ((DSAPublicKey) kp.getPublic() ).getY();
        x = ((DSAPrivateKey)kp.getPrivate()).getX();
    }

    
    /**
     * Return the bitlength of the contained key.
     */
    public int getBitLength() { return p.bitLength(); }


    /**
     * Create a copy of this object containing only the Public parts
     * of the key.
     */
    public PGPPublicKeyAlgorithm clonePublic() {
        return new PGPDSA(p,q,g,y);
    }


    /**
     * Create a copy of this object containing the Public & Private parts
     * of the key.
     *
     * <p>The clone will not contain any state with regard to encrypted data
     * and signatures.</p>
     */
    public PGPPublicKeyAlgorithm clonePrivate() {
        return new PGPDSA(p,q,g,x,y);
    }


// Methods from PGPSigner
//...........................................................................

    /** 
     * Initializes for signing
     *
     * <p>Use update to add data and computeSignature to compute the signature
     */
    public void initSign(int hashID, PGPAlgorithmFactory factory) {
        try {
            if (raw) {
                try {
                    md = factory.getHashAlgorithm(hashID);
                } catch (NoSuchAlgorithmException nsae) {
                    nsae.printStackTrace();
                    throw new RuntimeException("Hash #"+hashID+" not found.");
                }
            } else {
                if (! (factory.getHashAlgorithmString(hashID).equals("SHA-1")
                    || factory.getHashAlgorithmString(hashID).equals("SHA1")))
                {
                    throw new IllegalArgumentException("Your provider only " +
                        "supports DSA signatures with a SHA-1 hash. "+
                        "DSA + " + factory.getHashAlgorithmString(hashID) +
                        " will only work if you use a provider that supports " +
                        "the RawDSA signature algorithm, like for example " +
                        "Cryptix JCE");
                }
            }
            if(x == null) {
            	throw new IllegalStateException("This is not a secret key");
            }
            sig.initSign(new PGPDSAPrivateKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        }
    }
    

    /** 
     * Initializes for verifying
     *
     * <p>Use update to add data and verifySignature to verify the signature
     */
    public void initVerify(int hashID, PGPAlgorithmFactory factory) {
        try {
            if (raw) {
                try {
                    md = factory.getHashAlgorithm(hashID);
                } catch (NoSuchAlgorithmException nsae) {
                    nsae.printStackTrace();
                    throw new RuntimeException("Hash #"+hashID+" not found.");
                }
            } else {
                if (! (factory.getHashAlgorithmString(hashID).equals("SHA-1")
                    || factory.getHashAlgorithmString(hashID).equals("SHA1")))
                {
                    throw new IllegalArgumentException("Your provider only " +
                        "supports DSA signatures with a SHA-1 hash. "+
                        "DSA + " + factory.getHashAlgorithmString(hashID) +
                        " will only work if you use a provider that supports " +
                        "the RawDSA signature algorithm, like for example " +
                        "Cryptix JCE");
                }
            }
            sig.initVerify(new PGPDSAPublicKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        }
    }
    
    
    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data) {
        try {
            if (raw) {
                md.update(data);
            } else {
                sig.update(data);
            }
        } catch (SignatureException se) {
            throw new IllegalStateException("SignatureException");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    

    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data, int offset, int len) {
        try {
            if (raw) {
                md.update(data, offset, len);
            } else {
                sig.update(data, offset, len);
            }
        } catch (SignatureException se) {
            throw new IllegalStateException("SignatureException");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    

    /**
     * Updates a single byte of data to be verified or signed
     */
    public void update(byte data) {
        byte[] temp = new byte[1];
        temp[0] = data;
        update(temp);
    }

    
    /**
     * Calculate the signature
     *
     * <p>Use writeSignatureData to output it to a stream.
     */
    public void computeSignature() {
        
        byte[] sigdata;
        
        try { 
            if (raw) {
                sig.update(md.digest());
            }
            sigdata = sig.sign();
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
        
        PGPDSAElGamalSignatureParser parser;
        
        try {
            parser = new PGPDSAElGamalSignatureParser(sigdata);
        } catch (PGPDataFormatException dfe) {
            dfe.printStackTrace();
            throw new RuntimeException("PGPDataFormatException on encoding "+
                                       "signature. This usually means that the"+
                                       " JCE DSA output is wrong. - "+dfe);
        }
        
        r = parser.getR();
        s = parser.getS();
    }
    
    
    /**
     * Verify the signature
     *
     * @return true if the signature is correct
     */
    public boolean verifySignature() {

        PGPDSAElGamalSignatureParser parser 
            = new PGPDSAElGamalSignatureParser(r,s);
        byte[] sigdata = parser.getData();

        try {
            if (raw) {
                sig.update(md.digest());
            }
            return sig.verify(sigdata);
        } catch (SignatureException se) {
            throw new IllegalStateException("SignatureException");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    

    /**
     * Read the signature in PGP compatible format
     */
    public void decodeSignatureData(PGPDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        r = in.readMPI();
        s = in.readMPI();

        if ((r == null) || (s == null))
            throw new 
                PGPDataFormatException("Invalid sig, r == null || s == null");
        if ((r.signum() != 1) || (s.signum() != 1))
            throw new 
                PGPDataFormatException("Invalid sig, r <= 0 || s <= 0");
    }
    
    /**
     * Write the signature in PGP compatible format
     */
    public void encodeSignatureData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(r);
        out.writeMPI(s);
    }
    

// Inner key classes
//...........................................................................
    
    private class PGPDSAParams implements DSAParams {
        public BigInteger getP() { return p; }
        public BigInteger getQ() { return q; }
        public BigInteger getG() { return g; }
    }
    
    
    private class PGPDSAKey implements DSAKey, Key {
        public DSAParams getParams() { return new PGPDSAParams(); }
        public String getAlgorithm() { return "DSA"; }
        public String getFormat()    { return null; }
        public byte[] getEncoded()   { return null; }
    }
    
    
    private class PGPDSAPrivateKey extends PGPDSAKey implements DSAPrivateKey {
        public BigInteger getX() { return x; }
    }
    
    
    private class PGPDSAPublicKey extends PGPDSAKey implements DSAPublicKey {
        public BigInteger getY() { return y; }
    }


// Key material retrieval methods
//...........................................................................

    public DSAPublicKey getDSAPublicKey() {
        return new PGPDSAPublicKey();
    }
    
    public DSAPrivateKey getDSAPrivateKey() {
        return new PGPDSAPrivateKey();
    }

}
