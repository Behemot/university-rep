/* $Id: PGPZlib.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


/**
 * This implements the ZLIB (RFC 1950) compression algorithm.  RFC
 * 2440 (openpgp) defines this algorithm as type "2".  
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $ 
 */
public class PGPZlib extends PGPDeflate {


    public PGPZlib() {
        super(false);
    }

}
