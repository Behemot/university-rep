/* $Id: PGPCompressor.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.io.PGPCompressorInputStream;
import cryptix.openpgp.io.PGPCompressorOutputStream;

import java.io.InputStream;
import java.io.OutputStream;


/**
 * The PGPCompressor offers users an interface to compression and
 * expansion streams.  The actual compression routines used depend
 * on the implementation.
 *
 * <p>In addition to the methods listed in this interface, conforming
 * implementations must have a no-argument constructor (this is
 * required by the PGPAlgorithmFactory).
 * </p>
 *
 * @see cryptix.openpgp.algorithm.PGPAlgorithmFactory
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com) 
 * @version $Revision: 1.2 $ 
 */
public interface PGPCompressor {


  /**
   * Get a compression stream that compresses data as it is written.
   *
   * @param out the output stream to write the compressed data to.
   * @return an output stream that compresses as it writes.
   */
  PGPCompressorOutputStream getCompressionStream(OutputStream out);


  /** 
   * Get an expansion stream that expands data as it is read.
   *
   * @param in an input stream that contains compressed data.
   * @return an input stream that knows how to expand the data.
   */
  PGPCompressorInputStream getExpansionStream(InputStream in);



  boolean needsDummy();
  
}
