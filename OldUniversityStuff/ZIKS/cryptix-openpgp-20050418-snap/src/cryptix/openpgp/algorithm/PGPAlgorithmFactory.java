/* $Id: PGPAlgorithmFactory.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import java.util.Hashtable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;


/**
 * Factory for public key, string-to-key, compression, hash and cipher
 * algorithms.
 *
 * <p>In a normal PGP application, the programmer probably does not have to
 * deal with this class that much. The only thing he or she has to do is
 * initialize it with the default values and just use the get methods to
 * translate an algorithm ID into an implementation for that algorithm. 
 * The default implementation provides values for all algorithms that are 
 * mentioned in RFC2440.</p>
 *
 * <p>It's real strength however is it's ability to register additional 
 * algorithms. For example, once the AES has been chosen (and the maintainer
 * is a bit lazy in adding it), you could just use the registerCipherAlgorithm
 * method to add it yourself.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public final class PGPAlgorithmFactory {


// Constants
//...........................................................................

    /** Initialize with all default algorithms registered */
    public static final int INIT_DEFAULT          = 0x00;
    /** Initialize without any algorithms registered */
    public static final int INIT_EMPTY_ALL        = 0xFF;
    /** Initialize without any public key algorithms registered */
    public static final int INIT_EMPTY_PUBLICKEY  = 0x01;
    /** Initialize without any cipher algorithms registered */
    public static final int INIT_EMPTY_CIPHER     = 0x02;
    /** Initialize without any hash algorithms registered */
    public static final int INIT_EMPTY_HASH       = 0x04;
    /** Initialize without any compression algorithms registered */
    public static final int INIT_EMPTY_COMPRESSOR = 0x08;
    /** Initialize without any string-to-key algorithms registered */
    public static final int INIT_EMPTY_S2K        = 0x10;



// Vars
//...........................................................................

    private Hashtable
        PublicKeyClasses     = new Hashtable(),
        PublicKeyNames       = new Hashtable(),
        CompressionClasses   = new Hashtable(),
        S2KClasses           = new Hashtable(),
        HashStrings          = new Hashtable(),
        HashTextNames        = new Hashtable(),
        HashTextNamesReverse = new Hashtable(),
        CipherStrings        = new Hashtable(),
        CipherKeySizes       = new Hashtable(),
        CipherBlockSizes     = new Hashtable();



// Default data
//...........................................................................

    private static final int[] defaultPublicKeyIDs = {
        1, 2, 3, 16, 17, 20
    };
    private static final String[] defaultPublicKeyClasses = {
        "cryptix.openpgp.algorithm.PGPRSA",
        "cryptix.openpgp.algorithm.PGPRSA",
        "cryptix.openpgp.algorithm.PGPRSA",
        "cryptix.openpgp.algorithm.PGPElGamal",
        "cryptix.openpgp.algorithm.PGPDSA",
        "cryptix.openpgp.algorithm.PGPElGamal"
    };
    private static final String[] defaultPublicKeyNames = {
        "RSA", "RSA", "RSA", "ElGamal", "DSA", "ElGamal" };


    private static final int[] defaultCompressionIDs = {
        0, 1, 2
    };
    private static final String[] defaultCompressionClasses = {
        "cryptix.openpgp.algorithm.PGPUncompressed",
        "cryptix.openpgp.algorithm.PGPZip",
        "cryptix.openpgp.algorithm.PGPZlib"
    };


    private static final int[] defaultS2KIDs = {
        0, 1, 3
    };
    private static final String[] defaultS2KClasses = {
        "cryptix.openpgp.algorithm.PGPSimpleS2K",
        "cryptix.openpgp.algorithm.PGPSaltedS2K",
        "cryptix.openpgp.algorithm.PGPIteratedAndSaltedS2K"
    };


    private static final int[] defaultHashIDs = {
            1,       2,           3,        4,     5,       6,       7 };
    private static final String[] defaultHashStrings = {
        "MD5", "SHA1", "RIPEMD160", "DW SHA", "MD2", "Tiger", "HAVAL" };
    private static final String[] defaultHashTextNames = {
        "MD5", "SHA1", "RIPEMD160", null, "MD2", "TIGER192", "HAVAL-5-160" };


    private static final int[] defaultCipherIDs = {
          1,   2,   3,   4,   5,   7,   8,   9,  10 };
    private static final int[] defaultCipherKeySizes = {
        128, 192, 128, 128, 128, 128, 192, 256, 256 };
    private static final int[] defaultCipherBlockSizes = {
         64,  64,  64,  64,  64, 128, 128, 128, 128 };
    private static final String[] defaultCipherStrings = {
        "IDEA", "TripleDES", "CAST5", "Blowfish", "SAFER",  // 1-5
        "Rijndael", "Rijndael", "Rijndael", "Twofish" };    // 7-10



// Constructors
//...........................................................................

    /**
     * Constructor that initializes all default classes.
     */
    public PGPAlgorithmFactory () {
        this(INIT_DEFAULT);
    }


    /**
     * Constructor that initializes specific portions.
     *
     * <p>The parameter init specifies which algorithms will be registered and
     * which not. Use the constants that start with INIT_ to specify which
     * algorithms should be registerd. For example</p>
     *
     * <pre>// Initialize all default algorithms:
     * factory = new PGPAlgorithmFactory(PGPAlgorithmFactory.INIT_DEFAULT);
     * // Initialize without any algorithms registered:
     * factory = new PGPAlgorithmFactory(PGPAlgorithmFactory.INIT_EMPTY_ALL);
     * // Initialize with the default compression and s2k algorithms registered
     * // but not he ciphers, hashes and public key algorithms.
     * factory = new PGPAlgorithmFactory(
     *                           PGPAlgorithmFactory.INIT_EMPTY_PUBLICKEY &
     *                           PGPAlgorithmFactory.INIT_EMPTY_CIPHER &
     *                           PGPAlgorithmFactory.INIT_EMPTY_HASH);
     * </pre>
     */
    public PGPAlgorithmFactory (int init) {

        if ((init & INIT_EMPTY_PUBLICKEY)  == 0)
            DefaultPublicKeyAlgorithms();

        if ((init & INIT_EMPTY_COMPRESSOR) == 0)
            DefaultCompressionAlgorithms();

        if ((init & INIT_EMPTY_S2K)        == 0)
            DefaultS2KAlgorithms();

        if ((init & INIT_EMPTY_HASH)       == 0)
            DefaultHashAlgorithms();

        if ((init & INIT_EMPTY_CIPHER)     == 0)
            DefaultCipherAlgorithms();

    }



// Constructor help methods
//...........................................................................

    private void DefaultPublicKeyAlgorithms() {
        for (int i=0; i<defaultPublicKeyIDs.length; i++) {
            registerPublicKeyAlgorithm(defaultPublicKeyIDs[i],
                                       defaultPublicKeyClasses[i],
                                       defaultPublicKeyNames[i]);
        }
    }

    private void DefaultCompressionAlgorithms() {
        for (int i=0; i<defaultCompressionIDs.length; i++) {
            registerCompressionAlgorithm(defaultCompressionIDs[i],
                                         defaultCompressionClasses[i]);
        }
    }

    private void DefaultS2KAlgorithms() {
        for (int i=0; i<defaultS2KIDs.length; i++) {
            registerS2KAlgorithm(defaultS2KIDs[i],
                                 defaultS2KClasses[i]);
        }
    }

    private void DefaultHashAlgorithms() {
        for (int i=0; i<defaultHashIDs.length; i++) {
            registerHashAlgorithm(defaultHashIDs[i],
                                  defaultHashStrings[i],
                                  defaultHashTextNames[i]);
        }
    }

    private void DefaultCipherAlgorithms() {
        for (int i=0; i<defaultCipherIDs.length; i++) {
            registerCipherAlgorithm(defaultCipherIDs[i],
                                    defaultCipherStrings[i],
                                    defaultCipherKeySizes[i],
                                    defaultCipherBlockSizes[i]);
        }
    }



// Default instance method
//...........................................................................

    /** Cached default instance */
    private static PGPAlgorithmFactory defaultInstance = null;


    /**
     * Return the default instance of this factory.
     *
     * <p>Useful when you don't need to do any customization.</p>
     * <p>This method returns an instance that has been constructed using the
     * INIT_DEFAULT parameter.</p>
     */
    public static PGPAlgorithmFactory getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = new PGPAlgorithmFactory(INIT_DEFAULT);
        }
        return defaultInstance;
    }



// Register
//...........................................................................

    /** 
     * Register a new public key algorithm
     *
     * <p>If an algorithm is known by multiple ID's, just call this method
     * multiple times with the same classname.</p>
     *
     * @param id the algorithm ID (0-255) this algorithm is known by
     * @param classname the classname that implements the PGPPublicKeyAlgorithm
     *                  interface
     */
    public void registerPublicKeyAlgorithm(int id, String classname, 
                                           String name) 
    {
        PublicKeyClasses.put(new Integer(id),classname);
        PublicKeyNames.put(new Integer(id),name);
    }


    /** 
     * Register a new compression algorithm
     *
     * <p>If an algorithm is known by multiple ID's, just call this method
     * multiple times with the same classname.</p>
     *
     * @param id the algorithm ID (0-255) this algorithm is known by
     * @param classname the classname that implements the PGPCompressor
     *                  interface
     */
    public void registerCompressionAlgorithm(int id, String classname) {
        CompressionClasses.put(new Integer(id),classname);
    }


    /** 
     * Register a new string-to-key algorithm
     *
     * <p>If an algorithm is known by multiple ID's, just call this method
     * multiple times with the same classname.</p>
     *
     * @param id the algorithm ID (0-255) this algorithm is known by
     * @param classname the classname that implements the PGPStringToKey
     *                  interface
     */
    public void registerS2KAlgorithm(int id, String classname) {
        S2KClasses.put(new Integer(id),classname);
    }


    /** 
     * Register a new hash algorithm
     *
     * <p>If an algorithm is known by multiple ID's, just call this method
     * multiple times with the same classname.</p>
     *
     * @param id the algorithm ID (0-255) this algorithm is known by
     * @param jcename the name under which it is known in the JCE
     * @param textname the name under which it is known in RFC 2440
     *        (e.g. for use in the Hash: header for cleartext signed messages)
     */
    public void registerHashAlgorithm(int id, String jcename, String textname) {
        HashStrings.put(new Integer(id),jcename);
        if (textname != null) {
            HashTextNames.put(new Integer(id),textname);
            HashTextNamesReverse.put(textname,new Integer(id));
        }
    }


    /** 
     * Register a new cipher algorithm
     *
     * <p>If an algorithm is known by multiple ID's, just call this method
     * multiple times with the same classname.</p>
     *
     * @param id the algorithm ID (0-255) this algorithm is known by
     * @param name the name under which it is known in the JCE
     * @param keysize the keysize in bits for this cipher
     */
    public void registerCipherAlgorithm(int id, String name, 
                                        int keysize, int blocksize) 
    {
        CipherStrings.put(new Integer(id),name);
        CipherKeySizes.put(new Integer(id),new Integer(keysize));
        CipherBlockSizes.put(new Integer(id),new Integer(blocksize));
    }



// Unregister
//...........................................................................


    /**
     * Unregister an already registered public key algorithm.
     *
     * <p>Does nothing if this algorithm wasn't registered in the first place.
     * </p>
     *
     * @param id the algorithm ID (0-255) for the algorithm to be unregistered
     */
    public void unregisterPublicKeyAlgorithm(int id) {
        PublicKeyClasses.remove(new Integer(id));
        PublicKeyNames.remove(new Integer(id));
    }


    /**
     * Unregister an already registered compression algorithm.
     *
     * <p>Does nothing if this algorithm wasn't registered in the first place.
     * </p>
     *
     * @param id the algorithm ID (0-255) for the algorithm to be unregistered
     */
    public void unregisterCompressionAlgorithm(int id) {
        CompressionClasses.remove(new Integer(id));
    }


    /**
     * Unregister an already registered string-to-key algorithm.
     *
     * <p>Does nothing if this algorithm wasn't registered in the first place.
     * </p>
     *
     * @param id the algorithm ID (0-255) for the algorithm to be unregistered
     */
    public void unregisterS2KAlgorithm(int id) {
        S2KClasses.remove(new Integer(id));
    }


    /**
     * Unregister an already registered hash algorithm.
     *
     * <p>Does nothing if this algorithm wasn't registered in the first place.
     * </p>
     *
     * @param id the algorithm ID (0-255) for the algorithm to be unregistered
     */
    public void unregisterHashAlgorithm(int id) {
        HashStrings.remove(new Integer(id));
    }


    /**
     * Unregister an already registered cipher algorithm.
     *
     * <p>Does nothing if this algorithm wasn't registered in the first place.
     * </p>
     *
     * @param id the algorithm ID (0-255) for the algorithm to be unregistered
     */
    public void unregisterCipherAlgorithm(int id) {
        CipherStrings.remove(new Integer(id));
        CipherKeySizes.remove(new Integer(id));
        CipherBlockSizes.remove(new Integer(id));
    }



// Get methods
//...........................................................................


    /**
     * Get the public key algorithm for the specific id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     * @throws RuntimeException if the algorithm is registered, but the class 
     *         implementing the algorithm cannot be loaded for some reason.
     */
    public PGPPublicKeyAlgorithm getPublicKeyAlgorithm(int id) 
        throws NoSuchAlgorithmException
    {

        try {

            String classname = (String)PublicKeyClasses.get(new Integer(id));
            Class cl = Class.forName(classname);
            PGPPublicKeyAlgorithm pka = (PGPPublicKeyAlgorithm)cl.newInstance();
            return pka;

        } catch (ClassNotFoundException cnfe) {

            throw new RuntimeException("Publickey algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (InstantiationException ie) {

            throw new RuntimeException("Publickey algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (IllegalAccessException iae) {

            throw new RuntimeException("Publickey algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("Publickey algorithm #"+id+
                " not found");
        }

    }
    

    /**
     * Get the name of the public key algorithm for the specific id
     * 
     * This name is used in the return values of getAlgorithm() methods.
     *
     * @return returns the name of the public key algorithm or
     *         <code>null</code> if the id wasn't registered or the algorithm
     *         has no textname.
     */
    public String getPublicKeyName(int id) {
        
        return (String)PublicKeyNames.get(new Integer(id));
    }
    

    /**
     * Get the compression algorithm for the specific id
     *
     * @throws RuntimeException if the class implementing the algorithm cannot
     *         be loaded for some reason or if there is no algorithm registered
     *         for this ID.
     */
    public PGPCompressor getCompressionAlgorithm(int id) 
        throws NoSuchAlgorithmException
    {

        try {

            String classname = (String)CompressionClasses.get(new Integer(id));
            Class cl = Class.forName(classname);
            PGPCompressor comp = (PGPCompressor)cl.newInstance();
            return comp;

        } catch (ClassNotFoundException cnfe) {

            throw new RuntimeException("Compression algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (InstantiationException ie) {

            throw new RuntimeException("Compression algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (IllegalAccessException iae) {

            throw new RuntimeException("Compression algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("Compression algorithm #"+id+
                " not found");
        }

    }
    

    /**
     * Get the string-to-key algorithm for the specific id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     * @throws RuntimeException if the algorithm is registered, but the class 
     *         implementing the algorithm cannot be loaded for some reason.
     */
    public PGPStringToKey getS2KAlgorithm(int id) 
        throws NoSuchAlgorithmException
    {

        try {

            String classname = (String)S2KClasses.get(new Integer(id));
            Class cl = Class.forName(classname);
            PGPStringToKey s2k = (PGPStringToKey)cl.newInstance();
            return s2k;

        } catch (ClassNotFoundException cnfe) {

            throw new RuntimeException("String to key algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (InstantiationException ie) {

            throw new RuntimeException("String to key algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (IllegalAccessException iae) {

            throw new RuntimeException("String to key algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("String to key algorithm #"+id+
                " not found");
        }

    }
    

    /**
     * Get the hash algorithm for the specific id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     * @throws RuntimeException if the algorithm is registered, but the class 
     *         implementing the algorithm cannot be loaded for some reason.
     */
    public MessageDigest getHashAlgorithm(int id) 
        throws NoSuchAlgorithmException
    {
        try {
            
            String name = (String)HashStrings.get(new Integer(id));

            try { // try the cryptix provider first
                MessageDigest md = MessageDigest.getInstance(name, "CryptixCrypto");
                return md;
            } catch (NoSuchAlgorithmException nsae) {
            } catch (NoSuchProviderException nspe) {
            }

            // if that doesn't work: any other provider is fine
            MessageDigest md = MessageDigest.getInstance(name);
            return md;

        } catch (NoSuchAlgorithmException nsae) {

            throw new RuntimeException("Hash algorithm #"+id+
                " cannot be loaded. This could be a provider problem.");

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("Hash algorithm #"+id+
                " not found");
        }
            
    }
    
    /**
     * Get the name of the hashing algorithm for the specific id
     * 
     * @return returns the name of the hashing algorithm and <code>null</code>
     *         if the id wasn't registered.
     */
    public String getHashAlgorithmString(int id) {
        
        return (String)HashStrings.get(new Integer(id));
    }
    
    /**
     * Get the RFC2440 textname of the hashing algorithm for the specific id
     * 
     * This textname is used in the 'Hash:' header of clearsigned messages.
     *
     * @return returns the RFC2440 textname of the hashing algorithm and
     *         <code>null</code> if the id wasn't registered or the algorithm
     *         has no textname.
     */
    public String getHashTextName(int id) {
        
        return (String)HashTextNames.get(new Integer(id));
    }
    
    /**
     * Get the hash algorithm id for a RFC2440 textname of a hashing algorithm
     * 
     * This textname is used in the 'Hash:' header of clearsigned messages.
     *
     * @param str An RFC 2440 text name, e.g. "SHA1", "MD5", etc... See section
     *        9.4 of RFC 2440 for a complete list.
     * @return returns the RFC2440 textname of the hashing algorithm and
     *         <code>null</code> if the id wasn't registered or the algorithm
     *         has no textname.
     */
    public int getHashAlgorithmIDFromTextName(String str) {
        
        return ((Integer)HashTextNamesReverse.get(str)).intValue();
    }
    
    /**
     * Get the cipher algorithm for the specific id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     * @throws RuntimeException if the algorithm is registered, but the class 
     *         implementing the algorithm cannot be loaded for some reason.
     */
    public Cipher getCipherAlgorithm(int id, String mode) 
        throws NoSuchAlgorithmException
    {
        
        String name = (String)CipherStrings.get(new Integer(id));
        if ((name == null) || ("".equals(name))) {
            throw new NoSuchAlgorithmException("Cipher #"+id+" unknown");
        }
        
        String alg = name + "/" + mode + "/NoPadding";
        Provider[] providers = Security.getProviders();


        // Try the Cryptix provider first        
        try {
            Cipher cp = Cipher.getInstance(alg, "CryptixCrypto");
            return cp;
        } catch (NoSuchPaddingException nspe) {
        } catch (NoSuchAlgorithmException nsae) {
        } catch (NoSuchProviderException nspe) {
        }

        // iterate over all providers until one is found that supports the
        // algorithm/mode/padding combination we need
        for (int i=0; i<providers.length; i++) {
            try {
                Cipher cp = Cipher.getInstance(alg, providers[i].getName());
                return cp;
            } catch (NoSuchPaddingException nspe) {
            } catch (NoSuchAlgorithmException nsae) {
            } catch (NoSuchProviderException nspe) {
                throw new InternalError("Provider list inconsistency");
            }
        }

        throw new NoSuchAlgorithmException("Cipher #"+id+" not found");
    } 


    /**
     * Get the cipher key size for the specified id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     */
    public int getCipherKeySize(int id) 
        throws NoSuchAlgorithmException
    {

        try {

            Integer size = (Integer)CipherKeySizes.get(new Integer(id));
            return size.intValue() / 8;

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("Cipher algorithm #"+id+
                " not found");
        }

    }


    /**
     * Get the cipher block size for the specified id
     *
     * @throws NoSuchAlgorithmException if there is no algorithm registered
     *         for this ID.
     */
    public int getCipherBlockSize(int id) 
        throws NoSuchAlgorithmException
    {

        try {

            Integer size = (Integer)CipherBlockSizes.get(new Integer(id));
            return size.intValue() / 8;

        } catch (NullPointerException npe) {

            throw new NoSuchAlgorithmException("Cipher algorithm #"+id+
                " not found");
        }

    }


}
