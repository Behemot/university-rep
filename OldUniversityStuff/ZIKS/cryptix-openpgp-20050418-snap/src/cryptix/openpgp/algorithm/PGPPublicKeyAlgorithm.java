/* $Id: PGPPublicKeyAlgorithm.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import java.io.IOException;

import java.security.SecureRandom;


/**
 * Generic interface that contains shared methods between both signing and
 * encrypting algorithms.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public interface PGPPublicKeyAlgorithm {
    

// Methods from java.lang.Object
//...........................................................................


    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    boolean equals(Object x);
    

    /**
     * Returns the hashcode for this object.
     */
    int hashCode();
    
    
    /**
     * Return a short string for the algorithm (e.g. "RSA", "DSA" or "ElGamal");
     */
    String toString();
    


// Own methods
//...........................................................................


    /**
     * Read the public part of the key from a DataInput
     */
    void decodePublicData(PGPDataInputStream in)
    throws IOException, PGPDataFormatException, PGPFatalDataFormatException;
    

    /**
     * Read the secret part of the key from a DataInput
     */
    void decodeSecretData(PGPDataInputStream in)
    throws IOException, PGPDataFormatException, PGPFatalDataFormatException;


    /**
     * Write the public part of the key to a DataOutput
     */
    void encodePublicData(PGPDataOutputStream out) 
    throws IOException;


    /**
     * Write the secret part of the key to a DataOutput
     */
    void encodeSecretData(PGPDataOutputStream out) 
    throws IOException;
    
    
    /**
     * Forget the secret data
     *
     * <p>If no secret data is present, nothing happens.</p>
     *
     * <p>Note that this method cannot really make sure the secret data is not
     * present anymore in system memory and/or system swap space.</p>
     */
    void forgetSecretData();
    
    
    /**
     * Generate a keypair for a given keysize.
     *
     * <p>Use writePublicData and writeSecretData to save the key
     */
    void generateKeyPair(int keysize, SecureRandom sr);
    
    
    /**
     * Return the bitLength of the contained key.
     */
    int getBitLength();
     
     
    /**
     * Create a copy of this object containing only the Public parts
     * of the key.
     */
    PGPPublicKeyAlgorithm clonePublic();
    

    /**
     * Create a copy of this object containing the Public & Private parts
     * of the key.
     *
     * <p>The clone will not contain any state with regard to encrypted data
     * and signatures.</p>
     */
    PGPPublicKeyAlgorithm clonePrivate();
    

}
