/* $Id: PGPIteratedAndSaltedS2K.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.SecureRandom;


/**
 * Simple S2K - Directly hashes the string to produce key data
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public class PGPIteratedAndSaltedS2K implements PGPStringToKey {


    
    /**
     * Converts the passphrase into a key of size keysize bytes.
     *
     * <p>Optional salt and other information may be read from <i>in</i>.
     * A subclass should not read the S2K specifier octet, nor the hash
     * algorithm id.
     *
     * @param passphrase byte array of the passphrase
     * @param md hash to use
     * @param keysize number of bytes to produce
     * @param in the DataOutput to read salt and other optional data from
     * @return a key of keysize bytes
     */
    public byte[] readAndHash(byte[] passphrase, MessageDigest md, int keysize, 
                              DataInput in) throws IOException {

        byte[] salt = new byte[8];
        in.readFully(salt);
        int count = in.readUnsignedByte();
        
        return PGPIteratedAndSaltedS2K.toKey
                                      (passphrase, md, keysize, salt, count, 3);
    }
                            

    /**
     * Generates salt and counts and then converts the passphrase into a key.
     *
     * <p>A subclass should only write the salt and optional other data to out,
     * not the S2K specifier octet, nor the hash algorithm id.
     *
     * @param passphrase byte array of the passphrase
     * @param md hash to use
     * @param keysize number of bytes to produce
     * @param out the DataOutput to write salt and other optional data to
     * @param sr random generator used to generate the data
     * @return a key of keysize bytes
     */
    public byte[] generateAndHash(byte[] passphrase, MessageDigest md, 
                                  int keysize, DataOutput out, 
                                  SecureRandom sr) throws IOException {
    
        byte[] salt = new byte[8];
        sr.nextBytes(salt);
        out.write(salt);
        int count = 0x96; // 65536
        out.write(count);
        
        return PGPIteratedAndSaltedS2K.toKey
                                      (passphrase, md, keysize, salt, count, 3);
    }


    /**
     * Returns true, because this string-to-key algorithm does use salt.
     */
    public boolean isSalted() { return true; }

    
    /**
     * Method used by PGPSimpleS2K, PSPSaltedS2K and PGPIteratedAndSaltedS2K.
     *
     * This is the core method that contains all code for all three S2K
     * specifiers
     */
    static byte[] toKey(byte[] passphrase, MessageDigest md, int keysize, 
                        byte[] salt, int countbyte, int specifier) 
                        throws IOException {

        int count;
        if (specifier == 3) {
            count = (16 + (countbyte & 15)) << ((countbyte >> 4) + 6);
        } else {
            count = 0;
        }
        
        byte[] result = new byte[keysize];
        int pos = 0;

        for (int pass=0; pos < keysize; pass++) {

            md.reset();

            int done = 0;
            for (int j=0; j<pass; j++) md.update((byte)0);

            if (count < (passphrase.length + salt.length)) 
                count = passphrase.length + salt.length;
            
            while (count - done > passphrase.length + salt.length) {
                if (salt.length > 0) md.update(salt);
                md.update(passphrase);
                done = done + passphrase.length + salt.length;
            }
            for (int j=0; j<salt.length; j++) {
                if (done < count) { md.update(salt[j]); done++; }
            }
            for (int j=0; j<passphrase.length; j++) {
                if (done < count) { md.update(passphrase[j]); done++; }
            }
                
            byte[] hash = md.digest();
            
            int size=hash.length;
            if (pos+size > keysize) size = keysize-pos;
            
            System.arraycopy(hash,0,result,pos,size);
            
            pos += size;
            
        }
            
        return result;
    }

}
