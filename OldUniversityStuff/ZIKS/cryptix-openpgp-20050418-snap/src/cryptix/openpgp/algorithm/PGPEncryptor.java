/* $Id: PGPEncryptor.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import java.io.IOException;

import java.security.SecureRandom;


/**
 * Generic interface to be implemented for each algorithm that supports
 * encryption.
 *
 * <p>For example: RSA and ElGamal.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @author Erwin van der Koogh (erwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
 
public interface PGPEncryptor extends PGPPublicKeyAlgorithm
{
    /** 
     * Encrypts the byte[].
     *
     * @param byte[] data The data to encrypt.
     */
    void encrypt(byte[] data, SecureRandom sr);
    
    /** 
     * Decrypts the byte[]
     *
     * @return byte[] The decrypted data.
     * @throws PGPDecryptionFailedException if the decryption fails (for example
     *         because a wrong key was used).
     */
    byte[] decrypt() throws PGPDecryptionFailedException;
    
    /**
     * Read the encrypted data in PGP compatible format
     */
    void decodeEncryptedData(PGPDataInputStream in)
    throws IOException, PGPDataFormatException, PGPFatalDataFormatException;
    
    
    /** 
     * Write the encrypted in PGP compatible format
     */
    void encodeEncryptedData(PGPDataOutputStream out) throws IOException;
}
