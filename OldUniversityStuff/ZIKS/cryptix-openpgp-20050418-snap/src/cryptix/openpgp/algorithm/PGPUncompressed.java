/* $Id: PGPUncompressed.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.io.PGPCompressorInputStream;
import cryptix.openpgp.io.PGPCompressorOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Implements the "no compression" algorithm.
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $
 */ 
public class PGPUncompressed implements PGPCompressor {



  /*
   * An output stream that does no processing at all.
   */
  private class NopOutputStream extends PGPCompressorOutputStream {

    public NopOutputStream(OutputStream out) {
      super(out);
    }

    protected void writeInternal(byte[] data, int offset, int len) 
      throws IOException
    {
      out.write(data, offset, len);
    }
    
  }// end private class "NopOutputStream"


  /*
   * An input stream that does no processing at all
   */
  private class NopInputStream extends PGPCompressorInputStream {

    public NopInputStream(InputStream in) {
      super(in);
    }

    protected int readInternal(byte[] buffer, int offset, int len) 
      throws IOException
    {
      return(in.read(buffer, offset, len));
    }

  }// end private class "NopInputStream"





// PGPCompressor interface
//...........................................................................



  public PGPCompressorOutputStream getCompressionStream(OutputStream out) {
    return(new NopOutputStream(out));
  }


  public PGPCompressorInputStream getExpansionStream(InputStream in) {
    return(new NopInputStream(in));
  }

    
  public boolean needsDummy() {
     return false;
  }


}
