/* $Id: PGPSigner.java,v 1.3 2005/03/29 11:22:48 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import cryptix.openpgp.util.PGPUpdateable;

import java.io.IOException;


/**
 * Generic interface to be implemented for each algorithm that supports signing.
 * For example: DSA, RSA and ElGamal.
 * 
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public interface PGPSigner extends PGPPublicKeyAlgorithm, PGPUpdateable {


    /** 
     * Compare signatures
     */
    boolean signatureEquals(PGPSigner obj);
    

    /** 
     * Initializes for signing
     *
     * <p>Use update to add data and computeSignature to compute the signature
     */
    void initSign(int hashID, PGPAlgorithmFactory factory);


    /** 
     * Initializes for verifying
     *
     * <p>Use update to add data and verifySignature to verify the signature
     */
    void initVerify(int hashID, PGPAlgorithmFactory factory);
    
    
    /**
     * Updates the data to be verified or signed
     */
    void update(byte[] data);
    
    
    /**
     * Updates the data to be verified or signed
     */
    void update(byte[] data, int offset, int len);
    
    
    /**
     * Calculate the signature
     *
     * <p>Use writeSignatureData to output it to a stream.
     */
    void computeSignature();
    
    
    /**
     * Verify the signature
     *
     * @return true if the signature is correct
     */
    boolean verifySignature();
    
    
    /**
     * Read the signature in PGP compatible format
     */
    void decodeSignatureData(PGPDataInputStream in)
    throws IOException, PGPDataFormatException, PGPFatalDataFormatException;
    
    
    /** 
     * Write the signature in PGP compatible format
     */
    void encodeSignatureData(PGPDataOutputStream out) 
    throws IOException;

}
