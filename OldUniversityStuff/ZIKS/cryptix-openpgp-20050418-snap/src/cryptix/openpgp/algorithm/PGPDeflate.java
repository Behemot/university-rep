/* $Id: PGPDeflate.java,v 1.3 2005/04/06 05:40:53 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPCompressorInputStream;
import cryptix.openpgp.io.PGPCompressorOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;


/**
 * This is the abstract superclass for compression algorithms based on the 
 * DEFLATE compression algorithm as specified in RFC 1951. 
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $ 
 */
public abstract class PGPDeflate implements PGPCompressor {


// Constants
//.............................................................................

    //
    // Compression values.  It is not clear yet if these would
    // be useful to clients.
    //
    private static final int BEST_SPEED = 1;
    private static final int DEFAULT_COMPRESSION = 3;
    private static final int BEST_COMPRESSION = 9;


// Instance variables
//.............................................................................

    /** Specifies if RFC 1950 wrapping is used */
    private final boolean nowrap;


// Constructor
//.............................................................................

    /**
     * Main constructor
     *
     * @param nowrap specifies whether RFC 1950 wrapping is used
     */
    protected PGPDeflate(boolean nowrap) {
        this.nowrap = nowrap;
    }


// Inner classes
//...........................................................................

    /*
     * An output stream that does ZLib compression
     */
    private class ZlibOut extends PGPCompressorOutputStream {

        public ZlibOut(DeflaterOutputStream out) {
            super(out);
        }

        protected void writeInternal(byte[] data, int offset, int len) 
            throws IOException
        {
            out.write(data, offset, len);
        }

        protected void finishInternal() throws IOException {
            ((DeflaterOutputStream)out).flush();
            ((DeflaterOutputStream)out).finish();
        }
    
    }// end private class "ZlibOut"


    /*
     * An input stream that does Zlib de-compression
     */
    private class ZlibIn extends PGPCompressorInputStream {

        public ZlibIn(InflaterInputStream in) {
            super(in);
        }
    
        protected int readInternal(byte[] buffer, int offset, int len) 
            throws IOException
        {
            return(in.read(buffer, offset, len));
        }

    }// end private class "ZlibIn"



// PGPCompressor interface
//...........................................................................

    public PGPCompressorOutputStream getCompressionStream(OutputStream out) {
        return(new ZlibOut(new DeflaterOutputStream(out, createDeflater())));
    }

    public PGPCompressorInputStream getExpansionStream(InputStream in) {
        return(new ZlibIn(new InflaterInputStream(in, createInflater())));
    }
    
    public boolean needsDummy() {
        return nowrap;
    }


// Private helper methods
//...........................................................................

    private Deflater createDeflater() {
        return(new Deflater(getCompressionLevel(), nowrap));
    }

    private Inflater createInflater() {
        return(new Inflater(nowrap));
    }

    private int getCompressionLevel() {
        return(DEFAULT_COMPRESSION);
    }
    
} 
