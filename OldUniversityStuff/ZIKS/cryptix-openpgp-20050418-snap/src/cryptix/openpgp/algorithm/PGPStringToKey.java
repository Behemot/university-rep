/* $Id: PGPStringToKey.java,v 1.2 2005/03/13 17:46:35 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.SecureRandom;


/**
 * Interface for String-To-Key (S2K) specifiers.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public interface PGPStringToKey {

    
    /**
     * Converts the passphrase into a key of size keysize bytes.
     *
     * <p>Optional salt and other information may be read from <i>in</i>.
     * A subclass should not read the S2K specifier octet, nor the hash
     * algorithm id.
     *
     * @param passphrase byte array of the passphrase
     * @param md hash to use
     * @param keysize number of bytes to produce
     * @param in the DataOutput to read salt and other optional data from
     * @return a key of keysize bytes
     */
    byte[] readAndHash(byte[] passphrase, MessageDigest md, 
                       int keysize, DataInput in) throws IOException;

    /**
     * Generates salt and counts and then converts the passphrase into a key.
     *
     * <p>A subclass should only write the salt and optional other data to out,
     * not the S2K specifier octet, nor the hash algorithm id.
     *
     * @param passphrase byte array of the passphrase
     * @param md hash to use
     * @param keysize number of bytes to produce
     * @param out the DataOutput to write salt and other optional data to
     * @param sr random generator used to generate the data
     * @return a key of keysize bytes
     */
    byte[] generateAndHash(byte[] passphrase, MessageDigest md, int keysize, 
                           DataOutput out, SecureRandom sr) throws IOException;



    /**
     * Return if this string-to-key algorithm includes a salt.
     *
     * <p>This method is used as a security check in situations where a salt is
     * absolutely necessairy.</p>
     */
    boolean isSalted();
    
}
