/* $Id: PGPConstants.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


/**
 * Class containing algorithm constants.
 *
 * <p>The first letters of the constant denote the function: </p>
 * <ul>
 *  <li>ALG - Public-Key algorithms.</li>
 *  <li>CIPH - Ciphers.</li>
 *  <li>COMP - Compression algorithms.</li>
 *  <li>HASH - Hash algorithms.</li>
 *  <li>S2K - String to Key specifiers.</li>
 * </ul>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */
public class PGPConstants {

// Constructor
//.............................................................................

    private PGPConstants() {}   // static vars only, no instance



// Public key algorithms
//.............................................................................

    /** Algorithm identifier for RSA (sign) */
    public static final int ALG_RSA_SIGN        =  1;

    /** Algorithm identifier for RSA (encrypt) */
    public static final int ALG_RSA_ENCRYPT     =  1;

    /** Algorithm identifier for RSA (legacy) */
    public static final int ALG_RSA_LEGACY      =  1;

    /** Algorithm identifier for DSA (sign) */
    public static final int ALG_DSA_SIGN        = 17;

    /** Algorithm identifier for ElGamal (encrypt) */
    public static final int ALG_ELGAMAL_ENCRYPT = 16;
    
    /** Algorithm identifier for ElGamal (sign) */
    public static final int ALG_ELGAMAL_SIGN    = 20;
    


// Ciphers
//.............................................................................

    /** Compression identifier for IDEA */
    public static final int CIPH_IDEA          =  1;

    /** Compression identifier for TripleDES */
    public static final int CIPH_TRIPLEDES     =  2;

    /** Compression identifier for CAST5 */
    public static final int CIPH_CAST5         =  3;

    /** Compression identifier for Blowfish */
    public static final int CIPH_BLOWFISH      =  4;

    /** 
     * Compression identifier for SAFER
     *
     * <p>NOTE: At the time of writing Cryptix JCE did not support this 
     * algorithm, so this may not work.</p>
     */
    public static final int CIPH_SAFER         =  5;

    /** Compression identifier for Twofish */
    public static final int CIPH_TWOFISH       = 10;
   


// Compression algorithms
//.............................................................................

    /** Compression identifier for no compression */
    public static final int COMP_NOCOMPRESSION =  0;

    /** Compression identifier for ZIP */
    public static final int COMP_ZIP           =  1;

    /** Compression identifier for ZLIB */
    public static final int COMP_ZLIB          =  2;
    


// Hash algorithms
//.............................................................................

    /** Hash identifier for MD5 */
    public static final int HASH_MD5           =  1;

    /** Hash identifier for SHA-1 */
    public static final int HASH_SHA1          =  2;

    /** Hash identifier for RIPE-MD/160 */
    public static final int HASH_RIPEMD160     =  3;

    /** 
     * Hash identifier for Double Width SHA 
     *
     * <p>NOTE: At the time of writing Cryptix JCE did not support this 
     * algorithm, so this may not work.</p>
     */
    public static final int HASH_DWSHA         =  4;

    /** Hash identifier for MD2 */
    public static final int HASH_MD2           =  5;

    /** Hash identifier for Tiger */
    public static final int HASH_TIGER192      =  6;

    /** 
     * Hash identifier for HAVAL
     *
     * <p>NOTE: At the time of writing Cryptix JCE did not support this 
     * algorithm, so this may not work.</p>
     */
    public static final int HASH_HAVAL         =  7;
    


// String to Key specifiers
//.............................................................................

    /** String to key specifier for Simple S2K */
    public static final int S2K_SIMPLE         =  0;

    /** String to key specifier for Salted S2K */
    public static final int S2K_SALTED         =  1;

    /** String to key specifier for Iterated and Salted S2K */
    public static final int S2K_ITERATEDSALTED =  3;


}
