/* $Id: PGPElGamal.java,v 1.6 2005/04/05 06:15:46 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.algorithm;


import cryptix.jce.ElGamalKey;
import cryptix.jce.ElGamalParams;
import cryptix.jce.ElGamalPublicKey;
import cryptix.jce.ElGamalPrivateKey;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPDataInputStream;
import cryptix.openpgp.io.PGPDataOutputStream;

import cryptix.openpgp.util.PGPDSAElGamalSignatureParser;
import cryptix.openpgp.util.PGPMPI;

import java.io.IOException;

import java.math.BigInteger;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;



/**
 * ElGamal wrapper
 *
 * <p>This class contains the key and has methods to do the signing and 
 * encryption operations with this key.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @author Erwin van der Koogh (erwin@cryptix.org)
 * @author Jeroen C. van Gelderen (gelderen@cryptix.org)
 */
 
public class PGPElGamal implements PGPSigner, PGPEncryptor {
    
// Vars
//...........................................................................

    private BigInteger 
        p = null,  // Param
        g = null,  // Param
        x = null,  // PrivateKey
        y = null,  // PublicKey
        r = null,  // Signature
        s = null,  // Signature
        a = null,  // Encrypted Message
        b = null;  // Encrypted Message

    private Signature sig;
    
    private Cipher cipher;
    
// Constructor
//...........................................................................

    /**
     * General constructor
     */
    public PGPElGamal () {
        try {
            //### FIXME: Should allow for other providers
            cipher = Cipher.getInstance("ElGamal/ECB/PKCS#1", "CryptixCrypto");
        } catch(NoSuchAlgorithmException nsae) {
            nsae.printStackTrace(System.err);
            throw new RuntimeException("ElGamal algorithm not found." + nsae);
        } catch(NoSuchPaddingException nsp) {
            nsp.printStackTrace();
            throw new RuntimeException("Couldn't use PKCS#1 padding");
        } catch(NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }


    /**
     * Constructor for clonePublic
     */
    PGPElGamal (BigInteger p, BigInteger g, BigInteger y) {

        this();
        this.p = p;
        this.g = g;
        this.y = y;

    }


    /**
     * Constructor for clonePrivate
     */
    PGPElGamal (BigInteger p, BigInteger g, BigInteger x, BigInteger y) {

        this();
        this.p = p;
        this.g = g;
        this.x = x;
        this.y = y;

    }



// Methods from java.lang.Object
//...........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object obj) {
    
        if (obj instanceof PGPElGamal) {
        
            PGPElGamal objx = (PGPElGamal)obj;
            return objx.equals(p, g, x, y);
        
        } else {
        
            return false;
            
        }
        
    }
    
    
    /**
     * Compare help method.
     */
    /* package */ boolean equals(BigInteger p, BigInteger g,
                                 BigInteger x, BigInteger y)
    {

        return compare(this.p,p)
            && compare(this.g,g)
            && compare(this.x,x)
            && compare(this.y,y);

    }
    
    /**
     * Compare help method.
     */
    private static boolean compare(BigInteger a, BigInteger b) {
    
        if (a == null) {
            return (b == null);
        } else {
            if (b == null) return false;
            return a.equals(b);
        }
        
    }
    

    /**
     * Returns the hashcode for this object.
     */
    public int hashCode() {
    
        int hash = 0;

        if (p != null) hash ^= p.hashCode();
        if (g != null) hash ^= g.hashCode();
        if (x != null) hash ^= x.hashCode();
        if (y != null) hash ^= y.hashCode();

        return hash;
        
    }


    /**
     * Return a string representing the algorithm.
     */
    public String toString() { return "ElGamal"; }



// Signature compare methods
//...........................................................................

    /** 
     * Compare signatures
     */
    public boolean signatureEquals(PGPSigner obj) {

        if (obj instanceof PGPElGamal) {
        
            PGPElGamal objx = (PGPElGamal)obj;
            
            return objx.signatureEquals(r, s);
        
        } else {
        
            return false;
            
        }
        
    }


    /**
     * Compare help method.
     */
    /* package */ boolean signatureEquals(BigInteger r, BigInteger s)
    {

        return compare(this.r,r)
            && compare(this.s,s);

    }
    


// Methods from PGPPublicKeyAlgorithm
//...........................................................................

    /**
     * Read the public part of the key from a DataInput
     */
    public void decodePublicData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        p = in.readMPI();
        g = in.readMPI();
        y = in.readMPI();
    }
    

    /**
     * Read the secret part of the key from a DataInput
     */
    public void decodeSecretData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        x = in.readMPI();
    }
    

    /**
     * Write the public part of the key to a DataOutput
     */
    public void encodePublicData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(p);
        out.writeMPI(g);
        out.writeMPI(y);
    }
    

    /**
     * Write the secret part of the key to a DataOutput
     */
    public void encodeSecretData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(x);
    }
    

    /**
     * Forget the secret data
     *
     * <p>If no secret data is present, nothing happens.</p>
     *
     * <p>Note that this method cannot really make sure the secret data is not
     * present anymore in system memory and/or system swap space.</p>
     */
    public void forgetSecretData() {
        x = null;
    }
    
    
    /**
     * Generate a keypair for a given keysize.
     *
     * <p>Use writePublicData and writeSecretData to save the key
     */
    public void generateKeyPair(int keysize, SecureRandom sr) {
        KeyPairGenerator kpg;
        try {
            //### FIXME: Should allow for other providers
            kpg = KeyPairGenerator.getInstance("ElGamal", "CryptixCrypto");
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new RuntimeException("Cannot find ElGamal implementation");
        } catch (NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
        kpg.initialize(keysize,sr);
        KeyPair kp = kpg.generateKeyPair();
        p = ((ElGamalPublicKey) kp.getPublic() ).getParams().getP();
        g = ((ElGamalPublicKey) kp.getPublic() ).getParams().getG();
        y = ((ElGamalPublicKey) kp.getPublic() ).getY();
        x = ((ElGamalPrivateKey)kp.getPrivate()).getX();
        
        // ### As soon as we decide to support ElGamal signatures: do an extra 
        // ### check here to see if this key is vulnerable
        // ### to the attack on ElGamal signatures by Bleichenbacher. 
        // ### 
        // ### Reason: it looks to me as if the current routine used by
        // ### NAI's PGP generates keys that are vulnerable (Sophie Germain
        // ### primes with 2 as generator are always vulnerable). This is not a 
        // ### problem, as NAI's PGP does not support ElGamal signatures and
        // ### the attack only works on signatures, but we should be careful
        // ### as I suspect there will be more keypair generators with the same
        // ### flaw, including those in JCE's other than Cryptix JCE.
        // ###
        // ### Online version of the paper can be found at:
        // ### http://www.bell-labs.com/user/bleichen/bib.html
    }


    /**
     * Return the bitlength of the contained key.
     */
    public int getBitLength() { return p.bitLength(); }


    /**
     * Create a copy of this object containing only the Public parts
     * of the key.
     */
    public PGPPublicKeyAlgorithm clonePublic() {
        return new PGPElGamal(p,g,y);
    }


    /**
     * Create a copy of this object containing the Public & Private parts
     * of the key.
     *
     * <p>The clone will not contain any state with regard to encrypted data
     * and signatures.</p>
     */
    public PGPPublicKeyAlgorithm clonePrivate() {
        return new PGPElGamal(p,g,x,y);
    }



// Methods from PGPSigner
//...........................................................................

    /** 
     * Initializes for signing
     *
     * <p>Use update to add data and computeSignature to compute the signature
     */
    public void initSign(int hashID, PGPAlgorithmFactory factory) {
        try {
            String h = factory.getHashAlgorithmString(hashID);
            //### FIXME: Should allow for other providers
            sig = Signature.getInstance(h + "withElGamal", "CryptixCrypto");
            sig.initSign(new PGPElGamalPrivateKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new RuntimeException("ElGamal algorithm not found.");
        } catch (NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    

    /** 
     * Initializes for verifying
     *
     * <p>Use update to add data and verifySignature to verify the signature
     */
    public void initVerify(int hashID, PGPAlgorithmFactory factory) {
        try {
            String h = factory.getHashAlgorithmString(hashID);
            //### FIXME: Should allow for other providers
            sig = Signature.getInstance(h + "withElGamal", "CryptixCrypto");
            sig.initVerify(new PGPElGamalPublicKey());
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException("InvalidKeyException");
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new RuntimeException("ElGamal algorithm not found.");
        } catch (NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    
    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data) {
        try {
            sig.update(data);
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    
    
    /**
     * Updates the data to be verified or signed
     */
    public void update(byte[] data, int offset, int len) {
        try {
            sig.update(data, offset, len);
        } catch (SignatureException se) {
            throw new IllegalStateException("Signature not initialized");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    
    
    /**
     * Calculate the signature
     *
     * <p>Use writeSignatureData to output it to a stream.
     */
    public void computeSignature() {
    
        throw new RuntimeException("Generating ElGamal signatures is not "+
                                   "supported.");
                                   
//        byte[] sigdata;
//        
//        try { 
//            sigdata = sig.sign();
//        } catch (SignatureException se) {
//            throw new IllegalStateException("Signature not initialized");
//        } catch (NullPointerException npe) {
//            throw new IllegalStateException("Signature not initialized");
//        }
//        
//        PGPDSAElGamalSignatureParser parser;
//        
//        try {
//            parser = new PGPDSAElGamalSignatureParser(sigdata);
//        } catch (PGPDataFormatException dfe) {
//            throw new RuntimeException("PGPDataFormatException on encoding "+
//                                       "signature. This usually means that "+
//                                       "the JCE DSA/ElGamal output is wrong. "+
//                                       "- "+dfe);
//        }
//        
//        r = parser.getR();
//        s = parser.getS();
    }
    
    
    /**
     * Verify the signature
     *
     * @return true if the signature is correct
     */
    public boolean verifySignature() {
        PGPDSAElGamalSignatureParser parser 
            = new PGPDSAElGamalSignatureParser(r,s);
        byte[] sigdata = parser.getData();
        try {
            return sig.verify(sigdata);
        } catch (SignatureException se) {
            throw new IllegalStateException("SignatureException");
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Signature not initialized");
        }
    }
    

    /**
     * Read the signature in PGP compatible format
     */
    public void decodeSignatureData(PGPDataInputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        r = in.readMPI();
        s = in.readMPI();

        if ((r == null) || (s == null))
            throw new 
                PGPDataFormatException("Invalid sig, r == null || s == null");
        if ((r.signum() != 1) || (s.signum() != 1))
            throw new 
                PGPDataFormatException("Invalid sig, r <= 0 || s <= 0");
    }
    
    /**
     * Write the signature in PGP compatible format
     */
    public void encodeSignatureData(PGPDataOutputStream out) 
        throws IOException 
    {
        out.writeMPI(r);
        out.writeMPI(s);
    }
    


// Methods from PGPEncryptor
//...........................................................................

    /** 
     * Encrypts the byte[].
     *
     * @param byte[] data The data to encrypt.
     */
    public void encrypt(byte[] data, SecureRandom sr) {
        try {
            
            //### FIXME: Should allow for other providers
            cipher = Cipher.getInstance("ElGamal/ECB/PKCS#1", "CryptixCrypto");
            cipher.init(Cipher.ENCRYPT_MODE, new PGPElGamalPublicKey(), sr);
                
            byte[] encrypted = cipher.doFinal(data);
            
            int halfsize = encrypted.length >> 1;
            
            byte[] one = new byte[halfsize];
            byte[] two = new byte[halfsize];
            
            System.arraycopy(encrypted,        0, one, 0, halfsize);
            System.arraycopy(encrypted, halfsize, two, 0, halfsize);
            
            a = new BigInteger(1, one );
            b = new BigInteger(1, two );
            
        } catch(SecurityException se) {
            // Braindamaged SUN restrictions
            se.printStackTrace();
            throw new RuntimeException("Not allowed to use ElGamal. " + 
                "Perhaps you need to download the Unlimited Strength " +
                "Jurisdiction Policy Files from the Sun website? " + se);
        } catch(InvalidKeyException ivk) {
            // We construct the keys ourselves, so this shouldn't happen.
            ivk.printStackTrace();
            throw new InternalError("Key was invalid:");
        } catch(IllegalBlockSizeException ibs) {
            // Because we use padding, this should not happen.
            ibs.printStackTrace();
            throw new InternalError("Illegal blocksize");
        } catch(BadPaddingException bp) {
            // This exception is not thrown in encryption mode.
            bp.printStackTrace();
            throw new InternalError("Bad padding");
        } catch(NoSuchAlgorithmException nsae) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsae.printStackTrace();
            throw new RuntimeException("ElGamal algorithm not found. "+
                                       "Provider problem?");
        } catch(NoSuchPaddingException nsp) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsp.printStackTrace();
            throw new RuntimeException("Couldn't use PKCS#1 padding. "+
                                       "Provider problem?");
        } catch (NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    /** 
     * Decrypts the byte[]
     *
     * @return byte[] The decrypted data.
     * @throws PGPDecryptionFailedException if the decryption fails (for example
     *         because a wrong key was used).
     */
    public byte[] decrypt() throws PGPDecryptionFailedException {
        try {
            //### FIXME: Should allow for other providers
            cipher = Cipher.getInstance("ElGamal/ECB/PKCS#1", "CryptixCrypto");
            cipher.init(Cipher.DECRYPT_MODE, new PGPElGamalPrivateKey() );

            /*
             * Our BigIntegers may have too little or too many
             * bytes in it's toByteArray representation. Too little because 
             * superfluous zero bytes are stripped. Too many because toByte[]
             * always emits a sign-bit.
             *
             * We solve this by using toFixedLengByteArray to convert this.m
             * to a byte[] of the correct, fixed size.
             */
            int cipherBlockSize = cipher.getBlockSize() / 2;

            byte[] one = PGPMPI.toFixedLenByteArray(this.a, cipherBlockSize);
            byte[] two = PGPMPI.toFixedLenByteArray(this.b, cipherBlockSize);
            
            byte[] ct = new byte[one.length+two.length];
            System.arraycopy(one, 0, ct,          0, one.length);
            System.arraycopy(two, 0, ct, one.length, two.length);

            return cipher.doFinal( ct );

        } catch(SecurityException se) {
            // Braindamaged SUN restrictions
            se.printStackTrace();
            throw new RuntimeException("Not allowed to use ElGamal. " + 
                "Perhaps you need to download the Unlimited Strength " +
                "Jurisdiction Policy Files from the Sun website? " + se);
        } catch(IllegalArgumentException iae) {
            // PGPMPI.toFixedLenByteArray throws this one when m is negative
            // (won't happen) or when m is larger than cipherBlockSize (this
            // will happen when we used the wrong key and the right key is
            // larger than this one.
            throw new PGPDecryptionFailedException("Data larger than key. "+
                                                   "Wrong key used?");
        } catch(BadPaddingException bp) {
            // Decrypted data is not something that looks like a valid PKCS#1
            // padded block. So obviously the wrong key was used.
            throw new PGPDecryptionFailedException("Decrypted data != PKCS#1. "+
                                                   "Wrong key used?");
        } catch(IllegalBlockSizeException ibs) {
            // The use of the toFixedLenByteArray method makes sure that this
            // error can't happen.
            ibs.printStackTrace();
            throw new InternalError("Illegal blocksize");
        } catch(InvalidKeyException ivk) {
            // We construct the keys ourself, so this should not happen.
            ivk.printStackTrace();
            throw new InternalError("Key was invalid");
        } catch(NoSuchAlgorithmException nsae) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsae.printStackTrace();
            throw new RuntimeException("ElGamal algorithm not found. "+
                                       "Provider problem?");
        } catch(NoSuchPaddingException nsp) {
            // Obviously a provider problem. This should not happen when using
            // Cryptix JCE.
            nsp.printStackTrace();
            throw new RuntimeException("Couldn't use PKCS#1 padding. "+
                                       "Provider problem?");
        } catch(NoSuchProviderException nspe) {
            nspe.printStackTrace();
            throw new RuntimeException("Cryptix JCE provider not found.");
        }
    }
    
    /**
     * Read the encrypted data in PGP compatible format
     */
    public void decodeEncryptedData(PGPDataInputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        a = in.readMPI();
        b = in.readMPI();
    }
    
    
    /** 
     * Write the encrypted in PGP compatible format
     */
    public void encodeEncryptedData(PGPDataOutputStream out) 
        throws IOException
    {
        out.writeMPI(a);
        out.writeMPI(b);
    }


// Inner key classes
//...........................................................................

    private class PGPElGamalParams implements ElGamalParams {
        public BigInteger getP() { return p; }
        public BigInteger getG() { return g; }
        public BigInteger getQ() { return null; } //what's this Q?
    }  
    
    private class PGPElGamalKey implements ElGamalKey, Key {
        public ElGamalParams getParams() { return new PGPElGamalParams(); }
        public String getAlgorithm() { return "ElGamal"; }
        public String getFormat()    { return null; }
        public byte[] getEncoded()   { return null; }
    }
    
    
    private class PGPElGamalPrivateKey extends PGPElGamalKey 
        implements ElGamalPrivateKey
    {
        public BigInteger getX() { return x; }
    }
    
    
    private class PGPElGamalPublicKey extends PGPElGamalKey 
        implements ElGamalPublicKey
    {
        public BigInteger getY() { return y; }
    }


// Key material retrieval methods
//...........................................................................

    public ElGamalPublicKey getElGamalPublicKey() {
        return new PGPElGamalPublicKey();
    }
    
    public ElGamalPrivateKey getElGamalPrivateKey() {
        return new PGPElGamalPrivateKey();
    }

}
