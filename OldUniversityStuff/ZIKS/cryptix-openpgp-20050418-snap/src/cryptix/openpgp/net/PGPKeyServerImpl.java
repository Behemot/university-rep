/* $Id: PGPKeyServerImpl.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.net;


import cryptix.message.MessageException;
import cryptix.message.MessageFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;



/**
 * This class embodies an interface layer between the client-oriented
 * PGPKeyServer API and the individual classes that actually access
 * key servers over the network.  This class needs to be understood by
 * those that want to write code to access a key server, it can be
 * ignored by those who just want to use a keyserver using one of the
 * pre-existing classes.
 *
 * <p>
 * Subclasses of this class know how to access a keyserver and return
 * ascii-armored key data.
 *
 * <p>
 * All subclasses must:
 *
 * <ul>
 * <li>Implement the <code>fetchRaw</code> method.
 * <li>Call the constructor for this class in their constructor.
 * </ul>
 *
 *
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.2 $
 */
public abstract class PGPKeyServerImpl implements PGPKeyServer {


  private MessageFactory mfactory;



  /** 
   * Constructor.  All subclasses must call this constructor.
   *
   * @param format the special string representing the message
   * format.  "OpenPGP", for example.  This value is passed to 
   * the factory method of cryptix.message.MessageFactory.
   *
   * @throws NoSuchAlgorithmException if the format is unknown to the
   * MessageFactory.
   *
   * @see cryptix.message.MessageFactory
   */
  public PGPKeyServerImpl(String format) throws NoSuchAlgorithmException {
    mfactory = MessageFactory.getInstance(format);
  }



  // PGPKeyServer interface
  
  public Collection fetchById(String id) throws 
    KeyServerIOException, MessageException 
  {
    try {
      ArrayList bundles = new ArrayList();
      for(Iterator it = fetchRaw(id); it.hasNext(); ) {
        String ascii = (String) it.next();
        ByteArrayInputStream in = new ByteArrayInputStream(ascii.getBytes());
        Collection list = mfactory.generateMessages(in);
        in.close();
        for(Iterator msgs = list.iterator(); msgs.hasNext(); ) {
          bundles.add(msgs.next());
        }
      }    
      return(bundles);
    }
    catch(IOException e) {
      throw(new MessageException("An I/O error occured during internal message processing.  " + e.getMessage()));
    }
  }


	/**
   * By contract, all subclasses must implement this method.
   *
   * <p>
   * Subclasses are expected to be interfaces to specific key server
   * implementations and each must return keys in a valid
   * ascii-armoured format.  
   *
   * <p>
   * The parameter is some identifing value of a user or a key that
   * the key server can use to do key lookup.
   *
   * <p>
   * The return value must be an Iterator over String objects.  Each
   * string object should contain the ascii-armored key data from some
   * key server.  This ascii data is sent to
   * MessageFactory.generateMessages() for conversion to (probably)
   * PGPKeyBundleMessage objects.
   *
   * <p>
   * This method is targeted at subclasses, clients of key servers
   * should use the PGPKeyServer interface method, "fetchById".
	 *
   *
	 * @param id the key id or user id.
	 *
	 * @return a list of ascii-armored keys as Strings.  If no keys are
	 * found then this will be an empty list (never null).  These are
	 * formatted just as they are returnd from the key server.
	 *
	 * @throws KeyServerIOException if there is an IO Error while
	 * talking to the server.  You will also get this exception if the
	 * server does not like the request for some reason.
   *
   * @see #fetchById(String)
   * @see cryptix.message.MessageFactory
	 */
  public abstract Iterator fetchRaw(String id) throws KeyServerIOException;
  

}// end class
