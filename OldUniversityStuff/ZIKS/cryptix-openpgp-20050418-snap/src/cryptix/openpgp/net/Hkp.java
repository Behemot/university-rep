/* $Id: Hkp.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.openpgp.net;

import cryptix.message.MessageFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;




/**
 * This class looks up keys by some key identifier in a HKP key server.
 * 
 * <p>HKP stands for "Horowitz Key Protocol" which is just a fancy
 * name for a very simple HTTP GET request.  Such as it is, the
 * protocol is documented here:
 *
 * <pre>
 *  http://www.mit.edu/afs/net.mit.edu/project/pks/thesis/paper/thesis.html
 * </pre>
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.2 $
 */
public class Hkp extends PGPKeyServerImpl {

	//
	// The ascii-armoured key delimeters.
	//
	private static final String START_TAG = "-----BEGIN PGP PUBLIC KEY BLOCK-----";
	private static final String END_TAG = "-----END PGP PUBLIC KEY BLOCK-----";


	//
	// The BufferedInputStream that we use strips off linefeeds
	// so we use this as our linefeed character when we recreate
	// the ascii-armoured blocks.
	//
	private static final String CR = "\n";


	private String host;
	private int port;



	/**
	 * Construct the new HKP key fetcher that will fetch keys from the
	 * given server/port using the HTTP protocol.
	 *
	 * @param servername the hostname running the keyserver.
	 * @param port the port that the server is running on.
	 */
	public Hkp(String servername, int port) throws NoSuchAlgorithmException {
    super("OpenPGP");
		host = servername;
		this.port = port;
	}



  // PGPKeyServerImpl interface

  /**
	 * @throws KeyServerIOException if there is an IO Error while
	 * talking to the server.  You will also get this exception if the
	 * server does not like the request for some reason.
	 */
	public Iterator fetchRaw(String id) throws KeyServerIOException {

		List keys = null;

		try {
			URL url = new URL("http", host, port, "/pks/lookup?op=get&search=" +
												URLEncoder.encode(id));

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.connect();
			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				//
				// Well, the server did not return HTTP_OK.  For now
				// just throw an exception and let the user sort it out.
				//
				throw(new KeyServerIOException(host, port, "Server did not return HTTP_OK!" + 
																			 con.getResponseMessage()));
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				keys = parseResponse(in);
				in.close();
			}
			con.disconnect();		
		}
		catch(MalformedURLException e) {
			throw(new KeyServerIOException(host, 
																		 port, 
																		 e.getClass().getName() + ": " + 
																		 e.getMessage()));
		}
		catch(IOException e) {
			throw(new KeyServerIOException(host, 
																		 port, e.getClass().getName() + ": " + 
																		 e.getMessage()));
		}

		if (keys != null) {
			return(keys.iterator());
		} else {
			return(new EmptyIterator());
		}
	}



	// Private helper methods


	/**
	 * Parse the server response and return a list of keys (could be
	 * empty).
	 *
	 * @param in an open stream to the server.
	 * @return the list of keys parsed from the stream.
	 * @throws IOException if one is thrown by the stream.
	 */
	private List parseResponse(BufferedReader in) throws IOException {
		ArrayList keys = new ArrayList();
		String line = null;
		StringBuffer buf = new StringBuffer();
		boolean seekStart = true;
		while((line = in.readLine()) != null) {
			if (seekStart && line.startsWith(START_TAG)) {
				seekStart = false;
				buf.append(line).append(CR);
			}
			else if ((!seekStart) && line.startsWith(END_TAG)) {
				seekStart = true;
				buf.append(line).append(CR);
				keys.add(buf.toString());
				buf.setLength(0);
			}
			else if (!seekStart) {
				buf.append(line).append(CR);
			}
		}
		return(keys);
	}

}//end class





/**
 * An empty iterator expresses an empty list.
 */
class EmptyIterator implements Iterator {
	public boolean hasNext() {
		return(false);
	}

	public Object next() throws NoSuchElementException {
		throw(new NoSuchElementException());
	}

	public void remove() throws UnsupportedOperationException {
		throw(new UnsupportedOperationException());
	}
}//end class
