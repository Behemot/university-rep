/* $Id: KeyServerIOException.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.openpgp.net;



/** 
 * A generic exception that is thrown whenever an I/O error occurs
 * while talking to a key server.
 *
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.2 $
 */
public class KeyServerIOException extends Exception {
	

	/**
	 * Gerneate a new exception.
	 *
	 * @param host the keyserver hostname.
	 * @param port the keyserver port number.
	 * @param msg some detail message describing the error.
	 */
	public KeyServerIOException(String host, int port, String msg) {
		super("KeyServer I/O Error for " + host + ":" + port + " { " + msg + " }");
	}

}//end class
