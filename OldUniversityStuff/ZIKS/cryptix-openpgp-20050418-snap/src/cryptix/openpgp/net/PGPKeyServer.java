/* $Id: PGPKeyServer.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.openpgp.net;


import java.util.Collection;
import cryptix.message.MessageException;



/**
 * A generic, client interface to a key server.  A key server allows
 * for keys to be retrieved by some identifying value.
 *
 * <p>
 *
 * <pre>
 *
 *   PGPKeyServer keyserver = new Hkp("keyserver.pgp.com", 80);
 *   try {
 *     Collection keys = keyserver.fetchById("user@cryptix.org");
 *     if (keys.size() > 0) {
 *       for(Iterator i = keys.iterator(); i.hasNext(); ) {
 *          KeyBundleMessage msg = (KeyBundleMessage) i.next();
 *          KeyBundle key = msg.getKeyBundle();
 *          // do something with the key
 *       }
 *     }
 *   }
 *   catch(KeyServerIOException e) {
 *     // handle server down or other low level problem.
 *   }
 *   catch(MessageException e) {
 *     // handle key server message format error.
 *   }
 * 
 * </pre>
 *
 *
 * @author Mathias Kolehmainen (mathiask@computer.org)
 * @version $Revision: 1.2 $
 */
public interface PGPKeyServer {

  /**
   * Lookup and retrieve keys from a keyserver based on a user or
   * key id value.  Returns a collection of KeyBundleMessage objects.
   *
   * @param id the key id or user id.  In many cases the user id is simply
   * the email address associated with the key.
   *
   * @return a collection of Messages (which are likely to be
   * KeyBundleMessage objects) returned from the keyserver.  The
   * collection could be empty.
   *
   * @exception KeyServerIOException if the communication with the key
   * server fails.
   *
   * @exception MessageException if there is a message format exception.
   * To be thrown if the data returned by the key server cannot be parsed
   * by our systems.
   */
  Collection fetchById(String id) throws KeyServerIOException, MessageException;


}
