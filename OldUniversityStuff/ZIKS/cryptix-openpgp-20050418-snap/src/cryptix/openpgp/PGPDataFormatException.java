/* $Id: PGPDataFormatException.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


/**
 * Thrown when incorrect data is found in a PGP object. Unlike it's companion
 * PGPFatalDataFormatException, the behaviour when throwing this exception will
 * be always well defined, such that the offending data can be skipped and 
 * ignored and the application can continue with the next packet.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPDataFormatException extends PGPAbstractDataFormatException {
    

    /**
     * Constructs an exception without a specific error message.
     */
    public PGPDataFormatException () { super(); }


    /**
     * Constructs an exception with the specified error message.
     *
     * @param msg the error message.
     */
    public PGPDataFormatException (String msg) { super(msg); }
    
}
