/* $Id: PGPArmoury.java,v 1.2 2005/03/13 17:47:02 woudt Exp $
 *
 * Copyright (C) 1995-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;


/**
 * Methods for armouring and dearmouring of messages and other objects.
 *
 * @version $Revision: 1.2 $
 * @author  Jill Baker
 * @author  Ian Brown
 * @author  Gary Howland
 * @author  Edwin Woudt            (edwin@cryptix.org)
 * @author  Jeroen C. van Gelderen (gelderen@cryptix.org)
 */
public class PGPArmoury
{

// Class variables    
// ..........................................................................

    public static final String
        TYPE_PUBLIC_KEY = "PGP PUBLIC KEY BLOCK",
        TYPE_SECRET_KEY = "PGP PRIVATE KEY BLOCK";
    

// Instance variables
// ..........................................................................

    /** Headers of the armoured message. */
    private Properties headers;
    private Properties clearheaders;

    /** Clear text of a clearsigned message as a sequence of bytes .*/
    private byte[] clearText;

    /** Signature, or self-contained armoured packet(s) */
    private byte[] payload;
    
    /** The type of the contained information */
    private String type;


// Constructors
// ..........................................................................

    /**
     * Construct a PGP armored message from the given binary data,
     * assuming default headers (version) and default type ("PGP MESSAGE")
     */
    public PGPArmoury( byte[] payload )
    {
        this( getDefaultHeaders(), payload, "PGP MESSAGE" );
    }


    /**
     * Construct a PGP armored message from the given binary data and type,
     * assuming default headers (version)
     */
    public PGPArmoury( byte[] payload, String type )
    {
        this( getDefaultHeaders(), payload, type );
    }


    /**
     * Construct a PGP armored message from the given binary data and
     * headers, assuming the default type "PGP MESSAGE". If no version header 
     * is present the default version header is added.
     */
    public PGPArmoury( Properties headers, byte[] payload )
    {
        this( headers, payload, "PGP MESSAGE" );
    }


    /**
     * Construct a PGP armored message from the given binary data, type and
     * headers. If no version header is present the default version header
     * is added.
     */
    public PGPArmoury( Properties headers, byte[] payload, String type )
    {
        this.headers      = addMissingHeaders(headers);
        this.clearheaders = new Properties();
        this.clearText    = null;
        this.payload      = payload;
        this.type         = type;
    }


    /**
     * Construct a PGP armored message from the given text and signature bytes
     * assuming defaults for the headers.
     */
    public PGPArmoury( byte[] text, byte[] signature )
    throws IllegalArgumentException
    {
        this( getDefaultHeaders(), text, signature );
    }


    /**
     * Construct a PGP armoured message from the given headers, text and 
     * signature bytes. If no version header is present the default version 
     * header is added.
     */
    public PGPArmoury( Properties headers, byte[] clearText, byte[] signature )
    {
        this.headers      = addMissingHeaders(headers);
        this.clearheaders = new Properties();
        this.clearText    = clearText;
        this.payload      = signature;
    }
    

    /**
     * Construct a PGP armoured message from already-armoured data.
     */
    public PGPArmoury( String armouredText ) throws IllegalArgumentException
    {
        headers      = getDefaultHeaders();
        clearheaders = new Properties();
        parse( armouredText );
    }


// Instance methods
// ..........................................................................

    /**
     * Return the headers for this armoured message.
     */
    public Properties getHeaders() { 
        return headers; 
    }
    
    
    /**
     * Return the clear text part of this armoured message.
     */
    public byte[] getClearText() { 
        return clearText; 
    }
    
    
    /**
     * Return the payload of this armoured message.
     */
    public byte[] getPayload() { 
        return payload; 
    }


// ..........................................................................

    // Static methods to armour and de-armour a message (EXcluding the
    // "-----BEGIN PGP MESSAGE-----" (etc.), version, etc. wrappers.
    // Variants of these functions were originally placed in the
    // now deprecated Armoury class.

    public static String armour( byte[] bytes )
    {
        String mainEncodedString = PGPBase64.encode( bytes );

        int crcInt = PGPCRC.checksum( bytes );
        byte crcBytes[] = new byte[3];
        crcBytes[0] = (byte)(crcInt >> 16);
        crcBytes[1] = (byte)(crcInt >>  8);
        crcBytes[2] = (byte)(crcInt      );
        String crcEncodedString = PGPBase64.encode( crcBytes );

        return mainEncodedString + "=" + crcEncodedString;
    }


    public static byte[] unarmour( String s )
    {
        int crcPos = s.indexOf( "\r\n=" );

        String mainEncodedString = s.substring( 0, crcPos );
        byte[] mainBytes = PGPBase64.decode( mainEncodedString );

        String crcEncodedString = s.substring( crcPos + 3, crcPos + 7 );
        byte[] crcBytes = PGPBase64.decode( crcEncodedString );

        int crcInt = ((crcBytes[0] & 0xFF) << 16)
                   | ((crcBytes[1] & 0xFF) <<  8)
                   | ((crcBytes[2] & 0xFF)      );

        int calculatedCRC = PGPCRC.checksum( mainBytes );
        if (crcInt != calculatedCRC)
        {
            throw new IllegalArgumentException( 
                "Armoured Data has incorrect CRC" );
        }

        return mainBytes;
    }


    // Methods to read and write the headers

    private String headersToString(Properties head)
    {
        StringBuffer s = new StringBuffer( 256 );
        Enumeration e = head.propertyNames();
        while (e.hasMoreElements())
        {
            String name  = (String) e.nextElement();
            if (name.length() > 0)
            {
                String value = head.getProperty( name );
                s.append( name );
                s.append( ": " );

                int vPos = s.length();
                s.append( value );
                s.setCharAt( vPos, Character.toUpperCase( value.charAt(0) ));
                s.append( "\r\n" );
            }
        }
        s.append( "\r\n" );
        return new String( s );
    }

    private void stringToHeaders( String s, Properties head )
    throws IllegalArgumentException
    {
        StringTokenizer st = new StringTokenizer( s, "\r\n" );
        while (st.hasMoreTokens())
        {
            String line = st.nextToken();
            int pos = line.indexOf( ": " );
            if (pos == -1)
            {
                throw new IllegalArgumentException( "Header \"" + line
                    + "\" does not conform to PGP standard \"Name: Value\"" );
            }
            String name  = line.substring( 0, pos );
            if (name.length() > 0)
            {
                String value = line.substring( pos + 2 );
                head.put( name.toLowerCase(), value );
            }
        }
    }


    /**
     * Unarmour an armoured message.
     */
    public void parse( String armouredMessage ) throws IllegalArgumentException
    {

        armouredMessage = fixLineEndings(armouredMessage, "\r\n");

        int startPos, msgPos, propPos;
        String propString;

//        // Undo Quoted Printable format if neccessary.
//        while ((armouredMessage.indexOf('=') != -1)
//            && QuotedPrintable.isProbablyEncoded( armouredMessage ))
//        {
//            armouredMessage = QuotedPrintable.decode( armouredMessage );
//        }

        if ((startPos = armouredMessage.indexOf( 
                              "-----BEGIN PGP SIGNED MESSAGE-----\r\n" )) != -1)
        {
            // Get clear text section
            startPos += 36;
            int sigPos = armouredMessage.indexOf( 
                            "\r\n-----BEGIN PGP SIGNATURE-----\r\n", startPos );

            // Now deal with headers
            msgPos = armouredMessage.indexOf( "\r\n\r\n", startPos-2 );
            if (msgPos > startPos) {
                propString = armouredMessage.substring( startPos, msgPos );
                stringToHeaders( propString, clearheaders );
            }

            String text = armouredMessage.substring( msgPos + 4, sigPos );

            text = undashProtect( text );

            try {
                clearText = text.getBytes("UTF-8");
            } catch (UnsupportedEncodingException uee) {
                throw new InternalError("UnsupportedEncodingException");
            }

            propPos = sigPos + 33;

        }
        else if ((startPos = armouredMessage.indexOf( "-----BEGIN " )) != -1)
        {
            propPos = armouredMessage.indexOf( "\r\n", startPos );
            propPos = propPos + 2;
            type = armouredMessage.substring( startPos+11, propPos-7 );
        }
        else
        {
            throw new IllegalArgumentException( 
                                "Armoured Data without recognised -----BEGIN" );
        }

        // Now deal with headers
        msgPos = armouredMessage.indexOf( "\r\n\r\n", propPos );
        propString = armouredMessage.substring( propPos, msgPos );
        stringToHeaders( propString, headers );

        // Get signature
        payload = unarmour( armouredMessage.substring( msgPos + 4 ));
    }

    /**
     * Armour an unarmoured message.
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer( 8192 );
        if (clearText != null)
        {
            String text = "";
            try {
                text = new String(clearText, "UTF-8");
            } catch (UnsupportedEncodingException uee) {
                throw new InternalError("UnsupportedEncodingException");
            }
            
            text = fixLineEndings( text, "\r\n" );
            text = dashProtect( text );

            sb.append( "-----BEGIN PGP SIGNED MESSAGE-----\r\n" );
            
            if (headers.containsKey("Hash")) {
                sb.append( "Hash: "+headers.getProperty("Hash")+"\r\n" );
                headers.remove("Hash");
            }
            
            sb.append( "\r\n" + text );
            sb.append( "\r\n-----BEGIN PGP SIGNATURE-----\r\n" );
            sb.append( headersToString(headers) );
            sb.append( armour( payload ));
            sb.append( "-----END PGP SIGNATURE-----\r\n" );
        }
        else
        {
            sb.append( "-----BEGIN "+type+"-----\r\n" );
            sb.append( headersToString(headers) );
            sb.append( armour( payload ));
            sb.append( "-----END "+type+"-----\r\n" );
        }
        return new String( sb );
    }

    
    /**
     * Fix line endings to a certain format
     *
     * @param txt the text to be reformatted
     * @param eol the line ending to use (e.g. "\r\n")
     */
    public static String fixLineEndings(String txt, String eol) {
    
        char[] result = new char[txt.length() * 2];
        int pos=0;
        
        for (int i=0; i<txt.length(); i++) {
            
            char c = txt.charAt(i);
            if (c == '\r') {
                for (int j=0; j<eol.length(); j++) {
                    result[pos] = eol.charAt(j);
                    pos++;
                }
                if (i+1 < txt.length()) {
                    if (txt.charAt(i+1) == '\n') { i++; }
                }
            } else if (c == '\n') {
                for (int j=0; j<eol.length(); j++) {
                    result[pos] = eol.charAt(j);
                    pos++;
                }
                if (i+1 < txt.length()) {
                    if (txt.charAt(i+1) == '\r') { i++; }
                }
            } else {
                result[pos] = c;
                pos++;
            }
            
        }
        
        return new String(result, 0, pos);
    
    }
    

    /**
     * Remove trailing whitespace and replace all line endings with \r\n
     * @param s text to be encoded
     * @return canonicalized text
     */
    public static String canonicalize(String s)
    {
        int pos = 0;
        String result = "";
        
        while (pos < s.length()) {
            int r = s.indexOf("\r",pos); if (r == -1) r = s.length();
            int n = s.indexOf("\n",pos); if (n == -1) n = s.length(); 
            
            if (r<n) {
                result += removeTrailingWhitespace(s.substring(pos,r)) + "\r\n";
                pos = r+1;
                if (n == r+1) pos++;
            } else {
                result += removeTrailingWhitespace(s.substring(pos,n)) + "\r\n";
                pos = n+1;
                if (r == n+1) pos++;
            }
            
        }
        
        return result;
        
    }

    /**
     * Remove trailing whitespace
     * @param s text to be encoded
     * @return text with trailing whitespace removed
     */
    static String removeTrailingWhitespace(String s)
    {
        String result = s;
        if (result.length() > 0) {
            while ((result.substring(result.length()-1).equals("\t")) || 
                   (result.substring(result.length()-1).equals(" "))) {
                result = result.substring(0,result.length()-1);
                if (result.length() == 0) break;
            }
        }
        return result;
    }

    // dash protecting methods

    /**
     * Prefix all vulnerable lines with "- ".
     * @param s text to be encoded
     * @return dash-proteced text
     */
    public static String dashProtect(String s)
    {
        int startPos = 0;
        int len = s.length();
        StringBuffer buffer = new StringBuffer(2 * len);

        while (startPos < len)
        {
            int nextPos = s.indexOf("\r\n", startPos) + 2;
            if (nextPos == 1) nextPos = len;

            if ((s.startsWith("-",     startPos))
             || (s.startsWith("From ", startPos))
             || (s.startsWith(".\r\n", startPos)))
            {
                buffer.append("- ");
            }
            buffer.append(s.substring(startPos, nextPos));
            startPos = nextPos;
        }
        return buffer.toString();
    }

    /**
     * Remove all "- "s from the start of lines.
     *
     * @param s dash-proteced text to be decoded
     * @return text with dash-protection removed
     */
    public static String undashProtect(String s)
    {
        int startPos = (s.startsWith("- ")) ? 2 : 0;
        int len = s.length();
        StringBuffer buffer = new StringBuffer(len);

        while (startPos < len)
        {
            int nextPos = s.indexOf("\r\n- ", startPos);
            if (nextPos == -1)
            {
                buffer.append(s.substring(startPos));
                startPos = len;
            }
            else
            {
                buffer.append(s.substring(startPos, nextPos + 2));
                startPos = nextPos + 4;
            }
        }
        return buffer.toString();
    }
    
    
    /**
     * Given a set of headers, add the missing ones (version).
     */
    private static Properties addMissingHeaders(Properties headers) {
        // Add version header if not present.
        String version = 
            headers.getProperty("Version", PGPVersion.VERSION_STRING);
        headers.put( "Version", version );
        return headers;
    }
    
    
    /**
     * Construct a default set of headers (version).
     */
    private static Properties getDefaultHeaders() {
        return addMissingHeaders( new Properties() );
    }
    

// Main method    
//...........................................................................

    /**
     * Static main method to armour and dearmour files.
     */
    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
          
            System.out.println("Armour:   PGPArmoury BinFile AscFile "+
                               "Identifier");
            System.out.println("Example:  PGPArmoury key.pgp key.asc PGP "+
                               "PUBLIC KEY");
            System.out.println();
            System.out.println("Unarmour: PGPArmoury AscFile BinFile");
            System.out.println("Example:  PGPArmoury key.asc key.pgp");
            
        } else if (args.length == 2) {  // unarmour
            
            FileInputStream fis = new FileInputStream(args[0]);
            DataInputStream dis = new DataInputStream(fis);
            FileOutputStream fos = new FileOutputStream(args[1]);
            DataOutputStream dos = new DataOutputStream(fos);
            
            byte[] armoured = new byte[fis.available()];
            dis.readFully(armoured);
            String armouredString = new String(armoured,"UTF-8");
            
            PGPArmoury armoury = new PGPArmoury(armouredString);
            
            byte[] unarmoured = armoury.getPayload();
            
            dos.write(unarmoured);
            
        } else {                        // armour

            FileInputStream fis = new FileInputStream(args[0]);
            DataInputStream dis = new DataInputStream(fis);
            FileOutputStream fos = new FileOutputStream(args[1]);
            DataOutputStream dos = new DataOutputStream(fos);
            
            byte[] unarmoured = new byte[fis.available()];
            dis.readFully(unarmoured);
            
            String type = "";
            for (int i=2; i<args.length; i++) {
                if (!type.equals("")) type = type + " ";
                type = type + args[i];
            }
            
            PGPArmoury armoury = new PGPArmoury(unarmoured, type);
            
            String armouredString = armoury.toString();
            byte[] armoured = armouredString.getBytes("UTF-8");
            
            dos.write(armoured);
            
        }
        
    }
    

}
