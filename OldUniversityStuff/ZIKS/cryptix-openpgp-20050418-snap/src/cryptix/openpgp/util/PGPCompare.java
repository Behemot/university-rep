/* $Id: PGPCompare.java,v 1.2 2005/03/13 17:47:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


import java.util.Date;
import java.util.Vector;


/**
 * Convenience methods to compare objects.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPCompare {


// Constructor
//............................................................................

    private PGPCompare() {}   // Static methods only



// Basic compare methods
//............................................................................

    public static boolean equals(long    a, long    b) { return a == b; }
    public static boolean equals(int     a, int     b) { return a == b; }
    public static boolean equals(short   a, short   b) { return a == b; }
    public static boolean equals(byte    a, byte    b) { return a == b; }
    public static boolean equals(boolean a, boolean b) { return a == b; }
    public static boolean equals(float   a, float   b) { return a == b; }
    public static boolean equals(double  a, double  b) { return a == b; }
    public static boolean equals(char    a, char    b) { return a == b; }



// Object compare methods
//............................................................................

    public static boolean equals(String  a, String  b)
    { 
    	if(a == null && b == null) {return true; }
	   	if(a == null || b == null) {return false; }
    	return a.equals(b); 
    }
    public static boolean equals(Date    a, Date    b)
    { 
    	if(a == null && b == null) {return true; }
	   	if(a == null || b == null) {return false; }
    	return a.equals(b);
    }



// Array compare methods
//............................................................................

    public static boolean equals(byte[]  a, byte[]  b)
    {
    	if(a == null && b == null) {return true; }
	   	if(a == null || b == null) {return false; }
        if (a.length != b.length) {
            return false;
        } else {
            boolean equal = true;
            for (int i=0; i<a.length; i++) {
                if (a[i] != b[i]) equal = false;
            }
            return equal;
        }
    }
    
    public static boolean equals(Vector  a, Vector  b)
    {
	   	if(a == null && b == null) {return true; }
	   	if(a == null || b == null) {return false; }
        if (a.size() != b.size()) {
            return false;
        } else {
            boolean equal = true;
            for (int i=0; i<a.size(); i++) {
                if (! a.elementAt(i).equals(b.elementAt(i))) equal = false;
            }
            return equal;
        }
    }
}
