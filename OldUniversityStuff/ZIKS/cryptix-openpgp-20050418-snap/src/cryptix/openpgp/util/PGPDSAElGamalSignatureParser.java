/* $Id: PGPDSAElGamalSignatureParser.java,v 1.2 2005/03/13 17:47:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General Licence along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


import cryptix.openpgp.PGPDataFormatException;

import java.math.BigInteger;


/**
 * Reads and writes DER encoded DSA and ElGamal signatures.
 *
 * <p>DSA and ElGamal signatures are encoded by the JCA as a DER sequence 
 * of two ASN.1 INTEGER values like this:
 * <pre>SEQUENCE { r INTEGER; s INTEGER }</pre>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 */
public class PGPDSAElGamalSignatureParser {


// Instance variables
//............................................................................

    /** Signature values r and s */
    private BigInteger 
        r = null,
        s = null;

    

// Constructors
//............................................................................

    /**
     * Initializes the parser with a DER sequence.
     *
     * Use getR() and getS() to get the decoded numbers.
     *
     * @throws IllegalArgumentException if data is not correctly formatted
     */
    public PGPDSAElGamalSignatureParser(byte[] data) 
        throws PGPDataFormatException
    {
        try {
            int i=0;
    
            if ( (data[i++] != 0x30) || (data[i++] != data.length-2) ||
                 (data[i++] != 0x02) )
                throw new PGPDataFormatException("Corrupted signature data");
    
            byte len=data[i++];
    
            if (len > 21)
                throw new PGPDataFormatException("Corrupted signature data");
            
            byte[] rdata=new byte[len];
            for (int j=0; j<len; j++) {
                rdata[j] = data[i++];
            }

            if (data[i++] != 0x02)
                throw new PGPDataFormatException("Corrupted signature data");
    
            len=data[i++];
    
            if (len > 21)
                throw new PGPDataFormatException("Corrupted signature data");
            
            byte[] sdata=new byte[len];
            for (int j=0; j<len; j++) {
                sdata[j] = data[i++];
            }
            
            r = new BigInteger(rdata);
            s = new BigInteger(sdata);
            
            if ((i != data.length) || (r.signum() != 1) || (s.signum() != 1))
                throw new PGPDataFormatException("Corrupted signature data");

        } catch (NullPointerException npe) {
            throw new PGPDataFormatException("Corrupted signature data");
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            throw new PGPDataFormatException("Corrupted signature data");
        }            
    }
    

    /**
     * Initializes the parser with the two numbers r and s.
     *
     * Use getData() to get the encoded DER sequence.
     *
     * @throws IllegalArgumentException if r or s is null
     */
    public PGPDSAElGamalSignatureParser(BigInteger r, BigInteger s) {
        if ((r == null) || (s == null))
            throw new InternalError("Invalid sig, r == null || s == null");
        if ((r.signum() != 1) || (s.signum() != 1))
            throw new InternalError("Invalid sig, r <= 0 || s <= 0");
        this.r = r;
        this.s = s;
    }
    


// Getters
//............................................................................

    /** Returns the decoded r */
    public BigInteger getR() { return r; }


    /** Returns the decoded s */
    public BigInteger getS() { return s; }


    /**
     * Returns the encoded DER sequence
     */
    public byte[] getData() {
        byte[] rdata = r.toByteArray();
        byte[] sdata = s.toByteArray();
        byte[] data = new byte[6 + rdata.length + sdata.length];
        int i=0;
        data[i++] = 0x30;
        data[i++] = (byte)(data.length-2);
        data[i++] = 0x02;
        data[i++] = (byte)(rdata.length);
        for (int j=0; j<rdata.length; j++) {
            data[i++] = rdata[j];
        }
        data[i++] = 0x02;
        data[i++] = (byte)(sdata.length);
        for (int j=0; j<sdata.length; j++) {
            data[i++] = sdata[j];
        }
        return data;
    }        


}
