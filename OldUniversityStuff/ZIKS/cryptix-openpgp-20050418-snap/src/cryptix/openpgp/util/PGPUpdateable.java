/* $Id: PGPUpdateable.java,v 1.2 2005/03/13 17:47:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


/**
 * Interface for classes that have an 'update' method
 *
 * <p>This class was created, because cryptix.openpgp.io.PGPHashDataOutputStream
 * needs access to the update(..) method of a 
 * cryptix.openpgp.algorithm.PGPSigner class. Because cryptix.openpgp.io is
 * compiled before cryptix.openpgp.algorithm, the PGPSigner class can't be used
 * directly, therefore this interface was created in the .util package, which is
 * always compiled before .io and .algorithm.</p>
 * <p>Changing compiling order would not have worked, because the classes in
 * .algorithm use classes in .io</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public interface PGPUpdateable {

    void update(byte[] data);
    
}
