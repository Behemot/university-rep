/* $Id: PGPHex.java,v 1.3 2005/03/29 11:07:15 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


/**
 * Hex utilities.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPHex {

    private PGPHex() {} // static methods only
    
    private static final char[] digits = 
        {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    public static String toString(byte x) {
        char[] chars = { digits[x>>4 & 0xf], digits[x & 0xf] };
        return new String( chars );
    }
    
    public static String toString(int x) {
        char[] chars = { digits[x>>28 & 0xf], digits[x>>24 & 0xf],
                         digits[x>>20 & 0xf], digits[x>>16 & 0xf],
                         digits[x>>12 & 0xf], digits[x>> 8 & 0xf],
                         digits[x>> 4 & 0xf], digits[x     & 0xf]  };

        return new String( chars );
    }
    
    public static String toString(byte[] x, int off, int len) {

        StringBuffer buf = new StringBuffer( (x.length * 3) + (x.length >> 4) );

        for (int i=0; i<len; i++) {
            buf.append( digits[ x[off+i] >> 4  & 0xf] );
            buf.append( digits[ x[off+i]       & 0xf] );
            buf.append( ' ' );
            if ((i % 16) == 15) buf.append( '\n' );
        }

        return buf.toString();
    }

    public static String toString(byte[] x) {

        return toString(x, 0, x.length);
        
    }

}
