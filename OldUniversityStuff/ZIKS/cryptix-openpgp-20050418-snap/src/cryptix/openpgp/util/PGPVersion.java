/* $Id: PGPVersion.java,v 1.2 2005/03/13 17:47:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


/**
 * Represents the current version. The value in here will be replaced by the
 * build system at compile time.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public final class PGPVersion {

    /**
     * The version
     */
    public static final double VERSION = 0.123456789;

    public static final String VERSION_STRING = "Cryptix OpenPGP " +
                                                 PGPVersion.VERSION;

    /**
     * Prevent constructing a PGPVersion object. Static stuff only.
     */
    private PGPVersion() {}

}
