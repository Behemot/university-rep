/* $Id: PGPMPI.java,v 1.2 2005/03/13 17:47:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.util;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import java.math.BigInteger;


/**
 * Read and write BigIntegers in a PGP compatible format.
 *
 * <p>From <A HREF="http://www.ietf.org/rfc/rfc2440.txt">RFC 2440</A>:</p>
 * <pre>     3.2. Multi-Precision Integers
 *
 *     Multi-Precision Integers (also called MPIs) are unsigned integers
 *     used to hold large integers such as the ones used in cryptographic
 *     calculations.
 *
 *     An MPI consists of two pieces: a two-octet scalar that is the length
 *     of the MPI in bits followed by a string of octets that contain the
 *     actual integer.
 *
 *     These octets form a big-endian number; a big-endian number can be
 *     made into an MPI by prefixing it with the appropriate length.
 *
 *     Examples:
 *
 *     (all numbers are in hexadecimal)
 *
 *     The string of octets [00 01 01] forms an MPI with the value 1. The
 *     string [00 09 01 FF] forms an MPI with the value of 511.
 *
 *     Additional rules:
 *
 *     The size of an MPI is ((MPI.length + 7) / 8) + 2 octets.
 *    
 *     The length field of an MPI describes the length starting from its
 *     most significant non-zero bit. Thus, the MPI [00 02 01] is not formed
 *     correctly. It should be [00 01 01].</pre>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @author Jeroen C. van Gelderen (gelderen@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPMPI {
    
    private PGPMPI() {} //static methods only
    

    /**
     * Read a BigInteger from a stream
     *
     * @param in the DataInput that contains a BigInteger in PGP format
     * @return the BigInteger read
     * @throws IOException if an I/O error occurs
     * @throws EOFException if the end of the stream is reached before a
     *                      complete BigInteger could be read
     */
    public static BigInteger decode(DataInput in) throws IOException {

        int len = in.readUnsignedShort();
        byte[] buf = new byte[(len + 7) / 8];
        in.readFully(buf);
        return new BigInteger(1,buf);

    }
    

    /**
     * Write a BigInteger to a stream
     *
     * @param out the DataOutput to write the BigInteger to
     * @param x the BigInteger to be written
     * @throws IOException if an I/O error occurs
     */
    public static void encode(DataOutput out, BigInteger x) 
        throws IOException 
    {

        out.writeShort(x.bitLength());
        byte[] bytes = x.toByteArray();
        if (bytes[0] == 0) {
            out.write(bytes,1,bytes.length-1);   // remove sign byte
        } else {
            out.write(bytes);
        }

    }


    /**
     * Fit (stretch or shrink) the given positive BigInteger into a 
     * byte[] of resultByteLen bytes without losing precision.
     *
     * @trhows IllegalArgumentException
     *         If x negative, or won't fit in requested number of bytes.
     */
    public static byte[] toFixedLenByteArray(BigInteger x, int resultByteLen) {

        if (x.signum() != 1)
            throw new IllegalArgumentException("BigInteger not positive.");

        byte[] x_bytes = x.toByteArray();
        int x_len = x_bytes.length;

        if (x_len <= 0)
            throw new IllegalArgumentException("BigInteger too small.");

        /*
         * The BigInteger contract specifies that we now have at most one
         * superfluous leading zero byte:
         */
        int x_off = (x_bytes[0] == 0) ? 1 : 0;
        x_len -= x_off;

        /*
         * Check whether the BigInteger will fit in the requested byte length.
         */
        if ( x_len > resultByteLen)
            throw new IllegalArgumentException("BigInteger too large.");

        /*
         * Now stretch or shrink the encoding to fit in resByteLen bytes.
         */
        byte[] res_bytes = new byte[resultByteLen];
        int res_off = resultByteLen-x_len;
        System.arraycopy(x_bytes, x_off, res_bytes, res_off, x_len);
        return res_bytes;
    }
}
