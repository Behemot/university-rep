/* $Id: PGPSelfCertificateParameterBuilder.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPBooleanSP;
import cryptix.openpgp.signature.PGPByteArraySP;
import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyFlagsSP;
import cryptix.openpgp.signature.PGPKeyServerPrefsSP;
import cryptix.openpgp.signature.PGPStringSP;

import cryptix.pki.KeyID;

import java.security.InvalidKeyException;
import java.security.Key;

import java.util.Date;


/**
 * Parameters for a PGP Self Certificate
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class PGPSelfCertificateParameterBuilder 
    extends PGPAbstractSignatureParameterBuilder 
{

    public PGPSelfCertificateParameterBuilder(KeyID issuerkeyid) 
    {
        super(issuerkeyid, PGPSignatureConstants.TYPE_POSITIVE_CERT);
        addDefaultPreferences();
    }

    public PGPSelfCertificateParameterBuilder(Key issuerkey)
        throws InvalidKeyException
    {
        super(issuerkey, PGPSignatureConstants.TYPE_POSITIVE_CERT);
        addDefaultPreferences();
    }

    private void addDefaultPreferences()
    {
        // Default preferences
        //
        // Ciphers
        // 1.  - AES with 128-bit key      7
        // 2.  - AES with 192-bit key      8
        // 3.  - AES with 256-bit key      9
        // 4.  - CAST5                     3
        // 5.  - Triple-DES                2
        // 
        // Hashes
        // 1.  - SHA-1                     2
        // 2.  - RIPE-MD/160               3
        // 
        // Compression
        // 1.  - ZLIB                      2
        // 2.  - ZIP                       1
        // 3.  - Uncompressed              0
        
        byte[] ciph = {7, 8, 9, 3, 2};
        byte[] hash = {2, 3};
        byte[] comp = {2, 1, 0};
        
        PGPByteArraySP ciphprefs = new PGPByteArraySP();
        ciphprefs.setValue(ciph);
        ciphprefs.setPacketID(PGPSignatureConstants.SSP_PREFERRED_SYMMETRIC);
        
        PGPByteArraySP hashprefs = new PGPByteArraySP();
        hashprefs.setValue(hash);
        hashprefs.setPacketID(PGPSignatureConstants.SSP_PREFERRED_HASH);
        
        PGPByteArraySP compprefs = new PGPByteArraySP();
        compprefs.setValue(comp);
        compprefs.setPacketID(PGPSignatureConstants.SSP_PREFERRED_COMPRESSION);
        
        addPacket(ciphprefs);
        addPacket(hashprefs);
        addPacket(compprefs);
    }

    public void setExpirationDate(Date expiration)
    {
        if (expiration == null) {
            removePacket(PGPSignatureConstants.SSP_SIG_EXPIRATION_TIME);
        } else {
            PGPDateSP pkt = new PGPDateSP();
            pkt.setValue(expiration);
            pkt.setPacketID(PGPSignatureConstants.SSP_SIG_EXPIRATION_TIME);
            setPacket(pkt);
        }
    }
    
    public void setKeyExpirationDate(Date expiration)
    {
        if (expiration == null) {
            removePacket(PGPSignatureConstants.SSP_KEY_EXPIRATION_TIME);
        } else {
            PGPDateSP pkt = new PGPDateSP();
            pkt.setValue(expiration);
            pkt.setPacketID(PGPSignatureConstants.SSP_KEY_EXPIRATION_TIME);
            setPacket(pkt);
        }
    }
    
    public void setRevocable(boolean revocable) 
    {
        if (revocable) {
            removePacket(PGPSignatureConstants.SSP_REVOCABLE);
        } else {
            PGPBooleanSP pkt = new PGPBooleanSP();
            pkt.setValue(false);
            pkt.setPacketID(PGPSignatureConstants.SSP_REVOCABLE);
            setPacket(pkt);
        }
    }
    
    public void setPreferredSymmetricAlgorithms(byte[] preferences)
    {
        if (preferences == null) {
            removePacket(PGPSignatureConstants.SSP_PREFERRED_SYMMETRIC);
        } else {
            PGPByteArraySP pkt = new PGPByteArraySP();
            pkt.setValue(preferences);
            pkt.setPacketID(PGPSignatureConstants.SSP_PREFERRED_SYMMETRIC);
            setPacket(pkt);
        }
    }
    
    public void setPreferredHashAlgorithms(byte[] preferences)
    {
        if (preferences == null) {
            removePacket(PGPSignatureConstants.SSP_PREFERRED_HASH);
        } else {
            PGPByteArraySP pkt = new PGPByteArraySP();
            pkt.setValue(preferences);
            pkt.setPacketID(PGPSignatureConstants.SSP_PREFERRED_HASH);
            setPacket(pkt);
        }
    }
    
    public void setPreferredCompressionAlgorithms(byte[] preferences)
    {
        if (preferences == null) {
            removePacket(PGPSignatureConstants.SSP_PREFERRED_COMPRESSION);
        } else {
            PGPByteArraySP pkt = new PGPByteArraySP();
            pkt.setValue(preferences);
            pkt.setPacketID(PGPSignatureConstants.SSP_PREFERRED_COMPRESSION);
            setPacket(pkt);
        }
    }
    
    public void setKeyServerPreferences(boolean nomodify)
    {
        if (! nomodify) {
            removePacket(PGPSignatureConstants.SSP_KEYSERVER_PREFS);
        } else {
            PGPKeyServerPrefsSP pkt = new PGPKeyServerPrefsSP();
            pkt.setNoModify(nomodify);
            pkt.setPacketID(PGPSignatureConstants.SSP_KEYSERVER_PREFS);
            setPacket(pkt);
        }
    }
    
    public void setPreferredKeyServer(String url)
    {
        if (url == null) {
            removePacket(PGPSignatureConstants.SSP_PREFERRED_KEYSERVER);
        } else {
            PGPStringSP pkt = new PGPStringSP();
            pkt.setValue(url);
            pkt.setPacketID(PGPSignatureConstants.SSP_PREFERRED_KEYSERVER);
            setPacket(pkt);
        }
    }
    
    public void setPrimaryUserID(boolean primaryuserid)
    {
        if (! primaryuserid) {
            removePacket(PGPSignatureConstants.SSP_PRIMARY_USERID);
        } else {
            PGPBooleanSP pkt = new PGPBooleanSP();
            pkt.setValue(true);
            pkt.setPacketID(PGPSignatureConstants.SSP_PRIMARY_USERID);
            setPacket(pkt);
        }
    }
    
    public void setKeyFlags(boolean certification, boolean signdata,
                            boolean encryptcommunication, 
                            boolean encryptstorage)
    {
        if (certification && signdata && encryptcommunication && encryptstorage)
        {
            removePacket(PGPSignatureConstants.SSP_KEY_FLAGS);
        } else {
            PGPKeyFlagsSP pkt = new PGPKeyFlagsSP();
            pkt.setCertify(certification);
            pkt.setSign(signdata);
            pkt.setEncryptCommunication(encryptcommunication);
            pkt.setEncryptStorage(encryptstorage);
            pkt.setPacketID(PGPSignatureConstants.SSP_KEY_FLAGS);
            setPacket(pkt);
        }
    }
    
}
