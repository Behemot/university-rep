/* $Id: PGPPrincipal.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPIDPacket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.Principal;


/**
 * OpenPGP Principal.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class PGPPrincipal implements Principal {


// Instance variables
//.............................................................................


    /** The packet that contains the core part of this principal */
    private PGPIDPacket packet;



// Constructor
//.............................................................................


    /** 
     * Constructor that takes a packet 
     *
     * @param pkt the packet that contains the data for this principal.
     */
    protected PGPPrincipal (PGPIDPacket pkt) {
        if (pkt == null) throw new IllegalArgumentException("null argument");
        this.packet = pkt;
    }
    
    

// Own methods
//.............................................................................


    /**
     * Return the encoded representation of this principal.
     */
    public byte[] getEncoded() {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            packet.encode(baos);
            baos.close();
            return baos.toByteArray();

        } catch (IOException e) {
            throw new InternalError("IOException on ByteArrayOutputStream "+e);
        }
    }
    

    /**
     * Return the contained packet 
     *
     * <p>Note: packets are part of the low-level API. Normally you don't need
     * to use this method as other options are available.</p>
     */
    public PGPIDPacket getPacket() {
        return packet;
    }



// Methods from java.security.Principal
//.............................................................................


    /**
     * Compares this principal to another one.
     *
     * <p>
     * Default implementation: principals are considered equal if their packets
     * are equal.
     * </p>
     */
    public boolean equals(Object another) {
        if ( ! (another instanceof PGPPrincipal)) return false;
        PGPPrincipal that = (PGPPrincipal)another;
        return this.packet.equals(that.packet);
    }
    

    /**
     * Returns a hashcode for this principal
     *
     * <p>
     * Default implementation: returns the hashcode of the contained packet.
     * </p>
     */
    public int hashCode() {
        return this.packet.hashCode();
    }
    

    /**
     * Returns the name of this principal as a String.
     *
     * <p>
     * Default implementation: returns the result of the toString() method on
     * the contained packet.
     * </p>
     */
    public String getName() {
        return this.packet.toString();
    }
    

    /**
     * Returns a string representation of this principal.
     *
     * <p>
     * Default implementation: returns the result of the getName() method.
     * </p>
     */
    public String toString() {
        return getName();
    }

}
