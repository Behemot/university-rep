/* $Id: PGPCertificate.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPSignaturePacket;

import cryptix.pki.ExtendedCertificate;
import cryptix.pki.KeyID;

import java.security.cert.CertificateParsingException;

import java.util.Date;
import java.util.Properties;


/**
 * An OpenPGP Certificate.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @author  Ingo Luetkebohle
 * @version $Revision: 1.3 $
 */
public abstract class PGPCertificate extends ExtendedCertificate {


// Constructor
// ..........................................................................

    /**
     * Create a new Certificate object with the given type.
     */
    protected PGPCertificate(String type)
    {
        super(type);
    }



// Added abstract methods
// ..........................................................................


    /**
     * Return the contained signature packet.
     *
     * <p>Note: packets are part of the low-level API. Normally you don't need
     * to use this method as other options are available.</p>
     */
    public abstract PGPSignaturePacket getPacket();


    /**
     * Returns the creation date and time.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 2: 
     * 'signature creation type'.</p>
     */
    public abstract Date getCreationDate()
        throws CertificateParsingException;


    /**
     * Returns the expiration date and time.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 3: 
     * 'signature expiration type'.</p>
     */
    public abstract Date getExpirationDate()
        throws CertificateParsingException;
    

    /** 
     * Returns whether a certification signature is exportable.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 4: 
     * 'exportable certification'.</p>
     *
     * <p>Non-exportable, or "local," certifications are signatures made by a
     * user to mark a key as valid within that user's implementation only.
     * Thus, when an implementation prepares a user's copy of a key for
     * transport to another user (this is the process of "exporting" the
     * key), any local certification signatures are deleted from the key.</p>
     */
    public abstract boolean isExportable()
        throws CertificateParsingException;
    
    
    /** 
     * Returns the trust level
     *
     * <p>Corresponds with OpenPGP signature subpacket type 5: 
     * 'trust signature'.</p>
     *
     * <p>Meaning of the levels: Level 0 has the same meaning as an ordinary
     * validity signature.  Level 1 means that the signed key is asserted
     * to be a valid trusted introducer, with the 2nd octet of the body
     * specifying the degree of trust. Level 2 means that the signed key is
     * asserted to be trusted to issue level 1 trust signatures, i.e. that
     * it is a "meta introducer". Generally, a level n trust signature
     * asserts that a key is trusted to issue level n-1 trust signatures.</p>
     */
    public abstract int getTrustLevel()
        throws CertificateParsingException;
    

    /** 
     * Returns the trust amount
     *
     * <p>Corresponds with OpenPGP signature subpacket type 5: 
     * 'trust signature'.</p>
     * 
     * <p>The trust amount is in a range from 0-255, interpreted such that
     * values less than 120 indicate partial trust and values of 120 or
     * greater indicate complete trust.  The default values used by most
     * applications are 60 for partial trust and 120 for complete trust.</p>
     */
    public abstract int getTrustAmount()
        throws CertificateParsingException;


    /** 
     * Returns a regular expression that limits the scope of trust levels > 0.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 6: 
     * 'regular expression'.</p>
     * 
     * <p>Used in conjunction with trust signature packets (of level > 0) to
     * limit the scope of trust that is extended.  Only signatures by the
     * target key on user IDs that match the regular expression in the body
     * of this packet have trust extended by the trust signature subpacket.</p>
     *
     * <p>Regarding the syntax, here is an integral quote from RFC 2440:</p>
     * <pre>
     * 8. Regular Expressions
     * 
     *    A regular expression is zero or more branches, separated by '|'. It
     *    matches anything that matches one of the branches.
     * 
     *    A branch is zero or more pieces, concatenated. It matches a match
     *    for the first, followed by a match for the second, etc.
     * 
     *    A piece is an atom possibly followed by '*', '+', or '?'. An atom
     *    followed by '*' matches a sequence of 0 or more matches of the atom.
     *    An atom followed by '+' matches a sequence of 1 or more matches of
     *    the atom. An atom followed by '?' matches a match of the atom, or
     *    the null string.
     * 
     *    An atom is a regular expression in parentheses (matching a match for
     *    the regular expression), a range (see below), '.' (matching any
     *    single character), '^' (matching the null string at the beginning of
     *    the input string), '$' (matching the null string at the end of the
     *    input string), a '\' followed by a single character (matching that
     *    character), or a single character with no other significance
     *    (matching that character).
     * 
     *    A range is a sequence of characters enclosed in '[]'. It normally
     *    matches any single character from the sequence. If the sequence
     *    begins with '^', it matches any single character not from the rest
     *    of the sequence. If two characters in the sequence are separated by
     *    '-', this is shorthand for the full list of ASCII characters between
     *    them (e.g. '[0-9]' matches any decimal digit). To include a literal
     *    ']' in the sequence, make it the first character (following a
     *    possible '^').  To include a literal '-', make it the first or last
     *    character.
     * </pre>
     *
     * @return the regular expression or null if no regular expression is
     *          available
     */
    public abstract String getTrustRegularExpression()
        throws CertificateParsingException;


    /**
     * Returns signature's revocability status.  
     *
     * <p>Corresponds with OpenPGP signature subpacket type 7: 
     * 'revocable'.</p>
     * 
     * <p>Returns a boolean flag indicating whether the signature is revocable.
     * Signatures that are not revocable have any later revocation signatures 
     * ignored. They represent a commitment by the signer that he cannot revoke
     * his signature for the life of his key.  </p>
     */
    public abstract boolean isRevocable()
        throws CertificateParsingException;


    /**
     * Returns the keyID of the key issuing the certificate.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 16: 
     * 'issuer key ID'.</p>
     */
    public abstract KeyID getIssuerKeyID()
        throws CertificateParsingException;


    /**
     * Returns the list of machine readable notations on the certification that 
     * the issuer wishes to make.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 20: 
     * 'notation data'.</p>
     *
     * <p>Returned is a Properties object, containing a (possibly empty) set
     * of keys and values. The keys reside in two name spaces: The IETF name 
     * space and the user name space.
     * </p><p>
     * The IETF name space is registered with IANA. These keys will not
     * contain the "@" character (0x40) as this is a tag for the user name
     * space.
     * </p><p>
     * Keys in the user name space consist of a string tag followed
     * by "@" followed by a DNS domain name. For example, the "sample" tag used by
     * Example Corporation could be "sample@example.com".
     * </p>
     */
    public abstract Properties getMachineReadableNotationData()
        throws CertificateParsingException;
    

    /**
     * Returns the list of human readable notations on the certification that 
     * the issuer wishes to make.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 20: 
     * 'notation data'.</p>
     *
     * <p>Returned is a Properties object, containing a (possibly empty) set
     * of keys and values. The keys reside in two name spaces: The IETF name 
     * space and the user name space.
     * </p><p>
     * The IETF name space is registered with IANA. These keys will not
     * contain the "@" character (0x40) as this is a tag for the user name
     * space.
     * </p><p>
     * Keys in the user name space consist of a string tag followed
     * by "@" followed by a DNS domain name. For example, the "sample" tag used by
     * Example Corporation could be "sample@example.com".
     * </p>
     */
    public abstract Properties getHumanReadableNotationData()
        throws CertificateParsingException;
    

    /**
     * Returns a URL of a document that describes the policy that the signature
     * was issued under
     *
     * <p>Corresponds with OpenPGP signature subpacket type 26: 
     * 'policy URL'.</p>
     */
    public abstract String getPolicyURL()
        throws CertificateParsingException;


    /**
     * Returns whether key flags are specified
     *
     * <p>Corresponds with OpenPGP signature subpacket type 27: 
     * 'key flags'.</p>
     *
     * <p>If this method returns true, the application can call one of the
     * other getKeyFlag* methods for the specific key flags.</p>
     */
    public abstract boolean getKeyFlagsSpecified()
        throws CertificateParsingException;


    /**
     * Returns whether the certified key may be used to certify other keys.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public abstract boolean getKeyFlagCertification()
        throws CertificateParsingException;


    /**
     * Returns whether the certified key may be used to sign data.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public abstract boolean getKeyFlagSignData()
        throws CertificateParsingException;


    /**
     * Returns whether the certified key may be used to encrypt communications.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public abstract boolean getKeyFlagEncryptCommunication()
        throws CertificateParsingException;


    /**
     * Returns whether the certified key may be used to encrypt storage.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public abstract boolean getKeyFlagEncryptStorage()
        throws CertificateParsingException;


    /**
     * Returns which user id of the issuer was used to issue this certificate.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 28: 
     * 'signer's user id'.</p>
     *
     * <p>Many keyholders use a single key for different purposes, such as 
     * business communications as well as personal communications. This 
     * subpacket allows such a keyholder to state which of their roles is 
     * making a signature.</p>
     */
    public abstract PGPPrincipal getIssuerUserID()
        throws CertificateParsingException;

}
