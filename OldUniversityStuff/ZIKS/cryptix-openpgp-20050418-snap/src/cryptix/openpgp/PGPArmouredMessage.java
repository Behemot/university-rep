/* $Id: PGPArmouredMessage.java,v 1.5 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.message.EncryptedMessage;
import cryptix.message.KeyBundleMessage;
import cryptix.message.LiteralMessage;
import cryptix.message.Message;
import cryptix.message.MessageException;
import cryptix.message.SignedMessage;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;

import cryptix.openpgp.util.PGPArmoury;

import java.io.UnsupportedEncodingException;

import java.util.Properties;


/**
 * Represents an armoured message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.5 $
 */
public class PGPArmouredMessage extends Message {


// Instance variables
// ..........................................................................

    
    private Message msg;
    private PGPKeyBundle bundle;
    

// Constructors
// ..........................................................................

    /**
     * Creates an ArmouredMessage from another Message
     */
    public PGPArmouredMessage(Message msg) {
        super("OpenPGP");
        this.msg = msg;
        if (msg instanceof KeyBundleMessage)
            this.bundle = (PGPKeyBundle)((KeyBundleMessage)msg).getKeyBundle();
    }
    

    /**
     * Creates an ArmouredMessage from a KeyBundle
     */
    public PGPArmouredMessage(KeyBundle bundle) {
        super("OpenPGP");
        this.bundle = (PGPKeyBundle)bundle;
    }
    

// Methods from Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException
    {
        try {
            return getEncodedString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported - "+uee);
        }
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @return the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
        throw new IllegalArgumentException("Attributes not supported.");
    }


// Own methods
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public String getEncodedString() throws MessageException
    {
        try {

            PGPArmoury armoury;
            if (bundle != null) {

                if (bundle.containsPrivateKey()) {
                    armoury = new PGPArmoury(bundle.getEncoded(), 
                                             PGPArmoury.TYPE_SECRET_KEY);
                } else {
                    armoury = new PGPArmoury(bundle.getEncoded(), 
                                             PGPArmoury.TYPE_PUBLIC_KEY);
                }

            } else {

                if (msg instanceof PGPSignedMessage) {

                    LiteralMessage contents = 
                        (LiteralMessage)((SignedMessage)msg).getContents();

                    if (contents.getDataType() == LiteralMessage.TEXT) {

                        // Cleartext signed message
                        
                        PGPDetachedSignatureMessage dsm = 
                            ((PGPSignedMessage)msg).getDetachedSignature();

                        Properties p = new Properties();
                        
                        if (! ((PGPSignedMessage)msg).isLegacy()) {
                            p.setProperty("Hash", 
                                          (String)dsm.getAttribute("Hash"));
                        }
                        
                        armoury = new PGPArmoury(p, contents.getBinaryData(),
                            dsm.getEncoded());

                    } else {
                    
                        // Signed binary message
                        armoury = new PGPArmoury(msg.getEncoded());
                    
                    }

                } else {
                    // Encrypted or literal message
                    armoury = new PGPArmoury(msg.getEncoded());
                }

            }
            return armoury.toString();

        } catch (KeyBundleException kbe) {
            throw new MessageException(""+kbe);
        }
    }
    

    /**
     * Returns the contents of this message.
     *
     * @return either a Message or a KeyBundle
     */
    public Object getContents() {
        if (msg != null) 
            return msg;
        else
            return bundle;
    }

}
