/* $Id: PGPByteArrayDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.math.BigInteger;



/**
 * Outputstream to write data into a bytearray.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPByteArrayDataOutputStream extends PGPDataOutputStream {


// Instance variables
//............................................................................

    /** The underlying outputstream */
    private ByteArrayOutputStream out;
    


// Constructor
//............................................................................

    /**
     * Constructor that takes an outputstream
     */
    public PGPByteArrayDataOutputStream() {

        out = new ByteArrayOutputStream();

    }
    


// Overridden methods
//............................................................................

    /**
     * Writes one byte into the bytearray
     */
    protected void writeInternal(int b) throws IOException {
    
        out.write(b);
        
    }
    

    /**
     * Close the stream
     */
    public void close() throws IOException {
    
        out.close();
    
    }
    


// Convert to bytearray methods
//............................................................................

    /**
     * Returns the byte array that has been written
     */
    public byte[] toByteArray() {

        return out.toByteArray();

    }
    

}
