/* $Id: PGPSignatureDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.IOException;
import java.io.OutputStream;



/**
 * Outputstream to write PGP formatted (normal) packets
 *
 * <p>This class does support partial encodings</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPSignatureDataOutputStream extends PGPLengthDataOutputStream {


// Instance variables
//............................................................................

    /** The packetID */
    private byte packetID;



// Constructor
//............................................................................

    /**
     * Constructor that takes an outputstream and a packetID
     */
    public PGPSignatureDataOutputStream(OutputStream out, byte packetID) {
        super(out);
        this.packetID = packetID;
    }



// Implementation of abstract mathods
//............................................................................

    /**
     * Not supported method.
     *
     * <p>This method always throws a RuntimeException, because partial lengths
     * are not supported for signature subpackets.</p>
     */
    protected void writePartialLength(OutputStream out, long len)
        throws IOException
    {

        throw new RuntimeException("Partial lengths not supported in "+
                                   "Signature Subpackets.");

    }
    

    /**
     * Write a length to the outputstream.
     *
     * @throws IOException if something goes wrong with the outputstream
     */
    protected void writeLength(OutputStream out, long len)
        throws IOException
    {

        writePacketLength(out, len+1);
        out.write(packetID);

    }


// Internal methods
//............................................................................


    /** Write a length */
    private void writePacketLength(OutputStream out, long len) 
        throws IOException 
    {

        if (len < 0) {
            throw new IllegalArgumentException("Negative length not allowed");
        }

        if (len < 192) {
            out.write((int)len);
        } else if (len < 16320) {
            out.write((int)(((len - 192) >> 8) + 192));
            out.write((int)((len - 192) & 0xFF));
        } else if (len < 4294967296L) {
            out.write(255); 
            out.write((int)((len >> 24)       ));
            out.write((int)((len >> 16) & 0xFF));
            out.write((int)((len >>  8) & 0xFF));
            out.write((int)((len      ) & 0xFF));
        } else {
            throw new IllegalArgumentException("Maximum length exceeded");
        }

    }


}
