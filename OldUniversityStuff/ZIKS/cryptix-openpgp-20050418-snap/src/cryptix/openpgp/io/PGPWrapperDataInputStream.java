/* $Id: PGPWrapperDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.InputStream;
import java.io.IOException;


/**
 * Simple wrapper for java.io.InputStream.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPWrapperDataInputStream extends PGPDataInputStream {


// Instance variables
//............................................................................

    /** The underlying inputstream */
    private InputStream in;
    


// Constructor
//............................................................................

    /**
     * Constructor that takes a java.io.InputStream
     */
    public PGPWrapperDataInputStream(InputStream in) {
        
        this.in = in;
        
    }
    


// Overridden methods
//............................................................................

    /**
     * Read one byte
     *
     * <p>This method simply calls the read method on the underlying 
     * inputstream.</p>
     *
     * @return the byte read
     */
    protected int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        
        return in.read();
        
    }


    /**
     * Close the stream.
     *
     * <p>This method does not close the underlying inputstream.</p>
     */
    public void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {}       


}
