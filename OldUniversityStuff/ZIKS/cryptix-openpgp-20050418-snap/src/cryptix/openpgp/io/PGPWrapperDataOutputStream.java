/* $Id: PGPWrapperDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.OutputStream;
import java.io.IOException;

import java.math.BigInteger;



/**
 * Simple wrapper for java.io.OutputStream.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPWrapperDataOutputStream extends PGPDataOutputStream {


// Instance variables
//............................................................................

    /** The underlying outputstream */
    private OutputStream out;
    


// Constructor
//............................................................................


    /**
     * Constructor that takes a java.io.OutputStream
     */
    public PGPWrapperDataOutputStream(OutputStream out) {

        this.out = out;

    }
    


// Overridden methods
//............................................................................

    /**
     * Write one byte
     *
     * <p>This method simply calls the write method on the underlying 
     * outputstream.</p>
     */
    protected void writeInternal(int b) throws IOException {
    
        out.write(b);
        
    }
    

    /**
     * Close the stream
     *
     * <p>This method does not close the underlying outputstream.</p>
     */
    public void close() throws IOException {}
    

}
