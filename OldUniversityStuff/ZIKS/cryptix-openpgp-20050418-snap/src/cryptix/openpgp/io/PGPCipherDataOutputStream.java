/* $Id: PGPCipherDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.IOException;
import java.io.OutputStream;

import javax.crypto.Cipher;
import javax.crypto.ShortBufferException;


/**
 * Document me
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPCipherDataOutputStream extends PGPLengthDataOutputStream {


// Instance variables
//............................................................................

    /** The packetID */
    private PGPLengthDataOutputStream out;
    /** Buffer where we store the to be encrypted data */
    private byte[] buffer = new byte[4096];
    /** Cipher that will be used to encrypt the data */
    private Cipher c;
    /** The number of bytes written in the current chunk */
    private long chunkBytesWritten = 0;



// Constructor
//............................................................................

    /**
     * Construct a new PGPCipherDataOutputStream
     *
     * <p>The cipher c has to be initialized with the key before calling this
     * method. It also must use a mode where the size of the encrypted data
     * always equals the size of the decrypted data (e.g. CFB or OpenpgpCFB).
     * </p>
     *
     * @param out the underlying outputstream
     * @param c the cipher used to decrypt the data, 
     */
    public PGPCipherDataOutputStream(PGPLengthDataOutputStream out, Cipher c) {

        super();
        this.out = out;
        this.c = c;

    }



// Overridden methods
//............................................................................

    /**
     * Write one byte into the buffer.
     */
    protected void writeDirect(int b) throws IOException {
    
        buffer[(int)chunkBytesWritten] = (byte)b;
        chunkBytesWritten++;
        
        if (chunkBytesWritten == buffer.length) {
            clearBuffer();
        }
        
    }


    /**
     * Close this inputstream
     *
     * <p>This method does not close the underlying outputstream.</p>
     * <p>This method must always be called or otherwise some data will not
     * be written.</p>
     */
    public void close() throws IOException {
    
        clearBuffer();
        
    }
    


// Implementation of abstract methods
//............................................................................

    /**
     * Write a partial length to the outputstream.
     *
     * <p>This method calls setPartialLength on the underlying
     * outputstream.</p>
     *
     * @throws IOException if something goes wrong with the outputstream
     */
    protected void writePartialLength(OutputStream out, long len)
        throws IOException
    {

        clearBuffer();
        this.out.setPartialLength(len);

    }
    

    /**
     * Write a length to the outputstream.
     *
     * <p>This method calls setLength on the underlying outputstream.</p>
     *
     * @throws IOException if something goes wrong with the outputstream
     */
    protected void writeLength(OutputStream out, long len)
        throws IOException
    {

        clearBuffer();
        this.out.setLength(len);

    }
    
    

// Internal methods
//............................................................................


    /**
     * Encrypt the current buffer and write it to the underlying outputstream.
     */
    private void clearBuffer() 
        throws IOException
    {
    
        byte[] output = new byte[(int)chunkBytesWritten];

        try {
            if (c.update(buffer, 0, (int)chunkBytesWritten, output, 0) 
                                                            != buffer.length) {
                throw new RuntimeException("Length of unencrypted data > "+
                                           "length of encrypted data");
            }
        } catch (ShortBufferException sbe) {
            throw new RuntimeException("Length of unencrypted data < length "+
                                       "of encrypted data - "+sbe);
        }

        out.writeFully(buffer);
        chunkBytesWritten = 0;
    
    }

}
