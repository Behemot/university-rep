/* $Id: PGPLengthDataInputStream.java,v 1.3 2005/03/29 11:22:49 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;



/**
 * Generic inputstream to read PGP formatted packet data that contains length
 * information
 *
 * <p>This class contains generic methods for reading from PGP formatted data.
 * It makes sure that no more bytes can be read then there are in the packet.
 * </p>
 * <p>A special feature of this class is that it supports data that is cut into
 * multiple chunks, like the partial length packet bodies that can be used in
 * V4 packets. It will transparently call initNewChunk every time it runs out
 * of bytes, such that the class using this stream will think it deals with one
 * big chunk of data.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */
public abstract class PGPLengthDataInputStream extends PGPDataInputStream {


// Instance variables
//............................................................................

    /** The length of the current chunk */
    private long chunkLength = 0;
    /** The number of bytes already read from the current chunk */
    private long chunkBytesRead = -1;
    /** The inputstream that provides the base input to this stream */
    private InputStream in;
    /** True if nothing has been read yet */
    private boolean nothingRead = true;
    
    

// Constructor
//............................................................................

    /**
     * Constructor that takes an inputstream
     *
     * <p>Subclasses should call this constructor from their own.</p>
     */
    public PGPLengthDataInputStream(InputStream in) {
        this.in = in;
    }


    /**
     * Emtpy constructor for subclasses that handle their own reading.
     *
     * <p>If a subclass decides to use this constructor, then the readDirect
     * method must be overridden.</p>
     */
    public PGPLengthDataInputStream() {
        this.in = null;
    }



// Abstract method
//............................................................................

    /**
     * Initialize a new chunk and return the length
     *
     * <p>If no new chunk is available, then this method should not throw an
     * exception, instead it should return zero.</p>
     *
     * @throws IOException if the underlying inputstream returns an exception
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. If you throw this exception, you
     *                                must read all the remaining data in this
     *                                packet, such that the pointer in the
     *                                underlying inputstream is at the next
     *                                packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    protected abstract long getNewChunkLength(InputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException;



// Internal methods used by public read methods
//............................................................................


    /**
     * Reads one byte directly from the underlying inputstream
     */
    protected int readDirect()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        int result = in.read();

        if (result == -1) {
            throw new PGPFatalDataFormatException("Read tried past end of "+
                                                  "inputstream.");
        }
        
        return result;
        
    }


    /**
     * Reads one byte.
     */
    protected int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        chunkBytesRead++;
        
        if (nothingRead) {
        
            nothingRead = false;
            
            chunkLength = 5; // enough to read the packet length
            chunkBytesRead = 0;
            chunkLength = getNewChunkLength(in);

            if (chunkLength == 0) {
                throw new PGPDataFormatException("Zero length packet.");
            }

            int result = readDirect();
            chunkBytesRead = 1;
            return result;
            
        } else if (chunkLength > chunkBytesRead) {
        
            return readDirect();
            
        } else if (chunkLength == chunkBytesRead) {
        
            int result = readDirect();

            chunkLength = 5; // enough to read the packet length
            chunkBytesRead = 0;
            chunkLength = getNewChunkLength(in);

            chunkBytesRead = 0; // now at start

            return result;
            
        } else { // chunkLength < chunkBytesRead
        
            throw new PGPDataFormatException("Read tried past end of packet.");
            
        }

    }

    


    /**
     * Clean-up and close the stream.
     *
     * <p>This method checks if all bytes have been read, if not, the remaining
     * bytes will be read and a PGPDataFormatException will be thrown. </p>
     *
     * <p>This method does not close the underlying inputstream.</p>
     */
    public void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        if (chunkLength != chunkBytesRead) {

            byte[] buf = new byte[4096];
            while (readBuffer(buf) == 4096) {}
            throw new PGPDataFormatException("Extra data in packet.");
            
        }
    }


// Buffer reading
//............................................................................


    /**
     * Returns how many bytes are available in the current chunk.
     */
    public long available() {

        return chunkLength - chunkBytesRead;

    }



    /**
     * Read a buffer
     *
     * <p>The complete array is considered a buffer, so calling this method is
     * equivalent to readBuffer(b, 0, b.length); </p>
     *
     * <p>If not enough bytes are available to fill the complete array, as much
     * as possible are read and the number of succesfully read bytes is
     * returned. Unlike all other methods it does not throw an exception in this
     * case, so it is safe to use this method to fill a buffer.</p>
     *
     * @return how many bytes are read.
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public int readBuffer(byte[] b)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return readBuffer(b, 0, b.length);

    }
    
    

    /**
     * Read a buffer
     *
     * <p>Reading starts at offset and continues for a maximum of len bytes.</p>
     *
     * <p>If not enough bytes are available to fill the complete array, as much
     * as possible are read and the number of succesfully read bytes is
     * returned. Unlike all other methods it does not throw an exception in this
     * case, so it is safe to use this method to fill a buffer.</p>
     *
     * @return how many bytes are read.
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public int readBuffer(byte[] b, int offset, int len)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        for (int i=offset; i<(len+offset); i++) {
            if (available() > 0) {
                b[i] = (byte)readInternal();
            } else {
                return i;
            }
        }
        return b.length;

    }



    /**
     * Read all remaining bytes in this packet.
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public byte[] readByteArray()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buf = new byte[4096];
        int len;
        while ((len = readBuffer(buf)) == 4096) {
            baos.write(buf);
        }
        if (len>0) {
            baos.write(buf,0,len);
        }
        
        return baos.toByteArray();

    }


// String reading
//............................................................................


    /**
     * Read a string of an unknown length
     *
     * <p>This method reads an UTF-8 encoded string that contains all remaining
     * bytes in this packet.</p>
     *
     * <p>Bug: this methods uses the UTF-8 converter from Sun trough
     * java.lang.String which does not throw an exception on illegal UTF-8
     * input.</p>
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public String readString()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buf = new byte[4096];
        int len;
        while ((len = readBuffer(buf)) == 4096) {
            baos.write(buf);
        }
        if (len>0) {
            baos.write(buf,0,len);
        }
        
        try {
            return new String(baos.toByteArray(), "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

    }


}
