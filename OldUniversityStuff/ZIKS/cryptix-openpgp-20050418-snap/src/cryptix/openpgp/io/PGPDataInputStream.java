/* $Id: PGPDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.util.PGPMPI;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.math.BigInteger;



// ### FIXME: have to write own UTF-8 converter that does throw exceptions on
// ### illegal data.


/**
 * Generic superclass for all inputstreams
 *
 * <p>This class contains the basic methods for inputstreams. A subclass only
 * has to implement the readInternal and close methods and then all the other
 * methods here will magically work.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public abstract class PGPDataInputStream {


// Constructor
//............................................................................

    /**
     * Empty constructor.
     */
    public PGPDataInputStream() {}



// Abstract methods
//............................................................................


    /**
     * Read one byte.
     *
     * <p>This method is used by all other read methods in this class, so this
     * is the only method a subclass has to override.</p>
     *
     * @return the byte read
     */
    protected abstract int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException;


    /**
     * Clean-up and close the stream.
     */
    public abstract void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException;



// Public read methods
//............................................................................


// Boolean

    /**
     * Read a boolean
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public boolean readBoolean()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        if (readInternal() == 0) { return false; } else { return true; }

    }
    


// Byte

    /**
     * Read a byte
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public byte readByte()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        return (byte)    (readInternal());

    }
        

    /**
     * Read an unsigned byte
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public short readUnsignedByte() 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return (short)   (readInternal() & 0xFF);

    }



// Short

    /**
     * Read a short
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public short readShort()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return (short)(  (readInternal()          <<  8) 
                      |  (readInternal() & 0xFF        ) );

    }


    /**
     * Read an unsigned short
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public int readUnsignedShort()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return        ( ((readInternal() & 0xFF ) <<  8) 
                      |  (readInternal() & 0xFF        ) );

    }
    


// Int

    /**
     * Read an int
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public int readInt()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return        (  (readInternal()          << 24) 
                      | ((readInternal() & 0xFF ) << 16)
                      | ((readInternal() & 0xFF ) <<  8)
                      | ((readInternal() & 0xFF )      ) );

    }


    /**
     * Read an unsigned int
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public long readUnsignedInt()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return        ( ((readInternal() & 0xFFl) << 24) 
                      | ((readInternal() & 0xFFl) << 16)
                      | ((readInternal() & 0xFFl) <<  8)
                      | ((readInternal() & 0xFFl)      ) );

    }
    


// Long
    
    /**
     * Read a long
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public long readLong() 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        return        ( (( (long)readInternal() ) << 56) 
                      | ((readInternal() & 0xFFl) << 48)
                      | ((readInternal() & 0xFFl) << 40)
                      | ((readInternal() & 0xFFl) << 32)
                      | ((readInternal() & 0xFFl) << 24)
                      | ((readInternal() & 0xFFl) << 16)
                      | ((readInternal() & 0xFFl) <<  8)
                      | ((readInternal() & 0xFFl)      ) );

    }
    


// Byte Array

    /**
     * Read an array of bytes
     *
     * <p>The complete array is read, calling this method is equivalent to
     * readFully(b, 0, b.length); </p>
     *
     * <p>If not enough bytes are available a PGPDataFormatException is thrown. 
     * If you do not know how many bytes to expect, see readBuffer for a more
     * gentle way of reading bytes.</p>
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public void readFully(byte[] b)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        readFully(b, 0, b.length);

    }
    
    
    /**
     * Read an array of bytes
     *
     * <p>Reading starts at offset and continues for len bytes.</p>
     *
     * <p>If not enough bytes are available a PGPDataFormatException is thrown. 
     * If you do not know how many bytes to expect, see readBuffer for a more
     * gentle way of reading bytes.</p>
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public void readFully(byte[] b, int offset, int len)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        for (int i=offset; i<(len+offset); i++) {
            b[i] = (byte)readInternal();
        }

    }
    
    

// String

    /**
     * Read a string of a certain length
     *
     * <p>This method reads a string in UTF-8 encoding.</p>
     *
     * <p>Bug: this methods uses the UTF-8 converter from Sun trough
     * java.lang.String which does not throw an exception on illegal UTF-8
     * input.</p>
     *
     * @param len length of the string to be read
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public String readString(int len)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        byte[] b = new byte[len];
        readFully(b);
        try {
            return new String(b, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

    }


    /**
     * Read a null terminated string
     *
     * <p>This method reads an UTF-8 encoded string that contains all remaining
     * bytes in this packet.</p>
     *
     * <p>Bug: this methods uses the UTF-8 converter from Sun trough
     * java.lang.String which does not throw an exception on illegal UTF-8
     * input.</p>
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                          data. In this case the underlying inputstream
     *                          will point at the start of the next packet, such
     *                          that this packet can be safely ignored and you
     *                          can continue with the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public String readNullTerminatedString()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int b;
        while ((b = readInternal()) != 0) {
            baos.write(b);
        }
        byte[] bytes = baos.toByteArray();
        
        if (bytes.length == 0) {
            close();
            throw new PGPDataFormatException("String length == 0.");
        }
        
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

    }


    /**
     * Read a string with a length byte prepended
     *
     * <p>This method reads a string in UTF-8 encoding.</p>
     *
     * <p>Bug: this methods uses the UTF-8 converter from Sun trough
     * java.lang.String which does not throw an exception on illegal UTF-8
     * input.</p>
     *
     * @param len length of the string to be read
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public String readLengthPrependedString()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        short len = readUnsignedByte();
        byte[] b = new byte[len];
        readFully(b);
        try {
            return new String(b, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

    }



// MPI
    
    /**
     * Read a PGP encoded multi-precision-integer (MPI)
     *
     * <p>This method uses the cryptix.openpgp.util.PGPMPI class to read an MPI.
     *</p>
     *
     * @throws IOException if the underlying inputstream returns an exception.
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *                                data. In this case the underlying
     *                                inputstream will point at the start of the
     *                                next packet, such that this packet can be
     *                                safely ignored and you can continue with
     *                                the next packet. 
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *                                     in the data. If this happens it means
     *                                     that the current inputstream cannot
     *                                     be used to read more packets from.
     */
    public BigInteger readMPI()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {

        int len = readUnsignedShort();
        byte[] data = new byte[((len + 7) / 8) + 2];
        
        data[0] = (byte)(len >> 8);
        data[1] = (byte)(len);
        
        readFully(data,2,(len + 7) / 8);
        
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        DataInputStream dis = new DataInputStream(bais);
        
        return PGPMPI.decode(dis);

    }


}
