/* $Id: PGPPushBackDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.InputStream;
import java.io.IOException;



/**
 * An inputstream that allows one to 'push-back' some data.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPPushBackDataInputStream extends PGPDataInputStream {


// Instance variables
//............................................................................

    private byte[] buffer = new byte[0];
    private int buffersize = 0;
    private PGPLengthDataInputStream stream;
    

// Constructor
//............................................................................

    /**
     * Constructs a new push back stream, based on another stream
     */
    public PGPPushBackDataInputStream(PGPLengthDataInputStream stream) {
        
        this.stream = stream;
        
    }
    

// Push back
//............................................................................

    /**
     * Pushes back some bytes
     */
    public void pushBack(byte[] bytes) {
        
        byte[] temp = new byte[buffersize + bytes.length];
        System.arraycopy(buffer, 0, temp, 0, buffersize);
        System.arraycopy(bytes, 0, temp, buffersize, bytes.length);
        
    }
    

// Implemented abstract methods
//............................................................................

    /**
     * Read one byte.
     *
     * @return the byte read
     */
    protected int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        if (buffersize == 0) {
            return stream.readInternal();
        } else {
            buffersize--;
            return buffer[buffersize];
        }
    }


    /**
     * Clean-up and close the stream.
     */
    public void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        stream.close();
    }

}
