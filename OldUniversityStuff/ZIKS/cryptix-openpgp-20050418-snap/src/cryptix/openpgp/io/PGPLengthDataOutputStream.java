/* $Id: PGPLengthDataOutputStream.java,v 1.2 2005/03/13 17:12:58 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.math.BigInteger;



/**
 * Generic outputstream to write PGP formatted packet data that contains
 * length information
 *
 * <p>This class basically has two modes, one where the length of the written
 * data is (partly) known in advance and one when the length is not known.
 * In the first mode all data will be written to the underlying outputstream
 * immediately, which allows for full streaming. In the second mode all output
 * will be buffered until the close method is called, where all data is then
 * written in one big chunk to the outputstream.</p>
 *
 * <p>If the subclass wants, it can support writing data in chunks. This is 
 * particularly useful when in streaming a lot of data, when you do not know
 * in advance what the total length will be. In that case you just buffer the
 * data and write a chunk every time your buffer is full.</p>
 *
 * <p>A subclass may decide not to support partial chunks.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public abstract class PGPLengthDataOutputStream extends PGPDataOutputStream {


// Instance variables
//............................................................................

    /** The underlying outputstream */
    private OutputStream out;
    /** Buffer in case we don't know the length */
    private ByteArrayOutputStream buffer = null;
    /** If true then length info has been written and we can write directly
      * to the outputstream, if false then we have to buffer the info */
    private boolean lengthHasBeenWritten = false;
    /** If true then we are currently writing a partial length packet */
    private boolean partial = false;
    /** The number of bytes written in the current chunk */
    private long chunkBytesWritten = 0;
    /** The length of the current chunk if SetLength or setPartialLength has 
      * been used. */ 
    private long chunkLength = 0;
    
    

// Constructor
//............................................................................

    /**
     * Constructor that takes an outputstream
     *
     * <p>Subclasses should call this constructor from their own, after doing
     * their own initialization.</p>
     */
    public PGPLengthDataOutputStream(OutputStream out) {
        this.out = out;
    }


    /**
     * Emtpy constructor for subclasses that handle their own writing
     *
     * <p>If a subclass decides to use this constructor, then the writeDirect
     * method must be overridden.</p>
     */
    public PGPLengthDataOutputStream() {
        this.out = null;
    }



// Implemented abstract methods
//............................................................................


    /**
     * Write one byte directly to the underlying outputstream.
     */
    protected void writeDirect(int b) throws IOException {
    
        out.write(b);
        
    }
    

    /**
     * Internal method used by all other methods to write one byte.
     */
    protected void writeInternal(int b) throws IOException {
    
        if (! lengthHasBeenWritten) {
        
            if (chunkBytesWritten == 0) {
                buffer = new ByteArrayOutputStream();
            }

            buffer.write(b);
            chunkBytesWritten++;
            
        } else {
        
            chunkBytesWritten++;
            if (chunkBytesWritten > chunkLength) {
                throw new RuntimeException("Tried to write more bytes than "+
                                           "set.");
            }
            
            writeDirect(b);
            
        }
    
    }



// Abstract methods
//............................................................................


    /**
     * Write a partial length to the outputstream.
     *
     * <p>If an implementation does not support partial lengths it should throw
     * a (subclass of) RuntimeException</p>
     */
    protected abstract void writePartialLength(OutputStream out, long len)
        throws IOException;
    

    /**
     * Write a length to the outputstream.
     *
     * <p>This method will only be called once for every packet. It may be
     * preceded by several writePartialLength calls.</p>
     */
    protected abstract void writeLength(OutputStream out, long len)
        throws IOException;

    

// Public length control methods
//............................................................................


    /**
     * Set the length of the current packet
     *
     * <p>This method must only be called once for every packet. It may be 
     * preceded by several setPartialLength calls. If this condition is not met
     * an IllegalStateException will be thrown.</p>
     * <p>If no calls have been made setPartialLength then no bytes must have 
     * been written yet, otherwise an IllegalStateException will be thrown.</p>
     *
     * <p>This method calls writeLength.</p>
     *
     * @throws IOException only if writeLength returns an IOException
     */
    public void setLength(long len) throws IOException {

        if ((partial) && (chunkBytesWritten != chunkLength)) {

            throw new IllegalStateException("Partial length has not yet been " +
                                            "completed");

        } else if (partial) {

            partial = false;

        } else if (lengthHasBeenWritten) {

            throw new IllegalStateException("Cannot write two non-partial " +
                                            "lengths");

        } else if ((!lengthHasBeenWritten) && (chunkBytesWritten != 0)) {
            
            throw new IllegalStateException("Bytes have already been written");
    
        }

        chunkLength = len;
        chunkBytesWritten = 0;
        lengthHasBeenWritten = true;

        writeLength(out, len);
        
    }
    
    
    /**
     * Set the length of the current packet
     *
     * <p>If this is the first call to this method then no bytes must have 
     * been written yet, otherwise an IllegalStateException will be thrown.</p>
     * <p>After this method has been called one or more times a setLength must
     * be called for the final chunk. This will be checked in the close method.
     * </p>
     *
     * <p>This method calls writePartialLength.</p>
     *
     * @throws IOException only if writePartialLength returns an IOException
     */
    public void setPartialLength(long len) throws IOException {
    
        if ((partial) && (chunkBytesWritten != chunkLength)) {

            throw new IllegalStateException("Partial length has not yet been " +
                                           "completed");
        
        } else if (lengthHasBeenWritten) {
        
            throw new IllegalStateException("Cannot set a partial length " +
                                            "after a normal length");
        
        } else if ((!lengthHasBeenWritten) && (chunkBytesWritten != 0)) {
            
            throw new IllegalStateException("Bytes have already been written");

        }    
        
        partial = true;
        chunkLength = len;
        chunkBytesWritten = 0;
        lengthHasBeenWritten = true;

        writePartialLength(out, len);

    }
    


// Close method
//............................................................................


    /**
     * Close this inputstream
     *
     * <p>If setLength and setPartialLength have been used this method checks
     * to see if all bytes of the chunk have been written, otherwise an
     * IllegalStateException will be thrown.</p>
     * <p>If setLength and setPartialLength have not been used and thus all 
     * data has been buffered, the buffered data will be written after a
     * call to writeLength.</p>
     *
     * <p>This method does not close the underlying outputstream.</p>
     * <p>This method must always be called.</p>
     *
     * @throws IOException if either writeLength throws it or something goes
     *                     wrong while writing the buffer.
     */
    public void close() throws IOException {
    
        if (partial) {
        
            throw new IllegalStateException("Packet cannot end with a partial "+
                                            "length");
        
        } else if ((lengthHasBeenWritten)&&(chunkBytesWritten != chunkLength)) {
        
            throw new IllegalStateException("Packet has not been completely "+
                                            "written");
        
        } else if (!lengthHasBeenWritten) {
        
            byte[] b;
            if (buffer != null) {
                b = buffer.toByteArray();
            } else {
                b = new byte[0];
            }
            
            chunkLength = (long)b.length;
            chunkBytesWritten = 0;
            lengthHasBeenWritten = true;
            
            writeLength(out, (long)b.length);
            writeFully(b);

        }        
    
    }


}
