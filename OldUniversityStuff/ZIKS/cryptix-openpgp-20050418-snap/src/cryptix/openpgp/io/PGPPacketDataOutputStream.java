/* $Id: PGPPacketDataOutputStream.java,v 1.3 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.IOException;
import java.io.OutputStream;



/**
 * Outputstream to write PGP formatted (normal) packets
 *
 * <p>This class does support partial encodings</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */
public class PGPPacketDataOutputStream extends PGPLengthDataOutputStream {


// Instance variables
//............................................................................

    /** The packetID */
    private byte packetID;
    /** True if partial lengths have been written */
    private boolean partial = false;
    /** 0, 1 or 2 if some old style length should be enforced, -1 if not */
    private int forcetype = -1;



// Constructor
//............................................................................

    /**
     * Constructor that takes an outputstream, a packetID and a forced length
     * type
     */
    public PGPPacketDataOutputStream(OutputStream out, byte packetID, 
                                     int forcetype) 
    {
        super(out);
        this.packetID = packetID;
        this.forcetype = forcetype;
    }

    /**
     * Constructor that takes an outputstream and a packetID
     */
    public PGPPacketDataOutputStream(OutputStream out, byte packetID) 
    {
        super(out);
        this.packetID = packetID;
        this.forcetype = -1;
    }


// Implementation of abstract mathods
//............................................................................

    /**
     * Write a partial length to the outputstream.
     *
     * @throws IOException if something goes wrong with the outputstream
     * @throws IllegalArgumentException if len is not a power of two between 2^0
     *                                  and 2^30.
     */
    protected void writePartialLength(OutputStream out, long len)
        throws IOException
    {
        if (!partial) {
            writeNewType(out, packetID);
            partial = true;
        }
        writeNewPartialLength(out, len);
    }
    

    /**
     * Write a length to the outputstream.
     *
     * @throws IOException if something goes wrong with the outputstream
     */
    protected void writeLength(OutputStream out, long len)
        throws IOException
    {

        if (!partial) {

            if (packetID < 16) {
                writeOldTypeAndLength(out, packetID, len);
            } else {
                writeNewType(out, packetID);
                writeNewLength(out, len);
            }

        } else {
            writeNewLength(out, len);
        }

    }


// Internal methods
//............................................................................


    /** Write old-style type and length bytes */
    private void writeOldTypeAndLength(OutputStream out, byte type, long len)
        throws IOException 
    {
    
        if (len < 0) {
            throw new IllegalArgumentException("Negative length not allowed");
        }

        int lentype;

        if (forcetype == -1) {
            if (len < 256) {
                lentype = 0; 
            } else if (len < 65536) {
                lentype = 1;
            } else if (len < 4294967296L) {
                lentype = 2;
            } else {
                throw new IllegalArgumentException("Maximum length exceeded");
            }
        } else {
            if ((forcetype > 2) || (forcetype < 0))
                throw new IllegalArgumentException(
                    "Invalid forced length "+forcetype);
            lentype = forcetype;
        }
        
        out.write(128 + (type << 2) + lentype);
        
        if (lentype == 0) {
            out.write((byte)len);
        } else if (lentype == 1) {
            out.write((byte)(len >> 8));
            out.write((byte)(len));
        } else {
            out.write((byte)(len >> 24));
            out.write((byte)(len >> 16));
            out.write((byte)(len >>  8));
            out.write((byte)(len));
        }
    
    }
    
    /** Write a new type byte */
    private void writeNewType(OutputStream out, byte type) 
        throws IOException 
    {
        
        if ((type >= 64) || (type < 0)) {
            throw new IllegalArgumentException("Packet ID should be 0<=id<=63");
        }
        
        out.write(192 + type);
        
    }
    
    /** Write a new 'normal' length */
    private void writeNewLength(OutputStream out, long len) 
        throws IOException 
    {

        if (len < 0) {
            throw new IllegalArgumentException("Negative length not allowed");
        }

        int lentype;
        if (len < 192) {
            out.write((byte)len);
        } else if (len < 8384) {
            out.write(192 + (byte)((len-192)>>8));
            out.write((byte)(len-192));
        } else if (len < 4294967296L) {
            out.write(255);
            out.write((byte)(len >> 24));
            out.write((byte)(len >> 16));
            out.write((byte)(len >>  8));
            out.write((byte)(len));
        } else {
            throw new IllegalArgumentException("Maximum length exceeded");
        }

    }


    /** Helper table for writePartialLength */
    private static final long[] powtwo = 
        { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384,
          32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304,
          8388608, 16777216, 33554432, 67108864, 134217728, 268435456,
          536870912, 1073741824 };
    
    /** Write a new partial length */
    private void writeNewPartialLength(OutputStream out, long len) 
        throws IOException 
    {

        for (int i=0; i<powtwo.length; i++) {
            if (powtwo[i] == len) {
                out.write(224+i);
                return;
            }
        }

        throw new IllegalArgumentException("Partial length is not a power of "+
                                           "two between 2^0 and 2^30");
                                           
    }


}
