/* $Id: PGPCompressorInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.InputStream;
import java.io.FilterInputStream;
import java.io.IOException;


/** 
 * This class provides an expansion output stream framework.  An
 * expansion stream is one that expands data as it is read from an
 * input stream.  Clients can treat this just like any other
 * InputStream.  This class is really just like a FilterOutputStream,
 * but does define all the read methods in terms of the framework
 * method, readInternal.  That is the only method that subclasses need
 * to implement.
 *
 *
 * @see cryptix.openpgp.io.PGPCompressorOutputStream
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $ 
 */
public abstract class PGPCompressorInputStream extends FilterInputStream {


  /**
   * Constructor takes the source input stream as the only
   * argument.
   *
   * @param in the source input stream from which compressed data is read.
   */
  public PGPCompressorInputStream(InputStream in) {
    super(in);
  }


// FilterInputStream methods
//...........................................................................


  /**
   * Reads the next byte of data from this input stream.  The value 
   * byte is returned as an <code>int</code> in the range 
   * <code>0</code> to <code>255</code>. If no byte is available 
   * because the end of the stream has been reached, the value 
   * <code>-1</code> is returned. This method blocks until input data 
   * is available, the end of the stream is detected, or an exception 
   * is thrown. 
   *
   * <p> This method simply performs the call <code>readInternal(b, 0,
   * 1)</code> (where b is a newly allocated byte array of length 1)
   * and returns the <code>b[0]</code> converted to an int.
   *
   * <p>
   * The implementation here is non-optimal.  Clients are encouraged
   * to use one of the read methods that takes an array argument.
   * Subclasses should override this method if they want to make a 
   * more efficient implementation available.
   *
   * <p>
   * Data expansion is performed by the readInternal framework method.
   *
   * @return     the next byte of data, or <code>-1</code> if the end of the
   *             stream is reached.
   * @exception  IOException  if an I/O error occurs.
   */
  public int read() throws IOException {
    byte[] arr = new byte[1];
    int len = readInternal(arr, 0, 1);
    return((len <= 0) ? -1 : byteToInt(arr[0]));
  }


  /**
   * Reads up to <code>byte.length</code> bytes of data from this 
   * input stream into an array of bytes. This method blocks until some 
   * input is available. 
   *
   * <p> This method simply performs the call <code>readInternal(b, 0,
   * b.length)</code> and returns the result.
   *
   * <p>
   * Data expansion is performed by the writeInternal framework method.
   *
   * @param      b   the buffer into which the data is read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs.
   */
  public int read(byte b[]) throws IOException {
    return readInternal(b, 0, b.length);
  }
  

  /**
   * Reads up to <code>len</code> bytes of data from this input stream 
   * into an array of bytes. This method blocks until some input is 
   * available. 
   *
   * <p>
   * This method simply performs <code>readInternal(b, off, len)</code> 
   * and returns the result.
   *
   * <p>
   * Data expansion is performed by the writeInternal framework method.
   *
   * @param      b     the buffer into which the data is read.
   * @param      off   the start offset of the data.
   * @param      len   the maximum number of bytes read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs.
   */
  public int read(byte b[], int off, int len) throws IOException {
    return readInternal(b, off, len);
  }



// Methods for subclasses
//...........................................................................



  /** 
   * Subclasses are expected to use this method to read from the
   * underlying input stream.  Data from the stream is assumed to be
   * in a compressed format and it is the job of the concrete subclass
   * to figure out how to decompress the data before placing it in the
   * buffer provided.  The underlying stream can be accessed through the
   * 'in' member variable.
   *
   * @param      b     the buffer into which the data is read.
   * @param      off   the start offset of the data.
   * @param      len   the maximum number of bytes read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs.
   */
  protected abstract int readInternal(byte[] buffer, int offset, int len) 
    throws IOException;



// Private helper methods
//...........................................................................



  /**
   * @param b an unsigned byte
   * @return the byte represented as an int (0 - 255)
   */
  private static final int byteToInt(byte b) {
    int i = 0;
    if ((b & 0x80) != 0) {
      i |= 0x80;
      b &= 0x7F;
    }
    i |= b;
    return(i);
  }


}
