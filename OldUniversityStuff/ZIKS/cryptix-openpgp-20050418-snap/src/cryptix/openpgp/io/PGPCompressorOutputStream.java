/* $Id: PGPCompressorOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.OutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;


/** 
 * This class provides a compression output stream framework.  A
 * compression output stream compresses data as it passes through the
 * stream.  For the most part, clients can treat this just like any
 * other OutputStream, but they should call finish() when no more data
 * will be written.
 *
 * <p>Subclassors must implement <code>writeInternal</code>.  Subclassors
 * may want to override <code>finishInternal</code> to take care of any
 * end-of-data processing.
 * 
 *
 * @see cryptix.openpgp.io.PGPCompressorInputStream
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $ 
 */
public abstract class PGPCompressorOutputStream extends FilterOutputStream {

  //
  // If finish() has been called (and has completed successfully) this
  // is true.  
  //
  private boolean finished;



  /**
   * @param out the unerlying output stream
   * @param mode one of the two mode constants defined in this class
   */
  public PGPCompressorOutputStream(OutputStream out) {
    super(out);
    finished = false;
  }


  /** 
   * Indicate to this object that you wish for compression or
   * expansion to end.  
   *
   * <p>The default implementation just calls the
   * <code>finishInternal</code> framework method.  
   * 
   * @exception IOException if something goes wrong.
   */
  public final void finish() throws IOException {
    if (!finished) {
      finishInternal();
      finished = true;
    }
  }



// FilterOutputStream methods
//...........................................................................


  /**
   * (FilterOutputStream interface) This method is inefficient as
   * coded, so clients should avoid it.
   *
   * @param      b   the <code>byte</code>.
   * @exception IOException if an I/O error occurs.  
   */
  public final void write(int b) throws IOException {
    writeInternal(new byte[]{(byte)b}, 0, 1);
  }


  /**
   * (FilterOutputStream interface) Write data to the underlying
   * stream after passing through the compression algorithm.
   *
   * @param      b   the data to be written.
   * @exception  IOException  if an I/O error occurs.
   */
  public final void write(byte b[]) throws IOException {
    writeInternal(b, 0, b.length);
  }


  /**
   * (FilterOutputStream interface) Write data to the underlying
   * stream after passing through the compression algorithm.
   *
   * @param      b     the data.
   * @param      off   the start offset in the data.
   * @param      len   the number of bytes to write.
   * @exception  IOException  if an I/O error occurs.
   */
  public final void write(byte b[], int off, int len) throws IOException {
    writeInternal(b, off, len);
  }



  /**
   * (FilterOutputStream interface) Closes the unerlying output stream
   * and releases any system resources associated with the stream.
   *
   * <p> The <code>close</code> method of
   * <code>PGPCompressorOutputStream</code> calls its
   * <code>finish</code> method, and then calls the <code>close</code>
   * method of <code>FilterOutputStream</code>.
   *
   * @exception  IOException  if an I/O error occurs.
   */
  public void close() throws IOException {
    finish();
    super.close();      
  }



// Methods for subclasses 
//...........................................................................



  /** 
   * Framework method to be implemented by subclasses.  All of the write
   * methods in the FilterOutputStream interface call into this method
   * to perform the actualy writing (and compressing).  Subclasses should
   * implement the compression on the data, and then write the data through
   * to the underlying stream (held in the 'out' member variable).
   *
   * @param data the raw (uncompressed) data array
   * @param offset the offset into the data array at which to start reading
   * @param len the number of bytes in the data array to read
   * @exception IOException if something goes wrong.
   */
  protected abstract void writeInternal(byte[] data, int offset, int len) 
    throws IOException;


  /** 
   * Called by the user (or by close()) to indicate that no more data
   * will be compressed through the stream.  This default implementation
   * calls the <code>flush</code> method.  Subclasses should override as
   * necessary.  This method will only be called once per instance.
   *
   * @exception IOException if something goes wrong.
   */
  protected void finishInternal() throws IOException {
    flush();
  }



}
