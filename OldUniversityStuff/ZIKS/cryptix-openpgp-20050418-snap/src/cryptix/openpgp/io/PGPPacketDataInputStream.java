/* $Id: PGPPacketDataInputStream.java,v 1.3 2005/03/29 11:22:49 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.InputStream;
import java.io.IOException;



/**
 * Inputstream for normal PGP packets that supports partial lengths.
 *
 * <p>It supports both old and new style packets.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPPacketDataInputStream extends PGPLengthDataInputStream {


// Instance variables
//............................................................................

    /** This chunk is the last chunk */
    private boolean isFinal = false;
    /** This chunk is of indeterminate length */
    private boolean isIndeterminate = false;
    /** This chunk is a partial chunk, more will follow */
    private boolean isPartial = false;
    /** It is an old style packet */
    private boolean old;
    /** Lengthtype for old style packets */
    private int lentype;
    
    

// Constructor
//............................................................................

    /**
     * Construct a new PGPPacketDataInputStream
     *
     * @param in the underlying inputstream
     * @param old true if this is an oldstyle packet where part of the length
     *            is encoded in the packet type
     * @param lentype if old is true, then this is a value from 0-3 specifying
     *                the length type that was encoded in the packet type byte.
     */
    public PGPPacketDataInputStream(InputStream in, boolean old, int lentype) {

        super(in);

        if (old && ((lentype > 3) || (lentype < 0))) {
            throw new IllegalArgumentException("Illegal length type");
        }

        this.old = old;
        this.lentype = lentype;
        
        if (old && (lentype == 3)) {
            this.isIndeterminate = true;
        }
    }



// Implemented method from PGPDataInputStream
//............................................................................


    /**
     * Initialize a new chunk and return the length
     *
     * @throws IOException if the underlying inputstream returns an exception
     * @throws PGPDataException this method does not throw this exception
     * @throws PGPFatalDataException this method does not throw this exception
     */
    protected long getNewChunkLength(InputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {
        
        long chunkLength;
        
        if (isFinal) {
            
            chunkLength = 0;
            
        } else if (isIndeterminate) {
        
            chunkLength = in.available();

        } else {
            
            long len = readBodyLength();
            
            if (len < -1) { // Partial length
            
                chunkLength = (1 << ((-len) & 0x1f));
                isPartial = true;
            
            } else {
                
                chunkLength = len;
                isFinal = true;

            }
            
        }
        
        return chunkLength;
        
    }

    /**
     * Returns whether this is the final chunk
     */
    public boolean isFinal() { return isFinal; }


// Internal method
//............................................................................

    /**
     * Read the PGP encoded body length.
     *
     * @return [0, 1, 2, ...] for 'normal' lengths, [-1] for indeterminate 
     *         length and [-224 ... -254] for partial length.
     */
    private long readBodyLength()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {
    
        if (old) {

            switch (lentype) {
                case 0:
                    return readUnsignedByte();
                case 1:
                    return readUnsignedShort();
                case 2:
                    return readUnsignedInt();
                case 3:
                    return -1;   // Indeterminate length
                default:
                    throw new InternalError("lentype != 0, 1, 2 or 3");
            }

        } else {    // new packet type

            int octet1 = readUnsignedByte();

            if (octet1 < 192) {                                // 1 byte len

                return octet1;

            } else if ( (octet1 >= 192) && (octet1 < 224) ) {  // 2 byte len

                return ((octet1 - 192) << 8) + readUnsignedByte() + 192;

            } else if (octet1 == 255) {                        // 4 byte len

                return readUnsignedInt();

            } else {  // octet1 >= 224 && octet1 < 255         // partial

                return -octet1;

            }
        }

    }

}
