/* $Id: PGPChecksumDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPInvalidChecksumException;
import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.IOException;


/**
 * Inputstream to read data and check the checksum.
 *
 * <p>This class implements a very basic checksum: de sum of all bytes, modulo
 * 65536.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPChecksumDataInputStream extends PGPDataInputStream {


// Instance variables
//............................................................................

    /** The underlying inputstream */
    private PGPDataInputStream in;
    /** The running checksum */
    private int checksum = 0;



// Constructor
//............................................................................

    /**
     * Constructor that takes a PGPDataInputStream
     */
    public PGPChecksumDataInputStream(PGPDataInputStream in) {
        this.in = in;
    }
    


// Overridden methods
//............................................................................


    /**
     * Read one byte.
     *
     * @return the byte read
     */
    protected int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        
        int b = in.readUnsignedByte();
        checksum += b;
        return b;
        
    }


    /**
     * Read and check the checksum.
     *
     * <p>This method does not call the close() method on the underlying
     * PGPDataInputStream</p>
     *
     * @throws IOException if the underlying stream throws it.
     * @throws PGPDataFormatException if the underlying stream throws it.
     * @throws PGPFatalDataFormatException if the underlying stream throws it.
     * @throws PGPInvalidChecksumException if the checksum is not correct (note
     *         that this exception is also a subclass of PGPDataFormatException.
     */
    public void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
    
        int c = in.readUnsignedShort();
        if (c != checksum) {
            throw new PGPInvalidChecksumException("Read: "+c+
                                                 " Found: "+checksum);
        }
    
    }       


}
