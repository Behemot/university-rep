/* $Id: PGPCipherDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.InputStream;
import java.io.IOException;

import javax.crypto.Cipher;
import javax.crypto.ShortBufferException;


/**
 * Document me
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPCipherDataInputStream extends PGPLengthDataInputStream {


// Instance variables
//............................................................................

    /** The underlying inputstream */
    private PGPLengthDataInputStream in;
    /** Buffer where we store the decrypted data */
    private byte[] buffer = new byte[4096];
    /** Cipher that will be used to decrypt the data */
    private Cipher c;
    /** The length of the current chunk */
    private long chunkLength = 0;
    /** The number of bytes already read from the current chunk */
    private long chunkBytesRead = -1;
    

// Constructor
//............................................................................

    /**
     * Construct a new PGPCipherDataInputStream
     *
     * <p>The cipher c has to be initialized with the key before calling this
     * method. It also must use a mode where the size of the encrypted data
     * always equals the size of the decrypted data (e.g. CFB or OpenpgpCFB).
     * </p>
     *
     * @param in the underlying inputstream
     * @param c the cipher used to decrypt the data, 
     */
    public PGPCipherDataInputStream(PGPLengthDataInputStream in, Cipher c) {

        super();
        this.in = in;
        this.c = c;

    }



// Implemented methods from PGPDataInputStream
//............................................................................


    /**
     * Reads one byte from the buffer
     *
     * @throws IOException this method does not throw this exception
     * @throws PGPDataException this method does not throw this exception
     * @throws PGPFatalDataException this method does not throw this exception
     */
    protected int readDirect()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        chunkBytesRead++;
        return buffer[(int)chunkBytesRead - 1];

    }


    /**
     * Initialize a new chunk and return the length
     *
     * @throws IOException if the underlying inputstream returns an exception
     * @throws PGPDataException if the underlying inputstream returns it
     * @throws PGPFatalDataException if the underlying inputstream returns it
     */
    protected long getNewChunkLength(InputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {
        
        // grmbl... Sun does not specify that in place decryption is possible
        byte[] grmbl = new byte[buffer.length];

        chunkBytesRead = 0;
        chunkLength = this.in.readBuffer(grmbl);
        
        try {
            if (c.update(grmbl, 0, buffer.length, buffer, 0) != buffer.length) {
                throw new RuntimeException("Length of encrypted data > length "+
                                           "of decrypted data");
            }
        } catch (ShortBufferException sbe) {
            throw new RuntimeException("Length of encrypted data < length of "+
                                       "decrypted data - "+sbe);
        }
        
        return chunkLength;  
        
    }



}
