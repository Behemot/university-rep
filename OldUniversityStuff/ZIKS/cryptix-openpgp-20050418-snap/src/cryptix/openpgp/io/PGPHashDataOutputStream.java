/* $Id: PGPHashDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.util.PGPUpdateable;

import java.io.IOException;
import java.io.OutputStream;

import java.security.MessageDigest;



/**
 * Outputstream to return the hash of a public key packet.
 *
 * <p>This is used for both signatures and key fingerprints.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPHashDataOutputStream extends PGPPacketDataOutputStream {


// Instance variables
//............................................................................

    /** Used to calculate the hash */
    private MessageDigest md;
    /** Braindamaged SUN JCE forces us to hash everything twice for sigs */
    private PGPUpdateable signer;
    /** Number of bytes written */
    private int bytesWritten = 0;


    
// Constructor
//............................................................................

    /**
     * Constructor for fingerprints
     */
    public PGPHashDataOutputStream(MessageDigest md) 
    {
        super(null, (byte)6);
        this.md = md;
        this.signer = null;
    }


    /**
     * Constructor for signatures
     */
    public PGPHashDataOutputStream(MessageDigest md, PGPUpdateable signer) 
    {
        super(null, (byte)6);
        this.md = md;
        this.signer = signer;
    }



// Write method
//............................................................................

    /**
     * Internal method used by all other methods to write one byte.
     */
    protected void writeDirect(int b) throws IOException {
    
        md.update((byte)b);
        
        // blame SUN for having to hash everything twice
        if (signer != null) {
            byte[] bb = new byte[1];
            bb[0] = (byte)b;
            signer.update(bb);
        }
        
        bytesWritten ++;
    
    }



// Length methods
//............................................................................

    /**
     * Unsupported method
     *
     * <p>Always throws an InternalError, because a public key should not
     * use partial lengths.</p>
     */
    protected void writePartialLength(OutputStream out, long len)
        throws IOException
    {
        throw new InternalError("Partial lengths not allowed on public keys "+
                                "when calculating a hash.");
    }
    

    /**
     * Write a length to the outputstream.
     *
     * @throws IOException if something goes wrong with the outputstream
     */
    protected void writeLength(OutputStream out, long len)
        throws IOException
    {

        if (len > 65535) {
            throw new InternalError("Length > 65535.");
        }

        writeDirect(0x99);  // packet id  6: public key

        writeDirect((int)len >> 8);
        writeDirect((int)len & 0xFF);
        
    }


}
