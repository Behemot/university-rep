/* $Id: PGPChecksumDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.OutputStream;
import java.io.IOException;

import java.math.BigInteger;



/**
 * Outputstream to write data with a checksum.
 *
 * <p>This class implements a very basic checksum: de sum of all bytes, modulo
 * 65536.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPChecksumDataOutputStream extends PGPDataOutputStream {


// Instance variables
//............................................................................

    /** The undelrying outputstream */
    private PGPDataOutputStream out;
    /** The running checksum */
    private int checksum = 0;
    


// Constructor
//............................................................................

    /**
     * Constructor that takes a PGPDataOutputStream
     */
    public PGPChecksumDataOutputStream(PGPDataOutputStream out) {
        this.out = out;
    }
    


// Overridden methods
//............................................................................

    /**
     * Write one byte
     *
     * <p>This method updates the checksum and writes the byte to the underlying
     * outputstream.</p>
     */
    protected void writeInternal(int b) throws IOException {
    
        checksum += (b & 0xFF);

        out.writeByte((byte)b);
        
    }
    

    /**
     * Write the checksum and close this stream.
     *
     * <p>This method does not close the underlying outputstream.</p>
     */
    public void close() throws IOException {
    
        out.writeShort((short)(checksum & 0xFFFF));
        
    }
    

}
