/* $Id: PGPSignatureDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.InputStream;
import java.io.IOException;



/**
 * Inputstream for PGP signature subpackets.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPSignatureDataInputStream extends PGPLengthDataInputStream {


// Instance variables
//............................................................................

    /** This chunk is the last chunk */
    private boolean isFinal = false;
    
    

// Constructor
//............................................................................

    /**
     * Construct a new PGPSignatureDataInputStream
     *
     * @param in the underlying inputstream
     */
    public PGPSignatureDataInputStream(InputStream in) {

        super(in);

    }



// Implemented method from PGPDataInputStream
//............................................................................


    /**
     * Initialize a new chunk and return the length
     *
     * @throws IOException if the underlying inputstream returns an exception
     * @throws PGPDataException this method does not throw this exception
     * @throws PGPFatalDataException this method does not throw this exception
     */
    protected long getNewChunkLength(InputStream in) 
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {
        
        long chunkLength;
        
        if (isFinal) {
            
            chunkLength = 0;
            
        } else {
            
            chunkLength = readBodyLength(in);
            isFinal = true;
            
        }
        
        return chunkLength;
        
    }



// Internal method
//............................................................................

    /**
     * Read the PGP encoded signature subpacket body length.
     */
    private long readBodyLength(InputStream in)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException 
    {
        
        int octet1 = in.read();

        if (octet1 < 192) {                                // 1 byte len

            return octet1;

        } else if ( (octet1 >= 192) && (octet1 < 255) ) {  // 2 byte len

            return ((octet1 - 192) << 8) + in.read() + 192;

        } else { // (octet1 == 255)                        // 4 byte len

            return (((long)in.read()) << 24)
                 + (in.read() << 16)
                 + (in.read() << 8)
                 + (in.read());

        }

    }

}
