/* $Id: PGPOutputStreamAdapter.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import java.io.IOException;
import java.io.OutputStream;


/** 
 * A simple adapter class that converts a PGPDataOutputStream into
 * a java.io.OutputStream.
 *
 * <P>Most of the method comments here are copied from
 * java.io.OutputStream.
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $ 
*/
public class PGPOutputStreamAdapter extends OutputStream {


  private PGPDataOutputStream out;



  /** 
   * Construt the adapter which will write through to the
   * given PGPDataOutputStream.
   *
   * @param out the PGPDataOutputStream to write to 
   */
  public PGPOutputStreamAdapter(PGPDataOutputStream out) {
    this.out = out;
  }


  /**
   * Writes the specified byte to this output stream. The general 
   * contract for <code>write</code> is that one byte is written 
   * to the output stream. The byte to be written is the eight 
   * low-order bits of the argument <code>b</code>. The 24 
   * high-order bits of <code>b</code> are ignored.
   * <p>
   * Subclasses of <code>OutputStream</code> must provide an 
   * implementation for this method. 
   *
   * @param      b   the <code>byte</code>.
   * @exception  IOException  if an I/O error occurs. In particular, 
   *             an <code>IOException</code> may be thrown if the 
   *             output stream has been closed.
   */
  public void write(int b) throws IOException {
    out.writeByte((byte)b);
  }


  /**
   * Writes <code>b.length</code> bytes from the specified byte array 
   * to this output stream. The general contract for <code>write(b)</code> 
   * is that it should have exactly the same effect as the call 
   * <code>write(b, 0, b.length)</code>.
   *
   * @param      b   the data.
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.OutputStream#write(byte[], int, int)
   */
  public void write(byte b[]) throws IOException {
    write(b, 0, b.length);
  }


  /**
   * Writes <code>len</code> bytes from the specified byte array 
   * starting at offset <code>off</code> to this output stream. 
   * The general contract for <code>write(b, off, len)</code> is that 
   * some of the bytes in the array <code>b</code> are written to the 
   * output stream in order; element <code>b[off]</code> is the first 
   * byte written and <code>b[off+len-1]</code> is the last byte written 
   * by this operation.
   * <p>
   * If <code>b</code> is <code>null</code>, a 
   * <code>NullPointerException</code> is thrown.
   * <p>
   * If <code>off</code> is negative, or <code>len</code> is negative, or 
   * <code>off+len</code> is greater than the length of the array 
   * <code>b</code>, then an <tt>IndexOutOfBoundsException</tt> is thrown.
   *
   * @param      b     the data.
   * @param      off   the start offset in the data.
   * @param      len   the number of bytes to write.
   * @exception  IOException  if an I/O error occurs. In particular, 
   *             an <code>IOException</code> is thrown if the output 
   *             stream is closed.
   */
  public void write(byte b[], int off, int len) throws IOException {
    if (b == null) {
      throw new NullPointerException();
    } else if ((off < 0) || (off > b.length) || (len < 0) ||
               ((off + len) > b.length) || ((off + len) < 0)) {
      throw new IndexOutOfBoundsException();
    } else if (len == 0) {
      return;
    }
    out.writeFully(b, off, len);
  }


  /**
   * Does nothing.
   *
   * @exception  IOException  if an I/O error occurs.
   */
  public void flush() throws IOException {
  }


  /**
   * We call close on our underlying PGPDataOutputStream.
   *
   * @exception  IOException  if an I/O error occurs.
   */
  public void close() throws IOException {
    out.close();
  }
 

}
