/* $Id: PGPDataOutputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.util.PGPMPI;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.math.BigInteger;



/**
 * Generic superclass for all outputstreams
 *
 * <p>This class contains the basic methods for outputstreams. A subclass only
 * has to implement the writeInternal and close methods and then all the other
 * methods here will magically work.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public abstract class PGPDataOutputStream {


// Constructor
//............................................................................


    /**
     * Empty constructor
     */
    public PGPDataOutputStream() {}



// Abstract methods
//............................................................................


    /**
     * Internal method used by all other methods to write one byte.
     */
    protected abstract void writeInternal(int b) throws IOException;


    /**
     * Close this inputstream
     */
    public abstract void close() throws IOException;

        

// Public write methods
//............................................................................


// Boolean

    /**
     * Write a boolean
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeBoolean(boolean x) throws IOException {

        if (x) { writeInternal(1); } else { writeInternal(0); }

    }
    


// Byte

    /**
     * Write a byte
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeByte(byte x) throws IOException {

        writeInternal(x);

    }
        


// Short

    /**
     * Write a short
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeShort(short x) throws IOException {

        writeInternal(x >>  8);
        writeInternal(x      );

    }



// Int

    /**
     * Write an int
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeInt(int x) throws IOException {

        writeInternal(x >> 24);
        writeInternal(x >> 16);
        writeInternal(x >>  8);
        writeInternal(x      );

    }



// Long
    
    /**
     * Write a long
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeLong(long x) throws IOException {

        writeInternal((int)(x >> 56));
        writeInternal((int)(x >> 48));
        writeInternal((int)(x >> 40));
        writeInternal((int)(x >> 32));
        writeInternal((int)(x >> 24));
        writeInternal((int)(x >> 16));
        writeInternal((int)(x >>  8));
        writeInternal((int)(x      ));

    }    
    


// Byte Array

    /**
     * Write an array of bytes
     *
     * <p>The complete array is written, calling this method is equivalent to
     * writeFully(b, 0, b.length); </p>
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeFully(byte[] b) throws IOException {

        writeFully(b, 0, b.length);

    }
    
    
    /**
     * Write an array of bytes
     *
     * <p>Writing starts at offset and continues for len bytes.</p>
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeFully(byte[] b, int offset, int len) throws IOException {

        for (int i=offset; i<(len+offset); i++) {
            writeInternal(b[i]);
        }

    }
    
    

// String

    /**
     * Write a string 
     *
     * <p>This method writes a string in UTF-8 encoding.</p>
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeString(String x) throws IOException {

        byte[] b;
        try {
            b = x.getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

        writeFully(b);

    }


    /**
     * Write a null terminated string
     *
     * <p>This method simply calls writeString and then writes a null byte.</p>
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeNullTerminatedString(String x) throws IOException {

        writeString(x);
        
        writeInternal(0);

    }


    /**
     * Write a string with a length byte prepended
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeLengthPrependedString(String x) throws IOException {

        byte[] b;
        try {
            b = x.getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported");
        }

        if (b.length > 255) {
            throw new IllegalArgumentException("String longer than 255 bytes");
        }
        writeByte((byte)b.length);
        writeFully(b);

    }


// MPI
    
    /**
     * Write a PGP encoded multi-precision-integer (MPI)
     *
     * <p>This method uses the cryptix.openpgp.util.PGPMPI class to write an
     * MPI. </p>
     *
     * @throws IOException if the underlying outputstream returns an exception.
     */
    public void writeMPI(BigInteger x) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        PGPMPI.encode(dos,x);
        
        writeFully(baos.toByteArray());

    }


}
