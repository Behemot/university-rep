/* $Id: PGPInputStreamAdapter.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.IOException;
import java.io.InputStream;


/** 
 * An adapter class that converts a PGPPacketDataInputStream into a
 * java.io.InputStream.  There is some hand waving going on here since
 * a PGPPacketDataInputStream does not support the full suite of
 * InputStream functionality, and it also throws some different
 * exceptions (the PGPDataFormat ones) that the InputStream interface
 * does not support.
 *
 *
 * @see cryptix.openpgp.io.PGPOutputStreamAdapter
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $ 
 */
public class PGPInputStreamAdapter extends InputStream {


  private PGPPacketDataInputStream in;


  /** 
   * Construt the adapter which will use the given PGPPacketDataInputStream
   * as the underlying input stream to which all method calls are forwarded.
   *
   * @param in a PGPPacketDatInputStream to read from.
   */
  public PGPInputStreamAdapter(PGPPacketDataInputStream in) {
    this.in = in;
  }


  /**
   * Calls readBuffer on the underlying stream passing in a newly
   * allocated byte array for temporary storage.
   * 
   * @return     the next byte of data, or <code>-1</code> if the end of the
   *             stream is reached.
   * @exception  IOException  if an I/O error occurs, or if the underlying 
   *             stream throws a PGPDataFormatException or a 
   *             PGPFatalDataFormatException.
   */
  public int read() throws IOException {
    byte[] buf = new byte[1];
    try {
      int len = in.readBuffer(buf);
      return((len <= 0) ? -1 : byteToInt(buf[0]));
    }
    catch(PGPDataFormatException e) {
      throw(new IOException("Data format exception: " + e.getMessage()));
    }
    catch(PGPFatalDataFormatException e) {
      throw(new IOException("Fatal data format exception: " + e.getMessage()));
    }
  }
      


  /**
   * Calls the readBuffer method on the underlying stream.
   *
   * @param      b   the buffer into which the data is read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> is there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs, or if the underlying 
   *             stream throws a PGPDataFormatException or a 
   *             PGPFatalDataFormatException.
   * @see        java.io.InputStream#read(byte[], int, int)
   */
  public int read(byte b[]) throws IOException {
    try {
      int r = in.readBuffer(b);
      if (r == 0) return -1; else return r;
    }
    catch(PGPDataFormatException e) {
      throw(new IOException("Data format exception: " + e.getMessage()));
    }
    catch(PGPFatalDataFormatException e) {
      throw(new IOException("Fatal data format exception: " + e.getMessage()));
    }
  }


  /**
   * Call the readBuffer method on the underlying stream.
   *
   * @param      b     the buffer into which the data is read.
   * @param      off   the start offset in array <code>b</code>
   *                   at which the data is written.
   * @param      len   the maximum number of bytes to read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs, or if the underlying stream
   * throws a PGPDataFormatException or a PGPFatalDataFormatException.
   */
  public int read(byte b[], int off, int len) throws IOException {
    try {
      int r = in.readBuffer(b, off, len);
      if (r == 0) return -1; else return r;
    }
    catch(PGPDataFormatException e) {
      throw(new IOException("Data format exception: " + e.getMessage()));
    }
    catch(PGPFatalDataFormatException e) {
      throw(new IOException("Fatal data format exception: " + e.getMessage()));
    }
  }


  /**
   * This adapter always returns zero here.
   *
   * @return     zero.
   * @exception  IOException  if an I/O error occurs.
   */
  public int available() throws IOException {
    return 0;
  }


  /**
   * This adapter does not implement close, this method does nothing.
   *
   * @exception  IOException  if an I/O error occurs.
   */
  public void close() throws IOException {}



  /**
   * Marking is not supported, this does nothing.
   */
  public synchronized void mark(int readlimit) {}



  /**
   * Marking is not supported, this always throws an exception.
   *
   * @exception IOException is thrown whenever this is called.
   */
  public synchronized void reset() throws IOException {
    throw new IOException("mark/reset not supported");
  }



  /**
   * Marking is not supported, so this always returns false.
   *
   * 
   * @return false
   */
  public boolean markSupported() {
    return false;
  }
  


// Private helper methods
//...........................................................................



  /**
   * @param b an unsigned byte
   * @return the byte represented as an int (0 - 255)
   */
  private static final int byteToInt(byte b) {
    int i = 0;
    if ((b & 0x80) != 0) {
      i |= 0x80;
      b &= 0x7F;
    }
    i |= b;
    return(i);
  }


}
