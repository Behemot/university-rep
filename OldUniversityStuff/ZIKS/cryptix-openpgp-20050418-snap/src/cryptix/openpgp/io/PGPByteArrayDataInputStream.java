/* $Id: PGPByteArrayDataInputStream.java,v 1.2 2005/03/13 17:46:36 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.io;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import java.io.ByteArrayInputStream;
import java.io.IOException;


/**
 * Inputstream to read data from a bytearray.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPByteArrayDataInputStream extends PGPDataInputStream {


// Instance variables
//............................................................................

    /** The underlying inputstream */
    private ByteArrayInputStream in;



// Constructor
//............................................................................

    /** Constructor that takes a byte array */
    public PGPByteArrayDataInputStream(byte[] data) {
        
        in = new ByteArrayInputStream(data);
        
    }
    


// Overridden methods
//............................................................................

    /**
     * Read one byte.
     *
     * @return the byte read
     */
    protected int readInternal()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
        
        int result = in.read();
        if (result == -1) {
            throw new PGPDataFormatException("Read tried past end of array");
        }
        return result;
        
    }


    /**
     * Close the stream.
     *
     * @throws IOException this method does not throw this exception
     * @throws PGPDataFormatException if part of the bytearray has not been
     *         read
     * @throws PGPFatakDataFormatException this method does not throw this
     *         exception
     */
    public void close ()
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {
    
        if (in.available() != 0) {
            throw new PGPDataFormatException("Part of bytearray not read");
        }
    
    }       


}
