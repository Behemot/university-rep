/* $Id: PGPSignatureParameterSpec.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import java.security.spec.AlgorithmParameterSpec;

import java.util.Vector;


/**
 * Parameters for a V4 OpenPGP Signature
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPSignatureParameterSpec implements AlgorithmParameterSpec {

// Instance variables
//..............................................................................

    /** The hashed signature subpackets */
    private Vector hashed;
    /** The unhashed signature subpackets */
    private Vector unhashed;
    /** The signature type byte */
    private byte sigtype;


// Constructor
//..............................................................................

    /**
     * Constructs a PGPSignatureParameterSpec object, given some signature
     * subpackets and a signature type byte.
     */
    public PGPSignatureParameterSpec(Vector hashed, Vector unhashed, 
                                       byte sigtype) 
    {
        this.hashed = hashed;
        this.unhashed = unhashed;
        this.sigtype = sigtype;
    }


// Getters
//..............................................................................

    /**
     * Returns the hashed signature subpackets as a Vector
     */
    public Vector getHashed() {
        return hashed;
    }
       
    /**
     * Returns the unhashed signature subpackets as a Vector
     */ 
    public Vector getUnhashed() {
        return unhashed;
    }
    
    /**
     * Returns the OpenPGP signature type byte
     */
    public byte getSigType() {
        return sigtype;
    }

}
