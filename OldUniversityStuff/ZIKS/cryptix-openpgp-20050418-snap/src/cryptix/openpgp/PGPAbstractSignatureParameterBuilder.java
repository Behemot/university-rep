/* $Id: PGPAbstractSignatureParameterBuilder.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyIDSP;
import cryptix.openpgp.signature.PGPSignatureSubPacket;

import cryptix.pki.KeyID;
import cryptix.pki.KeyIDFactory;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import java.security.spec.AlgorithmParameterSpec;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;


/**
 * Abstract class for signature parameters.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class PGPAbstractSignatureParameterBuilder {

// Instance variables
//..............................................................................

    /** The hashed signature subpackets */
    private Vector hashed;
    /** The unhashed signature subpackets */
    private Vector unhashed;
    /** The signature type byte */
    private byte sigtype;


// Constructors
//..............................................................................

    /**
     * Construct a new PGPAbstractSignatureParameterBuilder using the given
     * issuer key id and signature type byte.
     *
     * @param issuerkeyid the key id of the key issuing the signature
     * @param sigtype     the signature type byte
     */
    protected PGPAbstractSignatureParameterBuilder(KeyID issuerkeyid, 
                                                   byte sigtype) 
    {
        init(issuerkeyid, sigtype);
    }

    /**
     * Construct a new PGPAbstractSignatureParameterBuilder using the given
     * issuer key and signature type byte.
     *
     * <p>
     * Note: this is just a convenience method that will extract the key id from
     * the given key. If you already have the key id, calling the other 
     * constructor with that key id is faster.
     * </p>
     *
     * @param issuerkey the key issuing the signature
     * @param sigtype   the signature type byte
     *
     * @throws InvalidKeyException if getting the key id of the issuer key fails
     *         because of an incompatible or invalid key.
     */
    protected PGPAbstractSignatureParameterBuilder(Key issuerkey, byte sigtype)
        throws InvalidKeyException
    {
        KeyID issuerkeyid;
        try {
            issuerkeyid = 
                KeyIDFactory.getInstance("OpenPGP").generateKeyID(issuerkey);
        } catch (NoSuchAlgorithmException nsae) {
            throw new InvalidKeyException("Could not get KeyIDFactory for " +
                                          "OpenPGP");
        }
        init(issuerkeyid, sigtype);
    }

    /**
     * Helper method for the constructors, which adds the default subpackets
     * for all signatures to the hashed and unhashed areas.
     */
    private void init(KeyID issuerkeyid, byte sigtype)
    {
        this.sigtype = sigtype;
        this.hashed = new Vector();
        this.unhashed = new Vector();

        PGPDateSP creation = new PGPDateSP();
        creation.setValue(new Date());
        creation.setPacketID(PGPSignatureConstants.SSP_SIG_CREATION_TIME);
        
        hashed.addElement(creation);
        
        PGPKeyIDSP keyid = new PGPKeyIDSP();
        keyid.setValue(issuerkeyid.getBytes(8));
        keyid.setPacketID(PGPSignatureConstants.SSP_ISSUER_KEYID);

        unhashed.addElement(keyid);
    }
    

// Instance methods
//..............................................................................

    /**
     * Build an immutable parameter specification
     *
     * <p>
     * This method can be called more than once, which is useful in case you 
     * want to create multiple signatures with almost the same parameters.
     * Note that in this case all parameters will stay the same, including the
     * creation date and time, which may not be desirable. Use the 
     * updateCreationDate() method to prevent this from happening.
     * </p>
     */
    public AlgorithmParameterSpec build() {
        return new PGPSignatureParameterSpec(hashed, unhashed, sigtype);
    }
    
    /**
     * Updates the signature creation date packet to the current date and time
     *
     * <p>
     * This method is simply a wrapper around setCreationDate, calling it with
     * the current date and time.
     * </p>
     */
    public void updateCreationDate() {
        setCreationDate(new Date());
    }
    
    /** 
     * Set the signature creation date packet
     *
     * <p>
     * With this method one can set the signature creation date and time to
     * any random value. While this has useful applications, it should be used
     * carefully because setting it to something else than the current time may
     * break some things.
     * </p>
     *
     * @param creation the new date and time
     */
    public void setCreationDate(Date creation) {
        PGPDateSP pkt = new PGPDateSP();
        pkt.setValue(creation);
        pkt.setPacketID(PGPSignatureConstants.SSP_SIG_CREATION_TIME);
        setPacket(pkt);
    }
    
    /**
     * Set the issuer key id
     *
     * @param issuerkeyid the key id of the key issuing the signature
     */
    public void setIssuerKeyID(KeyID issuerkeyid) {
        PGPKeyIDSP pkt = new PGPKeyIDSP();
        pkt.setValue(issuerkeyid.getBytes(8));
        pkt.setPacketID(PGPSignatureConstants.SSP_ISSUER_KEYID);
        setUnhashedPacket(pkt);
    }
    
    /**
     * Sets the signature type byte
     *
     * @param sigtype the new signature type byte
     */
    public void setSignatureType(byte sigtype) {
        this.sigtype = sigtype;
    }
    

// Vector interface methods
//..............................................................................

    /**
     * Add a packet to the hashed area
     * 
     * <p>
     * This method simply adds the packet to the hashed area, without checking
     * if a packet with the same id already exists. In many cases, using the
     * setPacket(...) method is therefore a better idea.
     * </p>
     *
     * @param ssp the signature subpacket that should be added to the hashed
     *            area.
     */
    public void addPacket(PGPSignatureSubPacket ssp)
    {
        hashed.addElement(ssp);
    }
    
    /**
     * Add or replace a packet into the hashed area
     *
     * <p>
     * If a packet with the same packetid already exists in the hashed area
     * it is replaced by the given packet. Otherwise the given packet is simply
     * added.
     * </p>
     * 
     * @param ssp the signature subpacket that should be added to the hashed
     *            area.
     * @return the packet that was replaced by the new packet, or null if no
     *          packet was replaced and the new packet was just added to the
     *          hashed area.
     */
    public PGPSignatureSubPacket setPacket(PGPSignatureSubPacket ssp) 
    {
        Enumeration em = hashed.elements();

        while (em.hasMoreElements()) {

            PGPSignatureSubPacket tmp = (PGPSignatureSubPacket)em.nextElement();

            if (tmp.getPacketID() == ssp.getPacketID()) {
                int i = hashed.indexOf(tmp);
                hashed.removeElementAt(i);
                hashed.insertElementAt(ssp, i);
                return tmp;
            }
        }

        // not found
        hashed.addElement(ssp);
        return null;
    }
    
    /**
     * Remove the packet in the hashed area with the given packetid
     *
     * <p>
     * Note: if more than one packet with the given packetid exist in the hashed
     * area, then only one (the first one) will be removed.
     * </p>
     *
     * @param packetid the ID of the packet to remove
     * @return the removed packet or null if no packet could be found
     */
    public PGPSignatureSubPacket removePacket(byte packetid)
    {
        Enumeration em = hashed.elements();

        while (em.hasMoreElements()) {
            PGPSignatureSubPacket tmp = (PGPSignatureSubPacket)em.nextElement();
            if (tmp.getPacketID() == packetid) {
                hashed.removeElement(tmp);
                return tmp;
            }
        }
        
        // not found
        return null;
    }
    
    /**
     * Add a packet to the unhashed area
     * 
     * <p>
     * This method simply adds the packet to the unhashed area, without checking
     * if a packet with the same id already exists. In many cases, using the
     * setUnhashedPacket(...) method is therefore a better idea.
     * </p>
     *
     * @param ssp the signature subpacket that should be added to the hashed
     *            area.
     */
    public void addUnhashedPacket(PGPSignatureSubPacket ssp)
    {
        unhashed.addElement(ssp);
    }
    
    /**
     * Add or replace a packet into the unhashed area
     *
     * <p>
     * If a packet with the same packetid already exists in the unhashed area
     * it is replaced by the given packet. Otherwise the given packet is simply
     * added.
     * </p>
     * 
     * @param ssp the signature subpacket that should be added to the unhashed
     *            area.
     * @return the packet that was replaced by the new packet, or null if no
     *          packet was replaced and the new packet was just added to the
     *          unhashed area.
     */
    public PGPSignatureSubPacket setUnhashedPacket(PGPSignatureSubPacket ssp) 
    {
        Enumeration em = unhashed.elements();

        while (em.hasMoreElements()) {

            PGPSignatureSubPacket tmp = (PGPSignatureSubPacket)em.nextElement();

            if (tmp.getPacketID() == ssp.getPacketID()) {
                int i = unhashed.indexOf(tmp);
                unhashed.removeElementAt(i);
                unhashed.insertElementAt(ssp, i);
                return tmp;
            }
        }

        // not found
        unhashed.addElement(ssp);
        return null;
    }
    
    /**
     * Remove the packet in the unhashed area with the given packetid
     *
     * <p>
     * Note: if more than one packet with the given packetid exist in the
     * unhashed area, then only one (the first one) will be removed.
     * </p>
     *
     * @param packetid the ID of the packet to remove
     * @return the removed packet or null if no packet could be found
     */
    public PGPSignatureSubPacket removeUnhashedPacket(byte packetid)
    {
        Enumeration em = unhashed.elements();

        while (em.hasMoreElements()) {
            PGPSignatureSubPacket tmp = (PGPSignatureSubPacket)em.nextElement();
            if (tmp.getPacketID() == packetid) {
                unhashed.removeElement(tmp);
                return tmp;
            }
        }
        
        // not found
        return null;
    }
    
}
