/* $Id: PGPLiteralMessageImpl.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.packet.PGPLiteralDataPacket;

import cryptix.message.LiteralMessage;
import cryptix.message.MessageException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


/**
 * Represents a literal message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPLiteralMessageImpl extends LiteralMessage {


// Instance vars
// ..........................................................................

    private final PGPLiteralDataPacket pkt;
    

// Constructor
// ..........................................................................

    /**
     * Creates a LiteralMessage of the specified format.
     */
    public PGPLiteralMessageImpl(PGPLiteralDataPacket pkt) {
        super("OpenPGP");
        this.pkt = pkt;
    }


// Methods from Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException
    {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            pkt.encode(baos);
            baos.close();
            return baos.toByteArray();
        
        } catch (IOException ioe) {
            throw new RuntimeException(
                "IOException in ByteArrayOutputStream - "+ioe);
        }
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @returns the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
        if (name.equals("filename")) {
        
            return pkt.getFileName();
        
        } else {
            throw new IllegalArgumentException(
                "Attribute ["+name+"] not supported");
        }
    }


// Methods from LiteralMessage
// ..........................................................................

    /**
     * Returns the type of data contained within this message.
     *
     * <p>
     * Can be one of UNKNOWN, BINARY and TEXT.
     * </p><p>
     * If the returned type is UNKNOWN, then usually the underlying format does 
     * not store this information. When this happens, support for retrieving 
     * the data in binary format must be supported by the implementing class, 
     * while support for retrieving data in text format is optional.
     * </p>
     */
    public int getDataType()
    {
        if (pkt.isBinary()) return BINARY;
        if (pkt.isText())   return TEXT;
        if (pkt.isLocal())  return UNKNOWN;
        throw new RuntimeException("PANIC");
    }


    /**
     * Returns the data in binary format as an array of bytes.
     *
     * <p>
     * MUST be supported when getDataType() returns BINARY or UNKNOWN.
     * <br>
     * MAY be supported when getDataType() returns TEXT.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getBinaryData() 
        throws UnsupportedOperationException, MessageException
    {
        return pkt.getBinaryData();
    }


    /**
     * Returns the data in binary format trough an inputstream.
     *
     * <p>
     * MUST be supported when getDataType() returns BINARY or UNKNOWN. 
     * <br>
     * MAY be supported when getDataType() returns TEXT.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public InputStream getBinaryDataInputStream() 
        throws UnsupportedOperationException, MessageException
    {
        // ### FIXME: Do real streaming here
        return new ByteArrayInputStream(pkt.getBinaryData());
    }


    /**
     * Returns the data in text format as a string.
     *
     * <p>
     * MUST be supported when getDataType() returns TEXT.
     * <br>
     * MAY be supported when getDataType() returns BINARY or UNKNOWN.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public String getTextData()
        throws UnsupportedOperationException, MessageException
    {
        return pkt.getTextData();
    }


    /**
     * Returns the data in text format as a character reader.
     *
     * <p>
     * MUST be supported when getDataType() returns TEXT.
     * <br>
     * MAY be supported when getDataType() returns BINARY or UNKNOWN.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public Reader getTextDataReader() 
        throws UnsupportedOperationException, MessageException
    {
        // ### FIXME: Do real streaming here
        return new StringReader(pkt.getTextData());
    }

}
