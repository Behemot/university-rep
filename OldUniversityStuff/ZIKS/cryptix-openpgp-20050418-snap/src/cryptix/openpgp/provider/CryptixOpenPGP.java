/* $Id: CryptixOpenPGP.java,v 1.5 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.util.PGPVersion;

import java.security.Provider;


/**
 * Provider implementation that enables JCA/E-compatible access to OpenPGP
 * functionality.
 *
 * @author  Ingo Luetkebohle
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.5 $
 */
public class CryptixOpenPGP extends Provider {

// Static variables and constants
//...........................................................................

 /* package */ static final String NAME    = "CryptixOpenPGP";
    private    static final String INFO    = "Cryptix OpenPGP Provider";
    private    static final double VERSION = PGPVersion.VERSION;


// Constructor
//...........................................................................

    public CryptixOpenPGP() {

        super(NAME, VERSION, INFO);

        // *** KeyStore / CertStore ***

        put("KeyStore.OpenPGP/Keyring", 
            "cryptix.openpgp.provider.PGPKeyRingKeyStore");
        put("KeyStore.OpenPGP/KeyRing", 
            "cryptix.openpgp.provider.PGPKeyRingKeyStore");

        put("ExtendedKeyStore.OpenPGP/Keyring", 
            "cryptix.openpgp.provider.PGPKeyRingKeyStore");
        put("ExtendedKeyStore.OpenPGP/KeyRing", 
            "cryptix.openpgp.provider.PGPKeyRingKeyStore");

        put("CertStore.OpenPGP/Keyring", 
            "cryptix.openpgp.provider.PGPKeyRingCertStore");
        put("CertStore.OpenPGP/KeyRing", 
            "cryptix.openpgp.provider.PGPKeyRingCertStore");

        put("ExtendedCertStore.OpenPGP/Keyring", 
            "cryptix.openpgp.provider.PGPKeyRingCertStore");
        put("ExtendedCertStore.OpenPGP/KeyRing", 
            "cryptix.openpgp.provider.PGPKeyRingCertStore");
            
        
        
        // *** Sun Certificate API ***      (java.security.cert)

//        put("CertificateFactory.OpenPGP", 
//            "cryptix.openpgp.provider.????");

//        put("CertPathBuilder.OpenPGP", 
//            "cryptix.openpgp.provider.????");
//        put("CertPathValidator.OpenPGP", 
//            "cryptix.openpgp.provider.????");

        
        // *** Sun Cryptography API ***     (java.security)

        put("KeyPairGenerator.OpenPGP/Signing/DSA", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$SigningDSA");
        put("KeyPairGenerator.OpenPGP/Signing/RSA", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$SigningRSA");
        put("KeyPairGenerator.OpenPGP/Signing/ElGamal", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$SigningElGamal");
        put("KeyPairGenerator.OpenPGP/Encryption/RSA", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$EncryptionRSA");
        put("KeyPairGenerator.OpenPGP/Encryption/ElGamal", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$EncryptionElGamal");
        put("KeyPairGenerator.OpenPGP/Legacy/RSA", 
            "cryptix.openpgp.provider.PGPKeyPairGenerator$LegacyRSA");
//        put("KeyPairGenerator.OpenPGP/Signing/ECDSA", 
//            "cryptix.openpgp.provider.????");
        

        // *** Cryptix PKI API ***          (cryptix.pki)
        
        put("PrincipalBuilder.OpenPGP/UserID", 
            "cryptix.openpgp.provider.PGPUserIDPrincipalBuilder");
//        put("PrincipalBuilder.OpenPGP/PhotoID", 
//            "cryptix.openpgp.provider.????");   // plugin?

        put("CertificateBuilder.OpenPGP", 
            "cryptix.openpgp.provider.PGPCertificateBuilder");
        put("CertificateBuilder.OpenPGP/Self", 
            "cryptix.openpgp.provider.PGPSelfCertificateBuilder");
        put("CertificateBuilder.OpenPGP/Legacy", 
            "cryptix.openpgp.provider.PGPLegacyCertificateBuilder");
        put("CertificateBuilder.OpenPGP/Legacy/Self", 
            "cryptix.openpgp.provider.PGPLegacySelfCertificateBuilder");

        put("KeyBundleFactory.OpenPGP", 
            "cryptix.openpgp.provider.PGPKeyBundleFactory");

        put("KeyIDFactory.OpenPGP", 
            "cryptix.openpgp.provider.PGPKeyIDFactory");

        
        // *** Cryptix Message API ***      (cryptix.message)

        put("EncryptedMessageBuilder.OpenPGP", 
            "cryptix.openpgp.provider.PGPEncryptedMessageBuilder");
        put("EncryptedMessageBuilder.OpenPGP/Legacy", 
            "cryptix.openpgp.provider.PGPEncryptedMessageBuilder$Legacy");
        put("KeyBundleMessageBuilder.OpenPGP", 
            "cryptix.openpgp.provider.PGPKeyBundleMessageBuilder");
        put("LiteralMessageBuilder.OpenPGP", 
            "cryptix.openpgp.provider.PGPLiteralMessageBuilder");
        put("SignedMessageBuilder.OpenPGP", 
            "cryptix.openpgp.provider.PGPSignedMessageBuilder$V4");
        put("SignedMessageBuilder.OpenPGP/V3", 
            "cryptix.openpgp.provider.PGPSignedMessageBuilder$V3");
        put("SignedMessageBuilder.OpenPGP/Legacy", 
            "cryptix.openpgp.provider.PGPSignedMessageBuilder$Legacy");

        put("MessageFactory.OpenPGP", 
            "cryptix.openpgp.provider.PGPMessageFactory");
            

        // *** Cryptix Stream API ***       (cryptix.message.stream)

        put("DecodedMessageInputStream.OpenPGP", 
            "cryptix.openpgp.provider.PGPDecodedMessageInputStream");
            
        put("EncryptedMessageOutputStream.OpenPGP", 
            "cryptix.openpgp.provider.PGPEncryptedMessageOutputStream");
        put("LiteralMessageOutputStream.OpenPGP", 
            "cryptix.openpgp.provider.PGPLiteralMessageOutputStream");
        put("SignedMessageOutputStream.OpenPGP", 
            "cryptix.openpgp.provider.PGPSignedMessageOutputStream$V4");
        put("SignedMessageOutputStream.OpenPGP/V3", 
            "cryptix.openpgp.provider.PGPSignedMessageOutputStream$V3");

    }

}
