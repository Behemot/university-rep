/* $Id: PGPLiteralMessageOutputStream.java,v 1.1 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.stream.LiteralMessageOutputStreamSpi;
import cryptix.message.stream.MessageOutputStream;
import cryptix.message.stream.MessageStreamException;

import java.io.IOException;
import java.io.OutputStream;

import java.security.SecureRandom;


/**
 * OpenPGP implementation of the LiteralMessageOutputStream service provider
 * interface.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPLiteralMessageOutputStream 
    implements LiteralMessageOutputStreamSpi
{

// Instance variables
//..............................................................................

    /** The underlying outputstream */
    private OutputStream out;
    
    /** Only used when the underlying outputstream is implemented with a 
      * PGPSignedMessageOutputStream */
    private PGPSignedMessageOutputStream sigout;
    private int nodata = 0;
    
    /** Unused */
    private SecureRandom sr;
    
    /** Indicates if data has already been written to the underlying stream */
    private boolean nodatawritten = true;
    
    /** Buffer */
    private byte[] buf = new byte[1 << 13]; // 2^13 = 8192
    
    /** Encoding of the size of the buffer */
    private int partialLengthByte = 224 + 13;

    /** Offset within buffer */
    private int bufoffset = 0;
    
    

// Methods from MessageOutputStreamSpi
//..............................................................................

    /**
     * Initializes this outputstream with the given outputstream as underlying
     * stream and the given SecureRandom object.
     *
     * @param out The underlying outputstream this stream will write results to.
     * @param sr  A SecureRandom object used for any randomness needed.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineInit(OutputStream out, SecureRandom sr)
        throws IllegalStateException, IOException, MessageStreamException
    {
        if (this.out != null) 
            throw new IllegalStateException("Already called init before");
        if (out == null) 
            throw new IllegalArgumentException("Outputstream cannot be null");
        
        this.out = out;
        this.sr  = sr;
        
        this.sigout = null;
        if (out instanceof MessageOutputStream) {
            if (((MessageOutputStream)out).getSpi() 
                    instanceof PGPSignedMessageOutputStream)
            {
                this.sigout = (PGPSignedMessageOutputStream)
                              ((MessageOutputStream)out).getSpi();
            }
        }

        buf[0] = (byte)'b';  // binary       ### FIXME
        buf[1] = 0;          // no file name ### FIXME
        buf[2] = 0;          // current time ### FIXME
        buf[3] = 0;          // current time ### FIXME
        buf[4] = 0;          // current time ### FIXME
        buf[5] = 0;          // current time ### FIXME
        bufoffset = 6;
        nodata = 6;
    }
        
    /**
     * Set a format specific attribute.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageStreamException
    {
        throw new IllegalArgumentException("No attributes supported");
    }

    /**
     * Write the specified part of a bytearray to the stream.
     *
     * @param b    the data
     * @param off  the starting point of the write operation
     * @param len  how many bytes to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b, int off, int len)
        throws IOException, MessageStreamException
    {
        if (out == null) 
            throw new IllegalStateException("Not yet initialized");

        if (nodatawritten) {
            if (sigout != null)
                sigout.writeNoData(0xCB);
            else
                out.write(0xCB); // new format packet, type 11: literaldata
            nodatawritten = false;
        }

        while (bufoffset + len > buf.length)
        {
            System.arraycopy(b, off, buf, bufoffset, buf.length - bufoffset);

            if (sigout != null)
                sigout.writeNoData(partialLengthByte);
            else
                out.write(partialLengthByte);

            if ((sigout != null) && (nodata != 0)) {
                for (int i=0; i<nodata; i++)
                    sigout.writeNoData(buf[i]);
                out.write(buf, nodata, buf.length-nodata);
                nodata = 0;
            } else {
                out.write(buf, 0, buf.length);
            }

            off += (buf.length - bufoffset);
            len -= (buf.length - bufoffset);
            bufoffset = 0;
        }
        System.arraycopy(b, off, buf, bufoffset, len);
        bufoffset += len;
    }

    /**
     * Write a complete bytearray to the stream
     *
     * @param b  the data
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b)
        throws IOException, MessageStreamException
    {
        engineWrite(b, 0, b.length);
    }
    
    /**
     * Write a single byte to the stream
     *
     * @param b  the byte to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(int b)
        throws IOException, MessageStreamException
    {
        byte[] b1 = new byte[1];
        b1[0] = (byte)b;
        engineWrite(b1, 0, 1);
    }
    
    /**
     * Flush the stream
     *
     * <p>
     * Only flushed the underlying outputstream. We could do some more, but 
     * because writing a buffer with a size that is not a power of two is a 
     * pain, we won't.
     * </p>
     * 
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineFlush()
        throws IOException, MessageStreamException
    {
        out.flush();
    }
    
    /**
     * Close the stream
     *
     * <p>
     * Sends all data through the underlying stream and then calls the close()
     * method of the underlying stream.
     * </p>
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineClose()
        throws IOException, MessageStreamException
    {
        // if we never wrote any partial data, write the type byte first
        if (nodatawritten) {
            if (sigout != null)
                sigout.writeNoData(0xCB);
            else
                out.write(0xCB); // new format packet, type 11: literaldata
        }

        // write final packet length
        if (sigout != null) {
            if (bufoffset < 192) {
                sigout.writeNoData((byte)bufoffset);
            } else if (bufoffset < 8384) {
                sigout.writeNoData(192 + (byte)((bufoffset-192)>>8));
                sigout.writeNoData((byte)(bufoffset-192));
            } else {
                sigout.writeNoData(255);
                sigout.writeNoData((byte)(bufoffset >> 24));
                sigout.writeNoData((byte)(bufoffset >> 16));
                sigout.writeNoData((byte)(bufoffset >>  8));
                sigout.writeNoData((byte)(bufoffset));
            }
        } else {
            if (bufoffset < 192) {
                out.write((byte)bufoffset);
            } else if (bufoffset < 8384) {
                out.write(192 + (byte)((bufoffset-192)>>8));
                out.write((byte)(bufoffset-192));
            } else {
                out.write(255);
                out.write((byte)(bufoffset >> 24));
                out.write((byte)(bufoffset >> 16));
                out.write((byte)(bufoffset >>  8));
                out.write((byte)(bufoffset));
            }
        }
        
        // write data
        if ((sigout != null) && (nodata != 0)) {
            for (int i=0; i<nodata; i++)
                sigout.writeNoData(buf[i]);
            out.write(buf, nodata, bufoffset-nodata);
        } else {
            out.write(buf, 0, bufoffset);
        }
        
        // close underlying
        out.close();
    }

}
