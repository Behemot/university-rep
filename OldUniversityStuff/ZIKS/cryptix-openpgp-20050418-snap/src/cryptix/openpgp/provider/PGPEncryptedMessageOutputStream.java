/* $Id: PGPEncryptedMessageOutputStream.java,v 1.1 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.stream.EncryptedMessageOutputStreamSpi;
import cryptix.message.stream.MessageOutputStream;
import cryptix.message.stream.MessageStreamException;

import cryptix.openpgp.PGPKeyBundle;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPEncryptor;

import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPPublicSubKeyPacket;
import cryptix.openpgp.packet.PGPPublicKeyEncryptedSessionKeyPacket;
import cryptix.openpgp.packet.PGPSymmetricKeyEncryptedSessionKeyPacket;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;

import java.io.IOException;
import java.io.OutputStream;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;

import java.util.Iterator;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * OpenPGP implementation of the EncryptedMessageOutputStream service provider
 * interface.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPEncryptedMessageOutputStream 
    implements EncryptedMessageOutputStreamSpi
{

// Instance variables
//..............................................................................

    /** The underlying outputstream */
    private OutputStream out;
    
    /** Source of randomness */
    private SecureRandom sr;
    
    /** Indicates if data has already been written to the underlying stream */
    private boolean nodatawritten = true;
    
    /** Buffer */
    private byte[] buf = new byte[1 << 13]; // 2^13 = 8192
    
    /** Encoding of the size of the buffer */
    private int partialLengthByte = 224 + 13;

    /** Offset within buffer */
    private int bufoffset = 0;
    
    /** Encrypting cipher */
    private Cipher cipher;
    private byte datacipher;
    private byte[] sessionkey;
    
    
// Methods from EncryptedMessageOutputStreamSpi
//..............................................................................

    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(PublicKey rcpt)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        PGPKeyPacket keypkt =
            (PGPKeyPacket)((PGPPublicKey)rcpt).getPacket();
        PGPEncryptor cryptor = (PGPEncryptor)keypkt.getAlgorithm();
        PGPPublicKeyEncryptedSessionKeyPacket pkt = 
            new PGPPublicKeyEncryptedSessionKeyPacket();
        pkt.setSessionKey(sessionkey, datacipher);
        pkt.encrypt(cryptor, sr);

        pkt.setPacketID(
            PGPPacketConstants.PKT_PUBLIC_KEY_ENCRYPTED_SESSION_KEY);
        pkt.setPublicKeyAlgorithmID(keypkt.getAlgorithmID());
        
        KeyID keyid;
        try {
            keyid = PGPKeyIDFactory.convert((PublicKey)rcpt);
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new MessageStreamException(""+ike);
        }
        pkt.setKeyID(keyid.getBytes(8));

        pkt.encode(out);
    }


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(KeyBundle rcpt)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        Iterator it2 = ((PGPKeyBundle)rcpt).getPublicSubKeys();
        PGPPublicKey publickey = null;
        while (it2.hasNext()) {
            // ### FIXME: Find the right subkey here, do not just use
            // the last available one.
            PGPPublicKey temp = (PGPPublicKey)it2.next();
            publickey = temp;
        }
        
        PGPEncryptor cryptor;
        int id;
        
        if (publickey == null) {
            
            // try the main key
            it2 = ((PGPKeyBundle)rcpt).getPublicKeys();
            publickey = (PGPPublicKey)it2.next();
            PGPPublicKeyPacket keypkt = 
                (PGPPublicKeyPacket)publickey.getPacket();
            id = keypkt.getAlgorithmID();
            try {
                // ### FIXME: check flags if this key can be used for 
                // encryption.
                cryptor = (PGPEncryptor)keypkt.getAlgorithm();
            } catch (ClassCastException cce) {
                throw new MessageStreamException("No encryption key found.");
            }
            
        } else {
            
            PGPPublicSubKeyPacket keypkt = 
                (PGPPublicSubKeyPacket)publickey.getPacket();
            id = keypkt.getAlgorithmID();
            cryptor = (PGPEncryptor)keypkt.getAlgorithm();
        }
        
        if (publickey == null) {
            throw new MessageStreamException("No encryption key found.");
        }
        
        PGPPublicKeyEncryptedSessionKeyPacket pkt = 
            new PGPPublicKeyEncryptedSessionKeyPacket();
        pkt.setSessionKey(sessionkey, datacipher);
        pkt.encrypt(cryptor, sr);
        
        pkt.setPacketID(
            PGPPacketConstants.PKT_PUBLIC_KEY_ENCRYPTED_SESSION_KEY);
        pkt.setPublicKeyAlgorithmID(id);
        
        KeyID keyid;
        try {
            keyid = PGPKeyIDFactory.convert(publickey);
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new MessageStreamException(""+ike);
        }
        pkt.setKeyID(keyid.getBytes(8));
    
        pkt.encode(out);
    }


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(SecretKey rcpt)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        throw new UnsupportedOperationException(
            "Symmetric key encryption not supported.");
    }


    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(String rcpt)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        byte s2kid  = 3;     // Iterated and Salted    ### FIXME
        byte hashid = 2;     // SHA-1                  ### FIXME
    
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        PGPSymmetricKeyEncryptedSessionKeyPacket pkt = 
            new PGPSymmetricKeyEncryptedSessionKeyPacket();
        pkt.setDirect((String)rcpt, factory, sr, datacipher, s2kid, 
                      hashid);
        pkt.setPacketID(
            PGPPacketConstants.PKT_SYMMETRIC_KEY_ENCRYPTED_SESSION_KEY);

        pkt.encode(out);
    }

// Methods from MessageOutputStreamSpi
//..............................................................................

    /**
     * Initializes this outputstream with the given outputstream as underlying
     * stream and the given SecureRandom object.
     *
     * @param out The underlying outputstream this stream will write results to.
     * @param sr  A SecureRandom object used for any randomness needed.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineInit(OutputStream out, SecureRandom sr)
        throws IllegalStateException, IOException, MessageStreamException
    {
        if (this.out != null) 
            throw new IllegalStateException("Already called init before");
        if (out == null) 
            throw new IllegalArgumentException("Outputstream cannot be null");
        
        this.out = out;
        this.sr  = sr;
        
        this.datacipher = 2;                // Triple DES   ### FIXME
        
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        int keysize, blocksize;
        try {
            this.cipher = factory.getCipherAlgorithm(datacipher, "OpenpgpCFB");
            keysize = factory.getCipherKeySize(datacipher);
            blocksize = factory.getCipherBlockSize(datacipher);
        } catch (NoSuchAlgorithmException nsae) {
            throw new MessageStreamException(""+nsae);
        }
        
        // generate key
        this.sessionkey = new byte[keysize]; 
        sr.nextBytes(sessionkey);
        StringTokenizer st = new StringTokenizer(cipher.getAlgorithm(), "/");
        SecretKeySpec keyspec = new SecretKeySpec(sessionkey, st.nextToken());
    
        // set iv
        byte[] iv = new byte[blocksize];
        for (int j=0; j<iv.length; j++) { iv[j] = 0; }
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        // init the Cipher
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new InternalError("InvalidKeyException on "+
                                    "encrypting a key - "+ike);
        } catch (InvalidAlgorithmParameterException iape) {
            iape.printStackTrace();
            throw new InternalError(
                "InvalidAlgorithmParameterException "+
                "on encrypting a key - "+iape);
        }

        // fill buffer with prefix
        byte[] prefix = new byte[blocksize+2];
        sr.nextBytes(prefix);
        
        prefix[blocksize]     = prefix[blocksize - 2]; // repeat last two
        prefix[blocksize + 1] = prefix[blocksize - 1]; // repeat last two
        
        byte[] ciphdata = cipher.update(prefix);
        System.arraycopy(ciphdata, 0, buf, 0, ciphdata.length);
        bufoffset = ciphdata.length;
    }
        
    /**
     * Set a format specific attribute.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageStreamException
    {
        throw new IllegalArgumentException("No attributes supported");
    }

    /**
     * Write the specified part of a bytearray to the stream.
     *
     * @param b    the data
     * @param off  the starting point of the write operation
     * @param len  how many bytes to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b, int off, int len)
        throws IOException, MessageStreamException
    {
        if (out == null) 
            throw new IllegalStateException("Not yet initialized");

        if (nodatawritten) {
            out.write(0xC9); // new format packet, type 9: symm. encr. data
            nodatawritten = false;
        }
        
        byte[] c = cipher.update(b, off, len);
        if (c == null) c = new byte[0];

        len = c.length;
        off = 0;

        while (bufoffset + len > buf.length)
        {
            System.arraycopy(c, off, buf, bufoffset, buf.length - bufoffset);

            out.write(partialLengthByte);

            out.write(buf, 0, buf.length);

            off += (buf.length - bufoffset);
            len -= (buf.length - bufoffset);
            bufoffset = 0;
        }
        System.arraycopy(c, off, buf, bufoffset, len);
        bufoffset += len;
    }

    /**
     * Write a complete bytearray to the stream
     *
     * @param b  the data
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b)
        throws IOException, MessageStreamException
    {
        engineWrite(b, 0, b.length);
    }
    
    /**
     * Write a single byte to the stream
     *
     * @param b  the byte to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(int b)
        throws IOException, MessageStreamException
    {
        byte[] b1 = new byte[1];
        b1[0] = (byte)b;
        engineWrite(b1, 0, 1);
    }
    
    /**
     * Flush the stream
     *
     * <p>
     * Only flushed the underlying outputstream. We could do some more, but 
     * because writing a buffer with a size that is not a power of two is a 
     * pain, we won't.
     * </p>
     * 
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineFlush()
        throws IOException, MessageStreamException
    {
        out.flush();
    }
    
    /**
     * Close the stream
     *
     * <p>
     * Sends all data through the underlying stream and then calls the close()
     * method of the underlying stream.
     * </p>
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineClose()
        throws IOException, MessageStreamException
    {
        // if we never wrote any partial data, write the type byte first
        if (nodatawritten) {
            out.write(0xC9); // new format packet, type 9: symm. encr. data
            nodatawritten = false;
        }
        
        // write final packet length
        if (bufoffset < 192) {
            out.write((byte)bufoffset);
        } else if (bufoffset < 8384) {
            out.write(192 + (byte)((bufoffset-192)>>8));
            out.write((byte)(bufoffset-192));
        } else {
            out.write(255);
            out.write((byte)(bufoffset >> 24));
            out.write((byte)(bufoffset >> 16));
            out.write((byte)(bufoffset >>  8));
            out.write((byte)(bufoffset));
        }
        
        // write data
        out.write(buf, 0, bufoffset);
        
        // close underlying
        out.close();
    }

}
