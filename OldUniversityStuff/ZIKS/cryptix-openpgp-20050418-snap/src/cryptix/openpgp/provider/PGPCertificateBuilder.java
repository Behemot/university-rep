/* $Id: PGPCertificateBuilder.java,v 1.4 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPCertificateParameterBuilder;
import cryptix.openpgp.PGPKeyBundle;
import cryptix.openpgp.PGPPrincipal;
import cryptix.openpgp.PGPPrivateKey;
import cryptix.openpgp.PGPPublicKey;
import cryptix.openpgp.PGPSignatureParameterSpec;
import cryptix.openpgp.PGPV3SignatureParameterSpec;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.io.PGPHashDataOutputStream;

import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPSignatureConstants;
import cryptix.openpgp.packet.PGPUserIDPacket;

import cryptix.openpgp.signature.PGPSignatureSubPacket;
import cryptix.openpgp.signature.PGPDateSP;

import cryptix.pki.CertificateBuilderSpi;
import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import java.security.spec.AlgorithmParameterSpec;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;


/**
 * Service provider interface for CertificateBuilder
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public class PGPCertificateBuilder extends CertificateBuilderSpi {

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     */
    public Certificate engineBuild(PublicKey subjectKey, 
                                   Principal subjectName, 
                                   PrivateKey issuer, SecureRandom sr) 
        throws CertificateException
    {
        try {
            return engineBuild(subjectKey, subjectName, issuer, sr, 
                               getDefaultParameterSpec(issuer));
        } catch (InvalidAlgorithmParameterException iape) {
            throw new CertificateException("Invalid default parameters. "+iape);
        } catch (InvalidKeyException ike) {
            throw new CertificateException("Invalid issuer key type. "+ike);
        }
    }
    
    
    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public Certificate engineBuild(PublicKey subjectKey, 
                                   Principal subjectName, 
                                   KeyBundle issuer, char[] passphrase,
                                   SecureRandom sr) 
        throws CertificateException, UnrecoverableKeyException
    {
        if (! (issuer instanceof PGPKeyBundle)) {
            throw new CertificateException(
                "Issuer not instance of PGPKeyBundle");
        }
        
        PublicKey temppubkey = (PublicKey)issuer.getPublicKeys().next();
        PrivateKey tempkey = issuer.getPrivateKey(temppubkey, passphrase);

        if (! (tempkey instanceof PGPPrivateKey)) {
            throw new CertificateException(
                "Issuer does not contain a private key.");
        }
        
        return engineBuild(subjectKey, subjectName, tempkey, sr);
    }
    
    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer and with the given parameters.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     */
    public Certificate engineBuild(PublicKey subjectKey, 
                                   Principal subjectName, 
                                   PrivateKey issuer, SecureRandom sr,
                                   AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException
    {
        // checks
        if (! (subjectKey instanceof PGPPublicKey)) {
            throw new CertificateException(
                "Subject key not instance of PGPPublicKey");
        }
        if (! (subjectName instanceof PGPPrincipal)) {
            throw new CertificateException(
                "Subject name not instance of PGPPrincipal");
        }
        if (! (issuer instanceof PGPPrivateKey)) {
            throw new CertificateException(
                "Issuer not instance of PGPPrivateKey");
        }
        
        // typecasts
        PGPPrivateKey signkey = (PGPPrivateKey)issuer;
        
        // init
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        byte algoid = signkey.getPacket().getAlgorithmID();
        PGPSignaturePacket pkt = new PGPSignaturePacket();
        int hashid;
        byte sigtype;
        
        if (algSpec instanceof PGPSignatureParameterSpec) {
            
            PGPSignatureParameterSpec sigSpec = 
                (PGPSignatureParameterSpec)algSpec;
            hashid = 2;    // SHA-1   ### FIXME: find preferences
        
            // get subpackets and sig type from spec
            Vector hashed = sigSpec.getHashed();
            Vector newhashed = new Vector();
            Vector unhashed = sigSpec.getUnhashed();
            sigtype = sigSpec.getSigType();
            
            // make expiration date relative
            Enumeration enum = hashed.elements();
            while (enum.hasMoreElements()) {
                PGPSignatureSubPacket ssp = 
                    (PGPSignatureSubPacket)enum.nextElement();
                if (ssp.getPacketID() == 
                    PGPSignatureConstants.SSP_KEY_EXPIRATION_TIME) 
                {
                    Date expire = ((PGPDateSP)ssp).getValue();
                    Date create = ((PGPPublicKey)subjectKey
                                    ).getPacket().getCreationDate();
                    long delta = expire.getTime() - create.getTime();
                    if (delta < 0) {
                        throw new InvalidAlgorithmParameterException(
                            "Key expires before it is created");
                    }
                    PGPDateSP newssp = new PGPDateSP();
                    newssp.setPacketID(ssp.getPacketID());
                    newssp.setCritical(ssp.getCritical());
                    newssp.setValue(new Date(delta));
                    newhashed.add(newssp);
                }
                else
                {
                    newhashed.add(ssp);
                }
            }
            
            // create signature packet
            pkt.setData(sigtype, (byte)algoid,
                       (byte)hashid, newhashed, unhashed);
            pkt.setPacketID(PGPPacketConstants.PKT_SIGNATURE);

        } else if (algSpec instanceof PGPV3SignatureParameterSpec) {
            
            hashid = 1;     // MD5
            PGPV3SignatureParameterSpec sigSpec = 
                (PGPV3SignatureParameterSpec)algSpec;
            sigtype = sigSpec.getSigType();
            
            // create signature packet
            pkt.setData(sigtype, sigSpec.getTime(), sigSpec.getIssuer(), 
                        (byte)algoid, (byte)hashid);
            pkt.setPacketID(PGPPacketConstants.PKT_SIGNATURE);
            
        } else {
            
            throw new InvalidAlgorithmParameterException("Expected "+
                "PGPSignatureParameterSpec or PGPV3SignatureParameterSpec, "+
                "got "+algSpec.getClass().toString());
        
        }

        // init 
        PGPPublicKey        pubkey  = (PGPPublicKey)subjectKey;
        PGPPrincipal        subject = (PGPPrincipal)subjectName;
        PGPPublicKeyPacket  keypkt  = (PGPPublicKeyPacket)pubkey.getPacket();
        PGPUserIDPacket     idpkt   = (PGPUserIDPacket)subject.getPacket();
        PGPSigner           signer  = (PGPSigner)
                                      signkey.getPacket().getAlgorithm();
        
        MessageDigest md;
        try {
            md = factory.getHashAlgorithm(hashid);
        } catch (NoSuchAlgorithmException nsae) {
            throw new CertificateException(""+nsae);
        }

        signer.initSign(hashid, factory);
        

        // hash the key
        PGPHashDataOutputStream hdos = 
            new PGPHashDataOutputStream(md, signer);
        try {
            keypkt.encodeBody(hdos);
            hdos.close();
        } catch (IOException ioe) {
            throw new InternalError("IOException on hashing key - "+ioe);
        }
        

        // hash the userID
        byte[] useridbytes;
        try {
            useridbytes = idpkt.getValue().getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported.");
        }
        if (pkt.getVersion() == 4) {
            byte[] pre = new byte[5];
            pre[0] = (byte)0xB4;
            pre[1] = (byte)((useridbytes.length >> 24) & 0xFF);
            pre[2] = (byte)((useridbytes.length >> 16) & 0xFF);
            pre[3] = (byte)((useridbytes.length >>  8) & 0xFF);
            pre[4] = (byte)((useridbytes.length      ) & 0xFF);
            md.update(pre);
            signer.update(pre);
        }
        md.update(useridbytes);
        signer.update(useridbytes);


        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);
        

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }


        // store hash
        byte[] digestcalc  = md.digest();
        pkt.setHash(digestcalc);
        
        // sign
        signer.computeSignature();
        pkt.setSignature(signer);
        
        
        return new PGPCertificateImpl(pkt, subject, pubkey);
    }
        

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer and with the given parameters.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public Certificate engineBuild(PublicKey subjectKey, 
                                   Principal subjectName, 
                                   KeyBundle issuer, char[] passphrase,
                                   SecureRandom sr,
                                   AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException,
               UnrecoverableKeyException
    {
        if (! (issuer instanceof PGPKeyBundle)) {
            throw new CertificateException(
                "Issuer not instance of PGPKeyBundle");
        }
        
        PublicKey temppubkey = (PublicKey)issuer.getPublicKeys().next();
        PrivateKey tempkey = issuer.getPrivateKey(temppubkey, passphrase);

        if (! (tempkey instanceof PGPPrivateKey)) {
            throw new CertificateException(
                "Issuer does not contain a private key.");
        }
        
        return engineBuild(subjectKey, subjectName, tempkey, sr, algSpec);
    }
        

    /** ### Blah */
    protected AlgorithmParameterSpec 
        getDefaultParameterSpec(PrivateKey issuer)
        throws InvalidKeyException
    {
        return (new PGPCertificateParameterBuilder(issuer)).build();
    }
        
}
