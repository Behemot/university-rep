/* $Id: PGPDecodedMessageInputStream.java,v 1.3 2005/04/18 12:10:26 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.*;
import cryptix.message.stream.*;
import cryptix.openpgp.*;
import cryptix.openpgp.algorithm.*;
import cryptix.openpgp.io.*;
import cryptix.openpgp.packet.*;
import cryptix.openpgp.signature.*;
import cryptix.pki.*;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;

// ### FIXME: this class is ugly and bulky

public class PGPDecodedMessageInputStream
    implements DecodedMessageInputStreamSpi
{
    private InputStream in = null;
    
    private InputStream encin = null;
    private MessageDigest mdc = null;
    
    private int verificationResult = 0;
    private int integrityResult = 0;
    
    public void engineInit(InputStream in, 
        DecryptionKeyCallback dkc, VerificationKeyCallback vkc)
        throws IllegalStateException, IOException, MessageStreamException
    {
        PGPPacketFactory    pf = PGPPacketFactory.getDefaultInstance();
        PGPAlgorithmFactory af = PGPAlgorithmFactory.getDefaultInstance();

        int id, len, type;
        boolean old;

        Vector sessionkeys = new Vector();
        MessageDigest md = null;
        boolean hasmdc = false;
        PGPSigner signer = null;
        PGPSignaturePacket sigpkt = null;
        PGPPacket pkt;
        
        while (true) 
        {
            try {
                type = in.read();
            } catch (IOException ioe) {
                throw new IOException("Error reading packet type: "+
                    ioe.toString());
            }
            
            if (type == -1) {
                throw new MessageStreamException(
                    "End of stream reached before data.");
            }
    
            if (type < 128) {
                throw new MessageStreamException("Invalid packet type.");
            }
    
            if (type >= 192) {   // new format
                id = type & 0x3F;
                old = false;
                len = -1;
            } else {             // old format
                id = (type >> 2) & 0x0F;
                old = true;
                len = type & 0x03;
            }
            
            pkt = pf.getPacket(id);
            
            if (pkt instanceof PGPLiteralDataPacket) {

                try {
                    if ((signer == null) && (md == null) && (mdc == null)) {
                        this.in = new LiteralInputStream(in, old, len);
                    } else {
                        this.in = new SignedLiteralInputStream(in, old, len, 
                                                           signer, md, sigpkt);
                    }
                } catch (PGPDataFormatException pdfe) {
                    throw new MessageStreamException(""+pdfe);
                } catch (PGPFatalDataFormatException pdfe) {
                    throw new MessageStreamException(""+pdfe);
                }
                return;

            } else if (pkt instanceof PGPCompressedDataPacket) {

                PGPPacketDataInputStream pin = 
                    new PGPPacketDataInputStream (in, old, len);
                
                try {

                    int alg = (int)pin.readUnsignedByte();
                    PGPCompressor cp = af.getCompressionAlgorithm(alg);
                    if (cp.needsDummy())
                        in = cp.getExpansionStream(
                            new PGPZLibInputStreamAdapter(pin));
                    else
                        in = cp.getExpansionStream(
                            new PGPInputStreamAdapter(pin));
                
                } catch (PGPDataFormatException pdfe) {
                    pdfe.printStackTrace();
                    throw new MessageStreamException("Error reading message "+
                        "compression type "+pdfe);
                } catch (PGPFatalDataFormatException pdfe) {
                    throw new MessageStreamException("Error reading message "+
                        "compression type "+pdfe);
                } catch (NoSuchAlgorithmException nsae) {
                    throw new MessageStreamException("compression id not "+
                        "found - "+nsae);
                }

            } else if (pkt instanceof PGPEncryptedDataPacket) {

                if (pkt instanceof 
                    PGPSymmetricallyEncryptedIntegrityProtectedDataPacket)
                {
                    hasmdc = true;
                    try {
                        mdc = MessageDigest.getInstance("SHA-1");
                    } catch (NoSuchAlgorithmException nsae) {
                        throw new RuntimeException("Could not find SHA-1");
                    }
                }

                int count = 0;
                for (int i=0; i<sessionkeys.size(); i++) {
                    PGPSessionKeyPacket skp = 
                        (PGPSessionKeyPacket)sessionkeys.elementAt(i);
                    if (skp instanceof PGPPublicKeyEncryptedSessionKeyPacket) {
                        count++;
                    }
                }
                
                KeyID[] hints = new KeyID[count*2];
                int pos = 0;
                for (int i=0; i<sessionkeys.size(); i++) {
                    PGPSessionKeyPacket skp = 
                        (PGPSessionKeyPacket)sessionkeys.elementAt(i);
                    if (skp instanceof PGPPublicKeyEncryptedSessionKeyPacket) {
                        hints[pos++] = new PGPKeyIDImpl(null, 
                            ((PGPPublicKeyEncryptedSessionKeyPacket)
                            skp).getKeyID(), 3);
                        hints[pos++] = new PGPKeyIDImpl(null, 
                            ((PGPPublicKeyEncryptedSessionKeyPacket)
                            skp).getKeyID(), 4);
                    }
                }
                
                DecryptionKeyReturnValue last = null;
                int retryCount = 0;
                
                PGPSessionKey decryptkey = null;
                
                while( decryptkey == null ) {

                    DecryptionKeyReturnValue val = dkc.getDecryptionKey(
                        new DecryptionKeyRequest(hints, retryCount, last));
                    
                    if (val.getError() == DecryptionKeyReturnValue.FAIL)
                        throw new MessageStreamException(
                            "Failed retrieving decryption key");
                
                    if (val.getBundle() != null) 
                    {
                        PGPKeyBundle pgpbundle = (PGPKeyBundle)val.getBundle();
                        
                        Iterator it = pgpbundle.getPublicSubKeys();
                        
                        while (it.hasNext()) {
                        
                            PGPPublicKey pubsubkey = 
                                (PGPPublicKey)it.next();
                            PGPPrivateKey privsubkey;
                            try {
                                
                                privsubkey = (PGPPrivateKey)
                                    pgpbundle.getPrivateSubKey(
                                    pubsubkey, val.getPassphrase());
                                    
                                PGPEncryptor cryptor = (PGPEncryptor)
                                    privsubkey.getPacket().getAlgorithm();
                                
                                for (int i=0; i<sessionkeys.size(); i++) {
                                    if (sessionkeys.elementAt(i) instanceof 
                                        PGPPublicKeyEncryptedSessionKeyPacket) 
                                    {
                                        PGPPublicKeyEncryptedSessionKeyPacket 
                                            pkeskp = 
                                        (PGPPublicKeyEncryptedSessionKeyPacket)
                                            sessionkeys.elementAt(i);
                                        
                                        boolean success = false;
                                        
                                        pkeskp.decrypt(cryptor);
                                        success = true;
                                        
                                        
                                        if (success) {
                                            decryptkey = pkeskp.getSessionKey();
                                        }
                                    }
                                }

                            } catch (UnrecoverableKeyException uke) {
                            } catch (PGPDataFormatException e) {
                            } catch (PGPDecryptionFailedException e) {
                            }
                        
                        }
                    
                    } else if (val.getPrivateKey() != null) {
                        
                        PGPEncryptor cryptor = (PGPEncryptor)((PGPKey)
                            val.getPrivateKey()).getPacket().getAlgorithm();
                        
                        for (int i=0; i<sessionkeys.size(); i++) {
                            if (sessionkeys.elementAt(i) instanceof 
                                PGPPublicKeyEncryptedSessionKeyPacket) 
                            {
                                PGPPublicKeyEncryptedSessionKeyPacket pkeskp = 
                                    (PGPPublicKeyEncryptedSessionKeyPacket)
                                    sessionkeys.elementAt(i);
                                
                                boolean success = false;
                                
                                try {
                                    
                                    pkeskp.decrypt(cryptor);
                                    success = true;
                                
                                } catch (PGPDataFormatException e) {
                                } catch (PGPDecryptionFailedException e) {
                                }
                                
                                if (success) {
                                    decryptkey = pkeskp.getSessionKey();
                                }
                            }
                        }
                        
                    } else {
                        
                        throw new MessageStreamException(
                            "Invalid decryption key callback result");
                        
                    }
                }                
                
                // get the key
                byte[] key = decryptkey.getBytes();
            
                // retrieve id and block/keysize
                boolean failed = false;
                byte cpid = decryptkey.getCipherID();
                int blocksize = 0;
                int keysize = 0;
                try {
                    blocksize = af.getCipherBlockSize(cpid);
                    keysize = af.getCipherKeySize(cpid);
                    
                } catch (NoSuchAlgorithmException nsae) {
                    failed = true;
                }
            
                // set iv
                byte[] iv = new byte[blocksize];
                for (int j=0; j<iv.length; j++) { iv[j] = 0; }
                IvParameterSpec ivspec = new IvParameterSpec(iv);
                
                // get a Cipher instance
                Cipher cp;
                try {
                    if (hasmdc)
                        cp = af.getCipherAlgorithm(cpid, "CFB");
                    else
                        cp = af.getCipherAlgorithm(cpid, "OpenpgpCFB");
                } catch (NoSuchAlgorithmException nsae) {
                    throw new RuntimeException("Inconsistency in factory: "+
                        "factory.getCipherBlockSize(id) and .getCipherKeySize"+
                        "(id) succeeded, but factory.getCipherAlgorithm(id) "+
                        "did not.");
                }
    
                // convert the key to a keyspec
                StringTokenizer st = 
                    new StringTokenizer(cp.getAlgorithm(), "/");
                SecretKeySpec keyspec = new SecretKeySpec(key, st.nextToken());
    
                // init the Cipher
                try {
                    cp.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
                } catch (InvalidKeyException ike) {
                    ike.printStackTrace();
                    throw new InternalError("InvalidKeyException on "+
                                            "decrypting a key - "+ike);
                } catch (InvalidAlgorithmParameterException iape) {
                    iape.printStackTrace();
                    throw new InternalError(
                        "InvalidAlgorithmParameterException "+
                        "on decrypting a key - "+iape);
                }
                
                in = new EncryptedInputStream(in, old, len, cp, blocksize, mdc);
                encin = in;
                
                byte[] prefix = new byte[blocksize+2];
                if (in.read(prefix) != prefix.length)
                    throw new MessageStreamException("Could not read prefix");
                
                //To thwart the Mister/Zuccherato attack outlined in
                // http://eprint.iacr.org/2005/033 we will decrypt it anyway,
                // even if the check failes. The result may be gibberish, which 
                // will result in a different error.
                
                //if ((prefix[blocksize-2] != prefix[blocksize]) ||
                //    (prefix[blocksize-1] != prefix[blocksize+1])) 
                //    throw new MessageStreamException("Wrong decryption key");
                    
            } else {
                
                PGPPacketDataInputStream pin = 
                        new PGPPacketDataInputStream (in, old, len);
                try {
                    pkt.decodeBody(pin, af);
                } catch (PGPDataFormatException pdfe) {
                    throw new MessageStreamException(""+pdfe);
                } catch (PGPFatalDataFormatException pdfe) {
                    throw new MessageStreamException(""+pdfe);
                }

                if (pkt instanceof PGPSessionKeyPacket) {
    
                    sessionkeys.add(pkt);
    
                } else if (pkt instanceof PGPMarkerPacket) {
                
                    // ignore
                
                } else if (pkt instanceof PGPOnePassSignaturePacket
                        || pkt instanceof PGPSignaturePacket) 
                {
                    int hashid;
                    byte[] keyid;
                    
                    if (pkt instanceof PGPOnePassSignaturePacket) {
                        PGPOnePassSignaturePacket opsp = 
                            (PGPOnePassSignaturePacket)pkt;
                        hashid = opsp.getHashID();
                        keyid = opsp.getKeyID();
                    } else {
                        sigpkt = (PGPSignaturePacket)pkt;
                        hashid = sigpkt.getHashID();
                        if (sigpkt.getVersion() <= 3) {
                            keyid = sigpkt.getKeyID();
                        } else {
                            try {
                                sigpkt.parseSignatureSubPackets();
                            } catch (PGPDataFormatException pdfe) {
                                throw new MessageStreamException(""+pdfe);
                            } catch (PGPFatalDataFormatException pdfe) {
                                throw new MessageStreamException(""+pdfe);
                            }
                            keyid = null;
                            Vector subpkts = sigpkt.getUnhashedSubPackets();
                            Iterator it = subpkts.iterator();
                            while (it.hasNext()) {
                                Object o = it.next();
                                if (o instanceof PGPKeyIDSP) {
                                    PGPKeyIDSP idsp = (PGPKeyIDSP)o;
                                    if (idsp.getPacketID() == 
                                        PGPSignatureConstants.SSP_ISSUER_KEYID)
                                    {
                                        keyid = idsp.getValue();
                                    }
                                }
                            }
                            subpkts = sigpkt.getHashedSubPackets();
                            it = subpkts.iterator();
                            while (it.hasNext()) {
                                Object o = it.next();
                                if (o instanceof PGPKeyIDSP) {
                                    PGPKeyIDSP idsp = (PGPKeyIDSP)o;
                                    if (idsp.getPacketID() == 
                                        PGPSignatureConstants.SSP_ISSUER_KEYID)
                                    {
                                        keyid = idsp.getValue();
                                    }
                                }
                            }
                        }
                    }
    
                    try {
                        md = af.getHashAlgorithm(hashid);
                    } catch (NoSuchAlgorithmException nsae) {
                        throw new MessageStreamException(""+nsae);
                    }
                    
                    KeyID[] ids;
                    if (keyid == null) {
                        ids = new KeyID[0];
                    } else {
                        KeyID id3 = new PGPKeyIDImpl(null, keyid, 3);
                        KeyID id4 = new PGPKeyIDImpl(null, keyid, 4);
                        ids = new KeyID[2]; ids[0] = id4; ids[1] = id3;
                    }
                    
                    VerificationKeyReturnValue val = vkc.getVerificationKey(
                        new VerificationKeyRequest(ids, 0, null));
                    
                    if (val.getError() == VerificationKeyReturnValue.FAIL)
                        throw new MessageStreamException(
                            "Failed retrieving verification key");
                    
                    if (val.getError() == VerificationKeyReturnValue.IGNORE) {
                        throw new RuntimeException("NYI");
                    } else {
                        KeyBundle bundle = val.getBundle();
                        PGPKey key;
                        if (bundle == null) {
                            key = (PGPKey)val.getPublicKey();
                        } else {
                            key = (PGPKey)bundle.getPublicKeys().next();
                        }
                        signer = (PGPSigner)key.getPacket().getAlgorithm();   
                        signer.initVerify(hashid, af);
                    }
                    
                } else {
                    
                    throw new MessageStreamException("Invalid streaming "+
                        "message found. Found unexpected "+pkt.getClass());
                }
            }
        }
    }

    public int engineRead()
        throws IOException
    {
        if (in == null) throw new IllegalStateException("Not initialized");
        return in.read();
    }


    public int engineRead(byte[] b, int off, int len) 
        throws IOException
    {
        if (in == null) throw new IllegalStateException("Not initialized");
        return in.read(b, off, len);
    }

    public void engineClose()
        throws IOException
    {
        if (in == null) throw new IllegalStateException("Not initialized");
        
        byte[] b = new byte[1];
        if (in.read(b) > 0)
            throw new MessageStreamException("Data left on closing");
        
        if (in instanceof SignedLiteralInputStream)
            verificationResult = ((SignedLiteralInputStream)in).verify();
        else
            verificationResult = 
                DecodedMessageInputStream.VERIFICATION_NOT_SIGNED;
        
        if (mdc == null) {
            integrityResult = DecodedMessageInputStream.INTEGRITY_NOT_PROTECTED;
        } else {
            b = new byte[2];
            if ((encin.read(b) != 2) || (b[0] != (byte)0xd3) || 
                                        (b[1] != (byte)0x14)) 
            {
                integrityResult = DecodedMessageInputStream.INTEGRITY_VIOLATED;
            } else {
                byte[] hashcalc = mdc.digest();
                b = new byte[20];
                if (encin.read(b) != 20) {
                    integrityResult = 
                        DecodedMessageInputStream.INTEGRITY_VIOLATED;
                } else {
                    boolean result = true;
                    for (int i=0; i<20; i++)
                        if (b[i] != hashcalc[i]) result = false;
                    if (result)
                        integrityResult = 
                            DecodedMessageInputStream.INTEGRITY_GOOD;
                    else
                        integrityResult = 
                            DecodedMessageInputStream.INTEGRITY_VIOLATED;
                }
            }
        }
        
        in.close();
    }

    
    public int engineGetVerificationResult()
        throws IllegalStateException
    {
        if (in == null) 
            throw new IllegalStateException("Not initialized");
        if (verificationResult == 0) 
            throw new IllegalStateException("Not yet closed");
        
        return verificationResult;
    }

    public int engineGetIntegrityResult()
        throws IllegalStateException
    {
        if (in == null) 
            throw new IllegalStateException("Not initialized");
        if (integrityResult == 0) 
            throw new IllegalStateException("Not yet closed");
        
        return integrityResult;
    }


    private class LiteralInputStream
        extends InputStream
    {
        private PGPPacketDataInputStream in;
        
        private LiteralInputStream(InputStream in, boolean old, int lentype)
            throws IOException, PGPDataFormatException, 
                   PGPFatalDataFormatException
        {
            this.in = new PGPPacketDataInputStream(in, old, lentype);
        
            // read header
            // ### FIXME: make this available some way
            byte type = this.in.readByte();
            String filename = this.in.readLengthPrependedString();
            long time = this.in.readUnsignedInt();
        }
        
        public int read()
            throws IOException
        {
            try {
                return in.readByte();
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }

        public int read(byte[] b)
            throws IOException
        {
            try {
                return in.readBuffer(b);
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }

        public int read(byte[] b, int off, int len)
            throws IOException
        {
            try {
                return in.readBuffer(b, off, len);
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
        
        public void close()
            throws IOException
        {
            try {
                in.close();
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
    }

    private class SignedLiteralInputStream
        extends InputStream
    {
        private PGPPacketDataInputStream in;
        private InputStream under;
        private PGPSigner signer;
        private MessageDigest md;
        private PGPSignaturePacket pkt;
        
        private SignedLiteralInputStream(InputStream in, boolean old, 
            int lentype, PGPSigner signer, MessageDigest md, 
            PGPSignaturePacket pkt)
            throws IOException, PGPDataFormatException, 
                   PGPFatalDataFormatException
        {
            this.in = new PGPPacketDataInputStream(in, old, lentype);
            this.under = in;
            this.signer = signer;
            this.md = md;
            this.pkt = pkt;
        
            // read header
            // ### FIXME: make this available some way
            byte type = this.in.readByte();
            String filename = this.in.readLengthPrependedString();
            long time = this.in.readUnsignedInt();
        }
        
        public int read()
            throws IOException
        {
            try {
                int value = in.readByte();
                byte[] data = {(byte)value};
                if (signer != null) signer.update(data);
                if (md != null)     md.update(data);
                return value;
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }

        public int read(byte[] b)
            throws IOException
        {
            try {
                int retlen = in.readBuffer(b);
                if (signer != null) signer.update(b, 0, retlen);
                if (md != null)     md.update(b, 0, retlen);
                return retlen;
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }

        public int read(byte[] b, int off, int len)
            throws IOException
        {
            try {
                int retlen = in.readBuffer(b, off, len);
                if (signer != null) signer.update(b, off, retlen);
                if (md != null)     md.update(b, off, retlen);
                return retlen;
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
        
        public int verify()
            throws IOException
        {
            PGPAlgorithmFactory af = PGPAlgorithmFactory.getDefaultInstance();
            PGPPacketFactory    pf = PGPPacketFactory.getDefaultInstance();
            
            int id, len, type;
            boolean old;
            
            int result = DecodedMessageInputStream.VERIFICATION_NOT_SIGNED;
                
            // read signature packet if it wasn't prepended
            if (pkt == null) {

                type = under.read();
        
                if (type == -1) {
                    throw new MessageStreamException(
                        "End of stream reached, expecting signature.");
                }
        
                if (type < 128) {
                    throw new MessageStreamException("Invalid packet type.");
                }
        
                if (type >= 192) {   // new format
                    id = type & 0x3F;
                    old = false;
                    len = -1;
                } else {             // old format
                    id = (type >> 2) & 0x0F;
                    old = true;
                    len = type & 0x03;
                }
                
                PGPPacket p = pf.getPacket(id);
                if (! (p instanceof PGPSignaturePacket))
                    throw new MessageStreamException("Expecting signature, "+
                        "got " + p);
                        
                pkt = (PGPSignaturePacket)p;
    
                // read the rest of the signature packet
                PGPPacketDataInputStream pin = 
                        new PGPPacketDataInputStream (under, old, len);
                try {
                    pkt.decodeBody(pin, af);
                } catch (PGPDataFormatException pdfe) {
                    throw new MessageStreamException(""+pdfe);
                } catch (PGPFatalDataFormatException pdfe) {
                    pdfe.printStackTrace();
                    throw new MessageStreamException(""+pdfe);
                }
            
            }            
            
            
            // hash the relevant portions of the signature packet
            int bytesWritten = pkt.hashData(md, signer);
            
    
            // hash the trailer
            if (pkt.getVersion() == 4) {
                byte[] trailer = new byte[6];
                trailer[0] = pkt.getVersion();
                trailer[1] = (byte)0xFF;
                trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
                trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
                trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
                trailer[5] = (byte)((bytesWritten      ) & 0xFF);
                md.update(trailer);
                signer.update(trailer);
            }
    
    
            // verify two bytes of hash
            byte[] digestcalc  = md.digest();
            byte[] digestinsig = pkt.getHash();
            boolean ok = ((digestcalc[0] == digestinsig[0]) && 
                          (digestcalc[1] == digestinsig[1]));
    
    
            if (! ok) {
                
                // hash was not ok, so no need to verify the sig itself.
                result = DecodedMessageInputStream.VERIFICATION_BAD_SIGNATURE;
            
            } else {
            
                // parse signature data
                try {
        
                    pkt.interpretSignature(signer);
        
                } catch (PGPDataFormatException dfe) {
                    throw new MessageStreamException("Invalid signature");
                }
                    
                // the real thing
                if (signer.verifySignature())
                    result = 
                        DecodedMessageInputStream.VERIFICATION_GOOD_SIGNATURE;
                else
                    result = 
                        DecodedMessageInputStream.VERIFICATION_BAD_SIGNATURE;
             
            }
            
            return result;
        }
        
        public void close()
            throws IOException
        {
            try {
                in.close();
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
    }

    private class EncryptedInputStream
        extends InputStream
    {
        private PGPPacketDataInputStream in;
        private Cipher cipher;
        private byte[] buf;
        private int bufoffset;
        private int blocksize;
        private int tot;
        private MessageDigest mdc;
        
        private EncryptedInputStream(InputStream in, boolean old, int lentype, 
            Cipher cipher, int blocksize, MessageDigest mdc)
            throws IOException
        {
            this.in = new PGPPacketDataInputStream(in, old, lentype);
            this.cipher = cipher;
            this.buf = new byte[blocksize*2];
            this.bufoffset = buf.length;
            this.blocksize = blocksize;
            this.mdc = mdc;
            try {
                if (mdc != null)
                    if (this.in.readByte() != 1)
                        throw new MessageStreamException("Invalid MDC version");
            } catch (PGPDataFormatException pdfe) {
                throw new MessageStreamException("Cannot read MDC version: "+
                                                 pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new MessageStreamException("Cannot read MDC version: "+
                                                 pdfe);
            }
        }
        
        public int read()
            throws IOException
        {
            if (bufoffset == buf.length) {
                byte[] b = new byte[1];
                if (read(b) == 0) return -1;
                return b[0] & 0xff;
            } else {
                if (mdc != null) mdc.update(buf, bufoffset, 1);
                return buf[bufoffset++] & 0xff;
            }
        }

        public int read(byte[] b)
            throws IOException
        {
            int result = read(b, 0, b.length);
            return result;
        }
        
        private boolean finalcall = false;
        
        public int available()
        {
            int result = 0;
            if (in.isFinal()) {
                if (mdc == null) {
                    result = (int)in.available()
                         + buf.length - bufoffset + blocksize;
                } else {
                    result = (int)in.available()
                         + buf.length - bufoffset + blocksize - 22;
                }
                if (finalcall) result = 0;
                finalcall = true;
            } else {
                result = (int)in.available()
                     + buf.length - bufoffset;
            }
            return result;
        }

        public int read(byte[] b, int off, int len)
            throws IOException
        {
            try {
                if (buf.length - bufoffset >= len) {
                    System.arraycopy(buf, bufoffset, b, 0, len);
                    if (mdc != null) mdc.update(b, 0, len);
                    bufoffset += len;
                    return len;
                }
                byte[] temp;
                if (in.available() > 32) {
                    temp = new byte[(int)in.available() & 0xffffffe0];
                    if (temp.length < len+blocksize*2+2)
                        temp = new byte[len+blocksize*2+2];
                } else {
                    temp = new byte[len+blocksize*2+2];
                }

                if ((in.available() == 0) && (buf.length - bufoffset == 0))
                    return 0;

                int size = in.readBuffer(temp);
                byte[] data;
                if (size < temp.length) {
                    try {
                        data = cipher.doFinal(temp, 0, size);
                    } catch (IllegalBlockSizeException ibse) {
                        throw new IOException(""+ibse);
                    } catch (BadPaddingException bpe) {
                        throw new IOException(""+bpe);
                    }
                } else {
                    data = cipher.update(temp, 0, size);
                }
                size = 0;
                if (bufoffset < buf.length) {
                    System.arraycopy(buf, bufoffset, b, off, 
                                     buf.length-bufoffset);
                    size += buf.length-bufoffset;
                }
                if (size + data.length > len) {
                    System.arraycopy(data, 0, b, off+size, len-size);
                    int newbuflen = data.length-(len-size);
                    buf = new byte[newbuflen];
                    System.arraycopy(data, len-size, buf, 0, newbuflen);
                    bufoffset = 0;
                    if (mdc != null) mdc.update(b, 0, len);
                    return len;
                } else {
                    System.arraycopy(data, 0, b, off+size, data.length);
                    bufoffset = buf.length;
                    if (mdc != null) mdc.update(b, 0, size + data.length);
                    return size + data.length;
                }
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
        
        public void close()
            throws IOException
        {
            try {
                in.close();
            } catch (PGPDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            } catch (PGPFatalDataFormatException pdfe) {
                throw new IOException(""+pdfe);
            }
        }
    }
}