/* $Id: PGPSignedMessageImpl.java,v 1.5 2005/04/18 12:12:29 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDetachedSignatureMessage;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPPublicKey;
import cryptix.openpgp.PGPSignedMessage;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.packet.PGPOnePassSignaturePacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPSignatureConstants;
import cryptix.openpgp.packet.PGPSignaturePacket;

import cryptix.openpgp.signature.PGPKeyIDSP;

import cryptix.pki.KeyID;
import cryptix.pki.KeyBundle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;

import java.util.Iterator;
import java.util.Vector;


/**
 * Represents a signed message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.5 $
 */
public class PGPSignedMessageImpl extends PGPSignedMessage {


// Instance variables
// ..........................................................................


    private final PGPSignaturePacket pkt;
    private final PGPLiteralMessageImpl contents;
    

// Constructor
// ..........................................................................

    /**
     * Creates a SignedMessage of the specified format.
     */
    public PGPSignedMessageImpl(boolean legacy, PGPSignaturePacket pkt,
                                PGPLiteralMessageImpl contents) 
    {
        super("OpenPGP", legacy);
        if (pkt == null)
            throw new IllegalArgumentException("pkt == null");
        if (contents == null)
            throw new IllegalArgumentException("contents == null");
        this.pkt = pkt;
        this.contents = contents;
    }


// Methods from Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException
    {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            byte[] keyid = null;
            
            if (pkt.getVersion() == 4) {

                Vector subpkts = pkt.getAllSubPackets();
                
                for (int i=0; i<subpkts.size(); i++) {
                    if (subpkts.elementAt(i) instanceof PGPKeyIDSP) {
                        PGPKeyIDSP sp = (PGPKeyIDSP)subpkts.elementAt(i);
                        if (sp.getPacketID() == 
                            PGPSignatureConstants.SSP_ISSUER_KEYID)
                        {
                            keyid = sp.getValue();
                        }
                    }
                }
                
            }
            else
            {
                keyid = pkt.getKeyID();
            }
            
            if (keyid == null) {
                throw new MessageException("Could not find keyid.");
            }

            if (legacy) {
                
                pkt.encode(baos);
                
                baos.write(contents.getEncoded());

            } else {

                PGPOnePassSignaturePacket opspkt = 
                    new PGPOnePassSignaturePacket(pkt.getType(), 
                            pkt.getHashID(), pkt.getAlgoID(), keyid, false);
                opspkt.setPacketID(PGPPacketConstants.PKT_ONE_PASS_SIGNATURE);
                opspkt.encode(baos);

                baos.write(contents.getEncoded());
                
                pkt.encode(baos);
            }
            
            
            baos.close();
            return baos.toByteArray();
        
        } catch (IOException ioe) {
            throw new RuntimeException(
                "IOException in ByteArrayOutputStream - "+ioe);
        }
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @returns the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
        throw new IllegalArgumentException(
            "Attributes not supported on this object.");
    }


// Methods from SignedMessage
// ..........................................................................

    /**
     * Get the message itself
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public Message getContents() throws MessageException
    {
        return contents;
    }
    

    /**
     * Verify if this message was correctly signed by a particular public key.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @returns true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public boolean verify(PublicKey pubkey) throws MessageException
    {
        // check key
        if (! (pubkey instanceof PGPPublicKey)) {
            throw new IllegalArgumentException("Not instance of PGPPublicKey");
        }
        
        PGPPublicKey signkey = (PGPPublicKey)pubkey;
        
        if (! (signkey.getPacket() instanceof PGPPublicKeyPacket)) {
            throw new IllegalArgumentException("Not a public signing key");
        }

        // init
        PGPSigner signer = (PGPSigner)signkey.getPacket().getAlgorithm();
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();

        MessageDigest md;
        try {
            md = factory.getHashAlgorithm(pkt.getHashID());
        } catch (NoSuchAlgorithmException nsae) {
            throw new MessageException(""+nsae);
        }

        signer.initVerify(pkt.getHashID(), factory);

        // hash the data
        md.update(contents.getBinaryData());
        signer.update(contents.getBinaryData());
        
        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);
        

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }


        // verify two bytes of hash
        byte[] digestcalc  = md.digest();
        byte[] digestinsig = pkt.getHash();
        boolean ok = ((digestcalc[0] == digestinsig[0]) && 
                      (digestcalc[1] == digestinsig[1]));


        if (! ok) {
            // hash was not ok, so no need to verify the sig itself.
            return false;
        }
        
        // parse signature data
        try {

            pkt.interpretSignature(signer);

        } catch (IOException ioe) {
            throw new MessageException("IOException while parsing sig "+ioe);
        } catch (PGPDataFormatException dfe) {
            throw new MessageException("Invalid signature");
        }
            
        // the real thing
        return signer.verifySignature();
    }
        

    /**
     * Verify if this message was correctly signed by a particular keybundle.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @returns true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public boolean verify(KeyBundle pubkey) throws MessageException
    {
        return verify((PublicKey)pubkey.getPublicKeys().next());
    }
        

    /**
     * Returns hints for which key was used to sign this message
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public KeyID[] getKeyIDHints() throws MessageException
    {
        try {
            byte[] keyid = null;
    
            if (pkt.getVersion() <= 3) {
                keyid = pkt.getKeyID();
            } else {
                pkt.parseSignatureSubPackets();
                Vector subpkts = pkt.getUnhashedSubPackets();
                Iterator it = subpkts.iterator();
                while (it.hasNext()) {
                    Object o = it.next();
                    if (o instanceof PGPKeyIDSP) {
                        PGPKeyIDSP idsp = (PGPKeyIDSP)o;
                        if (idsp.getPacketID() == 
                            PGPSignatureConstants.SSP_ISSUER_KEYID)
                        {
                            keyid = idsp.getValue();
                        }
                    }
                }
                subpkts = pkt.getHashedSubPackets();
                it = subpkts.iterator();
                while (it.hasNext()) {
                    Object o = it.next();
                    if (o instanceof PGPKeyIDSP) {
                        PGPKeyIDSP idsp = (PGPKeyIDSP)o;
                        if (idsp.getPacketID() == 
                            PGPSignatureConstants.SSP_ISSUER_KEYID)
                        {
                            keyid = idsp.getValue();
                        }
                    }
                }
            }
            
            KeyID[] ids;
            if (keyid == null) {
                ids = new KeyID[0];
            } else {
                KeyID id3 = new PGPKeyIDImpl(null, keyid, 3);
                KeyID id4 = new PGPKeyIDImpl(null, keyid, 4);
                ids = new KeyID[2]; ids[0] = id4; ids[1] = id3;
            }
            return ids;
            
        } catch (PGPDataFormatException pdfe) {
            throw new MessageException(""+pdfe);
        } catch (PGPFatalDataFormatException pdfe) {
            throw new MessageException(""+pdfe);
        }
    }
    

// Methods from PGPSignedMessage
// ..........................................................................

    /**
     * Returns a detached signature.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public PGPDetachedSignatureMessage getDetachedSignature()
        throws MessageException
    {
        return new PGPDetachedSignatureMessageImpl(pkt);
    }

}
