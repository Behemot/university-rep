/* $Id: PGPUserIDPrincipalBuilder.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.packet.PGPUserIDPacket;
import cryptix.openpgp.packet.PGPPacketConstants;

import cryptix.pki.PrincipalBuilderSpi;
import cryptix.pki.PrincipalException;

import java.security.Principal;


/**
 * Service provider interface for PrincipalBuilder
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPUserIDPrincipalBuilder extends PrincipalBuilderSpi {

    /**
     * Returns a new principal based on the given contents.
     *
     * @param contents the UserID, must be a String
     *
     * @throws PrincipalException on a variety of format specific problems.
     */
    public Principal engineBuild(Object contents) 
        throws PrincipalException
    {
        if (!(contents instanceof String)) 
            throw new PrincipalException("Invalid contents: String expected.");
        
        PGPUserIDPacket pkt = new PGPUserIDPacket();
        pkt.setValue((String)contents);
        pkt.setPacketID(PGPPacketConstants.PKT_USERID);
        
        return new PGPUserIDPrincipal(pkt);
    }
        
}
