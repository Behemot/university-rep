/* $Id: PGPEncryptedMessageBuilder.java,v 1.3 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.EncryptedMessageBuilderSpi;
import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPKeyBundle;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPEncryptor;

import cryptix.openpgp.packet.PGPCompressedDataPacket;
import cryptix.openpgp.packet.PGPContainerPacket;
import cryptix.openpgp.packet.PGPEncryptedDataPacket;
import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPPacketFactory;
import cryptix.openpgp.packet.PGPPublicKeyEncryptedSessionKeyPacket;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPPublicSubKeyPacket;
import cryptix.openpgp.packet.PGPSessionKeyPacket;
import cryptix.openpgp.packet.PGPSymmetricallyEncryptedDataPacket;
import cryptix.openpgp.packet.PGPSymmetricKeyEncryptedSessionKeyPacket;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;

import java.util.Iterator;
import java.util.Vector;

import javax.crypto.SecretKey;


/**
 * Service provider interface for EncryptedMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class PGPEncryptedMessageBuilder extends EncryptedMessageBuilderSpi {


    private boolean compression = false;
    private Message contents;
    private SecureRandom sr;
    private Vector recipients = new Vector();
    protected boolean v3 = false;
    

    /**
     * Initializes this builder with the given message.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineInit(Message contents, SecureRandom sr)
        throws IllegalStateException, MessageException
    {
        if (! ((contents instanceof PGPLiteralMessageImpl) || 
               (contents instanceof PGPSignedMessageImpl)))
        {
            throw new MessageException("Can only encrypt OpenPGP literal or "+
                "signed messages");
        }
        if (this.contents != null) {
            throw new IllegalStateException("Already initialized");
        }
        this.contents = contents;
        this.sr = sr;
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
            MessageException
    {
        if ("compressed".equals(name)) {
            if (attr instanceof String) {
                if ("false".equals(attr)) {
                    compression = false;
                } else if ("true".equals(attr)) {
                    compression = true;
                } else {
                    throw new IllegalArgumentException(
                        "Illegal value for attribute 'compressed'. Only "+
                        "'true' and 'false' are valid values.");
                }
            } else {
                throw new IllegalArgumentException(
                    "Illegal value for attribute 'compressed'. "+
                    "Expected a String containing 'true' or 'false'.");
            }
        } else {
            throw new IllegalArgumentException("Illegal attribute.");
        }
    }


    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineAddRecipient(PublicKey publickey)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.recipients.add(publickey);
    }


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineAddRecipient(KeyBundle bundle)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.recipients.add(bundle);
    }


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineAddRecipient(SecretKey symmetrickey)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        throw new UnsupportedOperationException(
            "Symmetric key encryption not supported.");
    }
        

    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineAddRecipient(String passphrase)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.recipients.add(passphrase);
    }
    

    /**
     * Returns the encrypted message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, if no calls have been made to any of the addRecipient
     *         methods or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public Message engineBuild()
        throws IllegalStateException, MessageException
    {
        Vector skps = new Vector();
    
        byte datacipher;
        if (v3)
            datacipher = 1;                // IDEA
        else
            datacipher = 2;                // Triple DES   ### FIXME
        
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        int keysize;
        try {
            keysize = factory.getCipherKeySize(datacipher);
        } catch (NoSuchAlgorithmException nsae) {
            throw new MessageException(""+nsae);
        }
        byte[] sessionkey = new byte[keysize]; 
        sr.nextBytes(sessionkey);
        
        Iterator it = recipients.iterator();
        while (it.hasNext()) {

            Object rcpt = it.next();

            if (rcpt instanceof KeyBundle) {
            
                Iterator it2 = ((PGPKeyBundle)rcpt).getPublicSubKeys();
                PGPPublicKey publickey = null;
                while (it2.hasNext()) {
                    // ### FIXME: Find the right subkey here, do not just use
                    // the last available one.
                    PGPPublicKey temp = (PGPPublicKey)it2.next();
                    publickey = temp;
                }
                
                PGPEncryptor cryptor;
                int id;
                
                if (publickey == null) {
                    
                    // try the main key
                    it2 = ((PGPKeyBundle)rcpt).getPublicKeys();
                    publickey = (PGPPublicKey)it2.next();
                    PGPPublicKeyPacket keypkt = 
                        (PGPPublicKeyPacket)publickey.getPacket();
                    id = keypkt.getAlgorithmID();
                    try {
                        // ### FIXME: check flags if this key can be used for 
                        // encryption.
                        cryptor = (PGPEncryptor)keypkt.getAlgorithm();
                    } catch (ClassCastException cce) {
                        throw new MessageException("No encryption key found.");
                    }
                    
                } else {
                    
                    PGPPublicSubKeyPacket keypkt = 
                        (PGPPublicSubKeyPacket)publickey.getPacket();
                    id = keypkt.getAlgorithmID();
                    cryptor = (PGPEncryptor)keypkt.getAlgorithm();
                }
                
                if (publickey == null) {
                    throw new MessageException("No encryption key found.");
                }
                
                PGPPublicKeyEncryptedSessionKeyPacket pkt = 
                    new PGPPublicKeyEncryptedSessionKeyPacket();
                pkt.setSessionKey(sessionkey, datacipher);
                pkt.encrypt(cryptor, sr);
                
                pkt.setPacketID(
                    PGPPacketConstants.PKT_PUBLIC_KEY_ENCRYPTED_SESSION_KEY);
                pkt.setPublicKeyAlgorithmID(id);
                
                KeyID keyid;
                try {
                    keyid = PGPKeyIDFactory.convert(publickey);
                } catch (InvalidKeyException ike) {
                    ike.printStackTrace();
                    throw new MessageException(""+ike);
                }
                pkt.setKeyID(keyid.getBytes(8));
            
                skps.add(pkt);

            } else if (rcpt instanceof PublicKey) {
            
                PGPKeyPacket keypkt =
                    (PGPKeyPacket)((PGPPublicKey)rcpt).getPacket();
                PGPEncryptor cryptor = (PGPEncryptor)keypkt.getAlgorithm();
                PGPPublicKeyEncryptedSessionKeyPacket pkt = 
                    new PGPPublicKeyEncryptedSessionKeyPacket();
                pkt.setSessionKey(sessionkey, datacipher);
                pkt.encrypt(cryptor, sr);

                pkt.setPacketID(
                    PGPPacketConstants.PKT_PUBLIC_KEY_ENCRYPTED_SESSION_KEY);
                pkt.setPublicKeyAlgorithmID(keypkt.getAlgorithmID());
                
                KeyID keyid;
                try {
                    keyid = PGPKeyIDFactory.convert((PublicKey)rcpt);
                } catch (InvalidKeyException ike) {
                    ike.printStackTrace();
                    throw new MessageException(""+ike);
                }
                pkt.setKeyID(keyid.getBytes(8));

                skps.add(pkt);
                
            } else if (rcpt instanceof String) {
            
                byte s2kid  = 3;  // Iterated and Salted    ### FIXME
                byte hashid = 2;  // SHA-1                  ### FIXME
            
                PGPSymmetricKeyEncryptedSessionKeyPacket pkt = 
                    new PGPSymmetricKeyEncryptedSessionKeyPacket();
                pkt.setDirect((String)rcpt, factory, sr, datacipher, s2kid, 
                              hashid);
                pkt.setPacketID(
                    PGPPacketConstants.PKT_SYMMETRIC_KEY_ENCRYPTED_SESSION_KEY);
                
                skps.add(pkt);

            }
        }
        
        PGPSessionKeyPacket[] skparr = new PGPSessionKeyPacket[skps.size()];
        for (int i=0; i<skparr.length; i++) {
            skparr[i] = (PGPSessionKeyPacket)skps.elementAt(i);
        }
        
        // ### FIXME: Support integrity protection here as well
        PGPContainerPacket pkt = new PGPSymmetricallyEncryptedDataPacket();
        pkt.setPacketID(PGPPacketConstants.PKT_SYMMETRICALLY_ENCRYPTED_DATA);

        if (compression) {
            pkt = new PGPCompressedDataPacket();
            pkt.setPacketID(PGPPacketConstants.PKT_COMPRESSED_DATA);
            ((PGPCompressedDataPacket)pkt).setAlgorithmID(1); // zip ### FIXME
        }
        
        // ### FIXME: This should be more efficient
        byte[] data = contents.getEncoded();
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        PGPPacketFactory pktfactory = PGPPacketFactory.getDefaultInstance();
        while(bais.available() > 0) {
            try {
                pkt.appendPacket(pktfactory.readPacket(bais, factory));
            } catch (IOException ioe) {
                throw new MessageException(""+ioe);
            } catch (PGPDataFormatException pdfe) {
                throw new MessageException(""+pdfe);
            } catch (PGPFatalDataFormatException pfdfe) {
                throw new MessageException(""+pfdfe);
            }
        }

        if (compression) {
            PGPContainerPacket pkt2 = new PGPSymmetricallyEncryptedDataPacket();
            pkt2.appendPacket(pkt);
            pkt2.setPacketID(
                PGPPacketConstants.PKT_SYMMETRICALLY_ENCRYPTED_DATA);
            pkt = pkt2;
        }
        
        ((PGPSymmetricallyEncryptedDataPacket)pkt).encrypt(
            sessionkey, datacipher, factory, sr);
        
        return new PGPEncryptedMessageImpl(
            skparr, (PGPSymmetricallyEncryptedDataPacket)pkt);

    }
    

// Inner classes for instantiation
//..............................................................................

    public static class Legacy extends PGPEncryptedMessageBuilder
    {
        public Legacy() { this.v3 = true; }
    }
    
}
