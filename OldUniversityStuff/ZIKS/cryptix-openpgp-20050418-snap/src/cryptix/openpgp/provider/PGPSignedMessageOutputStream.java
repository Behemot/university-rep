/* $Id: PGPSignedMessageOutputStream.java,v 1.1 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.stream.SignedMessageOutputStreamSpi;
import cryptix.message.stream.MessageStreamException;

import cryptix.pki.KeyBundle;

import cryptix.openpgp.PGPPrivateKey;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.packet.PGPOnePassSignaturePacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyIDSP;

import java.io.IOException;
import java.io.OutputStream;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.util.Date;
import java.util.Vector;


/**
 * OpenPGP implementation of the SignedMessageOutputStream service provider
 * interface.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPSignedMessageOutputStream 
    implements SignedMessageOutputStreamSpi
{

// Instance variables
//..............................................................................

    /** The underlying outputstream */
    private OutputStream out;
    
    /** Source of randomness */
    private SecureRandom sr;
    
    /** Indicates if data has already been written to the underlying stream */
    private boolean nodatawritten = true;
    
    /** Signing key */
    private PGPPrivateKey signingkey;
    
    private byte[] keyid;
    private byte algoid;
    private byte hashid;
    private byte type;
    
    private MessageDigest md;
    private PGPSigner signer;
    
    private boolean v4sig;
    

    protected PGPSignedMessageOutputStream(boolean v4sig)
    {
        this.v4sig = v4sig;
    }

// Methods from SignedMessageOutputStreamSpi
//..............................................................................

    /**
     * Adds a signer
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddSigner(PrivateKey signingkey)
        throws IllegalStateException, IOException, MessageStreamException
    {
        if (this.signingkey != null) {
            throw new RuntimeException("Nesting signatures not supported yet.");
        }
        this.signingkey = (PGPPrivateKey)signingkey;
        
        try {
            keyid = PGPKeyIDFactory.convert(signingkey).getBytes(8);
        } catch (InvalidKeyException ike) {
            throw new MessageStreamException(""+ike);
        }
        
        algoid = this.signingkey.getPacket().getAlgorithmID();
        hashid = 2; // SHA-1 ### FIXME prefs
        type = PGPSignatureConstants.TYPE_BINARY_DOCUMENT; // ### FIXME

        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        try {
            md = factory.getHashAlgorithm(hashid);
        } catch (NoSuchAlgorithmException nsae) {
            throw new MessageStreamException(""+nsae);
        }
        signer = (PGPSigner)this.signingkey.getPacket().getAlgorithm();
        
        signer.initSign(hashid, factory);
    }


    /**
     * Adds a signer from a keybundle, decrypting it with the given passphrase.
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     */
    public void engineAddSigner(KeyBundle signingkey, char[] passphrase)
        throws IllegalStateException, IOException, MessageStreamException,
               UnrecoverableKeyException
    {
        if (sr == null) throw new IllegalStateException("Not yet initialized.");
        if (this.signingkey != null) {
            throw new RuntimeException("Nesting signatures not supported yet.");
        }
        PGPPublicKey pubkey = (PGPPublicKey)signingkey.getPublicKeys().next();
        engineAddSigner(
            (PGPPrivateKey)signingkey.getPrivateKey(pubkey, passphrase));
    }


// Methods from MessageOutputStreamSpi
//..............................................................................

    /**
     * Initializes this outputstream with the given outputstream as underlying
     * stream and the given SecureRandom object.
     *
     * @param out The underlying outputstream this stream will write results to.
     * @param sr  A SecureRandom object used for any randomness needed.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineInit(OutputStream out, SecureRandom sr)
        throws IllegalStateException, IOException, MessageStreamException
    {
        if (this.out != null) 
            throw new IllegalStateException("Already called init before");
        if (out == null) 
            throw new IllegalArgumentException("Outputstream cannot be null");
        
        this.out = out;
        this.sr  = sr;
    }
        
    /**
     * Set a format specific attribute.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageStreamException
    {
        throw new IllegalArgumentException("No attributes supported");
    }

    /**
     * Write the specified part of a bytearray to the stream.
     *
     * @param b    the data
     * @param off  the starting point of the write operation
     * @param len  how many bytes to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b, int off, int len)
        throws IOException, MessageStreamException
    {
        if (out == null) 
            throw new IllegalStateException("Not yet initialized");

        if (nodatawritten) {
            nodatawritten = false;

            PGPOnePassSignaturePacket opspkt = new PGPOnePassSignaturePacket(
                type, hashid, algoid, keyid, false);
            opspkt.setPacketID(PGPPacketConstants.PKT_ONE_PASS_SIGNATURE);
            opspkt.encode(out);
        }

        out.write(b, off, len);

        md.update(b, off, len);
        signer.update(b, off, len);
    }
    
    /**
     * Write a byte of data without using it for the signature calculation
     */
    public void writeNoData(int b)
        throws IOException, MessageStreamException
    {
        if (out == null) 
            throw new IllegalStateException("Not yet initialized");

        if (nodatawritten) {
            nodatawritten = false;

            PGPOnePassSignaturePacket opspkt = new PGPOnePassSignaturePacket(
                type, hashid, algoid, keyid, false);
            opspkt.setPacketID(PGPPacketConstants.PKT_ONE_PASS_SIGNATURE);
            opspkt.encode(out);
        }

        out.write(b);
    }

    /**
     * Write a complete bytearray to the stream
     *
     * @param b  the data
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b)
        throws IOException, MessageStreamException
    {
        engineWrite(b, 0, b.length);
    }
    
    /**
     * Write a single byte to the stream
     *
     * @param b  the byte to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(int b)
        throws IOException, MessageStreamException
    {
        byte[] b1 = new byte[1];
        b1[0] = (byte)b;
        engineWrite(b1, 0, 1);
    }
    
    /**
     * Flush the stream
     *
     * <p>
     * Only flushed the underlying outputstream. We could do some more, but 
     * because writing a buffer with a size that is not a power of two is a 
     * pain, we won't.
     * </p>
     * 
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineFlush()
        throws IOException, MessageStreamException
    {
        out.flush();
    }
    
    /**
     * Close the stream
     *
     * <p>
     * Sends all data through the underlying stream and then calls the close()
     * method of the underlying stream.
     * </p>
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineClose()
        throws IOException, MessageStreamException
    {
        if (nodatawritten) {
            throw new MessageStreamException("No data written yet");
        }

        // create sig packet
        PGPSignaturePacket pkt = new PGPSignaturePacket();
        pkt.setPacketID(PGPPacketConstants.PKT_SIGNATURE);
        
        if (v4sig) { // version 4 signature
          
            // create subpackets
            PGPDateSP creation = new PGPDateSP();
            creation.setValue(new Date());
            creation.setPacketID(PGPSignatureConstants.SSP_SIG_CREATION_TIME);
            
            Vector hashed = new Vector();
            hashed.addElement(creation);
            
            PGPKeyIDSP keyidsp = new PGPKeyIDSP();
            keyidsp.setValue(keyid);
            keyidsp.setPacketID(PGPSignatureConstants.SSP_ISSUER_KEYID);
            
            Vector unhashed = new Vector();
            unhashed.addElement(keyidsp);
    
            pkt.setData(type, algoid, hashid, hashed, unhashed);
            
        } else { // version 3 signature
          
            // build timestamp
            byte[] time = new byte[4];
            int unixTime = (int)(System.currentTimeMillis()/1000);
            time[0] = (byte) ((unixTime >>>24) & 0xFF);
            time[1] = (byte) ((unixTime >>>16) & 0xFF);
            time[2] = (byte) ((unixTime >>> 8) & 0xFF);
            time[3] = (byte) ( unixTime        & 0xFF);
            
            pkt.setData(type, time, keyid, algoid, hashid);
        }

        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }

        // store hash
        byte[] digestcalc  = md.digest();
        pkt.setHash(digestcalc);
        
        // sign
        signer.computeSignature();
        pkt.setSignature(signer);
        
        pkt.encode(out);
        out.close();
    }


// Inner classes for instantiation
//..............................................................................

    public static class V4     extends PGPSignedMessageOutputStream {
        public V4()     { super(true);  }
    }

    public static class V3     extends PGPSignedMessageOutputStream {
        public V3()     { super(false); }
    }
}
