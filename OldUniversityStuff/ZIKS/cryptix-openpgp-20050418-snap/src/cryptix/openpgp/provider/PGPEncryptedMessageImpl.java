/* $Id: PGPEncryptedMessageImpl.java,v 1.4 2005/04/18 12:12:29 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.EncryptedMessage;
import cryptix.message.Message;
import cryptix.message.MessageException;
import cryptix.message.NotEncryptedToParameterException;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPKeyBundle;
import cryptix.openpgp.PGPPrivateKey;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPEncryptor;

import cryptix.openpgp.packet.PGPCompressedDataPacket;
import cryptix.openpgp.packet.PGPContainerPacket;
import cryptix.openpgp.packet.PGPEncryptedDataPacket;
import cryptix.openpgp.packet.PGPLiteralDataPacket;
import cryptix.openpgp.packet.PGPOnePassSignaturePacket;
import cryptix.openpgp.packet.PGPPacket;
import cryptix.openpgp.packet.PGPPublicKeyEncryptedSessionKeyPacket;
import cryptix.openpgp.packet.PGPSessionKey;
import cryptix.openpgp.packet.PGPSessionKeyPacket;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPSymmetricKeyEncryptedSessionKeyPacket;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.Key;
import java.security.UnrecoverableKeyException;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;


/**
 * Represents an encrypted message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public class PGPEncryptedMessageImpl extends EncryptedMessage {


// Instance vars
// ..........................................................................


    private PGPSessionKeyPacket[] skps;
    private PGPEncryptedDataPacket pkt;
    

// Constructor
// ..........................................................................

    /**
     * Creates a EncryptedMessage of the specified format.
     */
    public PGPEncryptedMessageImpl(PGPSessionKeyPacket[] skps, 
                                   PGPEncryptedDataPacket pkt) 
    {
        super("OpenPGP");
        this.skps = skps;
        this.pkt = pkt;
    }


// Methods from Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException
    {
        try {
        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            for (int i=0; i<skps.length; i++) 
                skps[i].encode(baos);

            pkt.encode(baos);

            baos.close();
            return baos.toByteArray();
        
        } catch (IOException ioe) {
            throw new RuntimeException(
                "IOException in ByteArrayOutputStream - "+ioe);
        }
            
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @returns the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
        throw new IllegalArgumentException("Attributes not supported.");
    }


// Methods from EncryptedMessage
// ..........................................................................

    /**
     * Decrypt the message with a key.
     *
     * <p>For most formats this key will usually be an instance of PrivateKey
     * (i.e. public key encryption was used). However, for some formats it could
     * also be possible to pass in a SecretKey (i.e. symmetric key encryption
     * was used).</p>
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular key.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support key based encryption.
     *
     * @returns the decrypted message.
     */
    public Message decrypt(Key key)
        throws NotEncryptedToParameterException, MessageException,
               UnsupportedOperationException
    {
        if (! (key instanceof PGPPrivateKey)) {
            throw new IllegalArgumentException(
                "Invalid key type, PGPPrivateKey expected.");
        }
        
        PGPPrivateKey pgpkey = (PGPPrivateKey)key;
        
        if (! (pgpkey.getPacket().getAlgorithm() instanceof PGPEncryptor)) {
            throw new IllegalArgumentException(
                "Not an encryption key.");
        }
        
        PGPEncryptor cryptor = (PGPEncryptor)pgpkey.getPacket().getAlgorithm();
        Vector sessionkeys = new Vector();
        
        for (int i=0; i<skps.length; i++) {
            if (skps[i] instanceof PGPPublicKeyEncryptedSessionKeyPacket) {

                PGPPublicKeyEncryptedSessionKeyPacket pkeskp = 
                    (PGPPublicKeyEncryptedSessionKeyPacket)skps[i];
                
                boolean success = false;
                
                try {
                    
                    pkeskp.decrypt(cryptor);
                    success = true;
                
                } catch (PGPDataFormatException e) {
                } catch (PGPDecryptionFailedException e) {
                }
                
                if (success) {
                    sessionkeys.add(pkeskp.getSessionKey());
                }

            }
        }
        
        if (sessionkeys.size() == 0) {
            throw new NotEncryptedToParameterException();
        }
        
        PGPSessionKey[] sks = new PGPSessionKey[sessionkeys.size()];
        for (int i=0; i<sks.length; i++) {
            sks[i] = (PGPSessionKey)sessionkeys.elementAt(i);
        }
        
        try {
            
            pkt.decrypt(sks, PGPAlgorithmFactory.getDefaultInstance());
        
        } catch (PGPDecryptionFailedException e) {
            throw new NotEncryptedToParameterException();
        } catch (PGPDataFormatException e) {
            throw new MessageException(""+e);
        } catch (PGPFatalDataFormatException e) {
            throw new MessageException(""+e);
        }

        Enumeration pkts = pkt.listPackets();
        if (! pkts.hasMoreElements()) {
            throw new MessageException("No data found");
        }
        
        PGPPacket content = (PGPPacket)pkts.nextElement();
        
        if (content instanceof PGPCompressedDataPacket) {
            pkts = ((PGPContainerPacket)content).listPackets();
            if (! pkts.hasMoreElements()) {
                throw new MessageException("No data found");
            }
            content = (PGPPacket)pkts.nextElement();
        }
        
        if (content instanceof PGPOnePassSignaturePacket) {
            // ignore for now
            content = (PGPPacket)pkts.nextElement();
        }
        
        Message msg = null;
        
        if (content instanceof PGPLiteralDataPacket) {
            msg = new PGPLiteralMessageImpl((PGPLiteralDataPacket)content);
            if (pkts.hasMoreElements()) content = (PGPPacket)pkts.nextElement();
        }
        
        if (content instanceof PGPSignaturePacket) {
            if (msg == null) {
                if (pkts.hasMoreElements()) {
                    PGPPacket more = (PGPPacket)pkts.nextElement();
                    if (more instanceof PGPLiteralDataPacket) {
                        msg = new PGPLiteralMessageImpl(
                            (PGPLiteralDataPacket)more);
                    }
                }
            }
            if (msg == null) {
                throw new MessageException(
                    "Could not find any data I understand. (Lone sig)");
            }
            msg = new PGPSignedMessageImpl(false, (PGPSignaturePacket)content,
                                           (PGPLiteralMessageImpl)msg);
        }
        
        if (msg == null) {
            throw new MessageException("Could not find any data I understand.");
        }
        
        return msg;
    }
        

    /**
     * Decrypt the message with a keybundle, decrypting the private key in the
     * keybundle using the given passphrase.
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular key.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support key based encryption.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     *
     * @returns the decrypted message.
     */
    public Message decrypt(KeyBundle bundle, char[] passphrase)
        throws NotEncryptedToParameterException, MessageException,
               UnsupportedOperationException, UnrecoverableKeyException
    {
        if (! (bundle instanceof PGPKeyBundle)) {
            throw new IllegalArgumentException(
                "Invalid key type, PGPKeyBundle expected.");
        }
        if (! ((PGPKeyBundle)bundle).containsPrivateKey()) {
            throw new IllegalArgumentException(
                "KeyBundle does not contain a private key.");
        }
        
        PGPKeyBundle pgpbundle = (PGPKeyBundle)bundle;
        
        Iterator it = pgpbundle.getPublicSubKeys();
        
        while (it.hasNext()) {
        
            PGPPublicKey pubsubkey = 
                (PGPPublicKey)it.next();
            PGPPrivateKey privsubkey = 
                (PGPPrivateKey)pgpbundle.getPrivateSubKey(pubsubkey, passphrase);
        
            try {
            
                return decrypt(privsubkey);
                
            } catch (NotEncryptedToParameterException netpe) {
            }
            
        }
        
        it = pgpbundle.getPublicKeys();
        PGPPublicKey pubmainkey = 
            (PGPPublicKey)it.next();
        PGPPrivateKey privmainkey = 
            (PGPPrivateKey)pgpbundle.getPrivateKey(pubmainkey, passphrase);

        if (privmainkey.getPacket().getAlgorithm() instanceof PGPEncryptor)
            return decrypt(privmainkey);
        
        throw new NotEncryptedToParameterException();
    }
        

    /**
     * Decrypt the message with a passphrase.
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular passphrase.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support password based encryption.
     *
     * @returns the decrypted message.
     */
    public Message decrypt(char[] passphrase)
        throws NotEncryptedToParameterException, MessageException, 
               UnsupportedOperationException
    {
        throw new RuntimeException("Not yet implemented");
    }


    /**
     * Returns hints for which keys can be used to decrypt this message
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public KeyID[] getKeyIDHints()
        throws MessageException
    {
        int count = 0;
        for (int i=0; i<skps.length; i++) {
            PGPSessionKeyPacket skp = skps[i];
            if (skp instanceof PGPPublicKeyEncryptedSessionKeyPacket) {
                count++;
            }
        }
        
        KeyID[] hints = new KeyID[count*2];
        int pos = 0;
        for (int i=0; i<skps.length; i++) {
            PGPSessionKeyPacket skp = skps[i];
            if (skp instanceof PGPPublicKeyEncryptedSessionKeyPacket) {
                hints[pos++] = new PGPKeyIDImpl(null, 
                    ((PGPPublicKeyEncryptedSessionKeyPacket)
                    skp).getKeyID(), 3);
                hints[pos++] = new PGPKeyIDImpl(null, 
                    ((PGPPublicKeyEncryptedSessionKeyPacket)
                    skp).getKeyID(), 4);
            }
        }
        
        return hints;
    }

}
