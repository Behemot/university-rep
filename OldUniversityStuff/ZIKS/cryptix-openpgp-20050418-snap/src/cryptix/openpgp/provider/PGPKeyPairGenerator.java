/* $Id: PGPKeyPairGenerator.java,v 1.4 2005/03/17 23:17:11 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPLegacyKeyPairGeneratorParameterSpec;
import cryptix.openpgp.PGPKey;
import cryptix.openpgp.PGPPublicKey;
import cryptix.openpgp.PGPPrivateKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPConstants;
import cryptix.openpgp.algorithm.PGPPublicKeyAlgorithm;

import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPPublicSubKeyPacket;
import cryptix.openpgp.packet.PGPSecretKeyPacket;
import cryptix.openpgp.packet.PGPSecretSubKeyPacket;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.KeyPair;
import java.security.KeyPairGeneratorSpi;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.SecureRandom;

import java.security.spec.AlgorithmParameterSpec;

import java.util.Date;


/**
 * Provider implementation of the KeyPairGenerator Service Provider Interface.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public class PGPKeyPairGenerator extends KeyPairGeneratorSpi {


// Constants
//...........................................................................


    /** 
     * Default keysizes:
     * 1024 bits DSA is maximum, 2048 bits is based on recommendation of 1881
     * bits by http://www.cryptosavvy.com/
     */
    protected static final int DEFAULT_KEY_SIZE_DSA     = 1024;
    protected static final int DEFAULT_KEY_SIZE_RSA     = 2048;
    protected static final int DEFAULT_KEY_SIZE_ELGAMAL = 2048;



// Instance variables
//...........................................................................


    /** Algorithm keysize */
    private int keysize;
    /** Algorithm ID */
    private int algid;
    /** Algorithm string */
    private String algorithm;
    /** SecureRandom instance */
    private SecureRandom sr = null;
    /** AlgorithmFactory instance */
    private PGPAlgorithmFactory factory = null;
    /** Is this a subkey or a main key? */
    private boolean subkey;
    /** Is this a legacy v3 key? */
    private boolean legacy;
    /** For legacy v3 keys the number of days after creation it expires */
    private int expiration;
    /** Public and Secret(Private) key packets. */
    private PGPKeyPacket pk, sk;



// Constructor
//...........................................................................


    /**
     * Constructor taking an algorithm ID, algorithm string and whether this is
     * a subkey or a 'main' key..
     *
     * @param algid an openpgp algorithm identifier
     * @param algorithm a string specifying the algorithm
     * @param defaultkeysize default keysize when no initialize(...) method is
     *        called
     * @param subkey true if this is a subkey, false if not
     * @param legacy true if a v3 legacy key should be created
     */
    protected PGPKeyPairGenerator(int algid, int defaultkeysize, 
        String algorithm, boolean subkey, boolean legacy) 
    {
        this.algid = algid;
        this.keysize = defaultkeysize;
        this.algorithm = algorithm;
        this.subkey = subkey;
        this.legacy = legacy;
        this.expiration = 0;
    }



// SPI methods
//...........................................................................


    /**
     * Generate the key
     */
    public KeyPair generateKeyPair() {
        
        if (sr == null)      sr = new SecureRandom(); 
        if (factory == null) factory = PGPAlgorithmFactory.getDefaultInstance();
        
        PGPPublicKeyAlgorithm alg;

        try {

            alg = factory.getPublicKeyAlgorithm(algid);

        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("NoSuchAlgorithm - "+nsae);
        }
        
        alg.generateKeyPair(keysize, sr);
        
        if (subkey) {

            sk = new PGPSecretSubKeyPacket();
            pk = new PGPPublicSubKeyPacket();

            sk.setPacketID(PGPPacketConstants.PKT_SECRET_SUBKEY);
            pk.setPacketID(PGPPacketConstants.PKT_PUBLIC_SUBKEY);

        } else {

            sk = new PGPSecretKeyPacket();
            pk = new PGPPublicKeyPacket();

            sk.setPacketID(PGPPacketConstants.PKT_SECRET_KEY);
            pk.setPacketID(PGPPacketConstants.PKT_PUBLIC_KEY);

        }
        
        if (legacy) {
            sk.setVersion(3); 
            pk.setVersion(3);
            sk.setV23ExpirationDate(expiration);
            pk.setV23ExpirationDate(expiration);
        } else {
            sk.setVersion(4); 
            pk.setVersion(4);
            if (expiration != 0)
                throw new IllegalStateException("V4 keys cannot have an "+
                    "expiration date set directly on the key.");
        }
        
        Date d = new Date();
        sk.setCreationDate(d);
        pk.setCreationDate(d); 
        
        sk.setAlgorithm(alg,               (byte)algid);
        pk.setAlgorithm(alg.clonePublic(), (byte)algid);
        
        return new KeyPair(new PGPPublicKey(pk, algorithm), 
                           new PGPPrivateKey(sk, algorithm));
        
    }
    

    /**
     * Initialize this keypair with the given keysize and securerandom object
     */
    public void initialize(int keysize, SecureRandom random) {
        if (keysize <= 0) throw new InvalidParameterException("Keysize < 0");
        this.keysize = keysize;
        this.sr = random;
        this.expiration = 0;
    }


    /**
     * Initialize this keypair with the given parameters and securerandom object
     */
    public void initialize(AlgorithmParameterSpec params, SecureRandom random) 
        throws InvalidAlgorithmParameterException
    {
        if (! legacy)
            throw new InvalidAlgorithmParameterException("Can only initialize "+
                "with an AlgorithmParameterSpec for legacy keys, use the "+
                "initialize method with the keysize for newer keys as that is "+
                "the only possible parameter.");
        if (! (params instanceof PGPLegacyKeyPairGeneratorParameterSpec))
            throw new InvalidAlgorithmParameterException("Parameters not of "+
                "type PGPLegacyKeyPairGeneratorParameterSpec");
        int keysize = 
            ((PGPLegacyKeyPairGeneratorParameterSpec)params).getKeySize();
        if (keysize <= 0) 
            throw new InvalidAlgorithmParameterException("Keysize <= 0");
        int expiration = 
            ((PGPLegacyKeyPairGeneratorParameterSpec)params).getExpiration();
        if (expiration < 0) 
            throw new InvalidAlgorithmParameterException("Expiration < 0");
        if (expiration > 65535) 
            throw new InvalidAlgorithmParameterException("Expiration > 65535");
        this.keysize = keysize;
        this.expiration = expiration;
        this.sr = random;
    }


// Algorithm specific static subclasses
//...........................................................................

    /** DSA signing key */
    public static class SigningDSA extends PGPKeyPairGenerator {
        public SigningDSA() { 
            super(PGPConstants.ALG_DSA_SIGN,        DEFAULT_KEY_SIZE_DSA,
                 "OpenPGP/Signing/DSA",        false, false);
        }
    }
    
    /** RSA signing key */
    public static class SigningRSA extends PGPKeyPairGenerator {
        public SigningRSA() { 
            super(PGPConstants.ALG_RSA_SIGN,        DEFAULT_KEY_SIZE_RSA,
                 "OpenPGP/Signing/RSA",        false, false);
        }
    }
    
    /** ElGamal signing key */
    public static class SigningElGamal extends PGPKeyPairGenerator {
        public SigningElGamal() { 
            super(PGPConstants.ALG_ELGAMAL_SIGN,    DEFAULT_KEY_SIZE_ELGAMAL,
                 "OpenPGP/Signing/ElGamal",    false, false);
        }
    }
    
    /** RSA encryption key */
    public static class EncryptionRSA extends PGPKeyPairGenerator {
        public EncryptionRSA() { 
            super(PGPConstants.ALG_RSA_ENCRYPT,     DEFAULT_KEY_SIZE_RSA,
                 "OpenPGP/Encryption/RSA",     true,  false);
        }
    }
    
    /** ElGamal encryption key */
    public static class EncryptionElGamal extends PGPKeyPairGenerator {
        public EncryptionElGamal() { 
            super(PGPConstants.ALG_ELGAMAL_ENCRYPT, DEFAULT_KEY_SIZE_ELGAMAL,
                 "OpenPGP/Encryption/ElGamal", true,  false);
        }
    }
    
    /** RSA legacy key */
    public static class LegacyRSA extends PGPKeyPairGenerator {
        public LegacyRSA() { 
            super(PGPConstants.ALG_RSA_ENCRYPT,     DEFAULT_KEY_SIZE_RSA,
                 "OpenPGP/Legacy/RSA",         false, true);
        }
    }
    
}
