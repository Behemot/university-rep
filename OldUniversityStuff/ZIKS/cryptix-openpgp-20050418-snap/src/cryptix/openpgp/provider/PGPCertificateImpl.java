/* $Id: PGPCertificateImpl.java,v 1.3 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPCertificate;
import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPKey;
import cryptix.openpgp.PGPPrincipal;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.io.PGPHashDataOutputStream;

import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPSignatureConstants;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPUserIDPacket;

import cryptix.openpgp.signature.PGPByteArraySP;
import cryptix.openpgp.signature.PGPBooleanSP;
import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyFlagsSP;
import cryptix.openpgp.signature.PGPKeyIDSP;
import cryptix.openpgp.signature.PGPNotationDataSP;
import cryptix.openpgp.signature.PGPSignatureSubPacket;
import cryptix.openpgp.signature.PGPStringSP;
import cryptix.openpgp.signature.PGPTrustSP;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;
import cryptix.pki.KeyIDFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;

import java.security.cert.CertificateException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;

import java.util.Date;
import java.util.Properties;
import java.util.Vector;


/**
 * An OpenPGP Certificate.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class PGPCertificateImpl extends PGPCertificate {


// Instance variables
// ..........................................................................

    private PGPSignaturePacket pkt;
    private PGPPrincipal subject;
    private PGPPublicKey key;


// Cached
// ..........................................................................

    private boolean hasCachedCreationDate;
    private Date cachedCreationDate;
    
    private boolean hasCachedExpirationDate;
    private Date cachedExpirationDate;

    private boolean hasCachedIsExportable;
    private boolean cachedIsExportable;
    
    private boolean hasCachedTrust;
    private int cachedTrustLevel, cachedTrustAmount;
    
    private boolean hasCachedTrustRegularExpression;
    private String cachedTrustRegularExpression;
    
    private boolean hasCachedIsRevocable;
    private boolean cachedIsRevocable;
    
    private boolean hasCachedIssuerKeyID;
    private KeyID cachedIssuerKeyID;
    
    private boolean hasCachedNotationData;
    private Properties cachedMachineReadableNotationData;
    private Properties cachedHumanReadableNotationData;
    
    private boolean hasCachedPolicyURL;
    private String cachedPolicyURL;
    
    private boolean hasCachedKeyFlags;
    private boolean cachedKeyFlagsSpecified;
    private boolean cachedKeyFlagCertification;
    private boolean cachedKeyFlagSignData;
    private boolean cachedKeyFlagEncryptCommunication;
    private boolean cachedKeyFlagEncryptStorage;
    
    private boolean hasCachedIssuerUserID;
    private PGPPrincipal cachedIssuerUserID;
    
    private boolean hasCachedPublicKeyID;
    private KeyID cachedPublicKeyID;
    

// Constructor
// ..........................................................................

    /**
     * Create a new Certificate object with the given type.
     */
    /* package */ PGPCertificateImpl(PGPSignaturePacket pkt, 
                                     PGPPrincipal subject, PGPPublicKey key)
    {
        super("OpenPGP");
        
        this.pkt = pkt;
        this.subject = subject;
        this.key = key;
    }


// Methods from java.security.cert.Certificate 
// and cryptix.pki.ExtendedCertificate
// ..........................................................................

    /**
     * Returns the certificate in encoded form.
     */
    public byte[] getEncoded()
        throws CertificateEncodingException
    {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            key.getPacket().encode(baos);
            subject.getPacket().encode(baos);
            pkt.encode(baos);
            baos.close();
            return baos.toByteArray();
            
        } catch (IOException e) {
            throw new InternalError("IOException in ByteArrayOutputStream "+e);
        }
    }


    /**
     * Returns the certified public key.
     */
    public PublicKey getPublicKey()
    {
        return key;
    }


    /**
     * Returns a string representation of this certificate.
     */
    public String toString()
    {
        throw new RuntimeException("NYI");
    }


    /**
     * Verify if the private key corresponding to the supplied public key issued
     * this signature for this certificate.
     */
    public void verify(PublicKey key)
        throws CertificateException, NoSuchAlgorithmException, 
               InvalidKeyException, NoSuchProviderException, SignatureException
    {
        // check key
        if (! (key instanceof PGPPublicKey)) {
            throw new InvalidKeyException("Not instance of PGPPublicKey");
        }
        
        PGPPublicKey signkey = (PGPPublicKey)key;
        
        if (! (signkey.getPacket() instanceof PGPPublicKeyPacket)) {
            throw new InvalidKeyException("Not a public signing key");
        }

        // init
        PGPPublicKeyPacket  keypkt  = (PGPPublicKeyPacket)this.key.getPacket();
        PGPUserIDPacket     idpkt   = (PGPUserIDPacket)subject.getPacket();
        PGPSigner           signer  = (PGPSigner)
                                      signkey.getPacket().getAlgorithm();
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        
        MessageDigest md = factory.getHashAlgorithm(pkt.getHashID());

        signer.initVerify(pkt.getHashID(), factory);
        

        // hash the key
        PGPHashDataOutputStream hdos = 
            new PGPHashDataOutputStream(md, signer);
        try {
            keypkt.encodeBody(hdos);
            hdos.close();
        } catch (IOException ioe) {
            throw new InternalError("IOException on hashing key - "+ioe);
        }
        

        // hash the userID
        byte[] useridbytes;
        try {
            useridbytes = idpkt.getValue().getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported.");
        }
        if (pkt.getVersion() == 4) {
            byte[] pre = new byte[5];
            pre[0] = (byte)0xB4;
            pre[1] = (byte)((useridbytes.length >> 24) & 0xFF);
            pre[2] = (byte)((useridbytes.length >> 16) & 0xFF);
            pre[3] = (byte)((useridbytes.length >>  8) & 0xFF);
            pre[4] = (byte)((useridbytes.length      ) & 0xFF);
            md.update(pre);
            signer.update(pre);
        }
        md.update(useridbytes);
        signer.update(useridbytes);


        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);
        

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }


        // verify two bytes of hash
        byte[] digestcalc  = md.digest();
        byte[] digestinsig = pkt.getHash();
        boolean ok = ((digestcalc[0] == digestinsig[0]) && 
                      (digestcalc[1] == digestinsig[1]));


        if (! ok) {
            // hash was not ok, so no need to verify the sig itself.
            throw new SignatureException("Invalid signature");
        }
        
        // parse signature data
        try {

            pkt.interpretSignature(signer);

        } catch (IOException ioe) {
            throw new InternalError("IOException while parsing signature "+ioe);
        } catch (PGPDataFormatException dfe) {
            throw new CertificateParsingException("Invalid signature");
        }
            
        // the real thing
        if (! signer.verifySignature()) {
            throw new SignatureException("Invalid signature");
        }
        
    }


    /**
     * Verify if the private key corresponding to the supplied public key issued
     * this signature for this certificate, using the given provider.
     */
    public void verify(PublicKey key, String sigProvider)
        throws CertificateException, NoSuchAlgorithmException, 
               InvalidKeyException, NoSuchProviderException, SignatureException
    {
        if (! sigProvider.equals(CryptixOpenPGP.NAME)) {
            throw new NoSuchProviderException("Only "+CryptixOpenPGP.NAME+
                " is supported as a provider.");
        }
        verify(key);
    }


    /** 
     * Verifies that this certificate was signed using the specified keybundle.
     */
    public void verify(KeyBundle bundle)
        throws CertificateException, NoSuchAlgorithmException,
               InvalidKeyException, NoSuchProviderException, SignatureException
    {
        verify((PublicKey)bundle.getPublicKeys().next());
    }


    /**
     * Returns whether the certificate is self signed
     */
    public boolean isSelfSigned()
        throws CertificateException
    {
        if (getIssuerKeyID().match(getPublicKeyID())) 
            return true;
        return false;
    }



// Methods from cryptix.openpgp.PGPCertificate
// ..........................................................................


    /**
     * Return the contained signature packet.
     *
     * <p>Note: packets are part of the low-level API. Normally you don't need
     * to use this method as other options are available.</p>
     */
    public PGPSignaturePacket getPacket()
    {
        return pkt;
    }


    /**
     * Returns the userID for which this certificate was issued.
     */
    public Principal getSubject() {
        return subject;
    }
    

    /**
     * Checks if this certificate is currently valid.
     *
     * <p>A certificate is valid if date is on or after the creation date and
     * before the expiration date (when available).</p>
     */
    public void checkValidity()
        throws CertificateExpiredException, CertificateNotYetValidException,
               CertificateParsingException
    {
        Date now = new Date();
        checkValidity(now);
    }
        

    /**
     * Checks if this certificate is valid on a given date.
     *
     * <p>A certificate is valid if date is on or after the creation date and
     * before the expiration date (when available).</p>
     */
    public void checkValidity(Date date)
        throws CertificateExpiredException, CertificateNotYetValidException,
               CertificateParsingException
    {
        if (date.compareTo(getCreationDate()) < 0) {
            throw new CertificateNotYetValidException(
                "date is before creation");
        }
        if (getExpirationDate() != null) {
            if (date.compareTo(getExpirationDate()) > 0) {
                throw new CertificateExpiredException(
                    "date is after expiration");
            }
        }
    }

    
    private boolean parsed = false;
    
    /** Helper method to parse the signature subpackets if necessairy */
    private void parse()
        throws CertificateParsingException
    {
        try {
            if (parsed) return;
            if (pkt.getVersion() > 3) pkt.parseSignatureSubPackets();
            parsed = true;
        } catch (PGPDataFormatException pdfe) {
            throw new CertificateParsingException(""+pdfe);
        } catch (PGPFatalDataFormatException pfdfe) {
            throw new CertificateParsingException(""+pfdfe);
        }
    }


    /** helper method for getting hashed packets */
    /* package */ PGPSignatureSubPacket getHashedPacket(byte ID)
        throws CertificateParsingException
    {
        parse();
        if (pkt.getVersion() == 3) return null;
        if (pkt.getVersion() != 4) {
            throw new CertificateParsingException(
                "Invalid sig version "+pkt.getVersion());
        }
        
        Vector hashed = pkt.getHashedSubPackets();
        Vector unhashed = pkt.getUnhashedSubPackets();

        PGPSignatureSubPacket ssp;
        PGPSignatureSubPacket thessp = null;
        
        for (int i=0; i<unhashed.size(); i++) {
            
            ssp = (PGPSignatureSubPacket)unhashed.elementAt(i);
            if (ssp.getPacketID() == ID) {
                throw new CertificateParsingException(
                "Packet found in unhashed area.");
            }
            
        }

        for (int i=0; i<hashed.size(); i++) {
            
            ssp = (PGPSignatureSubPacket)hashed.elementAt(i);
            if (ssp.getPacketID() == ID) {
                if (thessp != null) {
                    throw new CertificateParsingException(
                        "Packet found more than once.");
                } else {
                    thessp = ssp;
                }
            }
            
        }
        
        return thessp;
    }


    /** helper method for getting hashed or unhashed packets */
    /* package */ PGPSignatureSubPacket getUnhashedPacket(byte ID)
        throws CertificateParsingException
    {
        parse();
        if (pkt.getVersion() == 3) return null;
        if (pkt.getVersion() != 4) {
            throw new CertificateParsingException(
                "Invalid sig version "+pkt.getVersion());
        }
        
        Vector ssps = pkt.getAllSubPackets();
        
        PGPSignatureSubPacket ssp;
        PGPSignatureSubPacket thessp = null;
        
        for (int i=0; i<ssps.size(); i++) {
            
            ssp = (PGPSignatureSubPacket)ssps.elementAt(i);
            if (ssp.getPacketID() == ID) {
                if (thessp != null) {
                    throw new CertificateParsingException(
                        "Packet found more than once.");
                } else {
                    thessp = ssp;
                }
            }
            
        }
        
        return thessp;
    }


    /**
     * Returns the creation date and time.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 2: 
     * 'signature creation time'.</p>
     */
    public Date getCreationDate()
        throws CertificateParsingException
    {
        if (hasCachedCreationDate) return cachedCreationDate;
    
        if (pkt.getVersion() == 3) {

            byte[] timebytes = pkt.getTime();
            long time = ((((int)timebytes[0]) & 0xff) << 24) +
                        ((((int)timebytes[1]) & 0xff) << 16) +
                        ((((int)timebytes[2]) & 0xff) <<  8) +
                        ((((int)timebytes[3]) & 0xff));
            time = time * 1000;
            
            cachedCreationDate = new Date(time);
            hasCachedCreationDate = true;
            return cachedCreationDate;
        
        } else if (pkt.getVersion() == 4) {
        
            PGPSignatureSubPacket sp = getHashedPacket(
                PGPSignatureConstants.SSP_SIG_CREATION_TIME);
                
            if (sp == null) {
                throw new CertificateParsingException(
                    "No creation time in hashed area.");
            }

            cachedCreationDate = ((PGPDateSP)sp).getValue();
            hasCachedCreationDate = true;
            return cachedCreationDate;
            
        } else {
            throw new CertificateParsingException(
                "Invalid sig version "+pkt.getVersion());
        }
    }


    /**
     * Returns the expiration date and time.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 3: 
     * 'signature expiration time'.</p>
     *
     * <p>Returns null if the certificate does not expire.</p>
     */
    public Date getExpirationDate()
        throws CertificateParsingException
    {
        if (hasCachedExpirationDate) return cachedExpirationDate;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_SIG_EXPIRATION_TIME);

        if (sp == null) {
            cachedExpirationDate = null;
        } else {
            cachedExpirationDate = ((PGPDateSP)sp).getValue();
        }

        hasCachedExpirationDate = true;
        return cachedExpirationDate;
    }
    

    /** 
     * Returns whether a certification signature is exportable.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 4: 
     * 'exportable certification'.</p>
     *
     * <p>Non-exportable, or "local," certifications are signatures made by a
     * user to mark a key as valid within that user's implementation only.
     * Thus, when an implementation prepares a user's copy of a key for
     * transport to another user (this is the process of "exporting" the
     * key), any local certification signatures are deleted from the key.</p>
     */
    public boolean isExportable()
        throws CertificateParsingException
    {
        if (hasCachedIsExportable) return cachedIsExportable;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_EXPORTABLE_CERTIFICATION);
        
        if (sp == null) {
            cachedIsExportable = true;
        } else {
            cachedIsExportable = ((PGPBooleanSP)sp).getValue();
        }
        
        hasCachedIsExportable = true;
        return cachedIsExportable;
    }
    
    
    /** 
     * Returns the trust level
     *
     * <p>Corresponds with OpenPGP signature subpacket type 5: 
     * 'trust signature'.</p>
     *
     * <p>Meaning of the levels: Level 0 has the same meaning as an ordinary
     * validity signature.  Level 1 means that the signed key is asserted
     * to be a valid trusted introducer, with the 2nd octet of the body
     * specifying the degree of trust. Level 2 means that the signed key is
     * asserted to be trusted to issue level 1 trust signatures, i.e. that
     * it is a "meta introducer". Generally, a level n trust signature
     * asserts that a key is trusted to issue level n-1 trust signatures.</p>
     */
    public int getTrustLevel()
        throws CertificateParsingException
    {
        if (hasCachedTrust) return cachedTrustLevel;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_TRUST_SIGNATURE);
        
        if (sp == null) {
            cachedTrustLevel = 0;
            cachedTrustAmount = 0;
        } else {
            cachedTrustLevel = ((PGPTrustSP)sp).getDepth();
            cachedTrustAmount = ((PGPTrustSP)sp).getAmount();
        }
        
        hasCachedTrust = true;
        return cachedTrustLevel;
    }
    

    /** 
     * Returns the trust amount
     *
     * <p>Corresponds with OpenPGP signature subpacket type 5: 
     * 'trust signature'.</p>
     * 
     * <p>The trust amount is in a range from 0-255, interpreted such that
     * values less than 120 indicate partial trust and values of 120 or
     * greater indicate complete trust.  The default values used by most
     * applications are 60 for partial trust and 120 for complete trust.</p>
     */
    public int getTrustAmount()
        throws CertificateParsingException
    {
        if (hasCachedTrust) return cachedTrustAmount;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_TRUST_SIGNATURE);
        
        if (sp == null) {
            cachedTrustLevel = 0;
            cachedTrustAmount = 0;
        } else {
            cachedTrustLevel = ((PGPTrustSP)sp).getDepth();
            cachedTrustAmount = ((PGPTrustSP)sp).getAmount();
        }
        
        hasCachedTrust = true;
        return cachedTrustAmount;
    }


    /** 
     * Returns a regular expression that limits the scope of trust levels > 0.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 6: 
     * 'regular expression'.</p>
     * 
     * <p>Used in conjunction with trust signature packets (of level > 0) to
     * limit the scope of trust that is extended.  Only signatures by the
     * target key on user IDs that match the regular expression in the body
     * of this packet have trust extended by the trust signature subpacket.</p>
     *
     * <p>Regarding the syntax, here is an integral quote from RFC 2440:</p>
     * <pre>
     * 8. Regular Expressions
     * 
     *    A regular expression is zero or more branches, separated by '|'. It
     *    matches anything that matches one of the branches.
     * 
     *    A branch is zero or more pieces, concatenated. It matches a match
     *    for the first, followed by a match for the second, etc.
     * 
     *    A piece is an atom possibly followed by '*', '+', or '?'. An atom
     *    followed by '*' matches a sequence of 0 or more matches of the atom.
     *    An atom followed by '+' matches a sequence of 1 or more matches of
     *    the atom. An atom followed by '?' matches a match of the atom, or
     *    the null string.
     * 
     *    An atom is a regular expression in parentheses (matching a match for
     *    the regular expression), a range (see below), '.' (matching any
     *    single character), '^' (matching the null string at the beginning of
     *    the input string), '$' (matching the null string at the end of the
     *    input string), a '\' followed by a single character (matching that
     *    character), or a single character with no other significance
     *    (matching that character).
     * 
     *    A range is a sequence of characters enclosed in '[]'. It normally
     *    matches any single character from the sequence. If the sequence
     *    begins with '^', it matches any single character not from the rest
     *    of the sequence. If two characters in the sequence are separated by
     *    '-', this is shorthand for the full list of ASCII characters between
     *    them (e.g. '[0-9]' matches any decimal digit). To include a literal
     *    ']' in the sequence, make it the first character (following a
     *    possible '^').  To include a literal '-', make it the first or last
     *    character.
     * </pre>
     *
     * @returns the regular expression or null if no regular expression is
     *          available
     */
    public String getTrustRegularExpression()
        throws CertificateParsingException
    {
        if (hasCachedTrustRegularExpression) 
            return cachedTrustRegularExpression;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_REGULAR_EXPRESSION);
        
        if (sp == null) {
            cachedTrustRegularExpression = null;
        } else {
            cachedTrustRegularExpression = ((PGPStringSP)sp).getValue();
        }
        
        hasCachedTrustRegularExpression = true;
        return cachedTrustRegularExpression;
    }


    /**
     * Returns signature's revocability status.  
     *
     * <p>Corresponds with OpenPGP signature subpacket type 7: 
     * 'revocable'.</p>
     * 
     * <p>Returns a boolean flag indicating whether the signature is revocable.
     * Signatures that are not revocable have any later revocation signatures 
     * ignored. They represent a commitment by the signer that he cannot revoke
     * his signature for the life of his key.  </p>
     */
    public boolean isRevocable()
        throws CertificateParsingException
    {
        if (hasCachedIsRevocable) return cachedIsRevocable;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_REVOCABLE);
        
        if (sp == null) {
            cachedIsRevocable = true;
        } else {
            cachedIsRevocable = ((PGPBooleanSP)sp).getValue();
        }
        
        hasCachedIsRevocable = true;
        return cachedIsRevocable;
    }


    /**
     * Returns the keyID of the key issuing the certificate.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 16: 
     * 'issuer key ID'.</p>
     */
    public KeyID getIssuerKeyID()
        throws CertificateParsingException
    {
        if (hasCachedIssuerKeyID) return cachedIssuerKeyID;
        
        if (pkt.getVersion() == 3) {

            byte[] keyid = pkt.getKeyID();
            cachedIssuerKeyID = new PGPKeyIDImpl(null, keyid, 3);
            
        } else if (pkt.getVersion() == 4) {
            
            PGPSignatureSubPacket sp = getUnhashedPacket(
                PGPSignatureConstants.SSP_ISSUER_KEYID);

            if (sp == null) {
                cachedIssuerKeyID = null;
            } else {
                byte[] keyid = ((PGPKeyIDSP)sp).getValue();
                cachedIssuerKeyID = new PGPKeyIDImpl(null, keyid, 4);
            }

        } else {
            throw new CertificateParsingException(
                "Invalid sig version "+pkt.getVersion());
        }
        
        hasCachedIssuerKeyID = true;
        return cachedIssuerKeyID;
    }
    

    /**
     * Returns the keyID of the public key from this certificate.
     */
    public KeyID getPublicKeyID()
        throws CertificateParsingException
    {
        try {
            
            if (hasCachedPublicKeyID) return cachedPublicKeyID;
            
            KeyIDFactory kf = KeyIDFactory.getInstance("OpenPGP");
            cachedPublicKeyID = kf.generateKeyID(key);
                    
            hasCachedPublicKeyID = true;
            return cachedPublicKeyID;
        
        } catch (InvalidKeyException ike) {
            throw new CertificateParsingException(""+ike);
        } catch (NoSuchAlgorithmException nsae) {
            throw new CertificateParsingException(""+nsae);
        }
    }
    
    /** Helper method to cache the notation data */
    private void cacheNotationData() {
        cachedMachineReadableNotationData = new Properties();
        cachedHumanReadableNotationData = new Properties();
        
        if (pkt.getVersion() == 3) return;
        
        Vector hashed = pkt.getHashedSubPackets();
        for (int i=0; i<hashed.size(); i++) {

            PGPSignatureSubPacket sp = 
                (PGPSignatureSubPacket)hashed.elementAt(i);

            if (sp instanceof PGPNotationDataSP) {

                PGPNotationDataSP nsp = (PGPNotationDataSP)sp;
                String name = nsp.getNameData();
                String value = nsp.getValueData();

                if (nsp.getHumanReadable()) {
                    cachedHumanReadableNotationData.setProperty(name,value);
                } else {
                    cachedMachineReadableNotationData.setProperty(name,value);
                }
            }
        }
    }

    /**
     * Returns the list of machine readable notations on the certification that 
     * the issuer wishes to make.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 20: 
     * 'notation data'.</p>
     *
     * <p>Returned is a Properties object, containing a (possibly empty) set
     * of keys and values. The keys reside in two name spaces: The IETF name 
     * space and the user name space.
     * </p><p>
     * The IETF name space is registered with IANA. These keys will not
     * contain the "@" character (0x40) as this is a tag for the user name
     * space.
     * </p><p>
     * Keys in the user name space consist of a string tag followed
     * by "@" followed by a DNS domain name. For example, the "sample" tag used by
     * Example Corporation could be "sample@example.com".
     * </p>
     */
    public Properties getMachineReadableNotationData()
        throws CertificateParsingException
    {
        if (hasCachedNotationData) return cachedMachineReadableNotationData;
        
        cacheNotationData();
        
        return cachedMachineReadableNotationData;
    }
    

    /**
     * Returns the list of human readable notations on the certification that 
     * the issuer wishes to make.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 20: 
     * 'notation data'.</p>
     *
     * <p>Returned is a Properties object, containing a (possibly empty) set
     * of keys and values. The keys reside in two name spaces: The IETF name 
     * space and the user name space.
     * </p><p>
     * The IETF name space is registered with IANA. These keys will not
     * contain the "@" character (0x40) as this is a tag for the user name
     * space.
     * </p><p>
     * Keys in the user name space consist of a string tag followed
     * by "@" followed by a DNS domain name. For example, the "sample" tag used by
     * Example Corporation could be "sample@example.com".
     * </p>
     */
    public Properties getHumanReadableNotationData()
        throws CertificateParsingException
    {
        if (hasCachedNotationData) return cachedHumanReadableNotationData;
        
        cacheNotationData();
        
        return cachedHumanReadableNotationData;
    }
    

    /**
     * Returns a URL of a document that describes the policy that the signature
     * was issued under
     *
     * <p>Corresponds with OpenPGP signature subpacket type 26: 
     * 'policy URL'.</p>
     */
    public String getPolicyURL()
        throws CertificateParsingException
    {
        if (hasCachedPolicyURL) return cachedPolicyURL;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_POLICY_URL);
        
        if (sp == null) {
            cachedPolicyURL = null;
        } else {
            cachedPolicyURL = ((PGPStringSP)sp).getValue();
        }
        
        hasCachedPolicyURL = true;
        return cachedPolicyURL;
    }


    /** Helper method for geKeyFlag* methods */
    private void cacheKeyFlags() 
        throws CertificateParsingException
    {
        hasCachedKeyFlags = true;

        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_KEY_FLAGS);
        
        if (sp == null) {
            cachedKeyFlagsSpecified = false;
        } else {
            cachedKeyFlagsSpecified = true;
            cachedKeyFlagCertification = 
                ((PGPKeyFlagsSP)sp).getCertify();
            cachedKeyFlagSignData = 
                ((PGPKeyFlagsSP)sp).getSign();
            cachedKeyFlagEncryptCommunication = 
                ((PGPKeyFlagsSP)sp).getEncryptCommunication();
            cachedKeyFlagEncryptStorage = 
                ((PGPKeyFlagsSP)sp).getEncryptStorage();
        }

    }

    /**
     * Returns whether key flags are specified
     *
     * <p>Corresponds with OpenPGP signature subpacket type 27: 
     * 'key flags'.</p>
     *
     * <p>If this method returns true, the application can call one of the
     * other getKeyFlag* methods for the specific key flags.</p>
     */
    public boolean getKeyFlagsSpecified()
        throws CertificateParsingException
    {
        if (! hasCachedKeyFlags) {
            cacheKeyFlags();
        }
        
        return cachedKeyFlagsSpecified;
    }


    /**
     * Returns whether the certified key may be used to certify other keys.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public boolean getKeyFlagCertification()
        throws CertificateParsingException
    {
        if (! hasCachedKeyFlags) {
            cacheKeyFlags();
        }
        
        if (! cachedKeyFlagsSpecified) {
            throw new UnsupportedOperationException("Key flags not specified");
        }
        
        return cachedKeyFlagCertification;
    }


    /**
     * Returns whether the certified key may be used to sign data.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public boolean getKeyFlagSignData()
        throws CertificateParsingException
    {
        if (! hasCachedKeyFlags) {
            cacheKeyFlags();
        }
        
        if (! cachedKeyFlagsSpecified) {
            throw new UnsupportedOperationException("Key flags not specified");
        }
        
        return cachedKeyFlagSignData;
    }


    /**
     * Returns whether the certified key may be used to encrypt communications.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public boolean getKeyFlagEncryptCommunication()
        throws CertificateParsingException
    {
        if (! hasCachedKeyFlags) {
            cacheKeyFlags();
        }
        
        if (! cachedKeyFlagsSpecified) {
            throw new UnsupportedOperationException("Key flags not specified");
        }
        
        return cachedKeyFlagEncryptCommunication;
    }


    /**
     * Returns whether the certified key may be used to encrypt storage.
     *
     * @throws UnsupportedOperationException if getKeyFlagsSpecified() returns
     *         false;
     */
    public boolean getKeyFlagEncryptStorage()
        throws CertificateParsingException
    {
        if (! hasCachedKeyFlags) {
            cacheKeyFlags();
        }
        
        if (! cachedKeyFlagsSpecified) {
            throw new UnsupportedOperationException("Key flags not specified");
        }
        
        return cachedKeyFlagEncryptStorage;
    }


    /**
     * Returns which user id of the issuer was used to issue this certificate.
     *
     * <p>Corresponds with OpenPGP signature subpacket type 28: 
     * 'signer's user id'.</p>
     *
     * <p>Many keyholders use a single key for different purposes, such as 
     * business communications as well as personal communications. This 
     * subpacket allows such a keyholder to state which of their roles is 
     * making a signature.</p>
     */
    public PGPPrincipal getIssuerUserID()
        throws CertificateParsingException
    {
        if (hasCachedIssuerUserID) return cachedIssuerUserID;
        
        PGPSignatureSubPacket sp = getHashedPacket(
            PGPSignatureConstants.SSP_SIGNER_USERID);
        
        if (sp == null) {
            cachedIssuerUserID = null;
        } else {
            String value = ((PGPStringSP)sp).getValue();
            PGPUserIDPacket uidpkt = new PGPUserIDPacket();
            uidpkt.setValue(value);
            cachedIssuerUserID = new PGPUserIDPrincipal(uidpkt);
        }
        
        hasCachedIssuerUserID = true;
        return cachedIssuerUserID;    
    }

}
