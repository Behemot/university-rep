/* $Id: PGPKeyBundleMessageImpl.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.KeyBundleMessage;
import cryptix.message.Message;
import cryptix.message.MessageException;
import cryptix.message.NotEncryptedToParameterException;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;

import java.security.Key;
import java.security.UnrecoverableKeyException;


/**
 * Represents an encrypted message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyBundleMessageImpl extends KeyBundleMessage {


// Instance variables
// ..........................................................................

    
    private KeyBundle bundle;
    

// Constructor
// ..........................................................................

    /**
     * Creates a KeyBundleMessage of the specified format.
     */
    public PGPKeyBundleMessageImpl(KeyBundle bundle) {
        super("OpenPGP");
        this.bundle = bundle;
    }


// Methods from Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException
    {
        try {

            return bundle.getEncoded();

        } catch (KeyBundleException kbe) {
            throw new MessageException(""+kbe);
        }
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @returns the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
        throw new IllegalArgumentException("Attributes not supported.");
    }


// Methods from KeyBundleMessage
// ..........................................................................

    /**
     * Returns the KeyBundle contained in this message.
     *
     * @returns the contained keybundle.
     */
    public KeyBundle getKeyBundle() {
        return bundle;
    }


}
