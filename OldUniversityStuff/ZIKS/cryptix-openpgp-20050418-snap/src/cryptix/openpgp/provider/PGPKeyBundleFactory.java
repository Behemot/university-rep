/* $Id: PGPKeyBundleFactory.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPPublicKey;
import cryptix.openpgp.PGPPrincipal;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.packet.PGPPacket;
import cryptix.openpgp.packet.PGPPacketFactory;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPPublicSubKeyPacket;
import cryptix.openpgp.packet.PGPSecretKeyPacket;
import cryptix.openpgp.packet.PGPSecretSubKeyPacket;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPTrustPacket;
import cryptix.openpgp.packet.PGPUserIDPacket;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;
import cryptix.pki.KeyBundleFactorySpi;

import java.io.InputStream;
import java.io.IOException;

import java.util.Collection;
import java.util.Vector;


/**
 * Service provider interface for KeyBundleFactory
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyBundleFactory extends KeyBundleFactorySpi {

    private static final int NO_KEY      = 0;
    private static final int MAIN_KEY    = 1;
    private static final int USERID      = 2;
    private static final int SUB_KEY     = 3;
    private static final int SUB_KEY_SIG = 4;
    private static final int UNKNOWN     = 4;
    

    /**
     * Returns a PGPKeyBundle if onlyOne is true, otherwise a Vector.
     */
    /* package */ static Object helper(InputStream in, boolean onlyOne) 
        throws KeyBundleException, IOException
    {
        PGPPacketFactory    pf = PGPPacketFactory.getDefaultInstance();
        PGPAlgorithmFactory af = PGPAlgorithmFactory.getDefaultInstance();

        PGPPacket             pkt               = null;
        PGPPrincipal          lastUserID        = null;
        Vector                lastCertMap       = null;
        PGPPublicKey          lastPublicSubkey  = null;
        PGPSecretSubKeyPacket lastPrivateSubkey = null;
        
        PGPKeyBundleImpl bundle = new PGPKeyBundleImpl();
        Vector result = new Vector();

        int state = NO_KEY;
        boolean secret = false;
        
        if (in.available() == 0) {
            if (onlyOne) return null;   
            else         return result; // empty vector
        }
        
        while (in.available() > 0) {
            
            try {
                if (onlyOne) in.mark(16384);
                pkt = pf.readPacket(in, af);
            } catch (PGPDataFormatException dfe) {
                // ignore
            } catch (PGPFatalDataFormatException fdfe) {
                throw new KeyBundleException(""+fdfe);
            }
            
            if (pkt instanceof PGPPublicKeyPacket) {

                if (state != NO_KEY) {
                
                    if (onlyOne) {
                        in.reset();
                        return bundle;
                    }
                    
                    result.add(bundle);
                    bundle = new PGPKeyBundleImpl();
                }

                state = MAIN_KEY;
                secret = false;

                // ### FIXME: make this smarter to support dual-use keys
                String name = "Signing/"+af.getPublicKeyName(
                    ((PGPPublicKeyPacket)pkt).getAlgorithmID());

                bundle.mainkey = 
                    new PGPPublicKey((PGPPublicKeyPacket)pkt, name);

            } else if (pkt instanceof PGPSecretKeyPacket) {

                if (state != NO_KEY) {
                    result.add(bundle);
                    bundle = new PGPKeyBundleImpl();
                }

                state = MAIN_KEY;
                secret = true;

                // ### FIXME: make this smarter to support dual-use keys
                String name = "Signing/"+af.getPublicKeyName(
                    ((PGPSecretKeyPacket)pkt).getAlgorithmID());

                bundle.mainkey = new PGPPublicKey( 
                    ((PGPSecretKeyPacket)pkt).clonePublic(), name);
                bundle.privpkt = (PGPSecretKeyPacket)pkt;
            
            } else if (pkt instanceof PGPUserIDPacket) {
            
                if ((state == NO_KEY) || (state == SUB_KEY)) {
                    throw new KeyBundleException(
                        "UserID packet found at invalid location");
                } 
                
                state = USERID;
                
                lastUserID = new PGPUserIDPrincipal((PGPUserIDPacket)pkt);
                lastCertMap = new Vector();
                
                bundle.principals.add(lastUserID);
                bundle.principalsToCertificates.put(lastUserID, lastCertMap);
                
            } else if (pkt instanceof PGPPublicSubKeyPacket) {
            
                if (state == NO_KEY) {
                    throw new KeyBundleException(
                        "Subkey found at invalid location");
                }
                //if (secret) {
                //    throw new KeyBundleException(
                //        "Expected private subkey, found public subkey");
                //}
                
                state = SUB_KEY;
                
                // ### FIXME: make this smarter to support dual-use keys
                String name = "Encryption/"+af.getPublicKeyName(
                    ((PGPPublicSubKeyPacket)pkt).getAlgorithmID());
                    
                lastPublicSubkey = 
                    new PGPPublicKey((PGPPublicSubKeyPacket)pkt, name);
                lastPrivateSubkey = null;
                
                bundle.subkeys.add(lastPublicSubkey);

            } else if (pkt instanceof PGPSecretSubKeyPacket) {

                if (state == NO_KEY) {
                    throw new KeyBundleException(
                        "Subkey found at invalid location");
                }
                if (! secret) {
                    throw new KeyBundleException(
                        "Expected public subkey, found private subkey");
                }
                
                state = SUB_KEY;
                
                // ### FIXME: make this smarter to support dual-use keys
                String name = "Encryption/"+af.getPublicKeyName(
                    ((PGPSecretSubKeyPacket)pkt).getAlgorithmID());
                
                lastPublicSubkey = new PGPPublicKey(
                    ((PGPSecretSubKeyPacket)pkt).clonePublicSub(), name);
                lastPrivateSubkey = (PGPSecretSubKeyPacket)pkt;
                
                bundle.subkeys.add(lastPublicSubkey);
                bundle.privSubkeys.put(lastPublicSubkey, lastPrivateSubkey);

            } else if (pkt instanceof PGPSignaturePacket) { 
            
                if (state == NO_KEY) {

                    throw new KeyBundleException(
                        "Signature found at invalid location");

                } else if (state == MAIN_KEY) {

                    bundle.directKeySigs.add(pkt);

                } else if (state == USERID) {

                    PGPCertificateImpl cert = new PGPCertificateImpl(
                        (PGPSignaturePacket)pkt, lastUserID, bundle.mainkey);

                    bundle.certificates.add(cert);
                    lastCertMap.add(cert);

                } else if (state == SUB_KEY) {
                
                    state = SUB_KEY_SIG;
                    
                    bundle.subkeyBindingSigs.put(lastPublicSubkey, pkt);
                    
                } else if (state == SUB_KEY_SIG) {

                    // ### FIXME
                    // this is probably a revocation sig, so do something here
                    // for now: ignore
                
                } else if (state == UNKNOWN) {
                
                    // ignore sig
                    
                }
            
            } else if (pkt instanceof PGPTrustPacket) { 
            
                // ### FIXME
                // hmm... should find out how to parse these
            
            } else {
            
                // unknown packet, ignore ;)
                //
                // set state in order to avoid putting signatures on the wrong 
                // object
                //
                state = UNKNOWN;
            
            }
        }
        
        result.add(bundle);
        return result;
        
    }
    

    /**
     * Generates a (possible empty) collection of keybundles from an input 
     * stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does 
     * not support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public Collection engineGenerateKeyBundles(InputStream in) 
        throws KeyBundleException, IOException
    {
        return (Collection)helper(in,false);
    }
    

    /**
     * Generates a KeyBundle from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does 
     * not support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public KeyBundle engineGenerateKeyBundle(InputStream in)
        throws KeyBundleException, IOException
    {
        Object result = helper(in,true);
        if (result instanceof Vector)
            return (KeyBundle)((Vector)result).iterator().next();
        else
            return (KeyBundle)result;
    }


    /**
     * Generates a new empty KeyBundle.
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public KeyBundle engineGenerateEmptyKeyBundle()
        throws KeyBundleException
    {
        return new PGPKeyBundleImpl();
    }
        
}
