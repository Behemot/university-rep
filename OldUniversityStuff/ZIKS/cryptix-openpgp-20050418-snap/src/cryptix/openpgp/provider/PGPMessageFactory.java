/* $Id: PGPMessageFactory.java,v 1.5 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.Message;
import cryptix.message.MessageException;
import cryptix.message.MessageFactorySpi;

import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.packet.PGPCompressedDataPacket;
import cryptix.openpgp.packet.PGPEncryptedDataPacket;
import cryptix.openpgp.packet.PGPLiteralDataPacket;
import cryptix.openpgp.packet.PGPOnePassSignaturePacket;
import cryptix.openpgp.packet.PGPPacket;
import cryptix.openpgp.packet.PGPPacketFactory;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPSecretKeyPacket;
import cryptix.openpgp.packet.PGPSessionKeyPacket;
import cryptix.openpgp.packet.PGPSignaturePacket;

import cryptix.openpgp.util.PGPArmoury;

import cryptix.pki.KeyBundleException;

import java.io.ByteArrayInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;


/**
 * Service provider interface for MessageFactory
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.5 $
 */
public class PGPMessageFactory extends MessageFactorySpi {

    /**
     * Generates a (possible empty) collection of messages from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     */
    public Collection engineGenerateMessages(InputStream stream) 
        throws MessageException, IOException
    {
        BufferedInputStream in = new BufferedInputStream(stream);
        in.mark(1);
        int firstbyte = in.read();
        in.reset();
        
        if (firstbyte > 127)
            return generateBinary(in);
        else
            return generateAscii(in);
    }
    
    public static final String CRLF = "\r\n";
    
    private Collection generateAscii(BufferedInputStream in) 
        throws MessageException, IOException
    {
    
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        StringBuffer buf = new StringBuffer();
        PGPArmoury armoury;
        Vector result = new Vector();

        String line = br.readLine();

        while (line != null) {

            if (line.startsWith("-----BEGIN")) {

                do {
                    buf.append(line);
                    buf.append(CRLF);
                
                    line = br.readLine();

                } while (line != null && !line.startsWith("-----END"));

                if (line != null) {

                    buf.append(line);
                    buf.append(CRLF);
                
                    line = br.readLine();
                }

                try {

                    armoury = new PGPArmoury(buf.toString());

                } catch (IllegalArgumentException iae) {
                    throw new MessageException(iae.getMessage());
                }

                if (armoury.getClearText() != null) {

                    String text;
                    try {
                        text = new String(armoury.getClearText(), "UTF-8");
                    } catch (UnsupportedEncodingException uee) {
                        throw new InternalError("UTF-8 encoding not supported - "+uee);
                    }
                    byte[] data = armoury.getPayload();

                    PGPLiteralDataPacket ldp = new PGPLiteralDataPacket();
                    ldp.setData(text);
                    PGPLiteralMessageImpl lm = new PGPLiteralMessageImpl(ldp);

                    PGPPacketFactory pf = 
                        PGPPacketFactory.getDefaultInstance();
                    PGPAlgorithmFactory af = 
                        PGPAlgorithmFactory.getDefaultInstance();
                    ByteArrayInputStream bais = new ByteArrayInputStream(data);

                    PGPPacket pkt = null;
                    try {
                        pkt = pf.readPacket(bais, af);
                    } catch (PGPDataFormatException dfe) {
                        throw new MessageException(""+dfe);
                    } catch (PGPFatalDataFormatException fdfe) {
                        throw new MessageException(""+fdfe);
                    }

                    if (! (pkt instanceof PGPSignaturePacket)) {
                        throw new MessageException("Signature expected");
                    }
                    PGPSignaturePacket sp = (PGPSignaturePacket)pkt;

                    PGPSignedMessageImpl sm = 
                        new PGPSignedMessageImpl(false, sp, lm);

                    result.add(sm);

                } else {

                    byte[] data = armoury.getPayload();
                    ByteArrayInputStream bais = new ByteArrayInputStream(data);
                    result.addAll(generateBinary(bais));

                }

            } else {

                do {
                    buf.append(line);
                    buf.append(CRLF);

                    line = br.readLine();
                } while (line != null && !line.startsWith("-----BEGIN"));

                PGPLiteralDataPacket ldp = new PGPLiteralDataPacket();
                ldp.setData(buf.toString());
                PGPLiteralMessageImpl lm = new PGPLiteralMessageImpl(ldp);
                
                result.add(lm);
            }
            
            buf = new StringBuffer();
        }
        
        return result;
    }
    
    private Collection generateBinary(InputStream in) 
        throws MessageException, IOException
    {
        PGPPacketFactory    pf = PGPPacketFactory.getDefaultInstance();
        PGPAlgorithmFactory af = PGPAlgorithmFactory.getDefaultInstance();

        PGPPacket pkt = null;
        Vector result = new Vector();
        Vector sessionkeys = new Vector();
        
        while (in.available() > 0) {
            
            try {
                in.mark(16384);
                pkt = pf.readPacket(in, af);
            } catch (PGPDataFormatException dfe) {
                throw new MessageException(""+dfe);
            } catch (PGPFatalDataFormatException fdfe) {
                throw new MessageException(""+fdfe);
            }
            
            if (pkt instanceof PGPSessionKeyPacket) {
            
                sessionkeys.add(pkt);
            
            } else if (pkt instanceof PGPEncryptedDataPacket) {
            
                PGPSessionKeyPacket[] sks = 
                    new PGPSessionKeyPacket[sessionkeys.size()];
                for (int i=0; i<sks.length; i++) {
                    sks[i] = (PGPSessionKeyPacket)sessionkeys.elementAt(i);
                }
                result.add(new PGPEncryptedMessageImpl(sks, 
                                   (PGPEncryptedDataPacket)pkt));
                           
            } else if ((pkt instanceof PGPPublicKeyPacket) || 
                       (pkt instanceof PGPSecretKeyPacket))
            {
                try {
                    in.reset();

                    Object x = PGPKeyBundleFactory.helper(in, false);

                    if (x instanceof Collection) {
                        Iterator it = ((Collection)x).iterator();
                        while (it.hasNext()) {
                            PGPKeyBundleImpl k = (PGPKeyBundleImpl)it.next();
                            result.add(new PGPKeyBundleMessageImpl(k));
                        }
                    } else {
                        PGPKeyBundleImpl k = (PGPKeyBundleImpl)x;
                        result.add(new PGPKeyBundleMessageImpl(k));
                    }

                } catch (KeyBundleException kbe) {
                    throw new MessageException(""+kbe);
                }


            } else if (pkt instanceof PGPOnePassSignaturePacket) {
                
                // ignore this
                // we buffer the entire packet anyway, so doing one pass sigs
                // does not make much sense
                
                
            } else if (pkt instanceof PGPCompressedDataPacket) {
                
                Enumeration en = ((PGPCompressedDataPacket)pkt).listPackets();
                Object obj = en.nextElement();
                
                if (obj instanceof PGPSignaturePacket) 
                {
                    Object obj2 = en.nextElement();
                    
                    if (! (obj2 instanceof PGPLiteralDataPacket)) {
                        throw new MessageException("Invalid message format.");
                    }
                    
                    PGPSignedMessageImpl sm = new PGPSignedMessageImpl(false, 
                        (PGPSignaturePacket)obj, new PGPLiteralMessageImpl(
                            (PGPLiteralDataPacket)obj2));
                            
                    result.add(sm);
                    
                } else {

                    if (! (obj instanceof PGPLiteralDataPacket)) {
                        throw new MessageException("Invalid message format.");
                    }
                    
                    PGPLiteralMessageImpl lm = new PGPLiteralMessageImpl(
                                                    (PGPLiteralDataPacket)obj);
                    result.add(lm);
                }
                                
                
            } else if (pkt instanceof PGPLiteralDataPacket) {

                PGPLiteralMessageImpl lm = new PGPLiteralMessageImpl(
                                                    (PGPLiteralDataPacket)pkt);

                if ((result.size() > 0) && (result.elementAt(result.size()-1)
                        instanceof PGPDetachedSignatureMessageImpl))
                {

                    PGPDetachedSignatureMessageImpl dsm = 
                        (PGPDetachedSignatureMessageImpl)
                        result.remove(result.size() - 1);
                    PGPSignedMessageImpl sm = new PGPSignedMessageImpl(
                        false, dsm.getPacket(), lm);
                    result.add(sm);

                } else {
                    
                    result.add(lm);
                    
                }
                

            } else if (pkt instanceof PGPSignaturePacket) {
                
                if ((result.size() > 0) && (result.elementAt(result.size()-1)
                        instanceof PGPLiteralMessageImpl))
                {

                    PGPLiteralMessageImpl lm = (PGPLiteralMessageImpl)
                        result.remove(result.size() - 1);
                    PGPSignedMessageImpl sm = new PGPSignedMessageImpl(
                        false, (PGPSignaturePacket)pkt, lm);
                    result.add(sm);

                } else {
                    
                    PGPDetachedSignatureMessageImpl dsm = 
                        new PGPDetachedSignatureMessageImpl(
                            (PGPSignaturePacket)pkt);
                    result.add(dsm);
                    
                }
                

            } else {
                // ignore unknown packet
            }
            
        }
        
        return result;
    }
    

    /**
     * Generates a messages from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does 
     * not support the mark() and reset() methods.</p>
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public Message engineGenerateMessage(InputStream in)
        throws MessageException
    {
        throw new RuntimeException("NYI");
    }

}
