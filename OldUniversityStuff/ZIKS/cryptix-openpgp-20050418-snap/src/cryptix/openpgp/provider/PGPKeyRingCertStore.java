/* $Id: PGPKeyRingCertStore.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPKeyRingCertStoreParameters;

import cryptix.pki.ExtendedCertStoreSpi;
import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;
import cryptix.pki.KeyBundleSelector;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.security.InvalidAlgorithmParameterException;

import java.security.cert.Certificate;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertStoreParameters;
import java.security.cert.CRLSelector;

import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;


/**
 * CertStore implementation of a keyring.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyRingCertStore extends ExtendedCertStoreSpi {


// Instance variables
// ..........................................................................

    private File   file;
    private Vector bundles;


// Constructor
// ..........................................................................


    /**
     * Main constructor
     *
     * @param params the initialization parameters (can be null).
     * @throws InvalidAlgorithmParameterException if the given parameters are
     *         invalid.
     */
    public PGPKeyRingCertStore(CertStoreParameters params)
        throws InvalidAlgorithmParameterException
    {
        super(params);

        if (params == null) {
            throw new InvalidAlgorithmParameterException(
                "Null parameters not supported.");
        }
        if (! (params instanceof PGPKeyRingCertStoreParameters)) {
            throw new InvalidAlgorithmParameterException("Instance of "+
                "cryptix.openpgp.PGPKeyRingCertStoreParameters expected.");
        }
        
        file = ((PGPKeyRingCertStoreParameters)params).getFile();

        try {
            FileInputStream in = new FileInputStream(file);
            bundles = (Vector)PGPKeyBundleFactory.helper(in, false);
        } catch (IOException ioe) {
            throw new InvalidAlgorithmParameterException(
                "Error while reading keyring - "+ioe);
        } catch (KeyBundleException kbe) {
            throw new InvalidAlgorithmParameterException(
                "Error while reading keyring - "+kbe);
        }
        
    }
    


// Basic methods
// ..........................................................................


    public Collection engineGetCertificates(CertSelector selector)
        throws CertStoreException
    {
        Vector result = new Vector();
        Iterator it = bundles.iterator();
        while (it.hasNext()) {

            KeyBundle bundle = (KeyBundle)it.next();
            Iterator it2 = bundle.getCertificates();

            while (it2.hasNext()) {
                Certificate cert = (Certificate)it2.next();
                if (selector == null) {
                    result.add(cert);
                } else if (selector.match(cert)) {
                    result.add(cert);
                }
            }
        }
        
        return result;
        
    }
    
    
    public Collection engineGetCRLs(CRLSelector selector)
        throws CertStoreException
    {
        throw new RuntimeException("NYI");
    }
        


// Extended methods
// ..........................................................................


    /**
     * Returns a Collection of KeyBundles matching the given selector.
     *
     * <p>If no matches can be found, an empty collection will be returned.</p>
     *
     * @param selector A selector that specifies which keybundles to return.
     *        Specify null here to return all keybundles (if supported).
     * @throws CertStoreException on a variety of type specific problems, 
     *         including no support for the 'null' selector.
     * @returns a (possibly empty) Collection of KeyBundles that match the 
     *          given selector.
     */
    public Collection engineGetKeyBundles(KeyBundleSelector selector)
        throws CertStoreException
    {
        if (selector == null) {
            return (Vector)bundles.clone();
        }
        
        Vector result = new Vector();
        Iterator it = bundles.iterator();
        while (it.hasNext()) {
            KeyBundle bundle = (KeyBundle)it.next();
            if (selector.match(bundle)) {
                result.add(bundle);
            }
        }
        
        return result;
    }
    

    /**
     * Stores the given keybundle in this store.
     *
     * @param bundle the bundle to store
     * @throws CertStoreException on a variety of type specific problems
     */
    public void engineSetKeyBundleEntry(KeyBundle bundle)
        throws CertStoreException
    {
        bundles.add(bundle);

        try {

            FileOutputStream out = new FileOutputStream(file);

            Iterator it = bundles.iterator();
            while (it.hasNext()) {
                KeyBundle temp = (KeyBundle)it.next();
                out.write(temp.getEncoded());
            }
            
        } catch (IOException ioe) {
            throw new CertStoreException(
                "Error while writing keyring - "+ioe);
        } catch (KeyBundleException kbe) {
            throw new CertStoreException(
                "Error while writing keyring - "+kbe);
        }
    }

}
