/* $Id: PGPKeyIDFactory.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPKey;

import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.packet.PGPPublicKeyPacket;
import cryptix.openpgp.packet.PGPPublicSubKeyPacket;
import cryptix.openpgp.packet.PGPSecretKeyPacket;
import cryptix.openpgp.packet.PGPSecretSubKeyPacket;

import cryptix.pki.KeyID;
import cryptix.pki.KeyIDFactorySpi;

import java.security.InvalidKeyException;
import java.security.Key;


/**
 * Service provider interface for PGPKeyIDFactory
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyIDFactory extends KeyIDFactorySpi {


    /**
     * Generates a KeyID from a key.
     *
     * @throws InvalidKeyException if the given key is not of the right type
     *         to produce a KeyID.
     */
    public KeyID engineGenerateKeyID(Key key) 
        throws InvalidKeyException
    {
        return convert(key);
    }
    
    
    /**
     * Package protected static method to convert a key to a key ID.
     */
    /* package */ static KeyID convert(Key key) 
        throws InvalidKeyException
    {
        if (! (key instanceof PGPKey)) {
            throw new InvalidKeyException("Not an instance of PGPKey.");
        }
        
        PGPKeyPacket packet = ((PGPKey)key).getPacket();
        
        if ((packet instanceof PGPPublicKeyPacket) ||
            (packet instanceof PGPPublicSubKeyPacket))
        {
            return new PGPKeyIDImpl(packet);
        
        } else if (packet instanceof PGPSecretKeyPacket) {
        
            return new PGPKeyIDImpl(packet.clonePublic());
            
        } else if (packet instanceof PGPSecretSubKeyPacket) {
        
            return new PGPKeyIDImpl(packet.clonePublicSub());
            
        } else {
            throw new InvalidKeyException("No valid key packet found.");
        }
    }

}
