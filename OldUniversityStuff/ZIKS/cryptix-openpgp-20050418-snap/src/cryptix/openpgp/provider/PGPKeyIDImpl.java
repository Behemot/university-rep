/* $Id: PGPKeyIDImpl.java,v 1.4 2005/04/05 21:26:06 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.algorithm.PGPRSA;
import cryptix.openpgp.io.PGPHashDataOutputStream;
import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.util.PGPCompare;

import cryptix.pki.KeyID;

import java.io.IOException;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * An OpenPGP KeyID.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public class PGPKeyIDImpl extends KeyID {


// Instance variables
// ..........................................................................

    /** The hash which forms the fingerprint */
    private byte[] hash;
    /** V2/V3 keyID's are a bit different */
    private byte[] v3keyid;    


// Constructor
// ..........................................................................

    /* package */ PGPKeyIDImpl(PGPKeyPacket pkt)
    {
        super("OpenPGP");

        if (pkt.getVersion() == 4) {

            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-1");
            } catch (NoSuchAlgorithmException nsae) {
                throw new InternalError("SHA-1 hash algorithm not found");
            }
    
            PGPHashDataOutputStream hdos = new PGPHashDataOutputStream(md);
            try {
                pkt.encodeBody(hdos);
                hdos.close();
            } catch (IOException ioe) {
                throw new InternalError("IOException on calculating " +
                                        "fingerprint");
            }
        
            hash = md.digest();
            v3keyid = null;

        } else if ((pkt.getVersion() == 2) || (pkt.getVersion() == 3)) {
        
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException nsae) {
                throw new InternalError("MD5 hash algorithm not found");
            }

            md.update(((PGPRSA)(pkt.getAlgorithm())).encodeV3FingerprintData());
            hash = md.digest();
            
            v3keyid = ((PGPRSA)(pkt.getAlgorithm())).encodeV3KeyIDData();
        
        } else {
        
            throw new InternalError("Invalid version.");
            
        }

    }


    /* package */ PGPKeyIDImpl(byte[] hash, byte[] keyid, int version)
    {
        super("OpenPGP");

        if (version == 4) {
        
            if ((keyid != null) && (hash != null))
                throw new IllegalArgumentException(
                    "Either keyid or hash should be null for V4");
            
            if ((keyid == null) && (hash == null))
                throw new IllegalArgumentException(
                    "Either keyid or hash should not be null for V4");
                    
            if (hash != null) {
            
                if (hash.length != 20)
                    throw new IllegalArgumentException(
                        "Hash length should be 160 bits");
                        
                this.hash = hash;
                
            } else {

                if ((keyid.length != 8) && (keyid.length != 4))
                    throw new IllegalArgumentException(
                        "KeyID length should be 32 or 64 bits");
                        
                this.hash = keyid;
            }
            
        } else if (version == 3) {

            if (keyid == null)
                throw new IllegalArgumentException(
                    "keyid should not be null for V3");
                    
            if ((keyid.length != 8) && (keyid.length != 4))
                throw new IllegalArgumentException(
                    "KeyID length should be 32 or 64 bits");

            this.v3keyid = keyid;
            
            if (hash != null) {

                if (hash.length != 16)
                    throw new IllegalArgumentException(
                        "Hash length should be 128 bits");

                this.hash = hash;
            }
        
        } else {
            throw new IllegalArgumentException("version should be 3 or 4");
        }
            
            
    }



// Abstract methods
// ..........................................................................

    /**
     * Return a clone of this object
     */
    public Object clone() {
        if (v3keyid != null) {
            return new PGPKeyIDImpl(hash, v3keyid, 3);
        } else {
            return new PGPKeyIDImpl(hash, null, 4);
        }
    }
    

    /**
     * Returns the length of the contained KeyID.
     *
     * <p>This is the maximum number of bytes the getBytes(int) method can 
     * return.</p>
     */
    public int getLength() {
        if (hash != null)
            return hash.length;
        else
            return v3keyid.length;
    }


    /**
     * Returns the full keyID as a byte array.
     */
    public byte[] getBytes() {
        return getBytes(getLength());
    }


    /**
     * Returns a byte array of len bytes containing a reduces keyID.
     *
     * <p>This method reduces the output of getBytes() in a format specific way
     * to len bytes. </p>
     * <p>E.g. for OpenPGP getBytes(4) and getBytes(8) would return
     * the 32-bit and 64-bit key ID's, being the least significant bits of the
     * full 160-bit fingerprint.</p>
     *
     * @param len the number of bytes to return
     *
     * @throws IllegalArgumentException for unsupported lengths
     */
    public byte[] getBytes(int len) {

        if (len < 0) 
            throw new IllegalArgumentException("len < 0");
        if (len > getLength())
            throw new IllegalArgumentException("len > getLength");
            
        byte[] temp = new byte[len];

        if ((v3keyid != null) && (len <= v3keyid.length)) {
            System.arraycopy(v3keyid,v3keyid.length-len,temp,0,len);
        } else {
            System.arraycopy(hash,hash.length-len,temp,0,len);
        }
        
        return temp;
        
    }
    

    /**
     * Do an equality comparison.
     *
     * <p>Note that this is an exact equality match. See the match(...) methods
     * for if you want to match partial keyID's.</p>
     */
    public boolean equals(Object other) {

        if ( !(other instanceof PGPKeyIDImpl) ) return false;

        PGPKeyIDImpl that = (PGPKeyIDImpl)other;
        
        return PGPCompare.equals(this.hash,    that.hash) &&
               PGPCompare.equals(this.v3keyid, that.v3keyid);
    }
    

    /**
     * Matches this keyID to another.
     *
     * <p>This method differs from equals in that it can do a reduced match:
     * if for example only 32 bits are available in one keyID, while 160 bits 
     * are available in the other, then this method can still return true if
     * the 32 bits match.</p>
     *
     * @throws IllegalArgumentException if other is of an incorrect type
     */
    public boolean match(KeyID other) {
        
        if (! (other instanceof PGPKeyIDImpl))
            throw new IllegalArgumentException("invalid type");
        
        PGPKeyIDImpl that = (PGPKeyIDImpl)other;
        
        if (((this.v3keyid == null) && (that.v3keyid != null)) ||
            ((this.v3keyid != null) && (that.v3keyid == null)))
                return false;
        
        if ((this.hash != null) && (that.hash != null)) {
        
            int len = that.hash.length;
            if (len > this.hash.length) len = this.hash.length;
        
            for (int i=0; i<len; i++) {
                if (this.hash[this.hash.length-i-1] != 
                    that.hash[that.hash.length-i-1]) 
                {
                    return false;
                }
            }
            
        } else {
        
            int len = that.v3keyid.length;
            if (len > this.v3keyid.length) len = this.v3keyid.length;
        
            for (int i=0; i<len; i++) {
                if (this.v3keyid[this.v3keyid.length-i-1] != 
                    that.v3keyid[that.v3keyid.length-i-1]) 
                {
                    return false;
                }
            }
        
        }
        
        return true;
        
    }
    
    
    /**
     * Matches this keyID to a key.
     *
     * <p>This method differs from equals in that it can do a reduced match:
     * if for example only 32 bits are available in this keyID, while the full
     * keyID of a key is 160 bits, then this method can still return true if
     * the 32 bits match.</p>
     *
     * @throws IllegalArgumentException if other is of an incorrect type
     */
    public boolean match(Key other) {
    
        try {
            
            KeyID otherkeyid = PGPKeyIDFactory.convert(other);
                
            return match(otherkeyid);
            
        } catch (InvalidKeyException ike) {
        
            throw new IllegalArgumentException("Invalid key");
            
        }
            
    }
    

}
