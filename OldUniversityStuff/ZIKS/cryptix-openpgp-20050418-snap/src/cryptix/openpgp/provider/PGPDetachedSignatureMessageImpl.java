/* $Id: PGPDetachedSignatureMessageImpl.java,v 1.3 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.openpgp.PGPDetachedSignatureMessage;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.packet.PGPSignaturePacket;

import cryptix.pki.KeyBundle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.PublicKey;


/**
 * Represents a detached signature.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class PGPDetachedSignatureMessageImpl 
    extends PGPDetachedSignatureMessage 
{


// Instance variables
// ..........................................................................

    private final PGPSignaturePacket pkt;
    

// Constructor
// ..........................................................................

    /**
     * Creates a PGPDetachedSignatureMessage object.
     */
    protected PGPDetachedSignatureMessageImpl(PGPSignaturePacket pkt) 
    {
        super("OpenPGP");
        this.pkt = pkt;
    }


// Own methods
// ..........................................................................

    /**
     * Return the contained packet.
     */
    public PGPSignaturePacket getPacket()
    {
        return pkt;
    }
    

// Verify methods
// ..........................................................................

    /**
     * Verify if this detached signature is a correct signature on a particular
     * message by a particular public key.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @returns true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public boolean verify(Message msg, PublicKey pubkey) 
        throws MessageException
    {
        if (! (msg instanceof PGPLiteralMessageImpl)) {
            throw new IllegalArgumentException("Not a PGP LiteralMessage.");
        }
        PGPSignedMessageImpl sm = new PGPSignedMessageImpl(false, pkt,
            (PGPLiteralMessageImpl)msg);
        return sm.verify(pubkey);
    }
        

    /**
     * Verify if this detached signature is a correct signature on a particular
     * message by a particular keybundle.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @returns true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public boolean verify(Message msg, KeyBundle pubkey) 
        throws MessageException
    {
        return verify(msg, (PublicKey)pubkey.getPublicKeys().next());
    }


// Methods from cryptix.message.Message
// ..........................................................................

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public byte[] getEncoded() throws MessageException {

        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            pkt.encode(baos);
            baos.close();
            return baos.toByteArray();
        
        } catch (IOException ioe) {
            throw new RuntimeException(
                "IOException in ByteArrayOutputStream - "+ioe);
        }
    }
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @returns the attribute or null if the attribute isn't set.
     */
    public Object getAttribute(String name)
        throws IllegalArgumentException, MessageException
    {
    
        if (! name.equals("Hash")) {
            throw new IllegalArgumentException("Not supported.");
        }

        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        return factory.getHashTextName(pkt.getHashID());
    }
}
