/* $Id: PGPSignedMessageBuilder.java,v 1.4 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.LiteralMessage;
import cryptix.message.Message;
import cryptix.message.MessageException;
import cryptix.message.SignedMessageBuilderSpi;

import cryptix.openpgp.PGPPrivateKey;
import cryptix.openpgp.PGPPublicKey;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyIDSP;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.util.Date;
import java.util.Vector;


/**
 * Service provider interface for SignedMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public abstract class PGPSignedMessageBuilder extends SignedMessageBuilderSpi {

    private PGPLiteralMessageImpl contents;
    private SecureRandom sr;
    private PGPPrivateKey signingkey;
    private boolean v4sig;
    private boolean legacy;
    
    /**
     * Constructor for subclasses
     */
    protected PGPSignedMessageBuilder(boolean v4sig, boolean legacy)
    {
        this.v4sig = v4sig;
        this.legacy = legacy;
    }
    

    /**
     * Initializes this builder with the given message.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineInit(Message contents, SecureRandom sr)
        throws IllegalStateException, MessageException
    {
        if (this.contents != null) {
            throw new IllegalStateException("Already initialized");
        }
        if (! (contents instanceof PGPLiteralMessageImpl)) {
            throw new IllegalArgumentException(
                "We can only sign LiteralMessage objects.");
        }

        this.contents = (PGPLiteralMessageImpl)contents;

        if (sr != null) {
            this.sr = sr;
        } else {
            this.sr = new SecureRandom();
        }
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException,
               MessageException
    {
        throw new IllegalArgumentException("No attributes supported.");
    }


    /**
     * Adds a signer
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineAddSigner(PrivateKey signingkey)
        throws IllegalStateException, MessageException
    {
        if (sr == null) throw new IllegalStateException("Not yet initialized.");
        if (signingkey != null) {
            throw new RuntimeException("Nesting signatures not supported yet.");
        }
        this.signingkey = (PGPPrivateKey)signingkey;
    }


    /**
     * Adds a signer from a keybundle, decrypting it with the given passphrase.
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws MessageException on a variety of format specific problems.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     */
    public void engineAddSigner(KeyBundle bundle, char[] passphrase)
        throws IllegalStateException, MessageException, 
               UnrecoverableKeyException
    {
        if (sr == null) throw new IllegalStateException("Not yet initialized.");
        if (signingkey != null) {
            throw new RuntimeException("Nesting signatures not supported yet.");
        }
        PGPPublicKey pubkey = (PGPPublicKey)bundle.getPublicKeys().next();
        this.signingkey = 
            (PGPPrivateKey)bundle.getPrivateKey(pubkey, passphrase);
    }


    /**
     * Returns the signed message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, if no calls have been made to the addSigner method
     *         or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public Message engineBuild()
        throws IllegalStateException, MessageException
    {
        // init
        PGPSigner signer = (PGPSigner)signingkey.getPacket().getAlgorithm();
        byte algoid = signingkey.getPacket().getAlgorithmID();
        
        byte hashid;
        if (legacy)
            hashid = 1;    // MD5
        else
            hashid = 2;    // SHA-1   ### FIXME: find preferences

        PGPSignaturePacket pkt = new PGPSignaturePacket();
        pkt.setPacketID(PGPPacketConstants.PKT_SIGNATURE);

        if (v4sig) { // version 4 signature
          
            // create subpackets
            PGPDateSP creation = new PGPDateSP();
            creation.setValue(new Date());
            creation.setPacketID(PGPSignatureConstants.SSP_SIG_CREATION_TIME);
            
            Vector hashed = new Vector();
            hashed.addElement(creation);
            
            PGPKeyIDSP keyid = new PGPKeyIDSP();
            try {
                keyid.setValue(PGPKeyIDFactory.convert(signingkey).getBytes(8));
            } catch (InvalidKeyException ike) {
                throw new MessageException(""+ike);
            }
            keyid.setPacketID(PGPSignatureConstants.SSP_ISSUER_KEYID);
            
            Vector unhashed = new Vector();
            unhashed.addElement(keyid);
            
            
            if (contents.getDataType() == LiteralMessage.TEXT) {
                pkt.setData(PGPSignatureConstants.TYPE_TEXT_DOCUMENT, algoid, 
                    hashid, hashed, unhashed);
            } else {
                pkt.setData(PGPSignatureConstants.TYPE_BINARY_DOCUMENT, algoid, 
                    hashid, hashed, unhashed);
            }
            
        } else { // version 3 signature
          
            // build timestamp
            byte[] time = new byte[4];
            int unixTime = (int)(System.currentTimeMillis()/1000);
            time[0] = (byte) ((unixTime >>>24) & 0xFF);
            time[1] = (byte) ((unixTime >>>16) & 0xFF);
            time[2] = (byte) ((unixTime >>> 8) & 0xFF);
            time[3] = (byte) ( unixTime        & 0xFF);
            
            byte[] keyid;
            try {
                keyid = PGPKeyIDFactory.convert(signingkey).getBytes(8);
            } catch (InvalidKeyException ike) {
                throw new MessageException(""+ike);
            }
            
            
            if (contents.getDataType() == LiteralMessage.TEXT) {
                pkt.setData(PGPSignatureConstants.TYPE_TEXT_DOCUMENT, time, 
                    keyid, algoid, hashid);
            } else {
                pkt.setData(PGPSignatureConstants.TYPE_BINARY_DOCUMENT, time, 
                    keyid, algoid, hashid);
            }
        }
        
        // init hashes and signature
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        MessageDigest md;
        try {
            md = factory.getHashAlgorithm(hashid);
        } catch (NoSuchAlgorithmException nsae) {
            throw new MessageException(""+nsae);
        }
        signer.initSign(hashid, factory);

        // hash the data
        md.update(contents.getBinaryData());
        signer.update(contents.getBinaryData());
        
        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);
        

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }

        // store hash
        byte[] digestcalc  = md.digest();
        pkt.setHash(digestcalc);
        
        // sign
        signer.computeSignature();
        pkt.setSignature(signer);
        
        // build
        return new PGPSignedMessageImpl(legacy, pkt, contents);        
    }


// Inner classes for instantiation
//..............................................................................

    public static class V4     extends PGPSignedMessageBuilder {
        public V4()     { super(true,  false); }
    }

    public static class V3     extends PGPSignedMessageBuilder {
        public V3()     { super(false, false); }
    }

    public static class Legacy extends PGPSignedMessageBuilder {
        public Legacy() { super(false, true ); }
    }

}
