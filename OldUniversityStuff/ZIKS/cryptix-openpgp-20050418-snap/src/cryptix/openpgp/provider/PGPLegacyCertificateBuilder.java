/* $Id: PGPLegacyCertificateBuilder.java,v 1.1 2005/03/13 17:13:02 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPLegacyCertificateParameterBuilder;

import java.security.InvalidKeyException;
import java.security.PrivateKey;

import java.security.spec.AlgorithmParameterSpec;


/**
 * Service provider interface for CertificateBuilder
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class PGPLegacyCertificateBuilder extends PGPCertificateBuilder {

    /** ### Blah */
    protected AlgorithmParameterSpec 
        getDefaultParameterSpec(PrivateKey issuer)
        throws InvalidKeyException
    {
        return (new PGPLegacyCertificateParameterBuilder(issuer)).build();
    }

}
