/* $Id: PGPLiteralMessageBuilder.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.LiteralMessageBuilderSpi;
import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPLiteralDataPacket;

import cryptix.openpgp.util.PGPArmoury;

import java.security.SecureRandom;


/**
 * Service provider interface for LiteralMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPLiteralMessageBuilder extends LiteralMessageBuilderSpi {

    private PGPLiteralDataPacket pkt;

    /**
     * Initializes this builder with the given binary data and SecureRandom
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineInit(byte[] contents, SecureRandom sr)
        throws IllegalStateException, MessageException
    {
        if (pkt != null) throw new IllegalStateException("Already initialized");
        pkt = new PGPLiteralDataPacket();
        pkt.setData(contents);
        pkt.setPacketID(PGPPacketConstants.PKT_LITERAL_DATA);
    }
    

    /**
     * Initializes this builder with the given text data and SecureRandom
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineInit(String contents, SecureRandom sr)
        throws IllegalStateException, MessageException
    {
        if (pkt != null) throw new IllegalStateException("Already initialized");
        pkt = new PGPLiteralDataPacket();
        pkt.setData(PGPArmoury.canonicalize(contents));
        pkt.setPacketID(PGPPacketConstants.PKT_LITERAL_DATA);
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException,
               MessageException
    {
        if (pkt == null) throw new IllegalStateException("Not yet initialized");
        if (name.equals("filename")) {
            if (attr instanceof String) {
                pkt.setFileName((String)attr);
            } else {
                throw new IllegalArgumentException(
                    "String expected for filename");
            }
        } else {
            throw new IllegalArgumentException(
                "Invalid attribute: ["+name+"]");
        }
    }


    /**
     * Returns the built literal message
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         properly, or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public Message engineBuild()
        throws IllegalStateException, MessageException
    {
        if (pkt == null) throw new IllegalStateException("Not yet initialized");
        return new PGPLiteralMessageImpl(pkt);
    }
        
}
