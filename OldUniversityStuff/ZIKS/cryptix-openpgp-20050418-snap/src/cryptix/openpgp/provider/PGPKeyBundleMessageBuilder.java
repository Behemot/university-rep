/* $Id: PGPKeyBundleMessageBuilder.java,v 1.2 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.message.KeyBundleMessageBuilderSpi;
import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.pki.KeyBundle;


/**
 * Service provider interface for KeyBundleMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyBundleMessageBuilder extends KeyBundleMessageBuilderSpi {


    private KeyBundle bundle;
    

    /**
     * Initializes this builder with the given keybundle.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public void engineInit(KeyBundle bundle) 
        throws IllegalStateException, MessageException
    {
        this.bundle = bundle;
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageException
    {
        throw new IllegalArgumentException("Attributes not supported.");
    }


    /**
     * Returns a message containing a keybundle.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public Message engineBuild() 
        throws IllegalStateException, MessageException
    {
        if (bundle == null) {
            throw new IllegalStateException("Not yet initialized.");
        }
        
        return new PGPKeyBundleMessageImpl(bundle);
    }
        
}
