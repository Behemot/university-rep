/* $Id: PGPKeyBundleImpl.java,v 1.4 2005/03/13 17:46:38 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.provider;


import cryptix.openpgp.PGPCertificate;
import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPKey;
import cryptix.openpgp.PGPKeyBundle;
import cryptix.openpgp.PGPPrincipal;
import cryptix.openpgp.PGPPrivateKey;
import cryptix.openpgp.PGPPublicKey;
import cryptix.openpgp.PGPWrongPassphraseException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.io.PGPHashDataOutputStream;

import cryptix.openpgp.packet.PGPKeyPacket;
import cryptix.openpgp.packet.PGPPacketConstants;
import cryptix.openpgp.packet.PGPSignatureConstants;
import cryptix.openpgp.packet.PGPSignaturePacket;
import cryptix.openpgp.packet.PGPSecretKeyPacket;
import cryptix.openpgp.packet.PGPSecretSubKeyPacket;

import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyIDSP;
import cryptix.openpgp.signature.PGPSignatureSubPacket;

import cryptix.pki.KeyBundle;
import cryptix.pki.KeyBundleException;
import cryptix.pki.KeyID;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.Principal;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Vector;


/**
 * An OpenPGP KeyBundle.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @author  Ingo Luetkebohle
 * @version $Revision: 1.4 $
 */
public class PGPKeyBundleImpl extends PGPKeyBundle {

// Instance vars
// ..........................................................................

    /* package */ PGPPublicKey       mainkey                  = null;
    /* package */ PGPSecretKeyPacket privpkt                  = null;
    /* package */ Vector             directKeySigs            = new Vector();

    /* package */ Vector             principals               = new Vector();
    /* package */ Vector             certificates             = new Vector();
    /* package */ HashMap            principalsToCertificates = new HashMap();

    /* package */ Vector             subkeys                  = new Vector();
    /* package */ HashMap            privSubkeys              = new HashMap();
    /* package */ HashMap            subkeyBindingSigs        = new HashMap();
    
    public boolean containsPrivateKey() { return (privpkt != null); }
    

// Constructors
// ..........................................................................

    /**
     * Create a new KeyBundle object with the given type.
     */
    /* package */ PGPKeyBundleImpl()
    {
        super("OpenPGP");
    }
    
    
    /**
     * Clone helper constructor.
     */
    private PGPKeyBundleImpl(PGPPublicKey mainkey, PGPSecretKeyPacket privpkt,
        Vector directKeySigs, Vector principals, Vector certificates, 
        HashMap principalsToCertificates, Vector subkeys, HashMap privSubkeys, 
        HashMap subkeyBindingSigs) 
    {
        super("OpenPGP");
        this.mainkey                  = mainkey;
        this.privpkt                  = privpkt;
        this.directKeySigs            = directKeySigs;
        this.principals               = principals;
        this.certificates             = certificates;
        this.principalsToCertificates = principalsToCertificates;
        this.subkeys                  = subkeys;
        this.privSubkeys              = privSubkeys;
        this.subkeyBindingSigs        = subkeyBindingSigs;
    }
    



// Methods from KeyBundle
// ..........................................................................


    /**
     * Returns the keybundle in encoded format.
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public byte[] getEncoded() 
        throws KeyBundleException
    {
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Iterator it, it2;
    
            if (privpkt != null) {
                if (privSubkeys.size() != subkeys.size()) {
                    throw new KeyBundleException(
                        "Not all private subkeys present.");
                }
            } else {
                if (privSubkeys.size() != 0) {
                    throw new KeyBundleException(
                        "There are private subkeys present, but no private "+
                        "main key.");
                }
            }
    
            if (privpkt != null) {
                privpkt.encode(baos);
            } else {
                baos.write(mainkey.getEncoded());
            }
            
            it = directKeySigs.iterator();
            while (it.hasNext()) {
                ((PGPSignaturePacket)it.next()).encode(baos);
            }
            
            it = principals.iterator();
            while (it.hasNext()) {
                PGPPrincipal subject = (PGPPrincipal)it.next();
                baos.write(subject.getEncoded());
                it2 = ((Vector)principalsToCertificates.get(subject)).iterator();
                while (it2.hasNext()) {
                    ((PGPCertificate)it2.next()).getPacket().encode(baos);
                }
            }
            
            it = subkeys.iterator();
            while (it.hasNext()) {
                PGPKey subkey = (PGPKey)it.next();
                if (privpkt == null) {
                    baos.write(subkey.getEncoded());
                } else {
                    ((PGPKeyPacket)privSubkeys.get(subkey)).encode(baos);
                }
                ((PGPSignaturePacket)
                    subkeyBindingSigs.get(subkey)).encode(baos);
            }
            
            baos.close();
            return baos.toByteArray();

        } catch (IOException ioe) {
            throw new KeyBundleException(""+ioe);
        }
    }

    
    /**
     * Return a clone for this KeyBundle
     */
    public Object clone()
    {
        HashMap p2cClone = new HashMap();
        Iterator keys = principalsToCertificates.keySet().iterator();
        Iterator entries = principalsToCertificates.entrySet().iterator();

        while (keys.hasNext()) {
            p2cClone.put(keys.next(), 
                ((Vector)((Map.Entry)entries.next()).getValue()).clone());
        }

        return new PGPKeyBundleImpl(mainkey, privpkt, 
            (Vector)directKeySigs.clone(), (Vector)principals.clone(), 
            (Vector)certificates.clone(), p2cClone, (Vector)subkeys.clone(), 
            (HashMap)privSubkeys.clone(), (HashMap)subkeyBindingSigs.clone());
    }
    

    /**
     * Adds a certificate
     *
     * <p>It is assumed that the certificate contains the right pointers to
     * the public key and the principal, so that the implementation of the
     * keybundle can add these automatically when needed.</p>
     */
    public boolean addCertificate    (Certificate cert)
        throws KeyBundleException
    {
        if (!(cert instanceof PGPCertificate)) {
            throw new KeyBundleException("Invalid certificate type");
        }
        
        PGPCertificate pgpcert = (PGPCertificate)cert;
        PGPPublicKey pgppubkey = (PGPPublicKey)pgpcert.getPublicKey();
        PGPPrincipal pgpprinc  = (PGPPrincipal)pgpcert.getSubject();
        
        boolean result = false;
        

        if (mainkey == null) {
            mainkey = pgppubkey;
            result = true;
        } else if (! mainkey.equals(pgppubkey)) {
            throw new KeyBundleException("KeyBundle can only contain "+
                "certificates for one key. This certificate belonged to "+
                "another key.");
        }

        Vector mapping;
        if (! principals.contains(pgpprinc)) {
            principals.add(pgpprinc);
            mapping = new Vector();
            principalsToCertificates.put(pgpprinc, mapping);
            result = true;
        } else {
            mapping = (Vector)principalsToCertificates.get(pgpprinc);
        }
        
        if (! mapping.contains(pgpcert)) {
            mapping.add(cert);
            certificates.add(cert);
            result = true;
        }
        
        return result;
    }
    


    /**
     * Adds a principal.
     *
     * <p>Note: there is no need to call this method explicitly when 
     * addCertificate is used.</p>
     */
    public boolean addPrincipal  (Principal princ)
        throws KeyBundleException
    {
        if (!(princ instanceof PGPPrincipal)) {
            throw new KeyBundleException("Invalid principal type");
        }

        if (! principals.contains(princ)) {
            principals.add(princ);
            return true;
        }
        
        return false;
    }


    /**
     * Adds a public key.
     *
     * <p>Note: there is no need to call this method explicitly when 
     * addCertificate is used.</p>
     */
    public boolean addPublicKey  (PublicKey pubkey)
        throws KeyBundleException
    {
        if (!(pubkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid public key type");
        }
        
        if (mainkey == null) {
            mainkey = (PGPPublicKey)pubkey;
            return true;
        } else {
            throw new KeyBundleException(
                "KeyBundle can only contain one main key.");
        }
    }
        

    /**
     * Adds a private key, encrypting it with a passphrase.
     */
    public boolean addPrivateKey     (PrivateKey privkey, PublicKey pubkey,
                                      char[] passphrase, SecureRandom sr)
        throws KeyBundleException
    {
        addPrivateKey(privkey, pubkey);
        
        int s2kid, cipherid, hashid;
        if (((PGPKey)privkey).getPacket().getVersion() == 4) {
            s2kid    = 3;  // Iterated & Salted   ### FIXME
            cipherid = 3;  // CAST5               ### FIXME
            hashid   = 2;  // SHA-1               ### FIXME
        } else { // privkey.version == 2/3
            s2kid    = 0;  // Simple
            cipherid = 1;  // IDEA
            hashid   = 1;  // MD5
        }
        
        privpkt.encrypt(passphrase, s2kid, cipherid, hashid, sr,
                        PGPAlgorithmFactory.getDefaultInstance());
        privpkt.forgetSecretData();
        
        return true;
    }
    

    /**
     * Adds a private key, without encrypting it.
     */
    public boolean addPrivateKey (PrivateKey privkey, PublicKey pubkey)
        throws KeyBundleException
    {
        if (! (privkey instanceof PGPPrivateKey)) {
            throw new KeyBundleException("Invalid private key type");
        }
        if (! (pubkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid private key type");
        }
        
        if (! (pubkey.equals(this.mainkey))) {
            throw new KeyBundleException("Public key not found");
        }

        if (privpkt != null) {
            return false;
        }
        
        KeyID one, two;
        try {
            one = PGPKeyIDFactory.convert(pubkey);
            two = PGPKeyIDFactory.convert(privkey);
        } catch (InvalidKeyException ike) {
            throw new RuntimeException("Bug found! "+ike);
        }
        
        if (! (one.match(two))) {
            throw new KeyBundleException("Public and private key do not "+
                                         "belong together");
        }
        
        PGPPrivateKey pgpprivkey = (PGPPrivateKey)privkey;
        if (! (pgpprivkey.getPacket() instanceof PGPSecretKeyPacket)) {
            throw new KeyBundleException("Main key expected. Got subkey.");
        }
        
        PGPSecretKeyPacket pkt = (PGPSecretKeyPacket)pgpprivkey.getPacket();
        privpkt = (PGPSecretKeyPacket)pkt.clone();
        
        return true;
    }
    

    /**
     * Remove a key and all related principals and certificates.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public boolean removePublicKey   (PublicKey key)
        throws KeyBundleException
    {
        if (! key.equals(mainkey)) {
            return true;
        }
        
        mainkey                  = null;
        privpkt                  = null;
        directKeySigs            = new Vector();
        
        principals               = new Vector();
        certificates             = new Vector();        
        principalsToCertificates = new HashMap();
        
        subkeys                  = new Vector();
        privSubkeys              = new HashMap();
        subkeyBindingSigs        = new HashMap();
        
        return true;
    }

    /**
     * Remove the private key belonging to the given public key.
     */
    public boolean removePrivateKey  (PublicKey key)
        throws KeyBundleException
    {
        if ((! (privpkt == null)) || (! key.equals(mainkey))) {
            return false;
        }
        
        privpkt = null;
        
        return true;
    }

    /**
     * Remove a principal and all related keys and certificates.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public boolean removePrincipal   (Principal subject)
        throws KeyBundleException
    {
        if (! principals.contains(subject)) {
            return false;
        }
        
        Vector mapping = (Vector)principalsToCertificates.get(subject);
        certificates.removeAll(mapping);        
        principals.remove(subject);
        principalsToCertificates.remove(subject);
        
        return true;
    }

    /**
     * Remove a certificate and all related keys and principals.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public boolean removeCertificate (Certificate cert)
        throws KeyBundleException
    {
        if (! certificates.contains(cert)) {
            return false;
        }
        
        Principal subject = ((PGPCertificate)cert).getSubject();
        Vector mapping = (Vector)principalsToCertificates.get(subject);
        mapping.remove(cert);
        certificates.remove(cert);
        
        return true;
    }
    

    /**
     * Return an iterator over all keys contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.Key</p>
     */
    public Iterator getPublicKeys()
    {
        return new SingleObjectIterator(mainkey);
    }


    /**
     * Return an the private key belonging to the given public key, decryptin
     * it with the given passphrase.
     *
     * <p>Returns null if no private key is available</p>
     */
    public PrivateKey getPrivateKey(PublicKey key, char[] passphrase)
        throws UnrecoverableKeyException
    {
        if (! (mainkey.equals(key))) {
            return null;
        }
        
        if (privpkt == null) {
            return null;
        }
        
        PGPSecretKeyPacket clone = (PGPSecretKeyPacket)privpkt.clone();
        try {
            clone.decrypt(passphrase, PGPAlgorithmFactory.getDefaultInstance());
        } catch (PGPWrongPassphraseException e) {
            throw new UnrecoverableKeyException("Invalid passphrase");
        } catch (PGPDataFormatException e) {
            throw new UnrecoverableKeyException(
                "Parsing error, invalid decrypted data - "+e);
        }
        
        return new PGPPrivateKey(clone, key.getAlgorithm());
    }


    /**
     * Return an iterator over all principals contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.Principal</p>
     */
    public Iterator getPrincipals()
    {
        return new RemoveNotSupportedIterator(principals.iterator());
    }


    /**
     * Return an iterator over all certificate contained within this bundle.
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.cert.Certificate</p>
     */
    public Iterator getCertificates()
    {
        return new RemoveNotSupportedIterator(certificates.iterator());
    }
    

    /**
     * Return an iterator over the certificates contained within this bundle
     * that belong to a certain key and principal.
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.cert.Certificate</p>
     */
    public Iterator getCertificates(PublicKey key, Principal subject)
    {
        if (! key.equals(mainkey)) {
            return new EmptyIterator();
        }
        
        Vector mapping = (Vector)principalsToCertificates.get(subject);

        if (mapping == null) {
            return new EmptyIterator();
        }
        
        return new RemoveNotSupportedIterator(mapping.iterator());
    }
    


// Methods from PGPKeyBundle
// ..........................................................................


    /**
     * Adds a subkey to a keybundle (calculates binding signature).
     */
    public boolean addPublicSubKey (PublicKey pubsubkey, 
                                    PrivateKey signkey)
        throws KeyBundleException
    {
        // checks
        if (! (pubsubkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid public key type");
        }
        
        if (! (signkey instanceof PGPPrivateKey)) {
            throw new KeyBundleException("Invalid private key type");
        }
        
        if (subkeys.contains(pubsubkey)) {
            return false;
        }

        KeyID one, two;
        try {
            one = PGPKeyIDFactory.convert(mainkey);
            two = PGPKeyIDFactory.convert(signkey);
        } catch (InvalidKeyException ike) {
            throw new KeyBundleException("Invalid keys - "+ike);
        }
        
        if (! (one.match(two))) {
            throw new KeyBundleException("Given signing key does not "+
                "correspond to the main public key for this keybundle.");
        }
        
        // init
        PGPAlgorithmFactory factory = PGPAlgorithmFactory.getDefaultInstance();
        PGPKeyPacket keypacket = ((PGPPrivateKey)signkey).getPacket();
        PGPSigner signer = (PGPSigner)keypacket.getAlgorithm();
        byte algoid = keypacket.getAlgorithmID();
        int hashid = 2;    // SHA-1   ### FIXME: find preferences
        

        // create subpackets
        PGPDateSP creation = new PGPDateSP();
        creation.setValue(new Date());
        creation.setPacketID(PGPSignatureConstants.SSP_SIG_CREATION_TIME);
        
        Vector hashed = new Vector();
        hashed.addElement(creation);
        
        PGPKeyIDSP keyid = new PGPKeyIDSP();
        try {
            keyid.setValue(PGPKeyIDFactory.convert(signkey).getBytes(8));
        } catch (InvalidKeyException ike) {
            throw new KeyBundleException(""+ike);
        }
        keyid.setPacketID(PGPSignatureConstants.SSP_ISSUER_KEYID);

        Vector unhashed = new Vector();
        unhashed.addElement(keyid);
        

        // create signature packet
        PGPSignaturePacket pkt = new PGPSignaturePacket();
        pkt.setData(PGPSignatureConstants.TYPE_SUBKEY_BINDING, (byte)algoid,
                   (byte)hashid, hashed, unhashed);
        pkt.setPacketID(PGPPacketConstants.PKT_SIGNATURE);


        // init hashes and signature
        MessageDigest md;
        try {
            md = factory.getHashAlgorithm(hashid);
        } catch (NoSuchAlgorithmException nsae) {
            throw new KeyBundleException(""+nsae);
        }

        signer.initSign(hashid, factory);
        

        // hash the keys
        try {

            PGPHashDataOutputStream hdos;

            hdos = new PGPHashDataOutputStream(md, signer);
            mainkey.getPacket().encodeBody(hdos);
            hdos.close();

            hdos = new PGPHashDataOutputStream(md, signer);
            ((PGPPublicKey)pubsubkey).getPacket().encodeBody(hdos);
            hdos.close();
            
        } catch (IOException ioe) {
            throw new InternalError("IOException on hashing key - "+ioe);
        }


        // hash the relevant portions of the signature packet
        int bytesWritten = pkt.hashData(md, signer);
        

        // hash the trailer
        if (pkt.getVersion() == 4) {
            byte[] trailer = new byte[6];
            trailer[0] = pkt.getVersion();
            trailer[1] = (byte)0xFF;
            trailer[2] = (byte)((bytesWritten >> 24) & 0xFF);
            trailer[3] = (byte)((bytesWritten >> 16) & 0xFF);
            trailer[4] = (byte)((bytesWritten >>  8) & 0xFF);
            trailer[5] = (byte)((bytesWritten      ) & 0xFF);
            md.update(trailer);
            signer.update(trailer);
        }


        // store hash
        byte[] digestcalc  = md.digest();
        pkt.setHash(digestcalc);
        
        // sign
        signer.computeSignature();
        pkt.setSignature(signer);
        
        // store
        subkeyBindingSigs.put(pubsubkey, pkt);
        subkeys.add(pubsubkey);
        
        return true;
    }
    

    /**
     * Adds a subkey to a keybundle (borrows binding signature from another
     * keybundle).
     */
    public boolean addPublicSubKey (PublicKey pubsubkey, 
                                    KeyBundle other)
        throws KeyBundleException
    {
        if (this.subkeys.contains(pubsubkey)) {
            return false;
        }
    
        if (! (other instanceof PGPKeyBundleImpl)) {
            throw new KeyBundleException("Wrong keybundle type");
        }
        
        PGPKeyBundleImpl that = (PGPKeyBundleImpl)other;
        
        if (! that.subkeyBindingSigs.containsKey(pubsubkey)) {
            throw new KeyBundleException("Public key not found");
        }
        
        this.subkeys.add(pubsubkey);
        this.subkeyBindingSigs.put(pubsubkey, 
            that.subkeyBindingSigs.get(pubsubkey));
        
        return true;
    }
    

    /**
     * Adds a private subkey to the keybundle, encrypting it with the given
     * passphrase.
     */
    public boolean addPrivateSubKey (PrivateKey privsubkey, 
                                     PublicKey  pubsubkey,
                                     char[] passphrase, SecureRandom sr)
        throws KeyBundleException 
    {
        PGPSecretSubKeyPacket pkt = 
            addPrivateSubKeyHelper(privsubkey, pubsubkey);

        if (privSubkeys.containsKey(pubsubkey)) {
            return false;
        }
        
        PGPSecretSubKeyPacket clone = (PGPSecretSubKeyPacket)pkt.clone();
        
        int s2kid    = 3;  // Iterated & Salted   ### FIXME
        int cipherid = 3;  // CAST5               ### FIXME
        int hashid   = 2;  // SHA-1               ### FIXME
        clone.encrypt(passphrase, s2kid, cipherid, hashid, sr,
                        PGPAlgorithmFactory.getDefaultInstance());
        clone.forgetSecretData();
        
        privSubkeys.put(pubsubkey, clone);
        
        return true;
    }


    /**
     * Adds a private subkey to the keybundle, without encrypting it.
     */
    public boolean addPrivateSubKey (PrivateKey privsubkey, 
                                     PublicKey  pubsubkey)
        throws KeyBundleException
    {
        PGPSecretSubKeyPacket pkt = 
            addPrivateSubKeyHelper(privsubkey, pubsubkey);

        if (privSubkeys.containsKey(pubsubkey)) {
            return false;
        }
        
        PGPSecretSubKeyPacket clone = (PGPSecretSubKeyPacket)pkt.clone();
        privSubkeys.put(pubsubkey, clone);
        
        return true;
    }
    
    
    private PGPSecretSubKeyPacket addPrivateSubKeyHelper(PrivateKey privsubkey, 
                                                         PublicKey  pubsubkey) 
        throws KeyBundleException
    {
        if (! (privsubkey instanceof PGPPrivateKey)) {
            throw new KeyBundleException("Invalid private key type");
        }
        if (! (pubsubkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid public key type");
        }
        
        if (! (subkeys.contains(pubsubkey))) {
            throw new KeyBundleException("Public key not found");
        }

        PGPKeyPacket one = ((PGPPrivateKey)privsubkey).getPacket().clonePublic();
        PGPKeyPacket two = ((PGPPublicKey)pubsubkey).getPacket();
        
        if (! (one.equals(two))) {
            throw new KeyBundleException("Public and private key do not "+
                                         "belong together");
        }
        
        PGPPrivateKey pgpprivkey = (PGPPrivateKey)privsubkey;
        if (! (pgpprivkey.getPacket() instanceof PGPSecretSubKeyPacket)) {
            throw new KeyBundleException("Subkey expected. Got mainkey.");
        }
        
        PGPSecretSubKeyPacket pkt = 
            (PGPSecretSubKeyPacket)pgpprivkey.getPacket();
        
        return pkt;
    }


    /**
     * Removes a subkey.
     */
    public boolean removePublicSubKey (PublicKey subkey)
        throws KeyBundleException
    {
        if (! (subkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid public key type");
        }
        
        if (! subkeys.contains(subkey)) {
            return false;
        }
        
        subkeys.remove(subkey);
        subkeyBindingSigs.remove(subkey);
        
        return true;
    }


    /**
     * Removes the private subkey corresponding to the given public subkey.
     */
    public boolean removePrivateSubKey (PublicKey pubsubkey)
        throws KeyBundleException
    {
        if (! (pubsubkey instanceof PGPPublicKey)) {
            throw new KeyBundleException("Invalid public key type");
        }
        
        if (! privSubkeys.containsKey(pubsubkey)) {
            return false;
        }
        
        privSubkeys.remove(pubsubkey);
        
        return true;
    }


    /**
     * Returns an iterator over all subkeys contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * cryptix.openpgp.PGPKey</p>
     */
    public Iterator getPublicSubKeys()
    {
        return new RemoveNotSupportedIterator(subkeys.iterator());
    }


    /**
     * Return the private key corresponding to the given public key, decrypting
     * it with the given passphrase.
     *
     * <p>Returns null if no private key is available</p>
     */
    public PrivateKey getPrivateSubKey(PublicKey pubsubkey,
                                          char[] passphrase)
        throws UnrecoverableKeyException
    {
        if (! (pubsubkey instanceof PGPPublicKey)) {
            throw new IllegalArgumentException("Invalid public key type");
        }
        
        if (! (privSubkeys.containsKey(pubsubkey))) {
            return null;
        }
        
        PGPSecretSubKeyPacket pkt = 
            (PGPSecretSubKeyPacket)privSubkeys.get(pubsubkey);
            
        PGPSecretSubKeyPacket clone = (PGPSecretSubKeyPacket)pkt.clone();

        try {
            clone.decrypt(passphrase, PGPAlgorithmFactory.getDefaultInstance());
        } catch (PGPWrongPassphraseException e) {
            throw new UnrecoverableKeyException("Invalid passphrase");
        } catch (PGPDataFormatException e) {
            throw new UnrecoverableKeyException(
                "Parsing error, invalid decrypted data - "+e);
        }
        
        return new PGPPrivateKey(clone, pubsubkey.getAlgorithm());
    }


    /**
     * Returns the expiration date, or null of the keybundle does not expire
     *
     * <p>Note: this method assumes that all self signed certificates in this
     * keybundle are correctly signed and not revoked. It does not verify this. 
     * When getting a key from an untrusted source, these should thus be 
     * checked first.</p>
     */
    public Date getExpirationDate()
        throws KeyBundleException
    {
        PGPKeyPacket kp = mainkey.getPacket();
        if (kp.getVersion() <= 3) {
            
            int expire = kp.getV23ExpirationDate();
            if (expire == 0) return null;
            
            Date crea = kp.getCreationDate();
            return new Date(crea.getTime() + 24L*60*60*1000*expire);
            
        } else {
        
            try {
                
                Date exp = null;
                Iterator certs = getCertificates();
                while (certs.hasNext()) {
                    PGPCertificateImpl cert = (PGPCertificateImpl)certs.next();
                    if (cert.isSelfSigned()) {
                        PGPSignatureSubPacket sp = cert.getHashedPacket(
                            PGPSignatureConstants.SSP_KEY_EXPIRATION_TIME);
                        if (sp != null) {
                            Date date = ((PGPDateSP)sp).getValue();
                            Date crea = kp.getCreationDate();
                            date = new Date(date.getTime() + crea.getTime());
                            if (exp == null) exp = date;
                            else if (exp.compareTo(date) > 0) exp = date;
                        }
                    }
                }
    
                return exp;
            
            } catch (CertificateParsingException cpe) {
                throw new KeyBundleException(
                    "CertificateParsingException: "+cpe.getMessage());
            } catch (CertificateException ce) {
                throw new KeyBundleException(
                    "CertificateException: "+ce.getMessage());
            }
        }
    }
    
        
    /**
     * Returns whether this is a legacy key
     */
    public boolean isLegacy() {
        return mainkey.isLegacy();
    }
    
// Inner iterator classes
// ..........................................................................


    /**
     * An iterator that does not support the remove method
     */
    private class RemoveNotSupportedIterator implements Iterator {
    
        private Iterator parent;
    
        public RemoveNotSupportedIterator(Iterator parent) {
            this.parent = parent;
        }
        
        public boolean hasNext() { 
            return parent.hasNext(); 
        }
        
        public Object next() {
            return parent.next();
        }
        
        public void remove() {
            throw new UnsupportedOperationException(
                "Remove not supported. Use the remove methods in KeyBundle.");
        }
    
    }


    /**
     * An emtpy iterator
     */
    private class EmptyIterator implements Iterator {
    
        public EmptyIterator() {}
        
        public boolean hasNext() { 
            return false;
        }
        
        public Object next() {
            throw new NoSuchElementException("Iterator is empty.");
        }
        
        public void remove() {
            throw new UnsupportedOperationException(
                "Remove not supported. Use the remove methods in KeyBundle.");
        }
    
    }


    /**
     * An emtpy iterator
     */
    private class SingleObjectIterator implements Iterator {
    
        private Object obj;
        private boolean retrieved = false;
    
        public SingleObjectIterator(Object obj) {
            this.obj = obj;
        }
        
        public boolean hasNext() { 
            return !retrieved;
        }
        
        public Object next() {
            if (retrieved) 
                throw new NoSuchElementException("No more elements.");
            retrieved = true;
            return obj;
        }
        
        public void remove() {
            throw new UnsupportedOperationException(
                "Remove not supported. Use the remove methods in KeyBundle.");
        }
    
    }

}
