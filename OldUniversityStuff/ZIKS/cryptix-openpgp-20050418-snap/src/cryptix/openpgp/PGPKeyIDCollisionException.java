/* $Id: PGPKeyIDCollisionException.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


/**
 * Thrown when one key is requested, but multiple keys with the same
 * keyID is available.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPKeyIDCollisionException extends PGPException {
    

    /**
     * Constructs an exception without a specific error message.
     */
    public PGPKeyIDCollisionException  () { super(); }


    /**
     * Constructs an exception with the specified error message.
     *
     * @param msg the error message.
     */
    public PGPKeyIDCollisionException  (String msg) { super(msg); }
    
}
