/* $Id: PGPKeyRingCertStoreParameters.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import java.io.File;

import java.security.cert.CertStoreParameters;


/**
 * Certstore parameters for a keyring
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPKeyRingCertStoreParameters implements CertStoreParameters {


// Instance variables
// ..........................................................................


    private final File file;


// Constructor
// ..........................................................................


    public PGPKeyRingCertStoreParameters(String filename) {
        this.file = new File(filename);
    }


    public PGPKeyRingCertStoreParameters(File file) {
        this.file = file;
    }



// Method from java.security.cert.CertStoreParameters
// ..........................................................................


    /**
     * Returns a clone of this object.
     */
    public Object clone() {
        return new PGPKeyRingCertStoreParameters(this.file);
    }
    
    

// Own method
// ..........................................................................


    /**
     * Returns the file representing the keyring
     */
    public File getFile() {
        return this.file;
    }
    
    
    /**
     * Compares this object to another one
     */
    public boolean equals(Object other) {
        if (! (other instanceof PGPKeyRingCertStoreParameters))
            return false;
        return this.file.equals(((PGPKeyRingCertStoreParameters)other).file);
    }
    
    
    /**
     * Returns a hash code for this object
     */
    public int hashCode() {
        return this.file.hashCode();
    }
    
    
    /**
     * Returns a string representation of this object
     */
    public String toString() {
        return this.file.toString();
    }

}
