/* $Id: PGPV3SignatureParameterSpec.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import java.security.spec.AlgorithmParameterSpec;

import java.util.Vector;


/**
 * Parameters for a V3 OpenPGP Signature
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPV3SignatureParameterSpec implements AlgorithmParameterSpec {

// Instance variables
//..............................................................................

    /** The 8 byte issuer key id */
    private byte[] issuer;
    /** The 4 byte signature time */
    private byte[] time;
    /** The signature type byte */
    private byte sigtype;


// Constructor
//..............................................................................

    /**
     * Constructs a PGPV3SignatureParameterSpec object, given the issuer,
     * creation time and type byte
     */
    public PGPV3SignatureParameterSpec(byte[] issuer, byte[] time, 
                                       byte sigtype) 
    {
        this.issuer = issuer;
        this.time = time;
        this.sigtype = sigtype;
    }


// Getters
//..............................................................................

    /**
     * Returns the issuer key id as a bytearray
     */
    public byte[] getIssuer() {
        return issuer;
    }
       
    /**
     * Returns the creation time as a bytearray
     */ 
    public byte[] getTime() {
        return time;
    }
    
    /**
     * Returns the OpenPGP signature type byte
     */
    public byte getSigType() {
        return sigtype;
    }

}
