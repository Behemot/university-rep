/* $Id: PGPDetachedSignatureMessage.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.message.Message;
import cryptix.message.MessageException;

import cryptix.openpgp.packet.PGPSignaturePacket;

import cryptix.pki.KeyBundle;

import java.security.PublicKey;


/**
 * Represents a detached signature.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class PGPDetachedSignatureMessage extends Message {


// Constructor
// ..........................................................................

    /**
     * Creates a PGPDetachedSignatureMessage object.
     */
    protected PGPDetachedSignatureMessage(String format) 
    {
        super(format);
    }


// Verify methods
// ..........................................................................

    /**
     * Verify if this detached signature is a correct signature on a particular
     * message by a particular public key.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @return true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public abstract boolean verify(Message msg, PublicKey pubkey) 
        throws MessageException;
        

    /**
     * Verify if this detached signature is a correct signature on a particular
     * message by a particular keybundle.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @return true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public abstract boolean verify(Message msg, KeyBundle pubkey) 
        throws MessageException;

}
