/* $Id: PGPInvalidChecksumException.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


/**
 * Thrown when a checksum does not match.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPInvalidChecksumException extends PGPDataFormatException {
    

    /**
     * Constructs an exception without a specific error message.
     */
    public PGPInvalidChecksumException () { super(); }


    /**
     * Constructs an exception with the specified error message.
     *
     * @param msg the error message.
     */
    public PGPInvalidChecksumException (String msg) { super(msg); }
    
}
