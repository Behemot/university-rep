/* $Id: PGPPublicKey.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPKeyPacket;

import java.security.PublicKey;


/**
 * OpenPGP PublicKey.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PGPPublicKey extends PGPKey implements PublicKey {


    public PGPPublicKey(PGPKeyPacket pkt, String algorithm) {
        super(pkt, algorithm);
    }
    

}
