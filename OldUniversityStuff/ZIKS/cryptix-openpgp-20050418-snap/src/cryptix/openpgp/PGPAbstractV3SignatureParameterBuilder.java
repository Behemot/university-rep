/* $Id: PGPAbstractV3SignatureParameterBuilder.java,v 1.2 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.openpgp.packet.PGPSignatureConstants;

import cryptix.openpgp.signature.PGPDateSP;
import cryptix.openpgp.signature.PGPKeyIDSP;
import cryptix.openpgp.signature.PGPSignatureSubPacket;

import cryptix.pki.KeyID;
import cryptix.pki.KeyIDFactory;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import java.security.spec.AlgorithmParameterSpec;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;


/**
 * Abstract class for v3 signature parameters.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class PGPAbstractV3SignatureParameterBuilder {

// Instance variables
//..............................................................................

    /** The 8 byte issuer key id */
    private byte[] issuer;
    /** The 4 byte signature time */
    private byte[] time;
    /** The signature type byte */
    private byte sigtype;


// Constructors
//..............................................................................

    /**
     * Construct a new PGPAbstractV3SignatureParameterBuilder using the given
     * issuer key id and signature type byte.
     *
     * @param issuerkeyid the key id of the key issuing the signature
     * @param sigtype     the signature type byte
     */
    protected PGPAbstractV3SignatureParameterBuilder(KeyID issuerkeyid, 
                                                     byte sigtype) 
    {
        init(issuerkeyid, sigtype);
    }

    /**
     * Construct a new PGPAbstractV3SignatureParameterBuilder using the given
     * issuer key and signature type byte.
     *
     * <p>
     * Note: this is just a convenience method that will extract the key id from
     * the given key. If you already have the key id, calling the other 
     * constructor with that key id is faster.
     * </p>
     *
     * @param issuerkey the key issuing the signature
     * @param sigtype   the signature type byte
     *
     * @throws InvalidKeyException if getting the key id of the issuer key fails
     *         because of an incompatible or invalid key.
     */
    protected PGPAbstractV3SignatureParameterBuilder(Key issuerkey, byte sigtype)
        throws InvalidKeyException
    {
        KeyID issuerkeyid;
        try {
            issuerkeyid = 
                KeyIDFactory.getInstance("OpenPGP").generateKeyID(issuerkey);
        } catch (NoSuchAlgorithmException nsae) {
            throw new InvalidKeyException("Could not get KeyIDFactory for " +
                                          "OpenPGP");
        }
        init(issuerkeyid, sigtype);
    }

    /**
     * Helper method for the constructors, which adds the default subpackets
     * for all signatures to the hashed and unhashed areas.
     */
    private void init(KeyID issuerkeyid, byte sigtype)
    {
        this.sigtype = sigtype;
        this.issuer = issuerkeyid.getBytes(8);
        this.time = new byte[4];
        updateCreationDate();
    }
    

// Instance methods
//..............................................................................

    /**
     * Build an immutable parameter specification
     *
     * <p>
     * This method can be called more than once, which is useful in case you 
     * want to create multiple signatures with almost the same parameters.
     * Note that in this case all parameters will stay the same, including the
     * creation date and time, which may not be desirable. Use the 
     * updateCreationDate() method to prevent this from happening.
     * </p>
     */
    public AlgorithmParameterSpec build() {
        return new PGPV3SignatureParameterSpec(issuer, time, sigtype);
    }
    
    /**
     * Updates the signature creation date packet to the current date and time
     *
     * <p>
     * This method is simply a wrapper around setCreationDate, calling it with
     * the current date and time.
     * </p>
     */
    public void updateCreationDate() {
        setCreationDate(new Date());
    }
    
    /** 
     * Set the signature creation date packet
     *
     * <p>
     * With this method one can set the signature creation date and time to
     * any random value. While this has useful applications, it should be used
     * carefully because setting it to something else than the current time may
     * break some things.
     * </p>
     *
     * @param creation the new date and time
     */
    public void setCreationDate(Date creation) {
        long m = creation.getTime();
        m = m/1000;
        if ((m < 0) || (m > (0xFFFFFFFFL * 1000L))) {
            throw new IllegalArgumentException("Date not within acceptable "+
                                               "range");
        }
        time[0] = (byte)((m >> 24) & 0xff);
        time[1] = (byte)((m >> 16) & 0xff);
        time[2] = (byte)((m >>  8) & 0xff);
        time[3] = (byte)( m        & 0xff);
    }
    
    /**
     * Set the issuer key id
     *
     * @param issuerkeyid the key id of the key issuing the signature
     */
    public void setIssuerKeyID(KeyID issuerkeyid) {
        this.issuer = issuerkeyid.getBytes(8);
    }
    
    /**
     * Sets the signature type byte
     *
     * @param sigtype the new signature type byte
     */
    public void setSignatureType(byte sigtype) {
        this.sigtype = sigtype;
    }
    
}
