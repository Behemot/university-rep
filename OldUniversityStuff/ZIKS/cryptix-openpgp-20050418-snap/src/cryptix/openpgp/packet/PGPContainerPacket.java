/* $Id: PGPContainerPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;


/**
 * The PGPContainerPacket provides (or will provide) an infrastructure
 * for an ordered list of packets.  This class holds the collection of
 * "subpackets".  Subclasses can use the encodeSubPackets method to
 * call encode() recursively on the subpackets.  And, for the
 * decoding, clients can use devodeSubPackets to take packets out of a
 * stream and place them in this collection.
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com) 
 * @version $Revision: 1.2 $ 
 */
public abstract class PGPContainerPacket extends PGPPacket{
    

  // Buffer size for our stream copy routine in decodeBody
  private static final int BUFSIZE = 1024;

  private Vector packets;



// Constructor
// ..........................................................................


  /** 
   * Construct the new container packet.  
   */
  public PGPContainerPacket() {
    packets = new Vector();
  }



// Methods from PGPPacket
// ..........................................................................


  /**
   * Compare this container packet to another.  Two container packets
   * are equal if they both contain the same packets in the same
   * order.
   *
   * @return true if both are equal, false otherwise.  
   */
  public boolean equals(Object ssp) {
    if ((ssp != null) && (ssp instanceof PGPContainerPacket)) {
      PGPContainerPacket other = (PGPContainerPacket) ssp;
      Enumeration thisList = listPackets();
      Enumeration otherList = other.listPackets();
      while(thisList.hasMoreElements()) {
        if (! otherList.hasMoreElements())
          return(false);
        if (! thisList.nextElement().equals(otherList.nextElement()))
          return(false);
      }
      return(!otherList.hasMoreElements());
    }
    return(false);
  }



// Getters & Setters
// ..........................................................................


  /** 
   * Append a packet to the ordered list.  This will add
   * the given packet to the end of the list.  
   *
   * @param p the packet to add.
   */
  public void appendPacket(PGPPacket p) {
    packets.addElement(p);
  }


  /** 
   * Provides access to the list of subpackets, in order.
   *
   * @return an Enumeration of PGPPacket objects.
   */
  public Enumeration listPackets() {
    return(packets.elements());
  }



// Methods for subclasses
// ..........................................................................


  /** 
   * Encode all of the packets in this container, back to back into
   * the given output stream.  Subclasses should call this from their
   * implementations of encodeBody() or encode().  This buffers one
   * packet at a time.
   *
   * @param out the output stream on which to encode all the packets.
   * @exception IOException if the outputstream throws one
   */
  protected void encodeSubPackets(OutputStream out) 
    throws IOException 
  {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();   
    for(Enumeration i = listPackets(); i.hasMoreElements(); ) {
      ((PGPPacket) i.nextElement()).encode(buffer);
      buffer.flush();
      byte[] barr = buffer.toByteArray();
      out.write(barr);
      buffer.reset();
    }
  }




  /**
   * Assume that the passed input stream contains at least one
   * packet.  Use the factory to parse the packets from the
   * stream and add the packets to our list.
   *
   * @param in the raw packet data
   * @param fac the algorithm factory
   * @exception PGPDataFormatException may be thrown by the factory
   * @exception PGPFatalDataFormatException may be thrown by the factory
   * @exception IOException if there is a unexpected buffer error
   */
  protected void decodeSubPackets(InputStream in,  PGPAlgorithmFactory fac) 
    throws PGPDataFormatException, PGPFatalDataFormatException, IOException
  {   
    //
    // Here we break with our ideal world of streaming data, and 
    // collect the sub packet data into a buffer that we parse.
    // We might avoid this if there were a way to get the length
    // from the input stream.
    //
    // First we copy the entire input into 'xfer', then we 
    // copy it again into 'packetData'.
    //
    ByteArrayOutputStream xfer = new ByteArrayOutputStream();

    byte[] buf = new byte[BUFSIZE];
    int len;
    while((len = in.read(buf)) > 0) {
      xfer.write(buf, 0, len);
    }

    ByteArrayInputStream packetData = null;
    try {         
      packetData = new ByteArrayInputStream(xfer.toByteArray());
      xfer = null;
      while(packetData.available() > 0) {
        appendPacket(PGPPacketFactory.getDefaultInstance().readPacket(packetData, fac));
      }
      packetData.close();
    }
    catch(IOException e) {
      // hmm, trouble with the byte array
      throw(e);
    }
    finally {
      if (packetData != null) {
        try {
          packetData.close();
        } catch(Exception ignore) { }
        packetData = null;
      }   
    }  
  }
    
}
