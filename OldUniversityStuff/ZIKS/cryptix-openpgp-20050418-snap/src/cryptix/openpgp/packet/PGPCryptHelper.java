/* $Id: PGPCryptHelper.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

/**
 * Class containing a static helper method which contains the core encryption
 * routines common to all packets using encryption.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

package cryptix.openpgp.packet;


import java.util.StringTokenizer;

import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/* package */ class PGPCryptHelper {


    /** 
     * Constructor
     *
     * <p>Private in order to prevent instantiation: this class only has static
     * methods.</p>
     */
    private PGPCryptHelper() {}


    /**
     * Helper method that does the actual encryption/decryption
     *
     * @param encrypt true for encrypt, false for decrypt
     * @param cp a cipher instance
     * @param iv the iv
     * @param key the key
     * @param data the data to be encrypted/decrypted
     * @returns the encrypted/decrypted data
     */
    /* package */ static byte[] crypt(boolean encrypt, Cipher cp, byte[] iv, 
                                      byte[] key, byte[] data)
    {
        
        // construct the key
        StringTokenizer st = new StringTokenizer(cp.getAlgorithm(), "/");
        SecretKeySpec sk = new SecretKeySpec(key, st.nextToken());

            
        // convert the iv
        IvParameterSpec ivspec = new IvParameterSpec(iv);

            
        // init cipher
        try {
            if (encrypt) {
                cp.init(Cipher.ENCRYPT_MODE, sk, ivspec);
            } else {
                cp.init(Cipher.DECRYPT_MODE, sk, ivspec);
            }
        } catch (InvalidKeyException ike) {
            throw new InternalError("InvalidKeyException on "+
                                    "de/encrypting a key - "+ike);
        } catch (InvalidAlgorithmParameterException iape) {
            throw new InternalError("InvalidAlgorithmParameterException "+
                                    "on de/encrypting a key - "+iape);
        }
            

        // decrypt
        byte[] result;
        try {
            result = cp.doFinal(data);
        } catch (IllegalBlockSizeException ibse) {
            throw new RuntimeException("IllegalBlockSizeException on "+
                                       "de/encrypting a key - "+ibse);
        } catch (BadPaddingException bpe) {
            throw new InternalError("BadPaddingException on "+
                                    "de/encrypting a key - "+bpe);
        }

        return result;
        
    }
    
}
