/* $Id: PGPPacketFactory.java,v 1.3 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;

import java.util.Hashtable;
import java.io.InputStream;
import java.io.IOException;


/**
 * A factory to read packets from a stream
 *
 * <p>When initialized using the default constructor, this class knows how to
 * parse all standard OpenPGP packets. However, an application can register
 * their own packets if needed.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPPacketFactory {


// Constants
//...........................................................................

    /** Initialize with all default OpenPGP packets registered */
    public static final int INIT_DEFAULT          = 0x00;
    /** Initialize with no packets registered */
    public static final int INIT_EMPTY_ALL        = 0xFF;
    /** Initialize with no packets registered */
    public static final int INIT_EMPTY            = INIT_EMPTY_ALL;


// Vars
//...........................................................................

    private Hashtable
        PacketClasses   = new Hashtable();


// Default data
//...........................................................................

    private static final int[] defaultPacketIDs = {
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 18, 19
    };
    private static final String[] defaultPacketClasses = {
        "cryptix.openpgp.packet.PGPPublicKeyEncryptedSessionKeyPacket",    // 1
        "cryptix.openpgp.packet.PGPSignaturePacket",                       // 2
        "cryptix.openpgp.packet.PGPSymmetricKeyEncryptedSessionKeyPacket", // 3
        "cryptix.openpgp.packet.PGPOnePassSignaturePacket",                // 4
        "cryptix.openpgp.packet.PGPSecretKeyPacket",                       // 5
        "cryptix.openpgp.packet.PGPPublicKeyPacket",                       // 6
        "cryptix.openpgp.packet.PGPSecretSubKeyPacket",                    // 7
        "cryptix.openpgp.packet.PGPCompressedDataPacket",                  // 8
        "cryptix.openpgp.packet.PGPSymmetricallyEncryptedDataPacket",      // 9
        "cryptix.openpgp.packet.PGPMarkerPacket",                          //10
        "cryptix.openpgp.packet.PGPLiteralDataPacket",                     //11
        "cryptix.openpgp.packet.PGPTrustPacket",                           //12
        "cryptix.openpgp.packet.PGPUserIDPacket",                          //13
        "cryptix.openpgp.packet.PGPPublicSubKeyPacket",                    //14
        "cryptix.openpgp.packet.PGPSymmetricallyEncryptedIntegrityProtectedDataPacket", //18
        "cryptix.openpgp.packet.PGPModificationDetectionCodePacket",       //19
    };



// Constructors
//...........................................................................

    /** 
     * Constructor that registers all default packets
     */
    public PGPPacketFactory () {
        this(INIT_DEFAULT);
    }


    /** 
     * Constructor that registers the given packets
     */
    public PGPPacketFactory (int init) {

        if ((init & INIT_EMPTY_ALL)  == 0)
            DefaultPackets();

    }



// Constructor help methods
//...........................................................................

    private void DefaultPackets() {
        for (int i=0; i<defaultPacketIDs.length; i++) {
            registerPacket(defaultPacketIDs[i],
                           defaultPacketClasses[i]);
        }
    }



// Default instance method
//...........................................................................

    /** Cached default instance */
    private static PGPPacketFactory defaultInstance = null;


    /**
     * Return the default instance of this factory.
     *
     * <p>Useful when you don't need to do any customization.</p>
     * <p>This method returns an instance that has been constructed using the
     * INIT_DEFAULT parameter.</p>
     */
    public static PGPPacketFactory getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = new PGPPacketFactory(INIT_DEFAULT);
        }
        return defaultInstance;
    }



// Register
//...........................................................................

    /**
     * Register a new packet
     *
     * @param id the packet id under which this packet is known
     * @param classname the name of a class extending from PGPPacket that 
     *        implements this packet.
     */
    public void registerPacket(int id, String classname) {
        PacketClasses.put(new Integer(id),classname);
    }



// Unregister
//...........................................................................

    /**
     * Unregister an already registered packet
     *
     * <p>If the packet was not registered in the first place, nothing happens
     *
     * @param id the packet id to unregister
     */
    public void unregisterPacket(int id) {
        PacketClasses.remove(new Integer(id));
    }



// Get methods
//...........................................................................

    /**
     * Read a packet from a stream
     *
     * <p>
     * If a registered packet cannot be loaded for some reason, a
     * RuntimeException will be thrown. If no packet is registered for the
     * packet read from the stream, a PGPUnknownPacket will be loaded.
     * </p><p>
     * Note about lazy packets: normally, the complete record will be read from 
     * the inputstream. However, if the isLazy() method on the returned packet 
     * returns true, then the caller needs to do some more processing with that 
     * packet before a new packet can be read from the inputstream.
     * </p>
     *
     * @param in the inputstream to read a packet from
     * @param factory the factory to pass to the packet
     * @return the packet that was read
     * @throws IOException if something goes wrong while reading from the 
     *         inputstream
     * @throws PGPDataFormatException if a recoverable error occurs in the
     *         data. In this case the current packet is lost, but the pointer
     *         in the inputstream points to the next packet, such that that one
     *         can be read and this one ignored.
     * @throws PGPFatalDataFormatException if a non-recoverable error occurs in
     *         the data. In this case no more packets can be read from the
     *         inputstream.
     */
    public PGPPacket readPacket(InputStream in, PGPAlgorithmFactory factory)
        throws IOException, PGPDataFormatException, PGPFatalDataFormatException
    {

        int id, len, type;
        boolean old;

        try {
            type = in.read();
        } catch (IOException ioe) {
            throw new IOException("Error reading packet type: "+ioe.toString());
        }

        if (type < 128) {
            throw new PGPFatalDataFormatException("Invalid packet type.");
        }

        if (type >= 192) {   // new format
            id = type & 0x3F;
            old = false;
            len = -1;
        } else {             // old format
            id = (type >> 2) & 0x0F;
            old = true;
            len = type & 0x03;
        }

        PGPPacket packet = getPacket(id);
        PGPPacketDataInputStream pin = 
                new PGPPacketDataInputStream (in, old, len);
        packet.decodeBody(pin, factory);
        return packet;
    }


    public PGPPacket getPacket(int id)
    {
        try {

            String classname = (String)PacketClasses.get(new Integer(id));
            if (classname == null) {
    
                // Let's load a placeholder packet.
                PGPUnknownPacket packet = new PGPUnknownPacket();
                packet.setPacketID((byte)id);
                return packet;
    
            } else {
    
                Class cl = Class.forName(classname);
                PGPPacket packet = (PGPPacket)cl.newInstance();
                packet.setPacketID((byte)id);
                return packet;
    
            }
    
        } catch (ClassNotFoundException cnfe) {

            throw new RuntimeException("Packet #"+id+" cannot be loaded.");

        } catch (InstantiationException cnfe) {

            throw new RuntimeException("Packet #"+id+" cannot be loaded.");

        } catch (IllegalAccessException cnfe) {

            throw new RuntimeException("Packet #"+id+" cannot be loaded.");

        }
    }

}
