/* $Id: PGPSignatureConstants.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


/**
 * Class containing constants for all signature types and signature subpackets
 * defined by OpenPGP.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPSignatureConstants {

// Constructor
//.............................................................................

    private PGPSignatureConstants() {}   // static vars only, no instance



// Signature type constants
// ..........................................................................

    /** Signature of a binary document. */
    public static final byte TYPE_BINARY_DOCUMENT = 0x00;

    /** Signature of a canonical text document. */
    public static final byte TYPE_TEXT_DOCUMENT   = 0x01;

    /** Standalone signature of only its own subpacket contents.  */
    public static final byte TYPE_STANDALONE      = 0x02;

    /** Generic certification of a User ID and Public Key packet. */
    public static final byte TYPE_GENERIC_CERT    = 0x10;

    /** Persona certification of a User ID and Public Key packet. */
    public static final byte TYPE_PERSONA_CERT    = 0x11;

    /** Casual certification of a User ID and Public Key packet. */
    public static final byte TYPE_CASUAL_CERT     = 0x12;

    /** Positive certification of a User ID and Public Key packet. */
    public static final byte TYPE_POSITIVE_CERT   = 0x13;

    /** Subkey Binding Signature */
    public static final byte TYPE_SUBKEY_BINDING  = 0x18;

    /** Signature directly on a key */
    public static final byte TYPE_DIRECT_ON_KEY   = 0x1F;

    /** Key revocation signature */
    public static final byte TYPE_KEY_REVOKE      = 0x20;

    /** Subkey revocation signature */
    public static final byte TYPE_SUBKEY_REVOKE   = 0x28;

    /** Certification revocation signature */
    public static final byte TYPE_CERT_REVOKE     = 0x30;

    /** Timestamp signature. */
    public static final byte TYPE_TIMESTAMP       = 0x40;



// Signature Subpacket constants
// ..........................................................................
   
    /** 2 = signature creation time */
    public static final byte SSP_SIG_CREATION_TIME = 2;

    /** 3 = signature expiration time */
    public static final byte SSP_SIG_EXPIRATION_TIME = 3;

    /** 4 = exportable certification */
    public static final byte SSP_EXPORTABLE_CERTIFICATION = 4;

    /** 5 = trust signature */
    public static final byte SSP_TRUST_SIGNATURE = 5;

    /** 6 = regular expression */
    public static final byte SSP_REGULAR_EXPRESSION = 6;

    /** 7 = revocable */
    public static final byte SSP_REVOCABLE = 7;

    /** 9 = key expiration time */
    public static final byte SSP_KEY_EXPIRATION_TIME = 9;

    /** 11 = preferred symmetric algorithms */
    public static final byte SSP_PREFERRED_SYMMETRIC = 11;

    /** 12 = revocation key */
    public static final byte SSP_REVOCATION_KEY = 12;

    /** 16 = issuer key ID */
    public static final byte SSP_ISSUER_KEYID = 16;

    /** 20 = notation data */
    public static final byte SSP_NOTATIONDATA = 20;

    /** 21 = preferred hash algorithms */
    public static final byte SSP_PREFERRED_HASH = 21;

    /** 22 = preferred compression algorithms */
    public static final byte SSP_PREFERRED_COMPRESSION = 22;

    /** 23 = key server preferences */
    public static final byte SSP_KEYSERVER_PREFS = 23;

    /** 24 = preferred key server */
    public static final byte SSP_PREFERRED_KEYSERVER = 24;

    /** 25 = primary user id */
    public static final byte SSP_PRIMARY_USERID = 25;

    /** 26 = policy URL */
    public static final byte SSP_POLICY_URL = 26;

    /** 27 = key flags */
    public static final byte SSP_KEY_FLAGS = 27;

    /** 28 = signer's user id */
    public static final byte SSP_SIGNER_USERID = 28;

    /** 29 = reason for revocation */
    public static final byte SSP_REASON_FOR_REVOCATION = 29;
    

}
