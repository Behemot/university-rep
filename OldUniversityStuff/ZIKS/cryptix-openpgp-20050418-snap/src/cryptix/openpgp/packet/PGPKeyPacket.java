/* $Id: PGPKeyPacket.java,v 1.3 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPAbstractDataFormatException;
import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;
import cryptix.openpgp.PGPWrongPassphraseException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPPublicKeyAlgorithm;
import cryptix.openpgp.algorithm.PGPStringToKey;

import cryptix.openpgp.io.PGPByteArrayDataInputStream;
import cryptix.openpgp.io.PGPByteArrayDataOutputStream;
import cryptix.openpgp.io.PGPChecksumDataInputStream;
import cryptix.openpgp.io.PGPChecksumDataOutputStream;
import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.util.Date;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;


/**
 * Generic superclass for all keypackets. Note that this class may change a bit
 * to support encrypted keys.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public abstract class PGPKeyPacket extends PGPPacket implements KeySpec {
    
    
// Instance variables
// ..........................................................................

    private PGPPublicKeyAlgorithm algorithm;
    private int version;
    private Date creation;
    private int expire;
    private byte algoid;
    private byte[] secretdata;
    
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor
     */
    protected PGPKeyPacket() {}


    /** 
     * Helper constructor for cloning.
     */
    protected PGPKeyPacket(PGPKeyPacket toClone) {
        this.algorithm = toClone.algorithm.clonePrivate();
        this.version = toClone.version;
        this.creation = toClone.creation;
        this.expire = toClone.expire;
        this.algoid = toClone.algoid;
        this.secretdata = toClone.secretdata;
        setPacketID(toClone.getPacketID());
    }


// Getters & Setters
// ..........................................................................

    public Date getCreationDate()               { return creation; }
    public void setCreationDate(Date date)      { creation = date; }
    
    public int  getV23ExpirationDate()          { return expire;   }
    public void setV23ExpirationDate(int e)     { expire = e;      }
    
    public int  getVersion()                    { return version;  }
    public void setVersion(int v)               { version = v;     }
    
    public byte getAlgorithmID()                { return algoid;   }
    public void setAlgorithmID(byte a)          { algoid = a;      }
    
    
    public PGPPublicKeyAlgorithm getAlgorithm() { 
        return algorithm; 
    }
    
    
    public void setAlgorithm(PGPPublicKeyAlgorithm algorithm, byte algoid) { 
        this.algorithm = algorithm; 
        this.algoid = algoid;
    }
    
    

// Methods from java.lang.Object
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
    
        if (ssp instanceof PGPKeyPacket) {
        
            PGPKeyPacket kp = (PGPKeyPacket)ssp;
            
            return compare(creation,  kp.getCreationDate())
                &&        (expire  == kp.getV23ExpirationDate())
                &&        (version == kp.getVersion())
                && compare(algorithm, kp.getAlgorithm());

        } else {
        
            return false;
            
        }
            
    }
    
    
    /**
     * Compare help method.
     */
    private boolean compare(Object a, Object b) {
    
        if (a == null) {
            return (b == null);
        } else {
            if (b == null) return false;
            return a.equals(b);
        }
        
    }
    
    
    public abstract Object clone();
    

    public PGPPublicKeyPacket clonePublic() {

        PGPPublicKeyPacket newpkt = new PGPPublicKeyPacket();

        newpkt.setAlgorithm(this.algorithm.clonePublic(), this.algoid);
        newpkt.setCreationDate(this.creation);
        newpkt.setV23ExpirationDate(this.expire);
        newpkt.setVersion(this.version);
        
        return newpkt;
        
    }


    public PGPPublicSubKeyPacket clonePublicSub() {

        PGPPublicSubKeyPacket newpkt = new PGPPublicSubKeyPacket();

        newpkt.setAlgorithm(this.algorithm.clonePublic(), this.algoid);
        newpkt.setCreationDate(this.creation);
        newpkt.setVersion(this.version);
        
        return newpkt;
        
    }


// Methods from PGPPacket
// ..........................................................................

    /**
     * Return the forced length type for old style packets.
     *
     * <p>
     * For V3 key packets, it returns 1, which means that this packet will be
     * packed with a two byte length instead of a one byte length. This is for
     * PGP 2.6 compatibility. 
     * </p>
     */
    public int getForceLengthType() {
        if (version <= 3) return 1;
        return -1;
    }


// Decode methods
// ..........................................................................

    /**
     * Convenience method to decode the public part of the key
     */
    protected void decodePublicData(PGPPacketDataInputStream in,
                                    PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {
        
        
        version = in.readUnsignedByte();
        if ( (version < 2) || (version > 4) )
            throw new RuntimeException ("Unknown Keypacket version #"+version);
        
        
        long d = in.readUnsignedInt();
        creation = new Date ( d * 1000 );
        
        
        if ( version == 4 ) {
            expire = -1;
        } else {
            expire = in.readUnsignedShort();
        }
        
        
        algoid = (byte)in.readUnsignedByte();

        try {
            algorithm = factory.getPublicKeyAlgorithm(algoid);
        } catch (NoSuchAlgorithmException nsae) {
            throw new PGPDataFormatException("id not found - "+nsae);
        }

        
        algorithm.decodePublicData(in);
        
    }
    
    
    /**
     * Convenience method to decode the secret part of the key
     */
    protected void decodeSecretData(PGPPacketDataInputStream in) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {

        secretdata = in.readByteArray();
        
        if (secretdata[0] == 0) { // unencrypted secret key
    
            // construct stream
            PGPByteArrayDataInputStream pbais;
            pbais = new PGPByteArrayDataInputStream(secretdata);
            pbais.readByte(); // ignore encryption type byte
                
            // use a special stream to verify the checksum afterwards
            PGPChecksumDataInputStream cdis;
            cdis = new PGPChecksumDataInputStream(pbais);
        
            // read the data
            algorithm.decodeSecretData(cdis);
        
            // reads and checks the checksum, throws exception on fail
            cdis.close();
            
            // close the stream, throws exception if bytes left
            pbais.close();
            
        } 
        
    }
    


// Encode methods
// ..........................................................................

    /**
     * Convenience method to encode the public part of the key
     */
    protected void encodePublicData(PGPPacketDataOutputStream out) 
        throws IOException 
    {
        if ((version < 2) || (version > 4)) 
            throw new IllegalStateException("Invalid version "+version);
      
        out.writeByte((byte)version);
        
        // # fix this warning
        if (creation == null) { 
            System.out.println("Warning! Creation date not set!"); 
            creation = new Date(); 
        }
        int time = (int)(creation.getTime() / 1000);
        out.writeInt(time);
        
        if ((version == 2) || (version == 3))
            out.writeShort((short)expire);
        
        out.writeByte((byte)algoid);
        
        algorithm.encodePublicData(out);
        
    }
    
    
    /**
     * Convenience method to encode the secret part of the key
     */
    protected void encodeSecretData(PGPPacketDataOutputStream out) 
        throws IOException 
    {
        if ((version < 2) || (version > 4)) 
            throw new IllegalStateException("Invalid version "+version);
    
        if (secretdata != null) {
            
            // write the encrypted data
            out.writeFully(secretdata);
            
        } else { 
        
            // no encrypted data available, therefore let's write the
            // data unencrypted.
            out.writeByte((byte)0);
        
            // use a special outputstream that also writes the checksum.
            PGPChecksumDataOutputStream cdos;
            cdos = new PGPChecksumDataOutputStream(out);

            // get the secret data
            algorithm.encodeSecretData(cdos);
        
            // writes the checksum
            cdos.close();
        
        }
                
    }
    


// (d)e(n)crypt methods
//.............................................................................

    public void forgetSecretData() {
        algorithm.forgetSecretData();
    }

    public void encrypt(char[] passphrase, int s2kid, int cipherid, int hashid,
                        SecureRandom sr, PGPAlgorithmFactory factory) 
    {
        if ((version < 2) || (version > 4)) 
            throw new IllegalStateException("Invalid version "+version);

        try {
        
            // get the unencrypted data
            PGPByteArrayDataOutputStream pbados;
            pbados = new PGPByteArrayDataOutputStream();
            algorithm.encodeSecretData(pbados);
            byte[] temp = pbados.toByteArray();
            
            // append checksum
            int sum = 0;
            for (int i=0; i<temp.length; i++) 
                sum += ((int)temp[i]) & 0xff;

            byte[] data = new byte[temp.length+2];
            System.arraycopy(temp,0,data,0,temp.length);

            data[temp.length+0] = (byte)((sum >> 8) & 0xff);
            data[temp.length+1] = (byte)( sum       & 0xff);

            
            // create a place to save the output
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(baos);
            

            // write algorithm id's
            if (version == 4) {

                out.writeByte((byte)255); // new format
                out.writeByte((byte)cipherid);
                out.writeByte((byte)s2kid);
                out.writeByte((byte)hashid);

            } else { // (version == 3) || (version == 2)
                
                out.writeByte((byte)cipherid);
                if (s2kid != 0)
                    throw new IllegalArgumentException("s2kid should be 0 = " +
                        "simple for legacy keys");
                if (hashid != 1)
                    throw new IllegalArgumentException("hashid should be 1 = "+
                        "MD5 for legacy keys");
            }
            
            
            // init all algorithms
            PGPStringToKey s2k;
            Cipher cp;
            int keysize, blocksize;
            MessageDigest md;
            try {
                s2k  = factory.getS2KAlgorithm(s2kid);
                md   = factory.getHashAlgorithm(hashid);
                cp   = factory.getCipherAlgorithm(cipherid, "CFB");
                keysize   = factory.getCipherKeySize(cipherid);
                blocksize = factory.getCipherBlockSize(cipherid);
            } catch (NoSuchAlgorithmException nsae) {
                throw new IllegalArgumentException("id not found - "+nsae);
            }
            
        
            // get the hashed passphrase
            byte[] passbytes = new String(passphrase).getBytes("UTF-8");
            byte[] key = s2k.generateAndHash(passbytes, md, keysize, out, sr);

            
            // generate the IV
            byte[] iv = new byte[blocksize];
            sr.nextBytes(iv);
            out.write(iv);
            
            
            // the real thing
            if (version == 4) {

                out.write(PGPCryptHelper.crypt(true, cp, iv, key, data));
            
            } else { // (version == 3) || (version == 2)
                
                int pos = 0;
                
                for (int i=0; i<4; i++) {
                    out.write(data, pos, 2); // write mpi length unencrypted
                    int mpilen = (((data[pos++] & 0xff) << 8) + 
                                   (data[pos++] & 0xff) + 7) >> 3;
                    byte[] mpidata = new byte[mpilen];
                    System.arraycopy(data, pos, mpidata, 0, mpilen);
                    byte[] mpiresult = 
                        PGPCryptHelper.crypt(true, cp, iv, key, mpidata);
                    out.write(mpiresult); // write encrypted mpi data
                    System.arraycopy(mpiresult, mpilen-blocksize, iv, 0, 
                                     blocksize); // update iv
                    pos += mpilen;
                }
                
                out.write(data, pos, 2); // write checksum unencrypted
                
                if (pos + 2 != data.length)
                    throw new InternalError("Extra secret data");
            }
                        
            // save
            out.close(); baos.close();
            secretdata = baos.toByteArray();

        } catch (IOException ioe) {
        
            throw new InternalError("IOException on bytearray");
            
        }
        
    }

    public void decrypt(char[] passphrase, PGPAlgorithmFactory factory)
        throws PGPWrongPassphraseException, PGPDataFormatException
    {

        try {
            
            int s2kid, hashid;

            // construct streams
            ByteArrayInputStream bais = new ByteArrayInputStream(secretdata);
            DataInputStream in = new DataInputStream(bais);


            // read the algorithm identifiers
            int alg = in.readUnsignedByte();
            boolean hashcheck = false;
            if (alg == 255) {         // new format
                alg = in.readUnsignedByte(); 
                s2kid = in.readUnsignedByte(); 
                hashid = in.readUnsignedByte(); 
            } else if (alg == 254) {         // new hash format
                alg = in.readUnsignedByte(); 
                s2kid = in.readUnsignedByte(); 
                hashid = in.readUnsignedByte(); 
                hashcheck = true;
            } else if (alg != 0) {    // old format
                s2kid = 0;  // Simple
                hashid = 1; // MD5
            } else {                            // unencrypted
                return;
            }


            // init all algorithms
            PGPStringToKey s2k;
            Cipher cp;
            int keysize, blocksize;
            MessageDigest md;
            try {
                s2k  = factory.getS2KAlgorithm(s2kid);
                md   = factory.getHashAlgorithm(hashid);
                cp   = factory.getCipherAlgorithm(alg, "CFB");
                keysize   = factory.getCipherKeySize(alg);
                blocksize = factory.getCipherBlockSize(alg);
            } catch (NoSuchAlgorithmException nsae) {
                throw new IllegalArgumentException("id not found - "+nsae);
            }
            
        
            // get the hashed passphrase
            byte[] passbytes = new String(passphrase).getBytes("UTF-8");
            byte[] key = s2k.readAndHash(passbytes, md, keysize, in);

            
            // generate the IV
            byte[] iv = new byte[blocksize];
            in.readFully(iv); 
            

            // read all data
            byte[] data = new byte[in.available()];
            in.readFully(data);
                
                
            byte[] result;
            
            if (version == 4) 
            {
                // the real thing
                result = PGPCryptHelper.crypt(false, cp, iv, key, data);
    
                if (! hashcheck) {
                    // check checksum
                    int sum=0;
                    for (int i=0; i<result.length-2; i++)
                        sum += (((int)result[i]) & 0xff);
                    sum &= 0xffff;
                    
                    int expected = ((((int)result[result.length-2]) & 0xff) << 8) +
                                   (((int)result[result.length-1]) & 0xff);
    
                    if (sum != expected)
                        throw new PGPWrongPassphraseException("Wrong passphrase");
                } else {
                    // check hash
                    try {
                        md = MessageDigest.getInstance("SHA-1");
                    } catch (NoSuchAlgorithmException nsae) {
                        throw new InternalError("SHA-1 hash not found - "+nsae);
                    }
                    md.update(result, 0, result.length-20);
                    byte[] digest = md.digest();
                    for (int i=0; i<20; i++) {
                        if (digest[i] != result[result.length-20+i]) {
                            throw new PGPWrongPassphraseException(
                                "Wrong passphrase");
                        }
                    }
                }
            }
            else if ((version == 2) || (version == 3))
            {
                int pos = 0;
                int mpilen = (((data[pos++] & 0xff) << 8) + 
                               (data[pos++] & 0xff) + 7) >> 3;

                for (int i=0; i<4; i++)
                {
                    try {
                        cp = factory.getCipherAlgorithm(alg, "CFB");
                    } catch (NoSuchAlgorithmException nsae) {
                        throw new IllegalArgumentException(
                        "id not found - "+nsae);
                    }

                    byte[] mpidata = new byte[mpilen];
                    System.arraycopy(data, pos, mpidata, 0, mpilen);
                    byte[] mpiresult = 
                        PGPCryptHelper.crypt(false, cp, iv, key, mpidata);
                    
                    System.arraycopy(data, pos+mpilen-blocksize, iv, 0, 
                                     blocksize);
                    System.arraycopy(mpiresult, 0, data, pos, mpilen);
                    
                    pos += mpilen;
                    mpilen = (((data[pos++] & 0xff) << 8) + 
                               (data[pos++] & 0xff) + 7) >> 3;
                }

                result = new byte[data.length];
                System.arraycopy(data, 0, result, 0, data.length);
                
                // check checksum
                int sum=0;
                for (int i=0; i<result.length-2; i++)
                    sum += (((int)result[i]) & 0xff);
                sum &= 0xffff;
                
                int expected = ((((int)result[result.length-2]) & 0xff) << 8) +
                               (((int)result[result.length-1]) & 0xff);

                if (sum != expected)
                    throw new PGPWrongPassphraseException("Wrong passphrase");
            }
            else
            {
                throw new PGPDataFormatException("Invalid version");
            }
            

            // Feed the decrypted data back into the reader
            try {

                PGPByteArrayDataInputStream pbais;
                pbais = new PGPByteArrayDataInputStream(result);
                algorithm.decodeSecretData(pbais);
                if (! hashcheck) {
                    pbais.readShort(); //discard checksum
                } else {
                    // discard 20 bytes for SHA-1 hash
                    pbais.readInt(); pbais.readLong(); pbais.readLong(); 
                }
                pbais.close();
            
            } catch (PGPAbstractDataFormatException pe) {
                throw new PGPWrongPassphraseException("Wrong passphrase");
            }
            
        } catch (IOException ioe) {
        
            throw new InternalError("IOException on bytearray");
            
        } 
        
    }
    
    
}
