/* $Id: PGPPacket.java,v 1.3 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General License along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import java.io.IOException;
import java.io.OutputStream;


/**
 * Superclass of all packets
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public abstract class PGPPacket {


// Instance variables
// ..........................................................................

    /** the packet id, 256 means not set */
    private int packetid = 256;



// Getters/Setters
// ..........................................................................

    /**
     * Set the packet type of this packet.
     *
     * <p>See <a href="http://www.cryptix.org/docs/openpgp.html#x5231">
     * the OpenPGP specification for a list of the possible values.</p>
     */
    public void setPacketID (byte id) { packetid = id; }


    /**
     * Returns the packet type of this packet.
     *
     * <p>See <a href="http://www.cryptix.org/docs/openpgp.html#x5231">
     * the OpenPGP specification for a list of the possible values.</p>
     */
    public byte getPacketID () { return (byte)packetid; }
    


// Methods from java.lang.Object
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public abstract boolean equals(Object ssp);
    

    /**
     * Returns the hashcode for this object.
     *
     * <p>While this method is correct, it only uses the packetid and is thus
     * far from optimal. Therefore subclasses should override this method if
     * better Hashtable performance is needed.</p>
     */
    public int hashCode() {
        return getPacketID();
    }
    
    

// Decode methods
// ..........................................................................

    /**
     * Decode the packet body,
     *
     * <p>Implementations of this method should not try to read the type and
     * length information as that is being taken care of by PGPPacketFactory and
     * PGPPacketDataInputStream. It should only concern itself with the
     * information in the packet itself.</p>
     * <p>If invalid data is detected the implementation should throw either
     * PGPDataFormatException or PGPFatalDataFormatException. The implementation
     * does not have to take precautions to read all the remaining data in the
     * packet in case of a PGPDataFormatException, the caller will take care of
     * that.</p>
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet
     *                                and recovery is possible.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *                                     packet and recovery is not possible.
     *
     * @param in The inputstream that contains the data
     * @param factory The algorithm factory subclasses can use to get their
     *                algorithm specific classes from.
     */
    public abstract void decodeBody (PGPPacketDataInputStream in, 
                                     PGPAlgorithmFactory factory) 
    throws IOException, PGPFatalDataFormatException, PGPDataFormatException;



    /**
     * Returns if this packet has lazy decoding.
     *
     * <p>
     * Lazy decoding basically means that part of the inputstream has not 
     * been decoded yet after calling the decode method. This means that no
     * more packets may be read from the inputstream until this packet has
     * been read completely (using methods specific to that packet.
     * </p><p>
     * This mainly serves two purposes: it allows for streaming large packets
     * and it allows a caller to pass in some more information necessairy for
     * processing (for example a session key for decrypting).
     * </p><p>
     * This method returns false by default, which means that the packet is
     * read entirely by a call to the decode method. Subclasses that do 
     * implement lazy decoding should override this method to return true.
     * </p>
     */
    public boolean isLazy() {
        return false;
    }
    
    
    /**
     * Return the forced length type for old style packets.
     *
     * <p>
     * This is needed for PGP 2.x compatibility which requires some packets to
     * have a suboptimal length: e.g. a signature packet that would be fine 
     * with a 1 byte length (type 0), must have a 2 byte length (type 1) or
     * otherwise PGP 2.6 will complain.
     * </p><p>
     * Note that the default implementation just returns -1, meaning that no
     * length is forced.
     * </p>
     */
    public int getForceLengthType() {
        return -1;
    }
    

// Write methods
// ..........................................................................


    /**
     * Encode the packet.
     *
     * <p>This method simply constructs a PGPPacketDataOutputStream with the
     * right PacketID, calls encodeBody en closes the PGPPacketDataOutputStream
     * </p>
     *
     * @throws IOException if the outputstream throws one
     */
    public void encode (OutputStream out) throws IOException {
    
        PGPPacketDataOutputStream pout = new PGPPacketDataOutputStream (out, 
                                           getPacketID(), getForceLengthType());
        
        encodeBody(pout);
        
        pout.close();
    
    }



    /**
     * Encode the packet body.
     *
     * <p>Implementations of this method should not try to write the type and
     * length information as that is being taken care of by the encode method
     * and PGPPacketDataOutputStream. It should only write the data in the
     * packet itself.</p>
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public abstract void encodeBody (PGPPacketDataOutputStream out) 
    throws IOException;
    
    
}
