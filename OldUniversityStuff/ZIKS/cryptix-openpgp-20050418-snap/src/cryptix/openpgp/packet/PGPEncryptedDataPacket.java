/* $Id: PGPEncryptedDataPacket.java,v 1.3 2005/04/04 16:31:08 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;

import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * Represents an encrypted data packet.
 *
 * <p>There are two variants of this packet. One with and one without integrity
 * protection.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public abstract class PGPEncryptedDataPacket extends PGPContainerPacket {
    

// Instance variables
// ..........................................................................

    private byte[] encrypted, decrypted;
    private boolean mdc;
    

// Constructor
// ..........................................................................

    /**
     * Emtpy constructor
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    protected PGPEncryptedDataPacket (boolean mdc) {
        this.mdc = mdc;
    }
    


// Decryption
// ..........................................................................

    public void decrypt (PGPSessionKey[] sessionkeys, 
                         PGPAlgorithmFactory factory)
        throws PGPDecryptionFailedException,
               PGPDataFormatException, PGPFatalDataFormatException
    {
        if (decrypted != null) return;
    
        byte[] correctkey = null;
        byte correctid = 0;
        
        for (int i=0; i<sessionkeys.length; i++) {
            
            // get the key
            byte[] key = sessionkeys[i].getBytes();
            
            // retrieve id and block/keysize
            boolean failed = false;
            byte id = sessionkeys[i].getCipherID();
            int blocksize = 0;
            int keysize = 0;
            try {
                blocksize = factory.getCipherBlockSize(id);
                keysize = factory.getCipherKeySize(id);
                
            } catch (NoSuchAlgorithmException nsae) {
                failed = true;
            }
            
            // check if the key has the correct size, if not: ignore this key
            // also ignore this key if the factory started throwing exceptions
            if ((! failed) && (key.length == keysize)) {

                // set iv
                byte[] iv = new byte[blocksize];
                for (int j=0; j<iv.length; j++) { iv[j] = 0; }
                IvParameterSpec ivspec = new IvParameterSpec(iv);
                
                // get the encrypted data and put it back right away for later 
                // usage
                byte[] prefixcrypt = new byte[blocksize*2+2];
                System.arraycopy(encrypted, 0, prefixcrypt, 0, blocksize*2+2);
                
                // get a Cipher instance
                Cipher cp;
                try {
                    cp = factory.getCipherAlgorithm(id, "CFB");
                } catch (NoSuchAlgorithmException nsae) {
                    throw new RuntimeException("Inconsistency in factory: "+
                        "factory.getCipherBlockSize(id) and .getCipherKeySize"+
                        "(id) succeeded, but factory.getCipherAlgorithm(id) "+
                        "did not.");
                }
    
                // convert the key to a keyspec
                StringTokenizer st = new StringTokenizer(cp.getAlgorithm(), "/");
                SecretKeySpec keyspec = new SecretKeySpec(key, st.nextToken());
    
                // init the Cipher
                try {
                    cp.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
                } catch (InvalidKeyException ike) {
                    ike.printStackTrace();
                    throw new InternalError("InvalidKeyException on "+
                                            "decrypting a key - "+ike);
                } catch (InvalidAlgorithmParameterException iape) {
                    iape.printStackTrace();
                    throw new InternalError(
                        "InvalidAlgorithmParameterException "+
                        "on decrypting a key - "+iape);
                }
                
                // the real thing
                byte[] prefix;
                try {
                    prefix = cp.doFinal(prefixcrypt);
                } catch (BadPaddingException bpe) {
                    bpe.printStackTrace();
                    throw new InternalError("Hmm... got a BadPaddingException "+
                        "while we are not even using padding.");
                } catch (IllegalBlockSizeException ibse) {
                    throw new RuntimeException("Inconsistency between "+
                        "blocksize in factory and blocksize in cipher.");
                }
                
                // check
                if ((prefix[blocksize-2] == prefix[blocksize]) &&
                    (prefix[blocksize-1] == prefix[blocksize+1])) {
                    
                    if (correctkey != null) {
                        if (! PGPCompare.equals(correctkey, key)) {
                            throw new PGPDecryptionFailedException(
                                "Two different session keys have passed the "+
                                "test. It is therefore impossible to "+
                                "determine which one to use and thus "+
                                "impossible to correctly decrypt the data."
                            );
                        }
                    }
                    
                    correctkey = key;
                    correctid = id;
                
                }
            }
        }
        
        if (correctkey == null) {
            
            //throw new PGPDecryptionFailedException("No good session key found");
            
            //To thwart the Mister/Zuccherato attack outlined in
            // http://eprint.iacr.org/2005/033 we will decrypt it with a random
            // key here. The result will be gibberish, which will result in 
            // a different error.
            
            correctid = 7;  // AES128
            correctkey = new byte[16];
        }
        
        // init
        int blocksize = 0;
        int keysize = 0;
        Cipher cp = null;
        try {
            blocksize = factory.getCipherBlockSize(correctid);
            keysize = factory.getCipherBlockSize(correctid);
            if (mdc) {
                cp = factory.getCipherAlgorithm(correctid, "CFB");
            } else {
                cp = factory.getCipherAlgorithm(correctid, "OpenpgpCFB");
            }
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new InternalError("PANIC");
        }

        // convert the key to a keyspec
        StringTokenizer st = new StringTokenizer(cp.getAlgorithm(), "/");
        SecretKeySpec keyspec = new SecretKeySpec(correctkey, st.nextToken());
    
        // set iv
        byte[] iv = new byte[blocksize];
        for (int j=0; j<iv.length; j++) { iv[j] = 0; }
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        // init the Cipher
        try {
            cp.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new InternalError("InvalidKeyException on "+
                                    "decrypting a key - "+ike);
        } catch (InvalidAlgorithmParameterException iape) {
            iape.printStackTrace();
            throw new InternalError(
                "InvalidAlgorithmParameterException "+
                "on decrypting a key - "+iape);
        }
        
        // the real thing
        byte[] prefix;
        try {

            int ps = blocksize+2;
            //byte[] prefix = cp.update(encrypted, 0, ps);
            //decrypted = cp.doFinal(encrypted, ps, encrypted.length - ps);
            byte[] result = cp.doFinal(encrypted);
            prefix = new byte[ps];
            decrypted = new byte[result.length-ps];
            System.arraycopy(result, 0, prefix, 0, ps);
            System.arraycopy(result, ps, decrypted, 0, decrypted.length);
        
        } catch (IllegalBlockSizeException ibse) {
            ibse.printStackTrace();
            throw new InternalError("Should not happen on CFB mode. "+ibse);
        } catch (BadPaddingException bpe) {
            bpe.printStackTrace();
            throw new InternalError("Should not happen on CFB mode. "+bpe);
        }
        
        // hashing for modification detection / integrity protection
        if (mdc) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(prefix);
                md.update(decrypted, 0, decrypted.length-20);
                byte[] hash = md.digest();
                if ((decrypted[decrypted.length-22] != (byte)0xd0) &&
                    (decrypted[decrypted.length-21] != (byte)0x14))
                {
                    throw new PGPDataFormatException(
                        "MDC packet missing or invalid");
                }
                for (int i=0; i<20; i++) {
                    if (hash[i] != decrypted[decrypted.length-20+i]) {
                        throw new PGPDataFormatException(
                            "Invalid hash in MDC packet. "+
                            "The message has potentially been tampered with.");
                    }
                }
            } catch (NoSuchAlgorithmException nsae) {
                throw new RuntimeException("SHA-1 hash not found.");
            }
        }
        
        // decode contained packets
        try {
            ByteArrayInputStream bais;
            if (mdc) {        
                bais = new ByteArrayInputStream(decrypted, 0, 
                                                decrypted.length-22);
            } else {
                bais = new ByteArrayInputStream(decrypted);
            }
            decodeSubPackets(bais, factory);
            bais.close();
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
            throw new RuntimeException(
                "IOException on ByteArrayInputStream, should not happen. "+ioe);
        }
    }


    public void encrypt (byte[] key, int cipherid, PGPAlgorithmFactory factory,
                         SecureRandom sr)
    {
        if (encrypted != null) return;
    
        // encode
        try {
        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            encodeSubPackets(baos);
            baos.close();
            decrypted = baos.toByteArray();
            
        } catch (IOException ioe) {
            throw new RuntimeException(
                "IOException on ByteArrayOutputStream - "+ioe);
        }
        
        
        // init
        int blocksize = 0;
        int keysize = 0;
        Cipher cp = null;
        try {
            blocksize = factory.getCipherBlockSize(cipherid);
            keysize = factory.getCipherBlockSize(cipherid);
            if (mdc) {
                cp = factory.getCipherAlgorithm(cipherid, "CFB");
            } else {
                cp = factory.getCipherAlgorithm(cipherid, "OpenpgpCFB");
            }
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
            throw new InternalError("PANIC");
        }
        
        // convert the key to a keyspec
        StringTokenizer st = new StringTokenizer(cp.getAlgorithm(), "/");
        SecretKeySpec keyspec = new SecretKeySpec(key, st.nextToken());
    
        // set iv
        byte[] iv = new byte[blocksize];
        for (int j=0; j<iv.length; j++) { iv[j] = 0; }
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        // init the Cipher
        try {
            cp.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new InternalError("InvalidKeyException on "+
                                    "encrypting a key - "+ike);
        } catch (InvalidAlgorithmParameterException iape) {
            iape.printStackTrace();
            throw new InternalError(
                "InvalidAlgorithmParameterException "+
                "on encrypting a key - "+iape);
        }
        
        // prefix
        byte[] prefix = new byte[blocksize];
        byte[] repeat = new byte[2];
        sr.nextBytes(prefix);
        repeat[0] = prefix[prefix.length - 2];
        repeat[1] = prefix[prefix.length - 1];
        
        byte[] mdcsuffix = null;
        if (mdc) {

            try {

                mdcsuffix = new byte[22];
                mdcsuffix[0] = (byte)0xD0;
                mdcsuffix[1] = (byte)0x14;

                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(prefix);
                md.update(repeat);
                md.update(decrypted);
                
                byte[] mdcheader = { (byte)0xd0, (byte)0x14 };
                md.update(mdcheader);
                
                byte[] hash = md.digest();
                for (int i=0; i<20; i++) {
                    mdcsuffix[i+2] = hash[i];
                }
            } catch (NoSuchAlgorithmException nsae) {
                throw new RuntimeException("SHA-1 hash not found.");
            }
        }
        
        
        byte[] alldata;
        if (mdcsuffix == null)
            alldata = new byte[blocksize + 2 + decrypted.length];
        else
            alldata = new byte[blocksize + 2 + decrypted.length + 22];
            
        System.arraycopy(prefix,    0, alldata,           0,        blocksize);
        System.arraycopy(repeat,    0, alldata,   blocksize,                2);
        System.arraycopy(decrypted, 0, alldata, 2+blocksize, decrypted.length);
        if (mdcsuffix != null) 
            System.arraycopy(mdcsuffix, 0, alldata, alldata.length - 22, 22);
        
        try {

            encrypted = cp.doFinal(alldata);
        
        } catch (IllegalBlockSizeException ibse) {
            ibse.printStackTrace();
            throw new InternalError("Should not happen on CFB mode. "+ibse);
        } catch (BadPaddingException bpe) {
            bpe.printStackTrace();
            throw new InternalError("Should not happen on CFB mode. "+bpe);
        }
        
        
    }

// Read method
// ..........................................................................

    /**
     * Read the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *         packet. Note that when this exception is thrown recovery is not
     *         possible and the underlying inputstream cannot be used to read
     *         more packets from.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {
        
        if (mdc) {
            if (in.readByte() != 1) {
                in.readByteArray();
                throw new PGPDataFormatException("Invalid version for integrity "+
                    "protected encrypted data packet.");
            }
        }
        encrypted = in.readByteArray();
                        
    }
    

// Write method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        if (mdc) {
            out.writeByte((byte)1);   // mdc packet version
        }
        out.writeFully(encrypted);
        
    }


    
}
