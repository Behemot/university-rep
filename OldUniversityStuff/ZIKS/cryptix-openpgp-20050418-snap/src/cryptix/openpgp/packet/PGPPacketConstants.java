/* $Id: PGPPacketConstants.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


/**
 * Class containing constants for all packets defined by OpenPGP and some
 * others used by various implementations.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */
public class PGPPacketConstants {

// Constructor
//.............................................................................

    private PGPPacketConstants() {}   // static vars only, no instance


// Constants
//.............................................................................


    /** Packet ID for a public key encrypted session key. */
    public static final byte PKT_PUBLIC_KEY_ENCRYPTED_SESSION_KEY    =  1;

    /** Packet ID for a signature. */
    public static final byte PKT_SIGNATURE                           =  2;

    /** Packet ID for a symmetric key encrypted session key. */
    public static final byte PKT_SYMMETRIC_KEY_ENCRYPTED_SESSION_KEY =  3;

    /** Packet ID for a one pass signature. */
    public static final byte PKT_ONE_PASS_SIGNATURE                  =  4;

    /** Packet ID for a secret key. */
    public static final byte PKT_SECRET_KEY                          =  5;

    /** Packet ID for a public key. */
    public static final byte PKT_PUBLIC_KEY                          =  6;

    /** Packet ID for a secret subkey. */
    public static final byte PKT_SECRET_SUBKEY                       =  7;

    /** Packet ID for compressed data. */
    public static final byte PKT_COMPRESSED_DATA                     =  8;

    /** Packet ID for symmetrically encrypted data. */
    public static final byte PKT_SYMMETRICALLY_ENCRYPTED_DATA        =  9;

    /** Packet ID for a marker. */
    public static final byte PKT_MARKER                              = 10;

    /** Packet ID for literal data. */
    public static final byte PKT_LITERAL_DATA                        = 11;

    /** Packet ID for a trust packet. */
    public static final byte PKT_TRUST                               = 12;

    /** Packet ID for a userid. */
    public static final byte PKT_USERID                              = 13;

    /** Packet ID for a public subkey. */
    public static final byte PKT_PUBLIC_SUBKEY                       = 14;

    /** Packet ID for symmetrically encrypted integrity protected data. */
    public static final byte PKT_SYMMETRICALLY_ENCRYPTED_INTEGRITY_PROTECTED_DATA = 15;

    /** Packet ID for a modification detection code. */
    public static final byte PKT_MODIFICATION_DETECTION_CODE         = 16;

    /** 
     * Packet ID for a photographic user id. 
     *
     * <p>Note: this packet id is not part of the OpenPGP standard, but it is
     * a proprietary packet used by NAI's PGP 6.0 and later.</p>
     */
    public static final byte PKT_PHOTOID                             = 17;

}
