/* $Id: PGPSecretKeyPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


/**
 * Packet representing a secret (signing only or both encrypt and sign) key
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPSecretKeyPacket extends PGPKeyPacket {


// Constructor
//...........................................................................

    /**
     * Empty constructor
     */
    public PGPSecretKeyPacket() {}

    

    /**
     * Clone helper constructor
     */
    private PGPSecretKeyPacket(PGPSecretKeyPacket toClone) 
    {
        super(toClone);
    }
    


// Clone method
// ..........................................................................

    /**
     * Returns a clone of this object.
     */
    public Object clone() {
        return new PGPSecretKeyPacket(this);
    }

    

// Decode method
// ..........................................................................

    /**
     * Decode the packet body
     *
     * @param in the inputstream to read from
     * @param factory the factory to use to interpret this packet
     * @throws IOException if the underlying inputstream throws it
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *         data format. Recoverable basically means that the pointer in the
     *         underlying inputstream points to the next packet, such that the
     *         application can continue with the next packet and ignore this
     *         one.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *         in the data format. If this happens then it is impossible to
     *         read more data from the inputstream.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {
        decodePublicData (in, factory);
        decodeSecretData (in);
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body
     *
     * @param out the outputstream to write to
     * @throws IOException if the underlying outputstream throws it
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        encodePublicData (out);
        encodeSecretData (out);
    }


}
