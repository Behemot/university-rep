/* $Id: PGPMarkerPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;

/**
 * The marker packet.
 *
 * <p>RFC 2440 says:</p>
 * <pre>5.8. Marker Packet (Obsolete Literal Packet) (Tag 10)
 *
 * An experimental version of PGP used this packet as the Literal
 * packet, but no released version of PGP generated Literal packets
 * with this tag. With PGP 5.x, this packet has been re-assigned and is
 * reserved for use as the Marker packet.
 *
 * The body of this packet consists of:
 *
 *   - The three octets 0x50, 0x47, 0x50 (which spell "PGP" in UTF-8).
 *
 * Such a packet MUST be ignored when received.  It may be placed at
 * the beginning of a message that uses features not available in PGP
 * 2.6.x in order to cause that version to report that newer software
 * is necessary to process the message.</pre>
 *
 * <p>This implementation ignores this packet completely. It extends from 
 * PGPDummyPacket. When read, it does not check if the content is correct
 * according to the above quote from RFC 2440. When you create a new packet
 * though, it will insure it is correct.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPMarkerPacket extends PGPDummyPacket {
    
    
    /** 
     * Empty constructor.
     *
     * <p>Initializes this packet with the default payload.</p>
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPMarkerPacket () {
        super();
        byte[] content = new byte[3];
        content[0] = 0x50;
        content[1] = 0x47;
        content[2] = 0x50;
        this.setPayload(content);
    }
    
}
