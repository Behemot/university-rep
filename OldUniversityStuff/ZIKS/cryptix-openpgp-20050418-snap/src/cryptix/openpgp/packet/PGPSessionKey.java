/* $Id: PGPSessionKey.java,v 1.3 2005/03/29 11:22:50 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


/**
 * Class representing a sessionkey and a symmetric key algorithm identifier
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPSessionKey {


// Instance variables
// ..........................................................................

    private byte[] bytes;
    private byte cipherid;
    

// Constructor
// ..........................................................................

    /**
     * Constructor taking a session key as a byte array and a cipher ID
     */
    public PGPSessionKey(byte[] bytes, byte cipherid) {
        
        this.bytes    = bytes;
        this.cipherid = cipherid;
        
    }
    

// Getters
// ..........................................................................

    /** 
     * Return the session key as a byte array
     */
    public byte[] getBytes() { return bytes; }
    
    /** 
     * Return the id of the cipher used to encrypt the 
     * PGPSymmetricallyEncryptedDataPacket
     */
    public byte getCipherID() { return cipherid; }
    
}
