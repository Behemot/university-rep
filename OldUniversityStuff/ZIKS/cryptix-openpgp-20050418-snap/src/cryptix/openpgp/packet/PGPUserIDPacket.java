/* $Id: PGPUserIDPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


/**
 * Packet containing a user ID
 *
 * <p>This packet contains a simple string that represents a user ID. This
 * string is usually formatted in a certain way (containing a name and an
 * e-mail address. This class however does not parse it, it deals with the
 * string as a whole.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPUserIDPacket extends PGPIDPacket {
    

// Constructor
//...........................................................................

    /**
     * Empty constructor
     */
    public PGPUserIDPacket () {}



// Instance variables
//...........................................................................

    /** The user ID this packet represents */
    private String userID;



// Getters / Setters
//...........................................................................

    /** Get the user ID string */
    public String getValue () { return userID; }
    /** Return a string representation of this user ID */
    public String toString () { return getValue(); }
    /** Set the user ID string */
    public void setValue (String userID) { this.userID = userID; }
    


// Compare method
//...........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
    
        if (ssp instanceof PGPUserIDPacket) {
        
            PGPUserIDPacket uip = (PGPUserIDPacket)ssp;
            
            return userID.equals(uip.getValue());
            
        } else {
        
            return false;
            
        }
            
    }



// Encode method
//...........................................................................

    /**
     * Encode the packet body
     *
     * @param out the outputstream to write to
     * @throws IOException if the underlying outputstream throws it
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        out.writeString(userID);
        
    }

    

// Decode method
//...........................................................................

    /**
     * Decode the packet body
     *
     * @param in the inputstream to read from
     * @param factory this param is unused by this class
     * @throws IOException if the underlying inputstream throws it
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *         data format. Recoverable basically means that the pointer in the
     *         underlying inputstream points to the next packet, such that the
     *         application can continue with the next packet and ignore this
     *         one.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *         in the data format. If this happens then it is impossible to
     *         read more data from the inputstream.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                                     PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {
        
        userID = in.readString();

    }

    
}
