/* $Id: PGPSignaturePacket.java,v 1.3 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPSigner;

import cryptix.openpgp.io.PGPByteArrayDataInputStream;
import cryptix.openpgp.io.PGPByteArrayDataOutputStream;
import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.signature.PGPSignatureSubPacket;
import cryptix.openpgp.signature.PGPSignatureSubPacketFactory;

import cryptix.openpgp.util.PGPCompare;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.security.MessageDigest;

import java.util.Vector;


/**
 * A packet representing a signature. 
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPSignaturePacket extends PGPPacket {


// Constructor
// ..........................................................................

    /**
     * Emtpy constructor
     */
    public PGPSignaturePacket () {}

    

// Instance variables
// ..........................................................................

    // Both V3 and V4
    private byte type, algoid, hashid, version;
    private byte[] sigdata, hashval;
    // V3 specific
    private byte[] time, keyid;
    // V4 specific
    private byte[] hashedbytes, unhashedbytes;
    private Vector hashed, unhashed;
    


// Getters & Setters
// ..........................................................................

    /**
     * Version 3 setData
     *
     * @param type type, as defined in PGPSignatureConstants
     * @param time 4 byte signing time
     * @param keyid 8 byte signer keyID
     * @param algoid public key algorithm id
     * @param hashid hash algorithm id
     */
    public void setData (byte type, byte[] time, byte[] keyid, byte algoid, 
                         byte hashid) {
        this.version = 3;
        this.type = type;
        this.time = time;
        this.keyid = keyid;
        this.algoid = algoid;
        this.hashid = hashid;
    }


    /**
     * Version 4 setData
     *
     * @param type type, as defined in PGPSignatureConstants
     * @param algoid public key algorithm id
     * @param hashid hash algorithm id
     * @param hashed vector containing hashed subpackets
     * @param unhashed vector containing unhashed subpackets
     */
    public void setData (byte type, byte algoid, byte hashid, 
                         Vector hashed, Vector unhashed) {

        this.version = 4;

        this.type = type;
        this.algoid = algoid;
        this.hashid = hashid;

        this.hashed = hashed;
        this.unhashed = unhashed;

        try {

            // encode hashed and unhashed data
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int i=0; i<hashed.size(); i++) {
                ((PGPSignatureSubPacket)hashed.elementAt(i)).encode(baos);
            }
            hashedbytes = baos.toByteArray();
        
            baos = new ByteArrayOutputStream();
            for (int i=0; i<unhashed.size(); i++) {
                ((PGPSignatureSubPacket)unhashed.elementAt(i)).encode(baos);
            }
            unhashedbytes = baos.toByteArray();

        } catch (IOException ioe) {
            throw new InternalError("IOException in ByteArrayOutputStream");
        }
        
    }
    
  
    /**
     * Version 3/4 set hash value for quick check
     *
     * Note: while only the left two bytes are used, feel free to pass in the
     * complete hashresult.
     *
     * @param hashval the result of the hash
     */
    public void setHash (byte[] hashval) {
        this.hashval = new byte[2];
        this.hashval[0] = hashval[0];
        this.hashval[1] = hashval[1];
    }
    

    /**
     * Version 3/4 get signature type
     *
     * See PGPSignatureConstants for possible values
     */
    public byte getType() { return type; }


    /**
     * Version 3/4 get public key algorithm id
     *
     * See PGPAlgorithmConstants for possible values
     */
    public byte getAlgoID() { return algoid; }


    /**
     * Version 3/4 get hash algorithm id
     *
     * See PGPAlgorithmConstants for possible values
     */
    public byte getHashID() { return hashid; }


    /**
     * Get version
     *
     * Currently supported are version 3 (old) and version 4 (new).
     */
    public byte getVersion() { return version; }


    /**
     * Version 3/4 get hash value for quick check
     *
     * @returns a byte array of two bytes containing the left two bytes of the
     *          hash output
     */
    public byte[] getHash() { return hashval; }
    

    /**
     * Version 3 Get signing time
     *
     * Note: for version 4 signatures, this field is encoded as a
     * SignatureSubPacket
     */
    public byte[] getTime() { return time; }


    /**
     * Version 3 Get key ID of signer
     *
     * Note: for version 4 signatures, this field is encoded as a
     * SignatureSubPacket
     */
    public byte[] getKeyID() { return keyid; }
    
    
    /**
     * Version 4 Return all hashed subpackets
     */
    public Vector getHashedSubPackets()         { 
        if (hashed == null) throw new IllegalStateException(
                                         "Call parseSignatureSubPackets first");
        return hashed; 
    }


    /**
     * Version 4 Set hashed subpackets
     */
    public void setHashedSubPackets(Vector x)   { 
    
        this.hashed = x; 

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            for (int i=0; i<hashed.size(); i++) {
                ((PGPSignatureSubPacket)hashed.elementAt(i)).encode(baos);
            }
        } catch (IOException ioe) {
            throw new InternalError("IOException on encoding signature "+
                                    "subpackets - "+ioe);
        }

        hashedbytes = baos.toByteArray();
        if (hashedbytes.length > 65535) {
            throw new IllegalArgumentException("Encoded signature subpackets "+
                                               "too large (> 65535 bytes)");
        }
        
    }
    

    /**
     * Version 4 Return all unhashed subpackets
     */
    public Vector getUnhashedSubPackets()       { 
        if (unhashed == null) throw new IllegalStateException(
                                         "Call parseSignatureSubPackets first");
        return unhashed; 
    }


    /**
     * Version 4 Set unhashed subpackets
     */
    public void setUnhashedSubPackets(Vector x) { 
    
        this.unhashed = x; 

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            for (int i=0; i<unhashed.size(); i++) {
                ((PGPSignatureSubPacket)unhashed.elementAt(i)).encode(baos);
            }
        } catch (IOException ioe) {
            throw new InternalError("IOException on encoding signature "+
                                    "subpackets - "+ioe);
        }

        unhashedbytes = baos.toByteArray();
        if (unhashedbytes.length > 65535) {
            throw new IllegalArgumentException("Encoded signature subpackets "+
                                               "too large (> 65535 bytes)");
        }
        
    }


    /**
     * Version 4 Return all subpackets, both hashed and unhashed
     */
    public Vector getAllSubPackets() { 

        if (hashed == null) throw new IllegalStateException(
                                         "Call parseSignatureSubPackets first");
        if (unhashed == null) throw new IllegalStateException(
                                         "Call parseSignatureSubPackets first");

        Vector x = new Vector(hashed.size() + unhashed.size());
        for (int i=0; i<hashed.size(); i++) {
            x.addElement(hashed.elementAt(i));
        }
        for (int i=0; i<unhashed.size(); i++) {
            x.addElement(unhashed.elementAt(i));
        }
        return x;
    }
    



// Methods from java.lang.Object
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
    
        if (ssp instanceof PGPSignaturePacket) {
        
            PGPSignaturePacket that = (PGPSignaturePacket)ssp;
            
            boolean equal = PGPCompare.equals(this.type, that.type) 
                         && PGPCompare.equals(this.algoid, that.algoid)
                         && PGPCompare.equals(this.hashid, that.hashid)
                         && PGPCompare.equals(this.version, that.version)
                         && PGPCompare.equals(this.sigdata, that.sigdata)
                         && PGPCompare.equals(this.hashval, that.hashval);
            if (this.version == 3) {
                equal = equal 
                         && PGPCompare.equals(this.time, that.time)
                         && PGPCompare.equals(this.keyid, that.keyid);
            } else { //version 4
                equal = equal 
                         && PGPCompare.equals(this.hashedbytes,
                                              that.hashedbytes)
                         && PGPCompare.equals(this.unhashedbytes,
                                              that.unhashedbytes);
            }
            
            return equal;
                         
        } else {
        
            return false;
            
        }
            
    }
    
            
    
// Methods from PGPPacket
// ..........................................................................

    /**
     * Return the forced length type for old style packets.
     *
     * <p>
     * For V3 signature packets, it returns 1, which means that they will be
     * packed with a two byte length instead of a one byte length. This is for
     * PGP 2.6 compatibility. 
     * </p>
     */
    public int getForceLengthType() {
        if (version <= 3) return 1;
        return -1;
    }



// Encode method
// ..........................................................................

    /**
     * Encode the packet body
     *
     * @param out the outputstream to write to
     * @throws IOException if the underlying outputstream throws it
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {

        if (version == 4) {
        
            out.writeByte((byte)4);         // version number

            out.writeByte((byte)type);      // signature type
            out.writeByte((byte)algoid);    // public key algorithm used
            out.writeByte((byte)hashid);    // hash algorithm used

            out.writeShort((short)hashedbytes.length);
            out.writeFully(hashedbytes);

            out.writeShort((short)unhashedbytes.length);
            out.writeFully(unhashedbytes);  

            for (int i=0; i<2; i++) {       // left two bytes of hash result
                out.writeByte((byte)hashval[i]);
            }
        
            out.writeFully(sigdata);

        } else { // version == 3

            out.writeByte((byte)3);         // version number
            out.writeByte((byte)5);         // 5 bytes of hashed material
            out.writeByte((byte)type);      // signature type

            for (int i=0; i<4; i++) { // sign time
                out.writeByte((byte)time[i]);
            }

            for (int i=0; i<8; i++) { // key ID of signer
                out.writeByte((byte)keyid[i]);
            }

            out.writeByte((byte)algoid);    // public key algorithm used
            out.writeByte((byte)hashid);    // hash algorithm used

            for (int i=0; i<2; i++) {       // left two bytes of hash result
                out.writeByte((byte)hashval[i]);
            }

            out.writeFully(sigdata);
        
        }
        
    }
    


// Decode method
// ..........................................................................

    /**
     * Decode the packet body
     *
     * @param in the inputstream to read from
     * @param factory the factory to use to interpret this packet
     * @throws IOException if the underlying inputstream throws it
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *         data format. Recoverable basically means that the pointer in the
     *         underlying inputstream points to the next packet, such that the
     *         application can continue with the next packet and ignore this
     *         one.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *         in the data format. If this happens then it is impossible to
     *         read more data from the inputstream.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {
        byte temp;
        
        version = in.readByte();     // version number, ### should be V3 for now

        if (version == 3) { 
        
            temp = in.readByte();     // #bytes of hashed material, should be 5
            if (temp != 5) { 
                throw new RuntimeException("There should be 5 bytes of hashed "+
                                           " material");
            }
        
            type = in.readByte();     // signature type
        
            time = new byte[4];
            for (int i=0; i<4; i++) { // sign time
                time[i] = in.readByte();
            }
            keyid = new byte[8];
            for (int i=0; i<8; i++) { // key ID of signer
                keyid[i] = in.readByte();
            }
            algoid = in.readByte();   // public key algorithm used
            hashid = in.readByte();   // hash algorithm used

            hashval = new byte[2];
            for (int i=0; i<2; i++) { // left two bytes of signature
                hashval[i] = in.readByte();
            }
        
            sigdata = in.readByteArray();
            
        } else if (version == 4) {
        
            type = in.readByte();     // signature type
            algoid = in.readByte();   // public key algorithm used
            hashid = in.readByte();   // hash algorithm used
            
            int len = in.readUnsignedShort();  // length of hashed data
            hashedbytes = new byte[len];
            in.readFully(hashedbytes);
            
            len = in.readUnsignedShort();  // length of unhashed data
            unhashedbytes = new byte[len];
            in.readFully(unhashedbytes);
            
            hashval = new byte[2];
            for (int i=0; i<2; i++) { // left two bytes of signed hash value
                hashval[i] = in.readByte();
            }

            sigdata = in.readByteArray();
            
        } else {
            
            in.readByteArray(); // read all remaining data in this packet
            throw new PGPDataFormatException("Unsupported version ("+version+
                                             ") for signatures.");
            
        }

    }
  


// SignatureSubPacket parse methods
// ..........................................................................


    /**
     * Parse the SignatureSubPackets using the default 
     * PGPSignatureSubPacketFactory
     */
    public void parseSignatureSubPackets()
        throws PGPDataFormatException, PGPFatalDataFormatException
    {
    
        parseSignatureSubPackets (
            PGPSignatureSubPacketFactory.getDefaultInstance()
        );

    }


    /**
     * Parse the SignatureSubPackets using a given
     * PGPSignatureSubPacketFactory
     */
    public void parseSignatureSubPackets(PGPSignatureSubPacketFactory factory) 
        throws PGPDataFormatException, PGPFatalDataFormatException
    {

        if (this.version == 3) {
            throw new IllegalStateException("Version 3 signatures have no "+
                                            "SignatureSubPackets.");
        }

        try {

            hashed = new Vector();
            unhashed = new Vector();

            ByteArrayInputStream bais;

            bais = new ByteArrayInputStream(hashedbytes);

            while (bais.available() > 0) {
                PGPSignatureSubPacket ssp = factory.readPacket(bais);
                hashed.addElement(ssp);
            }

            bais = new ByteArrayInputStream(unhashedbytes);

            while (bais.available() > 0) {
                PGPSignatureSubPacket ssp = factory.readPacket(bais);
                unhashed.addElement(ssp);
            }

        } catch (IOException ioe) {
            throw new InternalError("PANIC: IOEx. on ByteArrayInputStream");
        }
        
    }
    

// Other methods
// ..........................................................................

    /**
     * Interpret the signature
     *
     * <p>This method passes the signature data in the PGPSigner object. The
     * application can then use the methods in that class to verify the
     * signature.</p>
     *
     * @param signer a PGPSigner object that contains the public key part of the
     *               key that signed this message.
     */
    public void interpretSignature (PGPSigner signer) 
        throws IOException, PGPDataFormatException
    {
      
        PGPByteArrayDataInputStream badis = 
                                    new PGPByteArrayDataInputStream(sigdata);

        try {

            signer.decodeSignatureData (badis);
            badis.close();
        
        } catch (PGPFatalDataFormatException dfe) {
            throw new InternalError("PGPByteArrayDataInputStream should not "+
                                    "throw a PGPFatalDataFormatException.");
        }
        
    }
    
    
    /**
     * Set the signature data
     *
     * <p>This method extracts the signature data from the PGPSigner object and
     * retains.</p>
     *
     * @param signer a PGPSigner object that contains a signature.
     *
     * @throws IllegalArgumentException if signer does not contains a signature
     */
    public void setSignature(PGPSigner signer) {

        try {

            PGPByteArrayDataOutputStream bados = 
                                            new PGPByteArrayDataOutputStream();
            signer.encodeSignatureData(bados);
            bados.close();
            sigdata = bados.toByteArray();

        } catch (IOException ioe) {
            throw new InternalError("IOException in ByteArrayOutputStream");
        } catch (NullPointerException npe) {
            throw new IllegalArgumentException("Signer did not contains a" +
                                               "signature.");
        }
    }
    

    /**
     * Hash the signature specific data
     *
     * <p>This method calls the update methods of both the MessageDigest and
     * PGPSigner objects with all data that has to be hashed for this 
     * signature packet, depending on the version.</p>
     *
     * <p>Unfortunately double hashing is necessairy, because Sun does not
     * allow applications to pass a MessageDigest object directly into a
     * signature.</p>
     */
    public int hashData(MessageDigest md, PGPSigner signer) {
    
        if (version == 4) {

            byte[] pre = new byte[6];

            pre[0] = version;
            pre[1] = type;
            pre[2] = algoid;
            pre[3] = hashid;
            pre[4] = (byte)((hashedbytes.length >> 8) & 0xFF);
            pre[5] = (byte)((hashedbytes.length     ) & 0xFF);
        
            md.update(pre);
            signer.update(pre);
            md.update(hashedbytes);
            signer.update(hashedbytes);
        
            return pre.length + hashedbytes.length;
        
        } else if (version == 3) {
        
            byte[] typebytes = new byte[1];
            typebytes[0] = type;
            
            md.update(typebytes);
            signer.update(typebytes);
            md.update(time);
            signer.update(time);
            
            return 5;
        
        } else {
        
            throw new IllegalStateException("Version should be 3 or 4");
            
        }
        
    }

}
