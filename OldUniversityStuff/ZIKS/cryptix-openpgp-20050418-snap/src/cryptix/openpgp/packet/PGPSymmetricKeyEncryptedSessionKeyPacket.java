/* $Id: PGPSymmetricKeyEncryptedSessionKeyPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPStringToKey;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;


/**
 * This class represents a session key.
 *
 * <p>This session key is either encrypted with a passphrase or this packet
 * specifies that the passphrase is used directly as the session key.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPSymmetricKeyEncryptedSessionKeyPacket 
    extends PGPSessionKeyPacket 
{
    
// Instance variables
// ..........................................................................

    private byte   version, datacipher;
    private byte[] encrypted, sessionkey;
    private boolean decrypted = false;


// Constructor
// ..........................................................................

    /**
     * Emtpy constructor
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPSymmetricKeyEncryptedSessionKeyPacket () {}

    
    
// Implementation of PGPSessionKeyPacket methods
// ..........................................................................

    /**
     * Return the session key contained within this packet.
     *
     * @throws IllegalStateException when the decrypt method has not been 
     *         (successfully) called yet.
     */
    public PGPSessionKey getSessionKey() {
    
        if (decrypted) {
            
            return new PGPSessionKey(sessionkey, datacipher);
            
        } else {
            throw new IllegalStateException("Data not decrypted yet.");
        }
        
    }


// Crypto
// ..........................................................................


    public void setEncrypted(byte[] sessionkey, String passphrase,
        PGPAlgorithmFactory factory, SecureRandom sr, 
        byte datacipherid, byte keycipherid, byte s2kid, byte hashid)
    {
        try {

            // the only currently defined version is 4
            version      = 4;
    
            // create a place to save the output
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
    
            // write algorithm id's
            dos.writeByte(keycipherid);
            dos.writeByte(s2kid);
            dos.writeByte(hashid);
            
            // init algorithms
            PGPStringToKey s2k;
            MessageDigest md;
            Cipher cp;
            try {
                s2k = factory.getS2KAlgorithm(s2kid);
                md = factory.getHashAlgorithm(hashid);
                cp = factory.getCipherAlgorithm(keycipherid, "CFB");
            } catch (NoSuchAlgorithmException nsae) {
                throw new IllegalArgumentException("id not found - "+nsae);
            }
            
            // check for salted s2k algorithm
            if (! s2k.isSalted()) {
                throw new IllegalArgumentException("Chosen S2K algorithm is "+
                                                   "not salted.");
            }
    
            // get sizes
            int dataciphersize, keyciphersize, blocksize;
            try {
                dataciphersize = factory.getCipherKeySize(datacipherid);
                keyciphersize = factory.getCipherKeySize(keycipherid);
                blocksize = factory.getCipherBlockSize(keycipherid);
            } catch (NoSuchAlgorithmException nsae) {
                throw new IllegalArgumentException("id not found - "+nsae);
            }
    
            // get the hashed passphrase, this will be the key used to encrypt a
            // random sesison key
            byte[] passbytes = passphrase.getBytes("UTF-8");
            byte[] encryptkey = s2k.generateAndHash(passbytes, md,
                                                    keyciphersize, dos, sr);
    
            // use an all-zero iv
            byte[] iv = new byte[blocksize];
            for (int i=0; i<iv.length; i++) iv[i] = 0;
            
            // generate a random session key
            sessionkey = new byte[dataciphersize];
            sr.nextBytes(sessionkey);
            
            // prepend the random session key with an id specifying the cipher
            // used to encrypt the following data
            byte[] data = new byte[sessionkey.length + 1];
            data[0] = datacipherid;
            System.arraycopy(sessionkey, 0, data, 1, sessionkey.length);
            
            // the real work
            byte[] crypted = PGPCryptHelper.crypt(true, cp, iv, encryptkey, data);
    
            // save the output
            dos.write(crypted);
            dos.close(); baos.close();
            encrypted  = baos.toByteArray();

        } catch (IOException ioe) {
        
            throw new InternalError("IOException on bytearray");
            
        }
    }


    public void setDirect(String passphrase, PGPAlgorithmFactory factory, 
        SecureRandom sr, byte datacipherid, byte s2kid, byte hashid)
    {
        try {
        
            // the only currently defined version is 4
            version      = 4;
    
            // create a place to save the output
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
    
            // write algorithm id's
            dos.writeByte(datacipherid);
            dos.writeByte(s2kid);
            dos.writeByte(hashid);
            
            // init algorithms
            PGPStringToKey s2k;
            MessageDigest md;
            int size;
            try {
                s2k = factory.getS2KAlgorithm(s2kid);
                md = factory.getHashAlgorithm(hashid);
                size = factory.getCipherKeySize(datacipherid);
            } catch (NoSuchAlgorithmException nsae) {
                throw new IllegalArgumentException("id not found - "+nsae);
            }
    
            // get the hashed passphrase and save the rest of the s2k specifier
            // in the output
            byte[] passbytes = passphrase.getBytes("UTF-8");
            sessionkey = s2k.generateAndHash(passbytes, md, size, dos, sr);
    
            // save the output
            dos.close(); baos.close();
            encrypted  = baos.toByteArray();

        } catch (IOException ioe) {
        
            throw new InternalError("IOException on bytearray");
            
        }
    }


    public void decrypt(String passphrase, PGPAlgorithmFactory factory) 
        throws PGPDataFormatException
    {

        try {
            
            ByteArrayInputStream bais = new ByteArrayInputStream(encrypted);
            DataInputStream dis = new DataInputStream(bais);
            
            byte cipherid = dis.readByte();
            byte s2kid    = dis.readByte();
            byte hashid   = dis.readByte();
            
            // init all algorithms
            PGPStringToKey s2k;
            MessageDigest md;
            int size, blocksize;
            try {
                s2k       = factory.getS2KAlgorithm(s2kid);
                md        = factory.getHashAlgorithm(hashid);
                size      = factory.getCipherKeySize(cipherid);
                blocksize = factory.getCipherBlockSize(cipherid);
            } catch (NoSuchAlgorithmException nsae) {
                throw new PGPDataFormatException(nsae.toString());
            }

            // get the hashed passphrase
            byte[] passbytes = passphrase.getBytes("UTF-8");
            byte[] key = s2k.readAndHash(passbytes, md, size, dis);
            
            // check for availability of more bytes, which indicates if the 
            // obtained key should be used directly or to decrypt a session jey
            if (dis.available() == 0) {
                
                sessionkey = key;
                datacipher = cipherid;
                
            } else {

                // use an all-zero iv
                byte[] iv = new byte[blocksize];
                for (int i=0; i<iv.length; i++) iv[i] = 0;
                
                // read all data
                byte[] data = new byte[dis.available()];
                dis.readFully(data);
    
                // init cipher object        
                Cipher cp;
                try {
                    cp = factory.getCipherAlgorithm(cipherid, "CFB");
                } catch (NoSuchAlgorithmException nsae) {
                    throw new PGPDataFormatException(nsae.toString());
                }
                
                // the real thing
                byte[] result = PGPCryptHelper.crypt(false, cp, iv, key, data);
                
                // dissect the results
                datacipher = result[0];
                sessionkey = new byte[result.length-1];
                System.arraycopy(result,1,sessionkey,0,sessionkey.length);
                
            }
            
            // clean up
            dis.close(); bais.close();
            decrypted = true;

        } catch (IOException ioe) {
        
            throw new InternalError("IOException on bytearray");
            
        }
    }


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPSymmetricKeyEncryptedSessionKeyPacket)) {
            return false;
        } else {
            PGPSymmetricKeyEncryptedSessionKeyPacket that =
                (PGPSymmetricKeyEncryptedSessionKeyPacket) ssp;
            return (this.version == that.version) &&
                   (this.datacipher == that.datacipher) &&
                   PGPCompare.equals(this.encrypted,  that.encrypted) &&
                   PGPCompare.equals(this.sessionkey, that.sessionkey);
        }
        
    }
    


// Read method
// ..........................................................................

    /**
     * Read the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *         packet. Note that when this exception is thrown recovery is not
     *         possible and the underlying inputstream cannot be used to read
     *         more packets from.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {

        version = in.readByte();
        if (version != 4) {
            in.readByteArray(); // read all remaining bytes
            throw new PGPDataFormatException("Unknown version for "+
                "symmetrically encrypted session key packet.");
        }
        
        // read all remaining bytes at once
        encrypted = in.readByteArray();
                        
    }



// Write method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        if (encrypted == null) {
            throw new IllegalStateException("Session key not initialized or "+
                                            "not encrypted yet.");
        }
        
        out.setLength(encrypted.length+1);
        out.writeByte(version);
        out.writeFully(encrypted);
        
    }


    
}
