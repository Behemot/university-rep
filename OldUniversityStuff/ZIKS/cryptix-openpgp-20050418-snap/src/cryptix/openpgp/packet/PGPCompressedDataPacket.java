/* $Id: PGPCompressedDataPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.io.PGPCompressorInputStream;
import cryptix.openpgp.io.PGPCompressorOutputStream;
import cryptix.openpgp.io.PGPInputStreamAdapter;
import cryptix.openpgp.io.PGPOutputStreamAdapter;
import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;
import cryptix.openpgp.io.PGPZLibInputStreamAdapter;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPCompressor;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.security.NoSuchAlgorithmException;


/**
 * "The Compressed Data packet contains compressed data. Typically,
 * this packet is found as the contents of an encrypted packet, or
 * following a Signature or One-Pass Signature packet, and contains
 * literal data packets." -- RFC2440
 *
 * <p>To use this class for encoding a packet, you must set the 
 * compression algorithm ID manually via the setAlgorithmID method.
 *
 *
 * @see cryptix.openpgp.packet.PGPPacketFactory
 *
 * @author Mathias Kolehmainen (ripper@roomfullaspies.com)
 * @version $Revision: 1.2 $
 */
public class PGPCompressedDataPacket extends PGPContainerPacket {
    

// Instance variables
// ..........................................................................

  private int compAlgId;



// Constructor
// ..........................................................................


  /** 
   * Default constructor does nothing.  WARNING: A call must be made
   * to setAlgorithmID for this class to encode correctly.  
   */
  public PGPCompressedDataPacket() { }



// Getters/Setters
// ..........................................................................


  /** 
   * Set the id of the compression algorithm.  The id must be registerd
   * with the factory and fit into a byte.
   *
   * @param id the compression algorithm identifier
   *
   * @see cryptix.openpgp.algorithm.PGPAlgorithmFactory
   * @see cryptix.openpgp.algorithm.PGPConstants
   */
  public void setAlgorithmID(int id) {
    // At the very least, it has got to fit in a byte.
    if ((id > 255) || (id < 0)) {
      throw(new IllegalArgumentException("Illegal algorithm id value: " + id));
    }
    compAlgId = id;
  }


  /** 
   * Get the id of the compression algorithm.
   *
   * @return the compression algorithm identifier
   *
   * @see cryptix.openpgp.algorithm.PGPAlgorithmFactory
   * @see cryptix.openpgp.algorithm.PGPConstants
   */
  public int getAlgorithmID() {
    return(compAlgId);
  }

      
// Decode methods
// ..........................................................................

  /**
   * Decode the compressed packet by uncompressing the data from the
   * stream and then picking off the sub packets and storing them in
   * our container.
   *
   * @throws IOException if something goes wrong with the input stream.
   * @throws PGPDataFormatException if invalid data is detected in the packet
   *                                and recovery is possible.
   * @throws PGPFatalDataFormatException if invalid data is detected in the
   *                                     packet and recovery is not possible.
   *
   * @param in The inputstream that contains the data
   * @param factory The algorithm factory subclasses can use to get their
   *                algorithm specific classes from.
   */
  public void decodeBody (PGPPacketDataInputStream in, 
                          PGPAlgorithmFactory factory) 
    throws IOException, PGPFatalDataFormatException, PGPDataFormatException
  {

    int alg = (int) in.readUnsignedByte();

    PGPCompressorInputStream expander;
    try {
        PGPCompressor cp = factory.getCompressionAlgorithm(alg);
        if (cp.needsDummy())
            expander = cp.getExpansionStream(new PGPZLibInputStreamAdapter(in));
        else
            expander = cp.getExpansionStream(new PGPInputStreamAdapter(in));
    } catch (NoSuchAlgorithmException nsae) {
        in.readByteArray();
        throw new PGPDataFormatException("id not found - "+nsae);
    }
    

    // read all the packets out of the expander stream...
    decodeSubPackets(expander, factory);
  }



// Write methods
// ..........................................................................

  
  /**
   * Encode the packet body by concatenating all our contained
   * packets, and then compressing them.  Note that a call must have
   * been previously made to setAlgorithmID.  It is the callers
   * responssibilty to close() the stream.
   *
   * @param out the output stream
   * @see #setAlgorithmID(int)
   * @exception IOException if something goes wrong with the output stream.  
   */
  public void encodeBody (PGPPacketDataOutputStream out) 
    throws IOException
  {
    int alg = getAlgorithmID();
    out.writeByte((byte)alg);

    PGPCompressorOutputStream compressor;
    try {
        // ### FIXME: Should not use default factory here
        PGPCompressor cp = PGPAlgorithmFactory.getDefaultInstance().getCompressionAlgorithm(alg);
        compressor = cp.getCompressionStream(new PGPOutputStreamAdapter(out));
    } catch (NoSuchAlgorithmException nsae) {
        throw new IllegalArgumentException("id not found - "+nsae);
    }

    // write all the packets into the compressor stream
    super.encodeSubPackets(compressor);

    compressor.finish();
  }
    
    
}


