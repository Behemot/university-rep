/* $Id: PGPOnePassSignaturePacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;


/**
 * A class representing a one-pass signature packet.
 *
 * <p>
 * This packet both has a binary and a plaintext form. While RFC 2440 only
 * lists the binary form of this packet, we also support the 'Hash:' armour
 * header key in clearsigned messages. 
 * </p><p>
 * Because the plaintext form contains less information than the binary form,
 * not all get methods will work with the plaintext form, in fact only the
 * getHashID and the isNested method work. Fortunately these two are also the
 * only useful ones for one pass signatures, so applications are encouraged not
 * to depend on the other information contained in a binary one-pass signature
 * packet.
 * </p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPOnePassSignaturePacket extends PGPPacket {


// Constructors
// ..........................................................................

    /**
     * Emtpy constructor for binary one pass signature packets
     *
     * This constructor is used by PGPPacketFactory, which calls the 
     * decode method to decode a binary one pass signature packet
     */
    public PGPOnePassSignaturePacket () {}

    
    /**
     * Constructor for plain-text one pass signature packets
     *
     * @param id contains the string identifier of the hash algorithm used in
     *        the signature, e.g. "SHA-1" or "MD5"
     * @param nested true if this packet is followed by another one pass
     *        signature packet, false otherwise
     */
    public PGPOnePassSignaturePacket (String id, boolean nested) {
        this(id, nested, PGPAlgorithmFactory.getDefaultInstance());
    }

    
    /**
     * Constructor for plain-text one pass signature packets using a specific
     * PGPAlgorithmFactory
     *
     * @param id contains the string identifier of the hash algorithm used in
     *        the signature, e.g. "SHA-1" or "MD5"
     * @param nested true if this packet is followed by another one pass
     *        signature packet, false otherwise
     * @param factory the algorithmfactory which will be used to interpret the
     *        id string 
     */
    public PGPOnePassSignaturePacket (String id, boolean nested,
                                      PGPAlgorithmFactory factory) 
    {
        this.version = -1;
        this.nested = nested;
        this.hashid = (byte)(factory.getHashAlgorithmIDFromTextName(id));
    }
    
    
    /**
     * Constructor for binary one pass signature packets
     *
     * @param type the signature type
     * @param hashid the id of the hash algorithm
     * @param algid the id of the public key algorithm
     * @param keyid the 8 byte keyid of the signing key
     * @param nested true if this packet is followed by another one pass
     *        signature packet, false otherwise
     */
    public PGPOnePassSignaturePacket (byte type, byte hashid, byte algid,
                                      byte[] keyid, boolean nested)
    {
        this.version = 3;
        this.type    = type;
        this.hashid  = hashid;
        this.algid   = algid;
        this.keyid   = keyid;
        this.nested  = nested;
    }

    

// Instance variables
// ..........................................................................

    private byte version, type, hashid, algid;
    private byte[] keyid;
    private boolean nested;



// Getters & Setters
// ..........................................................................

    /**
     * Return the id of the hash algorithm used in the signature
     *
     * <p>
     * A list of possible values can be found in 
     * cryptix.openpgp.algorithm.PGPConstants
     * </p>
     */
    public byte getHashID() {
        return hashid;
    }

    /**
     * Return the version number of this packet
     *
     * <p>
     * Note: this method does not work for a plaintext one-pass signature packet
     * </p>
     */
    public byte getVersion() {
        if (version == -1) throw new IllegalStateException("Plaintext sig");
        return version;
    }

    /**
     * Return the type of the signature
     *
     * <p>
     * A list of possible values can be found in 
     * cryptix.openpgp.packet.PGPSignatureConstants
     * </p><p>
     * Note: this method does not work for a plaintext one-pass signature packet
     * </p>
     */
    public byte getType() {
        if (version == -1) throw new IllegalStateException("Plaintext sig");
        return type;
    }

    /**
     * Return the id of the public key algorithm used in the signature
     *
     * <p>
     * A list of possible values can be found in 
     * cryptix.openpgp.algorithm.PGPConstants
     * </p><p>
     * Note: this method does not work for a plaintext one-pass signature packet
     * </p>
     */
    public byte getAlgorithmID() {
        if (version == -1) throw new IllegalStateException("Plaintext sig");
        return algid;
    }

    /**
     * Return the 8 byte / 64 bit key ID for this signing key
     *
     * <p>
     * Note: this method does not work for a plaintext one-pass signature packet
     * </p>
     */
    public byte[] getKeyID() {
        if (version == -1) throw new IllegalStateException("Plaintext sig");
        return keyid;
    }

    /**
     * Return if the message has multiple nested signatures
     *
     * <p>
     * RFC 2440 says about this:
     * </p><p>
     * [If this method returns true, it] indicates that the next packet is
     * another One-Pass Signature packet that describes another
     * signature to be applied to the same message data.
     * </p><p>
     * Note that if a message contains more than one one-pass signature,
     * then the signature packets bracket the message; that is, the first
     * signature packet after the message corresponds to the last one-pass
     * packet and the final signature packet corresponds to the first one-
     * pass packet.
     * </p>
     */
    public boolean isNested() {
        return nested;
    }



// Methods from java.lang.Object
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {

        if (ssp instanceof PGPOnePassSignaturePacket) {
        
            PGPOnePassSignaturePacket that = (PGPOnePassSignaturePacket)ssp;
            
            boolean equal = PGPCompare.equals(this.type, that.type) 
                         && PGPCompare.equals(this.algid, that.algid)
                         && PGPCompare.equals(this.hashid, that.hashid)
                         && PGPCompare.equals(this.version, that.version)
                         && PGPCompare.equals(this.keyid, that.keyid)
                         && PGPCompare.equals(this.nested, that.nested);

            return equal;
                         
        } else {
        
            return false;
            
        }

    }
    
            
    
// Encode method
// ..........................................................................

    /**
     * Encode the packet body
     *
     * @param out the outputstream to write to
     * @throws IOException if the underlying outputstream throws it
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        if (version == -1)
            throw new IllegalStateException("Cannot encode a plaintext " +
            "OnePassSignaturePacket in binary format.");
        
        out.writeByte(version);
        out.writeByte(type);
        out.writeByte(hashid);
        out.writeByte(algid);
        
        out.writeFully(keyid);
        
        out.writeBoolean(! nested);
        
    }
    


// Decode method
// ..........................................................................

    /**
     * Decode the packet body
     *
     * @param in the inputstream to read from
     * @param factory the factory to use to interpret this packet
     * @throws IOException if the underlying inputstream throws it
     * @throws PGPDataFormatException if a recoverable problem occurs in the
     *         data format. Recoverable basically means that the pointer in the
     *         underlying inputstream points to the next packet, such that the
     *         application can continue with the next packet and ignore this
     *         one.
     * @throws PGPFatalDataFormatException if a non-recoverable problem occurs
     *         in the data format. If this happens then it is impossible to
     *         read more data from the inputstream.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {

        version = in.readByte();

        if (version != 3) { // check for unsupported version
            in.readByteArray(); // read all remaining data in this packet
            throw new PGPDataFormatException("Unsupported version");
        }
        
        type   = in.readByte();
        hashid = in.readByte();
        algid  = in.readByte();
        
        keyid = new byte[8];
        in.readFully(keyid);
        
        nested = ! in.readBoolean();  // a zero (false) value means it is nested

    }
  
}
