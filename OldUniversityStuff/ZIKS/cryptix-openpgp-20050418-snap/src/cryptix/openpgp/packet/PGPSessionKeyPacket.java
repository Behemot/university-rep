/* $Id: PGPSessionKeyPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;

/**
 * This class represents an encrypted session key packet.
 *
 * <p>Subclasses will define the encryption method used.</p>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public abstract class PGPSessionKeyPacket extends PGPPacket {
    

    /**
     * Return the session key contained within this packet.
     */
    public abstract PGPSessionKey getSessionKey(); 


}
