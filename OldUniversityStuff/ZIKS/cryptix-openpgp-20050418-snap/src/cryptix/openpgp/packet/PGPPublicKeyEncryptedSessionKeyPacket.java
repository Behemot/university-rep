/* $Id: PGPPublicKeyEncryptedSessionKeyPacket.java,v 1.2 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPDecryptionFailedException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;
import cryptix.openpgp.algorithm.PGPEncryptor;

import cryptix.openpgp.io.PGPByteArrayDataInputStream;
import cryptix.openpgp.io.PGPByteArrayDataOutputStream;
import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


/**
 * This class represents a session key, encrypted with a public key.
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.2 $
 */

public class PGPPublicKeyEncryptedSessionKeyPacket 
    extends PGPSessionKeyPacket 
{
    
// Instance variables
// ..........................................................................

    private byte version, datacipher;
    private int pubalgid;
    private byte[] sessionkey, keyid, encrypted;
    private PGPEncryptor encryptor;
    private boolean decrypted = false;


// Constructor
// ..........................................................................

    /**
     * Emtpy constructor
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPPublicKeyEncryptedSessionKeyPacket () {}

    
    
// Implementation of PGPSessionKeyPacket methods
// ..........................................................................

    /**
     * Return the session key contained within this packet.
     *
     * @throws IllegalStateException when the decrypt method has not been 
     *         (successfully) called yet.
     */
    public PGPSessionKey getSessionKey() {
    
        if (decrypted) {
            
            return new PGPSessionKey(sessionkey, datacipher);
            
        } else {
            throw new IllegalStateException("Data not decrypted yet.");
        }
        
    }
    
    /**
     * Set the sessionkey and datacipher.
     */
    public void setSessionKey(byte[] sessionkey, byte datacipher) {
        this.sessionkey = sessionkey;
        this.datacipher = datacipher;
        this.version = 3;
    }


// Own getters and setters
// ..........................................................................

    /**
     * Return the key id.
     */
    public byte[] getKeyID()
    {
        return keyid;
    }
    
    /**
     * Set the key id.
     */
    public void setKeyID(byte[] keyid)
    {
        this.keyid = keyid;
    }
    
    /**
     * Get the public key algorithm id.
     */
    public int getPublicKeyAlgorithmID() 
    {
        return pubalgid;
    }
    
    /**
     * Set the public key algorithm id.
     */
    public void setPublicKeyAlgorithmID(int pubalgid)
    {
        this.pubalgid = pubalgid;
    }
    

// Crypto
// ..........................................................................

    public void encrypt(PGPEncryptor cryptor, SecureRandom sr)
    {

        byte[] data = new byte[sessionkey.length+3];

        data[0] = (byte)datacipher;
        System.arraycopy(sessionkey,0,data,1,sessionkey.length);

        int sum = 0;
        for (int i=1; i<data.length-2; i++) {
            sum += data[i] & 0xff;
        }
        data[data.length-2] = (byte)((sum >> 8) & 0xff);
        data[data.length-1] = (byte)( sum       & 0xff);

        cryptor.encrypt(data, sr);
        
        PGPByteArrayDataOutputStream bados = new PGPByteArrayDataOutputStream();
        
        try {
            
            cryptor.encodeEncryptedData(bados);
            bados.close();
            
        } catch (IOException ioe) {
            throw new InternalError("IOException on byte array output stream.");
        }
        
        encrypted = bados.toByteArray();

    }
    

    public void decrypt(PGPEncryptor cryptor) 
        throws PGPDataFormatException, PGPDecryptionFailedException
    {

        PGPByteArrayDataInputStream badis 
            = new PGPByteArrayDataInputStream(encrypted);

        try {

            cryptor.decodeEncryptedData(badis);
            badis.close();
        
        } catch (PGPFatalDataFormatException fdpe) {
            throw new PGPDataFormatException(fdpe.toString());
        } catch (IOException ioe) {
            throw new InternalError("IOException on byte array input stream.");
        }
        
        byte[] result = cryptor.decrypt();
        
        int sum = 0;
        for (int i=1; i<result.length-2; i++) {
            sum += result[i] & 0xff;
        }
        
        int expected = ((result[result.length-2] << 8) &0xff00)
                     +  (result[result.length-1] & 0x00ff);
        
        if (sum != expected) {
            throw new PGPDecryptionFailedException("Checksum mismatch");
        }
                
        datacipher = result[0];
        
        sessionkey = new byte[result.length-3];
        System.arraycopy(result,1,sessionkey,0,sessionkey.length);

        decrypted = true;

    }
    


// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPPublicKeyEncryptedSessionKeyPacket)) {
            return false;
        } else {
            PGPPublicKeyEncryptedSessionKeyPacket that =
                (PGPPublicKeyEncryptedSessionKeyPacket) ssp;
            return (this.version    == that.version) &&
                   (this.pubalgid   == that.pubalgid) &&
                   (this.datacipher == that.datacipher) &&
                   PGPCompare.equals(this.keyid,      that.keyid) &&
                   PGPCompare.equals(this.encrypted,  that.encrypted) &&
                   PGPCompare.equals(this.sessionkey, that.sessionkey);
        }
        
    }
    


// Read method
// ..........................................................................

    /**
     * Read the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *         packet. Note that when this exception is thrown recovery is not
     *         possible and the underlying inputstream cannot be used to read
     *         more packets from.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {

        version = in.readByte();
        if ((version != 3) && (version != 2)) {
            in.readByteArray(); // read all remaining bytes
            throw new PGPDataFormatException("Unknown version for "+
                "public key encrypted session key packet.");
        }
        
        keyid = new byte[8];
        in.readFully(keyid);

        pubalgid = in.readUnsignedByte();
        
        encrypted = in.readByteArray();
    }



// Write method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        out.setLength(encrypted.length+10);

        out.writeByte(version);
        out.writeFully(keyid);
        out.writeByte((byte)pubalgid);
        out.writeFully(encrypted);

    }


    
}
