/* $Id: PGPLiteralDataPacket.java,v 1.3 2005/03/13 17:46:37 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp.packet;


import cryptix.openpgp.PGPDataFormatException;
import cryptix.openpgp.PGPFatalDataFormatException;

import cryptix.openpgp.algorithm.PGPAlgorithmFactory;

import cryptix.openpgp.io.PGPPacketDataInputStream;
import cryptix.openpgp.io.PGPPacketDataOutputStream;

import cryptix.openpgp.util.PGPCompare;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.Date;


/**
 * <b>I'm currently implementing this class</b>
 *
 * @author Edwin Woudt (edwin@cryptix.org)
 * @version $Revision: 1.3 $
 */

public class PGPLiteralDataPacket extends PGPPacket {
    

// Constructor
// ..........................................................................

    /** 
     * Empty constructor.
     *
     * <p>Remember to use setPacketID to set the packet ID.</p>
     */
    public PGPLiteralDataPacket () {}
    


// Instance variables
// ..........................................................................

    private byte[] data;
    private String filename;
    private boolean binary;
    private boolean local;
    private long time;
    


// Getters/Setters
// ..........................................................................

    /**
     * Set a binary payload for this packet.
     */
    public void setData(byte[] data) { 
        if (data.length == 0) {
            throw new IllegalArgumentException("Array size == 0");
        }
        this.data = data; 
        binary = true;
    }


    /**
     * Set a text payload of this packet.
     */
    public void setData(String data) { 
        if (data.length() == 0) {
            throw new IllegalArgumentException("String size == 0");
        }
        try {
            this.data = data.getBytes("UTF-8"); 
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported - "+uee);
        }
        binary = false;
    }


    /**
     * Get the binary payload of this packet.
     *
     * <p>If text data is present, then this method will convert it to
     * binary data. (using UTF-8 encoding)</p>
     */
    public byte[] getBinaryData() { return data; }
    

    /**
     * Get the text payload of this packet.
     *
     * <p>If binary data is present, then this method will convert it to a 
     * String. (using UTF-8 encoding)</p>
     */
    public String getTextData() { 
        try {
            return new String(data, "UTF-8"); 
        } catch (UnsupportedEncodingException uee) {
            throw new InternalError("UTF-8 encoding not supported - "+uee);
        }
    }
    
    
    /**
     * Set the time.
     *
     * <p>The time is one of the following:<br>
     * - the last time the file was modified<br>
     * - the time the packet was created<br>
     * - or zero to indicate the current time</p>
     */
    public void setTime(long time) {
        this.time = time;
    }


    /**
     * Get the time.
     *
     * <p>The time is one of the following:<br>
     * - the last time the file was modified<br>
     * - the time the packet was created<br>
     * - or zero to indicate the current time</p>
     */
    public long getTime() {
        return time;
    }


    /**
     * Set the filename for the data in this packet.
     */
    public void setFileName(String filename) { 
        
        if (filename.length() > 255) {
            throw new IllegalArgumentException("Filename too long (>255 chars");
        }
        this.filename = filename; 
    }


    /**
     * Get the filename for the data in this packet.
     */
    public String getFileName() { return filename; }



// Format check methods
// ..........................................................................

    /**
     * Returns true if the contained data is in binary format
     *
     * <p>Note that this method also returns true for local format data.</p>
     */
    public boolean isBinary() { return binary; }


    /**
     * Returns true if the contained data is in text format
     */
    public boolean isText() { return ! (binary || local); }
    

    /**
     * Returns true if the contained data is in local format
     *
     * <p>This format is a deprecated format, that is only specified in RFC1991
     * and thus probably only used by old (PGP 2) clients.</p>
     * <p>Note that if this method returns true, then isBinary also returns
     * true.</p>
     */
    public boolean isLocal() { return local; }



// Compare method
// ..........................................................................

    /**
     * Compare this packet to another one.
     *
     * @return true if both are equal, false otherwise.
     */
    public boolean equals(Object ssp) {
        
        if (!(ssp instanceof PGPLiteralDataPacket)) {
            return false;
        } else {
            PGPLiteralDataPacket that = (PGPLiteralDataPacket) ssp;
            return PGPCompare.equals(this.getPacketID(), that.getPacketID()) &&
                   PGPCompare.equals(this.getBinaryData(),
                                     that.getBinaryData()) &&
                   PGPCompare.equals(this.isBinary(), that.isBinary());
        }
        
    }
    
    

// Read method
// ..........................................................................

    /**
     * Read the packet body.
     *
     * @throws IOException if something goes wrong with the input stream.
     * @throws PGPDataFormatException if invalid data is detected in the packet.
     *         Note that when this exception is thrown the complete packet will
     *         have been read, such that the next packet can potentially be
     *         parsed without any trouble.
     * @throws PGPFatalDataFormatException if invalid data is detected in the
     *         packet. Note that when this exception is thrown recovery is not
     *         possible and the underlying inputstream cannot be used to read
     *         more packets from.
     */
    public void decodeBody (PGPPacketDataInputStream in, 
                            PGPAlgorithmFactory factory) 
        throws IOException, PGPFatalDataFormatException, PGPDataFormatException
    {

        // read and parse the type
        byte type = in.readByte();

        if (type == 0x62) {
            binary = true; local = false;
        } else if (type == 0x74) {
            binary = false; local = false;
        } else if (type == 0x6C) {
            binary = true; local = true;
        } else {
            in.readByteArray(); // read all remaining data
            throw new PGPDataFormatException("Illegal literal data type");
        }
        

        // read the filename
        filename = in.readLengthPrependedString();
        
        // read the time
        time = in.readUnsignedInt();
        
        // all remaining bytes in the packet is binary data
        data = in.readByteArray();
                
    }



// Write method
// ..........................................................................

    /**
     * Encode the packet body.
     *
     * @throws IOException if something goes wrong with the output stream.
     */
    public void encodeBody (PGPPacketDataOutputStream out) 
        throws IOException
    {
        
        // write the type byte
        if (local) {
            out.writeByte((byte)0x6C); // 'l'
        } else if (binary) {
            out.writeByte((byte)0x62); // 'b'
        } else {
            out.writeByte((byte)0x74); // 't'
        }
        
        // write the filename
        if (filename == null) {
            out.writeLengthPrependedString("");
        } else {
            out.writeLengthPrependedString(filename);
        }
        
        // write the time
        if (time == 0) {
            time = (new Date()).getTime() / 1000;
        }
        out.writeInt((int)time);
        
        // write the data
        out.writeFully(data);
        
    }


}
