/* $Id: PGPSignedMessage.java,v 1.3 2005/03/13 17:46:34 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.openpgp;


import cryptix.message.SignedMessage;
import cryptix.message.MessageException;


/**
 * Represents an OpenPGP signed message.
 *
 * <p>
 * The difference between this class and the normal SignedMessage class is
 * a PGP specific method: getDetachedSignature().
 * </p><p>
 * All instances of SignedMessage returned by the OpenPGP provider are also
 * instances of this class.
 * </p>
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class PGPSignedMessage extends SignedMessage {


// Variables and getters
// ..........................................................................

    protected boolean legacy;
    /* package */ boolean isLegacy() { return legacy; }


// Constructor
// ..........................................................................

    /**
     * Creates a SignedMessage object.
     */
    protected PGPSignedMessage(String format, boolean legacy) 
    {
        super(format);
        this.legacy = legacy;
    }


// Extra PGP specific methods
// ..........................................................................

    /**
     * Returns a detached signature.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract PGPDetachedSignatureMessage getDetachedSignature()
        throws MessageException;
        
        
}
