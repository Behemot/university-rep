/* $Id: PrincipalBuilderSpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.Principal;


/**
 * Service provider interface for PrincipalBuilder
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class PrincipalBuilderSpi {

    /**
     * Returns a new principal based on the given contents.
     *
     * <p>The parameter will probably be a String most of the times, however 
     * other things like an Image (for a photo) are also possible.</p>
     *
     * @throws PrincipalException on a variety of format specific problems.
     */
    public abstract Principal engineBuild(Object contents) 
        throws PrincipalException;
        
}
