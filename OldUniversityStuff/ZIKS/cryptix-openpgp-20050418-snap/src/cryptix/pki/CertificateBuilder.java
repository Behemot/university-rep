/* $Id: CertificateBuilder.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import java.security.spec.AlgorithmParameterSpec;


/**
 * A class for building a Certificate
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class CertificateBuilder {


// Instance variables
// ..........................................................................

    private final CertificateBuilderSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new CertificateBuilder object containing the given
     * SPI object.
     */
    protected CertificateBuilder(CertificateBuilderSpi builderSpi,
                                 Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns a CertificateBuilder that implements the given format.
     */
    public static CertificateBuilder getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("CertificateBuilder", 
                                               format);
        return new CertificateBuilder( (CertificateBuilderSpi)o[0],
                                       (Provider)o[1], format);
    }
    

    /**
     * Returns a CertificateBuilder from the given provider that 
     * implements the given format.
     */
    public static CertificateBuilder getInstance(String format, 
                                                 String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("CertificateBuilder", 
                                               format, provider);
        return new CertificateBuilder( (CertificateBuilderSpi)o[0],
                                       (Provider)o[1], format);
    }
    

    /**
     * Returns a CertificateBuilder from the given provider that 
     * implements the given format.
     */
    public static CertificateBuilder getInstance(String format, 
                                                 Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("CertificateBuilder", 
                                               format, provider);
        return new CertificateBuilder( (CertificateBuilderSpi)o[0],
                                       (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public final Certificate build(PublicKey subjectKey, Principal subjectName, 
                                   KeyBundle issuer, char[] passphrase,
                                   SecureRandom sr) 
        throws CertificateException, UnrecoverableKeyException
    {
        return this.spi.engineBuild(subjectKey, subjectName, issuer, 
                                    passphrase, sr);
    }
        

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public final Certificate build(PublicKey subjectKey, Principal subjectName, 
                                   KeyBundle issuer, char[] passphrase,
                                   SecureRandom sr, 
                                   AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException,
               UnrecoverableKeyException
    {
        return this.spi.engineBuild(subjectKey, subjectName, issuer, 
                                    passphrase, sr, algSpec);
    }
        

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     */
    public final Certificate build(PublicKey subjectKey, Principal subjectName, 
                                   PrivateKey issuer, SecureRandom sr)
        throws CertificateException
    {
        return this.spi.engineBuild(subjectKey, subjectName, issuer, sr);
    }
        

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     */
    public final Certificate build(PublicKey subjectKey, Principal subjectName, 
                                   PrivateKey issuer, SecureRandom sr, 
                                   AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException
    {
        return this.spi.engineBuild(subjectKey, subjectName, issuer, sr, 
                                    algSpec);
    }
        
}
