/* $Id: ExtendedCertificate.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.SignatureException;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;

import java.util.Date;


/**
 * Extensions for java.security.cert.Certificate, which are common to all
 * certificates.
 */
public abstract class ExtendedCertificate extends Certificate
{
    
// Constructor
// ..........................................................................

    /**
     * Create a new Certificate object with the given type.
     */
    protected ExtendedCertificate(String type)
    {
        super(type);
    }


// Added abstract methods
// ..........................................................................

    /**
     * Returns the userID for which this certificate was issued.
     */
    public abstract Principal getSubject();
    

    /**
     * Checks if this certificate is currently valid.
     *
     * <p>A certificate is valid if date is on or after the creation date and
     * before the expiration date (when available).</p>
     */
    public abstract void checkValidity()
        throws CertificateExpiredException, CertificateNotYetValidException,
               CertificateParsingException;
        

    /**
     * Checks if this certificate is valid on a given date.
     *
     * <p>A certificate is valid if date is on or after the creation date and
     * before the expiration date (when available).</p>
     */
    public abstract void checkValidity(Date date)
        throws CertificateExpiredException, CertificateNotYetValidException,
               CertificateParsingException;


    /**
     * Returns whether the certificate is self signed
     */
    public abstract boolean isSelfSigned()
        throws CertificateException;
    
    
    /** 
     * Verifies that this certificate was signed using the specified keybundle.
     */
    public abstract void verify(KeyBundle bundle)
        throws CertificateException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchProviderException, SignatureException;

}