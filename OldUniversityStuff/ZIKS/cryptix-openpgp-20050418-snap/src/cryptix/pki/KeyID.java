/* $Id: KeyID.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.Key;


/**
 * A KeyID is an identification of a key, usually by a hash.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class KeyID {


// Instance variables
// ..........................................................................

    private final String type;
    

// Constructor
// ..........................................................................

    /**
     * Create a new KeyID object with the given type.
     */
    protected KeyID(String type)
    {
        this.type = type;
    }



// Methods from java.lang.object
// ..........................................................................

    /** Cache the hashcode */
    private transient int cachedHashCode;
    private transient boolean isHashCodeCached = false;

    /**
     * Returns a hashCode for this object based on the byte[]s returned by
     * getBytes(4).
     */
    public int hashCode() {

        if (! isHashCodeCached) {
            byte[] id = getBytes(4);
            cachedHashCode = (  (((int)id[0])          << 24) 
                             | ((((int)id[1]) & 0xFF ) << 16)
                             | ((((int)id[2]) & 0xFF ) <<  8)
                             | ((((int)id[3]) & 0xFF )      ) );
            isHashCodeCached = true;
        }
        return cachedHashCode;

    }



// Implemented methods
// ..........................................................................

    /**
     * Returns the type of this keyID.
     */
    public final String getType() {
        return this.type;
    }
    
    

// Abstract methods
// ..........................................................................

    /**
     * Return a clone of this object
     */
    public abstract Object clone();
    

    /**
     * Returns the length of the contained KeyID.
     *
     * <p>This is the maximum number of bytes the getBytes(int) method can 
     * return.</p>
     */
    public abstract int getLength();


    /**
     * Returns the full keyID as a byte array.
     */
    public abstract byte[] getBytes();


    /**
     * Returns a byte array of len bytes containing a reduced keyID.
     *
     * <p>This method reduces the output of getBytes() in a format specific way
     * to len bytes. </p>
     * <p>E.g. for OpenPGP getBytes(4) and getBytes(8) would return
     * the 32-bit and 64-bit key ID's, being the least significant bits of the
     * full 160-bit fingerprint.</p>
     *
     * @param len the number of bytes to return
     *
     * @throws IllegalArgumentException for unsupported lengths
     */
    public abstract byte[] getBytes(int len);
    

    /**
     * Do an equality comparison.
     *
     * <p>Note that this is an exact equality match. See the match(...) methods
     * for if you want to match partial keyID's.</p>
     */
    public abstract boolean equals(Object other);


    /**
     * Matches this keyID to another.
     *
     * <p>This method differs from equals in that it can do a reduced match:
     * if for example only 32 bits are available in one keyID, while 160 bits 
     * are available in the other, then this method can still return true if
     * the 32 bits match.</p>
     *
     * @throws IllegalArgumentException if other is of an incorrect type
     */
    public abstract boolean match(KeyID other);
    
    
    /**
     * Matches this keyID to a key.
     *
     * <p>This method differs from equals in that it can do a reduced match:
     * if for example only 32 bits are available in this keyID, while the full
     * keyID of a key is 160 bits, then this method can still return true if
     * the 32 bits match.</p>
     *
     * @throws IllegalArgumentException if other is of an incorrect type
     */
    public abstract boolean match(Key other);
    
}
