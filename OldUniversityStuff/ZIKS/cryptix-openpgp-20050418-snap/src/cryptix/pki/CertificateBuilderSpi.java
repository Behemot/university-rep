/* $Id: CertificateBuilderSpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidAlgorithmParameterException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import java.security.spec.AlgorithmParameterSpec;


/**
 * Service provider interface for CertificateBuilder
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class CertificateBuilderSpi {

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public abstract Certificate engineBuild(PublicKey subjectKey, 
                                            Principal subjectName, 
                                            KeyBundle issuer, char[] passphrase,
                                            SecureRandom sr) 
        throws CertificateException, UnrecoverableKeyException;

        
    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer and with the given parameters.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the keybundle that issues this certificate.
     * @param passphrase the passphrase to decrypt the private key.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     * @throws UnrecoverableKeyException on an invalid passphrase.
     */
    public abstract Certificate engineBuild(PublicKey subjectKey, 
                                            Principal subjectName, 
                                            KeyBundle issuer, char[] passphrase,
                                            SecureRandom sr,
                                            AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException,
               UnrecoverableKeyException;
        

    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     *
     * @throws CertificateException on a variety of format specific problems.
     */
    public abstract Certificate engineBuild(PublicKey subjectKey, 
                                            Principal subjectName, 
                                            PrivateKey issuer, SecureRandom sr) 
        throws CertificateException;

        
    /**
     * Returns a new Certificate based on the given subject key, subject name
     * and Issuer and with the given parameters.
     *
     * <p>This method will generally need to execute a cryptographic signing
     * operation, so it could take a while to execute.</p>
     *
     * @param subjectKey the key that will be binded to the principal by the
     *        resulting certificate.
     * @param subjectName the principal that will be binded to the key by the
     *        resulting certificate.
     * @param issuer the private key that issues this certificate.
     * @param sr a randomness source.
     * @param algSpec an algorithm parameter specification.
     *
     * @throws CertificateException on a variety of format specific problems.
     * @throws InvalidAlgorithmParameterException on invalid parameters.
     */
    public abstract Certificate engineBuild(PublicKey subjectKey, 
                                            Principal subjectName, 
                                            PrivateKey issuer, SecureRandom sr, 
                                            AlgorithmParameterSpec algSpec) 
        throws CertificateException, InvalidAlgorithmParameterException;
        
}
