/* $Id: KeyBundleFactory.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.io.InputStream;
import java.io.IOException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;

import java.util.Collection;


/**
 * A class for generating a KeyBundle
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class KeyBundleFactory {


// Instance variables
// ..........................................................................

    private final KeyBundleFactorySpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new KeyBundleFactory object containing the given
     * SPI object.
     */
    protected KeyBundleFactory(KeyBundleFactorySpi builderSpi,
                               Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns a KeyBundleFactory that implements the given format.
     */
    public static KeyBundleFactory getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("KeyBundleFactory", 
                                               format);
        return new KeyBundleFactory( (KeyBundleFactorySpi)o[0],
                                     (Provider)o[1], format);
    }
    

    /**
     * Returns a KeyBundleFactory from the given provider that 
     * implements the given format.
     */
    public static KeyBundleFactory getInstance(String format, 
                                               String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("KeyBundleFactory", 
                                               format, provider);
        return new KeyBundleFactory( (KeyBundleFactorySpi)o[0],
                                     (Provider)o[1], format);
    }
    

    /**
     * Returns a KeyBundleFactory from the given provider that 
     * implements the given format.
     */
    public static KeyBundleFactory getInstance(String format, 
                                               Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("KeyBundleFactory", 
                                               format, provider);
        return new KeyBundleFactory( (KeyBundleFactorySpi)o[0],
                                     (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................


    /**
     * Generates a (possible empty) collection of keybundles from an input 
     * stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public final Collection generateKeyBundles(InputStream in) 
        throws KeyBundleException, IOException
    {
        return this.spi.engineGenerateKeyBundles(in);
    }
    

    /**
     * Generates a KeyBundle from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public final KeyBundle generateKeyBundle(InputStream in)
        throws KeyBundleException, IOException
    {
        return this.spi.engineGenerateKeyBundle(in);
    }


    /**
     * Generates a new empty KeyBundle.
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public final KeyBundle generateEmptyKeyBundle()
        throws KeyBundleException
    {
        return this.spi.engineGenerateEmptyKeyBundle();
    }


}
