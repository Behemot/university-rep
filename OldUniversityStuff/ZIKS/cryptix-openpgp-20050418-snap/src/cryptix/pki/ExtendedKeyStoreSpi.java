/* $Id: ExtendedKeyStoreSpi.java,v 1.3 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.KeyStoreException;
import java.security.KeyStoreSpi;


/**
 * DOCUMENT ME
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class ExtendedKeyStoreSpi extends KeyStoreSpi {


// Extended methods
// ..........................................................................


    /**
     * Returns the keybundle identified by the given alias.
     *
     * @param alias the alias for the entry to return
     * @return the keybundle entry identified by alias or null if the
     *          given alias does not identify a keybundle entry
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public abstract KeyBundle engineGetKeyBundle(String alias)
        throws KeyStoreException;
    

    /**
     * Returns if the entry identified by alias is a keybundle entry.
     *
     * @param alias the alias for the entry to check
     * @return true if the entry identified by alias is a keybundle entry,
     *          false otherwise
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public abstract boolean engineIsKeyBundleEntry(String alias)
        throws KeyStoreException;
    

    /**
     * Stores the given keybundle in this store.
     *
     * @param bundle the bundle to store
     * @return the alias under which the bundle is stored
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public abstract String engineSetKeyBundleEntry(KeyBundle bundle)
        throws KeyStoreException;

}
