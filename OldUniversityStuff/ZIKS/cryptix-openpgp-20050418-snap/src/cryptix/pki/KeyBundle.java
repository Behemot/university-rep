/* $Id: KeyBundle.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import java.security.cert.Certificate;

import java.util.Iterator;


/**
 * A KeyBundle is a collection of keys, principals and certificates, which all
 * logically belong together.
 *
 * <p>Here are a few suggestions on how to implement this class for existing
 * public key structures:</p>
 *
 * <p>X.509 - an X.509 KeyBundle consists of just one certificate, together with
 * the publickey and principal as returned by the getSubjectDN() method on 
 * X509Certificate.</p>
 *
 * <p>OpenPGP - an OpenPGP KeyBundle consists of one 'PGP key', which means:
 * - one key object, the signing key. <br>
 * - one or more principal objects, which correspond to UserID's in OpenPGP
 * (potentially also PhotoID objects).<br>
 * - one or more certificates, which is basically a certification signature by 
 * someone on a principal/userid.<br>
 * - the subkeys (encryption keys) are exposed trough the OpenPGPKeyBundle
 * class.</p>
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @author  Ingo Luetkebohle
 * @version $Revision: 1.2 $
 */
public abstract class KeyBundle {


// Instance variables
// ..........................................................................

    private final String type;
    
    /**
     * Cached version of our hashCode as it is quite expensive to compute.
     * This field is considered valid if it is != -1.
     */
    private transient int cachedHashCode = -1;



// Constructor
// ..........................................................................

    /**
     * Create a new KeyBundle object with the given type.
     */
    protected KeyBundle(String type)
    {
        this.type = type;
    }



// Methods from java.lang.object
// ..........................................................................

    /**
     * Do equality comparison based on equality of the byte[]s returned by
     * getEncoded(). 
     */
    public boolean equals(Object other) {

        try {
            if ( !(other instanceof KeyBundle) ) return false;

            KeyBundle that = (KeyBundle)other;

            if (this.hashCode() != that.hashCode()) return false;

            byte[] thisBytes = this.getEncoded();
            byte[] thatBytes = that.getEncoded();

            if (thisBytes.length != thatBytes.length) return false;

            for(int i=0; i<thisBytes.length; i++)
                if (thisBytes[i] != thatBytes[i]) return false;

            return true;

        } catch(NullPointerException e) {
            /*
             * Okay, so getEncoded() returned a null ptr. It doesn't make
             * sense but let's not crash...
             */
            return false;

        } catch(KeyBundleException e) {
            return false;
        }
    }


    /**
     * Returns a hashCode for this object based on the encoded form. 
     */
    public int hashCode() {

        if (this.cachedHashCode != -1) return this.cachedHashCode;

        try {
            byte[] thisBytes = this.getEncoded();
            int hash = 0;
            for(int i=0; i<thisBytes.length; i++)
                hash = ((hash<<7) | (hash>>>25)) ^ (thisBytes[i]&0xFF);

            return this.cachedHashCode = hash;
        
        } catch(NullPointerException e) {
            /*
             * Okay, so getEncoded() returned a null ptr. It doesn't make
             * sense but let's not crash...
             */
            return 0;

        } catch(KeyBundleException e) {
            return 0;
        }
    }



// Implemented methods
// ..........................................................................

    /**
     * Returns the type of this keybundle.
     */
    public final String getType() {
        return this.type;
    }
    
    

// Abstract methods
// ..........................................................................


    /**
     * Returns the keybundle in encoded format.
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public abstract byte[] getEncoded() 
        throws KeyBundleException;

    
    /**
     * Return a clone for this KeyBundle
     */
    public abstract Object clone();
    

    /**
     * Adds a certificate
     *
     * <p>It is assumed that the certificate contains the right pointers to
     * the public key and the principal, so that the implementation of the
     * keybundle can add these automatically when needed.</p>
     */
    public abstract boolean addCertificate(Certificate cert)
        throws KeyBundleException;
    

    /**
     * Adds a principal.
     *
     * <p>Note: there is no need to call this method explicitly when 
     * addCertificate is used.</p>
     */
    public abstract boolean addPrincipal  (Principal princ)
        throws KeyBundleException;


    /**
     * Adds a public key.
     *
     * <p>Note: there is no need to call this method explicitly when 
     * addCertificate is used.</p>
     */
    public abstract boolean addPublicKey  (PublicKey pubkey)
        throws KeyBundleException;
        

    /**
     * Adds a private key, encrypting it with a passphrase.
     */
    public abstract boolean addPrivateKey (PrivateKey privkey, PublicKey pubkey,
                                           char[] passphrase, SecureRandom sr)
        throws KeyBundleException;
    

    /**
     * Adds a private key, without encrypting it.
     */
    public abstract boolean addPrivateKey (PrivateKey privkey, PublicKey pubkey)
        throws KeyBundleException;
    

    /**
     * Remove a public key and all related principals and certificates.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public abstract boolean removePublicKey   (PublicKey key)
        throws KeyBundleException;

    /**
     * Remove the private key belonging to the given public key.
     */
    public abstract boolean removePrivateKey  (PublicKey key)
        throws KeyBundleException;

    /**
     * Remove a principal and all related keys and certificates.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public abstract boolean removePrincipal   (Principal subject)
        throws KeyBundleException;

    /**
     * Remove a certificate and all related keys and principals.
     *
     * <p>The definition of 'related' is somewhat loose and left up to the
     * implementation. The only thing an implementation has to make sure is that
     * after removing one object it removes enough other objects in order to 
     * maintain a valid state according to the particular type.</p>
     */
    public abstract boolean removeCertificate (Certificate cert)
        throws KeyBundleException;
    

    /**
     * Return an iterator over all keys contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.Key</p>
     */
    public abstract Iterator getPublicKeys();


    /**
     * Return an the private key belonging to the given public key, decryptin
     * it with the given passphrase.
     *
     * <p>Returns null if no private key is available</p>
     */
    public abstract PrivateKey getPrivateKey(PublicKey key, char[] passphrase)
        throws UnrecoverableKeyException;


    /**
     * Return an iterator over all principals contained within this bundle
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.Principal</p>
     */
    public abstract Iterator getPrincipals();


    /**
     * Return an iterator over all certificate contained within this bundle.
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.cert.Certificate</p>
     */
    public abstract Iterator getCertificates();
    

    /**
     * Return an iterator over the certificates contained within this bundle
     * that belong to a certain key and principal.
     *
     * <p>The objects returned by the iterator will all be instances of
     * java.security.cert.Certificate</p>
     */
    public abstract Iterator getCertificates(PublicKey key,Principal principal);

}
