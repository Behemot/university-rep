/* $Id: ExtendedCertStoreSpi.java,v 1.3 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidAlgorithmParameterException;

import java.security.cert.CertStoreException;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertStoreSpi;

import java.util.Collection;


/**
 * DOCUMENT ME
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class ExtendedCertStoreSpi extends CertStoreSpi {


// Constructor
// ..........................................................................


    /**
     * Main constructor
     *
     * @param params the initialization parameters (can be null).
     * @throws InvalidAlgorithmParameterException if the given parameters are
     *         invalid.
     */
    public ExtendedCertStoreSpi(CertStoreParameters params)
        throws InvalidAlgorithmParameterException
    {
        super(params);
    }
    


// Extended methods
// ..........................................................................


    /**
     * Returns a Collection of KeyBundles matching the given selector.
     *
     * <p>If no matches can be found, an empty collection will be returned.</p>
     *
     * @param selector A selector that specifies which keybundles to return.
     *        Specify null here to return all keybundles (if supported).
     * @throws CertStoreException on a variety of type specific problems, 
     *         including no support for the 'null' selector.
     * @return a (possibly empty) Collection of KeyBundles that match the 
     *          given selector.
     */
    public abstract Collection engineGetKeyBundles(KeyBundleSelector selector)
        throws CertStoreException;
    

    /**
     * Stores the given keybundle in this store.
     *
     * @param bundle the bundle to store
     * @throws CertStoreException on a variety of type specific problems
     */
    public abstract void engineSetKeyBundleEntry(KeyBundle bundle)
        throws CertStoreException;

}
