/* $Id: KeyBundleSelector.java,v 1.3 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


/**
 * A selector that defines which KeyBundles should be selected in a query.
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public interface KeyBundleSelector {

    /** 
     * Match a keybundle.
     *
     * @param bundle the bundle to check against this selector
     * @return true if bundle should be selected, false otherwise
     */
    boolean match(KeyBundle bundle);

    /**
     * Clone this selector.
     */
    Object clone();
    
}
