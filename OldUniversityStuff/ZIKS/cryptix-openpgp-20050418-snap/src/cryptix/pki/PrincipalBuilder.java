/* $Id: PrincipalBuilder.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.Provider;


/**
 * A class for building a principal
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class PrincipalBuilder {


// Instance variables
// ..........................................................................

    private final PrincipalBuilderSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new PrincipalBuilder object containing the given
     * SPI object.
     */
    protected PrincipalBuilder(PrincipalBuilderSpi builderSpi,
                               Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns a PrincipalBuilder that implements the given format.
     */
    public static PrincipalBuilder getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("PrincipalBuilder", 
                                               format);
        return new PrincipalBuilder( (PrincipalBuilderSpi)o[0],
                                     (Provider)o[1], format);
    }
    

    /**
     * Returns a PrincipalBuilder from the given provider that 
     * implements the given format.
     */
    public static PrincipalBuilder getInstance(String format, 
                                               String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("PrincipalBuilder", 
                                               format, provider);
        return new PrincipalBuilder( (PrincipalBuilderSpi)o[0],
                                     (Provider)o[1], format);
    }
    

    /**
     * Returns a PrincipalBuilder from the given provider that 
     * implements the given format.
     */
    public static PrincipalBuilder getInstance(String format, 
                                               Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("PrincipalBuilder", 
                                               format, provider);
        return new PrincipalBuilder( (PrincipalBuilderSpi)o[0],
                                     (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Returns a new principal based on the given contents.
     *
     * <p>The parameter will probably be a String most of the times, however 
     * other things like an Image (for a photo) are also possible.</p>
     *
     * @throws PrincipalException on a variety of format specific problems.
     */
    public final Principal build(Object contents) 
        throws PrincipalException
    {
        return this.spi.engineBuild(contents);
    }
        
}
