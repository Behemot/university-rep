/* $Id: KeyIDFactorySpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidKeyException;
import java.security.Key;


/**
 * Service provider interface for KeyIDFactory
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class KeyIDFactorySpi {

    /**
     * Generates a KeyID from a key.
     *
     * @throws InvalidKeyException if the given key is not of the right type
     *         to produce a KeyID.
     */
    public abstract KeyID engineGenerateKeyID(Key key) 
        throws InvalidKeyException;

}
