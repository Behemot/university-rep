/* $Id: ExtendedKeyStore.java,v 1.3 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;


/**
 * DOCUMENT ME
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class ExtendedKeyStore extends KeyStore {


// Instance variables
// ..........................................................................

    private final ExtendedKeyStoreSpi spi;
    

// Constructor
// ..........................................................................

    /**
     * Create a new ExtendedKeyStore object containing the given
     * SPI object.
     */
    protected ExtendedKeyStore(ExtendedKeyStoreSpi storeSpi,
                               Provider provider, String type)
    {
        super(storeSpi, provider, type);
        this.spi = storeSpi;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an ExtendedKeyStore that implements the given type.
     */
    public static KeyStore getInstance(String type) 
        throws KeyStoreException
    {
        try {

            Object[] o = Support.getImplementation("ExtendedKeyStore", 
                                                   type);
            return new ExtendedKeyStore( (ExtendedKeyStoreSpi)o[0],
                                         (Provider)o[1], type);

        } catch (NoSuchAlgorithmException nsae) {
            throw new KeyStoreException(""+nsae);
        }
    }
    

    /**
     * Returns an ExtendedKeyStore from the given provider that 
     * implements the given type.
     */
    public static KeyStore getInstance(String type, String provider) 
        throws KeyStoreException
    {
        try {

            Object[] o = Support.getImplementation("ExtendedKeyStore", 
                                                   type, provider);
            return new ExtendedKeyStore( (ExtendedKeyStoreSpi)o[0],
                                         (Provider)o[1], type);

        } catch (NoSuchAlgorithmException nsae) {
            throw new KeyStoreException(""+nsae);
        } catch (NoSuchProviderException nspe) {
            throw new KeyStoreException(""+nspe);
        }
    }
    
    /**
     * Returns an ExtendedKeyStore from the given provider that 
     * implements the given type.
     */
    public static KeyStore getInstance(String type, Provider provider) 
        throws KeyStoreException
    {
        try {

            Object[] o = Support.getImplementation("ExtendedKeyStore", 
                                                   type, provider);
            return new ExtendedKeyStore( (ExtendedKeyStoreSpi)o[0],
                                         (Provider)o[1], type);

        } catch (NoSuchAlgorithmException nsae) {
            throw new KeyStoreException(""+nsae);
        }
    }
    

// Extended methods
// ..........................................................................


    /**
     * Returns the keybundle identified by the given alias.
     *
     * @param alias the alias for the entry to return
     * @return the keybundle entry identified by alias or null if the
     *          given alias does not identify a keybundle entry
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public final KeyBundle getKeyBundle(String alias)
        throws KeyStoreException
    {
        return this.spi.engineGetKeyBundle(alias);
    }
    

    /**
     * Returns if the entry identified by alias is a keybundle entry.
     *
     * @param alias the alias for the entry to check
     * @return true if the entry identified by alias is a keybundle entry,
     *          false otherwise
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public final boolean isKeyBundleEntry(String alias)
        throws KeyStoreException
    {
        return this.spi.engineIsKeyBundleEntry(alias);
    }
    

    /**
     * Stores the given keybundle in this store.
     *
     * @param bundle the bundle to store
     * @return the alias under which the bundle is stored
     * @throws KeyStoreException if the keystore has not been initialized 
     *         (loaded) yet
     */
    public final String setKeyBundleEntry(KeyBundle bundle)
        throws KeyStoreException
    {
        return this.spi.engineSetKeyBundleEntry(bundle);
    }

}
