/* $Id: KeyIDFactory.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;


/**
 * A class for generating a KeyID
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class KeyIDFactory {


// Instance variables
// ..........................................................................

    private final KeyIDFactorySpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new KeyIDFactory object containing the given
     * SPI object.
     */
    protected KeyIDFactory(KeyIDFactorySpi builderSpi,
                           Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns a KeyIDFactory that implements the given format.
     */
    public static KeyIDFactory getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("KeyIDFactory", 
                                               format);
        return new KeyIDFactory( (KeyIDFactorySpi)o[0],
                                 (Provider)o[1], format);
    }
    

    /**
     * Returns a KeyIDFactory from the given provider that 
     * implements the given format.
     */
    public static KeyIDFactory getInstance(String format, 
                                           String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("KeyIDFactory", 
                                               format, provider);
        return new KeyIDFactory( (KeyIDFactorySpi)o[0],
                                 (Provider)o[1], format);
    }
    

    /**
     * Returns a KeyIDFactory from the given provider that 
     * implements the given format.
     */
    public static KeyIDFactory getInstance(String format, 
                                           Provider provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("KeyIDFactory", 
                                               format, provider);
        return new KeyIDFactory( (KeyIDFactorySpi)o[0],
                                 (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................


    /**
     * Generates a KeyID from a key.
     *
     * @throws InvalidKeyException if the given key is not of the right type
     *         to produce a KeyID.
     */
    public final KeyID generateKeyID(Key key) 
        throws InvalidKeyException
    {
        return this.spi.engineGenerateKeyID(key);
    }
    

}
