/* $Id: ExtendedCertStore.java,v 1.3 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;

import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertStoreSpi;
import java.security.cert.CertStoreParameters;

import java.util.Collection;


/**
 * DOCUMENT ME
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public class ExtendedCertStore extends CertStore {


// Instance variables
// ..........................................................................

    private final ExtendedCertStoreSpi spi;
    

// Constructor
// ..........................................................................

    /**
     * Create a new ExtendedCertStore object containing the given
     * SPI object.
     */
    protected ExtendedCertStore(ExtendedCertStoreSpi storeSpi, 
                                Provider provider, 
                                CertStoreParameters params, String type)
    {
        super(storeSpi, provider, type, params);
        this.spi = storeSpi;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an ExtendedCertStore that implements the given format.
     */
    public static CertStore getInstance(String type,
                                        CertStoreParameters params) 
        throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("ExtendedCertStore", 
                                          type);
        return new ExtendedCertStore( getSpiInstance((String)o[0], params),
                                      (Provider)o[1], params, type);
    }
    

    /**
     * Returns an ExtendedCertStore from the given provider that 
     * implements the given format.
     */
    public static CertStore getInstance(String type, 
                                        CertStoreParameters params,
                                        String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException,
               InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("ExtendedCertStore", 
                                          type, provider);
        return new ExtendedCertStore( getSpiInstance((String)o[0], params),
                                      (Provider)o[1], params, type);
    }
    

    /**
     * Returns an ExtendedCertStore from the given provider that 
     * implements the given format.
     */
    public static CertStore getInstance(String type, 
                                        CertStoreParameters params,
                                        Provider provider) 
        throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        Object[] o = Support.getClassName("ExtendedCertStore", 
                                          type, provider);
        return new ExtendedCertStore( getSpiInstance((String)o[0], params),
                                      (Provider)o[1], params, type);
    }
    
    /** Helper method */
    private static ExtendedCertStoreSpi getSpiInstance(String classname, 
                                                     CertStoreParameters params)
        throws NoSuchAlgorithmException
    {
        try {
            Class spiClass = Class.forName(classname);
            Constructor ctor = spiClass.getConstructor(
                new Class[] { CertStoreParameters.class });
            ExtendedCertStoreSpi spi = (ExtendedCertStoreSpi)ctor.newInstance(
                new Object[] { params.clone() });
            return spi;
        } catch(ClassNotFoundException e) {
            throw new NoSuchAlgorithmException("Registered class not found");
        } catch(ClassCastException e) {
            throw new NoSuchAlgorithmException(
                "Registered class is not a CertStoreSpi");
        } catch(NoSuchMethodException e) {
            throw new NoSuchAlgorithmException(
                "Registered class does not have the proper constructor");
        } catch(InvocationTargetException e) {
            throw new NoSuchAlgorithmException(
                "Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        } catch(InstantiationException e) {
            throw new NoSuchAlgorithmException(
                "Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        } catch(IllegalAccessException e) {
            throw new NoSuchAlgorithmException(
                "Class for algorithm couldn't be instantiated: " + 
                e.getMessage());
        }
    }


// Extended methods
// ..........................................................................


    /**
     * Returns a Collection of KeyBundles matching the given selector.
     *
     * <p>If no matches can be found, an empty collection will be returned.</p>
     *
     * @param selector A selector that specifies which keybundles to return.
     *        Specify null here to return all keybundles (if supported).
     * @throws CertStoreException on a variety of type specific problems, 
     *         including no support for the 'null' selector.
     * @return a (possibly empty) Collection of KeyBundles that match the 
     *          given selector.
     */
    public final Collection getKeyBundles(KeyBundleSelector selector)
        throws CertStoreException
    {
        return this.spi.engineGetKeyBundles(selector);
    }
    

    /**
     * Stores the given keybundle in this store.
     *
     * @param bundle the bundle to store
     * @throws CertStoreException on a variety of type specific problems
     */
    public final void setKeyBundleEntry(KeyBundle bundle)
        throws CertStoreException
    {
        this.spi.engineSetKeyBundleEntry(bundle);
    }

}
