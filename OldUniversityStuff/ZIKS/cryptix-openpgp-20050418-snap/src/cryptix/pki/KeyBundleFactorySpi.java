/* $Id: KeyBundleFactorySpi.java,v 1.2 2005/03/13 17:47:10 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.pki;


import java.io.InputStream;
import java.io.IOException;

import java.util.Collection;


/**
 * Service provider interface for KeyBundleFactory
 *
 * @author  Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class KeyBundleFactorySpi {

    /**
     * Generates a (possible empty) collection of keybundles from an input 
     * stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public abstract Collection engineGenerateKeyBundles(InputStream in) 
        throws KeyBundleException, IOException;
    

    /**
     * Generates a KeyBundle from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public abstract KeyBundle engineGenerateKeyBundle(InputStream in)
        throws KeyBundleException, IOException;


    /**
     * Generates a new empty KeyBundle.
     *
     * @throws KeyBundleException on a variety of format specific problems.
     */
    public abstract KeyBundle engineGenerateEmptyKeyBundle()
        throws KeyBundleException;
        
}
