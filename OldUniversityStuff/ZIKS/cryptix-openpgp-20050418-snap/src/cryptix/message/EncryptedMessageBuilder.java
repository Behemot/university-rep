/* $Id: EncryptedMessageBuilder.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.SecretKey;


/**
 * A class for building an EncryptedMessage.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class EncryptedMessageBuilder {


// Instance variables
// ..........................................................................

    private final EncryptedMessageBuilderSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new EncryptedMessageBuilder object containing the given
     * SPI object.
     */
    protected EncryptedMessageBuilder(EncryptedMessageBuilderSpi builderSpi,
                                      Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an EncryptedMessageBuilder that implements the given format.
     */
    public static EncryptedMessageBuilder getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("EncryptedMessageBuilder", 
                                               format);
        return new EncryptedMessageBuilder( (EncryptedMessageBuilderSpi)o[0],
                                            (Provider)o[1], format);
    }
    

    /**
     * Returns an EncryptedMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static EncryptedMessageBuilder getInstance(String format, 
                                                      String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("EncryptedMessageBuilder", 
                                               format, provider);
        return new EncryptedMessageBuilder( (EncryptedMessageBuilderSpi)o[0],
                                            (Provider)o[1], format);
    }
    

    /**
     * Returns an EncryptedMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static EncryptedMessageBuilder getInstance(String format, 
                                                      Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("EncryptedMessageBuilder", 
                                               format, provider);
        return new EncryptedMessageBuilder( (EncryptedMessageBuilderSpi)o[0],
                                            (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Initializes this builder with the given message.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(Message contents) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, new SecureRandom());
    }
    

    /**
     * Initializes this builder with the given message and SecureRandom object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(Message contents, SecureRandom sr) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, sr);
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public final void setAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, MessageException
    {
        this.spi.engineSetAttribute(name, attr);
    }


    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void addRecipient(PublicKey publickey) 
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.spi.engineAddRecipient(publickey);
    }


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void addRecipient(KeyBundle bundle) 
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.spi.engineAddRecipient(bundle);
    }


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void addRecipient(SecretKey symmetrickey) 
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.spi.engineAddRecipient(symmetrickey);
    }
        

    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void addRecipient(String passphrase) 
        throws IllegalStateException, UnsupportedOperationException,
               MessageException
    {
        this.spi.engineAddRecipient(passphrase);
    }
    
    
    /**
     * Returns the encrypted message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, if no calls have been made to any of the addRecipient
     *         methods or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public final Message build() 
        throws IllegalStateException, MessageException
    {
        return this.spi.engineBuild();
    }
        
}
