/* $Id: MessageFactory.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import java.io.InputStream;
import java.io.IOException;

import java.util.Collection;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;


/**
 * Message factory class that can generate Message objects from their encodings.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class MessageFactory {


// Instance variables
// ..........................................................................

    private final MessageFactorySpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new MessageFactory object containing the given
     * SPI object.
     */
    protected MessageFactory(MessageFactorySpi builderSpi,
                             Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns a MessageFactory that implements the given format.
     * 
     * @param format the message format.  "OpenPGP", for example.
     */
    public static MessageFactory getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("MessageFactory", 
                                               format);
        return new MessageFactory( (MessageFactorySpi)o[0],
                                   (Provider)o[1], format);
    }
    

    /**
     * Returns a MessageFactory from the given provider that 
     * implements the given format.
     *
     * @param format the message format.  "OpenPGP", for example.
     * @param provider the provider name.
     */
    public static MessageFactory getInstance(String format, String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("MessageFactory", 
                                               format, provider);
        return new MessageFactory( (MessageFactorySpi)o[0],
                                   (Provider)o[1], format);
    }
    

    /**
     * Returns a MessageFactory from the given provider that 
     * implements the given format.
     *
     * @param format the message format.  "OpenPGP", for example.
     * @param provider the provider object.
     */
    public static MessageFactory getInstance(String format, Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("MessageFactory", 
                                               format, provider);
        return new MessageFactory( (MessageFactorySpi)o[0],
                                   (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................


    /**
     * Generates a (possible empty) collection of messages from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     */
    public final Collection generateMessages(InputStream in) 
        throws MessageException, IOException
    {
        return this.spi.engineGenerateMessages(in);
    }
    

    /**
     * Generates a message from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public final Message generateMessage(InputStream in)
        throws MessageException, IOException
    {
        return this.spi.engineGenerateMessage(in);
    }

}
