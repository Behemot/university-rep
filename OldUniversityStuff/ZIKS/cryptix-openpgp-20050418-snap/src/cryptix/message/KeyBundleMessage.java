/* $Id: KeyBundleMessage.java,v 1.3 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;


/**
 * Represents an message containing a keybundle.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class KeyBundleMessage extends Message {


// Constructor
// ..........................................................................

    /**
     * Creates a EncryptedMessage of the specified format.
     */
    public KeyBundleMessage(String format) {
        super(format);
    }


// Own methods
// ..........................................................................

    /**
     * Returns the KeyBundle contained in this message.
     *
     * @return the contained keybundle.
     */
    public abstract KeyBundle getKeyBundle();
        
}
