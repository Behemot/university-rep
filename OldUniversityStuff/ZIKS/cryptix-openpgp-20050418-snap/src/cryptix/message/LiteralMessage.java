/* $Id: LiteralMessage.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import java.io.InputStream;
import java.io.Reader;


/**
 * Message containing encoded 'literal data'.
 *
 * <p>
 * This method can transparantly handle both binary and text data, the 
 * difference between the two being that text data is usually encoded in a
 * format specific way. E.g. for OpenPGP this is UTF-8.
 * </p>
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class LiteralMessage extends Message {


// Constructor
// ..........................................................................

    /**
     * Creates a LiteralMessage of the specified format.
     */
    public LiteralMessage(String format) {
        super(format);
    }
    

// Constants
// ..........................................................................

    /** 
     * Returned by getDataType() when it is unknown if the contained data is
     * in text or binary format. 
     *     
     * <p>
     * This usually happens because the underlying format does not store this
     * information. When this happens, support for retrieving the data in 
     * binary format must be supported by the implementing class, while support
     * for retrieving data in text format is optional.
     * </p>
     */
    public static final int UNKNOWN = 0;


    /** 
     * Returned by getDataType() when the contained data is in binary format. 
     */
    public static final int BINARY  = 1;


    /** 
     * Returned by getDataType() when the contained data is in text format. 
     */
    public static final int TEXT    = 2;


// Own methods
// ..........................................................................

    /**
     * Returns the type of data contained within this message.
     *
     * <p>
     * Can be one of UNKNOWN, BINARY and TEXT.
     * </p><p>
     * If the returned type is UNKNOWN, then usually the underlying format does 
     * not store this information. When this happens, support for retrieving 
     * the data in binary format must be supported by the implementing class, 
     * while support for retrieving data in text format is optional.
     * </p>
     */
    public abstract int getDataType();


    /**
     * Returns the data in binary format as an array of bytes.
     *
     * <p>
     * MUST be supported when getDataType() returns BINARY or UNKNOWN.
     * <br>
     * MAY be supported when getDataType() returns TEXT.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract byte[] getBinaryData() 
        throws UnsupportedOperationException, MessageException;


    /**
     * Returns the data in binary format trough an inputstream.
     *
     * <p>
     * MUST be supported when getDataType() returns BINARY or UNKNOWN. 
     * <br>
     * MAY be supported when getDataType() returns TEXT.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract InputStream getBinaryDataInputStream() 
        throws UnsupportedOperationException, MessageException;


    /**
     * Returns the data in text format as a string.
     *
     * <p>
     * MUST be supported when getDataType() returns TEXT.
     * <br>
     * MAY be supported when getDataType() returns BINARY or UNKNOWN.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract String getTextData()
        throws UnsupportedOperationException, MessageException;


    /**
     * Returns the data in text format as a character reader.
     *
     * <p>
     * MUST be supported when getDataType() returns TEXT.
     * <br>
     * MAY be supported when getDataType() returns BINARY or UNKNOWN.
     * </p>
     *
     * @throws UnsupportedOperationException when this particular operation is
     *         not supported. See above for restrictions on when an implementing
     *         class is allowed to throw this exception.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract Reader getTextDataReader() 
        throws UnsupportedOperationException, MessageException;

}
