/* $Id: Message.java,v 1.3 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


/**
 * Top-level class for all messages.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @author Jeroen C. van Gelderen <gelderen@cryptix.org>
 * @version $Revision: 1.3 $
 */
public abstract class Message {


// Instance variables
// ..........................................................................

    private String format;

    /**
     * Cached version of our hashCode as it is quite expensive to compute.
     * This field is considered valid if it is != -1.
     */
    private transient int cachedHashCode = -1;
    

// Constructor
// ..........................................................................

    /**
     * Creates a Message of the specified format.
     */
    protected Message(String format) { 
        this.format = format; 
    }
    

// Methods from java.lang.object
// ..........................................................................

    /**
     * Do equality comparison based on equality of the byte[]s returned by
     * getEncoded(). 
     */
    public boolean equals(Object other) {

        try {
            if ( !(other instanceof Message) ) return false;

            Message that = (Message)other;

            if (this.hashCode() != that.hashCode()) return false;

            byte[] thisBytes = this.getEncoded();
            byte[] thatBytes = that.getEncoded();

            if (thisBytes.length != thatBytes.length) return false;

            for(int i=0; i<thisBytes.length; i++)
                if (thisBytes[i] != thatBytes[i]) return false;

            return true;

        } catch(NullPointerException e) {
            /*
             * Okay, so getEncoded() returned a null ptr. It doesn't make
             * sense but let's not crash...
             */
            return false;

        } catch(MessageException e) {
            return false;
        }
    }


    /**
     * Returns a hashCode for this object based on the encoded form. 
     */
    public int hashCode() {

        if (this.cachedHashCode != -1) return this.cachedHashCode;

        try {
            byte[] thisBytes = this.getEncoded();
            int hash = 0;
            for(int i=0; i<thisBytes.length; i++)
                hash = ((hash<<7) | (hash>>>25)) ^ (thisBytes[i]&0xFF);

            return this.cachedHashCode = hash;
        
        } catch(NullPointerException e) {
            /*
             * Okay, so getEncoded() returned a null ptr. It doesn't make
             * sense but let's not crash...
             */
            return 0;

        } catch(MessageException e) {
            return 0;
        }
    }


// Own methods
// ..........................................................................

    /**
     * Returns the encoding format of this message.
     *
     * <p>
     * Potential examples are: 
     * <ul>
     *  <li>"OpenPGP" as defined in RFC 2440</li>
     *  <li>"PGP2.x" as defined in RFC 1991</li>
     *  <li>"CMS" as defined in RFC 2630</li>
     *  <li>"PKCS#7" as defined in RFC 2315</li>
     *  <li>"PEM" as defined in RFC 1421</li>
     * </ul>
     * </p>
     */
    public String getFormat() { 
        return format; 
    }
    

    /**
     * Returns the message in encoded format.
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract byte[] getEncoded() throws MessageException;
    

    /**
     * Get a format specific attribute.
     *
     * @throws IllegalArgumentException if the attribute is not supported on
     *         this format. (Note: if an attribute is supported but not set, 
     *         then this method should return null instead of throwing this
     *         exception.)
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     *
     * @return the attribute or null if the attribute isn't set.
     */
    public abstract Object getAttribute(String name)
        throws IllegalArgumentException, MessageException;


}
