/* $Id: EncryptedMessageBuilderSpi.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;

import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.SecretKey;


/**
 * Service provider interface for EncryptedMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class EncryptedMessageBuilderSpi {

    /**
     * Initializes this builder with the given message.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineInit(Message contents, SecureRandom sr)
        throws IllegalStateException, MessageException;
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public abstract void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
            MessageException;


    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineAddRecipient(PublicKey publickey)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException;


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineAddRecipient(KeyBundle bundle)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException;


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineAddRecipient(SecretKey symmetrickey)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException;
        

    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineAddRecipient(String passphrase)
        throws IllegalStateException, UnsupportedOperationException,
               MessageException;
    

    /**
     * Returns the encrypted message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, if no calls have been made to any of the addRecipient
     *         methods or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract Message engineBuild()
        throws IllegalStateException, MessageException;
        
}
