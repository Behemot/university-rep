/* $Id: EncryptedMessage.java,v 1.4 2005/04/18 12:12:28 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;

import java.security.Key;
import java.security.UnrecoverableKeyException;


/**
 * Represents an encrypted message.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public abstract class EncryptedMessage extends Message {


// Constructor
// ..........................................................................

    /**
     * Creates a EncryptedMessage of the specified format.
     */
    public EncryptedMessage(String format) {
        super(format);
    }


// Own methods
// ..........................................................................

    /**
     * Decrypt the message with a key.
     *
     * <p>For most formats this key will usually be an instance of PrivateKey
     * (i.e. public key encryption was used). However, for some formats it could
     * also be possible to pass in a SecretKey (i.e. symmetric key encryption
     * was used).</p>
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular key.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support key based encryption.
     *
     * @return the decrypted message.
     */
    public abstract Message decrypt(Key key)
        throws NotEncryptedToParameterException, MessageException,
               UnsupportedOperationException;
        

    /**
     * Decrypt the message with a keybundle, decrypting the private key in the
     * keybundle using the given passphrase.
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular key.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support key based encryption.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     *
     * @return the decrypted message.
     */
    public abstract Message decrypt(KeyBundle bundle, char[] passphrase)
        throws NotEncryptedToParameterException, MessageException,
               UnsupportedOperationException, UnrecoverableKeyException;
        

    /**
     * Decrypt the message with a passphrase.
     *
     * @throws NotEncryptedToParameterException if the decryption failed because
     *         this message is not encrypted to that particular passphrase.
     * @throws MessageException on a variety of format specific problems.
     * @throws UnsupportedOperationException if this particular format does not
     *         support password based encryption.
     *
     * @return the decrypted message.
     */
    public abstract Message decrypt(char[] passphrase)
        throws NotEncryptedToParameterException, MessageException, 
               UnsupportedOperationException;


    /**
     * Returns hints for which keys can be used to decrypt this message
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract KeyID[] getKeyIDHints()
        throws MessageException;

}
