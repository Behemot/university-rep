/* $Id: MessageFactorySpi.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import java.io.InputStream;
import java.io.IOException;

import java.util.Collection;


/**
 * Service provider interface for MessageFactory
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class MessageFactorySpi {

    /**
     * Generates a (possible empty) collection of messages from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     */
    public abstract Collection engineGenerateMessages(InputStream in) 
        throws MessageException, IOException;
    

    /**
     * Generates a messages from an input stream.
     *
     * <p>Note: the entire inputstream will be read when the inputstream does not
     * support the mark() and reset() methods.</p>
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract Message engineGenerateMessage(InputStream in)
        throws MessageException, IOException;

}
