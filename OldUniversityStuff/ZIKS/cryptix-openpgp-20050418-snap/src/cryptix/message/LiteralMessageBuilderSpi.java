/* $Id: LiteralMessageBuilderSpi.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import java.security.SecureRandom;


/**
 * Service provider interface for LiteralMessageBuilder
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public abstract class LiteralMessageBuilderSpi {

    /**
     * Initializes this builder with the given binary data and SecureRandom
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineInit(byte[] contents, SecureRandom sr)
        throws IllegalStateException, MessageException;
    

    /**
     * Initializes this builder with the given text data and SecureRandom
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract void engineInit(String contents, SecureRandom sr)
        throws IllegalStateException, MessageException;
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public abstract void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException,
               MessageException;


    /**
     * Returns the built literal message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract Message engineBuild()
        throws IllegalStateException, MessageException;
        
}
