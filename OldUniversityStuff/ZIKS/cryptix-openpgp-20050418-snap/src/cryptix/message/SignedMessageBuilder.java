/* $Id: SignedMessageBuilder.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;


/**
 * A class for building a SignedMessage.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class SignedMessageBuilder {


// Instance variables
// ..........................................................................

    private final SignedMessageBuilderSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new SignedMessageBuilder object containing the given
     * SPI object.
     */
    protected SignedMessageBuilder(SignedMessageBuilderSpi builderSpi,
                                   Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an SignedMessageBuilder that implements the given format.
     */
    public static SignedMessageBuilder getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("SignedMessageBuilder", 
                                               format);
        return new SignedMessageBuilder( (SignedMessageBuilderSpi)o[0],
                                         (Provider)o[1], format);
    }
    

    /**
     * Returns an SignedMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static SignedMessageBuilder getInstance(String format, 
                                                   String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("SignedMessageBuilder", 
                                               format, provider);
        return new SignedMessageBuilder( (SignedMessageBuilderSpi)o[0],
                                         (Provider)o[1], format);
    }
    

    /**
     * Returns an SignedMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static SignedMessageBuilder getInstance(String format, 
                                                   Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("SignedMessageBuilder", 
                                               format, provider);
        return new SignedMessageBuilder( (SignedMessageBuilderSpi)o[0],
                                         (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Initializes this builder with the given message.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(Message contents) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, new SecureRandom());
    }
    

    /**
     * Initializes this builder with the given message and SecureRandom object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(Message contents, SecureRandom sr) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, sr);
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public final void setAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageException
    {
        this.spi.engineSetAttribute(name, attr);
    }


    /**
     * Adds a signer
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws MessageException on a variety of format specific problems.
     */
    public final void addSigner(PrivateKey signingkey)
        throws IllegalStateException, MessageException
    {
        this.spi.engineAddSigner(signingkey);
    }


    /**
     * Adds a signer from a keybundle, decrypting it with the given passphrase.
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws MessageException on a variety of format specific problems.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     */
    public final void addSigner(KeyBundle signingkey, char[] passphrase)
        throws IllegalStateException, MessageException,
               UnrecoverableKeyException
    {
        this.spi.engineAddSigner(signingkey, passphrase);
    }


    /**
     * Returns the signed message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, if no calls have been made to the addSigner method
     *         or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public final Message build() 
        throws IllegalStateException, MessageException
    {
        return this.spi.engineBuild();
    }
        
}
