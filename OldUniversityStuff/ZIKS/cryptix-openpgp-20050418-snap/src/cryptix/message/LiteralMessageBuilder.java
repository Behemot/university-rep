/* $Id: LiteralMessageBuilder.java,v 1.2 2005/03/13 17:46:33 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;


/**
 * A class for building a LiteralMessage.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.2 $
 */
public class LiteralMessageBuilder {


// Instance variables
// ..........................................................................

    private final LiteralMessageBuilderSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new LiteralMessageBuilder object containing the given
     * SPI object.
     */
    protected LiteralMessageBuilder(LiteralMessageBuilderSpi builderSpi,
                                    Provider provider, String format)
    {
        this.spi      = builderSpi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an LiteralMessageBuilder that implements the given format.
     */
    public static LiteralMessageBuilder getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("LiteralMessageBuilder", 
                                               format);
        return new LiteralMessageBuilder( (LiteralMessageBuilderSpi)o[0],
                                          (Provider)o[1], format);
    }
    

    /**
     * Returns an LiteralMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static LiteralMessageBuilder getInstance(String format, 
                                                    String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation("LiteralMessageBuilder", 
                                               format, provider);
        return new LiteralMessageBuilder( (LiteralMessageBuilderSpi)o[0],
                                          (Provider)o[1], format);
    }
    

    /**
     * Returns an LiteralMessageBuilder from the given provider that 
     * implements the given format.
     */
    public static LiteralMessageBuilder getInstance(String format, 
                                                    Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation("LiteralMessageBuilder", 
                                               format, provider);
        return new LiteralMessageBuilder( (LiteralMessageBuilderSpi)o[0],
                                          (Provider)o[1], format);
    }
    

// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Initializes this builder with the given binary data.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(byte[] contents) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, new SecureRandom());
    }
    

    /**
     * Initializes this builder with the given binary data and SecureRandom
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(byte[] contents, SecureRandom sr) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, sr);
    }
    

    /**
     * Initializes this builder with the given text data.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(String contents) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, new SecureRandom());
    }
    

    /**
     * Initializes this builder with the given text data and SecureRandom 
     * object.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws MessageException on a variety of format specific problems.
     */
    public final void init(String contents, SecureRandom sr) 
        throws IllegalStateException, MessageException
    {
        this.spi.engineInit(contents, sr);
    }
    

    /**
     * Set a format specific attribute.
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageException on a variety of format specific problems.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     */
    public final void setAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, MessageException
    {
        this.spi.engineSetAttribute(name, attr);
    }


    /**
     * Returns the built literal message
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly, or if multiple calls to this build() method are made.
     * @throws MessageException on a variety of format specific problems.
     */
    public final Message build() 
        throws IllegalStateException, MessageException
    {
        return this.spi.engineBuild();
    }
        
}
