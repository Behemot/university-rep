/* $Id: VerificationKeyCallback.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


/**
 * Interface for the verification key callback function
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public interface VerificationKeyCallback
{
    
    /**
     * Callback function for retrieving a verification key 
     *
     * <p>
     * Note: when verification fails, this method will be called again. This is
     * useful when multiple keys need to be tried for verification. Use the FAIL
     * error in the VerificationKeyReturnValue in that case to indicate that no 
     * more tries are needed.
     * </p>
     *
     * @param request information about the request
     */
    public VerificationKeyReturnValue getVerificationKey(
                                          VerificationKeyRequest request);
}
