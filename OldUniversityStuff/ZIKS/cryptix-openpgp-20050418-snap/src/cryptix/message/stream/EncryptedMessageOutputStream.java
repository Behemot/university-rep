/* $Id: EncryptedMessageOutputStream.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.io.IOException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;

import javax.crypto.SecretKey;


/**
 * A stream that encrypts another stream.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public final class EncryptedMessageOutputStream extends MessageOutputStream {
    

// Constructor
// ..........................................................................

    /**
     * Create a new EncryptedMessageOutputStream object containing the given
     * SPI object.
     */
    protected EncryptedMessageOutputStream
        (MessageOutputStreamSpi spi, Provider provider, String format)
    {
        super(spi, provider, format);
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an EncryptedMessageOutputStream that implements the given format.
     */
    public static EncryptedMessageOutputStream getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "EncryptedMessageOutputStream", format);
        return new EncryptedMessageOutputStream(
            (EncryptedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an EncryptedMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static EncryptedMessageOutputStream getInstance
        (String format, String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation(
            "EncryptedMessageOutputStream", format, provider);
        return new EncryptedMessageOutputStream(
            (EncryptedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an EncryptedMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static EncryptedMessageOutputStream getInstance
        (String format, Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "EncryptedMessageOutputStream", format, provider);
        return new EncryptedMessageOutputStream(
            (EncryptedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

// Wrapped SPI functions
// ..........................................................................

    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void addRecipient(PublicKey publickey)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        ((EncryptedMessageOutputStreamSpi)spi).engineAddRecipient(publickey);
    }


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void addRecipient(KeyBundle bundle)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        ((EncryptedMessageOutputStreamSpi)spi).engineAddRecipient(bundle);
    }


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void addRecipient(SecretKey symmetrickey)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        ((EncryptedMessageOutputStreamSpi)spi).engineAddRecipient(symmetrickey);
    }


    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void addRecipient(String passphrase)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException
    {
        ((EncryptedMessageOutputStreamSpi)spi).engineAddRecipient(passphrase);
    }
    
}
