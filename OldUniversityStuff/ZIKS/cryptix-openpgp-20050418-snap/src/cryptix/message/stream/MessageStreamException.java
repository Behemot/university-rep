/* $Id: MessageStreamException.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.io.IOException;


/**
 * This is the generic MessageStream exception.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class MessageStreamException extends IOException {


    /**
     * Constructs an exception without a specific error message.
     */
    public MessageStreamException () { super(); }


    /**
     * Constructs an exception with the specified error message.
     *
     * @param msg the error message.
     */
    public MessageStreamException (String msg) { super(msg); }

}
