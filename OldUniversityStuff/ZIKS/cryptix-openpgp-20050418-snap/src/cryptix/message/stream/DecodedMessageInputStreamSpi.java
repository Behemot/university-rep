/* $Id: DecodedMessageInputStreamSpi.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.io.InputStream;
import java.io.IOException;


/**
 * Service provider interface for DecodedMessageInputStream
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public interface DecodedMessageInputStreamSpi {

    public void engineInit(InputStream in, DecryptionKeyCallback dkc, 
                                                  VerificationKeyCallback vkc)
        throws IllegalStateException, IOException, MessageStreamException;

    
    public int engineRead()
        throws IOException;


    public int engineRead(byte[] b, int off, int len) 
        throws IOException;


    public void engineClose()
        throws IOException;

    
    public int engineGetVerificationResult()
        throws IllegalStateException;


    public int engineGetIntegrityResult()
        throws IllegalStateException;

}
