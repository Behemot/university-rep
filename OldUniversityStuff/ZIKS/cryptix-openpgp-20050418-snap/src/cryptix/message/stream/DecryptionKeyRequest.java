/* $Id: DecryptionKeyRequest.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyID;


/**
 * Request for the decryption key callback function
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class DecryptionKeyRequest
{
    
// Variables
//..............................................................................

    /** An array of key id's that could possible have signed the message 
     *  or null if no such information is available. */
    private KeyID[] keyidhints;
    /** The number of previous calls to the callback function */
    private int retrycount;
    /** The previous result, if retrycount >= 1, or null if retrycount == 0 */
    private DecryptionKeyReturnValue lastresult;


// Constructor
//..............................................................................

    public DecryptionKeyRequest(KeyID[] keyidhints, int retrycount,
                                DecryptionKeyReturnValue lastresult)
    {
        this.keyidhints = keyidhints;
        this.retrycount = retrycount;
        this.lastresult = lastresult;
    }
    

// Getters
//..............................................................................

    /**
     * Returns an array of key id's that could possible have encrypted the 
     * message or null if no such information is available.
     */
    public KeyID[] getKeyIDHints() { return keyidhints; }
    
    /**
     * Returns the number of previous calls to the callback function
     */
    public int getRetryCount() { return retrycount; }
    
    /**
     * Returns the previous result, if getRetryCount() >= 1.
     * 
     * Returns null when getRetryCount() == 1.
     */
    public DecryptionKeyReturnValue getLastResult() { return lastresult; }
    
}
