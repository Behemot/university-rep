/* $Id: LiteralMessageOutputStream.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;


/**
 * Class that encapsulates a generic data stream into a stream of a specific
 * format.
 *
 * <p>
 * See it as some kind of format specific container for literal data, after 
 * which the stream becomes ready for handling by other cryptographic 
 * primitives, like signing by a SignedMessageOutputStream or encryption by an
 * EncryptedMessageOutputStream (or both).
 * </p>
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public final class LiteralMessageOutputStream extends MessageOutputStream {
    

// Constructor
// ..........................................................................

    /**
     * Create a new LiteralMessageOutputStream object containing the given
     * SPI object.
     */
    protected LiteralMessageOutputStream
        (MessageOutputStreamSpi spi, Provider provider, String format)
    {
        super(spi, provider, format);
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an LiteralMessageOutputStream that implements the given format.
     */
    public static LiteralMessageOutputStream getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "LiteralMessageOutputStream", format);
        return new LiteralMessageOutputStream(
            (LiteralMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an LiteralMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static LiteralMessageOutputStream getInstance
        (String format, String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation(
            "LiteralMessageOutputStream", format, provider);
        return new LiteralMessageOutputStream(
            (LiteralMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an LiteralMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static LiteralMessageOutputStream getInstance
        (String format, Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "LiteralMessageOutputStream", format, provider);
        return new LiteralMessageOutputStream(
            (LiteralMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    
}
