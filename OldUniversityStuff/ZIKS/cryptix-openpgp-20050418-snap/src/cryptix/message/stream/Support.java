/* $Id: Support.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1995-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 *
 * Use, modification, copying and distribution of this software is subject
 * the terms and conditions of the Cryptix General Licence. You should have
 * received a copy of the Cryptix General Licence along with this library;
 * if not, you can download a copy from http://www.cryptix.org/ .
 */
package cryptix.message.stream;


import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;


/**
 * Support class for finding implementations of a particular algorithm or format
 * within a provider.
 *
 * <p>Taken from Cryptix JCE, package javax.crypto, cvs file version 1.5.</p>
 *
 * @author Jeroen C. van Gelderen <gelderen@cryptix.org>
 * @author Edwin Woudt <edwin@cryptix.org>
 */
/*package*/ final class Support
{
    /*package*/ static Object[]
    getImplementation(String type, String algorithm)
    throws NoSuchAlgorithmException
    {
        Provider[] providers = Security.getProviders();
        if( (providers==null) || (providers.length==0) )
            throw new NoSuchAlgorithmException("No providers installed");

        for(int i=0; i<providers.length; i++)
        {
            try {
                Object[] res = getImplementation(type, algorithm, providers[i]);
                return res;
            } catch (NoSuchAlgorithmException nsae) {
                // try next
            }
        }

        throw new NoSuchAlgorithmException(
            "Algorithm not found. [" + type + "." + algorithm + "]");
    }


    /*package*/ static Object[]
    getImplementation(String type, String algorithm, String provider)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Provider p = Security.getProvider(provider);
        if(p==null)
            throw new NoSuchProviderException(
                "Provider not found. [" + provider + "]");

        Object[] res = getImplementation(type, algorithm, p);
        return res;
    }


    /*package*/ static Object[]
    getImplementation(String type, String algorithm, Provider provider)
    throws NoSuchAlgorithmException
    {
        try
        {
            String class_name =
                provider.getProperty("Alg.Alias." + type + "." + algorithm);

            if(class_name==null)
                class_name = provider.getProperty(type + "." + algorithm);
            else
                class_name = provider.getProperty(type + "." + class_name);

            if(class_name == null)
                throw new NoSuchAlgorithmException(
                    "Algorithm not found. [" + type + "." + algorithm + "]");

            Object[] res = new Object[2];
            res[0] = Class.forName(class_name).newInstance();
            res[1] = provider;
            return res;
        }
        catch(LinkageError e)
        {
            throw new NoSuchAlgorithmException(
                "Error initializing class. ["+type+"."+algorithm+"]");
        }
        catch(ClassNotFoundException e)
        {
            throw new NoSuchAlgorithmException(
                "Registered class not found. ["+type+"."+algorithm+"]");
        }
        catch(InstantiationException e)
        {
            throw new NoSuchAlgorithmException(
                "Error initializing class. ["+type+"."+algorithm+"]");
        }
        catch(IllegalAccessException e)
        {
            throw new NoSuchAlgorithmException(
                "Error initializing class. ["+type+"."+algorithm+"]");
        }
    }



    /*package*/ static Object[]
    getClassName(String type, String algorithm)
    throws NoSuchAlgorithmException
    {
        Provider[] providers = Security.getProviders();
        if( (providers==null) || (providers.length==0) )
            throw new NoSuchAlgorithmException("No providers installed");

        for(int i=0; i<providers.length; i++)
        {
            try {
                Object[] res = getClassName(type, algorithm, providers[i]);
                return res;
            } catch (NoSuchAlgorithmException nsae) {
                // try next
            }
        }

        throw new NoSuchAlgorithmException(
            "Algorithm not found. [" + type + "." + algorithm + "]");
    }


    /*package*/ static Object[]
    getClassName(String type, String algorithm, String provider)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Provider p = Security.getProvider(provider);
        if(p==null)
            throw new NoSuchProviderException(
                "Provider not found. [" + provider + "]");

        Object[] res = getClassName(type, algorithm, p);
        return res;
    }


    /*package*/ static Object[]
    getClassName(String type, String algorithm, Provider provider)
    throws NoSuchAlgorithmException
    {
        String class_name =
            provider.getProperty("Alg.Alias." + type + "." + algorithm);

        if(class_name == null)
            class_name = provider.getProperty(type + "." + algorithm);
        else
            class_name = provider.getProperty(type + "." + class_name);

        if(class_name == null)
            throw new NoSuchAlgorithmException(
                "Algorithm not found. [" + type + "." + algorithm + "]");

        Object[] res = new Object[2];
        res[0] = class_name;
        res[1] = provider;
        return res;
    }

}
