/* $Id: MessageOutputStream.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.io.IOException;
import java.io.OutputStream;

import java.security.Provider;
import java.security.SecureRandom;


/**
 * A generic stream that encapsulates another stream in some message format.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public abstract class MessageOutputStream extends OutputStream {
    

// Instance variables
// ..........................................................................

    protected final MessageOutputStreamSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
// ..........................................................................

    /**
     * Create a new MessageOutputStream object containing the given
     * SPI object.
     */
    protected MessageOutputStream(MessageOutputStreamSpi spi,
                                  Provider provider, String format)
    {
        this.spi      = spi;
        this.provider = provider;
        this.format   = format;
    }


// Non-SPI getters
// ..........................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
// ..........................................................................

    /**
     * Initializes this outputstream with the given outputstream as underlying
     * stream and the given SecureRandom object.
     *
     * @param out The underlying outputstream this stream will write results to.
     * @param sr  A SecureRandom object used for any randomness needed.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void init(OutputStream out, SecureRandom sr)
        throws IllegalStateException, IOException, MessageStreamException
    {
        this.spi.engineInit(out, sr);
    }
        
    /**
     * Set a format specific attribute.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void setAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageStreamException
    {
        this.spi.engineSetAttribute(name, attr);
    }
               
    /**
     * Write the specified part of a bytearray to the stream.
     *
     * @param b    the data
     * @param off  the starting point of the write operation
     * @param len  how many bytes to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void write(byte[] b, int off, int len)
        throws IOException, MessageStreamException
    {
        this.spi.engineWrite(b, off, len);
    }

    /**
     * Write a complete bytearray to the stream
     *
     * @param b  the data
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void write(byte[] b)
        throws IOException, MessageStreamException
    {
        this.spi.engineWrite(b, 0, b.length);
    }
    
    /**
     * Write a single byte to the stream
     *
     * @param b  the byte to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void write(int b)
        throws IOException, MessageStreamException
    {
        this.spi.engineWrite(b);
    }
    
    /**
     * Flush the stream
     *
     * <p>
     * What flushing does is format specific. The general contract is that as
     * much bytes as possible are written to the underlying datastream, but no
     * guarantees are given about any buffers left. The reason for this is 
     * because cryptographic protocols usually work on specific blocksizes and
     * there usually are only handlers (padding) for incomplete blocks at the
     * end of the stream.
     * </p><p>
     * This method should call the flush() method of the underlying outputstream
     * after it is done.
     * </p>
     * 
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void flush()
        throws IOException, MessageStreamException
    {
        this.spi.engineFlush();
    }
    
    /**
     * Close the stream
     *
     * <p>
     * Sends all data through the underlying stream and then calls the close()
     * method of the underlying stream.
     * </p>
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void close()
        throws IOException, MessageStreamException
    {
        this.spi.engineClose();
    }
    
    /**
     * Return the underlying service provider interface
     *
     * <p>
     * This method should not be called by user applications.
     * </p>
     */
    public final MessageOutputStreamSpi getSpi()
    {
        return spi;
    }
    
}
