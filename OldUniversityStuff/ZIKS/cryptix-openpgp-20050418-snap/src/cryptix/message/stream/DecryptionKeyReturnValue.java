/* $Id: DecryptionKeyReturnValue.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.security.PrivateKey;

import javax.crypto.SecretKey;


/**
 * Return value for the decryption key callback function
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class DecryptionKeyReturnValue
{
    
// Constants
//..............................................................................

    /** No error */    
    public static final int NOERROR = 0;
    /** Fail the complete operation, i.e. throw an exception */
    public static final int FAIL    = 1;
    
    
// Variables
//..............................................................................

    /** Error value, one of NOERROR or FAIL */
    private int error;
    /** Symmetric secret key, or null in case of a different decryption key or 
      * an error */
    private SecretKey seckey;
    /** Asymmetric private key, or null in case of a different decryption key or 
      * an error */
    private PrivateKey privkey;
    /** Key bundle, or null in case of a different decryption key or an error */
    private KeyBundle bundle;
    /** Passphrase, only used for the keybundle */
    private char[] passphrase;
        
    
// Constructors
//..............................................................................

    /**
     * Construct a DecryptionKeyReturnValue with the given error.
     *
     * For NOERROR, use one of the other constructors
     */
    public DecryptionKeyReturnValue(int error)
    {
        if ((error <= 0) || (error > 1))
            throw new IllegalArgumentException("Invalid error");
        this.error = error;
    }
    
    /**
     * Construct a DecryptionKeyReturnValue with the given symmetric secret key.
     */
    public DecryptionKeyReturnValue(SecretKey seckey)
    {
        if (seckey == null)
            throw new IllegalArgumentException("Null key");
        this.error = NOERROR;
        this.seckey = seckey;
    }
    
    /**
     * Construct a DecryptionKeyReturnValue with the given asymmetric private 
     * key.
     */
    public DecryptionKeyReturnValue(PrivateKey privkey)
    {
        if (privkey == null)
            throw new IllegalArgumentException("Null key");
        this.error = NOERROR;
        this.privkey = privkey;
    }
    
    /**
     * Construct a DecryptionKeyReturnValue with the given keybundle and
     * passphrase.
     */
    public DecryptionKeyReturnValue(KeyBundle bundle, char[] passphrase)
    {
        if (bundle == null)
            throw new IllegalArgumentException("Null bundle");
        this.error = NOERROR;
        this.bundle = bundle;
        this.passphrase = passphrase;
    }


    /**
     * Construct a DecryptionKeyReturnValue with the given passphrase.
     *
     * Used for passphrase based encryption.
     */
    public DecryptionKeyReturnValue(char[] passphrase)
    {
        if (bundle == null)
            throw new IllegalArgumentException("Null bundle");
        this.error = NOERROR;
        this.bundle = bundle;
        this.passphrase = passphrase;
    }


// Getters
//..............................................................................

    /**
     * Return the error value.
     *
     * This is one of NOERROR or FAIL.
     */
    public int getError() { return error; }
    
    /**
     * Return the symmetric secret key.
     *
     * Returns null if there is an error or when there is a different decryption
     * key
     */
    public SecretKey getSecretKey() { return seckey; }

    /**
     * Return the symmetric secret key.
     *
     * Returns null if there is an error or when there is a different decryption
     * key
     */
    public PrivateKey getPrivateKey() { return privkey; }

    /**
     * Return the key bundle.
     *
     * Returns null if there is an error or when there is a different decryption
     * key
     */
    public KeyBundle getBundle() { return bundle; }
    
    /**
     * Return the passphrase.
     *
     * The passphrase is only used when this object contains a KeyBundle or for
     * passphrase based encryption. It will return null otherwise.
     */
    public char[] getPassphrase() { return passphrase; }
    
}
