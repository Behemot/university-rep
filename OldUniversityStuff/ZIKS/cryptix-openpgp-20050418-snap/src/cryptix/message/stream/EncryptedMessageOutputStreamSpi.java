/* $Id: EncryptedMessageOutputStreamSpi.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.io.IOException;

import java.security.PublicKey;
import java.security.UnrecoverableKeyException;

import javax.crypto.SecretKey;


/**
 * Service provider interface for EncryptedMessageOutputStream
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public interface EncryptedMessageOutputStreamSpi 
    extends MessageOutputStreamSpi
{

    /**
     * Adds a public key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(PublicKey publickey)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException;


    /**
     * Adds a keybundle recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to public keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(KeyBundle bundle)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException;


    /**
     * Adds a symmetric key recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to symmetric keys.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(SecretKey symmetrickey)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException;


    /**
     * Adds a passphrase recipient
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if the build() method has been called
     * @throws UnsupportedOperationException if this particular format does not
     *         support encrypting messages to passphrases.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddRecipient(String passphrase)
        throws IllegalStateException, UnsupportedOperationException,
               IOException, MessageStreamException;

}
