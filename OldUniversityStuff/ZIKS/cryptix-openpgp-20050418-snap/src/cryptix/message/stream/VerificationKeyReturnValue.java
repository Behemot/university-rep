/* $Id: VerificationKeyReturnValue.java,v 1.1 2005/03/29 11:22:48 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.security.PublicKey;


/**
 * Return value for the verification key callback function
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public class VerificationKeyReturnValue
{
    
// Constants
//..............................................................................

    /** No error */    
    public static final int NOERROR = 0;
    /** Fail the complete operation, i.e. throw an exception */
    public static final int FAIL    = 1;
    /** Ignore the verification, just return the data */
    public static final int IGNORE  = 2;
    
    
// Variables
//..............................................................................

    /** Error value, one of NOERROR, FAIL or IGNORE */
    private int error;
    /** Public key, or null in case of a bundle or an error */
    private PublicKey pubkey;
    /** Key bundle, or null in case of a key or an error */
    private KeyBundle bundle;
    
    
// Constructors
//..............................................................................

    /**
     * Construct a VerificationKeyReturnValue with the given error.
     *
     * For NOERROR, use one of the other constructors
     */
    public VerificationKeyReturnValue(int error)
    {
        if ((error <= 0) || (error > 2))
            throw new IllegalArgumentException("Invalid error");
        this.error = error;
    }
    
    /**
     * Construct a VerificationKeyReturnValue with the given public key.
     */
    public VerificationKeyReturnValue(PublicKey pubkey)
    {
        if (pubkey == null)
            throw new IllegalArgumentException("Null key");
        this.error = NOERROR;
        this.pubkey = pubkey;
    }
    
    /**
     * Construct a VerificationKeyReturnValue with the given keybundle.
     */
    public VerificationKeyReturnValue(KeyBundle bundle)
    {
        if (bundle == null)
            throw new IllegalArgumentException("Null bundle");
        this.error = NOERROR;
        this.bundle = bundle;
    }


// Getters
//..............................................................................

    /**
     * Return the error value.
     *
     * This is one of NOERROR, FAIL or IGNORE.
     */
    public int getError()     { return error;  }
    
    /**
     * Return the public key.
     *
     * Returns null if there is an error or when a keybundle is the result
     */
    public PublicKey getPublicKey() { return pubkey; }

    /**
     * Return the key bundle.
     *
     * Returns null if there is an error or when a key is the result
     */
    public KeyBundle getBundle()    { return bundle; }
    
}
