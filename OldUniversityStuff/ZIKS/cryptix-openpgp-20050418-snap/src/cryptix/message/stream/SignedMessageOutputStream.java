/* $Id: SignedMessageOutputStream.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.io.IOException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.UnrecoverableKeyException;


/**
 * A stream that adds signs and appends the signature to another stream.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public final class SignedMessageOutputStream extends MessageOutputStream {
    

// Constructor
// ..........................................................................

    /**
     * Create a new SignedMessageOutputStream object containing the given
     * SPI object.
     */
    protected SignedMessageOutputStream
        (MessageOutputStreamSpi spi, Provider provider, String format)
    {
        super(spi, provider, format);
    }


// getInstance methods
// ..........................................................................

    /**
     * Returns an SignedMessageOutputStream that implements the given format.
     */
    public static SignedMessageOutputStream getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "SignedMessageOutputStream", format);
        return new SignedMessageOutputStream(
            (SignedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an SignedMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static SignedMessageOutputStream getInstance
        (String format, String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation(
            "SignedMessageOutputStream", format, provider);
        return new SignedMessageOutputStream(
            (SignedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an SignedMessageOutputStream from the given provider that 
     * implements the given format.
     */
    public static SignedMessageOutputStream getInstance
        (String format, Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "SignedMessageOutputStream", format, provider);
        return new SignedMessageOutputStream(
            (SignedMessageOutputStreamSpi)o[0],
            (Provider)o[1], format);
    }

    
// Wrapped SPI functions
// ..........................................................................


    /**
     * Adds a signer
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public final void addSigner(PrivateKey signingkey)
        throws IllegalStateException, IOException, MessageStreamException
    {
        ((SignedMessageOutputStreamSpi)spi).engineAddSigner(signingkey);
    }


    /**
     * Adds a signer from a keybundle, decrypting it with the given passphrase.
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     */
    public final void addSigner(KeyBundle signingkey, char[] passphrase)
        throws IllegalStateException, IOException, MessageStreamException,
               UnrecoverableKeyException
    {
        ((SignedMessageOutputStreamSpi)spi).engineAddSigner(
            signingkey, passphrase);
    }

}
