/* $Id: SignedMessageOutputStreamSpi.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import cryptix.pki.KeyBundle;

import java.io.IOException;

import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;


/**
 * Service provider interface for SignedMessageOutputStream
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public interface SignedMessageOutputStreamSpi 
    extends MessageOutputStreamSpi
{

    /**
     * Adds a signer
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineAddSigner(PrivateKey signingkey)
        throws IllegalStateException, IOException, MessageStreamException;


    /**
     * Adds a signer from a keybundle, decrypting it with the given passphrase.
     *
     * @throws InvalidStateException if this message has not been initialized 
     *         properly or if data has already been streamed
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     * @throws UnrecoverableKeyException if the private key cannot be retrieved
     *         from the keybundle (for example because of an incorrect 
     *         passphrase).
     */
    public void engineAddSigner(KeyBundle signingkey, char[] passphrase)
        throws IllegalStateException, IOException, MessageStreamException,
               UnrecoverableKeyException;

}
