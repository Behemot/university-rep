/* $Id: MessageOutputStreamSpi.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.io.IOException;
import java.io.OutputStream;

import java.security.SecureRandom;


/**
 * Service provider interface for MessageOutputStream
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public interface MessageOutputStreamSpi {

    /**
     * Initializes this outputstream with the given outputstream as underlying
     * stream and the given SecureRandom object.
     *
     * @param out The underlying outputstream this stream will write results to.
     * @param sr  A SecureRandom object used for any randomness needed.
     *
     * @throws IllegalStateException if this message has been initialized 
     *         before.
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineInit(OutputStream out, SecureRandom sr)
        throws IllegalStateException, IOException, MessageStreamException;
        
    /**
     * Set a format specific attribute.
     *
     * @param name a name identifying the attribute
     * @param attr the attribute itself
     *
     * @throws IllegalStateException if this message has not been initialized 
     *         before.
     * @throws IllegalArgumentException if the attribute is not supported or the
     *         given object does not have the right type.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineSetAttribute(String name, Object attr)
        throws IllegalStateException, IllegalArgumentException, 
               MessageStreamException;

    /**
     * Write the specified part of a bytearray to the stream.
     *
     * @param b    the data
     * @param off  the starting point of the write operation
     * @param len  how many bytes to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b, int off, int len)
        throws IOException, MessageStreamException;

    /**
     * Write a complete bytearray to the stream
     *
     * @param b  the data
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(byte[] b)
        throws IOException, MessageStreamException;
    
    /**
     * Write a single byte to the stream
     *
     * @param b  the byte to write
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineWrite(int b)
        throws IOException, MessageStreamException;
    
    /**
     * Flush the stream
     *
     * <p>
     * What flushing does is format specific. The general contract is that as
     * much bytes as possible are written to the underlying datastream, but no
     * guarantees are given about any buffers left. The reason for this is 
     * because cryptographic protocols usually work on specific blocksizes and
     * there usually are only handlers (padding) for incomplete blocks at the
     * end of the stream.
     * </p><p>
     * This method should call the flush() method of the underlying outputstream
     * after it is done.
     * </p>
     * 
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineFlush()
        throws IOException, MessageStreamException;
    
    /**
     * Close the stream
     *
     * <p>
     * Sends all data through the underlying stream and then calls the close()
     * method of the underlying stream.
     * </p>
     *
     * @throws IOException on a general IOException that is not format specific.
     * @throws MessageStreamException on a variety of format specific problems.
     */
    public void engineClose()
        throws IOException, MessageStreamException;
    
}
