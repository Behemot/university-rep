/* $Id: DecodedMessageInputStream.java,v 1.1 2005/03/29 11:22:47 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message.stream;


import java.io.InputStream;
import java.io.IOException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;


/**
 * A stream that decodes, decrypts and verifies messages
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.1 $
 */
public final class DecodedMessageInputStream 
    extends InputStream
{

// Constants
//..............................................................................

    public static final int VERIFICATION_NOT_SIGNED     = 1;
    public static final int VERIFICATION_GOOD_SIGNATURE = 2;
    public static final int VERIFICATION_BAD_SIGNATURE  = 3;
    
    public static final int INTEGRITY_NOT_PROTECTED = 1;
    public static final int INTEGRITY_GOOD          = 2;
    public static final int INTEGRITY_VIOLATED      = 3;


// Instance variables
//..............................................................................

    protected final DecodedMessageInputStreamSpi spi;
    private final Provider provider;
    private final String format;
    

// Constructor
//..............................................................................

    /**
     * Create a new DecodedMessageInputStream object containing the given
     * SPI object.
     */
    protected DecodedMessageInputStream(DecodedMessageInputStreamSpi spi,
                                        Provider provider, String format)
    {
        this.spi      = spi;
        this.provider = provider;
        this.format   = format;
    }


// getInstance methods
//..............................................................................

    /**
     * Returns an DecodedMessageInputStream that implements the given format.
     */
    public static DecodedMessageInputStream getInstance(String format) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "DecodedMessageInputStream", format);
        return new DecodedMessageInputStream(
            (DecodedMessageInputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an DecodedMessageInputStream from the given provider that 
     * implements the given format.
     */
    public static DecodedMessageInputStream getInstance
        (String format, String provider) 
        throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Object[] o = Support.getImplementation(
            "DecodedMessageInputStream", format, provider);
        return new DecodedMessageInputStream(
            (DecodedMessageInputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

    /**
     * Returns an DecodedMessageInputStream from the given provider that 
     * implements the given format.
     */
    public static DecodedMessageInputStream getInstance
        (String format, Provider provider) 
        throws NoSuchAlgorithmException
    {
        Object[] o = Support.getImplementation(
            "DecodedMessageInputStream", format, provider);
        return new DecodedMessageInputStream(
            (DecodedMessageInputStreamSpi)o[0],
            (Provider)o[1], format);
    }
    

// Non-SPI getters
//..............................................................................

    /**
     * Returns the provider of this object.
     */
    public final Provider getProvider() {
        return this.provider;
    }
    

    /**
     * Returns the name of the format of this object.
     */
    public final String getFormat() {
        return this.format;
    }


// Wrapped SPI functions
//..............................................................................

    public final void init(InputStream in, DecryptionKeyCallback dkc, 
                                           VerificationKeyCallback vkc)
        throws IllegalStateException, IOException, MessageStreamException
    {
        this.spi.engineInit(in, dkc, vkc);
    }

    public final int read()
        throws IOException
    {
        return this.spi.engineRead();
    }
    
    public final int read(byte[] b, int off, int len) 
        throws IOException
    {
        return this.spi.engineRead(b, off, len);
    }

    public final void close()
        throws IOException
    {
        this.spi.engineClose();
    }
    
    public final int getVerificationResult()
        throws IllegalStateException
    {
        return this.spi.engineGetVerificationResult();
    }

    public final int getIntegrityResult()
        throws IllegalStateException
    {
        return this.spi.engineGetIntegrityResult();
    }

}
