/* $Id: SignedMessage.java,v 1.4 2005/04/18 12:12:28 woudt Exp $
 *
 * Copyright (C) 1999-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 * 
 * Use, modification, copying and distribution of this software is subject 
 * the terms and conditions of the Cryptix General Licence. You should have 
 * received a copy of the Cryptix General License along with this library; 
 * if not, you can download a copy from http://www.cryptix.org/ .
 */

package cryptix.message;


import cryptix.pki.KeyBundle;
import cryptix.pki.KeyID;

import java.security.PublicKey;


/**
 * Represents a message that is signed by one or more public keys.
 *
 * @author Edwin Woudt <edwin@cryptix.org>
 * @version $Revision: 1.4 $
 */
public abstract class SignedMessage extends Message {


// Constructor
// ..........................................................................

    /**
     * Creates a SignedMessage of the specified format.
     */
    public SignedMessage(String format) {
        super(format);
    }


// Own methods
// ..........................................................................

    /**
     * Get the message itself
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract Message getContents() throws MessageException;
    

    /**
     * Verify if this message was correctly signed by a particular public key.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @return true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public abstract boolean verify(PublicKey pubkey) throws MessageException;
        

    /**
     * Verify if this message was correctly signed by a particular keybundle.
     *
     * @throws MessageException on a variety of format specific problems.
     *
     * @return true if the message is correctly signed by the public key,
     *          false if the signature is invalid or if this message was not 
     *          signed at all by this public key.
     */
    public abstract boolean verify(KeyBundle pubkey) throws MessageException;
        

    /**
     * Returns hints for which key was used to sign this message
     *
     * @throws MessageException on a variety of format specific problems.
     */
    public abstract KeyID[] getKeyIDHints() throws MessageException;

}
