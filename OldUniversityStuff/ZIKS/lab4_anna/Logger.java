package security;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;

public class Logger {
	private String logFile_="Log.txt";
	private static Logger logger_;
	public static Logger getLogger(){
		if(logger_==null){
			logger_=new Logger();
		}
		return logger_;
	}
	public void writeLog(String S){
		try {
			BufferedWriter out=new BufferedWriter(new FileWriter(new File(logFile_),true));
			Date temp=new Date();
			out.write(temp.getDate()+"."+temp.getMonth()+"."+String.valueOf(temp.getYear()+1900)+" "
					+temp.getHours()+":"+temp.getMinutes()+" "+S);
			out.newLine();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
