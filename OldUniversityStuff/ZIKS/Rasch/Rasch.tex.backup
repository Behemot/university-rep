\documentclass[a4paper,12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[english,russian,ukrainian]{babel}
\usepackage[T2A]{fontenc}
\usepackage{indentfirst}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{titlesec}
\usepackage{titletoc}
\usepackage{listings}
\usepackage{lscape}
\usepackage{float}
\usepackage{listings}

\textwidth=6.9in
\textheight=10in
\oddsidemargin=0.7in
\voffset=-1in
\hoffset=-1in

\pagestyle{plain}

\newcommand{\titletop}{%
Національний технічний університет України ``Київський політехнічний інститут''\par
Факультет інформатики та обчислювальної техніки\par
Кафедра обчислювальної техніки}
\newcommand{\titlelineI}{Розрахункова робота\par
з курсу ``Захист інформації в комп'ютерних системах''}
\newcommand{\titlelineIII}{Виконав:\par
Михайленко А.В.\par
ФІОТ гр. ІО-72\par
№ Залікової книжки:\par
7211\par
\vspace{5mm}
}
\newcommand{\titlebottom}{Київ 2011}

\renewcommand{\maketitle}
{
  \thispagestyle{empty}
  \parbox[c][0.3\vsize][t]{\hsize}
  {\begin{center}\fontsize{13pt}{15pt}\selectfont\titletop\end{center}}

  \parbox[c][0.1\vsize][t]{\hsize}
  {\begin{center}\fontfamily{fma}\fontsize{18pt}{20pt}\selectfont\titlelineI\end{center}}


  \parbox[t][0.3\vsize][t]{\hsize}
  {
    \parbox[t]{0.4\hsize}{\raggedright}\hfill
    \parbox[t]{0.4\hsize}{\raggedright\fontsize{14pt}{15pt}\selectfont\titlelineIII}
  }

  \vfill
  \begin{center}\fontsize{13pt}{14pt}\selectfont\titlebottom\end{center}
}

%opening
\author{Anthony Mykhailenko}


\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Розділ 1. Розробка програми регістрації користувачів}
\subsection{Завдання}
Створити програму парольного доступу. На захищеному носії створюється системний журнал регістрації, що доступний тільки адміністратору, у якому фіксується інформація про облікові записи користувачів. Адміністратор фіксує логін, пароль, а також перевіряє пароль на відповідність вимогам (по довжині, словнику та часу використання). Окрім цього у реєстраційному журналі фіксується уся інформація, потрібна для аутентифікації користувачів. При певній кількості помилок авторизації обліковий запис користувача блокується.
\subsection{Варіант}
\begin{itemize}
\item Максимальна кількість помилок - 3.
\item Мінімальна довжина пароля - 5.
\item Функція аутентифікації - $\exp{a*x}$. Для простоти приймемо $a=2$.
\end{itemize}
\subsection{Приклад журналу реєстрації}
\begin{verbatim}
aaa 111111 a,c
account2 password2 N b
account1 password1 N b
account4 password4 N
account3 password3 N
account6 password6 N
account5 password5 N
account8 password8 N
account7 password7 N
account9 password9 N
admin 111111 Admin
\end{verbatim}
\subsection{Вигляд програми}
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Lab1.png}
\caption{Вид вікна вводу пароля}
\end{figure}
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Lab1-2.png}
\caption{Відмова у доступі}
\end{figure}



\subsection{Код}
\lstset{language=Java, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=3pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=PasswordManager.java]{part1/PasswordManager.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=LoginAuthenticatorChecker.java]{part1/LoginAuthenticatorChecker.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=LoginWindow.java]{part1/LoginWindow.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=LoginAuthenticationWindow.java]{part1/LoginAuthenticationWindow.java}

\pagebreak
\section{Розділ 2. Розробка програми керування доступом до захищеного носія}
\subsection{Завдання}
Створити програму, яка в режимі емуляції буде обмежувати доступ до захищених носіїв (визначених буквами латинського алфавіту А-Z) згідно з бінарними правами (має доступ / не має доступу). Інформація про права користувачів знаходиться у журналі реєстрації. Носії, що не доступні користувачу після входу у систему повинні або не відображатися, або при спробі доступу до них повинна бути надана відмова у доступі.
\subsection{Вигляд програми}
\begin{figure}[H]
\includegraphics[width=0.8\linewidth]{Lab2.png}
\caption{Вид вікна доступу до захищених носіїв}
\end{figure}
\subsection{Код}
\lstset{language=Java, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=3pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=FileTreeModel.java]{part1/FileTreeModel.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=AccessController.java]{part1/AccessController.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=MainWindowFactory.java]{part1/MainWindowFactory.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=UserWindow.java]{part1/UserWindow.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=AdminWindow.java]{part1/AdminWindow.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=AccountsTableModel.java]{part1/AccountsTableModel.java}
\pagebreak
\section{Розділ 3. Розробка програми аутентифікації}
\subsection{Завдання}
Розробити програму підтвердження аутентичності користувача, що працює за комп'ютером. З заданим інтервалом програма повинна задавати питання або видавати аргументи для секретної функції. Користувач повинен відповісти на питання або ввести результат секретної функції. При помилці аутентифікації система відмовляє у доступі користувачу. При певній кількості помилок аутентифікації обліковий запис користувача блокується.
\subsection{Варіант}
\begin{itemize}
\item Період аутентифікації - 60 секунд.
\item Кількість доступних помилок - 3.
\end{itemize}
\subsection{Вигляд програми}
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Lab3.png}
\caption{Вид вікна вікна аутентифікації}
\end{figure}
\subsection{Код}
\lstset{language=Java, basicstyle=\small, commentstyle=\tiny,numbers=left, numberstyle=\small, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=WorkAuthenticator.java]{part1/WorkAuthenticator.java}
\pagebreak
\section{Розділ 4. Розробка програми моніторингу безпеки}
\subsection{Завдання}
Створити програму, що забеспечує ведення операційного журналу, у якому фіксуються усі дії користувача (доступ до дисків та файлів, зміни паролів тощо). Згідно з цим журналом формуються списки небезпечних чи аномальних дій та шаблони поведінки користувачів.

\subsection{Приклад операційного журналу}
\begin{verbatim}
15.1.2011 14:7 Password error during login: aaa
15.1.2011 16:25 Account created: account99
15.1.2011 16:26 Account deleted: account99
15.1.2011 16:27 Authentication failed: admin
15.1.2011 16:38 User aaa. Accessed file: ./a/test.txt
15.1.2011 16:39 User aaa. Accessed file: ./c/test3.txt
15.1.2011 16:45 User aaa. Accessed file: ./a/test.txt
15.1.2011 16:45 User aaa. Accessed file: ./c/test3.txt
\end{verbatim}

\subsection{Код}
\lstset{language=Java, basicstyle=\small, commentstyle=\tiny,numbers=left, numberstyle=\small, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Logger.java]{part1/Logger.java}
\pagebreak
\section{Розділ 5. Розробка програми швидкого дискретного потенціювання}
\subsection{Завдання}
Розробити програму для реалізації швидкого дискретного потенціювання у алгоритмах RSA, El-Gamal та інших. Програма повинна оперувати великими числами (до декількох тисяч бітів).
\subsection{Вигляд програми}
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Lab5.png}
\caption{Вид вікна вікна аутентифікації}
\end{figure}

\subsection{Код}
\lstset{language=python, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Lab5.py]{part2/Lab5.py}
\pagebreak
\section{Розділ 6. Розробка генератора великих простих чисел}
\subsection{Завдання}
Створити програму, яка буде генерувати прості числа довільної розрядності (до декількох тисяч бітів) для генерації ключів у алгоритмах RSA, El-Gamal та інших. Перевірку на простоту виконувати за допомогою малої теореми Ферма, теста Мілера-Рабіна тощо.
\subsection{Вигляд програми}
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Lab6.png}
\caption{Вид вікна вікна аутентифікації}
\end{figure}

\subsection{Код}
\lstset{language=python, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Lab6.py]{part2/Lab6.py}
\pagebreak
\section{Розділ 7. Розробка програми керування ключами шифрування}
\subsection{Завдання}
Розробити програму генерації ключів та шифрування за алгоритмом RSA, використовуючи раніше створений алгоритм швидкого дискретного потенціювання та розширений алгоритм Евкліда.
\subsection{Алгоритм RSA}
\paragraph{Алгоритм создания открытого и секретного ключей}

RSA-ключи генерируются следующим образом:
\begin{enumerate}
\item Выбираются два случайных простых числа $p$ и $q$ заданного размера (например, 1024 бита каждое).
\item Вычисляется их произведение $n = pq$, которое называется модулем.
\item Вычисляется значение функции Эйлера от числа $n$: 
\[
    \Phi(n)=(p-1)(q-1)
\]


Выбирается целое число $e (1<e<\Phi(n))$, взаимно простое со значением функции . Обычно в качестве $e$ берут простые числа, содержащие небольшое количество единичных битов в двоичной записи, например, простые числа Ферма 17, 257 или 65537. 
\begin{itemize}
\item Число $e$ называется открытой экспонентой (англ. public exponent);
\item Время, необходимое для шифрования с использованием быстрого возведения в степень, пропорционально числу единичных бит в $e$;
\item Слишком малые значения $e$, например 3, потенциально могут ослабить безопасность схемы RSA.
\end{itemize}
\item Вычисляется число $d$, мультипликативно обратное к числу e по модулю $\Phi(n)$.
\begin{itemize}
\item Число d называется секретной экспонентой. Обычно, оно вычисляется при помощи расширенного алгоритма Евклида;
\item Пара $P = (e,n)$ публикуется в качестве открытого ключа RSA (англ. RSA public key);
\item Пара $S = (d,n)$ играет роль секретного ключа RSA (англ. RSA private key) и держится в секрете.
\end{itemize}
\end{enumerate}


\paragraph{Шифрование и расшифрование}

Предположим, сторона B хочет послать стороне A сообщение M.

Сообщением являются целые числа лежащие от 0 до (n-1).\
\textbf{Алгоритм шифрования:}
\begin{itemize}
\item Взять открытый ключ (e,n) стороны A; 
\item Взять открытый текст;
\item Передать шифрованное сообщение:
\[
    C=(M^e)mod (n)
\]

\end{itemize}
\textbf{Алгоритм расшифровки:}
\begin{itemize}
\item Принять зашифрованное сообщение C;
\item Применить свой секретный ключ (d,n) для расшифровки сообщения:
\[
    M=(C^d)mod (n)
\]

\end{itemize}


\subsection{Приклад шифрування}
\textbf{E: }65537

\textbf{D: }2999100126322269179600625560377832522327303539145789688875541229227178571510239931\\
6397494439836125238765488934348250923559853550474364492209005909085458673

\textbf{N: } 3145335653365059293062669184677260521935741591374581834530906473673573404401\\
8052891426806570785551582653865571304483327973439230262821647917319665142305407


\textbf{Повідомлення: } information

\textbf{Шифр: } 72949494142742772490213134196328203163217041917215324933808758229193975\\
63129450347833446631510623429275488739627402171730140810407795475450028828387629017

\subsection{Код}
\lstset{language=python, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Lab7.py]{part2/Lab7.py}
\pagebreak

\section{Розділ 8. Розробка програми формування сигнатури повідомлення}
\subsection{Завдання}
Розробити програму формування сигнатури з використанням заданого алгоритму шифрування.
\subsection{Варіант}
Варіант 11 - IDEA 
\subsection{Алгоритм IDEA}
\paragraph{Генерация ключей}
Из 128-битного ключа для каждого из восьми раундов шифрования генерируется по шесть 16-битных подключей, а для выходного преобразования генерируется четыре 16-битных подключа. Всего потребуется $52 = 8 \times 6 + 4$ различных подключей по 16 бит каждый. Процесс генерации пятидесяти двух 16-битных ключей заключается в следующем:
\begin{itemize}
\item Первым делом, 128-битный ключ разбивается на восемь 16-битных блоков. Это будут первые восемь подключей по 16 бит каждый.
\item Затем этот 128-битный ключ циклически сдвигается влево на 25 позиций, после чего новый 128-битный блок снова разбивается на восемь 16-битных блоков.
\item Процедура циклического сдвига и разбивки на блоки продолжается до тех пор, пока не будут сгенерированы все 52 16-битных подключа.
\end{itemize}

\paragraph{Шифрование}
 
Структура алгоритма IDEA показана на рисунке. Процесс шифрования состоит из восьми одинаковых раундов шифрования и одного выходного преобразования. Исходный незашифрованный текст делится на блоки по 64 бита. Каждый такой блок делится на четыре подблока по 16 бит каждый. На рисунке эти подблоки обозначены D1, D2, D3, D4. В каждом раунде используются свои подключи согласно таблице подключей. Над 16-битными подключами и подблоками незашифрованного текста производятся следующие операции:
\begin{itemize}
\item умножение по модулю $2^{16} + 1 = 65537$, причем вместо нуля используется $2^{16}$;
\item сложение по модулю $2^{16}$;
\item побитовое исключающее ИЛИ.
\end{itemize}
В конце каждого раунда шифрования имеется четыре 16-битных подблока, которые затем используются как входные подблоки для следующего раунда шифрования. Выходное преобразование представляет собой укороченный раунд, а именно, четыре 16-битных подблока на выходе восьмого раунда и четыре соответствующих подключа подвергаются операциям:
\begin{itemize}
\item умножение по модулю $2^{16} + 1$;
\item сложение по модулю $2^{16}$.
\end{itemize}
После выполнения выходного преобразования конкатенация подблоков $D1'$,$D2'$, $D3'$ и $D4'$ представляет собой зашифрованный текст. Затем берется следующий 64-битный блок незашифрованного текста и алгоритм шифрования повторяется. Так продолжается до тех пор, пока не зашифруются все 64-битные блоки исходного текста.

\begin{figure}[H]
    \includegraphics[width=0.5\linewidth]{idea.png}
\end{figure}

\paragraph{Расшифровка}
Метод вычисления, использующийся для расшифровки текста по существу такой же, как и при его шифровании. Единственное отличие состоит в том, что для расшифровки используются другие подключи. В процессе расшифровки подключи должны использоваться в обратном порядке. Первый и четвёртый подключи i-го раунда расшифровки получаются из первого и четвёртого подключа (10-i)-го раунда шифрования мультипликативной инверсией. Для 1-го и 9-го раундов второй и третий подключи расшифровки получаются из второго и третьего подключей 9-го и 1-го раундов шифрования аддитивной инверсией. Для раундов со 2-го по 8-й второй и третий подключи расшифровки получаются из третьего и второго подключей с 8-го по 2-й раундов шифрования аддитивной инверсией. Последние два подключа i-го раунда расшифровки равны последним двум подключам (9-i)-го раунда шифрования.
\subsection{Код}
\lstset{language=python, basicstyle=\tiny, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Lab8.py]{part2/Lab8.py}
\pagebreak

\begin{thebibliography}{20}
\bibitem{bib:1} Широчин В.П. ``Архитектура мышления и нейроинтелект'' К.: Юниор, 2004 - 560c .
\bibitem{bib:2} Широчин В.П., Широчин С.В., Мухин В.Е. ``Основи безпеки комп'ютерних систем '' К.: Корнейчук, 2009 - 288c .
\end{thebibliography}


\end{document}
