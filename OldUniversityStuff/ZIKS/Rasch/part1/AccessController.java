package security;

public class AccessController {
	private String accessRights_;
	private String login_;
	private static AccessController accessController_;
	
	public static AccessController getAccessController(){
		if(accessController_==null){
			accessController_=new AccessController();
		}
		return accessController_;
	}
	
	public void setRights(String accessRights){
		accessRights_=accessRights;
	}
	
	public String[] getRights(){
		return accessRights_.split(",");
	}
	
	public void setCurrentAccount(String login){
		login_=login;
	}
	
	public String getCurrentAccount(){
		return login_;
	}
	
	public void clearAccount(){
		login_=null;
	}
	
	
}
