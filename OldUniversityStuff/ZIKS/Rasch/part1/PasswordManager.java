package security;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class PasswordManager {
	private HashMap<Integer,String> logins_=new HashMap<Integer,String>();
	private HashMap<Integer,String> passwords_=new HashMap<Integer,String>();
	private HashMap<Integer,String> accessRights_=new HashMap<Integer,String>();
	private HashMap<Integer,Boolean> blockedStatus_=new HashMap<Integer,Boolean>();
	private HashMap<Integer,Integer> accessFails_=new HashMap<Integer,Integer>();
	private static String passwordFile_="passwords.txt";
	private static Integer maxTries_=3;
	private static Integer minPassword_=5;
	private static PasswordManager manager_;
	public static PasswordManager getPasswordManager(){
		if(manager_==null){
			manager_=new PasswordManager();
		}
		return manager_;
	}
	public static void reload(){
		manager_=new PasswordManager();
	}
	private  PasswordManager(){
		try{
			BufferedReader in=new BufferedReader(new FileReader(new File(passwordFile_)));
			String temp=in.readLine();
			while(temp!=null){
				
				String[] tokens=temp.split("\\s");
				logins_.put(tokens[0].hashCode(),tokens[0]);
				passwords_.put(tokens[0].hashCode(), tokens[1]);
				accessRights_.put(tokens[0].hashCode(), tokens[2]);
				if (tokens.length==4){
					blockedStatus_.put(tokens[0].hashCode(), true);
				}
				else{
					blockedStatus_.put(tokens[0].hashCode(), false);
				}
				temp=in.readLine();
			}
			in.close();
		}
		catch(FileNotFoundException NotFound){
			System.out.println("Passwords File Not Found");
		}
		catch(IOException IOE){
			System.out.println("IO Exception while reading passwords");
		}
	}
	
	public String checkPassword(String login, String password) throws LoginException{
		if (!passwords_.containsKey(login.hashCode())){
			Logger.getLogger().writeLog("Invalid login: "+login +" "+password);
			throw new LoginException("Unknown Login");
		}
		else{
			if(blockedStatus_.get(login.hashCode())==true){
				Logger.getLogger().writeLog("Attemt to login to blocked account: "+login);
				throw new LoginException("Account blocked. Contact your system administrator");
				
			}
			if(passwords_.get(login.hashCode()).compareTo(password)!=0){
				if(accessFails_.containsKey(login.hashCode())){
					Integer temp=accessFails_.get(login.hashCode());
					temp++;
					accessFails_.remove(login.hashCode());
					accessFails_.put(login.hashCode(), temp);
					if(temp>maxTries_){
						blockedStatus_.remove(login.hashCode());
						blockedStatus_.put(login.hashCode(), true);
						dumpPasswords();
						Logger.getLogger().writeLog("Maximum number of login tries exceeded." +
								"Account blocked: "+login);
						
						throw new LoginException("Maximum number of login tries exceeded. Account blocked");
					}
					else{
						Logger.getLogger().writeLog("Password error during login: "+login);
						throw new LoginException("Invalid Password");
					}
				} 
				else{
					accessFails_.put(login.hashCode(), 1);
					Logger.getLogger().writeLog("Password error during login: "+login);
					throw new LoginException("Invalid password");
				}
				
			}
			else{
				return accessRights_.get(login.hashCode());
			}
		}
	}
	
	public void registerAccount(String login,String password) throws LoginException{
		if(logins_.containsKey(login.hashCode())){
			throw new LoginException("Login already exists");
		}
		else{
			if(password.length()<minPassword_){
				throw new LoginException("Password too short");
			}
			logins_.put(login.hashCode(),login);
			passwords_.put(login.hashCode(), password);
			accessRights_.put(login.hashCode(), "N");
			blockedStatus_.put(login.hashCode(), false);
			dumpPasswords();
			Logger.getLogger().writeLog("Account created: "+login);
		}
	}
	
	
	
	private void dumpPasswords(){
		try{
			BufferedWriter out =new BufferedWriter(new FileWriter(new File(passwordFile_)));
			Iterator<Integer> it=logins_.keySet().iterator();
			while(it.hasNext()){
				int key=it.next();
				StringBuffer buf=new StringBuffer();
				buf.append(logins_.get(key)+" "+passwords_.get(key)+" "+accessRights_.get(key));
				if(blockedStatus_.get(key)==true){
					buf.append(" b");
				}
				out.write(buf.toString(),0,buf.toString().length());
				out.newLine();
			}
			out.close();
		}
		catch(FileNotFoundException NotFound){
			System.out.println("Passwords File Not Found");
		}
		catch(IOException IOE){
			System.out.println("IO Exception while reading passwords");
		}
	}
	
	public int getAccountCount(){
		return logins_.size();
	}
	
	public String getLogin(int i){
		Iterator<Entry<Integer,String>> it=logins_.entrySet().iterator();
		String res="";
		for(int j=0;j<i+1;j++){
			res=it.next().getValue();
			
		}
		return res;
	}
	
	public String getPassword(int i){
		Iterator<Entry<Integer,String>> it=passwords_.entrySet().iterator();
		String res="";
		for(int j=0;j<i+1;j++){
			res=it.next().getValue();
		}
		return res;
	}
	
	public String getAccessRights(int i){
		Iterator<Entry<Integer,String>> it=accessRights_.entrySet().iterator();
		String res="";
		for(int j=0;j<i+1;j++){
			res=it.next().getValue();
		}
		return res;
	}
	

	public Boolean getBlocked(int i){
		Iterator<Entry<Integer,Boolean>> it=blockedStatus_.entrySet().iterator();
		Boolean res=false;
		for(int j=0;j<i+1;j++){
			res=it.next().getValue();
		}
		return res;
	}
	
	public void blockUser(String login){
		blockedStatus_.remove(login.hashCode());
		blockedStatus_.put(login.hashCode(), true);
		dumpPasswords();
	}
	
	public void addInvalidTry(String login){
		System.out.println(accessFails_.get(login.hashCode()));
		if(accessFails_.containsKey(login.hashCode())){
			int temp=accessFails_.get(login.hashCode());
			temp++;
			accessFails_.remove(login.hashCode());
			accessFails_.put(login.hashCode(),temp);
			if (temp>maxTries_){
				blockUser(login);
			}
		}
		else{
			accessFails_.put(login.hashCode(),1);
		}
		
	}
	
	public void setLogin(String S,int i){
		Iterator<Entry<Integer,String>> it=logins_.entrySet().iterator();
		Entry<Integer,String> temp = null;
		for(int j=0;j<i+1;j++){
			temp=it.next();
		}
		temp.setValue(S);
		dumpPasswords();
	}
	
	public void setPassword(String S,int i){
		Iterator<Entry<Integer,String>> it=passwords_.entrySet().iterator();
		Entry<Integer,String> temp = null;
		for(int j=0;j<i+1;j++){
			temp=it.next();
		}
		temp.setValue(S);
		Logger.getLogger().writeLog("Password changed by administrator: "+getLogin(i));
		dumpPasswords();
	}
	
	public void setAccessRights(String S,int i){
		Iterator<Entry<Integer,String>> it=accessRights_.entrySet().iterator();
		Entry<Integer,String> temp = null;
		for(int j=0;j<i+1;j++){
			temp=it.next();
		}
		temp.setValue(S);
		Logger.getLogger().writeLog("Access rights changed by administrator: "+getLogin(i));
		dumpPasswords();
	}
	
	public void setBlocked(String S,int i){
		Iterator<Entry<Integer,Boolean>> it=blockedStatus_.entrySet().iterator();
		Entry<Integer,Boolean> temp = null;
		for(int j=0;j<i+1;j++){
			temp=it.next();
		}
		temp.setValue(Boolean.valueOf(S));
		dumpPasswords();
	}

	public void removeAccount(String login) throws LoginException{
		int hash=login.hashCode();
		if(logins_.containsKey(hash)){
			logins_.remove(hash);
			passwords_.remove(hash);
			accessRights_.remove(hash);
			blockedStatus_.remove(hash);
			dumpPasswords();
			Logger.getLogger().writeLog("Account deleted: "+login);
		}
		else{
			throw new LoginException("Unable to find account");
		}
	}
	
}
