package gui;

import java.awt.event.MouseEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.TableModel;
import javax.swing.JPopupMenu;

import security.AccessController;
import security.LoginException;

public class AdminWindow extends JFrame{
	private JMenuBar menuBar_=new JMenuBar();
	private JMenu accountMenu_=new JMenu("Account");
	private JMenuItem logoutMenuItem_=new JMenuItem("Log out");
	private JTable table_=new JTable();
	private JPopupMenu popup_=new JPopupMenu();
	private JMenuItem addAccount_=new JMenuItem("Add Account");
	private JMenuItem removeAccount_=new JMenuItem("Remove Account");
	
 	private class logoutListener extends MouseInputAdapter{
		private JFrame window_;
		public logoutListener(JFrame window){
			window_=window;
		}
		public void mousePressed(MouseEvent e){
			LoginWindow.getLoginWindow().reloadPasswords();
			LoginWindow.getLoginWindow().setVisible(true);
			AccessController.getAccessController().clearAccount();
			MainWindowFactory.clearFrames();
			window_.dispose();
			
		}
	}
	
	private class addListener extends MouseInputAdapter{
		private JTable table_;
		private JFrame window_;
		public addListener(JTable table,JFrame window){
			table_=table;
			window_=window;
		}
		public void mousePressed(MouseEvent e){
			String s = (String)JOptionPane.showInputDialog(
                    window_,
                    "Enter the login",
                    "Add account",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
			try{
				((AccountsTableModel)table_.getModel()).addAccount(s);
			}
			catch(LoginException LE){
				JOptionPane ErrorPane = new JOptionPane(null,
						JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
				ErrorPane.setMessage(LE.getMessage());
				JDialog errorDialog = ErrorPane.createDialog(window_,
				"Error");
				errorDialog.setVisible(true);
			}
			window_.repaint();
		}
	}
	
	private class removeListener extends MouseInputAdapter{
		private JTable table_;
		private JFrame window_;
		public removeListener(JTable table,JFrame window){
			table_=table;
			window_=window;
		}
		public void mousePressed(MouseEvent e){
			String s = (String)JOptionPane.showInputDialog(
                    window_,
                    "Enter the login:\n",
                    "Remove account",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
			try{
				((AccountsTableModel)table_.getModel()).removeAccount(s);
			}
			catch(LoginException LE){
				JOptionPane ErrorPane = new JOptionPane(null,
						JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
				ErrorPane.setMessage(LE.getMessage());
				JDialog errorDialog = ErrorPane.createDialog(window_,
				"Error");
				errorDialog.setVisible(true);
			}
			window_.repaint();
		}	
	}
	
	
	private class PopupListener extends MouseInputAdapter {
		JPopupMenu popup_;
		public PopupListener(JPopupMenu popup){
			popup_=popup;
		}
		
		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	            popup_.show(e.getComponent(),
	                       e.getX(), e.getY());
	        }
	    }
	}
	
	
	public AdminWindow(TableModel model){
		menuBar_.add(accountMenu_);
		accountMenu_.add(logoutMenuItem_);
		table_.setModel(model);
		table_.addMouseListener(new PopupListener(popup_));
		popup_.add(addAccount_);
		popup_.add(removeAccount_);
		addAccount_.addMouseListener(new addListener(table_,this));
		removeAccount_.addMouseListener(new removeListener(table_,this));
		this.setJMenuBar(menuBar_);
		this.add(new JScrollPane(table_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED
				,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		logoutMenuItem_.addMouseListener(new logoutListener(this));
		this.setSize(640, 480);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
