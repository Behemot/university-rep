#---------------------------------
def fast_pot(base,power,mod):
    res=1L;
    while (power>0):
        sign=power & 1L;
        if(sign==1):
            res=res*base%mod;
        base=base*base%mod;
        power=power>>1;
    return res;
#-------------------------------

