#encoding=UTF-8
import math;
import random;
import sys;
from PyQt4.QtCore import *;
from PyQt4.QtGui import *;
#---------------------------------
def fast_pot(base,power,mod):
    res=1L;
    while (power>0):
        sign=power & 1L;
        if(sign==1):
            res=res*base%mod;
        base=base*base%mod;
        power=power>>1;
    return res;
#-------------------------------
def miller_rabin_pass(a, s, d, n):
    a_to_power = fast_pot(a, d, n)
    if a_to_power == 1:
        return True;
    for i in xrange(s-1):
        if a_to_power == n - 1:
            return True;
        a_to_power = (a_to_power * a_to_power) % n
    return a_to_power == n - 1;
def miller_rabin(n):
    d=n-1;
    s=0;
    while d % 2 == 0:
       d >>= 1;
       s += 1;
    for repeat in xrange((int)(math.log(n,2))):
        a = 0;
        while a == 0:
            a = random.randrange(n);
        if not miller_rabin_pass(a, s, d, n):
            return False
    return True
#-------------------------------
def generate_prime(length=64):
    key=random.getrandbits(length);
    for i in xrange(2000000):
        if(miller_rabin(key+i)):return key+i;
        if(miller_rabin(key-i)):return key-i;
    else: return -1;
class Form(QDialog):
    def __init__(self,parent=None):
        super(Form, self).__init__(parent)
        self.generatorLabel = QLabel(u"Количество бит")        
        self.generatorEdit = QLineEdit()
        self.generateButton = QPushButton(u"Сгенерировать")
        self.generatorResult = QTextEdit(u"")
        layout = QVBoxLayout()
        layout.addWidget(self.generatorLabel)
        layout.addWidget(self.generatorEdit)
        layout.addWidget(self.generateButton)
        layout.addWidget(self.generatorResult)
        self.setLayout(layout)
        self.generatorEdit.setFocus()    
        self.connect(self.generateButton, SIGNAL("pressed()"), self.generatePrime)
        self.connect(self,SIGNAL("generated(QString)"),self.generatorResult,SLOT("setText(QString)"))
        self.setWindowTitle("Lab 6")
    def generatePrime(self):
        try:
            length = int(self.generatorEdit.text())
            res=generate_prime(length)
            self.emit(SIGNAL("generated(QString)"),QString(str(res)))
        except:
            self.emit(SIGNAL("generated(QString)"),QString("Invalid input"))
def main():
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    app.exec_()
#-------------------------------
main();
