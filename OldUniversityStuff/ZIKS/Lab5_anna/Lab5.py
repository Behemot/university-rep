#encoding=UTF-8
import sys;
from PyQt4.QtCore import *;
from PyQt4.QtGui import *;
#---------------------------------
def fast_pot(num,p,mod):
    result=1L;
    while (p>0):
        sign=p%2;
        if(sign==1):result=result*num%mod;
        num=num*num%mod;
        p=p>>1;
    return result;
#-------------------------------
class Form(QDialog):
    def __init__(self,parent=None):
        super(Form, self).__init__(parent)
        self.baseLabel = QLabel(u"Основание")
        self.baseEdit = QLineEdit()
        self.powerLabel = QLabel(u"Степень")
        self.powerEdit = QLineEdit()
        self.moduleLabel = QLabel(u"Модуль")
        self.moduleEdit = QLineEdit()
        self.calculateButton = QPushButton(u"Вычислить")
        self.resultLabel = QTextEdit(u"") 
        
        layout = QVBoxLayout()
        layout.addWidget(self.baseLabel)
        layout.addWidget(self.baseEdit)
        layout.addWidget(self.powerLabel)
        layout.addWidget(self.powerEdit)
        layout.addWidget(self.moduleLabel)
        layout.addWidget(self.moduleEdit)
        layout.addWidget(self.calculateButton)
        layout.addWidget(self.resultLabel)
        self.setLayout(layout)
        self.baseEdit.setFocus()    
        self.connect(self.calculateButton, SIGNAL("pressed()"), self.calculatePower)
        self.connect(self,SIGNAL("calculated(QString)"),self.resultLabel,SLOT("setText(QString)"))
        self.setWindowTitle("Lab 5")
    def calculatePower(self):
        try:
            base = long(self.baseEdit.text())
            power = long(self.powerEdit.text())
            module = long(self.moduleEdit.text())
            res = fast_pot(base,power,module)
            self.emit(SIGNAL("calculated(QString)"),QString(str(res)))
        except:
            self.emit(SIGNAL("calculated(QString)"),QString("Invalid input"))
def main():
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    app.exec_()

#-------------------------------
main();
