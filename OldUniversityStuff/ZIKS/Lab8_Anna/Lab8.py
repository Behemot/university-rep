#encoding=utf8

import math;
import random;
import sys;
import Util;
from io import *;
from PyQt4.QtCore import *;
from PyQt4.QtGui import *;

#-------------------------------------
def mod_inv(u,v):
    # Step X1. Initialise 
    u1 = 1
    u3 = u
    v1 = 0
    v3 = v
    # Remember odd/even iterations 
    it = 1
    # Step X2. Loop while v3 != 0 
    while (v3 != 0):
        # Step X3. Divide and "Subtract" 
        q = u3 / v3
        t3 = u3 % v3
        t1 = u1 + q * v1;
        # Swap 
        u1 = v1
        v1 = t1
        u3 = v3
        v3 = t3
        it = -it
    # Make sure u3 = gcd(u,v) == 1 
    if (u3 != 1):
        return 0   # Error: No inverse exists 
    # Ensure a positive result 
    if (it < 0):
        inv = v - u1
    else:
        inv = u1
    return inv

#-------------------------------------


def generate_subkeys(key,mode):
    subkeys=[]
    subkey_mask=0b1111111111111111
    temp_key=key
    for i in range(7):
        for j in range(8):
            temp=temp_key>>(j*16)
            subkeys.append(temp&subkey_mask)
        head=temp_key>>103
        temp_key=((temp_key^(head<<103))<<25)+head
    if(mode==1):
        subkeys_inv=[]
        subkeys_inv.append(mod_inv(subkeys[48],65537))
        subkeys_inv.append(-subkeys[49])
        subkeys_inv.append(-subkeys[50])
        subkeys_inv.append(mod_inv(subkeys[51],65537))
        for i in range(7):
            subkeys_inv.append(subkeys[47-i*6-1])
            subkeys_inv.append(subkeys[47-i*6])
            subkeys_inv.append(mod_inv(subkeys[47-i*6-5],65537))
            subkeys_inv.append(-subkeys[47-i*6-3])
            subkeys_inv.append(-subkeys[47-i*6-4])
            subkeys_inv.append(mod_inv(subkeys[47-i*6-2],65537))
        subkeys_inv.append(subkeys[47-7*6-1])
        subkeys_inv.append(subkeys[47-7*6])
        subkeys_inv.append(mod_inv(subkeys[47-7*6-5],65537))
        subkeys_inv.append(-subkeys[47-7*6-4])
        subkeys_inv.append(-subkeys[47-7*6-3])
        subkeys_inv.append(mod_inv(subkeys[47-7*6-2],65537))
        
        return subkeys_inv
    else:
        return subkeys;
#-------------------------------------
def idea_mul(x,y):
   MAX=65536
   MOD=65537
   x %= MAX
   y %= MAX
   if(x==0):
       x=MAX 
   if(y==0):
       y=MAX
   z =(x*y)%MOD
   if(z==MAX):
       z=0
   return z 
   
        
def encrypt_block(block,subkeys):
    part_mask=0b1111111111111111
    parts=[]
    for i in range(4):
        parts.append((block>>(i*16))&part_mask)
    for i in parts:
        print(i)
    print("------------------")
    for i in range(8):
        A=idea_mul(parts[0],subkeys[i*6])
        B=(parts[1]+subkeys[i*6+1])%65536
        C=(parts[2]+subkeys[i*6+2])%65536
        D=idea_mul(parts[3],subkeys[i*6+3])
        #----------
        S3=C
        C=C^A
        C=idea_mul(C,subkeys[i*6+4])
        S2=B
        B=B^D
        B=(B+C)%65536
        B=idea_mul(B,subkeys[i*6+5])
        C=(B+C)%65536
        parts[0]=A^B
        parts[3]=D^C
        parts[1]=B^S3
        parts[2]=C^S2
    parts[0]=idea_mul(parts[0],subkeys[48])
    parts[1]=(parts[1]+subkeys[49])%65536
    parts[2]=(parts[2]+subkeys[50])%65536
    parts[3]=idea_mul(parts[3],subkeys[51])
    print("------------------")
    for j in parts:
        print(j)
    print("------------------")
    
    return parts[0]+(parts[1]<<16)+(parts[2]<<32)+(parts[3]<<48);        

#-------------------------------------

   

            
def encrypt_file(file_name, key_file='key.pem'):
    f=open(key_file,'r')
    key=int(f.readline())
    subkeys=generate_subkeys(key,0);
    block_size=64;
    f.close()
    source_file=open(file_name,'rb')
    target_file=open(file_name+'.encrypted','wb')
    while True:
        src_arr=bytearray(source_file.read(block_size))
        if (len(src_arr)==0):
            break
        src=0;
        for i in src_arr:
            src=src<<8
            src+=int(i);
        code=encrypt_block(src,subkeys)
        target_file.write(str(code)+'\n')
    source_file.close()
    target_file.close()
def decrypt_file(file_name, key_file='key.pem'):
    f=open(key_file,'r')
    key=int(f.readline())
    subkeys=generate_subkeys(key,1);
    block_size=64;
    f.close()
    source_file=open(file_name,'rb')
    target_file=open(file_name+'.decrypted','w')
    while True:
        tmp=source_file.readline()
        if (tmp==''):
            break
        src=long(tmp)
        message=encrypt_block(src,subkeys)
        res_arr=[]
        while(message>0):
            buf=message&0b11111111
            res_arr.insert(0,buf)
            message=message>>8
        byte_arr=bytearray(res_arr)
        target_file.write(byte_arr.decode('utf8'))
    source_file.close()
    target_file.close()
#---------------------------------------
def dump_key():
    key=Util.generate_prime(128);
    public_file=FileIO('key.pem','w')            
    public_file.write(str(key))
    public_file.close()
    
#---------------------------------------    
class Form(QDialog):
    def __init__(self,parent=None):
        super(Form, self).__init__(parent)
        self.fileLabel = QLabel(u"File")
        self.fileEdit = QLineEdit()
        self.chooseButton = QPushButton(u"Открыть");
        self.generateKeyButton = QPushButton(u"Сгенерировать ключ");
        self.encryptButton = QPushButton(u"Зашифровать")
        self.decryptButton = QPushButton(u"Расшифровать")
        
        layout = QGridLayout()

        layout.addWidget(self.fileEdit,0,0,1,3)
        layout.addWidget(self.chooseButton,1,0)
        layout.addWidget(self.encryptButton,1,1)
        layout.addWidget(self.decryptButton,2,1)
        layout.addWidget(self.generateKeyButton,2,0)
        
        self.setLayout(layout)
        self.fileEdit.setFocus()    
        self.connect(self.chooseButton, SIGNAL("pressed()"), self.chooseFile)
        self.connect(self.encryptButton, SIGNAL("pressed()"), self.encrypt)
        self.connect(self.decryptButton, SIGNAL("pressed()"), self.decrypt)
        self.connect(self.generateKeyButton, SIGNAL("pressed()"), self.generate_key)
        
        self.connect(self,SIGNAL("fileChosen(QString)"),self.fileEdit,SLOT("setText(QString)"))
        self.setWindowTitle("Lab 7")
    def chooseFile(self):
        try:
            filename=QFileDialog.getOpenFileName(self, "Open", ".")
            self.emit(SIGNAL("fileChosen(QString)"),QString(filename))
        except:
            self.emit(SIGNAL("fileChosen(QString)"),QString("Invalid input"))
    def encrypt(self):
        Util.encrypt_file(str(self.fileEdit.text()));
    def decrypt(self):
        Util.decrypt_file(str(self.fileEdit.text()));
    def generate_key(self):
        dump_key();
def main():
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    app.exec_()
    
#---------------------------------------    
main()
#test()
