#encoding=utf8
import math;
import random;
import sys;
from io import *;
const=21842741944806558395794169952391265069L
#---------------------------------
def fast_pot(base,power,mod):
    res=1L;
    while (power>0):
        sign=power & 1L
        if(sign==1):
            res=res*base%mod
        base=base*base%mod
        power=power>>1
    return res;
#-------------------------------
def miller_rabin_pass(a, s, d, n):
    a_to_power = fast_pot(a, d, n)
    if a_to_power == 1:
        return True;
    for i in xrange(s-1):
        if a_to_power == n - 1:
            return True;
        a_to_power = (a_to_power * a_to_power) % n
    return a_to_power == n - 1;

def miller_rabin(n):
    d=n-1;
    s=0;
    while d % 2 == 0:
       d >>= 1
       s += 1
    for repeat in xrange((int)(math.log(n,2))):
        a = 0;
        while a == 0:
            a = random.randrange(n);
        if not miller_rabin_pass(a, s, d, n):
            return False
    return True
#-------------------------------
def generate_prime(length=64):
    mask=1<<length
    key=random.getrandbits(length)|mask;
    for i in xrange(2000000):
        if(miller_rabin(key+i)):return key+i
        if(miller_rabin(key-i)):return key-i
    else: return -1
#---------------------------------------
def get_gcd(a,b):
    a_temp=a;
    b_temp=b;
    while(b_temp!=0):
        t = b_temp
        b_temp = a_temp%b_temp
        a_temp = t
    return a_temp
#-------------------------------------
def modinv(u,v):
    # Step X1. Initialise 
    u1 = 1
    u3 = u
    v1 = 0
    v3 = v
    # Remember odd/even iterations 
    it = 1
    # Step X2. Loop while v3 != 0 
    while (v3 != 0):
        # Step X3. Divide and "Subtract" 
        q = u3 / v3
        t3 = u3 % v3
        t1 = u1 + q * v1;
        # Swap 
        u1 = v1
        v1 = t1
        u3 = v3
        v3 = t3
        it = -it
    # Make sure u3 = gcd(u,v) == 1 
    if (u3 != 1):
        return 0   # Error: No inverse exists 
    # Ensure a positive result 
    if (it < 0):
        inv = v - u1
    else:
        inv = u1
    return inv

#-------------------------------------

def generate_key(key_length):
    e=65537
    p=const
    q=key_length
    n=p*q
    phi=(p-1)*(q-1)
    d=modinv(e,phi)
    return [n,d,e]
    
#---------------------------------------
def encrypt_block(message,e,n):
    return fast_pot(message,e,n)
def decrypt_block(code,d,n):
    return fast_pot(code,d,n)
    
def encrypt_file(file_name, key_file='key.pem'):
    f=open(key_file,'r')
    second=long(f.readline())
    key=generate_key(second)
    n=key[0]
    e=key[2]
    block_size=int((int(math.log(n,2))-1)/8);
    f.close()
    source_file=open(file_name,'rb')
    target_file=open(file_name+'.encrypted','wb')
    while True:
        src_arr=bytearray(source_file.read(block_size))
        if (len(src_arr)==0):
            break
        src=0;
        for i in src_arr:
            src=src<<8
            src+=int(i);
        code=encrypt_block(src,e,n)
        target_file.write(str(code)+'\n')
    source_file.close()
    target_file.close()
def decrypt_file(file_name, key_file='key.pem'):
    f=open(key_file,'r')
    second=long(f.readline())
    key=generate_key(second)
    n=key[0]
    d=key[1]
    block_size=int((int(math.log(n,2))-1)/8);
    f.close()
    source_file=open(file_name,'rb')
    target_file=open(file_name+'.decrypted','w')
    while True:
        tmp=source_file.readline()
        if (tmp==''):
            break
        src=long(tmp)
        message=decrypt_block(src,d,n)
        res_arr=[]
        while(message>0):
            buf=message&0b11111111
            res_arr.insert(0,buf)
            message=message>>8
        byte_arr=bytearray(res_arr)
        try:
            target_file.write(byte_arr.decode('utf8'))
        except:
            target_file.write(u"Decryption Error")
    source_file.close()
    target_file.close()
#---------------------------------------
def dump_key():
    key=generate_key(512);
    public_key=[key[0],key[2]]
    private_key=[key[0],key[1]]
    public_file=FileIO('public_key.pem','w')            
    public_file.write(str(key[0])+'\n')
    public_file.write(str(key[2])+'\n')
    public_file.close()
    private_file=FileIO('private_key.pem','w')            
    private_file.write(str(key[0])+'\n')
    private_file.write(str(key[1])+'\n')
    private_file.close()
#---------------------------------------    

