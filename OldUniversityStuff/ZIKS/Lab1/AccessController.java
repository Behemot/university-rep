package security;

public class AccessController {
	private String accessRights_;
	private static AccessController accessController_;
	
	public static AccessController getAccessController(){
		if(accessController_==null){
			accessController_=new AccessController();
		}
		return accessController_;
	}
	
	public void setRights(String accessRights){
		accessRights_=accessRights;
	}
}
