package gui;


import java.awt.Dimension;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.event.MouseInputAdapter;

import security.LoginAuthenticatorChecker;

public class LoginAuthenticationWindow extends JFrame{
	private static final long serialVersionUID = -891818977227644782L;
	private static LoginAuthenticationWindow loginAuthenticationWindow_;
	private JLabel questionLabel_=new JLabel("");
	private JTextField answerField_=new JTextField();
	private JButton okButton_=new JButton("Authenticate");
	private LoginAuthenticatorChecker authChecker_=new LoginAuthenticatorChecker();
	public static LoginAuthenticationWindow getLoginAuthenticationWindow(){
		if(loginAuthenticationWindow_==null){
			loginAuthenticationWindow_=new LoginAuthenticationWindow();
		}
		return loginAuthenticationWindow_;
	}
	
	private LoginAuthenticationWindow(){
		this.setSize(320, 240);
		this.setTitle("Login Authentication");
		GroupLayout layout=new GroupLayout(this.getContentPane());
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		answerField_.setMaximumSize(new Dimension(this.getWidth()/2,this.getHeight()/10));
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(questionLabel_)
				.addComponent(answerField_)
				.addComponent(okButton_)
		);
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(questionLabel_)
				.addComponent(answerField_)
				.addComponent(okButton_)
		);
		this.getContentPane().setLayout(layout);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		okButton_.addMouseListener(new MouseInputAdapter(){
			public void mousePressed(MouseEvent e){
				LoginAuthenticationWindow.getLoginAuthenticationWindow().checkAuth();
			}
		});
	}
	
	public void generateRandom(){
		questionLabel_.setText("X = "+authChecker_.generateRandom());
	}
	
	public void checkAuth(){
		if(authChecker_.checkAuth(Integer.parseInt(answerField_.getText()))){
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.INFORMATION_MESSAGE, JOptionPane.PLAIN_MESSAGE);
			ErrorPane.setMessage("Login successful");
			JDialog errorDialog = ErrorPane.createDialog(this,
					"Error");
			errorDialog.setVisible(true);
			
		}
		else
		{
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
			ErrorPane.setMessage("Authentication Failure");
			JDialog errorDialog = ErrorPane.createDialog(this,
					"Error");
			errorDialog.setVisible(true);
			this.setVisible(false);
			LoginWindow.getLoginWindow().setVisible(true);
		}
	}
	
}
