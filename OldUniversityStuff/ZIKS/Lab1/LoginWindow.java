package gui;


import java.awt.Dimension;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.event.MouseInputAdapter;

import security.AccessController;
import security.LoginException;
import security.PasswordManager;

public class LoginWindow extends JFrame{
	private static final long serialVersionUID = -891818977227644782L;
	private static LoginWindow loginWindow_;
	private JLabel loginLabel_=new JLabel("Login: ");
	private JLabel passwordLabel_=new JLabel("Password: ");
	private JTextField loginField_=new JTextField();
	private JPasswordField passwordField_=new JPasswordField();
	private JButton loginButton_=new JButton("Login");
	private JButton registerButton_=new JButton("Register");
	private PasswordManager passwordManager_=new PasswordManager();
	
	public static LoginWindow getLoginWindow(){
		if(loginWindow_==null){
			loginWindow_=new LoginWindow();
		}
		return loginWindow_;
	}
	
	private LoginWindow(){
		this.setSize(320, 240);
		this.setTitle("Login");
		
		GroupLayout layout=new GroupLayout(this.getContentPane());
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		loginField_.setMaximumSize(new Dimension(this.getWidth()/2,this.getHeight()/10));
		passwordField_.setMaximumSize(new Dimension(this.getWidth()/2,this.getHeight()/10));
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(loginLabel_)
				.addComponent(new JPanel().add(loginField_))
				.addComponent(passwordLabel_)
				.addComponent(passwordField_)
				.addComponent(loginButton_)
				.addComponent(registerButton_)
		);
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(loginLabel_)
				.addComponent(loginField_)
				.addComponent(passwordLabel_)
				.addComponent(passwordField_)
				.addComponent(loginButton_)
				.addComponent(registerButton_)
		);
		this.getContentPane().setLayout(layout);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		loginButton_.addMouseListener(new MouseInputAdapter(){
			public void mousePressed(MouseEvent e){
				LoginWindow.getLoginWindow().checkLogin();
			}
		});
		
		registerButton_.addMouseListener(new MouseInputAdapter(){
			public void mousePressed(MouseEvent e){
				LoginWindow.getLoginWindow().registerUser();
			}
		});
		
		this.setVisible(true);
	}
	
	public void checkLogin(){
		try{
			String temp=passwordManager_.checkPassword(loginField_.getText(), passwordField_.getText());
			AccessController.getAccessController().setRights(temp);
			LoginAuthenticationWindow.getLoginAuthenticationWindow().generateRandom();
			LoginAuthenticationWindow.getLoginAuthenticationWindow().setVisible(true);
			this.setVisible(false);
		}
		catch(LoginException LE){
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
			ErrorPane.setMessage(LE.getMessage());
			JDialog errorDialog = ErrorPane.createDialog(this,
					"Error");
			errorDialog.setVisible(true);
		}
	}
	
	public void registerUser(){
		try{
			passwordManager_.registerAccount(loginField_.getText(), passwordField_.getText());
		}
		catch(LoginException LE){
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
			ErrorPane.setMessage(LE.getMessage());
			JDialog errorDialog = ErrorPane.createDialog(this,
					"Error");
			errorDialog.setVisible(true);
		}
	}

}
