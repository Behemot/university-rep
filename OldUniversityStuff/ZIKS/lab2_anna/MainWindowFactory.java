package gui;

import security.AccessController;
import javax.swing.JFrame;
import java.io.File;

public class MainWindowFactory {
	public static JFrame getMainWindow(){
		if(AccessController.getAccessController().getRights()[0].compareTo("Admin")==0){
			return new AdminWindow(new AccountsTableModel());
		}
		else{
			return new UserWindow(new FileTreeModel(new File("."),AccessController.getAccessController().getRights()));
		}
	}
}
