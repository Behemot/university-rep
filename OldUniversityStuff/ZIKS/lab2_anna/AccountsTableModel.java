package gui;
import javax.swing.table.AbstractTableModel;

import security.LoginException;
import security.PasswordManager;

public class AccountsTableModel extends AbstractTableModel{
	private PasswordManager passwordManager_=new PasswordManager();
	@Override
	public Class<?> getColumnClass(int arg0) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int arg0) {
		switch (arg0){
			case 0: return "Login";
			case 1: return "Password";
			case 2: return "Access Rights";
			case 3: return "Account Status";
		}
		return null;
	}

	@Override
	public int getRowCount() {
		return passwordManager_.getAccountCount();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		switch (arg1){
			case 0: return passwordManager_.getLogin(arg0);
			case 1: return passwordManager_.getPassword(arg0);
			case 2: return passwordManager_.getAccessRights(arg0);
			case 3: return passwordManager_.getBlocked(arg0).toString();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}

	@Override
	public void setValueAt(Object O, int arg0, int arg1) {
		switch (arg1){
		case 0: passwordManager_.setLogin((String)O,arg0);
		case 1: passwordManager_.setPassword((String)O,arg0);
		case 2: passwordManager_.setAccessRights((String)O,arg0);
		case 3: passwordManager_.setBlocked((String)O,arg0);
		}
	}

	public void addAccount(String login) throws LoginException{
		passwordManager_.registerAccount(login, "000000");
	}
	
	public void removeAccount(String login)throws LoginException{
		passwordManager_.removeAccount(login);
	}
	
}
