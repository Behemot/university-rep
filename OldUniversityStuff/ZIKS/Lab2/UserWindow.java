package gui;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

public class UserWindow extends JFrame{
	private JMenuBar menuBar_=new JMenuBar();
	private JMenu accountMenu_=new JMenu("Account");
	private JMenuItem logoutMenuItem_=new JMenuItem("Log out");
	private JTree fileTree_=new JTree();
	
	private class logoutListener extends MouseInputAdapter{
		JFrame window_;
		public logoutListener(JFrame window){
			window_=window;
		}
		public void mousePressed(MouseEvent e){
			LoginWindow.getLoginWindow().setVisible(true);
			window_.dispose();
		}
	}
	
	private class treeListener extends MouseInputAdapter{
		JTree tree_;
		public treeListener(JTree tree){
			tree_=tree;
		}
		public void mousePressed(MouseEvent e){
			try {
				if(e.getClickCount()==2){
					TreePath tp=tree_.getPathForLocation(e.getX(), e.getY());
					if(tree_.getModel().isLeaf(tree_.getLastSelectedPathComponent())){
						Process dot=Runtime.getRuntime().exec("gedit "+tp.getPath()[tp.getPathCount()-1]);
					}
				}		
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	
	}
	
	public UserWindow(TreeModel model){
		menuBar_.add(accountMenu_);
		accountMenu_.add(logoutMenuItem_);
		this.setJMenuBar(menuBar_);
		fileTree_.setModel(model);
		fileTree_.addMouseListener(new treeListener(fileTree_));
		this.add(new JScrollPane(fileTree_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED
				,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		logoutMenuItem_.addMouseListener(new logoutListener(this));
		this.setSize(640, 480);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
