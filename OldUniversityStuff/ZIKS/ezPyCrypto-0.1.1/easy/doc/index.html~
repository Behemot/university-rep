<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>ezPyCrypto - Encryption in Python made EASY</title>
  </head>
  <body>
    <h1>ezPyCrypto - Encryption in Python made EASY</h1>
	ezPyCrypto is the culmination of my search for an approachable
	yet powerful crypto library for Python.
	<b><blockquote><i>This may be the only Python crypto API you ever
		need to learn!</i></blockquote></b>
	I've been looking at a lot of cryptography libraries in my time,
	and have been frustrated, because everything I saw suffered from
	problems such as:
	<ul>
	  <li>Not multi-platform</li>
	  <li>Painfully difficult to understand and use - needing dozens
		or hundreds of lines of code just to do a simple operation</li>
	  <li>Difficult or impossible to compile and install, without intimate
		knowledge of the cource code (plus deep knowledge of cryptography).
	  <li>Painful dependency on 3rd party libraries, which often proved
		difficult or impossible to find, compile and/or install</li>
	  <li>Lack of adequate documentation and/or usage examples</li>
	</ul>
	I started to suspect that many crypto toolkit authors were coming from
	a mindset of <i>If it was hard to write, it should be hard to use!</i>. If I
	was deterred from using these toolkits, it occurred to me that others
	would be feeling deterred as well. I imagined with horror all the
	hours that are getting wasted by programmers the world over, in
	struggling to come to terms with underlying crypto theory, and mastering
	the arcane intricacies of the available crypto libraries (or, as
	programmers are notorious for, writing their own libraries, often just
	as arcane and difficult for others.) Meanwhile, zillions of good creative
	ideas for applications are simply not getting implemented, because
	programmers feel discouraged and find something else to do.<br>
	<br>
	As many will know, I've recently moved to
	<a href="http://www.python.orc">Python</a> as my programming language of
	choice. Quite simply, Python seriously kicks butt. It spares the programmer
	of all the red tape of keeping track of data types, and allows a much looser
	and freer creative process. If you're not using Python already, then just
	download it and install it and start programming. Most of the difficulty
	of learning Python is because of its ease of use and empowering freedom,
	since you have to let go of the mind-crippling limitations of other 
	languages you've learned. Luckily, this difficulty passes in a few days
	of dabbling - once you've followed the
	<a href="http://www.python.org/doc/current/tut/tut.html">Python Tutorial</a>,
	you'll be up and running - no problems.<br>
	<br>
	But back to the point. I looked at the various Python crypto libraries
	(there's bugger-all crypto in the standard Python libraries), and found
	most of them failing from one or more of the above limitations.<br>
	<br>
	The only exception was a fine piece of programming called
	<a href="http://www.amk.ca/python/code/crypto.html">PyCrypto</a>.
	PyCrypto has a very clean design, with well-organised Python classes at
	the front-end, and very portable C code at the back end. Unlike other
	crypto libs, PyCrypto compiles easily on *nix and Windows platforms,
	doesn't need any 3rd party libraries, and comes with excellent LaTeX
	documentation (which looks good when passed through latex2html).<br>
	<br>
	Also, PyCrypto supports most of the popular encryption algorithms,
	and supports any key length the programmer desires. Larger keys, more
	secure, but slower - programmer chooses the right trade-off. The people
	who wrote PyCrypto certainly <i>don't</i> have that elitist
	geek syndrome - the way they've designed and packaged their code shows
	a respect for the client programmer that is sadly rare.<br>
	<br>
	However, the PyCrypto API is still a little more low-level than I'd
	like. It still imposes a bit too much 'red tape' on the programmer,
	albeit much less than any other crypto library I've seen.<br>
	<br>
	Therefore, it occurred to me to write a wrapper layer over the top
	of PyCrypto, and build this layer to give maximum features and security
	together with absolute ease of use.<br>
	<br>
	<b>ezPyCrypto</b> basically defines a single class, called <b>key</b>,
	which does most cryptographic operations a programmer will need, including:
	<ul>
	  <li>Encrypting and Decrypting strings</li>
	  <li>Signing strings and Verifying signatures</li>
	  <li>Exporting and importing public and private keys</li>
	  <li>Generating new public/private keypairs</li>
	</ul>
	So, why not have a look now at the
	<a href="detail/index.html" target="_blank"><b>ezPyCrypto Documentation</b></a>
	<br>
	<br>
	And, if you like what you see, you can
	<a href="ezPyCrypto-0.1.tar.gz"><b>Download ezPyCrypto</b></a>. ezPyCrypto comes
	with a suite of mindlessly simple example programs, so you'll be up and
	running in minutes!<br>
	<br>
	Note that this download contains the PyCrypto backend, both in source code form,
	(and an executable Windows installer (python 2.2 only), in case you
	don't have a suitable C compiler. Installing PyCrypto on Debian is also easy:
	just type <b><font face='Courier'>apt-get&nbsp;install&nbsp;python-crypto</font></b><br>
    <hr>
<!-- Created: Fri Feb 28 01:10:00 NZDT 2003 -->
<!-- hhmts start -->
Last modified: Sat Mar  1 00:19:02 NZDT 2003
<!-- hhmts end -->
  </body>
</html>
