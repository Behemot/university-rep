import math;
import random;
import sys;
from PyQt4.QtCore import *;
from PyQt4.QtGui import *;


#---------------------------------
def fast_pot(base,power,mod):
    res=1L;
    while (power>0):
        sign=power & 1L;
        if(sign==1):
            res=res*base%mod;
        base=base*base%mod;
        power=power>>1;
    return res;
#-------------------------------
def miller_rabin_pass(a, s, d, n):
    a_to_power = fast_pot(a, d, n)
    if a_to_power == 1:
        return True;
    for i in xrange(s-1):
        if a_to_power == n - 1:
            return True;
        a_to_power = (a_to_power * a_to_power) % n
    return a_to_power == n - 1;

def miller_rabin(n):
    d=n-1;
    s=0;
    while d % 2 == 0:
       d >>= 1;
       s += 1;
    for repeat in xrange((int)(math.log(n,2))):
        a = 0;
        while a == 0:
            a = random.randrange(n);
        if not miller_rabin_pass(a, s, d, n):
            return False
    return True
#-------------------------------

def generate_prime(length=64):
    key=random.getrandbits(length);
    for i in xrange(2000000):
        if(miller_rabin(key+i)):return key+i;
        if(miller_rabin(key-i)):return key-i;
    else: return -1;
    
    
class Form(QDialog):
    def __init__(self,parent=None):
        super(Form, self).__init__(parent)
        self.baseLabel = QLabel("Base")
        self.baseEdit = QLineEdit()
        self.powerLabel = QLabel("Power")
        self.powerEdit = QLineEdit()
        self.moduleLabel = QLabel("Module")
        self.moduleEdit = QLineEdit()
        self.calculateButton = QPushButton("Calculate")
        self.resultLabel = QTextEdit("Result: ") 
        self.generatorLabel = QLabel("Number of bits")        
        self.generatorEdit = QLineEdit()
        self.generateButton = QPushButton("Generate prime number")
        self.generatorResult = QTextEdit("Generated number:")
        layout = QGridLayout()
        layout.addWidget(self.baseLabel,0,0)
        layout.addWidget(self.baseEdit,0,1)
        layout.addWidget(self.powerLabel,1,0)
        layout.addWidget(self.powerEdit,1,1)
        layout.addWidget(self.moduleLabel,2,0)
        layout.addWidget(self.moduleEdit,2,1)
        layout.addWidget(self.calculateButton,3,0,1,2)
        layout.addWidget(self.resultLabel,4,0,1,2)
        layout.addWidget(self.generatorLabel,5,0)
        layout.addWidget(self.generatorEdit,5,1)
        layout.addWidget(self.generateButton,6,0,1,2)
        layout.addWidget(self.generatorResult,7,0,1,2)
        
        self.setLayout(layout)
        self.baseEdit.setFocus()    
        self.connect(self.calculateButton, SIGNAL("pressed()"), self.calculatePower)
        self.connect(self,SIGNAL("calculated(QString)"),self.resultLabel,SLOT("setText(QString)"))
        self.connect(self.generateButton, SIGNAL("pressed()"), self.generatePrime)
        self.connect(self,SIGNAL("generated(QString)"),self.generatorResult,SLOT("setText(QString)"))
        self.setWindowTitle("Lab 5,6")
    def calculatePower(self):
        try:
            base = long(self.baseEdit.text())
            power = long(self.powerEdit.text())
            module = long(self.moduleEdit.text())
            res = fast_pot(base,power,module)
            self.emit(SIGNAL("calculated(QString)"),QString(str(res)))
        except:
            self.emit(SIGNAL("calculated(QString)"),QString("Invalid input"))
    def generatePrime(self):
        try:
            length = int(self.generatorEdit.text())
            res=generate_prime(length)
            self.emit(SIGNAL("generated(QString)"),QString(str(res)))
        except:
            self.emit(SIGNAL("generated(QString)"),QString("Invalid input"))
def main():
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    app.exec_()

#-------------------------------
main();
