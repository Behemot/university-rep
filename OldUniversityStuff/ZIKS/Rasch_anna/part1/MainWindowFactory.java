package gui;

import security.AccessController;
import javax.swing.JFrame;
import java.io.File;
import java.util.ArrayList;

public class MainWindowFactory {
	private static ArrayList<JFrame> mainFrames_=new ArrayList<JFrame>();
	public static JFrame getMainWindow(){
		if(AccessController.getAccessController().getRights()[0].compareTo("Admin")==0){
			JFrame temp=new AdminWindow(new AccountsTableModel());
			mainFrames_.add(temp);
			return temp;
		}
		else{
			JFrame temp=new UserWindow(new FileTreeModel(new File(".")
				,AccessController.getAccessController().getRights()));
			mainFrames_.add(temp);
			return temp;
		}
	}
	public static ArrayList<JFrame> getFrames(){
		return mainFrames_;
	}
	public static void clearFrames(){
		mainFrames_=new ArrayList<JFrame>(); 
	}
}
