package gui;
import java.io.File;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.HashMap;

public class FileTreeModel extends DefaultTreeModel{
	private static final long serialVersionUID = -7186896494847559010L;
	// We specify the root directory when we create the model.
	  protected File root_;
	  protected HashMap<String,Boolean> accessRights_=new HashMap<String,Boolean>();
	  public FileTreeModel(File root,String[] accessRights) { 
		  super(new DefaultMutableTreeNode(root));
		  root_ = root; 
		  for(int i=0;i<accessRights.length;i++){
			  accessRights_.put(accessRights[i].intern(), true);
		  }
	  }

	  // The model knows how to return the root object of the tree
	  public Object getRoot() { return root_; }

	  // Tell JTree whether an object in the tree is a leaf or not
	  public boolean isLeaf(Object node) {  return ((File)node).isFile(); }

	  // Tell JTree how many children a node has
	  public int getChildCount(Object parent) {
	    String[] children = ((File)parent).list();
	    if (parent==root_){
	    	int count=0;
		    for(int i=0;i<children.length;i++){
	    		if(accessRights_.containsKey(children[i])){
	    			count++;
	    		}
	    	}
	    	return count;
	    }
	    if (children == null) return 0;
	    return children.length;
	  }

	  // Fetch any numbered child of a node for the JTree.
	  // Our model returns File objects for all nodes in the tree.  The
	  // JTree displays these by calling the File.toString() method.
	  public Object getChild(Object parent, int index) {
	    String[] children = ((File)parent).list();
	    if ((children == null) || (index >= children.length)) return null;
	    if(parent==root_){
	    	int pos=0;
	    	int j=0;
	    	for(int i=0;i<children.length;i++){
	    		if(accessRights_.containsKey(children[i])){
	    			if(j==index){
	    				pos=i;
	    				break;
	    			}
	    			else{
	    				j++;
	    			}
	    		}
	    	}
	    	return new File((File) parent, children[pos]);
	    }
	    else{
	    	return new File((File) parent, children[index]);
	    }
	  }

	  // Figure out a child's position in its parent node.
	  public int getIndexOfChild(Object parent, Object child) {
	    String[] children = ((File)parent).list();
	    if (children == null) return -1;
	    String childname = ((File)child).getName();
	    for(int i = 0; i < children.length; i++) {
	      if (childname.equals(children[i])) return i;
	    }
	    return -1;
	  }
}
