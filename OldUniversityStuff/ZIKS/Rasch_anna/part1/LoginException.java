package security;

public class LoginException extends Exception{
	private String message_;
	public LoginException(String message){
		message_=message;
	}
	public String getMessage(){
		return message_;
	}
}
