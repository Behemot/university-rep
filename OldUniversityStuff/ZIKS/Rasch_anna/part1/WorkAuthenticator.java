package security;

import java.util.ArrayList;

import gui.LoginAuthenticationWindow;
import gui.LoginWindow;
import gui.MainWindowFactory;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class WorkAuthenticator extends Thread{
	public void run(){
		while(true){
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			LoginWindow.getLoginWindow().setVisible(false);
			LoginAuthenticationWindow.getLoginAuthenticationWindow().setVisible(false);
			ArrayList<JFrame> frames=MainWindowFactory.getFrames();
			for(int i=0;i<frames.size();i++){
				frames.get(i).setVisible(false);
			}
			LoginAuthenticatorChecker auth=new LoginAuthenticatorChecker();
			int x=auth.generateRandom();
			String s = (String)JOptionPane.showInputDialog(
                    new JFrame(),
                    "X = "+x,
                    "Authentication",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
			if(s!=null&auth.checkAuth(Integer.valueOf(s))){
				for(int i=0;i<frames.size();i++){
					frames.get(i).setVisible(true);
				}
			}
			else{
				for(int i=0;i<frames.size();i++){
					frames.get(i).dispose();
				}
				LoginWindow.getLoginWindow().setVisible(true);
				PasswordManager.getPasswordManager().addInvalidTry(AccessController.
						getAccessController().getCurrentAccount());
			}
			
		}
	}
}
