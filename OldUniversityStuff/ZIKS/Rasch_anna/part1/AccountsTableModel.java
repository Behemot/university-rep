package gui;
import javax.swing.table.AbstractTableModel;

import security.LoginException;
import security.PasswordManager;

public class AccountsTableModel extends AbstractTableModel{
	@Override
	public Class<?> getColumnClass(int arg0) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int arg0) {
		switch (arg0){
			case 0: return "Login";
			case 1: return "Password";
			case 2: return "Access Rights";
			case 3: return "Account Status";
		}
		return null;
	}

	@Override
	public int getRowCount() {
		return PasswordManager.getPasswordManager().getAccountCount();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		switch (arg1){
			case 0: return PasswordManager.getPasswordManager().getLogin(arg0);
			case 1: return PasswordManager.getPasswordManager().getPassword(arg0);
			case 2: return PasswordManager.getPasswordManager().getAccessRights(arg0);
			case 3: return PasswordManager.getPasswordManager().getBlocked(arg0).toString();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}

	@Override
	public void setValueAt(Object O, int arg0, int arg1) {
		switch (arg1){
		case 0: PasswordManager.getPasswordManager().setLogin((String)O,arg0);break;
		case 1: PasswordManager.getPasswordManager().setPassword((String)O,arg0);break;
		case 2: PasswordManager.getPasswordManager().setAccessRights((String)O,arg0);break;
		case 3: PasswordManager.getPasswordManager().setBlocked((String)O,arg0);break;
		}
	}

	public void addAccount(String login) throws LoginException{
		PasswordManager.getPasswordManager().registerAccount(login, "000000");
	}
	
	public void removeAccount(String login)throws LoginException{
		PasswordManager.getPasswordManager().removeAccount(login);
	}
	
}
