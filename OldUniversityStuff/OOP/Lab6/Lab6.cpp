#include <QApplication>
#include <QHBoxLayout>
#include <QTextEdit>
#include "Lab6.h"
#include <QTimer>

using std::string;
int main(int argc, char *argv[])
{
    string s="Hello World!";
    QApplication app(argc,argv);
    QWidget *window = new QWidget;
    QTextEdit *label=new QTextEdit;
    action *act=new action(label,s);
    QTimer *timer= new QTimer(label);
    QObject::connect(timer,SIGNAL(timeout()),act,SLOT(count()));
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(label);
    window->setWindowTitle("Lab6");
    window->setFixedSize(640,480);
    window->setLayout(layout);
    window->show();
    timer->start(500);
    return app.exec();
}

