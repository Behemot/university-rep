#include <QApplication>
#include <QPushButton>
#include <sstream>
class action:public QObject
{
    Q_OBJECT
    int push_count;
    QPushButton* button;
    public:
    action(QPushButton* q)
    {
        button=q;
        push_count=0;
    }
    public slots:
    void count()
    {
        push_count++;
        std::stringstream ss;
        ss<<push_count;
        QString qs(ss.str().c_str());
        button->setText("Push "+qs);
    };
    void display(int i)
    {
        std::stringstream ss;
        ss<<i;
        QString qs(ss.str().c_str());
        button->setText(qs);
    }
};

