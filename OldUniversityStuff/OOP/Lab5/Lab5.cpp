#include <QVBoxLayout>
#include <QSpinBox>
#include <QProgressBar>
#include <QSlider>
#include <QLabel>
#include "Lab5.h"

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    QWidget *window = new QWidget;
    window->setWindowTitle("Lab5");
    window->setFixedSize(640,480);
    QSpinBox *spinBox = new QSpinBox;
    QProgressBar *progressBar = new QProgressBar; 
    QPushButton *button=new QPushButton("Push 0");
    QPushButton *button2=new QPushButton();
    
    QSlider *slider=new QSlider();
    action *act=new action(button);
    action *act2=new action(button2);
    spinBox->setRange(0,100);
    progressBar->setRange(0,100);
    QObject::connect(slider,SIGNAL(valueChanged(int)),spinBox,SLOT(setValue(int)));
    QObject::connect(spinBox,SIGNAL(valueChanged(int)),slider,SLOT(setValue(int)));
    QObject::connect(spinBox,SIGNAL(valueChanged(int)),progressBar,SLOT(setValue(int)));
    QObject::connect(button,SIGNAL(clicked()),act,SLOT(count()));
    QObject::connect(spinBox,SIGNAL(valueChanged(int)),act2,SLOT(display(int)));
    
    spinBox->setValue(10);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(button2);
    layout->addWidget(spinBox);
    layout->addWidget(progressBar);
    layout->addWidget(slider);
    layout->addWidget(button);
    
    window->setLayout(layout);
    window->show();
    return app.exec();
}


