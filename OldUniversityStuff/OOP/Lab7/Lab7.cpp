#include "Lab7.h"
#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;

int main(int argc,char *argv[])
{
        vector<AbstractShape*> shapes;
        shapes.push_back(new Rectangle(3.0,4.0));
        shapes.push_back(new Square(2.0));
        shapes.push_back(new Rectangle(1.0,6.0));
        shapes.push_back(new Romb(1.0));
        cout<<"Circumferences:"<<endl;
        for(int i=0;i< shapes.size();i++)
        {
            cout<< i << " "<< shapes.at(i)->get_shape() <<" : "<<shapes.at(i)->get_circumference()<<endl;
        }
}

