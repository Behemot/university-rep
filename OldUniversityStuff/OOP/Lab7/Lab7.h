#include <string>
using std::string;
class AbstractShape
{
    public:
    virtual double get_circumference()=0;
    virtual string get_shape()=0;
};

class Rectangle: public AbstractShape
{
    double height,width;
    public:
    Rectangle(double h, double w){
        height=h;
        width=w;
    }
    double get_circumference(){
        return 2*height+2*width;
    }
    string get_shape()
    {
        return "Rectangle";
    }
    
};
class Square: public AbstractShape
{
    double side;
    public:
    Square(double s)
    {
        side=s;
    }
    
    double get_circumference(){
        return 4*side;
    }
    string get_shape()
    {
        return "Square";
    }
};

class Romb: public AbstractShape
{
    double side;
    public:
    Romb(double s)
    {
        side=s;
    }
    
    double get_circumference()
    {
        return 4*side;
    }
    string get_shape()
    {
        return "Romb";
    }
};
