// Lab1 
#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::string;
using std::endl;

char tab=L'\x0009';
const int max_len=11;
string string_convert(string inp_string)
{
    string outp_string(inp_string);
    size_t pos=outp_string.find(tab);
    while(pos!=string::npos)
    {
    	outp_string.replace(pos,1,max_len-pos%max_len,' ');
        pos=outp_string.find(tab);
    }
    return outp_string;
}

int main(int argc,char *argv[])
{
	string command_promt;
    getline(cin,command_promt,'\n');
    while(command_promt!="exit"){
        cout<<string_convert(command_promt)<<endl;;
    	getline(cin,command_promt,'\n');
    }
}
