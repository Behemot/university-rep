#include <vector>
using std::vector;

struct connection_point
{
    int x,y;
};


class part
{
    vector<connection_point> con_points;
    public:
    
    vector<connection_point> get_connection_points()
    {
		return con_points;
    }

    void set_connection_points(vector<connection_point> connectors)
    {
		con_points=connectors;
    }
    virtual void install()=0;
    
};

class simple_part: public part
{
	char* material;
	public:
	
	simple_part(char* mat);
	simple_part(char* mat,vector<connection_point> connects);
	
	char* get_material()
	{
		return material;
	}
	void install();

	
	 
}; 

class composite_part: public part
{
    vector<part*> build_parts;
    public:
    

    void install();
    vector<part*> get_build_parts()
    {
        return build_parts;
    }
    void set_build_parts(vector<part*> parts)
    {
        build_parts=parts;
    }
    void add_part(part* p)
    {
        build_parts.push_back(p);
    }
    
};

class panel: public simple_part
{
    public:
    panel(vector<connection_point> con);
    void install();   
};

class leg: public simple_part
{
    public:
    leg(vector<connection_point> con);
    void install();   
};

class carriage: public simple_part
{
    public:
    carriage(vector<connection_point> con);
    void install();   
};

class handle: public simple_part
{
    public:
    handle(vector<connection_point> con);
    void install();   
};
