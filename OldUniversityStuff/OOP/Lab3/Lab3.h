#include <string>
#include "gnuplot_i.hpp"
using std::string;
class Parent 
{
    protected:
    double x,y;
    
    public:
    
    Parent();
    Parent(double X,double Y);
    void setX(double X)
    {
        x=X;
    }
    void setY(double Y)
    {
        y=Y;
    }
    double getX()
    {
        return x;
    }
    double getY()
    {
        return y;
    }
    double F1();
    double F2();
};

class Child: public Parent
{
    public:
    double F3();
    
};

class Plotter
{   public:
    void plot_function();

};

class Extended_Plotter: public Plotter
{
    public:
    void plot_function(string func);
};
