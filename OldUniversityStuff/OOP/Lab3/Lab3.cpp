#include <iostream>
#include "Lab3.h"
#include <sstream>
#include <stdlib.h>
#define sleep_time 300
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::istringstream;

int main(int argc,char *argv[])
{
    int X,Y;
    cout<<"Input X,Y: ";
    string Xc,Yc,c1;
    getline(cin,Xc,' ');
    getline(cin,Yc,'\n');
    cout<<endl;
    istringstream istream(istringstream::out);
    istream.str(Xc);
    istream>>X;
    istream.str(Yc);
    istream>>Y;
    Parent *p=new Parent(X,Y);
    Child *c=new Child();
    Plotter *plot=new Plotter();
    Plotter *eplot=new Plotter();
    c->setX(X);
    c->setY(Y);
    
    cout<<"F1: " <<p->F1()<<endl;
    cout<<"F2: " <<p->F2()<<endl;
    cout<<"F3: " <<c->F3()<<endl;
    delete(p);
    delete(c);
    /**
    cout<<"Input function: ";
    cin.clear();
    getline(cin,c1,'\n');
    cout<<endl;
    */
    eplot->plot_function();
    delete(eplot);
}


Parent::Parent()
{
    x=0;
    y=0;  
}

Parent::Parent(double X,double Y)
{
    x=X;
    y=Y;
}

double Parent::F1()
{
    return (3.0*x*x+4.0*y*y-10.0*x-5.0*y)/(1.0+2.0*x*x-3.0*y*y);
}

double Parent::F2()
{
    return (x*x+y*y)/(x*x*x-y*y*y);
}

double Child::F3()
{
    return F1()/F2();
}

void Plotter::plot_function()
{
    string func="3*x**3+5*x**2-8*x-11";
    Gnuplot *gplot_= new Gnuplot();
    gplot_->set_style("dashes");
    gplot_->cmd("set terminal x11");
    gplot_->plot_equation(func,func);
    sleep(sleep_time); 
}

void Extended_Plotter::plot_function(string func)
{
    cout<<func<<endl;
    Gnuplot *gplot_= new Gnuplot();
    gplot_->set_style("dashes");
    gplot_->cmd("set terminal x11");
    gplot_->plot_equation(func,func);
    sleep(sleep_time); 
}
