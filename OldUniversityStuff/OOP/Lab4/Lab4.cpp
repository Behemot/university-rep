#include <iostream>
#include <stdlib.h>
using std::cout;
using std::cin;
using std::endl;

template<class Type> class vector
{
    Type* values;
    int vector_size;
    public:
    
    vector()
    {
        vector_size=0;
        values=(Type*)malloc(sizeof(Type));
    }
    vector(Type* val,int size)
    {
	    values=(Type*)malloc(size*sizeof(Type));
	    vector_size=size;
	    for(int i=0;i<vector_size;i++)
	    {
	        values[i]=val[i];
	    }
    }
    
    int size()
    {
        return vector_size;
    }
    
    Type at(int i)
    {
        return values[i];
    }
    
    Type operator* (vector b)
    {
        if (b.size()==vector_size)
        {
            Type result=0.;
            for(int i=0;i<vector_size;i++)
            {
                result+=values[i]*b.at(i);
            }
            return result;
        }
        else
        {
            return NULL;
        }
    }
    
    void push_back(Type d)
    {
        
        Type* temp=(Type*)malloc((vector_size+1)*sizeof(Type));
        for(int i=0;i<vector_size;i++)
        {
            temp[i]=values[i];
        }
        delete(values);
        values=temp;
    }
    
    Type pop_back()
    {
        Type res=values[vector_size-1];
        vector_size--;
        Type* temp=(Type*)malloc((vector_size)*sizeof(Type));
        for(int i=0;i<vector_size;i++)
        {
            temp[i]=values[i];
        }
        delete(values);
        values=temp;
        return res;
    }
    
};


int main(int argc,char *argv[])
{
   
    double a[]={1.35,1.22,3.11};
    double b[]={7.11,2.3,1.52};
    vector<double> left(a,sizeof(a)/sizeof(double));
    vector<double> right(b,sizeof(b)/sizeof(double));
    cout<<"Vector #1: [";
    for(int i=0;i<left.size()-1;i++)
    {
        cout<<left.at(i)<<" , ";
    }
    cout<<left.at(left.size()-1)<<"]"<<endl;
    
    cout<<"Vector #2: [";
    for(int i=0;i<right.size()-1;i++)
    {
        cout<<right.at(i)<<" , ";
    }
    cout<<right.at(right.size()-1)<<"]"<<endl;
    
    double res=left*right;
    
    cout<<"Result: "<< res<<endl;
}
