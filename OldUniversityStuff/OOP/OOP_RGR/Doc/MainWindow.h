#include <QGraphicsView>
#include <QKeyEvent>
#include <iostream>
using std::cout;
using std::endl;

class MainWindow: public QGraphicsView
{
Q_OBJECT
public:
    MainWindow(QGraphicsScene* scene);
signals:
    void movePressed(int x,int y);
    void firePressed();        
public slots:
    void victory();
protected:
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
};

