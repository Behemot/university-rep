#ifndef TARGET_H
#define TARGET_H
#include <QGraphicsItem>
#include <QKeyEvent>
class Target :public QObject,public QGraphicsItem 
{
Q_OBJECT
public:
    Target();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                QWidget *widget);
signals:
    void victory();                
public slots:
    void move(int x,int y);
    void fire();
protected:
    void advance(int step);
    
private:
    qreal vspeed;
    qreal hspeed;
    QColor color;
};
#endif
