 #include "ship.h"
 #include "target.h"
 #include "MainWindow.h"
 #include <QtGui>

 #include <math.h>

 static const int ShipCount = 25;

 int main(int argc, char **argv)
 {
     QApplication app(argc, argv);
     qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

     QGraphicsScene scene;
     scene.setSceneRect(100, 100, 1340, 700);
     scene.setItemIndexMethod(QGraphicsScene::NoIndex);

     for (int i = 0; i < ShipCount; ++i) {
         Ship *ship = new Ship;
         ship->setPos(abs(::sin((i * 6.28)) / ShipCount) * 200+600,
                       abs(::cos((i * 6.28)) / ShipCount) * 200+400);
         scene.addItem(ship);
     }
     
     Target* targ = new Target;
     targ->setPos(scene.width()/2 ,scene.height()/2);
     scene.addItem(targ);
     
     MainWindow view(&scene);
     view.setRenderHint(QPainter::Antialiasing);
     view.setBackgroundBrush(QPixmap(":/images/space.jpg"));
     view.setCacheMode(QGraphicsView::CacheBackground);
     view.setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
     view.setDragMode(QGraphicsView::ScrollHandDrag);
     view.setWindowTitle(QT_TRANSLATE_NOOP(QGraphicsView, "Alien shooter"));
     view.resize(1440, 800);
     view.show();

     QTimer timer;
     QObject::connect(&timer, SIGNAL(timeout()), &scene, SLOT(advance()));
     QObject::connect(&view,SIGNAL(movePressed(int,int)),dynamic_cast<QObject*>(targ),SLOT(move(int,int)));
     QObject::connect(&view,SIGNAL(firePressed()),dynamic_cast<QObject*>(targ),SLOT(fire()));
     QObject::connect(dynamic_cast<QObject*>(targ),SIGNAL(victory()),&view,SLOT(victory()));
     
     timer.start(1000 / 33);
     
     return app.exec();
 }
