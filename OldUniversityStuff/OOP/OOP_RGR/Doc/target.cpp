#include "target.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include <math.h>
#include <iostream>
using std::cout;
using std::endl;
 static const double Pi = 3.14159265358979323846264338327950288419717;

 Target::Target()
     : vspeed(0), hspeed(0),color(255, 0, 0)
 {
 }

 QRectF Target::boundingRect() const
 {
     return QRectF(-2, -2,
                   29, 29);
 }

 QPainterPath Target::shape() const
 {
     QPainterPath path;
     path.addRect(0, 0, 25, 25);
     return path;
 }

 void Target::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
 {
     // Body
     static const QPointF points[5]={
        QPointF(0,0),
        QPointF(25,0),
        QPointF(25,25),
        QPointF(0,25),
        QPointF(0,0)
     };
     QPen pen;
     pen.setWidth(5);
     pen.setColor(color);
     painter->setPen(pen);
     painter->drawPolyline(points,5);
     
 }

 
 void Target::advance(int step)
 {
      if (!step)return;
      int scale=4;
      setPos(mapToParent(scale*hspeed,scale*vspeed));    
 }
 
 void Target::move(int x,int y)
 {
    hspeed+=x;
    vspeed+=y;
 }
 
 void Target::fire()
 {
    QList<QGraphicsItem *> targets = scene()->items(QPolygonF()
                                                        << mapToScene(0, 0)
                                                        << mapToScene(25, 0)
                                                        << mapToScene(25, 25)
                                                        << mapToScene(0,25));
     foreach (QGraphicsItem *item, targets) {
         if (item == this)
             continue;
         scene()->removeItem(item);
     };
    QList<QGraphicsItem *> left = scene()->items();
    if (left.size()==1){
        emit victory();
        scene()->clear();
        
    }


 } 
 
