#include "MainWindow.h"

MainWindow::MainWindow(QGraphicsScene* scene):QGraphicsView(scene)
{
   
}
void MainWindow::keyPressEvent(QKeyEvent* event)
{
   switch(event->key()){
       case Qt::Key_Up:{emit movePressed(0,-1);};break;
       case Qt::Key_Down:{emit movePressed(0,1);};break;
       case Qt::Key_Left:{emit movePressed(-1,0);};break;
       case Qt::Key_Right:{emit movePressed(1,0);};break;
       case Qt::Key_Space:{emit firePressed();};break;
   }

}

void MainWindow::keyReleaseEvent(QKeyEvent* event)
{  
   switch(event->key()){
       case Qt::Key_Up:{emit movePressed(0,1);};break;
       case Qt::Key_Down:{emit movePressed(0,-1);};break;
       case Qt::Key_Left:{emit movePressed(1,0);};break;
       case Qt::Key_Right:{emit movePressed(-1,0);};break;
       case Qt::Key_Space:{};break;
   }
}

void MainWindow::victory()
{
    this->setBackgroundBrush(QPixmap(":/images/victory.jpg"));
    this->setCacheMode(QGraphicsView::CacheBackground);
     
}

