import Jama.Matrix;
import Jama.LUDecomposition;
public class Main {

	static double[][] transitionProbabilities=
				{
				 {-1,1,0,0,0,0,0,0,0,0,0},
				 {0,-1,0.857,0,0,0.143,0,0,0,0,0},
				 {0,0,-1,0.375,0.625,0,0,0,0,0,0},
				 {0,0,0.857,-1,0,0.143,0,0,0,0,0},
				 {0,0,0.857,0,-1,0.143,0,0,0,0,0},
				 {0,0,0,0,0,-1,1,0,0,0,0},
				 {0,0,0,0,0,0,-1,0.4,0.075,0,0.525},
				 {0,0,0,0,0,0,0,-1,0.125,0,0.875},
				 {0,0,0,0,0,0,0,0,-1,1,0},
				 {0,0,0,0,0,0,0,0,0,-1,1},
				 {0,0,0,0,0,0,0,0,0,0,-1}
				};
	static double[][] nodeParameters=
				{
					{0,10,7,0,0,8,0,5,19,0,0},//ki
					{15.,0,0,10,12,0,17,0,0,12,3},//li
					{1,0,0,2,1,0,2,0,0,3,3}//N of file
				};
	static double[][] b={{-1},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}};
	
	static double Q=0;
	static double[] Nh={0,0,0};
	static double[] Qh={0,0,0};
	public static void main(String[] args) {
		Matrix transProb=new Matrix(transitionProbabilities);
		transProb=transProb.transpose();
		Matrix B=new Matrix(b);
		LUDecomposition sysSolver=new LUDecomposition(transProb);
		Matrix resMat=sysSolver.solve(B);
		double[][] res=resMat.getArray();
		
		
		for(int i=0;i<nodeParameters[0].length;i++){
			Q+=nodeParameters[0][i]*res[i][0];
			if(nodeParameters[2][i]!=0){ 
				Nh[(int)nodeParameters[2][i]-1]=Nh[(int)nodeParameters[2][i]-1]+res[i][0];
				Qh[(int)nodeParameters[2][i]-1]=Qh[(int)nodeParameters[2][i]-1]+res[i][0]*nodeParameters[1][i];
			}
		}
		
		for (int i=0;i< Qh.length;i++){
			Qh[i]/=Nh[i];
		}
		System.out.println("ni:");
		
		for(int i=0;i<res.length;i++){
			System.out.println(res[i][0]);
		}
		
		System.out.println("Q: "+Q);
		System.out.println("Nh: ");
		for(int i=0;i<Nh.length;i++){
			System.out.println(Nh[i]);
		}
		System.out.println("Qh: ");
		for(int i=0;i<Qh.length;i++){
			System.out.println(Qh[i]);
		}
		
	}

}
