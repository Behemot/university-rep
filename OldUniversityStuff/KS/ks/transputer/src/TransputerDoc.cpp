// TransputerDoc.cpp : implementation of the CTransputerDoc class
//

#include "stdafx.h"
#include "Transputer.h"

#include "TransputerDoc.h"
#include "ITransputerModel.h"
#include "Global.h"
#include ".\transputerdoc.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTransputerDoc

IMPLEMENT_DYNCREATE(CTransputerDoc, CDocument)

BEGIN_MESSAGE_MAP(CTransputerDoc, CDocument)
	ON_COMMAND(ID_DEBUG_CLOCK, OnDebugClock)
	ON_COMMAND(ID_DEBUG_STOP, OnDebugStop)
	ON_COMMAND(ID_DEBUG_PAUSE, OnDebugPause)
	ON_COMMAND(ID_DEBUG_RUN, OnDebugRun)
	ON_COMMAND(ID_DEBUG_NEXT, OnDebugNext)
END_MESSAGE_MAP()


// CTransputerDoc construction/destruction

CTransputerDoc::CTransputerDoc():m_param(0)
{
	// TODO: add one-time construction code here
}

CTransputerDoc::~CTransputerDoc()
{
	delete m_param;
}

BOOL CTransputerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	delete m_param;
	m_model.Stop();
	m_param=0;
	m_model.Clear();
	m_scale = 50;
	CBCGPRegistry* reg = Global::GetRegistry("Settings");
	reg->Read("Scale",m_scale);
	TOPOLOGY_TYPE topology_type= NONE_TOPOLOGY;
	int type=int(topology_type);
	reg->Read("Topology",type);
	topology_type = (TOPOLOGY_TYPE)type;
	if(topology_type==NONE_TOPOLOGY)
		topology_type = SIMPLE_TOPOLOGY;
	ITransputer*  transputer_type = CreateTransputerObj(topology_type);
	m_param = transputer_type->CreateTransputerParam();
	if(reg->Open("Topology param"))
	{
		for(int i=0;i<m_param->GetCount();i++)
		{
			std::string name;
			if(m_param->GetName(i,name))
			{
				CString val;
				if(reg->Read(name.c_str(),val))
					m_param->SetValue(i,std::string(val.GetString()));
			}
		}
	}
	m_model.SetTopology(transputer_type->CreateTransputerModel(m_param));
	delete transputer_type;
	Global::GetNotifyManager()->Raise(Global::ON_DOCUMENT_CHANGE,this);
	m_model.m_handle = AfxGetMainWnd()->GetSafeHwnd();
	return TRUE;
}


// CTransputerDoc serialization

void CTransputerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar<<m_param->GetTopology();
		ar<<m_param->GetCount();
		std::string name,val;
		for(int i=0;i<m_param->GetCount();i++)
		{
			m_param->GetName(i,name);
			m_param->GetValue(i,val);
			ar<<CString(name.c_str());
            ar<<CString(val.c_str());
		}
		ar<<m_model.GetProcessCount();
		for(int i=0;i<m_model.GetProcessCount();i++)
		{
			ar<<CString(m_model.GetProcess(i).name.c_str());
			ar<<m_model.GetProcess(i).priority;
			ar<<m_model.GetProcess(i).proc_number;
			ar<<(int)m_model.GetProcess(i).m_program.size();
			for(int j=0;j<(int)m_model.GetProcess(i).m_program.size();j++)
			{
				ar<<CString(m_model.GetProcess(i).m_program[j].chanel.c_str());
				ar<<m_model.GetProcess(i).m_program[j].direction;
				ar<<m_model.GetProcess(i).m_program[j].execution;
				ar<<m_model.GetProcess(i).m_program[j].transmition;
			}
		}
		ar<<m_model.GetChanelCount();
		for(int i=0;i<m_model.GetChanelCount();i++)
		{
			ar<<CString(m_model.GetChanel(i).name.c_str());
			ar<<CString(m_model.GetChanel(i).proc1.c_str());
			ar<<CString(m_model.GetChanel(i).proc2.c_str());
			ar<<(int)m_model.GetChanel(i).route.size();
			for(int j=0;j<(int)m_model.GetChanel(i).route.size();j++)
				ar<<m_model.GetChanel(i).route[j];
		}
		ar<<m_scale;
	}
	else
	{
		if(!CanModify())
			return;
		m_model.Clear();
		int n;CString s;
		ar>>n;
		ITransputer* obj = CreateTransputerObj((TOPOLOGY_TYPE)n);
		if(!obj)obj = CreateTransputerObj(SIMPLE_TOPOLOGY);
		delete m_param;
		m_param = obj->CreateTransputerParam();
		ar>>n;
		CString name,val;
		for(int i=0;i<n;i++)
		{
			ar>>name;
			ar>>val;
			m_param->SetValue(std::string(name),std::string(val));
		}
		m_model.SetTopology(obj->CreateTransputerModel(m_param));
		delete obj;

		ar>>n;
		for(int i=0;i<n;i++)
		{
			TransputerModel::Process process;
			ar>>s;
			process.name = s;
			ar>>process.priority;
			ar>>process.proc_number;
			int m;
			ar>>m;
			process.m_program.resize(m);
			for(int j=0;j<m;j++)
			{
				ar>>s;
				process.m_program[j].chanel = s;
				ar>>process.m_program[j].direction;
				ar>>process.m_program[j].execution;
				ar>>process.m_program[j].transmition;
			}
			m_model.AddProcess(process);
		}
		ar>>n;
		for(int i=0;i<n;i++)
		{
			TransputerModel::Chanel chanel;
			ar>>s;chanel.name=s;
			ar>>s;chanel.proc1=s;
			ar>>s;chanel.proc2=s;
			int m;
			ar>>m;
			chanel.route.resize(m);
			for(int j=0;j<m;j++)
				ar>>chanel.route[j];
			m_model.AddChanel(chanel);
		}
		ar>>m_scale;
		Global::GetNotifyManager()->Raise(Global::ON_DOCUMENT_CHANGE,this);
	}
	SetModifiedFlag(FALSE);
}


// CTransputerDoc diagnostics

#ifdef _DEBUG
void CTransputerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTransputerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CTransputerDoc commands

bool CTransputerDoc::CanModify()
{
	if(m_model.IsLoaded())
	{
		if(AfxMessageBox("You cannot edit because the simulation started.\nDo you want to stop the simulation?",MB_YESNO)==IDYES)
		{
			m_model.Stop();
			SetModifiedFlag();
			return true;
		}
		return false;
	}
	SetModifiedFlag();
	return true;
}

int CTransputerDoc::AddProcess(CString& name,int priority,int proc_num)
{
	if(!CanModify())
		return 0;
	TransputerModel::Process p;
	p.name = name;
	p.priority = priority;
	p.proc_number = proc_num;
	int id = m_model.AddProcess(p);
	if(id!=-1)
		Global::GetNotifyManager()->Raise(Global::ON_PROCESS_COUNT_CHANGE,this);
	return id;
}

CString CTransputerDoc::GetNewProcessName(void)
{
	return m_model.GetNewProcessName().c_str();
}

int CTransputerDoc::GetProcessCount(void)
{
	return m_model.GetProcessCount();
}

CString CTransputerDoc::GetProcessName(int id)
{

	return m_model.GetProcess(id).name.c_str();
}

void CTransputerDoc::DeleteRows(std::vector<int>& rows)
{
	if(!CanModify())
		return;
	std::vector<std::string>names;
	for(int i=0;i<(int)rows.size();i++)
		names.push_back(m_model.GetProcess(rows[i]).name);
	std::vector<int>chanels;
	for(int i=0;i<m_model.GetChanelCount();i++)
		for(int j=0;j<(int)names.size();j++)
			if(m_model.GetChanel(i).proc1 == names[j] ||
				m_model.GetChanel(i).proc2 == names[j])
				chanels.push_back(i);
	for(int i=0;i<(int)names.size();i++)
		m_model.DeleteProcess(names[i]);
	DeleteChanels(chanels);
	if(names.size())
		Global::GetNotifyManager()->Raise(Global::ON_PROCESS_COUNT_CHANGE,this);
}

BOOL CTransputerDoc::SetProcessName(int id, LPCTSTR name)
{
	if(!CanModify())
		return true;
	std::string proc_name = m_model.GetProcess(id).name;
	bool chanel_chaneged = true;
	for(int i=0;i<m_model.GetProcessCount();i++)
	{
		if(m_model.GetChanel(i).proc1==proc_name)
		{
			m_model.GetChanel(i).proc1 = name;
			chanel_chaneged = true;
		}
		if(m_model.GetChanel(i).proc2==proc_name)
		{
			m_model.GetChanel(i).proc2 = name;
			chanel_chaneged = true;
		}
	}
	if(chanel_chaneged)
		Global::GetNotifyManager()->Raise(Global::ON_CHANEL_CHANGE,this);
	bool res = m_model.SetProcessName(id,std::string(name));
	if(res)
		Global::GetNotifyManager()->Raise(Global::ON_PROCESS_CHANGE,this);
	return res;
}

CString CTransputerDoc::GetNewChanelName(void)
{
	return m_model.GetNewChanelName().c_str();
}

TransputerModel* CTransputerDoc::GetTransputerModel(void)
{
	return &m_model;
}

int CTransputerDoc::GetProcessPriority(int id)
{
	return m_model.GetProcess(id).priority;
}

int CTransputerDoc::GetProcNumber(int id)
{
	return m_model.GetProcess(id).proc_number;
}

void CTransputerDoc::SetProcessPriority(int id, int priority)
{
	if(!CanModify())
		return;
	m_model.GetProcess(id).priority=priority;
}

void CTransputerDoc::SetProcNumber(int id, int num)
{
	if(!CanModify())
		return;
	m_model.GetProcess(id).proc_number=num;
}

int CTransputerDoc::AddChanel(CString& name, CString& proc1, CString& proc2)
{
	if(!CanModify())
		return 0;
	TransputerModel::Chanel c;
	c.name = name;
	c.proc1 = proc1;
	c.proc2 = proc2;
	int id = m_model.AddChanel(c);
	if(id!=-1)
		Global::GetNotifyManager()->Raise(Global::ON_CHANEL_COUNT_CHANGE,this);
	return id;
}

CString CTransputerDoc::GetChanelName(int id)
{
	return m_model.GetChanel(id).name.c_str();
}

CString CTransputerDoc::GetChanelProc1(int id)
{
	return m_model.GetChanel(id).proc1.c_str();
}

CString CTransputerDoc::GetChanelProc2(int id)
{
	return m_model.GetChanel(id).proc2.c_str();
}

void CTransputerDoc::SetProc1Name(int id, CString& name)
{
	if(!CanModify())
		return;
	int i = m_model.GetProcessID(std::string(name.GetString()));
	if(i==-1)
		return;
	m_model.GetChanel(id).proc1=name;
}

void CTransputerDoc::SetProc2Name(int id, CString& name)
{
	if(!CanModify())
		return;
	int i = GetProcessID(name);
	if(i==-1)
		return;
	m_model.GetChanel(id).proc2=name;
}

int CTransputerDoc::GetProcessID(CString& name)
{
	return m_model.GetProcessID(std::string(name));
}

int CTransputerDoc::GetChanelCount(void)
{
	return m_model.GetChanelCount();
}

void CTransputerDoc::DeleteChanels(std::vector<int>& rows)
{
	if(!CanModify())
		return;
	std::vector<std::string>names;
	for(int i=0;i<(int)rows.size();i++)
		names.push_back(m_model.GetChanel(rows[i]).name);
	bool program_changed = false;
	for(int i=0;i<(int)names.size();i++)
	{
		m_model.DeleteChanel(names[i]);
		program_changed |= ProgramChanelName(CString(names[i].c_str()),CString(""));
	}
	if(names.size())
		Global::GetNotifyManager()->Raise(Global::ON_CHANEL_COUNT_CHANGE,this);
	if(program_changed)
		Global::GetNotifyManager()->Raise(Global::ON_PROGRAM_CHANGE,this);
}

BOOL CTransputerDoc::SetChanelName(int row, CString& name)
{
	if(!CanModify())
		return true;
	CString pname = m_model.GetChanel(row).name.c_str();
	bool res = m_model.SetChanelName(row,std::string(name));
	if(res)
	{
		if(ProgramChanelName(pname,name))
			Global::GetNotifyManager()->Raise(Global::ON_PROGRAM_CHANGE,this);
		Global::GetNotifyManager()->Raise(Global::ON_CHANEL_CHANGE,this);
	}
	return res;
}

int CTransputerDoc::GetProgramLineCount(CString& process)
{
	int id = m_model.GetProcessID(std::string(process));
	if(id<0)
		return 0;
	return (int)m_model.GetProcess(id).m_program.size();
}

bool CTransputerDoc::ProgramChanelName(CString& prev, CString& current)
{
	std::string old = prev;
	std::string cur = current;
	bool res = false;
	for(int i=0;i<GetProcessCount();i++)
	{
		TransputerModel::Process& process = m_model.GetProcess(i);
		for(int j=0;j<(int)process.m_program.size();j++)
		{
			if(process.m_program[j].chanel == old)
			{
				process.m_program[j].chanel = cur;
				res = true;
			}
		}
	}
	return res;
}

void CTransputerDoc::InsertProgramLine(CString& process,int row)
{
	if(!CanModify())
		return;
	if(m_model.InsertProgramLine(std::string(process),row))
		Global::GetNotifyManager()->Raise(Global::ON_PROGRAM_LINE_COUNT_CHANGE,this);
}

void CTransputerDoc::DeleteProgramLines(CString& process, std::vector<int>& lines)
{
	if(!CanModify())
		return;
	std::string p = process;
	bool res = false;
	for(int i=0;i<(int)lines.size();i++)
		res |= m_model.DeleteProgramLine(p,lines[i]);
	if(res) 
		Global::GetNotifyManager()->Raise(Global::ON_PROGRAM_LINE_COUNT_CHANGE,this);
}

int CTransputerDoc::GetProgramExecution(CString& process, int row)
{
	return m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].execution;
}

int CTransputerDoc::GetProgramTransmition(CString& process, int row)
{
	return m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].transmition;
}

CString CTransputerDoc::GetProgramChanel(CString& process, int row)
{
	return m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].chanel.c_str();
}
void CTransputerDoc::SetProgramExecution(CString& process, int row, int time)
{
	if(!CanModify())
		return;
	m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].execution=time;
}

void CTransputerDoc::SetProgramTransmition(CString& process, int row, int time)
{
	if(!CanModify())
		return;
	m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].transmition=time;
}

void CTransputerDoc::SetProgramChanel(CString& process, int row, CString& chanel)
{
	if(!CanModify())
		return;
	m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].chanel=chanel;
}

bool CTransputerDoc::GetProgramDirection(CString& process, int row)
{
	return m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].direction;
}

void CTransputerDoc::SetProgramDirection(CString& process, int row,bool direction)
{
	if(!CanModify())
		return;
	m_model.GetProcess(m_model.GetProcessID(std::string(process))).m_program[row].direction=direction;
}

bool CTransputerDoc::VerifyModel()
{
	std::vector<std::string> errors;
	std::vector<std::string> warnings;
	m_model.Verify(errors,warnings);
	if(errors.size())
	{
		CString s="You cannot start simulation due to errors in model:\n";
		for(int i=0;i<(int)min(3,errors.size());i++)
		{
			s+="   -"+CString(errors[i].c_str())+"\n";
		}
		if(errors.size()>3)
			s+="   ...";
		AfxMessageBox(s,MB_OK);
		return false;
	}
	return true;
}

void CTransputerDoc::OnDebugClock()
{
	if(!VerifyModel())
		return;
	try
	{
	if(m_model.IsLoaded())
		m_model.Clock();
	else
		m_model.Load();
	}
	catch(TransputerModel::EventFinished)
	{
		AfxMessageBox("All process finished");
	}
	catch(TransputerModel::EventDeadlock)
	{
		AfxMessageBox("Deadlock detected");
	}
	Global::GetNotifyManager()->Raise(Global::ON_SIMULATION_STATE_CHANGED,this);
}

BOOL CTransputerDoc::IsProcessActive(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessActive(id);
}

BOOL CTransputerDoc::IsProcessReady(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessReady(id);
}

BOOL CTransputerDoc::IsProcessWaiting(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessWaiting(id);
}

BOOL CTransputerDoc::IsProcessTransmiting(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessTransmiting(id);
}

BOOL CTransputerDoc::IsProcessWriting(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessWriting(id);
}

BOOL CTransputerDoc::IsProcessReading(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessReading(id);
}

BOOL CTransputerDoc::IsProcessFinished(int id)
{
	if(!m_model.CanQueryState())
		return false;
	return m_model.IsProcessFinished(id);
}

int CTransputerDoc::GetProcessActiveBlock(int id)
{
	if(!m_model.CanQueryState())
		return -1;
	return m_model.GetProcessActiveBlock(id);
}

int CTransputerDoc::GetProcessBlockTimeLeft(int id)
{
	if(!m_model.CanQueryState())
		return -1;
	return m_model.GetProcessBlockTimeLeft(id);
}

void CTransputerDoc::OnDebugStop()
{
	m_model.Stop();
	Global::GetNotifyManager()->Raise(Global::ON_SIMULATION_STATE_CHANGED,this);
}

CString CTransputerDoc::GetActive(int id)
{
	if(!m_model.CanQueryState())
		return CString();
	int proc = m_model.GetActiveProces(id);
	if(proc==-1)
		return CString();
	return m_model.GetProcess(proc).name.c_str();
}

CString CTransputerDoc::GetMemoryState(int proc_num)
{
	if(!m_model.CanQueryState())
		return CString();
	int ch = m_model.GetActiveChanel(proc_num,proc_num);
	if(ch==-1)
		return CString();
	CString s;
	int proc1 = m_model.GetProcessID(m_model.GetChanel(ch).proc1);
	int proc2 = m_model.GetProcessID(m_model.GetChanel(ch).proc2);
	s.Format("%s %s %s %s",
		m_model.GetProcess(proc1).name.c_str(),
		IsProcessReading(proc1)?"?":"!",
		IsProcessReading(proc2)?"?":"!",
		m_model.GetProcess(proc2).name.c_str());	
	return s;
}

CString CTransputerDoc::GetHighQueue(int proc_num)
{
	if(!m_model.CanQueryState())
		return CString();
	std::vector<int> v;
	m_model.GetHighQueue(v,proc_num);
	CString s;
	for(int i=0;i<(int)v.size();i++)
	{
		if(i)
			s+="; ";
		s+=m_model.GetProcess(v[i]).name.c_str();
	}
	return s;
}

CString CTransputerDoc::GetLowQueue(int proc_num)
{
	if(!m_model.CanQueryState())
		return CString();
	std::vector<int> v;
	m_model.GetLowQueue(v,proc_num);
	CString s;
	for(int i=0;i<(int)v.size();i++)
	{
		if(i)
			s+="; ";
		s+=m_model.GetProcess(v[i]).name.c_str();
	}
	return s;
}

CString CTransputerDoc::GetWaitingQueue(int proc_num)
{
	if(!m_model.CanQueryState())
		return CString();
	std::vector<int> v;
	m_model.GetWaitingQueue(v,proc_num);
	CString s;
	for(int i=0;i<(int)v.size();i++)
	{
		if(i)
			s+="; ";
		s+=m_model.GetProcess(v[i]).name.c_str();
	}
	return s;
}

CString CTransputerDoc::GetLinkState(int proc1, int proc2, bool first)
{
	if(!m_model.CanQueryState())
		return CString();
	int ch = m_model.GetActiveChanel(proc1,proc2);
	if(ch==-1)
		return CString();
	int p1 = m_model.GetProcessID(m_model.GetChanel(ch).proc1);
	int p2 = m_model.GetProcessID(m_model.GetChanel(ch).proc2);
	if(first)
	{
		if(m_model.GetProcess(p1).proc_number!=proc1)
			p1 = p2;	
	}
	else
	{
		if(m_model.GetProcess(p1).proc_number!=proc2)
			p1 = p2;
	}
	CString s = m_model.GetProcess(p1).name.c_str();
	if(first)
		s+= IsProcessReading(p1)?" ?":" !";
	else
		s = IsProcessReading(p1)?"? ":"! " + s;
	return s;
}

void CTransputerDoc::OnDebugPause()
{
	m_model.Pause();
	Global::GetNotifyManager()->Raise(Global::ON_SIMULATION_STATE_CHANGED,this);
}

void CTransputerDoc::OnDebugRun()
{
	if(!m_model.IsLoaded())
		m_model.Load();
	::AfxBeginThread(TransputerModel::Start, &m_model);
}

CString CTransputerDoc::GetTickCount(bool force)
{
	if(!force&&!m_model.CanQueryState())
		return CString();
	CString s;
	s.Format("%d",m_model.GetClocks());
	return s;
}

int CTransputerDoc::GetScale()
{
	return m_scale;
}

void CTransputerDoc::SetScale(int scale)
{
	m_scale = scale;
	UpdateAllViews(NULL);
}

ITransputerParam* CTransputerDoc::GetTopologyParam(void)
{
	return m_param;
}

void CTransputerDoc::SetTopologyParam(int type,ITransputerParam::Dictionary& dict)
{
	if(!CanModify())
		return;
	ITransputer* obj = CreateTransputerObj((TOPOLOGY_TYPE)type);
	if(!obj)
		return;
	delete m_param;
	m_param = obj->CreateTransputerParam();
	m_param->SetValues(dict);
	m_model.SetTopology(obj->CreateTransputerModel(m_param));
	delete obj;
	UpdateAllViews(NULL);
}

CString CTransputerDoc::GetRoute(int row)
{
	if(row<0||row>=m_model.GetChanelCount())
		return CString();
	std::vector<int>& v = m_model.GetChanel(row).route;
	CString s,res;
	for(int i=0;i<(int)v.size();i++)
	{
		s.Format("%d,",v[i]);
		res+=s;
	}
	if(res.GetLength())
		res.Delete(res.GetLength()-1);
	return res;
}

bool CTransputerDoc::SetRoute(int row, CString& text)
{
	if(!CanModify())
		return true;
	CString str=text.Trim(),s;
	std::vector<int> v;
	for(;!str.IsEmpty();)
	{
		int p = str.Find(',');
		if(p==-1)
			p=str.GetLength();
		s = str.Left(p);
		str = str.Right(str.GetLength()-p-1);
		s = s.Trim();
		if(s.IsEmpty())
			return false;
		int n;char c;
		if(sscanf(s.GetString(),"%d%c",&n,&c)!=1)
			return false;
		v.push_back(n);
	}
	for(int i=0;i<(int)v.size();i++)
		for(int j=i+1;j<(int)v.size();j++)
			if(v[i]==v[j])
				return false;
	if(!CanModify())
		return false;
	TransputerModel::Chanel& c = m_model.GetChanel(row);
	c.route = v;
	return true;
}

void CTransputerDoc::ClearAllRoutes(void)
{
	if(!CanModify())
		return;
	for(int i=0;i<m_model.GetChanelCount();i++)
		m_model.GetChanel(i).route = std::vector<int>();
}

void CTransputerDoc::DoDefaultRouting(void)
{
	if(!CanModify())
		return;
	for(int i=0;i<m_model.GetChanelCount();i++)
	{
		if(!m_model.VerifyRoute(i) || m_model.GetChanel(i).route.size()==0)
			m_model.DoRouting(i);
	}
}

void CTransputerDoc::OnDebugNext()
{
	if(!m_model.IsLoaded())
		m_model.Load();
	::AfxBeginThread(TransputerModel::Step, &m_model);
}

int CTransputerDoc::GetTransmitionSource(int process)
{
	if(!m_model.CanQueryState())
		return -1;
	if(!IsProcessTransmiting(process))
		return -1;
	int ch = m_model.GetChanelID(m_model.GetProcess(process).m_program[m_model.GetProcess(process).cur_block].chanel);
	int p1=-1,p2=-2;
	m_model.GetLink(m_model.GetChanel(ch),p1,p2);	
	if(p1==-1 || m_model.GetActiveChanel(p1,p2)!=ch)
	{
		m_model.GetFirstLink(m_model.GetChanel(ch),p1,p2,process);
		if(p1==-1 || m_model.GetHalfActiveChanel(p1,p2,process)!=ch)
			return -1;
	}
	if(m_model.GetProcess(process).name==m_model.GetChanel(ch).proc1)
		return p1;
	return p2;
}

int CTransputerDoc::GetTransmitionDestination(int process)
{
	if(!m_model.CanQueryState())
		return -1;
	if(!IsProcessTransmiting(process))
		return -1;
	int ch = m_model.GetChanelID(m_model.GetProcess(process).m_program[m_model.GetProcess(process).cur_block].chanel);
	int p1=-1,p2=-2;
	m_model.GetLink(m_model.GetChanel(ch),p1,p2);	
	if(p1==-1 || m_model.GetActiveChanel(p1,p2)!=ch)
	{
		m_model.GetFirstLink(m_model.GetChanel(ch),p1,p2,process);
		if(p1==-1 || m_model.GetHalfActiveChanel(p1,p2,process)!=ch)
			return -1;
	}
	if(m_model.GetProcess(process).name==m_model.GetChanel(ch).proc1)
		return p2;
	return p1;
}

void CTransputerDoc::OnCloseDocument()
{
	m_model.Stop();
	CDocument::OnCloseDocument();
}
