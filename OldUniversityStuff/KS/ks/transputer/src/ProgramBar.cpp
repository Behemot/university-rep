#include "StdAfx.h"
#include ".\programbar.h"
#include "DataWrap.h"
#include "DlgProcess.h"
#include "Global.h"

BEGIN_MESSAGE_MAP(CProgramBar, CBCGPDockingControlBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_PROGRAM_ADD, OnInsert)
	ON_COMMAND(ID_PROGRAM_DELETE, OnDelete)
	ON_COMMAND(ID_CMB_PROCESS,OnProcess)
	ON_CBN_SELCHANGE(ID_CMB_PROCESS, OnProcess)
	ON_UPDATE_COMMAND_UI(ID_PROCESS_ADD, OnUpdateInsert)
END_MESSAGE_MAP()

CProgramBar::CProgramBar(void)
{
	Global::GetNotifyManager()->Register(Global::ON_DOCUMENT_CHANGE,Delegate(this,(Delegate::FUNC)SetDocument));
	Global::GetNotifyManager()->Register(Global::ON_PROCESS_CHANGE,Delegate(this,(Delegate::FUNC)UpdateProcessCombo));
	Global::GetNotifyManager()->Register(Global::ON_PROCESS_COUNT_CHANGE,Delegate(this,(Delegate::FUNC)UpdateProcessCombo));
	Global::GetNotifyManager()->Register(Global::ON_PROGRAM_CHANGE,Delegate(this,(Delegate::FUNC)OnChange));
}

CProgramBar::~CProgramBar(void)
{
	Global::GetNotifyManager()->Release(Global::ON_DOCUMENT_CHANGE,Delegate(this,(Delegate::FUNC)SetDocument));
	Global::GetNotifyManager()->Release(Global::ON_PROCESS_CHANGE,Delegate(this,(Delegate::FUNC)UpdateProcessCombo));
	Global::GetNotifyManager()->Release(Global::ON_PROCESS_COUNT_CHANGE,Delegate(this,(Delegate::FUNC)UpdateProcessCombo));
	Global::GetNotifyManager()->Release(Global::ON_PROGRAM_CHANGE,Delegate(this,(Delegate::FUNC)OnChange));
}

int CProgramBar::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CBCGPDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty ();
	// Create view:
	if (!m_wndProgram.Create (rectDummy,this,1003,WS_CHILD|WS_VISIBLE))
	{
		TRACE0("Failed to create process grid\n");
		return -1;      // fail to create
	}
	m_wndProgram.SetDataModel(&ProcessDataSource(NULL));
	m_wndToolBar.Create (this, dwDefaultToolbarStyle, IDR_PROGRAM);
	m_wndToolBar.LoadToolBar (IDR_PROGRAM, 0, 0, TRUE /* Is locked */);
	CBCGPToolbarComboBoxButton cmbProcess(ID_CMB_PROCESS, 
			CImageHash::GetImageOfCommand (ID_CMB_PROCESS, FALSE),
			CBS_DROPDOWNLIST);
	m_wndToolBar.ReplaceButton(ID_CMB_PROCESS,cmbProcess);

	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
		
	m_wndToolBar.SetBarStyle (
		m_wndToolBar.GetBarStyle () & 
			~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

	m_wndToolBar.SetOwner (this);
	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame (FALSE);
	// Fill view context (dummy code, don't seek here something magic :-)):
	AdjustLayout ();
	return 0;
}

void CProgramBar::AdjustLayout ()
{
	if (GetSafeHwnd () == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect (rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout (FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos (NULL, rectClient.left, rectClient.top, 
								rectClient.Width (), cyTlb,
								SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndProgram.SetWindowPos (NULL, rectClient.left , rectClient.top + cyTlb ,
								rectClient.Width () , rectClient.Height () - cyTlb ,
								SWP_NOACTIVATE | SWP_NOZORDER);
}
void CProgramBar::OnSize(UINT nType, int cx, int cy)
{
	CBCGPDockingControlBar::OnSize(nType, cx, cy);

	AdjustLayout();
}

void CProgramBar::OnInsert()
{
	CCellID celID = m_wndProgram.GetFocusCell();
	int row = celID.row;
	if(row<0)
		row = 0;
	m_doc->InsertProgramLine(m_process,row);
	row++;
	m_wndProgram.SetFocus();
	m_wndProgram.SetFocusCell(row,m_wndProgram.GetFixedColumnCount());
	m_wndProgram.SetSelectedRange(row,m_wndProgram.GetFixedColumnCount(),row,m_wndProgram.GetColumnCount()-1,1);
}

void CProgramBar::OnDelete()
{
	CCellRange range = m_wndProgram.GetSelectedCellRange();
	std::vector<int> del_rows;
	for(int i=range.GetMinRow();i<=range.GetMaxRow();i++)
		if(m_wndProgram.IsCellSelected(i,1))
			del_rows.push_back(i-1);
	if(!del_rows.size())
		return;
	m_doc->DeleteProgramLines(m_process,del_rows);
	m_wndProgram.SetFocus();
	m_wndProgram.SetFocusCell(-1,-1);
	m_wndProgram.SetSelectedRange(-1,-1,-1,-1);
}

void CProgramBar::OnUpdateInsert(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
}

int CProgramBar::SetDocument(CTransputerDoc* doc)
{
	m_doc=doc;
	m_wndProgram.SetDataModel(&ProgramDataSource(m_doc,m_process));
	UpdateProcessCombo(m_doc);
	return 0;
}

int CProgramBar::OnChange(void* p)
{
	m_wndProgram.RedrawWindow();
	return 0;
}

int CProgramBar::UpdateProcessCombo(void* p)
{
	CBCGPToolbarComboBoxButton* cmbProcess= (CBCGPToolbarComboBoxButton*)m_wndToolBar.GetButton(m_wndToolBar.CommandToIndex(ID_CMB_PROCESS));
	if(!cmbProcess||!m_doc)
		return 0;
	cmbProcess->RemoveAllItems();
	for(int i=0;i<m_doc->GetProcessCount();i++)
		cmbProcess->AddItem(m_doc->GetProcessName(i));
	int id = cmbProcess->GetComboBox()->FindString(0,m_process);
	cmbProcess->GetComboBox()->SetCurSel(id);
	if(cmbProcess->GetComboBox()->GetCurSel()==-1 && !m_process.IsEmpty() || cmbProcess->GetItem(cmbProcess->GetComboBox()->GetCurSel()) && cmbProcess->GetItem()!=m_process)
		OnProcess();
	m_wndToolBar.RedrawWindow();
	return 0;	
}

void CProgramBar::OnProcess()
{
	CBCGPToolbarComboBoxButton* cmbProcess= (CBCGPToolbarComboBoxButton*)m_wndToolBar.GetButton(m_wndToolBar.CommandToIndex(ID_CMB_PROCESS));
	CString process = cmbProcess->GetItem(cmbProcess->GetCurSel());
	if (m_process != process)
	{
		m_process = process;
		m_wndProgram.SetDataModel(&ProgramDataSource(m_doc,m_process));
	}
}

void CProgramBar::SelectProcess(CString& name)
{
	if(m_process == name)
		return;
	CBCGPToolbarComboBoxButton* cmbProcess= (CBCGPToolbarComboBoxButton*)m_wndToolBar.GetButton(m_wndToolBar.CommandToIndex(ID_CMB_PROCESS));
	cmbProcess->SelectItem(name);
	m_wndToolBar.RedrawWindow();
	OnProcess();
}
