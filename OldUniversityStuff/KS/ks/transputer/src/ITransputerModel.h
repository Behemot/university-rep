#pragma once
#include "ITransputerView.h"
#include <string>
#include <vector>

const std::string VIRTUAL_CHANELS = "Use Virtual Chanels";
const std::string TRANSPUTER_COUNT = "Transputer count";
const std::string GRID_WIDTH = "Torus width";
const std::string GRID_HEIGHT = "Torus height";

class ITransputer
{
public:
	ITransputer(void);
	~ITransputer(void);
	virtual ITransputerParam* CreateTransputerParam()=0;
	virtual ITopology* CreateTransputerModel(ITransputerParam* param)=0;
	virtual ITransputerView* CreateTransputerView(TransputerModel* model)=0;
};

enum TOPOLOGY_TYPE{NONE_TOPOLOGY,SIMPLE_TOPOLOGY,CIRCLE_TOPOLOGY,TOR_TOPOLOGY,STAR_TOPOLOGY};

class SimpleModel:public ITransputer
{
public:
	virtual ITransputerParam* CreateTransputerParam();
	virtual ITopology* CreateTransputerModel(ITransputerParam* param);
	virtual ITransputerView* CreateTransputerView(TransputerModel* model);
};

class CircleModel:public ITransputer
{
public:
	virtual ITransputerParam* CreateTransputerParam();
	virtual ITopology* CreateTransputerModel(ITransputerParam* param);
	virtual ITransputerView* CreateTransputerView(TransputerModel* model);
};

class TorModel:public ITransputer
{
public:
	virtual ITransputerParam* CreateTransputerParam();
	virtual ITopology* CreateTransputerModel(ITransputerParam* param);
	virtual ITransputerView* CreateTransputerView(TransputerModel* model);
};

class StarModel:public ITransputer
{
public:
	virtual ITransputerParam* CreateTransputerParam();
	virtual ITopology* CreateTransputerModel(ITransputerParam* param);
	virtual ITransputerView* CreateTransputerView(TransputerModel* model);
};

class SimpleTopology:public ITopology
{
	bool m_virtual;
public:
	SimpleTopology(ITransputerParam* param):m_virtual(true)
	{
		std::string s;
		param->GetValue(VIRTUAL_CHANELS,s);
		int v;
		sscanf(s.c_str(),"%d",&v);
		m_virtual = v?1:0;
	}
	virtual int Count(){return 2;}
	virtual int Width(){return -1;}
	virtual int Height(){return -1;}
	virtual bool HasLink(int p1,int p2){return true;}
	virtual int GetLinkCount(){return 3;}
	virtual int GetLinkID(int p1,int p2)
	{
		if(p1!=p2)
			return 2;
		return p1;
	}
	virtual int GetNextLink(int p1,int p2)
	{
		if(p1 == p2)
			return p1;
		return p2;
	}
	virtual bool IsLinkVirtual(){return m_virtual;}
};

class CircleTopology:public ITopology
{
	int m_count;
	bool m_virtual;
public:
	CircleTopology(ITransputerParam* param)
	{
		std::string s;
		param->GetValue(TRANSPUTER_COUNT,s);
		sscanf(s.c_str(),"%d",&m_count);
		param->GetValue(VIRTUAL_CHANELS,s);
		int v;
		sscanf(s.c_str(),"%d",&v);
		m_virtual = v?1:0;
	}
	virtual int Count(){return m_count;}
	virtual int Width(){return -1;}
	virtual int Height(){return -1;}
	virtual bool IsLinkVirtual(){return m_virtual;}
	virtual bool HasLink(int p1,int p2)
	{
		if(p1==p2)
			return true;
		if(p2<p1)
		{
			int t = p1;
			p1 = p2;
			p2 = t;
		}
		if(p1+1==p2||p1==0&&p2==Count()-1)
			return true;
		return false;
	}
	virtual int GetLinkCount(){return m_count*2;}
	virtual int GetLinkID(int p1,int p2)
	{
		if(p2<p1)
		{
			int t = p1;
			p1 = p2;
			p2 = t;
		}
		if(p1==p2)
			return p1;
		if(p1==0&&p2==m_count-1)
			return p2+m_count;
		if(p1+1==p2)
			return p1+m_count;
		return -1;
	}
	virtual int GetNextLink(int p1,int p2)
	{
		if(p1==p2)
			return p1;
		int ma = max(p1,p2);
		int mi = min(p1,p2);
		bool cc = (ma-mi)<(Count()-ma+mi);
		if(cc)
		{
			if(p1<p2)
				return p1+1;
			return p1-1;
		}	
		if(p1<p2)
		{
			if(p1==0)
				return Count()-1;
			return p1-1;
		}
		if(p1==Count()-1)
			return 0;
		return p1+1;
	}
};

class TorTopology:public ITopology
{
	int m_width,m_height;
	bool m_virtual;
public:
	TorTopology(ITransputerParam* param)
	{
		std::string s;
		param->GetValue(GRID_WIDTH,s);
		sscanf(s.c_str(),"%d",&m_width);
		param->GetValue(GRID_HEIGHT,s);
		sscanf(s.c_str(),"%d",&m_height);
		param->GetValue(VIRTUAL_CHANELS,s);
		int v;
		sscanf(s.c_str(),"%d",&v);
		m_virtual = v?1:0;
	}
	virtual int Count(){return m_width*m_height;}
	virtual int Width(){return m_width;}
	virtual int Height(){return m_height;}
	virtual bool HasLink(int p1,int p2)
	{
		if(p1==p2)
			return true;
		if(p2<p1)
		{
			int t = p1;
			p1 = p2;
			p2 = t;
		}
		int x1 = p1%Width(),x2 = p2%Width(),y1 = p1/Width(),y2 = p2/Width();
		if(y1==y2&&(x1+1==x2 || x1==0 && x2==Width()-1))
			return true;
		if(x1==x2&&(y1+1==y2 || y1==0 && y2==Width()-1))
			return true;
		return false;
	}
	virtual int GetLinkCount(){return 3*m_width*m_height;}
	virtual int GetLinkID(int p1,int p2)
	{
		if(p2<p1)
		{
			int t = p1;
			p1 = p2;
			p2 = t;
		}
		if(p1==p2)
		{
			return p1;
		}
		int y1 = p1/m_width,y2 = p2/m_width, x1 = p1%m_width, x2 = p2%m_width;
		if(y1==y2)
		{
			if(x1+1==x2)
				return m_width*m_height+m_width*(y1)+x1;
			if(x1==0&&x2==m_width-1)
				return m_width*m_height+m_width*(y1+1)-1;
		}
		if(x1==x2)
		{
			if(y1+1==y2)
				return 2*m_width*m_height+m_width*(x1)+y1;
			if(y1==0&&y2==m_width-1)
				return 2*m_width*m_height+m_width*(x1+1)-1;
		}
		return -1;
	}
	virtual int GetNextLink(int p1,int p2)
	{
		if(p1==p2)
			return p1;
		int y1 = p1/m_width,y2 = p2/m_width, x1 = p1%m_width, x2 = p2%m_width;
		int dl,dr;
		if(x1==x2)
		{
			dr = y2-y1;
			if(dr<0)dr+=m_width;
			dl = y1-y2;
			if(dl<0)dl+=m_width;         
			p1+=dl<dr?-m_width:m_width;
		}
		else
		{
			dr = x2-x1;
			if(dr<0)dr+=m_width;
			dl = x1-x2;
			if(dl<0)dl+=m_width;         
			p1+=dl<dr?(x1==0?m_width-1:-1):(x1==m_width-1?-m_width+1:1);
		}
		if(p1<0)
			p1+=m_width*m_height;
		if(p1>=m_width*m_height)
			p1-=m_width*m_height;
		return p1;
	}
	virtual bool IsLinkVirtual(){return m_virtual;}
};

class StarTopology:public ITopology
{
	int m_count;
	bool m_virtual;
public:
	StarTopology(ITransputerParam* param)
	{
		std::string s;
		param->GetValue(TRANSPUTER_COUNT,s);
		sscanf(s.c_str(),"%d",&m_count);
		param->GetValue(VIRTUAL_CHANELS,s);
		int v;
		sscanf(s.c_str(),"%d",&v);
		m_virtual = v?1:0;
	}
	virtual int Count(){return m_count;}
	virtual int Width(){return -1;}
	virtual int Height(){return -1;}
	virtual bool HasLink(int p1,int p2){return true;}
	virtual int GetLinkCount(){return m_count*2+1;}
	virtual int GetLinkID(int p1,int p2)
	{
		if(p2<p1)
		{
			int t = p1;
			p1 = p2;
			p2 = t;
		}
		if(p1==p2)
			return p1;
		if(p1==0&&p2==m_count-1)
			return p2+m_count;
		if(p1+1==p2)
			return p1+m_count;
		return 2*m_count;	
	}
	virtual int GetNextLink(int p1,int p2)
	{
		if(p1==p2)
			return p1;
		if(p1+1==p2 || p1==m_count&& p2==0)
			return p2;
		if(p2+1==p1 || p2==m_count&& p1==0)
			return p2;
		return p2;

	}
	virtual bool IsLinkVirtual(){return m_virtual;}
};

ITransputer* CreateTransputerObj(TOPOLOGY_TYPE type);
void GetTopologies(std::vector<int>& ids,std::vector<std::string>& names);