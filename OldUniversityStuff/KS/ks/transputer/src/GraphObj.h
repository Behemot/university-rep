#pragma once
#include "TransputerDoc.h"

class CTransputerObj
{
	int m_scale;
	int m_cnt;
	CFont m_font,m_boldfont;
	CTransputerDoc* m_doc;
public:
	CTransputerObj(CTransputerDoc* doc);
	~CTransputerObj(void);
	CRect GetRect(int id);
	CSize GetPlotSize();
	void Draw(CDC* dc,int id);
	void Draw(CDC* dc);
};
