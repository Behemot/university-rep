package data;

import java.util.ArrayList;

public class ThreeStatesRules implements ModellingRules{

	@Override
	public int and(ArrayList<Integer> input) {
		int result=Node.ONE;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ZERO){
				result=Node.ZERO;
				break;
			}
			else if (input.get(i)==Node.NONE){
				result=Node.NONE;
				break;
			}
		}
		return result;
	}

	@Override
	public int nand(ArrayList<Integer> input) {
		int result=Node.ONE;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ZERO){
				result=Node.ZERO;
				break;
			}
			else if (input.get(i)==Node.NONE){
				result=Node.NONE;
				break;
			}
		}
		if(result==Node.ONE){
			return Node.ZERO;
		}
		else if (result==Node.ZERO){
			return Node.ONE;
		}
		else{
			return Node.NONE;
		}
	}

	@Override
	public int nor(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ONE){
				result=Node.ONE;
				break;
			}
			else if (input.get(i)==Node.NONE){
				result=Node.NONE;
			}
		}
		if(result==Node.ONE){
			return Node.ZERO;
		}
		else if (result==Node.ZERO){
			return Node.ONE;
		}
		else{
			return Node.NONE;
		}
	}

	@Override
	public int or(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ONE){
				result=Node.ONE;
				break;
			}
			else if (input.get(i)==Node.NONE){
				result=Node.NONE;
			}
		}
		return result;
	}

	@Override
	public boolean transitionAppliable() {
		return true;
	}

	@Override
	public String transitionRules(int before, int after) {
		String t1=Integer.toBinaryString(before);
		String t2=Integer.toBinaryString(after);
		if(t1.length()<t2.length()){
			StringBuffer temp=new StringBuffer();
			for(int i=0;i<t2.length()-t1.length();i++){
				temp.append('0');
			}
			temp.append(t1);
			t1=temp.toString();
		}
		
		char[] t3=t1.toCharArray();
		
		for(int i=0;i<t1.length();i++){
			if((t1.charAt(i)=='1'&&t2.charAt(i)=='0')||(t1.charAt(i)=='0'&&t2.charAt(i)=='1')){
				t3[i]='X';
			}
		}
		return new String(t3);
	}

	@Override
	public int xor(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if (input.get(i)==Node.NONE){
				result=Node.NONE;
				break;
			}
			else if(result==Node.ZERO&&input.get(i)==Node.ONE){
				result=Node.ONE;
			}
			else if(result==Node.ONE&&input.get(i)==Node.ONE){
				result=Node.ZERO;
				break;
			}
		}
		return result;
	}


}
