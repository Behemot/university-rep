package data;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class ReportTableModel extends AbstractTableModel{
	private ArrayList<String> captions_;
	private ArrayList<ArrayList<Integer>> data_;
	
	public ReportTableModel(ArrayList<String> captions,ArrayList<ArrayList<Integer>> data){
		captions_=captions;
		data_=data;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return captions_.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return captions_.get(columnIndex);
	}

	@Override
	public int getRowCount() {
		return data_.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		int temp=data_.get(rowIndex).get(columnIndex);
		switch(temp){
		case(Node.ONE): return "1";
		case(Node.ZERO): return "0";
		case(Node.ONE_TO_ZERO): return "L";
		case(Node.ZERO_TO_ONE): return "H";
		case(Node.NONE): return "X";
		default:return "F";
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
}
