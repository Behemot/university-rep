package data;
import java.util.ArrayList;

public interface ModellingRules {
	public int or(ArrayList<Integer> input);
	public int and(ArrayList<Integer> input);
	public int xor(ArrayList<Integer> input);
	public int nand(ArrayList<Integer> input);
	public int nor(ArrayList<Integer> input);
	public String transitionRules(int before,int after);
	public boolean transitionAppliable();
}
