package data;

import java.util.ArrayList;

public class TwoStatesRules implements ModellingRules{

	@Override
	public int and(ArrayList<Integer> input) {
		int result=Node.ONE;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ZERO){
				result=Node.ZERO;
				break;
			}
		}
		return result;
	}

	@Override
	public int nand(ArrayList<Integer> input) {
		int result=Node.ONE;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ZERO){
				result=Node.ZERO;
				break;
			}
		}
		if(result==Node.ONE){
			return Node.ZERO;
		}
		else{
			return Node.ONE;
		}
	}

	@Override
	public int nor(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ONE){
				result=Node.ONE;
				break;
			}
		}
		if(result==Node.ONE){
			return Node.ZERO;
		}
		else{
			return Node.ONE;
		}
	}

	@Override
	public int or(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if(input.get(i)==Node.ONE){
				result=Node.ONE;
				break;
			}
		}
		return result;
	}

	@Override
	public boolean transitionAppliable() {
		return false;
	}

	@Override
	public String transitionRules(int before, int after) {
		return "";
	}

	@Override
	public int xor(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if(result==Node.ZERO&&input.get(i)==Node.ONE){
				result=Node.ONE;
			}
			else if(result==Node.ONE&&input.get(i)==Node.ONE){
				result=Node.ZERO;
				break;
			}
		}
		return result;
	}

}
