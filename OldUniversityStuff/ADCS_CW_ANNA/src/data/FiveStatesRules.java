package data;

import java.util.ArrayList;

public class FiveStatesRules implements ModellingRules{

	int[][] andTable_={{Node.ZERO,Node.ZERO,Node.ZERO,Node.ZERO,Node.ZERO},
					   {Node.ZERO,Node.ONE,Node.NONE,Node.ZERO_TO_ONE,Node.ONE_TO_ZERO},
					   {Node.ZERO,Node.NONE,Node.NONE,Node.NONE,Node.NONE},
					   {Node.ZERO,Node.ZERO_TO_ONE,Node.NONE,Node.ZERO_TO_ONE,Node.NONE},
					   {Node.ZERO,Node.ONE_TO_ZERO,Node.NONE,Node.NONE,Node.ONE_TO_ZERO}};
	int[][] orTable_={{Node.ZERO,Node.ONE,Node.NONE,Node.ZERO_TO_ONE,Node.ONE_TO_ZERO},
				      {Node.ONE,Node.ONE,Node.ONE,Node.ONE,Node.ONE},
				      {Node.NONE,Node.ONE,Node.NONE,Node.NONE,Node.NONE},
				      {Node.ZERO_TO_ONE,Node.ONE,Node.NONE,Node.ZERO_TO_ONE,Node.NONE},
				      {Node.ONE_TO_ZERO,Node.ONE,Node.NONE,Node.NONE,Node.ONE_TO_ZERO}};
	int[] notTable_={Node.ONE,Node.ZERO,Node.NONE,Node.ZERO_TO_ONE,Node.ONE_TO_ZERO};
	@Override
	public int and(ArrayList<Integer> input) {
		if(input.size()==1){
			return input.get(0);
		}
		else{
			int result= andTable_[input.get(0)][input.get(1)];
			for(int i=2;i<input.size();i++){
				result= andTable_[result][input.get(i)];
			}
			return result;
		}
		
	}

	@Override
	public int nand(ArrayList<Integer> input) {
		if(input.size()==1){
			return notTable_[input.get(0)];
		}
		else{
			int result= andTable_[input.get(0)][input.get(1)];
			for(int i=2;i<input.size();i++){
				result= andTable_[result][input.get(i)];
			}
			return notTable_[result];
		}
	}

	@Override
	public int nor(ArrayList<Integer> input) {
		if(input.size()==1){
			return notTable_[input.get(0)];
		}
		else{
			int result= orTable_[input.get(0)][input.get(1)];
			for(int i=2;i<input.size();i++){
				result= orTable_[result][input.get(i)];
			}
			return notTable_[result];
		}
	}

	@Override
	public int or(ArrayList<Integer> input) {
		if(input.size()==1){
			return input.get(0);
		}
		else{
			int result= orTable_[input.get(0)][input.get(1)];
			for(int i=2;i<input.size();i++){
				result= orTable_[result][input.get(i)];
			}
			return result;
		}

	}

	@Override
	public boolean transitionAppliable() {
		return true;
	}

	@Override
	public String transitionRules(int before, int after) {
		String t1=Integer.toBinaryString(before);
		String t2=Integer.toBinaryString(after);
		if(t1.length()<t2.length()){
			StringBuffer temp=new StringBuffer();
			for(int i=0;i<t2.length()-t1.length();i++){
				temp.append('0');
			}
			temp.append(t1);
			t1=temp.toString();
		}
		
		char[] t3=t1.toCharArray();
		
		for(int i=0;i<t1.length();i++){
			if(t1.charAt(i)=='1'&&t2.charAt(i)=='0'){
				t3[i]='L';
			}
			else if(t1.charAt(i)=='0'&&t2.charAt(i)=='1'){
				t3[i]='H';
			}
		}
		return new String(t3);
	}

	@Override
	public int xor(ArrayList<Integer> input) {
		int result=Node.ZERO;
		for(int i=0;i<input.size();i++){
			if (input.get(i)==Node.NONE){
				result=Node.NONE;
				break;
			}
			else if(result==Node.ZERO&&input.get(i)==Node.ONE){
				result=Node.ONE;
			}
			else if(result==Node.ONE&&input.get(i)==Node.ONE){
				result=Node.ZERO;
				break;
			}
		}
		return result;
	}

}
