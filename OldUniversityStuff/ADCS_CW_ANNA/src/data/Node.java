package data;
import java.util.ArrayList;
import java.io.Serializable;

public class Node implements Serializable{
	public static final int STUB=-1;
	public static final int AND=0;
	public static final int OR=1;
	public static final int XOR=2;
	public static final int NAND=3;
	public static final int NOR=4;
	public static final int ZERO=0;
	public static final int ONE=1;
	public static final int NONE=2;
	public static final int ZERO_TO_ONE=3;
	public static final int ONE_TO_ZERO=4;
	
	private static final long serialVersionUID = -3388768794943281957L;
	private String id_;
	private int type_;
	private int prevState_;
	private int currentState_;
	private ArrayList<Node> in_=new ArrayList<Node>();
	private ArrayList<Node> out_=new ArrayList<Node>(); 
	
	public Node(int type,String id){
		type_=type;
		id_=id;
		prevState_=Node.NONE;
		currentState_=Node.NONE;
	}
	public void addParent(Node c){
		in_.add(c);
	}
	public void addChild(Node c){
		out_.add(c);
	}
	public ArrayList<Node> getParents(){
		return in_;
	}
	public ArrayList<Node> getChildren(){
		return out_;
	}
	/**
	 * @return the prevState_
	 */
	public int getPrevState() {
		return prevState_;
	}
	/**
	 * @param prevState the prevState_ to set
	 */
	public void setPrevState(int prevState) {
		prevState_ = prevState;
	}
	/**
	 * @return the currentState_
	 */
	public int getCurrentState() {
		return currentState_;
	}
	/**
	 * @param currentState the currentState_ to set
	 */
	public void setCurrentState(int currentState) {
		currentState_ = currentState;
	}
	public int getType(){
		return type_;
	}
	
	public String getID(){
		return id_;
	}
	
	public void dumpState(){
		prevState_=currentState_;
	}
}
