package data;

public class UnconnectedException extends Exception{
	public UnconnectedException(String s){
		super(s);
	}
}
