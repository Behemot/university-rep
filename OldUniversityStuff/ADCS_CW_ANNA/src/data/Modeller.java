package data;
import java.util.ArrayList;

public class Modeller {
	private static Modeller generator_;
	private ArrayList<Node> entryStubs_;
	private ArrayList<Node> exitStubs_;
	private ArrayList<Node> blocks_;
	private ModellingRules rules_;
	
	public static Modeller getModeller(){
		if (generator_==null){
			generator_=new Modeller();
		}
		return generator_;
	}
	private Modeller(){
		
	}
	public void setRules(ModellingRules rules){
		rules_=rules;
	}
	public void setNodes(ArrayList<Node> nodes) throws UnconnectedException{
		entryStubs_=new ArrayList<Node>();
		blocks_=new ArrayList<Node>();
		exitStubs_=new ArrayList<Node>();
		
		for(int i=0;i<nodes.size();i++){
			if ((nodes.get(i).getParents().size()==0 &&nodes.get(i).getChildren().size()==0)
					||(nodes.get(i).getParents().size()>1 &&nodes.get(i).getChildren().size()==0)){
				System.out.println(nodes.get(i).getType()+" - " +nodes.get(i).getID());
				throw (new UnconnectedException(nodes.get(i).getID()));
			}
			else if (nodes.get(i).getParents().size()==0){
				entryStubs_.add(nodes.get(i));
			}
			else if (nodes.get(i).getChildren().size()==0){
				exitStubs_.add(nodes.get(i));
			}
			else{
				blocks_.add(nodes.get(i));
			}
			
		}
	}
	
	
	public ArrayList<String> getCaptions(){
		ArrayList<String> captions=new ArrayList<String>();
		for(int i=0;i<entryStubs_.size();i++){
			captions.add(entryStubs_.get(i).getID());
		}
		for(int i=0;i<blocks_.size();i++){
			captions.add(blocks_.get(i).getID());
		}
		for(int i=0;i<exitStubs_.size();i++){
			captions.add(exitStubs_.get(i).getID());
		}
		return captions;
	}
	
	
	public ArrayList<ArrayList<Integer>> startModelling(){
		int current=0;
		ArrayList<ArrayList<Integer>> report=new ArrayList<ArrayList<Integer>>(); 
		while (current<=((int)Math.pow(2,entryStubs_.size()))-1){
			setInput(current);
			iterate();
			ArrayList<Integer> before=dumpStates();
			iterate();
			ArrayList<Integer> after=dumpStates();
			report.add(before);
			report.add(after);
			int break_clause=0;
			while(!checkEnd(before,after)&&break_clause<=blocks_.size()*3){
				iterate();
				before=after;
				after=dumpStates();
				report.add(after);
				break_clause++;
			}
			if(rules_.transitionAppliable()&&(current!=((int)Math.pow(2,entryStubs_.size()))-1)){
				String inp=rules_.transitionRules(current, current+1);
				setTransitionInput(inp);
				iterate();
				ArrayList<Integer> before_trans=dumpStates();
				iterate();
				ArrayList<Integer> after_trans=dumpStates();
				report.add(before_trans);
				report.add(after_trans);
				break_clause=0;
				while(!checkEnd(before_trans,after_trans)&&break_clause<=blocks_.size()*3){
					iterate();
					before_trans=after_trans;
					after_trans=dumpStates();
					report.add(after_trans);
					break_clause++;
				}
			}
			current++;
		}
		return report;
	}
	
	private boolean checkEnd(ArrayList<Integer> before, ArrayList<Integer> after){
		boolean result=true;
		for(int i=0;i<before.size();i++){
			if(before.get(i)!=after.get(i)){
				result=false;
				break;
			}
		}
		return result;
	}
	
	private void setTransitionInput(String bytes){
		for(int i=0;i<entryStubs_.size();i++){
			entryStubs_.get(i).setCurrentState(Node.ZERO);
		}
		if(bytes.length()<entryStubs_.size()){
			StringBuffer temp=new StringBuffer();
			for(int i=0;i<entryStubs_.size()-bytes.length();i++){
				temp.append('0');
			}
			temp.append(bytes);
			bytes=temp.toString();
		}
		for(int i=0;i<entryStubs_.size();i++){
			entryStubs_.get(i).setCurrentState(Node.ZERO);
			entryStubs_.get(i).setPrevState(Node.ZERO);
		}
		for(int i=0;i<bytes.length();i++){
			if(bytes.charAt(i)=='1'){
				entryStubs_.get(i).setCurrentState(Node.ONE);
				entryStubs_.get(i).setPrevState(Node.ONE);
			}
			else if(bytes.charAt(i)=='X'){
				entryStubs_.get(i).setCurrentState(Node.NONE);
				entryStubs_.get(i).setPrevState(Node.NONE);
			}
			else if(bytes.charAt(i)=='L'){
				entryStubs_.get(i).setCurrentState(Node.ONE_TO_ZERO);
				entryStubs_.get(i).setPrevState(Node.ONE_TO_ZERO);
			}
			else if(bytes.charAt(i)=='H'){
				entryStubs_.get(i).setCurrentState(Node.ZERO_TO_ONE);
				entryStubs_.get(i).setPrevState(Node.ZERO_TO_ONE);
			}
			
		}
	}
	
	private void setInput(int inp){
		String bytes= Integer.toBinaryString(inp);
		if(bytes.length()<entryStubs_.size()){
			StringBuffer temp=new StringBuffer();
			for(int i=0;i<entryStubs_.size()-bytes.length();i++){
				temp.append('0');
			}
			temp.append(bytes);
			bytes=temp.toString();
		}
		for(int i=0;i<entryStubs_.size();i++){
			entryStubs_.get(i).setCurrentState(Node.ZERO);
			entryStubs_.get(i).setPrevState(Node.ZERO);
		}
		for(int i=0;i<bytes.length();i++){
			if(bytes.charAt(i)=='1'){
				entryStubs_.get(i).setCurrentState(Node.ONE);
				entryStubs_.get(i).setPrevState(Node.ONE);
			}
		}
	}
	
	private void iterate(){
		for(int i=0;i<blocks_.size();i++){
			ArrayList<Node> parents=blocks_.get(i).getParents();
			ArrayList<Integer> input=new ArrayList<Integer>();
			for(int j=0;j<parents.size();j++){
				input.add(parents.get(j).getPrevState());
			}
			int newState=-999;
			switch(blocks_.get(i).getType()){
				case(Node.AND): newState=rules_.and(input);break;
				case(Node.OR): newState=rules_.or(input);break;
				case(Node.XOR): newState=rules_.xor(input);break;
				case(Node.NAND): newState=rules_.nand(input);break;
				case(Node.NOR): newState=rules_.nor(input);break;

			}
			blocks_.get(i).setCurrentState(newState);
		}
		for(int i=0;i<exitStubs_.size();i++){
			ArrayList<Node> parents=exitStubs_.get(i).getParents();
			exitStubs_.get(i).setCurrentState(parents.get(0).getPrevState());
		}
		
	}
	
	private ArrayList<Integer> dumpStates(){
		ArrayList<Integer> export=new ArrayList<Integer>();
		for(int i=0;i<entryStubs_.size();i++){
			entryStubs_.get(i).setPrevState(entryStubs_.get(i).getCurrentState());
			export.add(entryStubs_.get(i).getCurrentState());
		}
		for(int i=0;i<blocks_.size();i++){
			blocks_.get(i).setPrevState(blocks_.get(i).getCurrentState());
			export.add(blocks_.get(i).getCurrentState());
		}
		for(int i=0;i<exitStubs_.size();i++){
			exitStubs_.get(i).setPrevState(exitStubs_.get(i).getCurrentState());
			export.add(exitStubs_.get(i).getCurrentState());
		}
		return export;
	}
	
	
}
