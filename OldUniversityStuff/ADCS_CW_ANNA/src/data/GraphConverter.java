package data;
import java.util.ArrayList;

import org.jgraph.JGraph;
import org.jgraph.graph.GraphModel;
import org.jgraph.graph.GraphCell;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.Edge;
import org.jgraph.graph.DefaultEdge;

public class GraphConverter {
	public static ArrayList<Node> convert(JGraph graph){
		GraphModel model=graph.getModel();
		ArrayList<Node> result=new ArrayList<Node>();
		ArrayList<GraphCell> cells=new ArrayList<GraphCell>();
		ArrayList<Edge> edges=new ArrayList<Edge>();
		for(int i=0;i<model.getRootCount();i++){
			Object temp=model.getRootAt(i);
			if(temp.getClass()==DefaultGraphCell.class){
				cells.add((GraphCell)temp);
			} else if(temp.getClass()==DefaultEdge.class){
				edges.add((Edge)temp);
			}
		}
		
		for(int i=0;i<cells.size();i++){
			String[] tokens=cells.get(i).toString().split("/");
			String t=tokens[0].toLowerCase().trim();
			if(t.intern()=="and"){
				result.add(new Node(Node.AND,tokens[1]));
			}
			else if (t.intern()=="or"){
				result.add(new Node(Node.OR,tokens[1]));
			}
			else if (t.intern()=="xor"){
				result.add(new Node(Node.XOR,tokens[1]));
			}
			else if (t.intern()=="nor"){
				result.add(new Node(Node.NOR,tokens[1]));
			}
			else if (t.intern()=="nand"){
				result.add(new Node(Node.AND,tokens[1]));
			}
			else {
				result.add(new Node(Node.STUB,tokens[0]));
			}
			
		}
		for(int i=0;i<edges.size();i++){
			GraphCell source=null;
			GraphCell destination=null;
			int ready=0;
			for(int j=0;j<cells.size();j++){
				if(((DefaultGraphCell)cells.get(j)).getChildren().contains(edges.get(i).getSource())){
					source=cells.get(j);
					ready++;
				}
				else if(((DefaultGraphCell)cells.get(j)).getChildren().contains(edges.get(i).getTarget())){
					destination=cells.get(j);
					ready++;
				}
				if(ready==2){
					break;
				}
			}
			if(source!=null&&destination!=null){
				result.get(cells.indexOf(source)).addChild(result.get(cells.indexOf(destination)));
				result.get(cells.indexOf(destination)).addParent(result.get(cells.indexOf(source)));
				//System.out.println("Connected "+result.get(cells.indexOf(source)).getID()+" and "+result.get(cells.indexOf(destination)).getID());
			}
		}
		return result;
	}
}
