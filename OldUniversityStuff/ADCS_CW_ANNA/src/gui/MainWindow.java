package gui;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import data.FiveStatesRules;
import data.GraphConverter;
import data.Modeller;
import data.ReportTableModel;
import data.ThreeStatesRules;
import data.TwoStatesRules;
import data.UnconnectedException;

public class MainWindow extends JFrame{
	private static final long serialVersionUID = 1L;
	private static MainWindow mainWindow_;
	public static MainWindow getMainWindow(){
		if(mainWindow_==null){
			mainWindow_=new MainWindow();
		}
		return mainWindow_;
	}
	private GraphEdX graph_=new GraphEdX();
	private JTable reportTable_=new JTable();
	
	private MainWindow(){
		JMenuBar mainMenuBar=new JMenuBar();
		JMenu fileMenu=new JMenu("File");
		fileMenu.add(new JMenuItem(new AbstractAction("New") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MainWindow.getMainWindow().newGraph();
			}
		}));
		fileMenu.add(new JMenuItem(new AbstractAction("Open") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MainWindow.getMainWindow().getGraph().openFile();
			}
		}));

		fileMenu.add(new JMenuItem(new AbstractAction("Save") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MainWindow.getMainWindow().getGraph().saveFile();
			}
		}));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(new AbstractAction("Exit") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MainWindow.getMainWindow().dispose();
			}
		}));
		
		JMenu executeMenu=new JMenu("Execute");
		executeMenu.add(new JMenuItem(new AbstractAction("Modelling 2-state") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				try{
					Modeller.getModeller().
						setNodes(GraphConverter.convert(MainWindow.
								getMainWindow().getGraph().graph));
					Modeller.getModeller().setRules(new TwoStatesRules());
					reportTable_.setModel(new ReportTableModel(Modeller.getModeller().getCaptions(),
							Modeller.getModeller().startModelling()));
					reportTable_.repaint();
				}
				catch(UnconnectedException E){
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
					ErrorPane.setMessage("Unconnected block: "+E.getMessage());
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}
		}));
		executeMenu.add(new JMenuItem(new AbstractAction("Modelling 3-state") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				try{
					Modeller.getModeller().
						setNodes(GraphConverter.convert(MainWindow.
								getMainWindow().getGraph().graph));
					Modeller.getModeller().setRules(new ThreeStatesRules());
					reportTable_.setModel(new ReportTableModel(Modeller.getModeller().getCaptions(),
							Modeller.getModeller().startModelling()));
					reportTable_.repaint();
				}
				catch(UnconnectedException E){
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
					ErrorPane.setMessage("Unconnected block: "+E.getMessage());
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}
		}));
		executeMenu.add(new JMenuItem(new AbstractAction("Modelling 5-state") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				try{
					Modeller.getModeller().
						setNodes(GraphConverter.convert(MainWindow.
								getMainWindow().getGraph().graph));
					Modeller.getModeller().setRules(new FiveStatesRules());
					reportTable_.setModel(new ReportTableModel(Modeller.getModeller().getCaptions(),
							Modeller.getModeller().startModelling()));
					reportTable_.repaint();
				}
				catch(UnconnectedException E){
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
					ErrorPane.setMessage("Unconnected block: "+E.getMessage());
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}
		}));
		
		mainMenuBar.add(fileMenu);
		mainMenuBar.add(executeMenu);
		this.setJMenuBar(mainMenuBar);
		JTabbedPane tabs=new JTabbedPane();
		tabs.addTab("Circuit field", new JScrollPane(graph_));
		tabs.addTab("Report",new JScrollPane(reportTable_));
		this.getContentPane().add(tabs);
		this.setSize(1024,768);
		this.setTitle("ADCS Course Work");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	public GraphEdX getGraph(){
		return graph_;
	}
	public void newGraph(){
		this.getContentPane().removeAll();
		graph_=new GraphEdX();
		
		this.getContentPane().add(new JScrollPane(graph_));
		this.getContentPane().validate();
		this.repaint();
	}
	
}
