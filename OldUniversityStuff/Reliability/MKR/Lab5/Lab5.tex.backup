\documentclass[a4paper,12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[english,russian,ukrainian]{babel}
\usepackage[T2A]{fontenc}
\usepackage{indentfirst}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{titlesec}
\usepackage{titletoc}
\usepackage{listings}
\usepackage{lscape}
\usepackage{float}
\usepackage{listings}

\textwidth=6.9in
\textheight=10in
\oddsidemargin=0.7in
\voffset=-1in
\hoffset=-1in

\pagestyle{plain}

\newcommand{\titletop}{%
Національний технічний університет України ``Київський політехнічний інститут''\par
Факультет інформатики та обчислювальної техніки\par
Кафедра обчислювальної техніки}
\newcommand{\titlelineI}{Лабораторна робота №5\par
з курсу ``Надійність комп'ютерних системах''}
\newcommand{\titlelineIII}{Виконав:\par
Михайленко А.В.\par
ФІОТ гр. ІО-72\par
\vspace{5mm}
}
\newcommand{\titlebottom}{Київ 2011}

\renewcommand{\maketitle}
{
  \thispagestyle{empty}
  \parbox[c][0.3\vsize][t]{\hsize}
  {\begin{center}\fontsize{13pt}{15pt}\selectfont\titletop\end{center}}

  \parbox[c][0.1\vsize][t]{\hsize}
  {\begin{center}\fontfamily{fma}\fontsize{18pt}{20pt}\selectfont\titlelineI\end{center}}


  \parbox[t][0.3\vsize][t]{\hsize}
  {
    \parbox[t]{0.4\hsize}{\raggedright}\hfill
    \parbox[t]{0.4\hsize}{\raggedright\fontsize{14pt}{15pt}\selectfont\titlelineIII}
  }

  \vfill
  \begin{center}\fontsize{13pt}{14pt}\selectfont\titlebottom\end{center}
}

%opening
\author{Anthony Mykhailenko}


\begin{document}
\maketitle
\pagebreak

\section{Задание}
ДМ построен по схеме RAIDi с заданными $\mu$, $\lambda$. Для дополнительного повышения его надёжности можно увеличить количество ремонтников или количество запасных дисков.
Для ремонта ДМ можно использовать: 

штатного ремонтника, затраты:
\begin{itemize}
  \item $S_{\text{ш}}$ в месяц на одного ремонтника;
  \item $E$ — цена одного диска;
  \item $T_{\text{ш,в}}$ = 1.5 ч;
  \item средняя стоимость запчастей, необходимых для одного ремонта $B_{\text{ш}}$ = 0.5$E$;
  \item длительность ремонта путём замены $T_{\text{ш,з}} = 0$ ч.
\end{itemize}
договор про обслуживание с сервисным центром, затраты
\begin{itemize}
\item $S_{\text{д}}$ в месяц;
\item $T_{\text{д}}$, в = 20 ч;
\item $B_{\text{д}}$ = 0.6$E$;
\item $T_{\text{д}}$, з = 3 ч.
\end{itemize}
ремонт в сервисном центре, затраты
\begin{itemize}
\item $S_{\text{р}}$ = 0.9E за один ремонт,
\item $T_{\text{р,в}}$ = 100 ч,
\item $B_{\text{р}}$ = 0,
\item $T_{\text{р, з}}$ .
\end{itemize}
Определить количество запасных дисков и выбрать форму обслуживания для наиболее дешёвого обеспечения надёжности ДМ на уровне $P_{\gamma}$ .
\paragraph{Вариант:}

RAID1

$\mu$ = 0.01 · (C30 + 1) = $0.12$

$\lambda$ = 10−6 (C20 + 1) = $12 \cdot 10^{-6} \text{ч}^{−1}$

$S_{\text{ш}}$ = E · (0.5 + C10 ) = $1.5 \cdot E$

$S_{\text{д}}$ = 0.1 · 2−C4 · E = $0.01250000 \cdot E$

$T_{\text{р,з}}$ = 6 + C9 = $8$

$P_{\gamma}$ = 1 − 10−(4+C11 ) = $1 − 10^{−6}$

\section{Определение функции надёжности}
Дисковый массив типа RAID1 предполагает зеркалирование данных при помощи дисковых пар (один диск рабочий - один запасной).

Для примера здесь и далее будем предполагать что масив состоит из 3 дисковых пар.

Система отказывает, если отказала дисковая пара, таким образом, ёё можно представить в виде марковской цепи следующего вида.
\begin{figure}[H]
%\includegraphics[width=0.8\linewidth]{graph.jpg}
\caption{Марковская цепь, описывающая состояние системы}
\end{figure}

Вероятность пребывания в каждой вершине графа находиться из уравнений Калмагорова:
\[
\begin{array}{l}
\frac{dW_0}{dt}\\
\frac{dW_1}{dt}\\
\frac{dW_2}{dt}\\
\frac{dW_3}{dt}\\
\frac{dW_4}{dt}\\
W_0(0)=1\\
W_1(0)=0\\
W_2(0)=0\\
W_3(0)=0\\
W_4(0)=0\\
\end{array}
\]

Таким образом, система находиться в рабочем состоянии в вершинах $W_0, W_1, W_2, W_3$, из чего следует, что ёё надежность равна:
\[
P(t)=W_0+W_1+W_2+W_3
\]
Аналитическая форма записи решения очень массивна, по-этому приведём лишь график, показывающий эту зависимость:
\begin{figure}[H]
\caption{График зависимости P(t)}
\end{figure}

\section{Выбор оптимального способа ремонта}

Для того что-бы выбрать оптимальный способ ремонта, необходимо найти функции надежности при всех вариантах поддержки.

Повышение количества резервных дисков в системе RAID1 невозможно из-за конструктивных особенностей.

Исходя из того, что интенсивность восстановления равна $\mu=\frac{1}{T_{\text{В}}+T_{\text{З}}}$ и необходимый термин работы равен 1 году:
\begin{itemize}
  \item Штатный ремонтник:
      \[
       P_{\text{Ш}}(365*24)=1-0.00001513262021008836.
      \]

  \item Контракт с сервисным центром:
      \[
       P_{\text{К}}(365*24)=1-0.00017346338178193447.
      \]
  \item Ремонт в сервисном центре:
      \[
       P_{\text{С}}(365*24)=1-0.00080392684819898204.
      \]
\end{itemize}

Как видно из вычисленных данных, ни один из методов не даёт необходимых гарантий по надежности при данных параметрах.

Но из представленых оптимальным являеться \textbf{штатный ремонтник}, имеющий надежность на порядок выше остальных вариантов.


\end{document}
