gcc version 4.4.3 (Ubuntu 4.4.3-4ubuntu5)
Compiling: /home/anthony/PRO_CW/Local/main.cpp 

     1	#include <mpi.h>
     2	#include <iostream>
     3	#include <stdlib.h>
     4	#include <limits.h>
     5	#include <time.h>
     6	
     7	using std::cout;
     8	using std::endl;
     9	
    10	int N=16;
    11	int P;
    12	int H;
    13	int fill_var=1;
    14	
    15	int a;
    16	int b;
    17	int* MB;
    18	int* MC;
    19	int* MD;
    20	MPI_Status status;
    21	int rank;
    22	
    23	timespec start,end;
    24	double work_time;
    25	
    26	int* init_matrix(){
    27		int *mat=(int*)malloc(sizeof(int)*N*N);
    28		return mat;
    29	}
    30	
    31	void fill_mat(int *MM,int fill_var){
    32		for(int i=0;i<N*N;i++){
    33			MM[i]=fill_var;
    34		}
    35	}
    36	
    37	int count(int *MB, int *MC, int *MD){
    38		int res=INT_MAX;
    39		int buf;
    40		
    41		for(int i=0;i<N;i++){
    42			for(int j=rank*H;j<(rank+1)*H;j++){
    43				buf=MD[i*N+j];
    44				for(int k=0;k<N;k++){
    45					buf+=MB[i*N+k]*MC[k*N+j];
    46				}
    47				if(res>buf){res=buf;};
    48			}
    49		}	
    50		
    51		return res;
    52	}
    53	
    54	int main(int argc, char *argv[]){
    55		MPI_Init(&argc,&argv);
    56		if(argc==2){
    57			N=atoi(argv[1]);
    58		}
    59		MB=init_matrix();
    60		MC=init_matrix();
    61		MD=init_matrix();
    62		
    63		MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    64		MPI_Comm_size(MPI_COMM_WORLD,&P);
    65		H=N/P;
    66		
    67		
    68		if(rank==0){
    69			clock_gettime(CLOCK_MONOTONIC,&start);
    70			fill_mat(MB,fill_var);
    71			fill_mat(MC,fill_var);
    72			fill_mat(MD,fill_var);
    73			MPI_Send(MB,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
    74			MPI_Send(MC,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
    75			MPI_Send(MD,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
    76			if(P>2){
    77				MPI_Send(MB,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
    78				MPI_Send(MC,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
    79				MPI_Send(MD,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
    80			}
    81		
    82		}
    83		else if(rank==P-1){
    84			MPI_Recv(MB,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
    85			MPI_Recv(MC,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
    86			MPI_Recv(MD,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
    87			if (P>4){
    88				MPI_Send(MB,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
    89				MPI_Send(MC,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
    90				MPI_Send(MD,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
    91			}
    92					
    93		}
    94		else if (rank==(P/2+1)){
    95			MPI_Recv(MB,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
    96			MPI_Recv(MC,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
    97			MPI_Recv(MD,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
    98		}
    99		else if(rank==P/2){
   100			MPI_Recv(MB,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   101			MPI_Recv(MC,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   102			MPI_Recv(MD,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   103		}
   104		else if(rank<(P-1)&&rank>(P/2+1)){
   105			MPI_Recv(MB,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
   106			MPI_Recv(MC,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
   107			MPI_Recv(MD,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
   108			MPI_Send(MB,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
   109			MPI_Send(MC,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
   110			MPI_Send(MD,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
   111		}
   112		else if(rank>0&&rank<P/2){
   113			MPI_Recv(MB,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   114			MPI_Recv(MC,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   115			MPI_Recv(MD,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   116			MPI_Send(MB,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
   117			MPI_Send(MC,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
   118			MPI_Send(MD,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
   119		}
   120		
   121		a=count(MB,MC,MD);
   122					
   123		if(rank==0){
   124			if(P>2){
   125				MPI_Recv(&b,1,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
   126				if (b<a){a=b;};
   127			}
   128			MPI_Recv(&b,1,MPI_INT,P-1,rank,MPI_COMM_WORLD,&status);
   129			if (b<a){a=b;};
   130			cout<<"Result: "<< a <<endl;
   131			clock_gettime(CLOCK_MONOTONIC,&end);
   132			work_time=double(end.tv_sec-start.tv_sec)+(double)(end.tv_nsec-start.tv_nsec)/1000000000;
   133			cout<<"Time: "<<work_time<<endl;
   134			
   135		}
   136		else if(rank>0&&rank<P/2){
   137			MPI_Recv(&b,1,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
   138			if (b<a){a=b;};
   139			MPI_Send(&a,1,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
   140		}
   141		else if(rank<(P-1)&&rank>(P/2+1)){
   142			MPI_Recv(&b,1,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   143			if (b<a){a=b;};
   144			MPI_Send(&a,1,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
   145		}
   146		else if(rank==(P-1)){
   147			if(P>4){
   148				MPI_Recv(&b,1,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
   149				if (b<a){a=b;};
   150			}
   151			MPI_Send(&a,1,MPI_INT,0,0,MPI_COMM_WORLD);
   152		}
   153		else if(rank==(P/2+1)){
   154			MPI_Send(&a,1,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
   155		}
   156		else if(rank==(P/2)){
   157			MPI_Send(&a,1,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
   158		}
   159	
   160		MPI_Finalize();
   161		
   162		return 0;
   163	}


