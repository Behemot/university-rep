#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

using std::cout;
using std::endl;

int N=16;
int P;
int H;
int fill_var=1;

int a;
int b;
int* MB;
int* MC;
int* MD;
MPI_Status status;
int rank;
timespec start,end;
double work_time;

int* init_matrix(){
	int *mat=(int*)malloc(sizeof(int)*N*N);
	return mat;
}

void fill_mat(int *MM,int fill_var){
	for(int i=0;i<N*N;i++){
		MM[i]=fill_var;
	}
}

int count(int *MB, int *MC, int *MD){
	int res=INT_MAX;
	int buf;
	
	for(int i=0;i<N;i++){
		for(int j=rank*H;j<(rank+1)*H;j++){
			buf=MD[i*N+j];
			for(int k=0;k<N;k++){
				buf+=MB[i*N+k]*MC[k*N+j];
			}
			if(res>buf){res=buf;};
		}
	}	
	
	return res;
}

int main(int argc, char *argv[]){
	MPI_Init(&argc,&argv);
	if(argc==2){
		N=atoi(argv[1]);
	}
	MB=init_matrix();
	MC=init_matrix();
	MD=init_matrix();
	
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&P);
	H=N/P;
	
	
	if(rank==0){
		clock_gettime(CLOCK_MONOTONIC,&start);
		fill_mat(MB,fill_var);
		fill_mat(MC,fill_var);
		fill_mat(MD,fill_var);
		MPI_Send(MB,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
		MPI_Send(MC,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
		MPI_Send(MD,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
		if(P>2){
			MPI_Send(MB,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
			MPI_Send(MC,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
			MPI_Send(MD,N*N,MPI_INT,P-1,0,MPI_COMM_WORLD);
		}
	
	}
	else if(rank==P-1){
		MPI_Recv(MB,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
		MPI_Recv(MC,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
		MPI_Recv(MD,N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
		if (P>4){
			MPI_Send(MB,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
			MPI_Send(MC,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
			MPI_Send(MD,N*N,MPI_INT,P-2,rank,MPI_COMM_WORLD);
		}
				
	}
	else if (rank==(P/2+1)){
		MPI_Recv(MB,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		MPI_Recv(MC,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		MPI_Recv(MD,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
	}
	else if(rank==P/2){
		MPI_Recv(MB,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		MPI_Recv(MC,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		MPI_Recv(MD,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
	}
	else if(rank<(P-1)&&rank>(P/2+1)){
		MPI_Recv(MB,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		MPI_Recv(MC,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		MPI_Recv(MD,N*N,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		MPI_Send(MB,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
		MPI_Send(MC,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
		MPI_Send(MD,N*N,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
	}
	else if(rank>0&&rank<P/2){
		MPI_Recv(MB,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		MPI_Recv(MC,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		MPI_Recv(MD,N*N,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		MPI_Send(MB,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
		MPI_Send(MC,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
		MPI_Send(MD,N*N,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
	}
	
	a=count(MB,MC,MD);
				
	if(rank==0){
		if(P>2){
			MPI_Recv(&b,1,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
			if (b<a){a=b;};
		}
		MPI_Recv(&b,1,MPI_INT,P-1,rank,MPI_COMM_WORLD,&status);
		if (b<a){a=b;};
		cout<<"Result: "<< a <<endl;
		clock_gettime(CLOCK_MONOTONIC,&end);
		work_time=double(end.tv_sec-start.tv_sec)+(double)(end.tv_nsec-start.tv_nsec)/1000000000;
		cout<<"Time: "<<work_time<<endl;
		
	}
	else if(rank>0&&rank<P/2){
		MPI_Recv(&b,1,MPI_INT,rank+1,rank+1,MPI_COMM_WORLD,&status);
		if (b<a){a=b;};
		MPI_Send(&a,1,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
	}
	else if(rank<(P-1)&&rank>(P/2+1)){
		MPI_Recv(&b,1,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
		if (b<a){a=b;};
		MPI_Send(&a,1,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
	}
	else if(rank==(P-1)){
		if(P>4){
			MPI_Recv(&b,1,MPI_INT,rank-1,rank-1,MPI_COMM_WORLD,&status);
			if (b<a){a=b;};
		}
		MPI_Send(&a,1,MPI_INT,0,0,MPI_COMM_WORLD);
	}
	else if(rank==(P/2+1)){
		MPI_Send(&a,1,MPI_INT,rank+1,rank,MPI_COMM_WORLD);
	}
	else if(rank==(P/2)){
		MPI_Send(&a,1,MPI_INT,rank-1,rank,MPI_COMM_WORLD);
	}

	MPI_Finalize();
	
	return 0;
}
