#include "classes.h"
#include <mpi.h>
#include <limits.h>
#include <iostream>
using namespace std;
const int N=Vector::N;
const int P=8;
const int H=N/P;


Matrix* MB = new Matrix();
Matrix* MC = new Matrix();
Matrix* MD = new Matrix();
Matrix* ME = new Matrix();

int e = INT_MIN;


int startLine;
int endLine;
int sum;

void assemblePart(Matrix* source, Matrix* source2,int* dest, int* list, int count )
{
  for(int k=0; k<count; k++)
  {
    int startLine = list[k]*H;
    int endLine = (list[k]+1)*H;
    if(list[k]==15)
    {
      endLine = N;
    }
    for(int i = 0; i < N; i++)
    {
      for(int j = 0; j < endLine - startLine; j++)
      {
        dest[H*N*k + i*H + j] = source->get(i,j+startLine);
      }
    }
    for(int i = 0; i < N; i++)
    {
      for(int j = 0; j < endLine - startLine; j++)
      {
        dest[count*H*N + H*N*k + i*H + j] = source2->get(i,j+startLine);
      }
    }
  }
}


void disassemblePart(Matrix* dest, Matrix* dest2, int* source, int* list, int count)
{
  for(int k=0; k<count; k++)
  {
    int startLine = list[k]*H;
    int endLine = (list[k]+1)*H;
    if(list[k]==15)
    {
      endLine = N;
    }
    for(int i = 0; i < N; i++)
    {
      for(int j = 0; j < H; j++)
      {
        dest->set(i,j + startLine, source[H*N*k + i*H + j]);
      }
    }
    for(int i = 0; i < N; i++)
    {
      for(int j = 0; j < endLine - startLine; j++)
      {
        dest2->set(i,j + startLine,source[count*H*N + H*N*k + i*H + j]);
      }
    }
  }
}

void fromBufToMat(int* buf, Matrix* MB_, Matrix* ME_)
{
  for(int i = 0; i < N; i++)
  {
    for(int j = 0; j < N; j++)
    {
      MB_->set(i,j, buf[i*N+j]);
    }
  }
  for(int i = 0; i < N; i++)
  {
    for(int j = 0; j < N; j++)
    {
      ME_->set(i,j, buf[N*N+i*N+j]);
    }
  }
  
}

void calc()
{
  sum=0;
  for(int i=0; i<N; i++)
  {
    for(int j=startLine; j<endLine; j++)
    {
      for(int k=0; k<N; k++)
      {
        sum = sum+MB->get(i, k)*MC->get(k, j)+ME->get(i,k)*MD->get(k,j);
      }
      if(sum > e)
      {
        e = sum;
      }
      sum = 0;
    }
  }
}

void getmax(int from)
{
  MPI_Status status;
  int buf;
  MPI_Recv(&buf, 1, MPI_INT,from,0,MPI_COMM_WORLD, &status);
  if(buf > e)
  {
    e = buf;
  }
}
//////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
  MPI_Status status;
  int tid;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &tid);
  startLine = tid*H;
  endLine = (tid+1)*H;
  printf("%i started\n",tid);
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 0 )
  {
    MB->matrixInput();
    ME->matrixInput();
    int* temp = new int[2*N*N];
    for(int i = 0; i < N; i++)
    {
      for(int j = 0; j < N; j++)
      {
        temp[i*N+j] = MB->get(i,j);
      }
      
    }
    for(int i =0;i<N;i++){
        for(int j=0;j<N;j++){
            temp[N*N+i*N+j]= ME->get(i,j);
        }
    }
    //розсилка
    MPI_Send(temp,2*N*N, MPI_INT,1,0,MPI_COMM_WORLD);//1s
    MPI_Send(temp,2*N*N, MPI_INT,4,0,MPI_COMM_WORLD);//2s
    MPI_Send(temp,2*N*N, MPI_INT,2,0,MPI_COMM_WORLD);//3s

    //3r
    delete(temp);
    temp = new int[2*H*N];
    MPI_Recv(temp,2*H*N,MPI_INT,2,0,MPI_COMM_WORLD,&status);
    int* list = new int [1];

    list[0]= tid;
    disassemblePart( MD, MC, temp, list, 1 );
    
    //Виконуєм max(MB * MC_h + ME * MO)
    calc();
    getmax(1);
    getmax(4);
    getmax(2);
    cout<<"e="<<e<<endl;
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 1 )
  {
    //розсилка
    int* temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);//1r
    fromBufToMat(temp,MB,ME);
    MPI_Send(temp,2*N*N, MPI_INT,5,0,MPI_COMM_WORLD);//3s
    MPI_Send(temp,2*N*N, MPI_INT,3,0,MPI_COMM_WORLD);//4s

    //4r
    delete(temp);
    temp = new int[2*H*N];
    MPI_Recv(temp,2*H*N,MPI_INT,3,0,MPI_COMM_WORLD,&status);
    int* list = new int [1];
    list[0]= tid;
    disassemblePart( MD, MC, temp, list, 1 );
    
    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 2 )
  {
    //розсилка

    int* temp = new int[4*H*N+1];
    MPI_Recv(temp,4*H*N,MPI_INT,6,0,MPI_COMM_WORLD,&status);
    int* list = new int [2];
    list[0]= 2;
    list[1]= 0;

    disassemblePart( MD, MC, temp, list, 2 );


    //3r--
    delete(temp);
    temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
    fromBufToMat(temp,MB,ME);;

    //3s--
    delete(temp);
    delete(list);
    temp = new int[2*H*N];
    list = new int [1];
    list[0]= 0;
    assemblePart( MD, MC, temp, list, 1 );
    
    MPI_Send(temp,2*H*N, MPI_INT,0,0,MPI_COMM_WORLD);

    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    getmax(3);
    getmax(6);
    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 3 )
  {
    //розсилка
    //2r
    int* temp = new int[4*H*N];
    MPI_Recv(temp,4*H*N,MPI_INT,7,0,MPI_COMM_WORLD,&status);
    int* list = new int [2];
    list[0]= 3;
    list[1]= 1;

    disassemblePart( MD, MC, temp, list, 2 );
    

    //3r--
    delete(temp);
    temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,1,0,MPI_COMM_WORLD,&status);
    fromBufToMat(temp,MB,ME);
    
    //3s--
    delete(temp);
    delete(list);
    temp = new int[2*H*N];
    list = new int [1];
    list[0]= 1;
    assemblePart( MD, MC, temp, list, 1 );
    
    MPI_Send(temp,2*H*N, MPI_INT,1,0,MPI_COMM_WORLD);

    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    MPI_Send(&e, 1, MPI_INT,2,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 4 )
  {
    //розсилка
    int* temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);//3r
    fromBufToMat(temp,MB,ME);
    MPI_Send(temp,2*N*N, MPI_INT,6,0,MPI_COMM_WORLD);//4s

    //4r --
    delete(temp);
    temp = new int[2*H*N];
    MPI_Recv(temp,2*H*N,MPI_INT,6,0,MPI_COMM_WORLD,&status);
    int* list = new int [1];
    list[0]= tid;
    disassemblePart( MD, MC, temp, list, 1 );
    
    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    getmax(5);
    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 5 )
  {
    //розсилка
    int* temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,1,0,MPI_COMM_WORLD,&status);//3r
    fromBufToMat(temp,MB,ME);
    MPI_Send(temp,2*N*N, MPI_INT,7,0,MPI_COMM_WORLD);//4s

    //4r --
    delete(temp);
    temp = new int[2*H*N];
    MPI_Recv(temp,2*H*N,MPI_INT,7,0,MPI_COMM_WORLD,&status);
    int* list = new int [1];
    list[0]= tid;
    disassemblePart( MD, MC, temp, list, 1 );
    
    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    MPI_Send(&e, 1, MPI_INT,4,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 6 )
  {
    //розсилка
    //1r--
    int* temp = new int[8*H*N];
    MPI_Recv(temp,8*H*N,MPI_INT,7,0,MPI_COMM_WORLD,&status);
    int* list = new int [4];
    list[0]= 6;
    list[1]= 2;
    list[2]= 4;
    list[3]= 0;
    disassemblePart( MD, MC, temp, list, 4 );
 
    //2s
    delete(temp);
    delete(list);
    temp = new int[4*H*N];
    list = new int [2];
    list[0]= 2;
    list[1]= 0;
    assemblePart( MD, MC, temp, list, 2 );
    
    MPI_Send(temp,4*H*N,MPI_INT,2,0,MPI_COMM_WORLD);

    //3s--
    delete(temp);
    temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,4,0,MPI_COMM_WORLD,&status);
    fromBufToMat(temp,MB,ME);
    //3r--
    delete(temp);
    delete(list);
    temp = new int[2*H*N];
    list = new int [1];
    list[0]= 4;
    assemblePart( MD, MC, temp, list, 1 );
    MPI_Send(temp,2*H*N,MPI_INT,4,0,MPI_COMM_WORLD);

    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    getmax(7);
    MPI_Send(&e, 1, MPI_INT,2,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  if(tid == 7 )
  {
    endLine = N;

    MD->matrixInput();
    MC->matrixInput();
    ME->matrixInput();
    //розсилка
    //1s
    int* temp = new int[8*H*N];
    int* list = new int[4];
    list[0]= 6;
    list[1]= 2;
    list[2]= 4;
    list[3]= 0;
    assemblePart(MD, MC, temp, list, 4);
    MPI_Send(temp, 8*H*N, MPI_INT,6,0,MPI_COMM_WORLD);

    //2s
    delete(temp);
    delete(list);
    temp = new int[4*H*N];
    list = new int[2];
    list[0]= 3;
    list[1]= 1;
    assemblePart(MD, MC, temp, list, 2);
    MPI_Send(temp, 4*H*N, MPI_INT,3,0,MPI_COMM_WORLD);

    //3r
    delete(temp);
    temp = new int[2*N*N];
    MPI_Recv(temp, 2*N*N,MPI_INT,5,0,MPI_COMM_WORLD,&status);
    fromBufToMat(temp,MB,ME);
    //4r--
    delete(temp);
    delete(list);
    temp = new int[2*H*N];
    list = new int [1];
    list[0]= 5;
    assemblePart( MD, MC, temp, list, 1 );
    MPI_Send(temp,2*H*N,MPI_INT,5,0,MPI_COMM_WORLD);

    //Виконуєм max(MB * MC_h + beta * MO)
    calc();
    MPI_Send(&e, 1, MPI_INT,6,0,MPI_COMM_WORLD);
  }
////////////////////////////////////////////////////////////////////////////////////
  MPI_Finalize();
  return 0;
}
