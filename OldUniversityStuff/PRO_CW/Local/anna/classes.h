//------------------------------------------------------
// Лабораторна робота #2
//------------------------------------------------------
const bool AUTO = true;
const bool QUIET = false;

class Vector
{
public:
	static const int N=24;

	Vector();
	~Vector();
	void vectorInput();
	void vectorInput(int n);
	void vectorOutput();
	int get(int number) const;
	void set(int number, int value);
	int* data;
	
};

class Matrix
{
public:
	Matrix();
	~Matrix();
	void matrixInput();
	void matrixInput(int n);
	void matrixOutput();
	void set(int index, int number, int value);
	int get(int index, int number) const;
private:
	Vector** data;
	
};

