     1	#include "classes.h"
     2	#include <mpich2/mpi.h>
     3	#include <limits.h>
     4	#include <iostream>
     5	using namespace std;
     6	const int N=Vector::N;
     7	const int P=8;
     8	const int H=N/P;
     9	
    10	
    11	Matrix* MB = new Matrix();
    12	Matrix* MC = new Matrix();
    13	Matrix* MD = new Matrix();
    14	
    15	int e = INT_MIN;
    16	int beta;
    17	
    18	
    19	int startLine;
    20	int endLine;
    21	int sum;
    22	
    23	void assemblePart(Matrix* source, Matrix* source2, int* dest, int* list, int count )
    24	{
    25	  for(int k=0; k<count; k++)
    26	  {
    27	    int startLine = list[k]*H;
    28	    int endLine = (list[k]+1)*H;
    29	    if(list[k]==15)
    30	    {
    31	      endLine = N;
    32	    }
    33	    for(int i = 0; i < N; i++)
    34	    {
    35	      for(int j = 0; j < endLine - startLine; j++)
    36	      {
    37	        dest[H*N*k + i*H + j] = source->get(i,j+startLine);
    38	      }
    39	    }
    40	    for(int i = 0; i < N; i++)
    41	    {
    42	      for(int j = 0; j < endLine - startLine; j++)
    43	      {
    44	        dest[count*H*N + H*N*k + i*H + j] = source2->get(i,j+startLine);
    45	      }
    46	    }
    47	  }
    48	}
    49	
    50	
    51	void disassemblePart(Matrix* dest, Matrix* dest2, int* source, int* list, int count)
    52	{
    53	  for(int k=0; k<count; k++)
    54	  {
    55	    int startLine = list[k]*H;
    56	    int endLine = (list[k]+1)*H;
    57	    if(list[k]==15)
    58	    {
    59	      endLine = N;
    60	    }
    61	    for(int i = 0; i < N; i++)
    62	    {
    63	      for(int j = 0; j < H; j++)
    64	      {
    65	        dest->set(i,j + startLine, source[H*N*k + i*H + j]);
    66	      }
    67	    }
    68	    for(int i = 0; i < N; i++)
    69	    {
    70	      for(int j = 0; j < endLine - startLine; j++)
    71	      {
    72	        dest2->set(i,j + startLine,source[count*H*N + H*N*k + i*H + j]);
    73	      }
    74	    }
    75	  }
    76	}
    77	
    78	void fromBufToMB(int* buf, Matrix* MB_)
    79	{
    80	  for(int i = 0; i < N; i++)
    81	  {
    82	    for(int j = 0; j < N; j++)
    83	    {
    84	      MB_->set(i,j, buf[i*N+j]);
    85	    }
    86	  }
    87	}
    88	
    89	void calc()
    90	{
    91	  sum=0;
    92	  for(int i=0; i<N; i++)
    93	  {
    94	    for(int j=startLine; j<endLine; j++)
    95	    {
    96	      for(int k=0; k<N; k++)
    97	      {
    98	        sum = sum+MB->get(i, k)*MC->get(k, j);
    99	      }
   100	      sum = sum + beta*MD->get(i, j);
   101	      if(sum > e)
   102	      {
   103	        e = sum;
   104	      }
   105	      sum = 0;
   106	    }
   107	  }
   108	}
   109	
   110	void getmax(int from)
   111	{
   112	  MPI_Status status;
   113	  int buf;
   114	  MPI_Recv(&buf, 1, MPI_INT,from,0,MPI_COMM_WORLD, &status);
   115	  if(buf > e)
   116	  {
   117	    e = buf;
   118	  }
   119	}
   120	//////////////////////////////////////////////////////
   121	int main(int argc, char* argv[])
   122	{
   123	  MPI_Status status;
   124	  int tid;
   125	  MPI_Init(&argc,&argv);
   126	  MPI_Comm_rank(MPI_COMM_WORLD, &tid);
   127	  startLine = tid*H;
   128	  endLine = (tid+1)*H;
   129	  printf("%i started\n",tid);
   130	////////////////////////////////////////////////////////////////////////////////////
   131	  if(tid == 0 )
   132	  {
   133	    MB->matrixInput();
   134	    int* temp = new int[N*N];
   135	    for(int i = 0; i < N; i++)
   136	    {
   137	      for(int j = 0; j < N; j++)
   138	      {
   139	        temp[i*N+j] = MB->get(i,j);
   140	      }
   141	    }
   142	    //розсилка
   143	    MPI_Send(temp,N*N, MPI_INT,1,0,MPI_COMM_WORLD);//1s
   144	    MPI_Send(temp,N*N, MPI_INT,4,0,MPI_COMM_WORLD);//2s
   145	    MPI_Send(temp,N*N, MPI_INT,2,0,MPI_COMM_WORLD);//3s
   146	
   147	    //3r
   148	    delete(temp);
   149	    temp = new int[2*H*N+1];
   150	    MPI_Recv(temp,2*H*N+1,MPI_INT,2,0,MPI_COMM_WORLD,&status);
   151	    int* list = new int [1];
   152	
   153	    list[0]= tid;
   154	    disassemblePart( MD, MC, temp, list, 1 );
   155	    beta = temp[2*H*N];
   156	
   157	    //Виконуєм max(MB * MC_h + beta * MO)
   158	    calc();
   159	    getmax(1);
   160	    getmax(4);
   161	    getmax(2);
   162	    cout<<"e="<<e<<endl;
   163	  }
   164	////////////////////////////////////////////////////////////////////////////////////
   165	  if(tid == 1 )
   166	  {
   167	    //розсилка
   168	    int* temp = new int[N*N];
   169	    MPI_Recv(temp, N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);//1r
   170	    fromBufToMB(temp,MB);
   171	    MPI_Send(temp,N*N, MPI_INT,5,0,MPI_COMM_WORLD);//3s
   172	    MPI_Send(temp,N*N, MPI_INT,3,0,MPI_COMM_WORLD);//4s
   173	
   174	    //4r
   175	    delete(temp);
   176	    temp = new int[2*H*N+1];
   177	    MPI_Recv(temp,2*H*N+1,MPI_INT,3,0,MPI_COMM_WORLD,&status);
   178	    int* list = new int [1];
   179	    list[0]= tid;
   180	    disassemblePart( MD, MC, temp, list, 1 );
   181	    beta = temp[2*H*N];
   182	
   183	    //Виконуєм max(MB * MC_h + beta * MO)
   184	    calc();
   185	    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
   186	  }
   187	////////////////////////////////////////////////////////////////////////////////////
   188	  if(tid == 2 )
   189	  {
   190	    //розсилка
   191	
   192	    int* temp = new int[4*H*N+1];
   193	    MPI_Recv(temp,4*H*N+1,MPI_INT,6,0,MPI_COMM_WORLD,&status);
   194	    int* list = new int [2];
   195	    list[0]= 2;
   196	    list[1]= 0;
   197	
   198	    disassemblePart( MD, MC, temp, list, 2 );
   199	    beta = temp[4*H*N];
   200	
   201	
   202	    //3r--
   203	    delete(temp);
   204	    temp = new int[N*N];
   205	    MPI_Recv(temp, N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);
   206	    fromBufToMB(temp,MB);
   207	
   208	    //3s--
   209	    delete(temp);
   210	    delete(list);
   211	    temp = new int[2*H*N+1];
   212	    list = new int [1];
   213	    list[0]= 0;
   214	    assemblePart( MD, MC, temp, list, 1 );
   215	    temp[2*H*N] = beta;
   216	
   217	    MPI_Send(temp,2*H*N+1, MPI_INT,0,0,MPI_COMM_WORLD);
   218	
   219	    //Виконуєм max(MB * MC_h + beta * MO)
   220	    calc();
   221	    getmax(3);
   222	    getmax(6);
   223	    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
   224	  }
   225	////////////////////////////////////////////////////////////////////////////////////
   226	  if(tid == 3 )
   227	  {
   228	    //розсилка
   229	    //2r
   230	    int* temp = new int[4*H*N+1];
   231	    MPI_Recv(temp,4*H*N+1,MPI_INT,7,0,MPI_COMM_WORLD,&status);
   232	    int* list = new int [2];
   233	    list[0]= 3;
   234	    list[1]= 1;
   235	
   236	    disassemblePart( MD, MC, temp, list, 2 );
   237	    beta = temp[4*H*N];
   238	
   239	
   240	    //3r--
   241	    delete(temp);
   242	    temp = new int[N*N];
   243	    MPI_Recv(temp, N*N,MPI_INT,1,0,MPI_COMM_WORLD,&status);
   244	    fromBufToMB(temp,MB);
   245	
   246	    //3s--
   247	    delete(temp);
   248	    delete(list);
   249	    temp = new int[2*H*N+1];
   250	    list = new int [1];
   251	    list[0]= 1;
   252	    assemblePart( MD, MC, temp, list, 1 );
   253	    temp[2*H*N] = beta;
   254	    MPI_Send(temp,2*H*N+1, MPI_INT,1,0,MPI_COMM_WORLD);
   255	
   256	    //Виконуєм max(MB * MC_h + beta * MO)
   257	    calc();
   258	    MPI_Send(&e, 1, MPI_INT,2,0,MPI_COMM_WORLD);
   259	  }
   260	////////////////////////////////////////////////////////////////////////////////////
   261	  if(tid == 4 )
   262	  {
   263	    //розсилка
   264	    int* temp = new int[N*N];
   265	    MPI_Recv(temp, N*N,MPI_INT,0,0,MPI_COMM_WORLD,&status);//3r
   266	    fromBufToMB(temp,MB);
   267	    MPI_Send(temp,N*N, MPI_INT,6,0,MPI_COMM_WORLD);//4s
   268	
   269	    //4r --
   270	    delete(temp);
   271	    temp = new int[2*H*N+1];
   272	    MPI_Recv(temp,2*H*N+1,MPI_INT,6,0,MPI_COMM_WORLD,&status);
   273	    int* list = new int [1];
   274	    list[0]= tid;
   275	    disassemblePart( MD, MC, temp, list, 1 );
   276	    beta = temp[2*H*N];
   277	
   278	    //Виконуєм max(MB * MC_h + beta * MO)
   279	    calc();
   280	    getmax(5);
   281	    MPI_Send(&e, 1, MPI_INT,0,0,MPI_COMM_WORLD);
   282	  }
   283	////////////////////////////////////////////////////////////////////////////////////
   284	  if(tid == 5 )
   285	  {
   286	    //розсилка
   287	    int* temp = new int[N*N];
   288	    MPI_Recv(temp, N*N,MPI_INT,1,0,MPI_COMM_WORLD,&status);//3r
   289	    fromBufToMB(temp,MB);
   290	    MPI_Send(temp,N*N, MPI_INT,7,0,MPI_COMM_WORLD);//4s
   291	
   292	    //4r --
   293	    delete(temp);
   294	    temp = new int[2*H*N+1];
   295	    MPI_Recv(temp,2*H*N+1,MPI_INT,7,0,MPI_COMM_WORLD,&status);
   296	    int* list = new int [1];
   297	    list[0]= tid;
   298	    disassemblePart( MD, MC, temp, list, 1 );
   299	    beta = temp[2*H*N];
   300	
   301	    //Виконуєм max(MB * MC_h + beta * MO)
   302	    calc();
   303	    MPI_Send(&e, 1, MPI_INT,4,0,MPI_COMM_WORLD);
   304	  }
   305	////////////////////////////////////////////////////////////////////////////////////
   306	  if(tid == 6 )
   307	  {
   308	    //розсилка
   309	    //1r--
   310	    int* temp = new int[8*H*N+1];
   311	    MPI_Recv(temp,8*H*N+1,MPI_INT,7,0,MPI_COMM_WORLD,&status);
   312	    int* list = new int [4];
   313	    list[0]= 6;
   314	    list[1]= 2;
   315	    list[2]= 4;
   316	    list[3]= 0;
   317	    disassemblePart( MD, MC, temp, list, 4 );
   318	    beta = temp[8*H*N];
   319	
   320	    //2s
   321	    delete(temp);
   322	    delete(list);
   323	    temp = new int[4*H*N+1];
   324	    list = new int [2];
   325	    list[0]= 2;
   326	    list[1]= 0;
   327	    assemblePart( MD, MC, temp, list, 2 );
   328	    temp[4*H*N] = beta;
   329	    MPI_Send(temp,4*H*N+1,MPI_INT,2,0,MPI_COMM_WORLD);
   330	
   331	    //3s--
   332	    delete(temp);
   333	    temp = new int[N*N];
   334	    MPI_Recv(temp, N*N,MPI_INT,4,0,MPI_COMM_WORLD,&status);
   335	    fromBufToMB(temp,MB);
   336	
   337	    //3r--
   338	    delete(temp);
   339	    delete(list);
   340	    temp = new int[2*H*N+1];
   341	    list = new int [1];
   342	    list[0]= 4;
   343	    assemblePart( MD, MC, temp, list, 1 );
   344	    temp[2*H*N] = beta;
   345	    MPI_Send(temp,2*H*N+1,MPI_INT,4,0,MPI_COMM_WORLD);
   346	
   347	    //Виконуєм max(MB * MC_h + beta * MO)
   348	    calc();
   349	    getmax(7);
   350	    MPI_Send(&e, 1, MPI_INT,2,0,MPI_COMM_WORLD);
   351	  }
   352	////////////////////////////////////////////////////////////////////////////////////
   353	  if(tid == 7 )
   354	  {
   355	    endLine = N;
   356	
   357	    MD->matrixInput();
   358	    MC->matrixInput();
   359	    beta = 1;
   360	    //розсилка
   361	    //1s
   362	    int* temp = new int[8*H*N+1];
   363	    int* list = new int[4];
   364	    list[0]= 6;
   365	    list[1]= 2;
   366	    list[2]= 4;
   367	    list[3]= 0;
   368	    assemblePart(MD, MC, temp, list, 4);
   369	    temp[8*H*N] = beta;
   370	    MPI_Send(temp, 8*H*N, MPI_INT,6,0,MPI_COMM_WORLD);
   371	
   372	    //2s
   373	    delete(temp);
   374	    delete(list);
   375	    temp = new int[4*H*N+1];
   376	    list = new int[2];
   377	    list[0]= 3;
   378	    list[1]= 1;
   379	    assemblePart(MD, MC, temp, list, 2);
   380	    temp[4*H*N] = beta;
   381	    MPI_Send(temp, 4*H*N, MPI_INT,3,0,MPI_COMM_WORLD);
   382	
   383	    //3r
   384	    delete(temp);
   385	    temp = new int[N*N];
   386	    MPI_Recv(temp, N*N,MPI_INT,5,0,MPI_COMM_WORLD,&status);
   387	    fromBufToMB(temp,MB);
   388	
   389	    //4r--
   390	    delete(temp);
   391	    delete(list);
   392	    temp = new int[2*H*N+1];
   393	    list = new int [1];
   394	    list[0]= 5;
   395	    assemblePart( MD, MC, temp, list, 1 );
   396	    temp[2*H*N] = beta;
   397	    MPI_Send(temp,2*H*N+1,MPI_INT,5,0,MPI_COMM_WORLD);
   398	
   399	    //Виконуєм max(MB * MC_h + beta * MO)
   400	    calc();
   401	    MPI_Send(&e, 1, MPI_INT,6,0,MPI_COMM_WORLD);
   402	  }
   403	////////////////////////////////////////////////////////////////////////////////////
   404	  MPI_Finalize();
   405	  return 0;
   406	}
     1	//------------------------------------------------------
     2	// Лабораторна робота #2
     3	//------------------------------------------------------
     4	const bool AUTO = true;
     5	const bool QUIET = false;
     6	
     7	class Vector
     8	{
     9	public:
    10		static const int N=1000;
    11	
    12		Vector();
    13		~Vector();
    14		void vectorInput();
    15		void vectorInput(int n);
    16		void vectorOutput();
    17		int get(int number) const;
    18		void set(int number, int value);
    19		int* data;
    20		
    21	};
    22	
    23	class Matrix
    24	{
    25	public:
    26		Matrix();
    27		~Matrix();
    28		void matrixInput();
    29		void matrixInput(int n);
    30		void matrixOutput();
    31		void set(int index, int number, int value);
    32		int get(int index, int number) const;
    33	private:
    34		Vector** data;
    35		
    36	};
    37	
     1	//------------------------------------------------------
     2	// Лабораторна робота #6 F1: A = B * MIN(C)
     3	//                       F2: MR = SORT (MA * MB)
     4	//                       F3: D = SORT (A - M)*TRANS (MC)
     5	// Шевчук Олександр Вікторович
     6	// Група ІО-72
     7	// 30.11.2009
     8	// Заголовочний файл з реалізаціями класів Vector і Matrix
     9	//------------------------------------------------------
    10	#include "classes.h"
    11	#include <iostream>
    12	using namespace std;
    13	
    14	Vector::Vector()
    15	{
    16	  data = new int[Vector::N];
    17	}
    18	
    19	
    20	void Vector::vectorInput()
    21	{
    22	  for(int i=0; i<Vector::N;i++)
    23	  {
    24	    if(AUTO)
    25	    {
    26	      data[i] = 1;
    27	    }else
    28	    {
    29	      for(;;)
    30	      {
    31	        try
    32	        {
    33	          cout<<"Enter the cipher\n";
    34	          cin>>data[i];
    35	          break;
    36	        }catch(int)
    37	        {
    38	          cout<<"You have entered a symbol instead of number. Reinput please";
    39	        }
    40	      }
    41	    }
    42	  }
    43	}
    44	
    45	void Vector::vectorInput(int n)
    46	{
    47	  for(int i=0; i<Vector::N;i++)
    48	  {
    49	    if(AUTO)
    50	    {
    51	      data[i] = n;
    52	    }else
    53	    {
    54	      for(;;)
    55	      {
    56	        try
    57	        {
    58	          cout<<"Enter the cipher\n";
    59	          cin>>data[i];
    60	          break;
    61	        }catch(int)
    62	        {
    63	          cout<<"You have entered a symbol instead of number. Reinput please";
    64	        }
    65	      }
    66	    }
    67	  }
    68	}
    69	
    70	void Vector::vectorOutput()
    71	{
    72	        if(!QUIET)
    73	        {
    74	                for(int i=0; i<Vector::N;i++)
    75	                {
    76	                        cout<<data[i]<<" ";
    77	                }
    78	                cout<<endl;
    79	        }
    80	}
    81	
    82	int Vector::get(int number) const
    83	{
    84	        return data[number];
    85	}
    86	
    87	void Vector::set(int number, int value)
    88	{
    89	        data[number] = value;
    90	}
    91	
    92	
    93	//___________________________________-
    94	
    95	Matrix::Matrix()
    96	{
    97	        data = new Vector*[Vector::N];
    98	        for(int i=0;i<Vector::N;i++)
    99	        {
   100	                data[i] = new Vector();
   101	        }
   102	}
   103	
   104	void Matrix::matrixInput()
   105	{
   106	        for(int i=0; i<Vector::N; i++)
   107	        {
   108	                data[i]->vectorInput();
   109	        }
   110	}
   111	
   112	void Matrix::matrixInput(int n)
   113	{
   114	        for(int i=0; i<Vector::N; i++)
   115	        {
   116	                data[i]->vectorInput(n);
   117	        }
   118	}
   119	
   120	void Matrix::matrixOutput()
   121	{
   122	        for(int i=0; i<Vector::N; i++)
   123	        {
   124	                data[i]->vectorOutput();
   125	        }
   126	}
   127	
   128	void Matrix::set(int index, int number, int value)
   129	{
   130	        data[index]->set(number, value);
   131	}
   132	
   133	int Matrix::get(int index, int number) const
   134	{
   135	        return data[index]->get(number);
   136	};
