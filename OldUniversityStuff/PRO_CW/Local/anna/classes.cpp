//------------------------------------------------------
// Лабораторна робота #6 F1: A = B * MIN(C)
//                       F2: MR = SORT (MA * MB)
//                       F3: D = SORT (A - M)*TRANS (MC)
// Шевчук Олександр Вікторович
// Група ІО-72
// 30.11.2009
// Заголовочний файл з реалізаціями класів Vector і Matrix
//------------------------------------------------------
#include "classes.h"
#include <iostream>
using namespace std;

Vector::Vector()
{
  data = new int[Vector::N];
}


void Vector::vectorInput()
{
  for(int i=0; i<Vector::N;i++)
  {
    if(AUTO)
    {
      data[i] = 1;
    }else
    {
      for(;;)
      {
	try
	{
	  cout<<"Enter the cipher\n";
	  cin>>data[i];
	  break;
	}catch(int)
	{
	  cout<<"You have entered a symbol instead of number. Reinput please";
	}
      }
    }
  }
}

void Vector::vectorInput(int n)
{
  for(int i=0; i<Vector::N;i++)
  {
    if(AUTO)
    {
      data[i] = n;
    }else
    {
      for(;;)
      {
	try
	{
	  cout<<"Enter the cipher\n";
	  cin>>data[i];
	  break;
	}catch(int)
	{
	  cout<<"You have entered a symbol instead of number. Reinput please";
	}
      }
    }
  }
}

void Vector::vectorOutput()
{
	if(!QUIET)
	{
		for(int i=0; i<Vector::N;i++)
		{
			cout<<data[i]<<" ";
		}
		cout<<endl;
	}
}
	
int Vector::get(int number) const
{
	return data[number];
}
	
void Vector::set(int number, int value)
{
	data[number] = value;
}
	
	
//___________________________________-

Matrix::Matrix()
{
	data = new Vector*[Vector::N];
	for(int i=0;i<Vector::N;i++)
	{
		data[i] = new Vector();
	}
}

void Matrix::matrixInput()
{
	for(int i=0; i<Vector::N; i++)
	{	
		data[i]->vectorInput();
	}
}

void Matrix::matrixInput(int n)
{
	for(int i=0; i<Vector::N; i++)
	{	
		data[i]->vectorInput(n);
	}
}
	
void Matrix::matrixOutput()
{
	for(int i=0; i<Vector::N; i++)
	{	
		data[i]->vectorOutput();
	}
}
	
void Matrix::set(int index, int number, int value)
{
	data[index]->set(number, value);
}
	
int Matrix::get(int index, int number) const
{
	return data[index]->get(number);
};