#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <limits.h>

using std::cout;
using std::endl;
using std::cin;

int N=16;
int P=8;
int H;
int id=0;
int fill_const=1;
int a=INT_MAX;
int* MB;
int* MC;
int* MD;

HANDLE event=CreateEvent(NULL,TRUE,FALSE,NULL);
HANDLE mutex=CreateMutex(NULL,FALSE,NULL);
HANDLE* semaphores;
HANDLE* s2;
CRITICAL_SECTION cs1,cs2;

int get_tid(){
	int res;
	WaitForSingleObject(mutex,INFINITE);
	res=id;
	id+=1;
	ReleaseMutex(mutex);
	return res;
}
int* init_matrix(){
	int* mat=new int[N*N];
	return mat;
}

int* fill_mat(int fill_var){
	int* MM=new int[N*N]; 
	for(int i=0;i<N*N;i++){
		MM[i]=fill_var;
	}
	return MM;
}

int count(int* MB, int* MC, int* MD,int tid){
    int res=INT_MAX;
    int buf;
    for(int i=0;i<N;i++){
        for(int j=tid*H;j<(tid+1)*H;j++){
	    buf=MD[i*N+j];
	    for(int k=0;k<N;k++){
	        buf+=MB[i*N+k]*MC[k*N+j];
	    }
	    if(res>buf){res=buf;};
	}
    } 
    return res;
}	

DWORD WINAPI run(void* p){
    int tid=get_tid();
    int a_loc;
    int* MB_loc=new int[N*N];
    if(tid==0){
        MB=fill_mat(fill_const);
        MC=fill_mat(fill_const);
        MD=fill_mat(fill_const);
        SetEvent(event);
    }
    else{
        WaitForSingleObject(event,INFINITE);
    }
    
    EnterCriticalSection(&cs1);
    for(int i=0;i<N*N;i++){
	MB_loc[i]=MB[i];
    }
    MB_loc=MB;
    LeaveCriticalSection(&cs1);
    a_loc=count(MB_loc,MC,MD,tid);
    EnterCriticalSection(&cs2);
    if(a>a_loc)a=a_loc;	
    LeaveCriticalSection(&cs2);
    
    if(tid==0){
    	WaitForMultipleObjects(P-1,semaphores,TRUE,INFINITE);
    	cout<<"Result: "<<a<<endl;
    }
    else{
    	ReleaseSemaphore(semaphores[tid-1],1,NULL);
    }
    
    return 0;
}

int main (int argc,char *argv[]){
    FILETIME start_time,end_time;
    double work_time;
    HANDLE Threads[P];
    MB=init_matrix();
    MC=init_matrix();
    MD=init_matrix();
    if (argc==3){
    	N=atoi(argv[1]);
	P=atoi(argv[2]);
    }
    H=N/P;
    char c;
    cin>>c;
    GetSystemTimeAsFileTime(&start_time);
    semaphores=new HANDLE[P-1];
    for(int i=0;i<P-1;i++){
	semaphores[i]=CreateSemaphore(NULL,0,1,NULL);
    }
    InitializeCriticalSection(&cs1);
    InitializeCriticalSection(&cs2);
    for(int i=0;i<P;i++){
	Threads[i]=CreateThread(NULL,0,run,NULL,0,NULL);
    }
    WaitForMultipleObjects(P,Threads,TRUE,INFINITE);
    GetSystemTimeAsFileTime(&end_time);
    work_time=(double)(end_time.dwLowDateTime-start_time.dwLowDateTime)/10000000;	
    cout<<"---------------------------------------------"<<endl;
    cout<<"N: "<<N<<endl;
    cout<<"P: "<<P<<endl;
    cout<<"Work time: "<< work_time<<endl;
    cout<<"---------------------------------------------"<<endl;
    return 0;
}
