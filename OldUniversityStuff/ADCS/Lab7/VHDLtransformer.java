package data;

import java.util.ArrayList;

public class VHDLtransformer {

	final static int ENTITY=0;
	final static int COMPONENT=1;
	
	public String generateVHDL(ArrayList<ArrayList<Integer[]>> y,ArrayList<ArrayList<Integer[]>> j
			,ArrayList<ArrayList<Integer[]>> k,int numOfTriggers){
		int numOfInputs=0;
		for(int i=0;i<y.size();i++){
			if(y.get(i).size()!=0){
				numOfInputs=y.get(i).get(0).length;
				break;
			}
		}
		StringBuffer components=new StringBuffer();
		StringBuffer signals=new StringBuffer();
		StringBuffer connections=new StringBuffer();
		//Needed components
		for(int i=0;i<y.size();i++){
			components.append(addFunction("y"+i, y.get(i),COMPONENT,numOfInputs));
		}
		for(int i=0;i<j.size();i++){
			components.append(addFunction("j"+i, j.get(i),COMPONENT,numOfInputs));
			components.append(addFunction("k"+i, k.get(i),COMPONENT,numOfInputs));
		}
		components.append(addTrigger(COMPONENT));
		components.append(addClock(COMPONENT));
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			components.append(addVariable(COMPONENT, "var"+i, (i+1)*10));
		}
		//Connections and signals
		int counter=0;
		for(int i=0;i<y.size();i++){
			signals.append("ys"+i+",");
			connections.append("E"+counter+": y"+i+" port map (" );
			for(int l=0;l<numOfTriggers;l++){
				connections.append("qs"+l+",");
			}
			for(int l=0;l<numOfInputs-numOfTriggers;l++){
				connections.append("xs"+l+",");
			}
			connections.append("ys"+i+");\n");
			counter++;
		}
		for(int i=0;i<j.size();i++){
			signals.append("js"+i+",");
			connections.append("E"+counter+": j"+i+" port map (" );
			for(int l=0;l<numOfTriggers;l++){
				connections.append("qs"+l+",");
			}
			for(int l=0;l<numOfInputs-numOfTriggers;l++){
				connections.append("xs"+l+",");
			}
			connections.append("js"+i+");\n");
			counter++;
		}
		for(int i=0;i<k.size();i++){
			signals.append("ks"+i+",");
			connections.append("E"+counter+": k"+i+" port map (" );
			for(int l=0;l<numOfTriggers;l++){
				connections.append("qs"+l+",");
			}
			for(int l=0;l<numOfInputs-numOfTriggers;l++){
				connections.append("xs"+l+",");
			}
			connections.append("ks"+i+");\n");
			counter++;
		}
		for(int i=0;i<numOfTriggers;i++){
			signals.append("qs"+i+",");
			connections.append("E"+counter+": JK port map(js"+i+",ks"+i+",R,Cs,qs"+i+");\n");
			counter++;
		}
		for(int i=0;i<y.size();i++){
			connections.append("E"+counter+": out"+i+" <= ys"+i+";\n");
			counter++;
		}
		signals.append("Cs,");
		connections.append("E"+counter+": Clock port map (Cs);\n");
		counter++;
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			signals.append("xs"+i+",");
			connections.append("E"+counter+": var"+i+" port map (xs"+i+");\n");
			counter++;
		}
		
		signals.deleteCharAt(signals.length()-1);
		//Form a result function string
		StringBuffer result=new StringBuffer();
		result.append("library IEEE;\n" +//Libraries hook-up
				"use IEEE.STD_LOGIC_1164.ALL;\n");// +
		result.append(addClock(ENTITY));
		
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			result.append(addVariable(ENTITY, "var"+i, (i+1)*10));
		}
		result.append(addTrigger(ENTITY));
		result.append(addAND(ENTITY));
		result.append(addOR(ENTITY));
		result.append(addNOT(ENTITY));
		
		for(int i=0;i<y.size();i++){
			result.append(addFunction("y"+i, y.get(i),ENTITY,numOfInputs));
		}
		for(int i=0;i<j.size();i++){
			result.append(addFunction("j"+i, j.get(i),ENTITY,numOfInputs));
			result.append(addFunction("k"+i, k.get(i),ENTITY,numOfInputs));
		}
		//Create main entity
		result.append("entity Main is\n" +
				"	port (\n");
		result.append("		R: in BIT;\n");
		result.append("		C: in BIT;\n");
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			result.append("		x"+i+": in BIT;\n");
		}
		for(int i=0;i<y.size();i++){
			result.append("		out"+i+": out BIT;\n");
		}
		result.deleteCharAt(result.length()-2);
		result.append("	);\n" +
				"end Main;\n\n");
		result.append("architecture Main of Main is\n");
		result.append(components.toString());
		result.append("signal "+signals.toString()+": BIT;\n" +
				"begin\n"+connections.toString()+"end Main;\n\n");
		return result.toString();
	}

	
	private String addFunction(String name,ArrayList<Integer[]> func,int mode,int inpNum){
		StringBuffer components=new StringBuffer();
		StringBuffer signals=new StringBuffer();
		StringBuffer connections=new StringBuffer();
		if(mode==ENTITY&&func.size()!=0){
			components.append(addOR(COMPONENT));
			components.append(addAND(COMPONENT));
			components.append(addNOT(COMPONENT));
			int counter=0;
			int signalcounter=0;
			String[] andConnectors=new String[func.size()];
			for(int i=0;i<func.size();i++){
				if(func.get(i).length==1){
					andConnectors[i]="in"+i;
				}
				else{
					int initial=0;
					String con[]=new String[2];
					int k=0;
					while(initial<2&&k<func.get(i).length){
						if(func.get(i)[k]==1){
							con[initial]="in"+k;
							initial++;
						}
						else if(func.get(i)[k]==0){
							signals.append("S"+signalcounter+",");
							connections.append("E"+counter+": NOT1 port map (in"+k+",S"+signalcounter+");\n");
							con[initial]="S"+signalcounter;
							signalcounter++;
							counter++;
							initial++;
						}
						k++;
					}
					if (initial==1){
						andConnectors[i]=con[0];
					}
					else{
						signals.append("S"+signalcounter+",");
						connections.append("E"+counter+": AND2 port map ("+con[0]+","+con[1]+",S"+signalcounter+");\n");
						andConnectors[i]="S"+signalcounter;
						signalcounter++;
						counter++;
					}
					for(int j=k;j<func.get(i).length;j++){
						String next="";
						if(func.get(i)[j]==1){
							next="in"+j;
							signals.append("S"+signalcounter+",");
							connections.append("E"+counter+": AND2 port map ("+andConnectors[i]+","+next+",S"+signalcounter+");\n");
							andConnectors[i]=("S"+signalcounter);
							signalcounter++;
							counter++;
							
						}
						else if(func.get(i)[j]==0){
							signals.append("S"+signalcounter+",");
							connections.append("E"+counter+": NOT1 port map (in"+j+",S"+signalcounter+");\n");
							next="S"+signalcounter;
							signalcounter++;
							counter++;
							signals.append("S"+signalcounter+",");
							connections.append("E"+counter+": AND2 port map ("+andConnectors[i]+","+next+",S"+signalcounter+");\n");
							andConnectors[i]=("S"+signalcounter);
							signalcounter++;
							counter++;
						}
					}
				}
			}
			if(andConnectors.length==1){
				connections.append("E"+counter+": Output <="+andConnectors[0]+";\n");
			}
			else{
				signals.append("S"+signalcounter+",");
				connections.append("E"+counter+": OR2 port map ("+andConnectors[0]+","+andConnectors[1]+",S"+signalcounter+");\n");
				String output="S"+signalcounter;
				signalcounter++;
				counter++;
				for(int i=2;i<andConnectors.length;i++){
					signals.append("S"+signalcounter+",");
					connections.append("E"+counter+": OR2 port map ("+output+","+andConnectors[i]+",S"+signalcounter+");\n");
					signalcounter++;
					counter++;
				}
				connections.append("E"+counter+": Output <="+output+";\n");
				
			}
			signals.deleteCharAt(signals.length()-1);
		}
		//Form a result function string
		StringBuffer result=new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append(name+" is\n" +
				"	port (\n");
		for(int i=0;i<inpNum;i++){
			result.append("		in"+i+": in BIT;\n");
		}
		result.append("		Output: out BIT\n" +
				"	);\n");
		if(mode==ENTITY){
			result.append("end "+name+";\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture "+name+" of "+name+" is\n");
			result.append(components.toString());
			if(func.size()!=0){
				result.append("signal "+signals.toString()+": BIT;\n" +
						"begin\n"+connections.toString()+"end "+name+";\n\n");
			}
			else{
				result.append("begin\n" +
						"	E0: Output <= '0';\n" +
						"end "+name+";\n\n");
			}
		}
		return result.toString();
	}
	
	
	
	private String addTrigger(int mode){
		StringBuffer result=new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("JK is\n" +
				"	port(J,K: in BIT;\n" +
				"		Reset: in BIT;\n" +
				"		Clock: in BIT;\n" +
				"		Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end JK;\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture JK of JK is\n " +
					"	signal temp: BIT;\n" +
					"begin\n" +
					"	process(Clock)\n" +
					"	begin\n" +
					"		if Clock'event and Clock=\'1\' then \n" +
					"			if Reset=\'1\' then\n" +
					"				temp <= \'0\';\n" +
					"			else \n" +
					"				if(J=\'0\' and K=\'0\') then \n" +
					"					temp<=temp;\n" +
					"				elsif(J=\'0\' and K=\'1\') then\n" +
					"					temp<=\'0\';\n" +
					"				elsif(J=\'1\' and K=\'0\') then\n" +
					"					temp<=\'1\';\n" +
					"				elsif(J=\'1\' and K=\'1\') then\n" +
					"					temp<=not(temp);\n" +
					"				end if;\n" +
					"			end if;\n" +
					"		end if;\n" +
					"	end process;\n" +
					"	Output<=temp;\n" +
					"end JK;\n\n"
					);
		}
		return result.toString();
	}
	
	private String addAND(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("AND2 is\n" +
				"port(in1,in2: in BIT;\n" +
				"	Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end AND2;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		
		
		if(mode==ENTITY){
			result.append("architecture AND2 of AND2 is\n " +
					"begin\n" +
					"	Output<=in1 and in2;\n"+
					"end AND2;\n\n"
					);
	
		}
		return result.toString();
	}
	
	private String addOR(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("OR2 is\n" +
				"port(in1,in2: in BIT;\n" +
				"	Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end OR2;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture OR2 of OR2 is\n " +
					"begin\n" +
					"	Output<=in1 or in2;\n"+
					"end OR2;\n\n"
					);
	
		}
		return result.toString();
	}

	private String addNOT(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("NOT1 is\n" +
				"port(Input: in BIT;\n" +
				"	Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end NOT1;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture NOT1 of NOT1 is\n " +
					"begin\n" +
					"	Output<= not(Input);\n"+
					"end NOT1;\n\n"
					);
	
		}
		return result.toString();
	}
	
	private String addClock(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("Clock is\n" +
				"port(Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end Clock;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture Clock of Clock is\n " +
					"	signal t: BIT;\n" +
					"begin\n" +
					"	Output<= not t after 1 ns;\n" +
					"	t<= not t after 1 ns;\n" +
					"end Clock;\n\n"
					);
	
		}
		return result.toString();
	}
	
	private String addVariable(int mode, String name,int time){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append(name +" is\n" +
				"port(Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end "+name+";\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture "+name+" of "+name+" is\n " +
					"	signal t: BIT;\n" +
					"begin\n" +
					"	Output<= not t after "+time+" ns;\n" +
					"	t<= not t after "+time+" ns;\n" +
					"end "+name+";\n\n"
					);
	
		}
		return result.toString();
	
	}

}
