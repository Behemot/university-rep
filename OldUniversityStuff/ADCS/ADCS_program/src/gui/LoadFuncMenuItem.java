package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Load menu button class.
 * 
 * @author anthony
 * 
 */
public class LoadFuncMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -50357213091528457L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class LoadTableListener extends MouseInputAdapter {
		GuiManager gui_;

		public LoadTableListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.loadFunctions();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public LoadFuncMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new LoadTableListener(g));
	}

}
