package gui;

import javax.swing.JFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Class representing window object.
 * 
 * @author anthony
 * 
 */
public class MainWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2740437090361841747L;

	/**
	 * Inner class that handles window operation routines
	 * 
	 * @author anthony
	 * 
	 */
	private class MainWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			e.getWindow().dispose();
		}
	}

	/**
	 * Constructor that sets up window an it's listener.
	 */
	public MainWindow() {
		this.addWindowListener(new MainWindowListener());
	}

}
