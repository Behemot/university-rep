package gui;

import javax.swing.table.AbstractTableModel;
import data.TriggersSetupTable;

public class StructuralTableModel extends AbstractTableModel{

	private static final long serialVersionUID = -3080253391004858274L;
	private TriggersSetupTable setupTable_;
	
	public StructuralTableModel(TriggersSetupTable setupTable){
		setupTable_=setupTable;
	}
	
	@Override
	public int getColumnCount() {
		return setupTable_.getWidth();
	}

	@Override
	public int getRowCount() {
		return setupTable_.getSize();
	}

	@Override
	public Object getValueAt(int y, int x) {
		switch(x){
			case 0: return setupTable_.getPreviousState(y);
			case 1: return setupTable_.getNextState(y);
			case 2: return setupTable_.getConditions(y);
			case 3: return setupTable_.getSignals(y);
			default: return setupTable_.getControlSignals(y).get(x-4);
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex){
			case 0: return "Previous State";
			case 1: return "Next State";
			case 2: return "Conditions";
			case 3: return "Signals";
			default: return "J"+String.valueOf(columnIndex-4)+"K"+String.valueOf(columnIndex-4);
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}


	
}
