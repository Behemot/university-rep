package gui;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class PathTableModel extends AbstractTableModel{
	private static final long serialVersionUID = -3080253391004858274L;
	private ArrayList<ArrayList<Integer>> paths_;
	private int maxWidth_=0;
	
	public PathTableModel(ArrayList<ArrayList<Integer>> paths){
		paths_=paths;
		for(int i=0;i<paths_.size();i++){
			if(paths_.get(i).size()>maxWidth_){
				maxWidth_=paths_.get(i).size();
			}
		}
	}
	
	@Override
	public int getColumnCount() {
		return maxWidth_;
	}

	@Override
	public int getRowCount() {
		return paths_.size();
	}

	@Override
	public Object getValueAt(int y, int x) {
		if(paths_.get(y).size()>x){
			return paths_.get(y).get(x);
		}
		else{
			return "";
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return String.valueOf(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}


	
	
	
}
