package gui;


import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;


public class CreateStructuralTableMenuItem extends JMenuItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1025625702313115087L;
	private class CreateStructuralTableListener extends MouseInputAdapter {
		GuiManager gui_;

		public CreateStructuralTableListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.createStructuralTable();
		}
	}
	/**
	 * Constructor that sets up listeners.
	 */
	
	public CreateStructuralTableMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new CreateStructuralTableListener(g));
	}

}
