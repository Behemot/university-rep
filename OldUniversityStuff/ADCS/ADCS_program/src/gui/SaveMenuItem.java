package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Save menu button class.
 * 
 * @author anthony
 * 
 */
public class SaveMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class SaveListener extends MouseInputAdapter {
		GuiManager gui_;

		public SaveListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.save();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public SaveMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new SaveListener(g));
	}

}
