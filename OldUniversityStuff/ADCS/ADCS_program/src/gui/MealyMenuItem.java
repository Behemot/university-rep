package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Create Mealy Machine menu button class.
 * 
 * @author anthony
 * 
 */
public class MealyMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1425281522311463684L;

	/**
	 * Create Mealy Machine menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class MealyListener extends MouseInputAdapter {
		GuiManager gui_;

		public MealyListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.createMealyMachine();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public MealyMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new MealyListener(g));
	}

}