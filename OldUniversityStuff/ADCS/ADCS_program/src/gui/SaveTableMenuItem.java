package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Save menu button class.
 * 
 * @author anthony
 * 
 */
public class SaveTableMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class SaveTableListener extends MouseInputAdapter {
		GuiManager gui_;

		public SaveTableListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.saveTable();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public SaveTableMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new SaveTableListener(g));
	}

}
