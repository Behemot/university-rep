package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Save menu button class.
 * 
 * @author anthony
 * 
 */
public class SaveMachineMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class SaveMachineListener extends MouseInputAdapter {
		GuiManager gui_;

		public SaveMachineListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.saveMachine();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public SaveMachineMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new SaveMachineListener(g));
	}

}
