package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Load menu button class.
 * 
 * @author anthony
 * 
 */
public class LoadTableMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -50357213091528457L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class LoadTableListener extends MouseInputAdapter {
		GuiManager gui_;

		public LoadTableListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.loadTable();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public LoadTableMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new LoadTableListener(g));
	}

}
