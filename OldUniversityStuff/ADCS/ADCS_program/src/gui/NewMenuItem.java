package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * New menu button class.
 * 
 * @author anthony
 * 
 */
public class NewMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1299575399995864282L;

	/**
	 * New menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class NewListener extends MouseInputAdapter {
		GuiManager gui_;

		public NewListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.reset();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public NewMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new NewListener(g));
	}

}
