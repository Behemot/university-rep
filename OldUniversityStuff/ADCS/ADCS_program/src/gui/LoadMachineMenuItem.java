package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Load menu button class.
 * 
 * @author anthony
 * 
 */
public class LoadMachineMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -50357213091528457L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class LoadMachineListener extends MouseInputAdapter {
		GuiManager gui_;

		public LoadMachineListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.loadMachine();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public LoadMachineMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new LoadMachineListener(g));
	}

}
