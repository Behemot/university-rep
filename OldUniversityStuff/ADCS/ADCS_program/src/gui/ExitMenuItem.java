package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Exit menu button class.
 * 
 * @author anthony
 * 
 */
public class ExitMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6010837360240974928L;

	/**
	 * Exit menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	class ExitListener extends MouseInputAdapter {
		GuiManager gui_;

		public ExitListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.disposeWindow();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public ExitMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new ExitListener(g));
	}

}
