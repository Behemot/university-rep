package gui;


import java.awt.FileDialog;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;


import fileSystem.DotStream;
import fileSystem.Serializer;
import data.Chart;
import data.Graph;
import data.MachineEncoder;
import data.MealyEdge;
import data.MealyMachine;
import data.MealyNode;
import data.Parser;
import data.ParsingError;
import data.QuineMcCluskeyMinimizer;
import data.TriggersSetupTable;
import data.VHDLtransformer;
import att.grappa.GrappaPanel;
import att.grappa.Node;
import att.grappa.Edge;
import att.grappa.GrappaSupport;


/**
 * Class that operates GUI and interacts with data model.
 * 
 * @author anthony
 * 
 */
public class GuiManager {

	/**
	 * Main window object.
	 */
	private MainWindow mainWindow_ = new MainWindow();

	/**
	 * Main text field.
	 */
	private JTextField mainTextField_ = new JTextField();
	/**
	 * Main window menu bar.
	 */
	private JMenuBar menuBar_ = new JMenuBar();
	/**
	 * File menu object.
					
	 */
	private JMenu fileMenu_ = new JMenu("File");
	/**
	 * New menu button.
	 */
	private NewMenuItem newMenuItem_ = new NewMenuItem("New", this);
	/**
	 * Save menu button.
	 */
	private SaveMenuItem saveMenuItem_ = new SaveMenuItem("Save", this);
	/**
	 * Load menu button.
	 */
	private LoadMenuItem loadMenuItem_ = new LoadMenuItem("Load", this);
	/**
	 * Save Mealy Machine Graph button.
	 */
	private SaveMachineMenuItem saveMachineMenuItem_=new SaveMachineMenuItem("Save Machine Graph",this);
	/**
	 * Load Mealy Machine Graph button.
	 */
	
	private LoadMachineMenuItem loadMachineMenuItem_=new LoadMachineMenuItem("Load Machine Graph",this);
	
	/**
	 * Save Table menu button.
	 */
	private SaveTableMenuItem saveTableMenuItem_ = new SaveTableMenuItem("Save Table", this);
	/**
	 * Load Table menu button.
	 */
	private LoadTableMenuItem loadTableMenuItem_ = new LoadTableMenuItem("Load Table", this);
	
	/**
	 * Save Functions menu button.
	 */
	private SaveFuncMenuItem saveFuncMenuItem_ = new SaveFuncMenuItem("Save Functions", this);
	/**
	 * Load Functions menu button.
	 */
	private LoadFuncMenuItem loadFuncMenuItem_ = new LoadFuncMenuItem("Load Functions", this);
	private SaveVHDLMenuItem saveVHDLItem_=new SaveVHDLMenuItem("Save VHDL",this);
	
	/**
	 * Exit menu button.
	 */
	private ExitMenuItem exitMenuItem_ = new ExitMenuItem("Exit", this);
	/**
	 * Parsing menu object.
	 */
	private JMenu parsingMenu_ = new JMenu("Parsing");
	/**
	 * Parse menu item.
	 */
	private ParseMenuItem parseMenuItem_ = new ParseMenuItem("Parse", this);
	/**
	 * Create Mealy Machine menu item.
	 */
	private MealyMenuItem mealyMenuItem_=new MealyMenuItem("Create Mealy Machine",this);
	/**
	 * Create Structural Table menu item.
	 */
	private CreateStructuralTableMenuItem structMenuItem_=new CreateStructuralTableMenuItem("Create Structual Table", this);
	
	/**
	 * Paths table.
	 */
	private JTable pathsTable_=new JTable();
	/**
	 * Cycles table
	 */
	private JTable cyclesTable_=new JTable();
	/**
	 * Dot stream for saving and loading machine graph.
	 */
	private DotStream dotStream_=new DotStream();
	/**
	 * Serializer for saving and loading.
	 */	
	private Serializer serializer_ = new Serializer();
	/**
	 * Current chart.
	 */
	private Chart chart_ = new Chart();
	/**
	 * Current graph.
	 */
	private Graph graph_;
	/**
	 * Current Mealy Machine.
	 */
	private MealyMachine machine_;
	/**
	 * Grappa graph.
	 */
	private att.grappa.Graph grappaGraph_=new att.grappa.Graph("Mealy Machine"); 
	/**
	 * Grappa panel for Mealy machine display.
	 */
	private GrappaPanel mealyMachinePanel_=new GrappaPanel(grappaGraph_);
	
	private JScrollPane grappaPane_;
	private JTabbedPane pathTabs_=new JTabbedPane();
	private JTabbedPane mainTabs_=new JTabbedPane();
	
	private TriggersSetupTable struct_;
	private JTable structTable_=new JTable();
	private JTextArea minimizationText_=new JTextArea();
	
	private ArrayList<ArrayList<Integer[]>> signals_functions=new ArrayList<ArrayList<Integer[]>>();
	private ArrayList<ArrayList<Integer[]>> trigger_signals_j=new ArrayList<ArrayList<Integer[]>>();
	private ArrayList<ArrayList<Integer[]>> trigger_signals_k=new ArrayList<ArrayList<Integer[]>>();
	private ArrayList<ArrayList<Integer[]>> minimized_signals=new ArrayList<ArrayList<Integer[]>>();
	private ArrayList<ArrayList<Integer[]>> minimized_trigger_j=new ArrayList<ArrayList<Integer[]>>();
	private ArrayList<ArrayList<Integer[]>> minimized_trigger_k=new ArrayList<ArrayList<Integer[]>>();
	
	
	
	/**
	 * Initialization routine for GUI.
	 */
	public void initialize() {
		grappaGraph_.addPanel(mealyMachinePanel_);
		
		mealyMachinePanel_.setVisible(true);
		mainWindow_.setSize(640, 480);
		mainWindow_.setJMenuBar(menuBar_);
		mainWindow_.setTitle("Logical Algorythm Chart Editor");
		pathsTable_.setModel(new PathTableModel(new ArrayList<ArrayList<Integer>>()));
		cyclesTable_.setModel(new PathTableModel(new ArrayList<ArrayList<Integer>>()));
		JScrollPane pathsPane_=new JScrollPane(pathsTable_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane cyclesPane_=new JScrollPane(cyclesTable_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);		
		pathTabs_.addTab("Paths", pathsPane_);
		pathTabs_.addTab("Cycles", cyclesPane_);
		grappaPane_=new JScrollPane(mealyMachinePanel_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane structPane_=new JScrollPane(structTable_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane minimizationPane_=new JScrollPane(minimizationText_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		mainTabs_.addTab("Editor", mainTextField_);
		mainTabs_.addTab("Paths and Cycles",pathTabs_);
		mainTabs_.addTab("Mealy Machine Graph",grappaPane_);
		mainTabs_.addTab("Structural Table", structPane_);
		mainTabs_.addTab("Minimization Results",minimizationPane_);
		mainWindow_.getContentPane().add(mainTabs_);
		menuBar_.add(fileMenu_);
		fileMenu_.add(newMenuItem_);
		fileMenu_.addSeparator();
		fileMenu_.add(saveMenuItem_);
		fileMenu_.add(saveMachineMenuItem_);
		fileMenu_.add(saveTableMenuItem_);
		fileMenu_.add(saveFuncMenuItem_);
		fileMenu_.add(saveVHDLItem_);
		fileMenu_.addSeparator();
		fileMenu_.add(loadMenuItem_);
		fileMenu_.add(loadMachineMenuItem_);
		fileMenu_.add(loadTableMenuItem_);
		fileMenu_.add(loadFuncMenuItem_);
		fileMenu_.addSeparator();
		fileMenu_.add(exitMenuItem_);
		
		menuBar_.add(parsingMenu_);
		parsingMenu_.add(parseMenuItem_);
		parsingMenu_.add(mealyMenuItem_);
		parsingMenu_.add(structMenuItem_);
		mainWindow_.setVisible(true);
	}

	/**
	 * Closes program window.
	 */
	public void disposeWindow() {
		mainWindow_.dispose();
	}

	/**
	 * Resets the parser.
	 */
	public void reset() {
		chart_ = new Chart();
		this.reload();
	}

	/**
	 * Saves current work to the file.
	 */
	public void save() {
		this.startParsing();
		File S;
		FileDialog SDialog = new FileDialog(mainWindow_);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			serializer_.serialize(S, chart_);
		}
	}

	/**
	 * Loads work from file.
	 */
	public void load() {
		JOptionPane ConfirmPane = new JOptionPane(null,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		ConfirmPane
				.setMessage("Do you want to save current chart before loading new one?");
		JDialog dialog = ConfirmPane.createDialog(mainWindow_, "Save");
		dialog.setVisible(true);
		try {
			if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.CANCEL_OPTION) {
			} else {
				if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.YES_OPTION) {
					this.save();
				}
				FileDialog FDialog = new FileDialog(mainWindow_, "Open");
				FDialog.setMode(FileDialog.LOAD);
				FDialog.setVisible(true);
				if (FDialog.getFile() != null) {
					File F = new File(FDialog.getFile());
					chart_ = serializer_.deserialize(F);
					this.reload();
				} else {
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
					ErrorPane.setMessage("Incorrect filename");
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}

		} catch (NullPointerException NP) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
			ErrorPane.setMessage("Critical failure while loading.");
			JDialog errorDialog = ErrorPane.createDialog(mainWindow_, "Error");
			errorDialog.setVisible(true);
			chart_ = new Chart();
			this.reload();
		}
	}

	public void startParsing() {
		Parser parser_ = new Parser();
		try {
			chart_ = parser_.parseString(mainTextField_.getText());
			graph_=new Graph(chart_);
			graph_.calculatePaths();
			pathsTable_.setModel(new PathTableModel(graph_.getPaths()));
			cyclesTable_.setModel(new PathTableModel(graph_.getCycles()));
			this.reload();
		} catch (ParsingError PE) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.PLAIN_MESSAGE);
			ErrorPane.setMessage(PE.getError());
			JDialog errorDialog = ErrorPane.createDialog(mainWindow_, "Error");
			errorDialog.setVisible(true);
		}
	}

	public void createMealyMachine(){
		startParsing();
		grappaGraph_.reset();
		MachineEncoder encoder=new MachineEncoder(new MealyMachine(graph_));
		machine_=encoder.getEncodedMachine();
		ArrayList<MealyNode> nodes_=machine_.getNodes();
		ArrayList<MealyEdge> edges_=machine_.getEdges();
		Node[] grappaNodes=new Node[nodes_.size()];
		Edge[] grappaEdges=new Edge[edges_.size()];
		for(int i=0;i<nodes_.size();i++){
			grappaNodes[i]=new Node(grappaGraph_,String.valueOf(nodes_.get(i).getID()));
			grappaGraph_.addNode(grappaNodes[i]);
		}
		for(int i=0;i<edges_.size();i++){
			
			int start=0,end=0;
			for(int j=0;j<nodes_.size();j++){
				if(nodes_.get(j).ifOutgoing(edges_.get(i))){
					start=j;
				}
				if(nodes_.get(j).ifIncoming(edges_.get(i))){
					end=j;
				}
			}
			grappaEdges[i]=new Edge(grappaGraph_,grappaNodes[start],grappaNodes[end],String.valueOf(i));
			grappaEdges[i].setAttribute(Edge.LABEL_ATTR, edges_.get(i).getConditions()+"/"+edges_.get(i).getSignals());
			grappaNodes[end].addEdge(grappaEdges[i], true);
			grappaNodes[start].addEdge(grappaEdges[i], false);
			grappaGraph_.addEdge(grappaEdges[i]);
		}
		try{
			Process dot=Runtime.getRuntime().exec("dot");
			GrappaSupport.filterGraph(grappaGraph_, dot);
			
		}
		catch(IOException IO){
			System.out.println("Layout Error");
		}
		reload();
		
	}
	
	
	
	public void saveMachine() {
		createMealyMachine();
		File S;
		FileDialog SDialog = new FileDialog(mainWindow_);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			dotStream_.write(S, grappaGraph_);
		}
	}
	
	
	public void loadMachine() {
		JOptionPane ConfirmPane = new JOptionPane(null,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		ConfirmPane
				.setMessage("Do you want to save current chart before loading new one?");
		JDialog dialog = ConfirmPane.createDialog(mainWindow_, "Save");
		dialog.setVisible(true);
		try {
			if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.CANCEL_OPTION) {
			} else {
				if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.YES_OPTION) {
					this.save();
				}
				FileDialog FDialog = new FileDialog(mainWindow_, "Open");
				FDialog.setMode(FileDialog.LOAD);
				FDialog.setVisible(true);
				if (FDialog.getFile() != null) {
					File F = new File(FDialog.getFile());
					grappaGraph_ = dotStream_.read(F);
					
				} else {
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
					ErrorPane.setMessage("Incorrect filename");
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}

		} catch (NullPointerException NP) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
			ErrorPane.setMessage("Critical failure while loading.");
			JDialog errorDialog = ErrorPane.createDialog(mainWindow_, "Error");
			errorDialog.setVisible(true);
			chart_ = new Chart();
			this.reload();
		}
		mealyMachinePanel_=new GrappaPanel(grappaGraph_);
		grappaGraph_.addPanel(mealyMachinePanel_);
		grappaPane_=new JScrollPane(mealyMachinePanel_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mainTabs_.remove(2);
		mainTabs_.addTab("Mealy Machine Graph",grappaPane_);
		reload();
	}
	
	
	public void createStructuralTable(){
		this.createMealyMachine();
		signals_functions=new ArrayList<ArrayList<Integer[]>>();
		trigger_signals_j=new ArrayList<ArrayList<Integer[]>>();
		trigger_signals_k=new ArrayList<ArrayList<Integer[]>>();
		minimized_signals=new ArrayList<ArrayList<Integer[]>>();
		minimized_trigger_j=new ArrayList<ArrayList<Integer[]>>();
		minimized_trigger_k=new ArrayList<ArrayList<Integer[]>>();
		
		struct_=new TriggersSetupTable(machine_);
		structTable_.setModel(new StructuralTableModel(struct_));
		String[] prevStates=new String[struct_.getSize()];
		String[] conditions=new String[struct_.getSize()];
		String[] signals=new String[struct_.getSize()];
		ArrayList<ArrayList<String>> triggerControls=new ArrayList<ArrayList<String>>();
		for(int i=0;i<struct_.getSize();i++){
			prevStates[i]=struct_.getPreviousState(i);
			conditions[i]=struct_.getConditions(i);
			signals[i]=struct_.getSignals(i);
			triggerControls.add(struct_.getControlSignals(i));
		}
		
		for(int i=0;i<signals[0].length();i++){
			ArrayList<Integer[]> temp_func=new ArrayList<Integer[]>();
			signals_functions.add(temp_func);
			for(int j=0;j<signals.length;j++){
				String temp_str=prevStates[j]+conditions[j];
				Integer[] temp_term=new Integer[temp_str.length()];
				for(int k=0;k<temp_term.length;k++){
					switch (temp_str.charAt(k)){
						case '1': temp_term[k]=1;break;
						case '0': temp_term[k]=0;break;
						case '-': temp_term[k]=-1;break;
					}
				}
				if(signals[j].charAt(i)=='1'||signals[j].charAt(i)=='-'){
					temp_func.add(temp_term);
				}
			}
		}
		
		for(int i=0;i<triggerControls.get(0).size();i++){
			ArrayList<Integer[]> temp_func_j=new ArrayList<Integer[]>();
			ArrayList<Integer[]> temp_func_k=new ArrayList<Integer[]>();
			trigger_signals_j.add(temp_func_j);
			trigger_signals_k.add(temp_func_k);
			for(int j=0;j<triggerControls.size();j++){
				String temp_str=prevStates[j]+conditions[j];
				Integer[] temp_term=new Integer[temp_str.length()];
				for(int k=0;k<temp_term.length;k++){
					switch (temp_str.charAt(k)){
						case '1': temp_term[k]=1;break;
						case '0': temp_term[k]=0;break;
						case '-': temp_term[k]=-1;break;
					}
				}
				if(triggerControls.get(j).get(i).charAt(0)=='1'){
					temp_func_j.add(temp_term);
				}
				if(triggerControls.get(j).get(i).charAt(1)=='1'){
					temp_func_k.add(temp_term);
				}
			}
		}
		
		
		QuineMcCluskeyMinimizer minimizer=new QuineMcCluskeyMinimizer();
		for(int i=0;i<signals_functions.size();i++){
			if(signals_functions.get(i).size()!=0){
				minimized_signals.add(minimizer.minimize(signals_functions.get(i)));
			}
			else{
				minimized_signals.add(new ArrayList<Integer[]>());
			}
		}
		
		for(int i=0;i<trigger_signals_j.size();i++){
			if(trigger_signals_j.get(i).size()!=0){
				minimized_trigger_j.add(minimizer.minimize(trigger_signals_j.get(i)));
			}
			else{
				minimized_trigger_j.add(new ArrayList<Integer[]>());
			}
		}
		
		for(int i=0;i<trigger_signals_k.size();i++){
			if(trigger_signals_k.get(i).size()!=0){
				minimized_trigger_k.add(minimizer.minimize(trigger_signals_k.get(i)));
			}
			else{
				minimized_trigger_k.add(new ArrayList<Integer[]>());
			}
		}
		printFunctions();
		reload();
	}
	
	private void printFunctions(){
		StringBuffer result=new StringBuffer();
		result.append("Non-minimized functions:\n");
		addText(signals_functions,result,"y");
		result.append("\n");
		addText(trigger_signals_j,result,"j");
		result.append("\n");
		addText(trigger_signals_k,result,"k");
		result.append("\n");
		
		result.append("Minimized functions:\n");
		addText(minimized_signals,result,"y");
		result.append("\n");
		addText(minimized_trigger_j,result,"j");
		result.append("\n");
		addText(minimized_trigger_k,result,"k");
		result.append("\n");
			
		result.append("Compression rates:\n");
		for(int i=0;i<signals_functions.size();i++){
			result.append("y"+i+": "+(double)signals_functions.get(i).size()/(double)minimized_signals.get(i).size()+"\n");
		}
		for(int i=0;i<trigger_signals_j.size();i++){
			result.append("j"+i+": "+(double)trigger_signals_j.get(i).size()/(double)minimized_trigger_j.get(i).size()+"\n");
		}
		for(int i=0;i<trigger_signals_k.size();i++){
			result.append("k"+i+": "+(double)trigger_signals_k.get(i).size()/(double)minimized_trigger_k.get(i).size()+"\n");
		}
		
		minimizationText_.setText(result.toString());
	
	}
	
	
	private void addText(ArrayList<ArrayList<Integer[]>> array,StringBuffer buf,String mod){
		for(int i=0;i<array.size();i++){
			buf.append(mod+i+": ");
			for(int j=0;j<array.get(i).size();j++){
				buf.append("(");
				for(int k=0;k<array.get(i).get(j).length;k++){
					switch (array.get(i).get(j)[k]){
						case 1:buf.append("1");break;
						case 0:buf.append("0");break;
						case -1:buf.append("-");break;
					}
				}
				if(j<array.get(i).size()-1){
					buf.append(")||");
				}
				else{
					buf.append(")");
				}
					
			}
			buf.append("\n");
		}
	
	}
	
	
	/**
	 * Saves current work to the file.
	 */
	public void saveTable() {
		this.createStructuralTable();
		File S;
		FileDialog SDialog = new FileDialog(mainWindow_);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			serializer_.serializeTable(S, struct_);
		}
	}

	/**
	 * Loads work from file.
	 */
	public void loadTable() {
		JOptionPane ConfirmPane = new JOptionPane(null,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		ConfirmPane
				.setMessage("Do you want to save current table before loading new one?");
		JDialog dialog = ConfirmPane.createDialog(mainWindow_, "Save");
		dialog.setVisible(true);
		try {
			if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.CANCEL_OPTION) {
			} else {
				if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.YES_OPTION) {
					this.save();
				}
				FileDialog FDialog = new FileDialog(mainWindow_, "Open");
				FDialog.setMode(FileDialog.LOAD);
				FDialog.setVisible(true);
				if (FDialog.getFile() != null) {
					File F = new File(FDialog.getFile());
					struct_ = serializer_.deserializeTable(F);
					structTable_.setModel(new StructuralTableModel(struct_));
					this.reload();
				} else {
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
					ErrorPane.setMessage("Incorrect filename");
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}

		} catch (NullPointerException NP) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
			ErrorPane.setMessage("Critical failure while loading.");
			JDialog errorDialog = ErrorPane.createDialog(mainWindow_, "Error");
			errorDialog.setVisible(true);
			this.reload();
		}
	}

	
	public void saveFunctions() {
		this.createStructuralTable();
		ArrayList<ArrayList<ArrayList<Integer[]>>> pack=new ArrayList<ArrayList<ArrayList<Integer[]>>>();
		pack.add(signals_functions);
		pack.add(trigger_signals_j);
		pack.add(trigger_signals_k);
		pack.add(minimized_signals);
		pack.add(minimized_trigger_j);
		pack.add(minimized_trigger_k);
		File S;
		FileDialog SDialog = new FileDialog(mainWindow_);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			System.out.println("OUT FUNCTION: "+pack.get(1).size());
			
			serializer_.serializeFunctions(S, pack);
		}
	}
	
	public void loadFunctions() {
		JOptionPane ConfirmPane = new JOptionPane(null,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		ConfirmPane
				.setMessage("Do you want to save current function before loading new one?");
		JDialog dialog = ConfirmPane.createDialog(mainWindow_, "Save");
		dialog.setVisible(true);
		try {
			if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.CANCEL_OPTION) {
			} else {
				if (Integer.parseInt(ConfirmPane.getValue().toString()) == JOptionPane.YES_OPTION) {
					this.save();
				}
				FileDialog FDialog = new FileDialog(mainWindow_, "Open");
				FDialog.setMode(FileDialog.LOAD);
				FDialog.setVisible(true);
				if (FDialog.getFile() != null) {
					File F = new File(FDialog.getFile());
					ArrayList<ArrayList<ArrayList<Integer[]>>> pack= serializer_.deserializeFunctions(F);
					signals_functions=pack.get(0);
					trigger_signals_j=pack.get(1);
					trigger_signals_k=pack.get(2);
					minimized_signals=pack.get(3);
					minimized_trigger_j=pack.get(4);
					minimized_trigger_k=pack.get(5);
					printFunctions();
					this.reload();
				} else {
					JOptionPane ErrorPane = new JOptionPane(null,
							JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
					ErrorPane.setMessage("Incorrect filename");
					JDialog errorDialog = ErrorPane.createDialog(mainWindow_,
							"Error");
					errorDialog.setVisible(true);
				}
			}

		} catch (NullPointerException NP) {
			JOptionPane ErrorPane = new JOptionPane(null,
					JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION);
			ErrorPane.setMessage("Critical failure while loading.");
			JDialog errorDialog = ErrorPane.createDialog(mainWindow_, "Error");
			errorDialog.setVisible(true);
			this.reload();
		}
	}

	public void saveVHDL(){
		this.createStructuralTable();
		File S;
		FileDialog SDialog = new FileDialog(mainWindow_);
		SDialog.setMode(FileDialog.SAVE);
		SDialog.setVisible(true);
		VHDLtransformer vhdl=new VHDLtransformer();
		if (SDialog.getFile() != null) {
			S = new File(SDialog.getFile());
			serializer_.saveVHDL(S, vhdl.generateVHDL(minimized_signals,
					minimized_trigger_j, minimized_trigger_k, struct_.getPreviousState(0).length()));
		}

	}
	
	
	public void reload() {
		mainTextField_.setText(chart_.toString());
		mainWindow_.repaint();
		
	}
	
	
}
