package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Parse menu button class.
 * 
 * @author anthony
 * 
 */
public class ParseMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1425281522311463684L;

	/**
	 * Parse menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class ParseListener extends MouseInputAdapter {
		GuiManager gui_;

		public ParseListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.startParsing();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public ParseMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new ParseListener(g));
	}

}
