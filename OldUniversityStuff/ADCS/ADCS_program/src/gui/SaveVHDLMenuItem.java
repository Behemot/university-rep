package gui;

import javax.swing.JMenuItem;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Save menu button class.
 * 
 * @author anthony
 * 
 */
public class SaveVHDLMenuItem extends JMenuItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Save menu button listener.
	 * 
	 * @author anthony
	 * 
	 */
	private class SaveVHDLListener extends MouseInputAdapter {
		GuiManager gui_;

		public SaveVHDLListener(GuiManager g) {
			gui_ = g;
		}

		public void mousePressed(MouseEvent e) {
			gui_.saveVHDL();
		}
	}

	/**
	 * Constructor that sets up listeners.
	 */
	public SaveVHDLMenuItem(String S, GuiManager g) {
		super(S);
		this.addMouseListener(new SaveVHDLListener(g));
	}

}
