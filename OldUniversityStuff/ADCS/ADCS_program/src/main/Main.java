package main;

import gui.GuiManager;

public class Main {
	public static void main(String[] args) {
		GuiManager manager = new GuiManager();
		manager.initialize();
	}
}
