package data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Superclass for chart node.
 * 
 * @author anthony
 * 
 */
public class Node implements Serializable {

	/**
	 * Serialization UID
	 */
	private static final long serialVersionUID = 1336213532698443851L;
	/**
	 * Node type: 0 - operational 1 - conditional 2 - jump node 3 - label node 4 - start node 5 - end node
	 */
	protected int type_;
	/**
	 * Natural destination of the node.
	 */
	protected Node naturalDestination_;

	public Node(int type){
		type_=type;
	}
	/**
	 * Sets new natural destination.
	 * 
	 * @param N node object.
	 */
	public void setNaturalDestination(Node N) {
		naturalDestination_ = N;
	}

	/**
	 * Returns node destination.
	 * 
	 * @return node object.
	 */
	public Node getDestination(ArrayList<String> signals) {
		return naturalDestination_;
	}

	/**
	 * Returns node type.
	 * 
	 * @return integer referring to node type.
	 */
	public int getType() {
		return type_;
	}

}
