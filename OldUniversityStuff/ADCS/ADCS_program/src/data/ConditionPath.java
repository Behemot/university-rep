package data;

public class ConditionPath {
	private String condition_;
	private Node node_;
	
	public ConditionPath(String cond){
		condition_=cond;
	}
	
	public void setCondition(String cond){
		condition_=cond;
	}
	
	public void setNode(Node n){
		node_=n;
	}
	
	public String getCondition(){
		return condition_;
	}
	
	public Node getNode(){
		return node_;
	}
}
