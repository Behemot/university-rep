package data;
import java.util.ArrayList;

public class MachineEncoder {
	
	ArrayList<ArrayList<Boolean>> codes_;
	ArrayList<Integer> codePosition_;
	ArrayList<MealyNode> nodes_;
	ArrayList<MealyEdge> edges_;
	public MachineEncoder(MealyMachine M){
		nodes_=M.getNodes();
		edges_=M.getEdges();
		codes_=new ArrayList<ArrayList<Boolean>>();
		codePosition_=new ArrayList<Integer>();
		for(int i=0;i<nodes_.size();i++){
			codePosition_.add(new Integer(-1));
		}
		ArrayList<Boolean> firstCode=new ArrayList<Boolean>();
		for(int i=0;i<(int)Math.floor(Math.log(nodes_.size())/Math.log(2));i++){
			firstCode.add(false);
		}
		codes_.add(firstCode);
		codePosition_.set(0, 0);
		setCodes(nodes_.get(0),firstCode);
	
	}
	
	public void setCodes(MealyNode parent,ArrayList<Boolean> parentCode){
		ArrayList<MealyEdge> outEdges=parent.getOutgoingEdges();
		ArrayList<MealyNode> destinationNodes=new ArrayList<MealyNode>();
		for(int i=0;i<outEdges.size();i++){
			destinationNodes.add(outEdges.get(i).getEnd());
		}
		for(int i=0;i<destinationNodes.size();i++){

			
			int codePos=codePosition_.get(nodes_.indexOf(destinationNodes.get(i))); 
			if(codePos==-1){
				while(checkCode(parentCode)==null){
					extendCodes();
				}
				ArrayList<Boolean> nextCode=checkCode(parentCode);
				codes_.add(nextCode);
				codePosition_.set(nodes_.indexOf(destinationNodes.get(i)), codes_.size()-1);
				setCodes(destinationNodes.get(i),nextCode);
			
			}
			else{
				destinationNodes.get(i).removeIncomingEdge(outEdges.get(i));
				generatePath(parentCode,codes_.get(codePos),outEdges.get(i));
			}
		}	
	}
	
	private ArrayList<Boolean> checkCode(ArrayList<Boolean> code){
		ArrayList<Boolean> codePossibility=new ArrayList<Boolean>(code);
		ArrayList<Integer> changePossibilities=new ArrayList<Integer>();
		for(int i=0;i<codePossibility.size();i++){
			if(codePossibility.get(i)==false){
				changePossibilities.add(i);
			}
		}
		boolean foundMatch=false;
		for(int i=changePossibilities.size()-1;i>=0;i--){
			codePossibility=new ArrayList<Boolean>(code);
			codePossibility.set(changePossibilities.get(i),true);
			foundMatch=false;
			for(int j=0;j<codes_.size();j++){
				boolean matches=true;
				for(int k=0;k<codes_.get(j).size();k++){
					if(codePossibility.get(k)!=codes_.get(j).get(k)){
						matches=false;
						break;
					}
				}
				if(matches==true){
					foundMatch=true;
					break;
				}
			}
			if(foundMatch==false){
				break;
			}
		}
		if(foundMatch==true||changePossibilities.size()==0){
			return null;
		}
		else{
			return codePossibility;
		}
	}
	
	private void extendCodes(){
		for(int i=0;i<codes_.size();i++){
			codes_.get(i).add(false);
		}
	}
	
	private void generatePath(ArrayList<Boolean> start,ArrayList<Boolean> end,MealyEdge E){
		ArrayList<Integer> differencePoints =new ArrayList<Integer>();
		for(int i=0;i<start.size();i++){
			if(start.get(i)!=end.get(i)){
				differencePoints.add(i);
			}
		}
		if (differencePoints.size()>1){
			ArrayList<Boolean> possibleIntermediate=new ArrayList<Boolean>(start);
			if(possibleIntermediate.get(differencePoints.get(0))){
				possibleIntermediate.set(differencePoints.get(0),false);
			}
			else{
				possibleIntermediate.set(differencePoints.get(0),true);
			}
			
			boolean foundMatch=false;
			for(int j=0;j<codes_.size();j++){
				boolean matches=true;
				for(int k=0;k<codes_.get(j).size();k++){
					if(possibleIntermediate.get(k)!=codes_.get(j).get(k)){
						matches=false;
						break;
					}
				}
				if(matches==true){
					foundMatch=true;
					break;
				}
			}
			
			if(foundMatch){
				extendCodes();
				possibleIntermediate=new ArrayList<Boolean>(start);
				possibleIntermediate.set(possibleIntermediate.size()-1, true);
				
			}
			
			nodes_.add(new MealyNode(""));
			codes_.add(possibleIntermediate);
			codePosition_.add(codes_.size()-1);
			nodes_.get(nodes_.size()-1).addEdge(E, MealyNode.INCOMING);
			E.setEnd(nodes_.get(nodes_.size()-1));
			edges_.add(new MealyEdge("","",edges_.size()));
			nodes_.get(nodes_.size()-1).addEdge(edges_.get(edges_.size()-1), MealyNode.OUTGOING);
			edges_.get(edges_.size()-1).setStart(nodes_.get(nodes_.size()-1));
			generatePath(possibleIntermediate,end,edges_.get(edges_.size()-1));
		}
		else{
			int codepos=0;
			int nodepos=0;
			boolean found=false;
			boolean foundnode=false;
			for(int i=0;i<codes_.size();i++){
				found=true;
				for(int j=0;j<codes_.get(i).size();j++){
					if(codes_.get(i).get(j)!=end.get(j)){
						found=false;
					}
				}
				if(found==true){
					codepos=i;
					break;
				}
			}
			if(found){
				for(int i=0;i<codePosition_.size();i++){
					if(codePosition_.get(i)==codepos){
						foundnode=true;
						nodepos=i;
					}
				}
				if(foundnode){
					nodes_.get(nodepos).addEdge(E, MealyNode.INCOMING);
					E.setEnd(nodes_.get(nodepos));
				}
			}
		}
		
	}
	
	
	public MealyMachine getEncodedMachine(){
		for(int i=0;i<nodes_.size();i++){
			String id="";
			for(int j=0;j<codes_.get(codePosition_.get(i)).size();j++){
				if(codes_.get(codePosition_.get(i)).get(j)){
					id+="1";
				}
				else{
					id+="0";
				}
			}
			nodes_.get(i).setID(id);
		}
		return new MealyMachine(nodes_, edges_);
	}
			
	
}
