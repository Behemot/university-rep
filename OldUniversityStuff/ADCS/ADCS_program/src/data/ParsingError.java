package data;

/**
 * Parsing exception class.
 * 
 * @author anthony
 * 
 */
public class ParsingError extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3408723309081184111L;
	/**
	 * Error message.
	 */
	private String error_;

	/**
	 * Constructor that specifies error message.
	 * 
	 * @param S
	 *            error message String.
	 */
	public ParsingError(String S) {
		error_ = S;
	}

	/**
	 * Getter for the error message.
	 * 
	 * @return message String.
	 */
	public String getError() {
		return error_;
	}
}
