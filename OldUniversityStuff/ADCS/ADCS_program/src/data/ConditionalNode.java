package data;

import java.util.ArrayList;

/**
 * Conditional node class
 * 
 * @author anthony
 * 
 */
public class ConditionalNode extends Node {
	/**
	 * Serialization UID.
	 */
	private static final long serialVersionUID = -2117683929117164725L;
	/**
	 * Conditional signals waited by the node;
	 */
	private ArrayList<String> conditionalSignals_;
	/**
	 * Destination of the node if the condition is true;
	 */
	private Node conditionalDestination_;

	/**
	 * Basic constructor
	 * 
	 * @param signals
	 *            ArrayList of conditional signals to be expected.
	 */
	public ConditionalNode(ArrayList<String> signals) {
		super(1);
		conditionalSignals_ = signals;
		
	}

	/**
	 * Set conditional node destination in case conditions are true.
	 * 
	 * @param N
	 *            destination node.
	 */
	public void setConditionalDestination(Node N) {
		conditionalDestination_ = N;
	}
	/**
	 * Returns conditional destination of the node.
	 * @return conditional destination node.
	 */
	public Node getConditionalDestination(){
		return conditionalDestination_;
	}
	
	/**
	 * Returns natural destination of the node.
	 * @return natural destination node.
	 */
	public Node getNaturalDestination(){
		return naturalDestination_;
	}
	@Override
	public Node getDestination(ArrayList<String> signals) {
		if(signals!=null){
			boolean isActive_ = false;
			for (int i = 0; i < conditionalSignals_.size(); i++) {
				isActive_ = false;
				for (int j = 0; j < signals.size(); j++) {
					if (conditionalSignals_.get(i).equals(signals.get(j))) {
						isActive_ = true;
						break;
					}
				}
				if (isActive_ == false) {
					break;
				}
			}
			if (isActive_) {
				return conditionalDestination_;
			} else {
				return naturalDestination_;
			}
		}
		else{
			return naturalDestination_;
		}
	}

	/**
	 * Returns conditional signals the node accepts.
	 * 
	 * @return ArrayList of String representing signals.
	 */
	public ArrayList<String> getConditionalSignals() {
		return conditionalSignals_;
	}
}
