package data;
import java.util.ArrayList;

/** 
 * Class that represents Mealy machine for designated graph.
 */
public class MealyMachine {
	
	
	private ArrayList<MealyNode> nodes_=new ArrayList<MealyNode>();
	private ArrayList<MealyEdge> edges_=new ArrayList<MealyEdge>();
	private boolean[] isProcessed;
	private MealyNode[] tempNodes;
	private int curID_=0;
	private int edgeID_=0;
	private ArrayList<Node> graphNodes_;
	/**
	 * Constructor that transforms graph into Mealy machine.
	 * @param g input Graph.
	 */
	public MealyMachine(Graph g){
		graphNodes_=g.getNodes();
		Node start=graphNodes_.get(0);
		start=start.getDestination(null);
		isProcessed=new boolean[graphNodes_.size()];
		tempNodes=new MealyNode[graphNodes_.size()];
		traceRoute(start,null);
	}
	private void traceRoute(Node N,MealyEdge E){
		if(N.getType()==0){
			if(E!=null&&E.getSignals().compareTo("")!=0){
				MealyNode destination;
				if(!isProcessed[graphNodes_.indexOf(N)]){
					nodes_.add(new MealyNode(String.valueOf(curID_)));
					destination=nodes_.get(nodes_.size()-1);
					tempNodes[graphNodes_.indexOf(N)]=nodes_.get(nodes_.size()-1);
					curID_++;
				}
				else{
					destination=tempNodes[graphNodes_.indexOf(N)];
				}
				destination.addEdge(E, MealyNode.INCOMING);
				E.setEnd(destination);
				if(!isProcessed[graphNodes_.indexOf(N)]){
					isProcessed[graphNodes_.indexOf(N)]=true;
					ArrayList<String> temp=((OperationalNode)N).getOperationalSignals();
					String signals="";
					for(int i=0;i<temp.size()-1;i++){
						signals+=temp.get(i)+",";
					}
					signals+=temp.get(temp.size()-1);
					MealyEdge NE=new MealyEdge("",signals,edgeID_);
					edgeID_++;
					destination.addEdge(NE, MealyNode.OUTGOING);
					NE.setStart(destination);
					edges_.add(NE);
					traceRoute(N.getDestination(null),NE);
				}
			}
			else if(E!=null&&E.getSignals().compareTo("")==0){
					ArrayList<String> temp=((OperationalNode)N).getOperationalSignals();
					String signals="";
					for(int i=0;i<temp.size();i++){
						signals+=temp.get(i);
					}
					E.setSignals(signals);
					traceRoute(N.getDestination(null),E);
				
			}
			else{
				MealyNode destination;
				if(!isProcessed[graphNodes_.indexOf(N)]){
					nodes_.add(new MealyNode(String.valueOf(curID_)));
					destination=nodes_.get(nodes_.size()-1);
					isProcessed[graphNodes_.indexOf(N)]=true;
					tempNodes[graphNodes_.indexOf(N)]=nodes_.get(nodes_.size()-1);
					curID_++;
				}
				else{
					destination=tempNodes[graphNodes_.indexOf(N)];
				}
				ArrayList<String> temp=((OperationalNode)N).getOperationalSignals();
				String signals="";
				for(int i=0;i<temp.size()-1;i++){
					signals+=temp.get(i)+",";
				}
				signals+=temp.get(temp.size()-1);
				MealyEdge NE=new MealyEdge("",signals,edgeID_);
				edgeID_++;
				edges_.add(NE);
				destination.addEdge(NE, MealyNode.OUTGOING);
				NE.setStart(destination);
				traceRoute(N.getDestination(null),NE);
			}
		}
		else if(N.getType()==1){
			if(E!=null&&E.getSignals().compareTo("")==0){
				if(!isProcessed[graphNodes_.indexOf(N)]){
					ArrayList<String> temp=((ConditionalNode)N).getConditionalSignals();
					String signals="";
					for(int i=0;i<temp.size()-1;i++){
						signals+=temp.get(i);
					}
					signals+=temp.get(temp.size()-1);
					MealyEdge NE=E.clone(edgeID_);
					edgeID_++;
					edges_.add(NE);
					E.setConditions(E.getConditions()+","+signals);
					NE.setConditions(NE.getConditions()+","+"n("+signals+")");
					for(int i=0;i<nodes_.size();i++){
						if(nodes_.get(i).ifOutgoing(E)){
							nodes_.get(i).addEdge(NE, MealyNode.OUTGOING);
							NE.setStart(nodes_.get(i));
							break;
						}
					}
					traceRoute(((ConditionalNode)N).getNaturalDestination(),NE);
					traceRoute(((ConditionalNode)N).getConditionalDestination(),E);
				}
				else{
					tempNodes[graphNodes_.indexOf(N)].addEdge(E, MealyNode.INCOMING);
					E.setEnd(tempNodes[graphNodes_.indexOf(N)]);
				}
			}
			else if(E!=null&&E.getSignals().compareTo("")!=0){
				MealyNode destination;
				if(!isProcessed[graphNodes_.indexOf(N)]){
					nodes_.add(new MealyNode(String.valueOf(curID_)));
					destination=nodes_.get(nodes_.size()-1);
					tempNodes[graphNodes_.indexOf(N)]=nodes_.get(nodes_.size()-1);
					curID_++;
				}
				else{
					destination=tempNodes[graphNodes_.indexOf(N)];
				}
				destination.addEdge(E, MealyNode.INCOMING);
				E.setEnd(destination);
				if(!isProcessed[graphNodes_.indexOf(N)]){
					isProcessed[graphNodes_.indexOf(N)]=true;
					ArrayList<String> temp=((ConditionalNode)N).getConditionalSignals();
					String signals="";
					for(int i=0;i<temp.size()-1;i++){
						signals+=temp.get(i)+",";
					}
					signals+=temp.get(temp.size()-1);
					MealyEdge NET=new MealyEdge(signals,"",edgeID_);
					edgeID_++;
					MealyEdge NEF=new MealyEdge("n("+signals+")","",edgeID_);
					edgeID_++;
					edges_.add(NET);
					edges_.add(NEF);
					destination.addEdge(NET, MealyNode.OUTGOING);
					destination.addEdge(NEF, MealyNode.OUTGOING);
					NET.setStart(destination);
					NEF.setStart(destination);
					traceRoute(((ConditionalNode)N).getNaturalDestination(),NEF);
					traceRoute(((ConditionalNode)N).getConditionalDestination(),NET);
				}
			}
			else{
				MealyNode destination;
				if(!isProcessed[graphNodes_.indexOf(N)]){
					nodes_.add(new MealyNode(String.valueOf(curID_)));
					destination=nodes_.get(nodes_.size()-1);
					tempNodes[graphNodes_.indexOf(N)]=nodes_.get(nodes_.size()-1);
					curID_++;
				}
				else{
					destination=tempNodes[graphNodes_.indexOf(N)];
				}
				if(!isProcessed[graphNodes_.indexOf(N)]){
					isProcessed[graphNodes_.indexOf(N)]=true;
					ArrayList<String> temp=((ConditionalNode)N).getConditionalSignals();
					String signals="";
					for(int i=0;i<temp.size()-1;i++){
						signals+=temp.get(i)+",";
					}
					signals+=temp.get(temp.size()-1);
					MealyEdge NET=new MealyEdge(signals,"",edgeID_);
					edgeID_++;
					MealyEdge NEF=new MealyEdge("n("+signals+")","",edgeID_);
					edgeID_++;
					edges_.add(NET);
					edges_.add(NEF);
					destination.addEdge(NET, MealyNode.OUTGOING);
					destination.addEdge(NEF, MealyNode.OUTGOING);
					NET.setStart(destination);
					NEF.setStart(destination);
					traceRoute(((ConditionalNode)N).getNaturalDestination(),NEF);
					traceRoute(((ConditionalNode)N).getConditionalDestination(),NET);
				}

			}
		}
		else if(N.getType()==5){
			MealyNode destination;
			if(!isProcessed[graphNodes_.indexOf(N)]){
				nodes_.add(new MealyNode(String.valueOf(curID_)));
				destination=nodes_.get(nodes_.size()-1);
				isProcessed[graphNodes_.indexOf(N)]=true;
				tempNodes[graphNodes_.indexOf(N)]=nodes_.get(nodes_.size()-1);
				curID_++;
			}
			else{
				destination=tempNodes[graphNodes_.indexOf(N)];
			}
			destination.addEdge(E, MealyNode.INCOMING);
			E.setEnd(destination);
		}
	}
	
	
	
	public MealyMachine(ArrayList<MealyNode> nodes,ArrayList<MealyEdge> edges){
		nodes_=nodes;
		edges_=edges;
	}
	public ArrayList<MealyNode> getNodes(){
		return nodes_;
	}
	
	public ArrayList<MealyEdge> getEdges(){
		return edges_;
	}
	
}
