package data;

import java.util.ArrayList;


/**
 * Graph class; 
 * @author anthony
 *
 */
public class Graph {
	/**
	 * All the nodes in the graph.
	 */
	private ArrayList<Node> nodes_=new ArrayList<Node>();
	/**
	 * All paths in the graph.
	 */
	private ArrayList<ArrayList<Integer>> paths_=new ArrayList<ArrayList<Integer>>(); 
	/**
	 * All cycles in the graph.
	 */
	private ArrayList<ArrayList<Integer>> cycles_=new ArrayList<ArrayList<Integer>>();
	/**
	 * Constructor that transforms Chart object into Graph object.
	 * @param C Chart object.
	 */
	public Graph(Chart C){
		ArrayList<Node> temp_=C.getNodes();
		for(int i=0;i<temp_.size();i++){
			nodes_.add(temp_.get(i));
		}
		for(int i=0;i<nodes_.size();i++){
			if (nodes_.get(i).getType()==1){
				while(!(((ConditionalNode)nodes_.get(i)).getConditionalDestination().getType()==0||
				((ConditionalNode)nodes_.get(i)).getConditionalDestination().getType()==1)){
					((ConditionalNode)nodes_.get(i)).
					setConditionalDestination(((ConditionalNode)nodes_.get(i))
							.getConditionalDestination().getDestination(null));
				}
				if(((ConditionalNode)nodes_.get(i)).getNaturalDestination()!=null){
					while(!(((ConditionalNode)nodes_.get(i)).getNaturalDestination().getType()==0||
							((ConditionalNode)nodes_.get(i)).getNaturalDestination().getType()==1)){
								((ConditionalNode)nodes_.get(i)).
								setNaturalDestination(((ConditionalNode)nodes_.get(i))
										.getNaturalDestination().getDestination(null));
					}
				}
			}
			else{
				if(nodes_.get(i).getDestination(null)!=null){
					while(!(nodes_.get(i).getDestination(null).getType()==0||
							nodes_.get(i).getDestination(null).getType()==1)){
						nodes_.get(i).setNaturalDestination(nodes_.get(i).getDestination(null).getDestination(null));
					}
				}
			}
		}
		
		for(int i=0;i<nodes_.size();i++){
			if (nodes_.get(i).getType()==2||nodes_.get(i).getType()==3){
				nodes_.remove(i);
			}
		}
		nodes_.add(0, new Node(4));
		nodes_.get(0).setNaturalDestination(nodes_.get(1));
		nodes_.add(new Node(5));
		nodes_.get(nodes_.size()-2).setNaturalDestination(nodes_.get(nodes_.size()-1));
	}
	
	/**
	 * Returns all the paths of the graph.
	 * @return ArrayList of paths.
	 */
	public ArrayList<ArrayList<Integer>> getPaths(){
		return paths_;
	}
	
	/**
	 * Returns all the cycles of the graph.
	 * @return ArrayList of cycles.
	 */
	public ArrayList<ArrayList<Integer>> getCycles(){
		return cycles_;
	}
	/**
	 * Return all the nodes of the graph.
	 * @return ArrayList of Node objects.
	 */
	public ArrayList<Node> getNodes(){
		return nodes_;
	}
	/**
	 * Calculates all the paths and cycles in the graph.
	 */
	public void calculatePaths(){
		ArrayList<Integer> path=new ArrayList<Integer>();
		path.add(new Integer(0));
		this.addPath(path);
	}
	/**
	 * Builds possible paths deriving from given.
	 * @param path base path.
	 */
	private void addPath(ArrayList<Integer> path){
		if(nodes_.get(path.get(path.size()-1)).getType()==5){
			paths_.add(path);
		}
		else{ 
			boolean isCycle=false;
			for(int i=0;i<path.size()-1;i++){
				if(path.get(i)==path.get(path.size()-1)){
					for(int j=0;j<i;j++){
						path.remove(0);
					}
					cycles_.add(path);
					isCycle=true;
					break;
				}
			}
			if(!isCycle){
				if(nodes_.get(path.get(path.size()-1)).getType()==1){
					ArrayList<Integer> condPath=new ArrayList<Integer>();
					for(int i=0;i<path.size();i++){
						condPath.add(path.get(i).intValue());
					}
					condPath.add(nodes_.indexOf(((ConditionalNode)nodes_.
							get(condPath.get(condPath.size()-1))).getConditionalDestination()));
					this.addPath(condPath);
				}
				
				path.add(nodes_.indexOf((nodes_.get(path.get(path.size()-1)).getDestination(null))));
				this.addPath(path);
			}
		}
	}
	
}
