package data;
import java.util.ArrayList;


public class QuineMcCluskeyMinimizer {
	private ArrayList<Integer[]> terms_;
	
	
	
	
	public ArrayList<Integer[]> minimize(ArrayList<Integer[]> starting_function){
		terms_=new ArrayList<Integer[]>(starting_function);
		
		//Concatenate all possible terms
		for(int i=0;i<terms_.size();i++){
			for(int j=0;j<terms_.size();j++){
				if(check_concatenation(terms_.get(i),terms_.get(j))){
					terms_.add(concatenate(terms_.get(i), terms_.get(j)));
				}
			}
		}
		
		//Find prime terms
		
		boolean[] isConsumed=new boolean[terms_.size()];
		for(int i=0;i<terms_.size();i++){
			isConsumed[i]=false;
		}
		
		for(int i=terms_.size()-1;i>=0;i--){
			for(int j=0;j<terms_.size();j++){
				if(check_consumption(terms_.get(i), terms_.get(j))&&(i!=j)){
					isConsumed[j]=true;
				}
			}
		}
		
		ArrayList<Integer[]> temp=new ArrayList<Integer[]>();
		for(int i=0;i<terms_.size();i++){
			if(!isConsumed[i]){
				temp.add(terms_.get(i));
			}
		}
		terms_=temp;
		
		
		
		int z=0;
		while(z<terms_.size()){
			boolean isDuplicate=false;
			for(int j=0;j<z;j++){
				isDuplicate=true;
				for(int k=0;k<terms_.get(z).length;k++){
					if(terms_.get(j)[k]!=terms_.get(z)[k]){
						isDuplicate=false;
						break;
					}
				}
				if(isDuplicate){
					break;
				}
			}
			if(isDuplicate){
				terms_.remove(z);
			}
			else{
				z++;
			}
		}
		
		
		
		//Find minimal coverage.
		
		boolean[][] isCovering=new boolean[terms_.size()][starting_function.size()];
		for(int i=0;i<terms_.size();i++){
			for(int j=0;j<starting_function.size();j++){
				isCovering[i][j]=false;
				if(check_coverage(terms_.get(i), starting_function.get(j))){
					isCovering[i][j]=true;
				}
			}
		}
		
		ArrayList<Integer[]> result=new ArrayList<Integer[]>();
		
		Integer[] numOfCoveringTerms=new Integer[starting_function.size()];
		Integer[] posOfTerm=new Integer[starting_function.size()];
		for(int i=0;i<starting_function.size();i++){
			numOfCoveringTerms[i]=0;
		}
		
		for(int i=0;i<starting_function.size();i++){
			for(int j=0;j<terms_.size();j++){
				if(isCovering[j][i]){
					numOfCoveringTerms[i]++;
					posOfTerm[i]=j;
				}
			}
		}
		
		boolean[] isUsed=new boolean[terms_.size()];
		for(int i=0;i<terms_.size();i++){
			isUsed[i]=false;
		}
		
		
		for(int i=0;i<starting_function.size();i++){
			if(numOfCoveringTerms[i]==1){
				if(!isUsed[posOfTerm[i]]){
					result.add(terms_.get(posOfTerm[i]));
					isUsed[posOfTerm[i]]=true;
				}
			}
		}
		
		boolean[] covered=new boolean[starting_function.size()];
		for(int i=0;i<starting_function.size();i++){
			covered[i]=false;
		}
		for(int i=0;i<terms_.size();i++){
			if(isUsed[i]){
				for(int k=0;k<starting_function.size();k++){
					if(isCovering[i][k]){
						covered[k]=true;
					}
				}
			}
		}
		
		
		
		boolean allCovered=true;
		for(int i=0;i<covered.length;i++){
			 if(covered[i]==false){
				 allCovered=false;
				 break;
			 }
		}
		
		while(!allCovered){
			int next_term=0;
			int most_covered=0;
			for(int i=0;i<terms_.size();i++){
				if(!isUsed[i]){
					int temp_count=0;
					for(int j=0;j<starting_function.size();j++){
						if(isCovering[i][j]==true&&covered[j]==false){
							temp_count++;
						}
					}
					if(temp_count>most_covered){
						most_covered=temp_count;
						next_term=i;
					}
				}
			}
			
			result.add(terms_.get(next_term));
			for(int i=0;i<starting_function.size();i++){
				if(isCovering[next_term][i]){
					covered[i]=true;
				}
			}
			
			allCovered=true;
			for(int i=0;i<covered.length;i++){
				 if(covered[i]==false){
					 allCovered=false;
					 break;
				}
			}
		
		}
		
		return result;
	}
	
	
	/**
	 * Checks if two terms are eligible for concatenation.
	 * @param L left term.
	 * @param R right term.
	 * @return true if terms are eligible,false otherwise.
	 */
	private boolean check_concatenation(Integer[] L, Integer[] R){
		int non_significant_l=0,non_significant_r=0;
		int different=0;
		for(int i=0;i<L.length;i++){
			if(L[i]==-1){
				non_significant_l++;
			}
			if(R[i]==-1){
				non_significant_r++;
				
			}
			if(L[i]!=R[i]){
				if(L[i]==-1 || R[i]==-1){
					return false;
				}
				else{
					different++;
				}
			}
		}
		if (different==1){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * Concatenates two terms.
	 * @param R left term.
	 * @param L right term.
	 * @return concatenated term.
	 */
	private Integer[] concatenate(Integer[] R,Integer[] L)
	{
		Integer[] result=new Integer[R.length];
		for(int i=0;i<R.length;i++){
			if(R[i]==L[i]){
				result[i]=R[i];
			}
			else{
				result[i]=-1;
			}
		}
		return result;
	}
	
	/**
	 * Checks if one terms can be consumed by the other.
	 * @param consumer the term which will consume.
	 * @param consumee the term which will be consumed.
	 * @return true if conditions apply, false otherwise.
	 */
	private boolean check_consumption(Integer[] consumer, Integer[] consumee){
		int dif=0;
		for(int i=0;i<consumer.length;i++){
			if(consumer[i]!=consumee[i]){
				dif++;
			}
			if(!((consumer[i]==-1&&consumee[i]!=-1)||(consumer[i]==consumee[i]))){
				return false;
			}
		}
		if(dif!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean check_coverage(Integer[] consumer, Integer[] consumee){
		for(int i=0;i<consumer.length;i++){
			if(!((consumer[i]==-1&&consumee[i]!=-1)||(consumer[i]==consumee[i]))){
				return false;
			}
		}
		return true;
	}
	
}
