package data;

/**
 * Mealy machine edge class
 * @author anthony
 *
 */
public class MealyEdge {
	/**
	 * Transition conditions.
	 */
	private String conditions_;
	/**
	 * Emited signals
	 */
	private String signals_;
	
	
	private MealyNode start_;
	private MealyNode end_;
	
	/**
	 * Edge id.
	 */
	private int id_;
	public MealyEdge(String conditions,String signals,int id){
		conditions_=conditions;
		signals_=signals;
		id_=id;
	}
	/**
	 * Returns transition conditions.
	 * @return conditions string.
	 */
	public String getConditions(){
		return conditions_;
	}
	
	/**
	 * Returns emited signals.
	 * @return signals string.
	 */
	public String getSignals(){
		return signals_;
	}
	
	/**
	 * Sets emited signals.
	 * @param S signals string.
	 */
	public void setSignals(String S){
		signals_=S;
	}
	/**
	 * Sets transition signals.
	 * @param S signals string.
	 */
	public void setConditions(String S){
		conditions_=S;
	}
	
	public int getID(){
		return id_;
	}
	public MealyEdge clone(int id){
		return new MealyEdge(conditions_,signals_,id);
	}
	
	public void setStart(MealyNode s){
		start_=s;
	}
	public void setEnd(MealyNode e){
		end_=e;
	}
	
	public MealyNode getStart(){
		return start_;
	}
	public MealyNode getEnd(){
		return end_;
	}
	
}
