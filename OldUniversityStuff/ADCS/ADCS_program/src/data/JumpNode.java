package data;

public class JumpNode extends Node {

	/**
	 * Serialization UID.
	 */
	private static final long serialVersionUID = 4304924916012303762L;
	/**
	 * Jump point ID used to determine node destination.
	 */
	private int id_;

	/**
	 * Constructor that specifies jump point ID.
	 * 
	 * @param id
	 */
	public JumpNode(int id) {
		super(2);
		id_ = id;
	}

	/**
	 * Returns jump point ID.
	 * 
	 * @return ID integer.
	 */
	public int getID() {
		return id_;
	}
}
