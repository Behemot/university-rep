package data;

public class LabelNode extends Node {

	/**
	 * Serialization UID
	 */
	private static final long serialVersionUID = -4715817410042105250L;
	/**
	 * Label point ID used to determine node source.
	 */
	private int id_;

	/**
	 * Constructor that specifies label point ID.
	 * 
	 * @param id
	 */
	public LabelNode(int id) {
		super(3);
		id_ = id;
		
	}

	/**
	 * Returns label point ID.
	 * 
	 * @return ID integer.
	 */
	public int getID() {
		return id_;
	}

}
