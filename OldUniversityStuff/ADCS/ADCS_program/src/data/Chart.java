package data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Chart class.
 * 
 * @author anthony
 * 
 */
public class Chart implements Serializable {

	/**
	 * Serialization ID.
	 */
	private static final long serialVersionUID = -4057645912437190657L;
	/**
	 * All the nodes in the chart.
	 */
	private ArrayList<Node> nodes_;

	/**
	 * Basic constructor.
	 * 
	 * @param nodes all the nodes in the new chart.
	 */
	public Chart(ArrayList<Node> nodes) {
		nodes_ = nodes;
	}

	public Chart() {
		nodes_ = new ArrayList<Node>();
	}

	/**
	 * Returns all the nodes in the chart.
	 * 
	 * @return ArrayList of nodes.
	 */
	public ArrayList<Node> getNodes() {
		return nodes_;
	}

	/**
	 * Prints chart in string form.
	 */
	@Override
	public String toString() {
		String S = new String();
		for (int i = 0; i < nodes_.size(); i++) {
			switch (nodes_.get(i).getType()) {
			case 0: {
				S = S + "(";
				ArrayList<String> signals = ((OperationalNode) nodes_.get(i))
						.getOperationalSignals();
				for (int j = 0; j < signals.size() - 1; j++) {
					S += signals.get(i) + ",";
				}
				S += signals.get(signals.size() - 1);
				S = S + ")";
			}
				;
				break;
			case 1: {
				S = S + "(";
				ArrayList<String> signals = ((ConditionalNode) nodes_.get(i))
						.getConditionalSignals();
				for (int j = 0; j < signals.size() - 1; j++) {
					S += signals.get(j) + ",";
				}
				S += signals.get(signals.size() - 1);
				S = S + ")";
			}
				;
				break;
			case 2: {
				S = S + "(j" + ((JumpNode) nodes_.get(i)).getID() + ")";
			}
				;
				break;
			case 3: {
				S = S + "(l" + ((LabelNode) nodes_.get(i)).getID() + ")";
			}
				;
				break;
			}
		}
		return S;
	}
}
