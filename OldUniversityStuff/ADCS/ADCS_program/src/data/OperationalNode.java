package data;

import java.util.ArrayList;

/**
 * Operational node class.
 * 
 * @author anthony
 * 
 */
public class OperationalNode extends Node {
	/**
	 * Serialization UID.
	 */
	private static final long serialVersionUID = 4837625599049494514L;
	/**
	 * Array of operational signals the node sends.
	 */
	private ArrayList<String> operationalSignals_;

	/**
	 * Basic constructor.
	 * 
	 * @param signals
	 *            ArrayList of operational signals that are sent by the node.
	 */
	public OperationalNode(ArrayList<String> signals) {
		super(0);
		operationalSignals_ = signals;
	}

	/**
	 * Returns operational signals the node is carrying.
	 * 
	 * @return ArrayList of String representing signals.
	 */
	public ArrayList<String> getOperationalSignals() {
		return operationalSignals_;
	}
}
