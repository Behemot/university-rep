package fileSystem;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import att.grappa.Graph;
import att.grappa.Parser;

public class DotStream {
	public void write(File F,Graph g){
		try{
			g.printGraph(new FileWriter(F));
		}
		catch(IOException IO){
			System.out.println("Error while writing DOT file");
		}
	}
	
	public Graph read(File F){
		Graph g=new Graph("");
		try{
			
			Parser p=new Parser(new FileInputStream(F),(OutputStream)System.out,g); 
			p.parse();
		}
		catch(FileNotFoundException FNF){
			System.out.println("File not found");
		}
		catch(Exception E){
			
		}
		return g;
	}
	
}
