package fileSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import data.Chart;
import data.TriggersSetupTable;
import java.util.ArrayList;

public class Serializer {
	/**
	 * Method that serializes chart to file.
	 * 
	 * @param outFile
	 *            target file.
	 * @param c
	 *            chart to be serialized.
	 * @return true if serialization was successful, false otherwise.
	 */
	public boolean serialize(File outFile, Chart c) {

		try {
			if (outFile.createNewFile() == false) {
				outFile.delete();
				outFile.createNewFile();
			}
		} catch (SecurityException SE) {
			System.out.println("Unable to access disk");
			return false;
		} catch (IOException IOe) {
			System.out
					.println("IO Exception while creating serialization file");
			return false;
		}

		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(outFile));
			out.writeObject(c);
			out.close();
		} catch (IOException IOe) {
			System.out.println("IO Exception while serializing");
			return false;
		}

		return true;
	}

	/**
	 * Method that deserializes NetManager object from file.
	 * 
	 * @param outFile
	 *            deserialization file;
	 * @return Chart instance or null if deserialization failed.
	 */
	public Chart deserialize(File outFile) {
		Chart net_;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					outFile));
			net_ = (Chart) in.readObject();
			in.close();
		} catch (FileNotFoundException FileNotFound) {
			System.out.println("Temporary file not found");
			return null;
		} catch (IOException IOe) {
			System.out.println("Unable to restore data");
			return null;
		} catch (ClassNotFoundException ClassNotFound) {
			System.out.println("Wrong file");
			return null;
		}

		return net_;

	}
	
	/**
	 * Method that serializes chart to file.
	 * 
	 * @param outFile
	 *            target file.
	 * @param c
	 *            chart to be serialized.
	 * @return true if serialization was successful, false otherwise.
	 */
	public boolean serializeTable(File outFile, TriggersSetupTable c) {

		try {
			if (outFile.createNewFile() == false) {
				outFile.delete();
				outFile.createNewFile();
			}
		} catch (SecurityException SE) {
			System.out.println("Unable to access disk");
			return false;
		} catch (IOException IOe) {
			System.out
					.println("IO Exception while creating serialization file");
			return false;
		}

		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(outFile));
			out.writeObject(c);
			out.close();
		} catch (IOException IOe) {
			System.out.println("IO Exception while serializing");
			return false;
		}

		return true;
	}

	/**
	 * Method that deserializes NetManager object from file.
	 * 
	 * @param outFile
	 *            deserialization file;
	 * @return Chart instance or null if deserialization failed.
	 */
	public TriggersSetupTable deserializeTable(File outFile) {
		TriggersSetupTable net_;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					outFile));
			net_ = (TriggersSetupTable) in.readObject();
			in.close();
		} catch (FileNotFoundException FileNotFound) {
			System.out.println("Temporary file not found");
			return null;
		} catch (IOException IOe) {
			System.out.println("Unable to restore data");
			return null;
		} catch (ClassNotFoundException ClassNotFound) {
			System.out.println("Wrong file");
			return null;
		}

		return net_;

	}

	
	
	public boolean serializeFunctions(File outFile, ArrayList<ArrayList<ArrayList<Integer[]>>> c) {
		System.out.println("IN FUNCTION: "+c.get(1).size());
		try {
			if (outFile.createNewFile() == false) {
				outFile.delete();
				outFile.createNewFile();
			}
		} catch (SecurityException SE) {
			System.out.println("Unable to access disk");
			return false;
		} catch (IOException IOe) {
			System.out
					.println("IO Exception while creating serialization file");
			return false;
		}

		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(outFile));
			out.writeObject(c.size());
			for(int i=0;i<c.size();i++){
				out.writeObject(c.get(i).size());
			}
			for(int i=0;i<c.size();i++){
				for(int j=0;j<c.get(i).size();j++){
					out.writeObject(c.get(i).get(j).size());
				}
			}
			for(int i=0;i<c.size();i++){
				for(int j=0;j<c.get(i).size();j++){
					for(int k=0;k<c.get(i).get(j).size();k++){
						out.writeObject(c.get(i).get(j).get(k));
					}
				}
			}
			out.close();
		} catch (IOException IOe) {
			System.out.println("IO Exception while serializing");
			return false;
		}

		return true;
	}


	public ArrayList<ArrayList<ArrayList<Integer[]>>> deserializeFunctions(File outFile) {
		ArrayList<ArrayList<ArrayList<Integer[]>>> net_=new ArrayList<ArrayList<ArrayList<Integer[]>>>();
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					outFile));
			int L1Size=(Integer)in.readObject();
			int[] L2Sizes=new int[L1Size];
			for(int i=0;i<L1Size;i++){
				L2Sizes[i]=(Integer)in.readObject();
			}
			ArrayList<ArrayList<Integer>> L3Sizes=new ArrayList<ArrayList<Integer>>();
			for(int i=0;i<L1Size;i++){
				L3Sizes.add(new ArrayList<Integer>());
				for(int j=0;j<L2Sizes[i];j++){
					L3Sizes.get(i).add((Integer)in.readObject());
				}
			}
			for(int i=0;i<L1Size;i++){
				net_.add(new ArrayList<ArrayList<Integer[]>>());
				for(int j=0;j<L2Sizes[i];j++){
					net_.get(i).add(new ArrayList<Integer[]>());
					for(int k=0;k<L3Sizes.get(i).get(j);k++){
						net_.get(i).get(j).add((Integer[])in.readObject());
					}
				}
			}
				in.close();
		} catch (FileNotFoundException FileNotFound) {
			System.out.println("Temporary file not found");
			return null;
		} catch (IOException IOe) {
			System.out.println("Unable to restore data");
			return null;
		} catch (ClassNotFoundException ClassNotFound) {
			System.out.println("Wrong file");
			return null;
		}
		return net_;

	}
	
	public void saveVHDL(File F,String S ){
		try {
			if (F.createNewFile() == false) {
				F.delete();
				F.createNewFile();
			}
		} catch (SecurityException SE) {
			System.out.println("Unable to access disk");
		} catch (IOException IOe) {
			System.out
					.println("IO Exception while creating serialization file");
		}

		try {
			BufferedWriter out = new BufferedWriter(
					new FileWriter(F));
			out.write(S, 0, S.length());
			out.newLine();
			out.close();
		} catch (IOException IOe) {
			System.out.println("IO Exception while serializing");
		}
	}

	
}
