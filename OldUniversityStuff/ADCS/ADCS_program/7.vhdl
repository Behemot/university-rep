library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Clock is
port(Output: out BIT);
end Clock;

architecture Clock of Clock is
 	signal t: BIT;
begin
	Output<= not t after 1 ns;
	t<= not t after 1 ns;
end Clock;

entity var0 is
port(Output: out BIT);
end var0;

architecture var0 of var0 is
 	signal t: BIT;
begin
	Output<= not t after 100 ns;
	t<= not t after 100 ns;
end var0;

entity var1 is
port(Output: out BIT);
end var1;

architecture var1 of var1 is
 	signal t: BIT;
begin
	Output<= not t after 100 ns;
	t<= not t after 100 ns;
end var1;

entity var2 is
port(Output: out BIT);
end var2;

architecture var2 of var2 is
 	signal t: BIT;
begin
	Output<= not t after 100 ns;
	t<= not t after 100 ns;
end var2;

entity JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end JK;
architecture JK of JK is
 	signal temp: BIT;
begin
	process(Clock)
	begin
		if Clock'event and Clock='1' then 
			if Reset='1' then
				temp <= '0';
			else 
				if(J='0' and K='0') then 
					temp<=temp;
				elsif(J='0' and K='1') then
					temp<='0';
				elsif(J='1' and K='0') then
					temp<='1';
				elsif(J='1' and K='1') then
					temp<=not(temp);
				end if;
			end if;
		end if;
	end process;
	Output<=temp;
end JK;

entity AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end AND2;

architecture AND2 of AND2 is
 begin
	Output<=in1 and in2;
end AND2;

entity OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end OR2;

architecture OR2 of OR2 is
 begin
	Output<=in1 or in2;
end OR2;

entity NOT1 is
port(Input: in BIT;
	Output: out BIT);
end NOT1;

architecture NOT1 of NOT1 is
 begin
	Output<= not(Input);
end NOT1;

entity y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y0;

architecture y0 of y0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in3,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in4,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in5,S9);
E10: AND2 port map (S8,S9,S10);
E11: NOT1 port map (in6,S11);
E12: AND2 port map (S10,S11,S12);
E13: Output <=S12;
end y0;

entity y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y1;

architecture y1 of y1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: AND2 port map (S0,in1,S1);
E2: NOT1 port map (in2,S2);
E3: AND2 port map (S1,S2,S3);
E4: NOT1 port map (in3,S4);
E5: AND2 port map (S3,S4,S5);
E6: NOT1 port map (in4,S6);
E7: AND2 port map (S5,S6,S7);
E8: NOT1 port map (in5,S8);
E9: AND2 port map (S7,S8,S9);
E10: NOT1 port map (in6,S10);
E11: AND2 port map (S9,S10,S11);
E12: Output <=S11;
end y1;

entity y2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y2;

architecture y2 of y2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: NOT1 port map (in2,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in3,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in5,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in6,S9);
E10: AND2 port map (S8,S9,S10);
E11: NOT1 port map (in8,S11);
E12: AND2 port map (S10,S11,S12);
E13: Output <=S12;
end y2;

entity y3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y3;

architecture y3 of y3 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: NOT1 port map (in3,S2);
E3: AND2 port map (S1,S2,S3);
E4: NOT1 port map (in4,S4);
E5: AND2 port map (S3,S4,S5);
E6: NOT1 port map (in5,S6);
E7: AND2 port map (S5,S6,S7);
E8: NOT1 port map (in6,S8);
E9: AND2 port map (S7,S8,S9);
E10: Output <=S9;
end y3;

entity y4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y4;

architecture y4 of y4 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: NOT1 port map (in4,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in5,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in6,S7);
E8: AND2 port map (S6,S7,S8);
E9: AND2 port map (S8,in9,S9);
E10: Output <=S9;
end y4;

entity y5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end y5;

architecture y5 of y5 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17,S18,S19: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: NOT1 port map (in4,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in5,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in6,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in9,S9);
E10: AND2 port map (S8,S9,S10);
E11: AND2 port map (in0,in1,S11);
E12: AND2 port map (S11,in2,S12);
E13: AND2 port map (S12,in3,S13);
E14: AND2 port map (S13,in4,S14);
E15: NOT1 port map (in5,S15);
E16: AND2 port map (S14,S15,S16);
E17: NOT1 port map (in6,S17);
E18: AND2 port map (S16,S17,S18);
E19: OR2 port map (S10,S18,S19);
E20: Output <=S19;
end y5;

entity j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j0;

architecture j0 of j0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: AND2 port map (S0,in1,S1);
E2: NOT1 port map (in2,S2);
E3: AND2 port map (S1,S2,S3);
E4: NOT1 port map (in3,S4);
E5: AND2 port map (S3,S4,S5);
E6: NOT1 port map (in4,S6);
E7: AND2 port map (S5,S6,S7);
E8: NOT1 port map (in5,S8);
E9: AND2 port map (S7,S8,S9);
E10: NOT1 port map (in6,S10);
E11: AND2 port map (S9,S10,S11);
E12: Output <=S11;
end j0;

entity k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k0;

architecture k0 of k0 is
begin
	E0: Output <= '0';
end k0;

entity j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j1;

architecture j1 of j1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in3,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in4,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in5,S9);
E10: AND2 port map (S8,S9,S10);
E11: NOT1 port map (in6,S11);
E12: AND2 port map (S10,S11,S12);
E13: Output <=S12;
end j1;

entity k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k1;

architecture k1 of k1 is
begin
	E0: Output <= '0';
end k1;

entity j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j2;

architecture j2 of j2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: NOT1 port map (in2,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in3,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in5,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in6,S9);
E10: AND2 port map (S8,S9,S10);
E11: NOT1 port map (in8,S11);
E12: AND2 port map (S10,S11,S12);
E13: Output <=S12;
end j2;

entity k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k2;

architecture k2 of k2 is
begin
	E0: Output <= '0';
end k2;

entity j3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j3;

architecture j3 of j3 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: NOT1 port map (in3,S2);
E3: AND2 port map (S1,S2,S3);
E4: NOT1 port map (in4,S4);
E5: AND2 port map (S3,S4,S5);
E6: NOT1 port map (in5,S6);
E7: AND2 port map (S5,S6,S7);
E8: NOT1 port map (in6,S8);
E9: AND2 port map (S7,S8,S9);
E10: Output <=S9;
end j3;

entity k3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k3;

architecture k3 of k3 is
begin
	E0: Output <= '0';
end k3;

entity j4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j4;

architecture j4 of j4 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17,S18: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: NOT1 port map (in4,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in5,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in6,S7);
E8: AND2 port map (S6,S7,S8);
E9: AND2 port map (S8,in9,S9);
E10: AND2 port map (in0,in1,S10);
E11: AND2 port map (S10,in2,S11);
E12: AND2 port map (S11,in3,S12);
E13: NOT1 port map (in4,S13);
E14: AND2 port map (S12,S13,S14);
E15: NOT1 port map (in5,S15);
E16: AND2 port map (S14,S15,S16);
E17: AND2 port map (S16,in6,S17);
E18: OR2 port map (S9,S17,S18);
E19: Output <=S18;
end j4;

entity k4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k4;

architecture k4 of k4 is
begin
	E0: Output <= '0';
end k4;

entity j5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j5;

architecture j5 of j5 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: AND2 port map (S2,in4,S3);
E4: NOT1 port map (in5,S4);
E5: AND2 port map (S3,S4,S5);
E6: Output <=S5;
end j5;

entity k5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k5;

architecture k5 of k5 is
begin
	E0: Output <= '0';
end k5;

entity j6 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end j6;

architecture j6 of j6 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: NOT1 port map (in4,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in5,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in6,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in9,S9);
E10: AND2 port map (S8,S9,S10);
E11: Output <=S10;
end j6;

entity k6 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end k6;

architecture k6 of k6 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: AND2 port map (S1,in3,S2);
E3: AND2 port map (S2,in4,S3);
E4: AND2 port map (S3,in5,S4);
E5: AND2 port map (S4,in6,S5);
E6: Output <=S5;
end k6;

entity Main is
	port (
		R: in BIT;
		C: in BIT;
		x0: in BIT;
		x1: in BIT;
		x2: in BIT;
		out0: out BIT;
		out1: out BIT;
		out2: out BIT;
		out3: out BIT;
		out4: out BIT;
		out5: out BIT
	);
end Main;

architecture Main of Main is
component y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component y2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component y3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component y4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component y5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k3 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k4 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k5 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component j6 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component k6 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		in6: in BIT;
		in7: in BIT;
		in8: in BIT;
		in9: in BIT;
		Output: out BIT
	);
end component;

component JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end component;

component Clock is
port(Output: out BIT);
end component;

component var0 is
port(Output: out BIT);
end component;

component var1 is
port(Output: out BIT);
end component;

component var2 is
port(Output: out BIT);
end component;

signal ys0,ys1,ys2,ys3,ys4,ys5,js0,js1,js2,js3,js4,js5,js6,ks0,ks1,ks2,ks3,ks4,ks5,ks6,qs0,qs1,qs2,qs3,qs4,qs5,qs6,Cs,xs0,xs1,xs2: BIT;
begin
E0: y0 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys0);
E1: y1 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys1);
E2: y2 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys2);
E3: y3 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys3);
E4: y4 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys4);
E5: y5 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ys5);
E6: j0 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js0);
E7: j1 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js1);
E8: j2 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js2);
E9: j3 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js3);
E10: j4 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js4);
E11: j5 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js5);
E12: j6 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,js6);
E13: k0 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks0);
E14: k1 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks1);
E15: k2 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks2);
E16: k3 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks3);
E17: k4 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks4);
E18: k5 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks5);
E19: k6 port map (qs0,qs1,qs2,qs3,qs4,qs5,qs6,xs0,xs1,xs2,ks6);
E20: JK port map(js0,ks0,R,Cs,qs0);
E21: JK port map(js1,ks1,R,Cs,qs1);
E22: JK port map(js2,ks2,R,Cs,qs2);
E23: JK port map(js3,ks3,R,Cs,qs3);
E24: JK port map(js4,ks4,R,Cs,qs4);
E25: JK port map(js5,ks5,R,Cs,qs5);
E26: JK port map(js6,ks6,R,Cs,qs6);
E27: out0 <= ys0;
E28: out1 <= ys1;
E29: out2 <= ys2;
E30: out3 <= ys3;
E31: out4 <= ys4;
E32: out5 <= ys5;
E33: Clock port map (Cs);
E34: var0 port map (xs0);
E35: var1 port map (xs1);
E36: var2 port map (xs2);
end Main;


