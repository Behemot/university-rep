library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Clock is
port(Output: out BIT);
end Clock;

architecture Clock of Clock is
 	signal t: BIT;
begin
	Output<= not t after 1 ns;
	t<= not t after 1 ns;
end Clock;

entity var0 is
port(Output: out BIT);
end var0;

architecture var0 of var0 is
 	signal t: BIT;
begin
	Output<= not t after 10 ns;
	t<= not t after 10 ns;
end var0;

entity var1 is
port(Output: out BIT);
end var1;

architecture var1 of var1 is
 	signal t: BIT;
begin
	Output<= not t after 20 ns;
	t<= not t after 20 ns;
end var1;

entity var2 is
port(Output: out BIT);
end var2;

architecture var2 of var2 is
 	signal t: BIT;
begin
	Output<= not t after 30 ns;
	t<= not t after 30 ns;
end var2;

entity JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end JK;
architecture JK of JK is
 	signal temp: BIT;
begin
	process(Clock)
	begin
		if Clock'event and Clock='1' then 
			if Reset='1' then
				temp <= '0';
			else 
				if(J='0' and K='0') then 
					temp<=temp;
				elsif(J='0' and K='1') then
					temp<='0';
				elsif(J='1' and K='0') then
					temp<='1';
				elsif(J='1' and K='1') then
					temp<=not(temp);
				end if;
			end if;
		end if;
	end process;
	Output<=temp;
end JK;

entity AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end AND2;

architecture AND2 of AND2 is
 begin
	Output<=in1 and in2;
end AND2;

entity OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end OR2;

architecture OR2 of OR2 is
 begin
	Output<=in1 or in2;
end OR2;

entity NOT1 is
port(Input: in BIT;
	Output: out BIT);
end NOT1;

architecture NOT1 of NOT1 is
 begin
	Output<= not(Input);
end NOT1;

entity y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end y0;

architecture y0 of y0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in5,S7);
E8: AND2 port map (S6,S7,S8);
E9: NOT1 port map (in1,S9);
E10: AND2 port map (in0,S9,S10);
E11: NOT1 port map (in2,S11);
E12: AND2 port map (S10,S11,S12);
E13: NOT1 port map (in5,S13);
E14: AND2 port map (S12,S13,S14);
E15: OR2 port map (S8,S14,S15);
E16: Output <=S15;
end y0;

entity y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end y1;

architecture y1 of y1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: AND2 port map (S6,in5,S7);
E8: NOT1 port map (in1,S8);
E9: AND2 port map (in0,S8,S9);
E10: NOT1 port map (in2,S10);
E11: AND2 port map (S9,S10,S11);
E12: AND2 port map (S11,in5,S12);
E13: OR2 port map (S7,S12,S13);
E14: Output <=S13;
end y1;

entity j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end j0;

architecture j0 of j0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: AND2 port map (S6,in5,S7);
E8: NOT1 port map (in0,S8);
E9: NOT1 port map (in1,S9);
E10: AND2 port map (S8,S9,S10);
E11: AND2 port map (S10,in2,S11);
E12: OR2 port map (S7,S11,S12);
E13: Output <=S12;
end j0;

entity k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end k0;

architecture k0 of k0 is
begin
	E0: Output <= '0';
end k0;

entity j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end j1;

architecture j1 of j1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9: BIT;
begin
E0: NOT1 port map (in1,S0);
E1: AND2 port map (in0,S0,S1);
E2: NOT1 port map (in2,S2);
E3: AND2 port map (S1,S2,S3);
E4: NOT1 port map (in5,S4);
E5: AND2 port map (S3,S4,S5);
E6: NOT1 port map (in1,S6);
E7: AND2 port map (in0,S6,S7);
E8: AND2 port map (S7,in2,S8);
E9: OR2 port map (S5,S8,S9);
E10: Output <=S9;
end j1;

entity k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end k1;

architecture k1 of k1 is
begin
	E0: Output <= '0';
end k1;

entity j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end j2;

architecture j2 of j2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: NOT1 port map (in4,S5);
E6: AND2 port map (S4,S5,S6);
E7: NOT1 port map (in5,S7);
E8: AND2 port map (S6,S7,S8);
E9: Output <=S8;
end j2;

entity k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end k2;

architecture k2 of k2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: AND2 port map (S0,in2,S1);
E2: Output <=S1;
end k2;

entity Main is
	port (
		R: in BIT;
		C: in BIT;
		x0: in BIT;
		x1: in BIT;
		x2: in BIT;
		out0: out BIT;
		out1: out BIT
	);
end Main;

architecture Main of Main is
component y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		in4: in BIT;
		in5: in BIT;
		Output: out BIT
	);
end component;

component JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end component;

component Clock is
port(Output: out BIT);
end component;

component var0 is
port(Output: out BIT);
end component;

component var1 is
port(Output: out BIT);
end component;

component var2 is
port(Output: out BIT);
end component;

signal ys0,ys1,js0,js1,js2,ks0,ks1,ks2,qs0,qs1,qs2,Cs,xs0,xs1,xs2: BIT;
begin
E0: y0 port map (qs0,qs1,qs2,xs0,xs1,xs2,ys0);
E1: y1 port map (qs0,qs1,qs2,xs0,xs1,xs2,ys1);
E2: j0 port map (qs0,qs1,qs2,xs0,xs1,xs2,js0);
E3: j1 port map (qs0,qs1,qs2,xs0,xs1,xs2,js1);
E4: j2 port map (qs0,qs1,qs2,xs0,xs1,xs2,js2);
E5: k0 port map (qs0,qs1,qs2,xs0,xs1,xs2,ks0);
E6: k1 port map (qs0,qs1,qs2,xs0,xs1,xs2,ks1);
E7: k2 port map (qs0,qs1,qs2,xs0,xs1,xs2,ks2);
E8: JK port map(js0,ks0,R,Cs,qs0);
E9: JK port map(js1,ks1,R,Cs,qs1);
E10: JK port map(js2,ks2,R,Cs,qs2);
E11: out0 <= ys0;
E12: out1 <= ys1;
E13: Clock port map (Cs);
E14: var0 port map (xs0);
E15: var1 port map (xs1);
E16: var2 port map (xs2);
end Main;


