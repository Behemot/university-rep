package data;
import java.util.ArrayList;
/**
 * Mealy machine node.
 * @author anthony
 *
 */
public class MealyNode {
	public static boolean INCOMING=true;
	public static boolean OUTGOING=false;
	/**
	 * Node ID.
	 */
	int id_;
	/**
	 * Incoming edges.
	 */
	private ArrayList<MealyEdge> inEdges_=new ArrayList<MealyEdge>();
	/**
	 * Outgoing edges.
	 */
	private ArrayList<MealyEdge> outEdges_=new ArrayList<MealyEdge>();
	
	/**
	 * Creates node with specified id.
	 * @param id node identifier.
	 */
	public MealyNode(int id){
		id_=id;
	}
	
	/**
	 * Adds an incidental edge to the node.
	 * @param edge incidental MealyEdge.
	 * @param in true if the edge is incoming, fals if it's outgoing.
	 */
	public void addEdge(MealyEdge edge,boolean in){
		if(in){
			inEdges_.add(edge);
		}
		else{
			outEdges_.add(edge);
		}
	}
	
	/**
	 * Returns node identifier.
	 * @return node ID.
	 */
	public int getID(){
		return id_;
	}
	
	public boolean ifOutgoing(MealyEdge M){
		for(int i=0;i<outEdges_.size();i++){
			if(outEdges_.get(i).getID()==M.getID()){
				return true;
			}
		}
		return false;
	}
	
	public boolean ifIncoming(MealyEdge M){
		for(int i=0;i<inEdges_.size();i++){
			if(inEdges_.get(i).getID()==M.getID()){
				return true;
			}
		}
		return false;
	}
	
}
