library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Clock is
port(Output: out BIT);
end Clock;

architecture Clock of Clock is
 	signal t: BIT;
begin
	Output<= not t after 1 ns;
	t<= not t after 1 ns;
end Clock;

entity var0 is
port(Output: out BIT);
end var0;

architecture var0 of var0 is
 	signal t: BIT;
begin
	Output<= not t after 10 ns;
	t<= not t after 10 ns;
end var0;

entity JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end JK;
architecture JK of JK is
 	signal temp: BIT;
begin
	process(Clock)
	begin
		if Clock'event and Clock='1' then 
			if Reset='1' then
				temp <= '0';
			else 
				if(J='0' and K='0') then 
					temp<=temp;
				elsif(J='0' and K='1') then
					temp<='0';
				elsif(J='1' and K='0') then
					temp<='1';
				elsif(J='1' and K='1') then
					temp<=not(temp);
				end if;
			end if;
		end if;
	end process;
	Output<=temp;
end JK;

entity AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end AND2;

architecture AND2 of AND2 is
 begin
	Output<=in1 and in2;
end AND2;

entity OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end OR2;

architecture OR2 of OR2 is
 begin
	Output<=in1 or in2;
end OR2;

entity NOT1 is
port(Input: in BIT;
	Output: out BIT);
end NOT1;

architecture NOT1 of NOT1 is
 begin
	Output<= not(Input);
end NOT1;

entity y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end y0;

architecture y0 of y0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4,S5,S6,S7,S8,S9: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: AND2 port map (in0,in1,S5);
E6: NOT1 port map (in2,S6);
E7: AND2 port map (S5,S6,S7);
E8: AND2 port map (S7,in3,S8);
E9: OR2 port map (S4,S8,S9);
E10: Output <=S9;
end y0;

entity y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end y1;

architecture y1 of y1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: AND2 port map (S0,in1,S1);
E2: NOT1 port map (in2,S2);
E3: AND2 port map (S1,S2,S3);
E4: Output <=S3;
end y1;

entity y2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end y2;

architecture y2 of y2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: NOT1 port map (in2,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in3,S3);
E4: AND2 port map (S2,S3,S4);
E5: Output <=S4;
end y2;

entity j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end j0;

architecture j0 of j0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: AND2 port map (S0,in1,S1);
E2: NOT1 port map (in2,S2);
E3: AND2 port map (S1,S2,S3);
E4: Output <=S3;
end j0;

entity k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end k0;

architecture k0 of k0 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: NOT1 port map (in2,S1);
E2: AND2 port map (S0,S1,S2);
E3: AND2 port map (S2,in3,S3);
E4: Output <=S3;
end k0;

entity j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end j1;

architecture j1 of j1 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4: BIT;
begin
E0: NOT1 port map (in0,S0);
E1: NOT1 port map (in1,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in2,S3);
E4: AND2 port map (S2,S3,S4);
E5: Output <=S4;
end j1;

entity k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end k1;

architecture k1 of k1 is
begin
	E0: Output <= '0';
end k1;

entity j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end j2;

architecture j2 of j2 is
component OR2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component AND2 is
port(in1,in2: in BIT;
	Output: out BIT);
end component;

component NOT1 is
port(Input: in BIT;
	Output: out BIT);
end component;

signal S0,S1,S2,S3,S4: BIT;
begin
E0: AND2 port map (in0,in1,S0);
E1: NOT1 port map (in2,S1);
E2: AND2 port map (S0,S1,S2);
E3: NOT1 port map (in3,S3);
E4: AND2 port map (S2,S3,S4);
E5: Output <=S4;
end j2;

entity k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end k2;

architecture k2 of k2 is
begin
	E0: Output <= '0';
end k2;

entity Main is
	port (
		R: in BIT;
		C: in BIT;
		x0: in BIT;
		out0: out BIT;
		out1: out BIT;
		out2: out BIT
	);
end Main;

architecture Main of Main is
component y0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component y1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component y2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component j0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component k0 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component j1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component k1 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component j2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component k2 is
	port (
		in0: in BIT;
		in1: in BIT;
		in2: in BIT;
		in3: in BIT;
		Output: out BIT
	);
end component;

component JK is
	port(J,K: in BIT;
		Reset: in BIT;
		Clock: in BIT;
		Output: out BIT);
end component;

component Clock is
port(Output: out BIT);
end component;

component var0 is
port(Output: out BIT);
end component;

signal ys0,ys1,ys2,js0,js1,js2,ks0,ks1,ks2,qs0,qs1,qs2,Cs,xs0: BIT;
begin
E0: y0 port map (qs0,qs1,qs2,xs0,ys0);
E1: y1 port map (qs0,qs1,qs2,xs0,ys1);
E2: y2 port map (qs0,qs1,qs2,xs0,ys2);
E3: j0 port map (qs0,qs1,qs2,xs0,js0);
E4: j1 port map (qs0,qs1,qs2,xs0,js1);
E5: j2 port map (qs0,qs1,qs2,xs0,js2);
E6: k0 port map (qs0,qs1,qs2,xs0,ks0);
E7: k1 port map (qs0,qs1,qs2,xs0,ks1);
E8: k2 port map (qs0,qs1,qs2,xs0,ks2);
E9: JK port map(js0,ks0,R,Cs,qs0);
E10: JK port map(js1,ks1,R,Cs,qs1);
E11: JK port map(js2,ks2,R,Cs,qs2);
E12: out0 <= ys0;
E13: out1 <= ys1;
E14: out2 <= ys2;
E15: Clock port map (Cs);
E16: var0 port map (xs0);
end Main;


