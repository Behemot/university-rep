package data;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Class that incapsulates triggers setup information for transitions/
 * @author anthony
 *
 */
public class TriggersSetupTable implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5509916820572052578L;
	/**

	 * Class that represents row of the table.
	 * @author anthony
	 *
	 */
	public class TriggerSetupRecord implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -8074807578866021793L;
		/**
		 * Previous machine state.
		 */
		private String prevState_;
		/**
		 * Next machine state.
		 */
		private String nextState_;
		/**
		 * State change conditions.
		 */
		private String conditions_;
		/**
		 * Signals, emmited during state change.
		 */
		private String signals_;
		/**
		 * Trigger signals, needed to provide state change.
		 */
		private ArrayList<String> triggerSignals_=new ArrayList<String>();
		
		
		private TriggerSetupRecord(String prevState,String nextState,String conditions,String signals){
			prevState_=prevState;
			nextState_=nextState;
			conditions_=conditions;
			signals_=signals;
			for(int i=0;i<prevState.length();i++){
				if(prevState.charAt(i)=='0'&&nextState.charAt(i)=='0'){
					triggerSignals_.add("-0");
				}
				else if(prevState.charAt(i)=='0'&&nextState.charAt(i)=='1'){
					triggerSignals_.add("-1");
				}
				else if(prevState.charAt(i)=='1'&&nextState.charAt(i)=='0'){
					triggerSignals_.add("1-");
				}
				else if(prevState.charAt(i)=='1'&&nextState.charAt(i)=='1'){
					triggerSignals_.add("0-");
				}
				else{
					System.out.println("Error on state character at: "+ prevState_ +" -- "+nextState_);
				}
					
			}
		}
		
		public String getPreviousState(){
			return prevState_;
		}
		
		public String getNextState(){
			return nextState_;
		}
		
		public String getConditions(){
			return conditions_;
		}
		public String getSignals(){
			return signals_;
		}
		public ArrayList<String> getTriggerControls(){
			return triggerSignals_;
		}
	}
	
	private ArrayList<TriggerSetupRecord> transitions_=new ArrayList<TriggerSetupRecord>();
	
	public TriggersSetupTable(MealyMachine M){
		ArrayList<MealyEdge> edges=M.getEdges();
 		ArrayList<String> previousStates=new ArrayList<String>();
 		ArrayList<String> nextStates=new ArrayList<String>();
		ArrayList<String> conditions=new ArrayList<String>();
		ArrayList<String> signals=new ArrayList<String>();
		for(int i=0;i<edges.size();i++){
			System.out.println(edges.get(i).getStart().getID());
			previousStates.add(edges.get(i).getStart().getID());
			nextStates.add(edges.get(i).getEnd().getID());
			conditions.add(edges.get(i).getConditions());
			signals.add(edges.get(i).getSignals());
		}
		
		ArrayList<ArrayList<Integer>> posConditionInts=new ArrayList<ArrayList<Integer>>(); 
		ArrayList<ArrayList<Integer>> negConditionInts=new ArrayList<ArrayList<Integer>>(); 
		ArrayList<String> condStrings=new ArrayList<String>();
		
		int numOfConditions=0;
		
		for(int i=0;i<conditions.size();i++){
			String[] temp=conditions.get(i).split(",");
			posConditionInts.add(new ArrayList<Integer>());
			negConditionInts.add(new ArrayList<Integer>());
			for(int j=0;j<temp.length;j++){
				if(temp[j].length()==0){
					break;
					
				}
				if(temp[j].charAt(0)=='n'){
					temp[j]=temp[j].substring(2, temp[j].length()-1);
					String[] temp2=temp[j].split(",");
					for(int k=0;k<temp2.length;k++){
						temp2[k]=temp2[k].substring(1);
						negConditionInts.get(negConditionInts.size()-1).add(Integer.valueOf(temp2[k]));
						if(Integer.valueOf(temp2[k])+1>numOfConditions){
							System.out.println("TEST");///
							numOfConditions=Integer.valueOf(temp2[k])+1;
						}
					}
				}
				else{
					String[] temp2=temp[j].split(",");
					for(int k=0;k<temp2.length;k++){
						temp2[k]=temp2[k].substring(1);
						posConditionInts.get(posConditionInts.size()-1).add(Integer.valueOf(temp2[k]));
						if(Integer.valueOf(temp2[k])+1>numOfConditions){
							System.out.println("TEST");///
							
							numOfConditions=Integer.valueOf(temp2[k])+1;
						}
					}
				}
			}
		}
		
		for(int i=0;i<posConditionInts.size();i++){
			StringBuffer temp=new StringBuffer();
			for(int j=0;j<numOfConditions;j++){
				temp.append('-');
			}
			for(int j=0;j<posConditionInts.get(i).size();j++){
				temp.setCharAt(posConditionInts.get(i).get(j), '1');
			}
			for(int j=0;j<negConditionInts.get(i).size();j++){
				temp.setCharAt(negConditionInts.get(i).get(j), '0');
			}
			condStrings.add(temp.toString());
		}
		////////
		System.out.println("Width of conditions: "+numOfConditions);
		for(int i=0;i<condStrings.size();i++){
			System.out.println(i+" : "+condStrings.get(i) );
		}
		///////
		ArrayList<ArrayList<Integer>> posSignalInts=new ArrayList<ArrayList<Integer>>(); 
		ArrayList<String> signalStrings=new ArrayList<String>();
		
		int numOfSignals=0;
		
		for(int i=0;i<signals.size();i++){
			String[] temp=signals.get(i).split(",");
			posSignalInts.add(new ArrayList<Integer>());
			for(int j=0;j<temp.length;j++){
				if(temp[j].length()==0){
					break;
				}
				String[] temp2=temp[j].split(",");
				for(int k=0;k<temp2.length;k++){
					temp2[k]=temp2[k].substring(1);
					posSignalInts.get(posSignalInts.size()-1).add(Integer.valueOf(temp2[k]));
					if(Integer.valueOf(temp2[k])+1>numOfSignals){
						numOfSignals=Integer.valueOf(temp2[k])+1;
					}
				}
				
			}
		}
		
		for(int i=0;i<posSignalInts.size();i++){
			StringBuffer temp=new StringBuffer();
			for(int j=0;j<numOfSignals;j++){
				temp.append('0');
			}
			for(int j=0;j<posSignalInts.get(i).size();j++){
				temp.setCharAt(posSignalInts.get(i).get(j), '1');
			}
			signalStrings.add(temp.toString());
		}
	
		for(int i=0;i<previousStates.size();i++){
			transitions_.add(new TriggerSetupRecord(previousStates.get(i), 
					nextStates.get(i), condStrings.get(i), signalStrings.get(i)));
		}
		
		
		
		
	}
	
	public String getPreviousState(int i){
		return transitions_.get(i).getPreviousState();
	}
	
	public String getNextState(int i){
		return transitions_.get(i).getNextState();
	}
	
	public String getConditions(int i){
		return transitions_.get(i).getConditions();
	}
	
	public String getSignals(int i){
		return transitions_.get(i).getSignals();
	}
	
	public ArrayList<String> getControlSignals(int i){
		return transitions_.get(i).getTriggerControls();
	}

	public int getSize(){
		return transitions_.size();
	}
	public int getWidth(){
		return transitions_.get(0).getTriggerControls().size()+4;
	}
}
