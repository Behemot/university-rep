package data;

import java.util.regex.*;
import java.util.ArrayList;

public class Parser {
	public Chart parseString(String S) throws ParsingError {
		if (S.length() < 4) {
			throw new ParsingError("Error: Invalid input");
		}
		ArrayList<Node> generatedNodes_ = new ArrayList<Node>();
		ArrayList<JumpNode> jumpPoints_ = new ArrayList<JumpNode>();
		ArrayList<LabelNode> labelPoints_ = new ArrayList<LabelNode>();
		// Splitting into tokens
		Pattern tokenSplitter = Pattern.compile("\\s*\\)\\s*?\\(\\s*");
		String[] tokens = tokenSplitter.split(S);
		tokens[0] = tokens[0].trim();
		tokens[0] = new String(tokens[0].substring(1));
		tokens[tokens.length - 1] = tokens[tokens.length - 1].trim();
		tokens[tokens.length - 1] = new String(tokens[tokens.length - 1]
				.substring(0, tokens[tokens.length - 1].length() - 1));
		Pattern signalSplitter = Pattern.compile("\\s*,\\s*");
		// Parsing tokens

		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].charAt(0) == 'x' || tokens[i].charAt(0) == 'X') {
				String[] signals = signalSplitter.split(tokens[i]);
				ArrayList<String> signalStrings = new ArrayList<String>();
				Pattern signalPattern = Pattern.compile("x[\\d++]",
						Pattern.CASE_INSENSITIVE);
				for (int j = 0; j < signals.length; j++) {
					Matcher signalMatcher = signalPattern.matcher(signals[j]);
					if (!signalMatcher.matches()) {
						throw new ParsingError("Error on token: " + signals[j]);
					}
					signalStrings.add(signals[j]);
				}
				generatedNodes_.add(new ConditionalNode(signalStrings));
			} else if (tokens[i].charAt(0) == 'y' || tokens[i].charAt(0) == 'Y') {
				String[] signals = signalSplitter.split(tokens[i]);
				ArrayList<String> signalStrings = new ArrayList<String>();
				Pattern signalPattern = Pattern.compile("y[\\d++]",
						Pattern.CASE_INSENSITIVE);
				for (int j = 0; j < signals.length; j++) {
					Matcher signalMatcher = signalPattern.matcher(signals[j]);
					if (!signalMatcher.matches()) {
						throw new ParsingError("Error on token: " + signals[j]);
					}
					signalStrings.add(signals[j]);
				}
				generatedNodes_.add(new OperationalNode(signalStrings));
			} else if (tokens[i].charAt(0) == 'j' || tokens[i].charAt(0) == 'J') {
				Pattern jumpPointPattern = Pattern.compile("j[\\d++]");
				Matcher jumpPointMatcher = jumpPointPattern.matcher(tokens[i]);
				if (!jumpPointMatcher.matches()) {
					throw new ParsingError("Error on token: " + tokens[i]);
				}
				int id = Integer.parseInt(tokens[i].substring(1));
				JumpNode J = new JumpNode(id);
				generatedNodes_.add(J);
				jumpPoints_.add(J);
			} else if (tokens[i].charAt(0) == 'l' || tokens[i].charAt(0) == 'L') {
				Pattern jumpPointPattern = Pattern.compile("l[\\d++]");
				Matcher jumpPointMatcher = jumpPointPattern.matcher(tokens[i]);
				if (!jumpPointMatcher.matches()) {
					throw new ParsingError("Error on token: " + tokens[i]);
				}
				int id = Integer.parseInt(tokens[i].substring(1));
				LabelNode L = new LabelNode(id);
				generatedNodes_.add(L);
				labelPoints_.add(L);
			} else {
				throw new ParsingError("Error on token: " + tokens[i]);
			}

		}

		// Connecting nodes

		for (int i = 0; i < generatedNodes_.size(); i++) {
			switch (generatedNodes_.get(i).getType()) {
			case 0: {
				if (i != generatedNodes_.size() - 1) {
					generatedNodes_.get(i).setNaturalDestination(
							generatedNodes_.get(i + 1));
				}
			}
				;
				break;
			case 1: {
				if (i < (generatedNodes_.size() - 2)) {
					generatedNodes_.get(i).setNaturalDestination(
							generatedNodes_.get(i + 2));
					((ConditionalNode) generatedNodes_.get(i))
							.setConditionalDestination(generatedNodes_
									.get(i + 1));
				} else if (i == generatedNodes_.size() - 2) {
					((ConditionalNode) generatedNodes_.get(i))
							.setConditionalDestination(generatedNodes_
									.get(i + 1));
				} else {
					throw new ParsingError(
							"Can't finish with conditional token");
				}
			}
				;
				break;
			case 2: {
				for (int j = 0; j < labelPoints_.size(); j++) {
					if (((JumpNode) generatedNodes_.get(i)).getID() == labelPoints_
							.get(j).getID()) {
						generatedNodes_.get(i).setNaturalDestination(
								labelPoints_.get(j));
						labelPoints_.remove(j);
						break;
					}
				}
				if (generatedNodes_.get(i).getDestination(null) == null) {
					throw new ParsingError("Error on token: j"
							+ ((JumpNode) generatedNodes_.get(i)).getID()
							+ ". No such label");
				}
			}
				;
				break;
			case 3: {
				if (i != generatedNodes_.size() - 1) {
					generatedNodes_.get(i).setNaturalDestination(
							generatedNodes_.get(i + 1));
				}
			}
				;
				break;
			}

		}
		if (!labelPoints_.isEmpty()) {
			throw new ParsingError("Error on token: l"
					+ labelPoints_.get(0).getID()
					+ " . No jump point to this label");
		}
		
		for(int i=1;i<generatedNodes_.size();i++){
			boolean hasSource_=false;
			for(int j=0;j<generatedNodes_.size();j++){
				if (generatedNodes_.get(j).getType()==1){
					if(generatedNodes_.get(i)==((ConditionalNode)generatedNodes_.get(j)).
					getConditionalDestination()||generatedNodes_.get(i)==
					((ConditionalNode)generatedNodes_.get(j)).getNaturalDestination()){
						hasSource_=true;
						break;
					}
				}
				else{
					if(generatedNodes_.get(i)==generatedNodes_.get(j).getDestination(null)){
						hasSource_=true;
						break;
					}
				}
			
			}
			if(!hasSource_){
				throw new ParsingError("Error on node: "+tokens[i]+". No source node.");
			}
		}
		return new Chart(generatedNodes_);
		
	}
}
