package data;

import java.util.ArrayList;

public class VHDLtransformer {

	final static int ENTITY=0;
	final static int COMPONENT=1;
	
	public String generateVHDL(ArrayList<ArrayList<Integer[]>> y,
			ArrayList<ArrayList<Integer[]>> t,int numOfTriggers){
		int numOfInputs=0;
		for(int i=0;i<y.size();i++){
			if(y.get(i).size()!=0){
				numOfInputs=y.get(i).get(0).length;
				break;
			}
		}
		StringBuffer components=new StringBuffer();
		StringBuffer signals=new StringBuffer();
		StringBuffer connections=new StringBuffer();
		//Needed components
		for(int i=0;i<y.size();i++){
			components.append(addFunction("y"+i, y.get(i),COMPONENT,numOfInputs));
		}
		for(int i=0;i<t.size();i++){
			components.append(addFunction("t"+i, t.get(i),COMPONENT,numOfInputs));
		}
		components.append(addTrigger(COMPONENT));
		components.append(addClock(COMPONENT));
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			components.append(addVariable(COMPONENT, "var"+i, (i+1)*10));
		}
		//Connections and signals
		int counter=0;
		for(int i=0;i<y.size();i++){
			signals.append("ys"+i+",");
			connections.append("E"+counter+": y"+i+" port map (" );
			for(int l=0;l<numOfTriggers;l++){
				connections.append("qs"+l+",");
			}
			for(int l=0;l<numOfInputs-numOfTriggers;l++){
				connections.append("xs"+l+",");
			}
			connections.append("ys"+i+");\n");
			counter++;
		}
		for(int i=0;i<t.size();i++){
			signals.append("ts"+i+",");
			connections.append("E"+counter+": t"+i+" port map (" );
			for(int l=0;l<numOfTriggers;l++){
				connections.append("qs"+l+",");
			}
			for(int l=0;l<numOfInputs-numOfTriggers;l++){
				connections.append("xs"+l+",");
			}
			connections.append("ts"+i+");\n");
			counter++;
		}
		for(int i=0;i<numOfTriggers;i++){
			signals.append("qs"+i+",");
			connections.append("E"+counter+": T port map(ts"+i+",R,Cs,qs"+i+");\n");
			counter++;
		}
		for(int i=0;i<y.size();i++){
			connections.append("E"+counter+": out"+i+" <= ys"+i+";\n");
			counter++;
		}
		signals.append("Cs,");
		connections.append("E"+counter+": Clock port map (Cs);\n");
		counter++;
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			signals.append("xs"+i+",");
			connections.append("E"+counter+": var"+i+" port map (xs"+i+");\n");
			counter++;
		}
		
		signals.deleteCharAt(signals.length()-1);
		//Form a result function string
		StringBuffer result=new StringBuffer();
		result.append("library IEEE;\n" +//Libraries hook-up
				"use IEEE.STD_LOGIC_1164.ALL;\n");// +
		result.append(addClock(ENTITY));
		
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			result.append(addVariable(ENTITY, "var"+i, (i+1)*10));
		}
		result.append(addTrigger(ENTITY));
		result.append(addNOR(ENTITY));
		
		for(int i=0;i<y.size();i++){
			result.append(addFunction("y"+i, y.get(i),ENTITY,numOfInputs));
		}
		for(int i=0;i<t.size();i++){
			result.append(addFunction("t"+i, t.get(i),ENTITY,numOfInputs));
		}
		//Create main entity
		result.append("entity Main is\n" +
				"	port (\n");
		result.append("		R: in BIT;\n");
		result.append("		C: in BIT;\n");
		for(int i=0;i<numOfInputs-numOfTriggers;i++){
			result.append("		x"+i+": in BIT;\n");
		}
		for(int i=0;i<y.size();i++){
			result.append("		out"+i+": out BIT;\n");
		}
		result.deleteCharAt(result.length()-2);
		result.append("	);\n" +
				"end Main;\n\n");
		result.append("architecture Main of Main is\n");
		result.append(components.toString());
		result.append("signal "+signals.toString()+": BIT;\n" +
				"begin\n"+connections.toString()+"end Main;\n\n");
		return result.toString();
	}

	
	private String addFunction(String name,ArrayList<Integer[]> func,int mode,int inpNum){
		StringBuffer components=new StringBuffer();
		StringBuffer signals=new StringBuffer();
		StringBuffer connections=new StringBuffer();
		if(mode==ENTITY&&func.size()!=0){
			components.append(addNOR(COMPONENT));
			int counter=0;
			int signalcounter=0;
			String[] andConnectors=new String[func.size()];
			for(int i=0;i<func.size();i++){
				int slotsLeft=3;
				String[] slots=new String[3];
				slots[0]="\'0\'";
				slots[1]="\'0\'";
				slots[2]="\'0\'";
				for(int j=0;j<func.get(i).length;j++){
					if(slotsLeft==0&&(func.get(i)[j]==0||func.get(i)[j]==1)){
						connections.append("E"+counter+": NOR3 port map ("+slots[0]+","+slots[1]+","+slots[2]+",S"+signalcounter+");\n");
						String temp="S"+signalcounter;
						signals.append("S"+signalcounter+",");
						signalcounter++;
						counter++;
						connections.append("E"+counter+": NOR3 port map ("+temp+",\'0\',\'0\',S"+signalcounter+");\n");
						slots[0]="S"+signalcounter;
						signals.append("S"+signalcounter+",");
						signalcounter++;
						counter++;
						slots[1]="\'0\'";
						slots[2]="\'0\'";
						slotsLeft=2;
					}
					if(func.get(i)[j]==0){
						slots[3-slotsLeft]="in"+j;
						slotsLeft--;
					}else if(func.get(i)[j]==1){
						connections.append("E"+counter+": NOR3 port map (in"+j+",\'0\',\'0\',S"+signalcounter+");\n");
						slots[3-slotsLeft]="S"+signalcounter;
						slotsLeft--;
						counter++;
						signals.append("S"+signalcounter+",");
						signalcounter++;
					}
					
				}
				connections.append("E"+counter+": NOR3 port map ("+slots[0]+","+slots[1]+","+slots[2]+",S"+signalcounter+");\n");
				andConnectors[i]="S"+signalcounter;
				signals.append("S"+signalcounter+",");
				signalcounter++;
				counter++;
			}
			
			int slotsLeft=3;
			String[] slots=new String[3];
			slots[0]="\'0\'";
			slots[1]="\'0\'";
			slots[2]="\'0\'";
			for(int i=0;i<andConnectors.length;i++){
				if(slotsLeft==0){
					connections.append("E"+counter+": NOR3 port map ("+slots[0]+","+slots[1]+","+slots[2]+",S"+signalcounter+");\n");
					slots[0]="S"+signalcounter;
					slots[1]="\'0\'";
					slots[2]="\'0\'";
					signals.append("S"+signalcounter+",");
					slotsLeft=2;
					signalcounter++;
					counter++;
				}
				slots[3-slotsLeft]=andConnectors[i];
				slotsLeft--;
				
			}
			connections.append("E"+counter+": NOR3 port map ("+slots[0]+","+slots[1]+","+slots[2]+",S"+signalcounter+");\n");
			signals.append("S"+signalcounter+",");
			String temp="S"+signalcounter;
			signalcounter++;
			counter++;
			connections.append("E"+counter+": NOR3 port map ("+temp+",\'0\',\'0\',S"+signalcounter+");\n");
			signals.append("S"+signalcounter);
			counter++;
			connections.append("E"+counter+": Output <= S"+signalcounter+";\n");
		}
		
		//Form a result function string
		StringBuffer result=new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append(name+" is\n" +
				"	port (\n");
		for(int i=0;i<inpNum;i++){
			result.append("		in"+i+": in BIT;\n");
		}
		result.append("		Output: out BIT\n" +
				"	);\n");
		if(mode==ENTITY){
			result.append("end "+name+";\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture "+name+" of "+name+" is\n");
			result.append(components.toString());
			if(func.size()!=0){
				result.append("signal "+signals.toString()+": BIT;\n" +
						"begin\n"+connections.toString()+"end "+name+";\n\n");
			}
			else{
				result.append("begin\n" +
						"	E0: Output <= '0';\n" +
						"end "+name+";\n\n");
			}
		}
		return result.toString();
	}
	
	
	
	private String addTrigger(int mode){
		StringBuffer result=new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("T is\n" +
				"	port(T: in BIT;\n" +
				"		Reset: in BIT;\n" +
				"		Clock: in BIT;\n" +
				"		Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end T;\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture T of T is\n " +
					"	signal temp: BIT;\n" +
					"begin\n" +
					"	process(Clock)\n" +
					"	begin\n" +
					"		if Clock'event and Clock=\'1\' then \n" +
					"			if Reset=\'1\' then\n" +
					"				temp <= \'0\';\n" +
					"			else \n" +
					"				if(T=\'0\') then \n" +
					"					temp<=temp;\n" +
					"				elsif(T=\'1\') then\n" +
					"					temp<=not (temp);\n" +
					"				end if;\n" +
					"			end if;\n" +
					"		end if;\n" +
					"	end process;\n" +
					"	Output<=temp;\n" +
					"end T;\n\n"
					);
		}
		return result.toString();
	}

	private String addNOR(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("NOR3 is\n" +
				"port(in1,in2,in3: in BIT;\n" +
				"	Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end NOR3;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture NOR3 of NOR3 is\n " +
					"begin\n" +
					"	Output<=not(in1 or in2 or in3);\n"+
					"end NOR3;\n\n"
					);
	
		}
		return result.toString();
	}

	
	private String addClock(int mode){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append("Clock is\n" +
				"port(Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end Clock;\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture Clock of Clock is\n " +
					"	signal t: BIT;\n" +
					"begin\n" +
					"	Output<= not t after 1 ns;\n" +
					"	t<= not t after 1 ns;\n" +
					"end Clock;\n\n"
					);
	
		}
		return result.toString();
	}
	
	private String addVariable(int mode, String name,int time){
		StringBuffer result= new StringBuffer();
		if(mode==ENTITY){
			result.append("entity ");
		}
		else{
			result.append("component ");
		}
		result.append(name +" is\n" +
				"port(Output: out BIT);\n");
		if(mode==ENTITY){
			result.append("end "+name+";\n\n");
		}
		else{
			result.append("end component;\n\n");
		}
		if(mode==ENTITY){
			result.append("architecture "+name+" of "+name+" is\n " +
					"	signal t: BIT;\n" +
					"begin\n" +
					"	Output<= not t after "+time+" ns;\n" +
					"	t<= not t after "+time+" ns;\n" +
					"end "+name+";\n\n"
					);
	
		}
		return result.toString();
	
	}

}
