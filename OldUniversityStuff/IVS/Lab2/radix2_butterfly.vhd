library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity radix2_butterfly is 
port( in1_real, in1_complex, in2_real, in2_complex, w: in real
       out1_real, out1_complex, out2: out real)
end radix2_butterfly;

architecture 
