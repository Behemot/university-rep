The following files were generated for 'xfft_v7_1_pipeline' in directory 
/home/anthony/IVS/Lab34/ipcore_dir/

xfft_v7_1_pipeline.asy:
   Graphical symbol information file. Used by the ISE tools and some
   third party tools to create a symbol representing the core.

xfft_v7_1_pipeline.gise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

xfft_v7_1_pipeline.ngc:
   Binary Xilinx implementation netlist file containing the information
   required to implement the module in a Xilinx (R) FPGA.

xfft_v7_1_pipeline.vhd:
   VHDL wrapper file provided to support functional simulation. This
   file contains simulation model customization data that is passed to
   a parameterized simulation model for the core.

xfft_v7_1_pipeline.vho:
   VHO template file containing code that can be used as a model for
   instantiating a CORE Generator module in a VHDL design.

xfft_v7_1_pipeline.xco:
   CORE Generator input file containing the parameters used to
   regenerate a core.

xfft_v7_1_pipeline.xise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

xfft_v7_1_pipeline_readme.txt:
   Text file indicating the files generated and how they are used.

xfft_v7_1_pipeline_xmdf.tcl:
   ISE Project Navigator interface file. ISE uses this file to determine
   how the files output by CORE Generator for the core can be integrated
   into your ISE project.

xfft_v7_1_pipeline_flist.txt:
   Text file listing all of the output files produced when a customized
   core was generated in the CORE Generator.


Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

