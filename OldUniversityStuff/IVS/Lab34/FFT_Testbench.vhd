-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
component xfft_v7_1
	port (
	clk: in std_logic;
	start: in std_logic;
	unload: in std_logic;
	xn_re: in std_logic_vector(7 downto 0);
	xn_im: in std_logic_vector(7 downto 0);
	fwd_inv: in std_logic;
	fwd_inv_we: in std_logic;
	rfd: out std_logic;
	xn_index: out std_logic_vector(5 downto 0);
	busy: out std_logic;
	edone: out std_logic;
	done: out std_logic;
	dv: out std_logic;
	xk_index: out std_logic_vector(5 downto 0);
	xk_re: out std_logic_vector(14 downto 0);
	xk_im: out std_logic_vector(14 downto 0));
end component;



------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG

  -- Component Instantiation
	SIGNAL Clk: std_logic;
	SIGNAL Start: std_logic;
	SIGNAL Unload: std_logic;
	SIGNAL Xn_re: std_logic_vector(7 downto 0);
	SIGNAL Xn_im: std_logic_vector(7 downto 0);
	SIGNAL Fwd_inv: std_logic;
	SIGNAL Fwd_inv_we: std_logic;
	SIGNAL Rfd: std_logic;
	SIGNAL Xn_index: std_logic_vector(5 downto 0);
	SIGNAL Busy: std_logic;
	SIGNAL Edone: std_logic;
	SIGNAL Done: std_logic;
	SIGNAL Dv: std_logic;
	SIGNAL Xk_index: std_logic_vector(5 downto 0);
	SIGNAL Xk_re: std_logic_vector(14 downto 0);
	SIGNAL Xk_im: std_logic_vector(14 downto 0);

  BEGIN

fft : xfft_v7_1
		port map (
			clk => Clk,
			start => Start,
			unload => Unload,
			xn_re => Xn_re,
			xn_im => Xn_im,
			fwd_inv => Fwd_inv,
			fwd_inv_we => Fwd_inv_we,
			rfd => Rfd,
			xn_index => Xn_index,
			busy => Busy,
			edone => Edone,
			done => Done,
			dv => Dv,
			xk_index => Xk_index,
			xk_re => Xk_re,
			xk_im => Xk_im);

  --  Test Bench Statements
     tb : PROCESS
     BEGIN

       -- wait for 100 ns; -- wait until global set/reset completes

        -- Add user defined stimulus here

        --wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
