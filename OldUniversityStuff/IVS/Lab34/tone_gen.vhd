--osardar1@jhu.edu
--tone_gen.vhd
--Generates tones to test FFT

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.all;

entity tone_gen is port(
	clk1	: in STD_LOGIC;
	freq	: in STD_LOGIC_VECTOR(3 downto 0);
	sine_out: out STD_LOGIC_VECTOR(7 downto 0));
end tone_gen;

architecture Behavioral of tone_gen is
	signal counter	: STD_LOGIC_VECTOR(3 downto 0) := x"0";
	signal phase	: STD_LOGIC_VECTOR(1 downto 0) := b"00";
	signal sine_out_int : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	
	component sin_lut port(
		clk		: IN std_logic;
		phase_in: IN std_logic_VECTOR(1 downto 0);
		sine	: OUT std_logic_VECTOR(7 downto 0));
	end component;
	
begin
	sin_lut0 : sin_lut port map(
		clk => clk1,
		phase_in => phase,
		sine => sine_out_int);

	process(clk1)
	begin
		if(rising_edge(clk1)) then
			counter <= counter + '1';
			
			if(counter = freq) then
				counter <= (others => '0');
				phase <= phase + '1';
			end if;
		end if;
	end process;
	
	sine_out <= sine_out_int;
end Behavioral;