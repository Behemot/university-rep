--osardar1@jhu.edu
--clk_gen.vhd
--Divides 50MHz clock down to 1MHz for the DFT

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.all;

entity clk_gen is port(
	clk50	: in STD_LOGIC;
	clk1	: out STD_LOGIC);
end clk_gen;

architecture Behavioral of clk_gen is
	signal counter : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal clk1_int: STD_LOGIC := '0';
begin
	process(clk50)
	begin
		if(rising_edge(clk50)) then
			counter <= counter + '1';
			
			--Generate 1MHz Signal
			if(counter = x"18") then
				counter <= (others => '0');
				clk1_int <= not clk1_int;
			end if;
		end if;
	end process;
	
	clk1 <= clk1_int;
end Behavioral;