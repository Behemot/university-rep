onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /fft_natural_tb/clk1_tb
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/freq_tb
add wave -noupdate -format Logic /fft_natural_tb/start_tb
add wave -noupdate -format Logic /fft_natural_tb/fwd_inv_tb
add wave -noupdate -format Logic /fft_natural_tb/fwd_inv_we_tb
add wave -noupdate -format Logic /fft_natural_tb/rfd_tb
add wave -noupdate -format Logic /fft_natural_tb/dv_tb
add wave -noupdate -format Logic /fft_natural_tb/done_tb
add wave -noupdate -format Logic /fft_natural_tb/busy_tb
add wave -noupdate -format Logic /fft_natural_tb/edone_tb
add wave -noupdate -format Logic /fft_natural_tb/unload_tb
add wave -noupdate -divider <NULL>
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/sine_fixed
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/sine_float
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xn_re_tb
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xn_im_tb
add wave -noupdate -format Literal -radix unsigned /fft_natural_tb/xn_index_tb
add wave -noupdate -divider <NULL>
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_im_tb
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_re_tb
add wave -noupdate -format Literal -radix unsigned /fft_natural_tb/xk_index_tb
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_re_flip
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_im_flip
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_re_sq
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_im_sq
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_sq_sum
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_sqrt
add wave -noupdate -format Literal -radix unsigned /fft_natural_tb/index_tb(47)
add wave -noupdate -format Literal -radix hexadecimal /fft_natural_tb/xk_sqrt_max
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7156986031 ps} 0} {{Cursor 4} {8672258723 ps} 0}
configure wave -namecolwidth 201
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {924354958 ps} {33002391950 ps}
