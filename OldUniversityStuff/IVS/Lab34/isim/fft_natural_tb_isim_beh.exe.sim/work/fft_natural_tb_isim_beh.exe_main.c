/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_2592010699;
char *STD_STANDARD;
char *UNISIM_P_3222816464;
char *IEEE_P_1367372525;
char *IEEE_P_2717149903;
char *STD_TEXTIO;
char *UNISIM_P_0947159679;
char *IEEE_P_3499444699;
char *XILINXCORELIB_P_1837083571;
char *IEEE_P_3972351953;
char *IEEE_P_1242562249;
char *XILINXCORELIB_P_3381355550;
char *XILINXCORELIB_P_1849098369;
char *XILINXCORELIB_P_2602938013;
char *IEEE_P_0774719531;
char *IEEE_P_3620187407;
char *XILINXCORELIB_P_3155556343;
char *XILINXCORELIB_P_3160202542;
char *XILINXCORELIB_P_3743709326;
char *IEEE_P_3564397177;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    std_textio_init();
    ieee_p_2717149903_init();
    ieee_p_1367372525_init();
    ieee_p_0774719531_init();
    ieee_p_1242562249_init();
    ieee_p_3972351953_init();
    ieee_p_3564397177_init();
    unisim_p_0947159679_init();
    unisim_p_3222816464_init();
    work_a_0780303089_3212880686_init();
    xilinxcorelib_p_1837083571_init();
    xilinxcorelib_p_3381355550_init();
    xilinxcorelib_p_1849098369_init();
    xilinxcorelib_p_2602938013_init();
    xilinxcorelib_p_3160202542_init();
    xilinxcorelib_p_3155556343_init();
    xilinxcorelib_p_3743709326_init();
    xilinxcorelib_a_0006162580_3212880686_init();
    xilinxcorelib_a_3854613474_3212880686_init();
    xilinxcorelib_a_2106249298_3212880686_init();
    xilinxcorelib_a_2551472932_3212880686_init();
    xilinxcorelib_a_2948401865_3212880686_init();
    xilinxcorelib_a_1243855295_3212880686_init();
    xilinxcorelib_a_2581227851_3212880686_init();
    work_a_3457021571_3627378042_init();
    work_a_3372685717_3212880686_init();
    unisim_a_2562466605_1496654361_init();
    unisim_a_1717296735_4086321779_init();
    unisim_a_3519694068_2693788048_init();
    unisim_a_1916428545_3403034321_init();
    unisim_a_3055263662_1392679692_init();
    unisim_a_0218001308_3676810390_init();
    unisim_a_1095153226_3676810390_init();
    unisim_a_3839058572_3676810390_init();
    unisim_a_1568553482_3676810390_init();
    unisim_a_0931581782_3676810390_init();
    unisim_a_4053252614_3676810390_init();
    unisim_a_0727751866_3676810390_init();
    unisim_a_1264347105_3676810390_init();
    unisim_a_0534317075_3676810390_init();
    unisim_a_2989391088_3676810390_init();
    unisim_a_2880541983_3676810390_init();
    unisim_a_3180579060_3676810390_init();
    unisim_a_2526245287_3676810390_init();
    unisim_a_1790552799_3676810390_init();
    unisim_a_2363982922_3676810390_init();
    unisim_a_3318909725_3676810390_init();
    unisim_a_3539595177_3676810390_init();
    unisim_a_3650883126_3676810390_init();
    unisim_a_0344698425_3676810390_init();
    unisim_a_0847007988_3676810390_init();
    unisim_a_3384037652_3676810390_init();
    unisim_a_1367769374_3676810390_init();
    unisim_a_0472242593_3676810390_init();
    unisim_a_3706666297_3676810390_init();
    unisim_a_2126231830_3676810390_init();
    unisim_a_0152059386_3676810390_init();
    unisim_a_4135062285_3672491681_init();
    unisim_a_0184336390_3672491681_init();
    unisim_a_2495524655_3634591992_init();
    unisim_a_3377428034_3634591992_init();
    unisim_a_3401159022_3634591992_init();
    unisim_a_0739137552_3634591992_init();
    unisim_a_2548838872_3634591992_init();
    unisim_a_0029328054_3634591992_init();
    unisim_a_1446710196_3752513572_init();
    unisim_a_4104775526_3752513572_init();
    unisim_a_3120128138_3752513572_init();
    unisim_a_2545276020_3752513572_init();
    unisim_a_3822252837_3752513572_init();
    unisim_a_1094721527_3752513572_init();
    unisim_a_1863101193_3752513572_init();
    unisim_a_3449702363_3752513572_init();
    unisim_a_0774281858_3731405331_init();
    unisim_a_0789928158_3731405331_init();
    unisim_a_2015308372_3731405331_init();
    unisim_a_2950243211_3731405331_init();
    unisim_a_2032986120_3731405331_init();
    unisim_a_3600803327_3731405331_init();
    unisim_a_3330121465_3731405331_init();
    unisim_a_2718997652_3731405331_init();
    unisim_a_4164620091_3731405331_init();
    unisim_a_2988095912_3731405331_init();
    unisim_a_0350208134_1521797606_init();
    unisim_a_2892212195_1521797606_init();
    unisim_a_1941169844_3118875749_init();
    unisim_a_2261302797_3723259517_init();
    unisim_a_0868425105_1864968857_init();
    unisim_a_1646226234_1266530935_init();
    unisim_a_3484885994_2523279426_init();
    unisim_a_3702704980_1602505438_init();
    unisim_a_2456189163_0111667855_init();
    unisim_a_1398595224_1990404599_init();
    unisim_a_2777468956_0319854582_init();
    unisim_a_1566006631_3088576162_init();
    unisim_a_2680519808_1064626918_init();
    work_a_0407352029_0632001823_init();
    work_a_3550718384_3212880686_init();


    xsi_register_tops("work_a_3550718384_3212880686");

    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    UNISIM_P_3222816464 = xsi_get_engine_memory("unisim_p_3222816464");
    IEEE_P_1367372525 = xsi_get_engine_memory("ieee_p_1367372525");
    IEEE_P_2717149903 = xsi_get_engine_memory("ieee_p_2717149903");
    STD_TEXTIO = xsi_get_engine_memory("std_textio");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    XILINXCORELIB_P_1837083571 = xsi_get_engine_memory("xilinxcorelib_p_1837083571");
    IEEE_P_3972351953 = xsi_get_engine_memory("ieee_p_3972351953");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    XILINXCORELIB_P_3381355550 = xsi_get_engine_memory("xilinxcorelib_p_3381355550");
    XILINXCORELIB_P_1849098369 = xsi_get_engine_memory("xilinxcorelib_p_1849098369");
    XILINXCORELIB_P_2602938013 = xsi_get_engine_memory("xilinxcorelib_p_2602938013");
    IEEE_P_0774719531 = xsi_get_engine_memory("ieee_p_0774719531");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    XILINXCORELIB_P_3155556343 = xsi_get_engine_memory("xilinxcorelib_p_3155556343");
    XILINXCORELIB_P_3160202542 = xsi_get_engine_memory("xilinxcorelib_p_3160202542");
    XILINXCORELIB_P_3743709326 = xsi_get_engine_memory("xilinxcorelib_p_3743709326");
    IEEE_P_3564397177 = xsi_get_engine_memory("ieee_p_3564397177");

    return xsi_run_simulation(argc, argv);

}
