/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xdc0cde0 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *IEEE_P_2592010699;



static void unisim_a_1566006631_3088576162_p_0(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:
LAB3:    t1 = (t0 + 48471);
    t3 = (t0 + 1672U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_2592010699) + 4000);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 1;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (1 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 29320U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 48473);
    t16 = ((IEEE_P_2592010699) + 4000);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 4;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (4 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t19 = (t0 + 17544);
    t21 = (t19 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t14, 16U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t25 = (t0 + 17416);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void unisim_a_1566006631_3088576162_p_1(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:
LAB3:    t1 = (t0 + 48478);
    t3 = (t0 + 2632U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_2592010699) + 4000);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 1;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (1 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 29352U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 48480);
    t16 = ((IEEE_P_2592010699) + 4000);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 4;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (4 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t19 = (t0 + 17608);
    t21 = (t19 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t14, 16U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t25 = (t0 + 17432);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void unisim_a_1566006631_3088576162_p_2(char *t0)
{
    char t4[16];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:
LAB3:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 2472U);
    t3 = *((char **)t1);
    t5 = ((IEEE_P_2592010699) + 4000);
    t6 = (t0 + 29336U);
    t7 = (t0 + 29336U);
    t1 = xsi_base_array_concat(t1, t4, t5, (char)97, t2, t6, (char)97, t3, t7, (char)101);
    t8 = (t0 + 17672);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t1, 8U);
    xsi_driver_first_trans_fast(t8);

LAB2:    t13 = (t0 + 17448);
    *((int *)t13) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void unisim_a_1566006631_3088576162_p_3(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    t1 = (t0 + 14968U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t1 = (t0 + 5032U);
    t4 = *((char **)t1);
    t1 = (t0 + 17736);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t4, 32U);
    xsi_driver_first_trans_delta(t1, 0U, 32U, t3);
    t9 = (t0 + 17736);
    xsi_driver_intertial_reject(t9, t3, t3);
    t1 = (t0 + 14968U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t1 = (t0 + 5192U);
    t4 = *((char **)t1);
    t1 = (t0 + 17800);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t4, 4U);
    xsi_driver_first_trans_delta(t1, 0U, 4U, t3);
    t9 = (t0 + 17800);
    xsi_driver_intertial_reject(t9, t3, t3);
    t1 = (t0 + 17464);
    *((int *)t1) = 1;

LAB1:    return;
}


extern void unisim_a_1566006631_3088576162_init()
{
	static char *pe[] = {(void *)unisim_a_1566006631_3088576162_p_0,(void *)unisim_a_1566006631_3088576162_p_1,(void *)unisim_a_1566006631_3088576162_p_2,(void *)unisim_a_1566006631_3088576162_p_3};
	xsi_register_didat("unisim_a_1566006631_3088576162", "isim/fft_natural_tb_isim_beh.exe.sim/unisim/a_1566006631_3088576162.didat");
	xsi_register_executes(pe);
}
