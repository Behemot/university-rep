#!/usr/bin/env python

import math;
import sys;
import random;
import pylab;
import numpy;
class Signal:
    N=8;
    n=64;
    Omega=128;
    disc_frequency=2*Omega;
    dt=1./disc_frequency;
    T_max=dt*n;
    def __init__(self):
        random.seed();
        for i in range(0,10):
            self.A_p=[(random.random()*10.) for i in range(0,self.N)];
            self.Phi_p=[random.random()*2.*math.pi for i in range(0,self.N)];
            self.Omega_p=[random.random()*self.Omega*1. for i in range(0,self.N)];  
        
    def at_t(self,t):
        
        if (t>=0 and t<=self.T_max):
            return sum(self.A_p[i]*math.sin(self.Omega_p[i]*t+self.Phi_p[i]) for i in range(self.N));
        else:
            return 0;
    def getValues(self,tk):
        points=[];
        t_cur=0;
        while(t_cur<tk):
            points.append(self.at_t(t_cur));
            t_cur+=self.dt;
        return points;    
    def getArgs(self,tk):
        points=[];
        t_cur=0;
        while(t_cur<tk):
            points.append(t_cur);
            t_cur+=self.dt;
        return points;
    def getMatExp(self,points=[]):
        if (type(points).__name__=='float' or type(points).__name__=='float64'):
            temp=points;
            points=self.getValues(temp);
        total=0;
        for i in points:
            total+=i;
        return total/len(points);
    def getDispersion(self,tk):
        points=self.getValues(tk);
        Mx=self.getMatExp(points);
        return sum((i-Mx)**2 for i in points)/len(points);
#------------------------------------------
def getCorrelation(x,y,tau):
    if (tau<Signal.dt): tau=Signal.dt;
    tau_points=(int)(tau/Signal.dt);
    Mx=x.getMatExp(Signal.T_max+tau);
    My=y.getMatExp(Signal.T_max);
    x_p=x.getValues(Signal.T_max+tau);
    y_p=y.getValues(Signal.T_max);
    return sum((x_p[i+tau_points]-Mx)*(y_p[i]-My) for i in range(len(y_p)))/len(y_p);
    #return sum((x_p[i+tau_points]-Mx)*(y_p[i]-My) for i in range(100))/100;
       
#
def discrete_plot(args, vals, xlabel, ylabel, filename):
    
    pylab.plot(args, vals);
    pylab.xlabel(xlabel);
    pylab.ylabel(ylabel);
    pylab.grid(True);
    pylab.savefig(filename);
    pylab.clf();
def save_plot(function, name, filename, range_start = 0, range_end = 4.5E4):
    step = (range_end - range_start) / 300.;
    pltime = numpy.arange(range_start, range_end + step, step);
    plvalue = [function(t) for t in pltime];
    pylab.plot(pltime, plvalue);
    pylab.xlabel(u"t");
    pylab.ylabel(name);
    pylab.grid(True);
    pylab.savefig(filename);
    pylab.clf();
    
def plot_points(args,vals,xlabel,ylabel,filename):
    pylab.plot(args,vals,linestyle='None',marker='x',color='blue')
    pylab.vlines(args,numpy.zeros(len(vals)),vals,color='blue')
    pylab.plot(args,vals,color='blue')
    pylab.xlabel(xlabel)
    pylab.ylabel(ylabel)
    pylab.grid(True)
    pylab.savefig(filename)
    pylab.clf()    
#-------------------------------------------------------    
def W(N,k):
    x = 2. * math.pi * k / N
    return math.cos(x) - 1j * math.sin(x)
    
def FFT(x):
    result = [0 for i in range(len(x))];
    if (len(x) == 1):
        result[0]= x[0]
    else:
        even = [x[2 * k] for k in range(len(x) / 2)]
        odd = [x[2 * k + 1] for k in range(len(x) / 2)]

        even_result = FFT(even)
        odd_result = FFT(odd)
        
        for k in range(len(odd_result)): #Multiplication by twiddle factors e^(-2*pi*j*k/N)
            odd_result[k] = odd_result[k] * W(len(x),k)
        
        for k in range(len(x) / 2):
            result[k] = even_result[k] + odd_result[k];
            result[k + len(x) / 2] = even_result[k] - odd_result[k];
            
    return result;        


def W1(N, p, k):
    x = 2. * math.pi * p * k / N
    return math.cos(x) - 1j * math.sin(x)

def DFT(points):
    A = []
    for p in range(Signal.n):
        F = 0. + 0j;
        for k in range(Signal.n):
            F += points[k] * W1(Signal.n, p, k)
        A.append(F)
    return A
     
#-------------------------------------------------------    

def main():
    X = Signal();

    discrete_plot(X.getArgs(Signal.T_max), X.getValues(Signal.T_max), u"T", u"X(t)", u"X(t).png");
    points = X.getValues(Signal.T_max);
    F = FFT(points)
    A = [math.sqrt(F[i].real * F[i].real + F[i].imag * F[i].imag) for i in range (len(F))]
    P = [math.atan(F[i].imag / F[i].real) for i in range(len(F))]
    
    F1 = DFT(points);
    A1 = [math.sqrt(F1[i].real * F1[i].real + F1[i].imag * F1[i].imag) for i in range (len(F1))]
    P1 = [math.atan(F1[i].imag / F1[i].real) for i in range(len(F1))]
    freq = [i*2.*math.pi/Signal.T_max for i in range(Signal.n)]
    
    epsilon_abs = [abs(A[i] - A1[i]) for i in range(len(A))] 
    phase_abs = [ abs(P[i]-P1[i]) for i in range(len(P))]  
    
    plot_points(freq,A,u"w",u"A(w)","FFT_AFC.png")
    plot_points(freq,P,u"w",u"P(w)","FFT_PFC.png")
    plot_points(freq,A1,u"w",u"A(w)","DFT_AFC.png")
    plot_points(freq,P1,u"w",u"P(w)","DFT_PFC.png")
    eps_mid = 0.
    phase_mid = 0.
   
    print "Amplitude - Phase"
    for i in range(len(epsilon_abs)):
        eps_mid += epsilon_abs[i]
        phase_mid += phase_abs[i]
        print str(epsilon_abs[i])+ " - " + str(phase_abs[i])
    
    print "------------------------------------"
    eps_mid /= len(epsilon_abs)
    phase_mid /= len(phase_abs)  
    print str(eps_mid) + " - " + str(phase_mid)

#---------------------------------------------------------
main()

