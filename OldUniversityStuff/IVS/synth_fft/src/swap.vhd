-- SWAP UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity swap is
port (
       a : in std_logic_vector (31 downto 0) ;
       b : in std_logic_vector (31 downto 0) ;
       clock : in std_logic ;
       rst_swap , en_swap : in std_logic ;
       finish_swap : out std_logic ;
       d : out std_logic_vector (31 downto 0) ;
       large_exp : out std_logic_vector (7 downto 0) ;
       c  : out std_logic_vector (32 downto 0 ) ) ;
end swap ;

architecture rtl of swap is
begin
process (a , b , clock , rst_swap , en_swap)
variable x , y : std_logic_vector (7 downto 0) ;
variable p , q : std_logic_vector (22 downto 0) ;
begin
if(rst_swap = '1' ) then 
c <= '0' & a(22 downto 0) & "000000000" ;
finish_swap <= '0' ;
elsif(rst_swap = '0') then
if(en_swap = '1') then
x := a (30 downto 23) ;
y := b (30 downto 23) ;
p := a (22 downto 0) ;
q := b (22 downto  0) ;
if (clock = '1') then
if (x < y) then
c <= '1' & a (22 downto 0) & "000000000" ; -- '1' for checking
d <= '1' & b (22 downto 0) & "00000000" ; -- '1' for implicit one 
large_exp <= b (30 downto 23) ; 
finish_swap <= '1' ;

elsif (y < x) then
c <= '1' & b (22 downto 0) & "000000000" ;
d <= '1' & a (22 downto 0) & "00000000" ; -- '1' for implicit 1.
large_exp <= a (30 downto 23) ;
finish_swap <= '1' ;

elsif ( (x=y) and (p < q)) then
c <= '1' & a (22 downto 0) & "000000000" ; -- '1' for checking
d <= '1' & b (22 downto 0) & "00000000" ; -- '1' for implicit one
large_exp <= b (30 downto 23) ; 
finish_swap <= '1' ; 

else
c <= '1' & b (22 downto 0) & "000000000" ;
d <= '1' & a (22 downto 0) & "00000000" ; -- '1' for implicit 1.
large_exp <= a (30 downto 23) ;
finish_swap <= '1' ;

end if ;
end if ;
end if ;
end if ;
end process;
end rtl;

