library ieee ;
use ieee.std_logic_1164.all ;
use work.butter_lib.all ;
use ieee.std_logic_arith.all ;
use std.textio.all ;
use ieee.std_logic_textio.all ;
use ieee.std_logic_unsigned.all ;

entity normalize is 
port ( 
      a , b : in std_logic_vector (31 downto 0) ;
      numb : in std_logic_vector (32 downto 0) ;
      exp : in std_logic_vector (7 downto 0) ;
      signbit , addsub , clock , en_norm , rst_norm  : in std_logic  ;
      zero_detect : in std_logic_vector(1 downto 0) ;
      exit_n : out std_logic ;
      normal_sum : out std_logic_vector (31 downto 0) ) ;
end normalize ;

architecture rtl of normalize is
begin
process (clock)
variable numb_temp : std_logic_vector (31 downto 0) ;
variable temp_exp : std_logic_vector (7 downto 0) ; 
variable t , t2 : std_logic := '1' ;
begin
if (rst_norm = '0') then
if (en_norm = '1') then
if (t = '1') then
numb_temp := numb(31 downto 0) ;
temp_exp := exp (7 downto 0) ;
t := '0';
end if ;
if (t2 = '1') then
if (zero_detect = 0) then
 if (addsub = '0') then
  if (numb_temp = 0) then
  normal_sum <= numb_temp(31 downto 0) ; 
  exit_n <= '1' ;
  t2 := '0' ;
  elsif (numb_temp(31) = '1' and clock = '1') then
   normal_sum <= signbit & temp_exp(7 downto 0) & numb_temp(30 downto 8) ;--check!!
   exit_n <= '1' ;
   t2 := '0' ;
  elsif (clock = '1') then
   numb_temp := numb_temp(30 downto 0) & '0' ;
   temp_exp := temp_exp - "00000001" ;   
  end if ;
 elsif (addsub = '1' and numb(32) = '1' and clock = '1') then
 temp_exp := temp_exp + "00000001" ;
 normal_sum <= signbit & temp_exp(7 downto 0) & numb_temp(31 downto 9) ;
 exit_n <= '1' ;
 t2 := '0' ;
 elsif (clock = '1') then
 normal_sum <= signbit & temp_exp(7 downto 0) & numb_temp(30 downto 8) ;
 exit_n <= '1' ;
 t2 := '0' ;
 end if;
elsif (zero_detect = 1) then
normal_sum <= b;
exit_n <= '1' ;
t2 := '0' ;
elsif (zero_detect = 2) then
normal_sum <= a ;
exit_n <= '1' ;
t2 := '0' ;
end if ;
end if ;
end if ;
elsif (rst_norm = '1') then
exit_n <= '0' ;
t := '1' ;
t2 := '1' ;
end if ;
end process ;
end rtl ;



 











