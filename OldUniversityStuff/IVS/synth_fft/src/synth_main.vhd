library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity synth_main is
port (
      data_io : in std_logic_vector(31 downto 0);
      final_op : out std_logic_vector(31 downto 0) ;
      clock_main,clock,enbl,reset,init : in std_logic) ;     
end synth_main ;

architecture rtl of synth_main is 
signal shft , waves : std_logic_vector(3 downto 0) ;

component subtractor 
  port ( 
       a : in std_logic_vector (31 downto 0) ;
       b : in std_logic_vector (31 downto 0) ;
       clock , rst_sub , sub_en : in std_logic ;
       a_smaller , fin_sub , num_zero : out std_logic ;
       zero_detect : out std_logic_vector(1 downto 0) ;
       sub : out std_logic_vector (8 downto 0);
       change : out std_logic ) ;
end component ;

component swap
 port (
       a : in std_logic_vector (31 downto 0) ;
       b : in std_logic_vector (31 downto 0) ;
       clock : in std_logic ;
       rst_swap , en_swap : in std_logic ;
       finish_swap : out std_logic ;
       d : out std_logic_vector (31 downto 0) ;
       large_exp : out std_logic_vector (7 downto 0) ;
       c  : out std_logic_vector (32 downto 0 ) ) ;
end component ;

 
component shift2
 port (
       sub_control : in std_logic_vector (8 downto 0) ;	
       c_in  : in std_logic_vector (32 downto 0) ;
       shift_out : out std_logic_vector (31 downto 0) ;
       clock , shift_en , rst_shift : in std_logic ;
       finish_out : out std_logic ) ;
end component ;

component control_main
 port ( 
       a_small , sign_a , sign_b : in std_logic ;
       sign_out , add_sub , reset_all : out std_logic ;
       en_sub , en_swap , en_shift , addpulse , normalise : out std_logic ;
       fin_sub , fin_swap , finish_shift , add_finish , end_all : in std_logic ;
       clock_main , clock , reset , enbl , zero_num , change : in std_logic ) ;
end component ;

component summer 
 port ( 
       num1 , num2 : in std_logic_vector (31 downto 0) ;
       exp : in std_logic_vector (7 downto 0) ;
       addpulse_in , addsub , rst_sum : in std_logic ;
       add_finish : out std_logic ;
       sumout : out std_logic_vector ( 32 downto 0) ) ;
end component ;

component normalize
 port ( 
      a , b : in std_logic_vector (31 downto 0) ;
      numb : in std_logic_vector (32 downto 0) ;
      exp : in std_logic_vector (7 downto 0) ;
      signbit , addsub , clock , en_norm , rst_norm  : in std_logic  ;
      zero_detect : in std_logic_vector(1 downto 0) ;
      exit_n : out std_logic ;
      normal_sum : out std_logic_vector (31 downto 0) ) ;
end component ;

component but_gen
port (
      add_incr , add_clear , stagedone : in std_logic ;
      but_butterfly : out std_logic_vector(3 downto 0) ) ;
end component ;

component stage_gen 
port (
      add_staged , add_clear : in std_logic ;
      st_stage : out std_logic_vector(1 downto 0) ) ;  
end component ;

component iod_staged is
port (
      but_fly : in std_logic_vector(3 downto 0) ;
      stage_no : in std_logic_vector(1 downto 0) ;
      add_incr , io_mode  : in std_logic ;
      add_iod , add_staged , add_fftd : out std_logic ; 
      butterfly_iod : out std_logic_vector(3 downto 0) ) ;
end component ;

component baseindex
port (
      ind_butterfly : in std_logic_vector(3 downto 0) ;
      ind_stage : in std_logic_vector(1 downto 0) ;
      add_fft : in std_logic ;
      fftadd_rd : out std_logic_vector(3 downto 0) ;
      c0 , c1 , c2 , c3 : in std_logic ) ; 
end component ;

component ioadd_gen
port (
      io_butterfly : in std_logic_vector(3 downto 0) ;
      add_iomode , add_ip , add_op : in std_logic ;
      base_ioadd : out std_logic_vector(3 downto 0) ) ;
end component ;

component mux_add 
port (
      a , b : in std_logic_vector(3 downto 0) ;
      sel : in std_logic ;
      q : out std_logic_vector(3 downto 0) ) ;
end component ;

component ram_shift
port (
      data_in : in std_logic_vector(3 downto 0) ;
      clock_main : in std_logic ;
      data_out : out std_logic_vector(3 downto 0) ) ;
end component ;

component cycles
port (
      clock_main , preset , c0_en , cycles_clear : in std_logic ;
      waves : out std_logic_vector(3 downto 0) ) ;
end component ;

component counter 
port (
      c : out std_logic_vector(2 downto 0) ;
      disable , clock_main , reset : in std_logic) ;
end component ;


component mult_clock
port (
      clock_main , mult1_c0 , mult1_iomode , mult_clear : in std_logic ;
      mult1_addincr : out std_logic ) ;
end component ;

component cont_gen 
port (
      con_staged , con_iod , con_fftd , con_init : in std_logic ;
      con_ip , con_op , con_iomode , con_fft : out std_logic ;
      con_enbw , con_enbor , c0_enable , con_preset : out std_logic ;
      con_clear , disable : out std_logic ;
      c0 , clock_main : in std_logic ;
      en_rom , en_romgen , reset_counter : out std_logic ; 
      con_clkcount : in std_logic_vector(2 downto 0) ) ;
end component ;

component and_gates 
port (
      waves_and : in std_logic_vector(3 downto 0) ;
      clock_main , c0_en : in std_logic ;
      c0,c1,c2,c3 : out std_logic ;
      c0_c1,c2_c3,c0_c2,c1_c3 : out std_logic ) ;
end component ;

component r_block
port (
       data : in std_logic_vector(31 downto 0) ;
       trigger : in std_logic ;
       r_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component l_block
port (
       data_l : in std_logic_vector(31 downto 0) ;
       trigger_l : in std_logic ;
       l_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component level_edge  
 port (
       data_edge : in std_logic_vector(31 downto 0) ;
       trigger_edge : in std_logic ;
       edge_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component mux 
port (
       d0 , d1 : in std_logic_vector(31 downto 0) ;
       mux_out : out std_logic_vector(31 downto 0) ;
       choose : in std_logic ) ;
end component ;

component negate 
port (
       neg_in : in std_logic_vector(31 downto 0) ;
       neg_en , clock_main : in std_logic ;
       neg_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component multiply
port(
      num_mux , num_rom : in std_logic_vector(31 downto 0) ;
      clock  : in std_logic ;
      mult_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component divide
port (
       data_in : in std_logic_vector(31 downto 0) ;
       data_out : out std_logic_vector(31 downto 0) ) ;
end component ;

component romadd_gen is
port (
      io_rom,c0,c1,c2,c3 : in std_logic ;
      stage_rom : in std_logic_vector(1 downto 0) ;
      butterfly_rom : in std_logic_vector(3 downto 0) ;
      romadd : out std_logic_vector(2 downto 0) ;
      romgen_en : in std_logic );
end component ;

component reg_dpram is
port (
      data_fft , data_io : in std_logic_vector (31 downto 0);
      q : out std_logic_vector (31 downto 0);
      clock , io_mode : in std_logic;
      we , re : in std_logic;
      waddress: in std_logic_vector (3 downto 0);
      raddress: in std_logic_vector (3 downto 0));
end component ;

component rom is
port (
      clock , en_rom : in std_logic ;
      romadd : in std_logic_vector(2 downto 0) ;
      rom_data : out std_logic_vector(31 downto 0) ) ;
end component ;

component print_result is
port (clock,op : in std_logic ;
    --  fin_res : out std_logic_vector(31 downto 0) ;
      result : in std_logic_vector(31 downto 0));
end component ;

begin
--,final_op
result : print_result port map (clock_main,op,ram_data) ;
but : but_gen port map (incr , clear , staged ,butterfly_iod) ;
stg : stage_gen port map (staged , clear , stage) ;
iod_stgd : iod_staged port map(butterfly_iod,stage,incr,io_mode,iod,staged,fftd,butterfly) ; 
base : baseindex port map (butterfly , stage , fft_en , fftadd_rd , c0 , c1 , c2 , c3) ;
ioadd : ioadd_gen port map (butterfly , io_mode , ip , op , io_add) ;
ram_shift1 : ram_shift port map (fftadd_rd , clock_main , shift1) ;
ram_shift2 : ram_shift port map (shift1 , clock_main , shft) ;
ram_shift3 : ram_shift port map (shft , clock_main , shift3) ;
ram_shift4 : ram_shift port map (shift3 , clock_main ,shift4) ;
ram_shift5 : ram_shift port map (shift4 , clock_main , shift5) ;
--ram_shift6 : ram_shift port map (shift5 , clock_main , shift6) ;
multx1 : mux_add port map (shift5 , io_add , io_mode , ram_wr) ;
multx2 : mux_add port map (fftadd_rd , io_add , io_mode , ram_rd) ;
cyc : cycles port map (clock_main , preset , c0_en , cyc_clear , waves) ;
gates : and_gates port map(waves,clock_main,c0_en,c0,c1,c2,c3,c0_c1,c2_c3,c0_c2,c1_c3) ;
cnt : counter port map (clk_count , disable , clock_main , reset_count) ; 
mux_clock : mult_clock port map (clock_main , c0 , io_mode , clear , incr) ;
control : cont_gen port map (staged , iod , fftd , init , ip , op , io_mode , fft_en ,
enbw , enbor , c0_en , preset , clear , disable , c0 , clock_main ,rom_en,romgen_en,reset_count,clk_count) ;

reg_ram : reg_dpram port map (out_data,data_io,ram_data,clock_main,io_mode,enbw,enbor,ram_wr,ram_rd) ;

f1 : r_block port map (ram_data , c0 , d2) ;
f2 : l_block port map (ram_data , c1 , d3) ;
f3 : r_block port map (ram_data , c2 , d4) ;
f4 : r_block port map (ram_data , c3 , d5) ;
f5 : r_block port map (d8 , c1_c3 , d9) ;
f6 : l_block port map (d8 , c0_c2 , d10) ;
f7 : l_block port map (d12 , c3 , d13) ;
f8 : l_block port map (d12 , c1 , d14) ;
f9 : r_block port map (d17 , clock_main , d18) ;
f10 : r_block port map (data_rom , clock_main , rom_ff) ;
mux1 : mux port map (d2 , d3 , d6 , c2_c3) ;
mux2 : mux port map (d4 , d5 , d7 , c1_c3) ;
mux3 : mux port map (d13 , d14 , d15 , c1_c3) ;
neg1 : negate port map (d10 , c0_c1 ,clock_main , d11) ;
neg2 : negate port map (d15 , c0_c1 ,clock_main , d16) ;
mult1 : multiply port map (d6 , rom_ff , clock_main , d8) ;
div : divide port map (d18 , d19) ;
f11 : level_edge port map (d19,clock_main,out_data) ;

rom_add1 : romadd_gen port map (io_mode,c0,c1,c2,c3,stage,butterfly,rom_add,romgen_en) ;
rom1 : rom port map (clock ,rom_en,rom_add,data_rom) ;

b11 : subtractor port map ( d16 , d7 , clock , rstb , ensubb , a_smallb , finsubb , numzerob , zerodetectb , subb ,  changeb) ;
b2 : swap port map ( a=>d16 , b=>d7 , clock=>clock , rst_swap=>rstb , en_swap=>enswapb , finish_swap=>finswapb , d=>swap_num2b , large_exp=>expb , c=>swap_num1b ) ;
b4 : shift2 port map (sub_control=>subb , c_in=>swap_num1b , shift_out=>shift_outb , clock=>clock , shift_en=>enshiftb,
rst_shift=>rstb , finish_out=>finshiftb ) ;
b5 : control_main port map ( a_smallb , d16(31) , d7(31) , signbitb , addsubb , rstb , ensubb , 
enswapb , enshiftb , addpulseb , normaliseb , finsubb , finswapb , finshiftb ,finish_sumb , end_allb , 
clock_main , clock , reset , enbl , numzerob , changeb ) ;
b6 : summer port map ( shift_outb , swap_num2b , expb , addpulseb , addsubb , rstb , finish_sumb , sum_outb ) ;
b7 : normalize port map (d16 , d7 , sum_outb , expb , signbitb , addsubb , clock , normaliseb , rstb , zerodetectb , end_allb , d17) ;

a1 : subtractor port map ( d9 ,  d11 , clock , rst , ensub , a_small , finsub , numzero , zerodetect , suba , changea) ;
a2 : swap port map (d9 ,d11 ,clock ,rst ,enswap , finswap ,swap_num2 , exp , swap_num1 ) ;
a4 : shift2 port map (suba ,swap_num1 ,shift_outa ,clock , enshift , rst , finshift ) ;
a5 : control_main port map ( a_small , d9(31) , d11(31) , signbit , addsub , rst , ensub , 
enswap , enshift , addpulse , normalise , finsub , finswap , finshift ,finish_sum , end_all , 
clock_main , clock , reset , enbl , numzero , changea ) ;
a6 : summer port map ( shift_outa , swap_num2 , exp , addpulse , addsub , rst , finish_sum , sum_out ) ;
a7 : normalize port map (d9 , d11 , sum_out , exp , signbit , addsub , clock , normalise , rst , zerodetect , end_all , d12) ;

end rtl ;

