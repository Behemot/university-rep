-- DIVIDE BY TWO UNIT. THIS FILE HOWEVER PASSED THE DATA UNCHANGED
-- BECAUSE DIVISION IS REQUIRED ONLY IF SCALING IS USED TO AVOID OVERFLOW.
--  NO SCALING WAS USED IN THIS PROJECT, SO THAT RESULTS OF MATLAB MATCHED WITH OURS
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity divide is 
 port (
       data_in : in std_logic_vector(31 downto 0) ;
       data_out : out std_logic_vector(31 downto 0) ) ;
end divide ;
 
architecture rtl of divide is
begin
process(data_in)
variable divide_exp : std_logic_vector(7 downto 0) ;
variable divide_mant : std_logic_vector(31 downto 0) ;
begin
if (data_in = "00000000000000000000000000000000") then 
data_out <= "00000000000000000000000000000000" ;
elsif (data_in = "10000000000000000000000000000000") then
data_out <= "00000000000000000000000000000000" ;
else
divide_exp := data_in(30 downto 23) ;
divide_mant := data_in (31 downto 0) ;
divide_exp := divide_exp - "00000001" ;
--data_out <= divide_mant(31) & divide_exp(7 downto 0) & divide_mant(22 downto 0)  ;
data_out <= data_in(31 downto 0) ; -- pass data unchanged
end if ;
end process ;
end rtl ;
