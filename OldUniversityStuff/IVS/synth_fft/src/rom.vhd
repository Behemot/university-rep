-- ROM TO STORE SINE AND COSINE VALUES
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity rom is
port (
      clock , en_rom : in std_logic ;
      romadd : in std_logic_vector(2 downto 0) ;
      rom_data : out std_logic_vector(31 downto 0) ) ;
end rom ;

architecture rtl of rom is
begin 
process(clock,en_rom)
begin
if(en_rom = '1') then
if(clock = '1') then
case romadd is
 when "000" =>
 rom_data <= "00111111100000000000000000000000" ;
 when "001" =>
 rom_data <= "00000000000000000000000000000000" ;
 when "010" =>
 rom_data <= "00111111001101010000010010000001" ;
 when "011" =>
 rom_data <= "00111111001101010000010010000001" ;
 when "100" =>
 rom_data <= "00000000000000000000000000000000" ;
 when "101" =>
 rom_data <=  "00111111100000000000000000000000" ;
 when "110" =>
 rom_data <= "10111111001101010000010010000001"  ;
 when "111" =>
 rom_data <= "00111111001101010000010010000001" ;
 when others => 
 rom_data <= "01000000000000000000000000000000" ;
end case ;
end if ;
end if ;
end process ;
end rtl ;



