-- NEGATIVE EDGE TRIGGERED FLIP FLOPS
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity r_block is 
 port (
       data : in std_logic_vector(31 downto 0) ;
       trigger : in std_logic ;
       r_out : out std_logic_vector(31 downto 0) ) ;
end r_block ;

architecture rtl of r_block is
begin
process(data , trigger)
begin
if (trigger='0' and trigger'event) then
r_out <= data(31 downto 0) ;
end if ;
end process ;
end rtl ;