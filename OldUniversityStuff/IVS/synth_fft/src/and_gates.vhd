--THIS FILE RECEIVES THE OUTPUT OF THE WAVEFORM GENERATOR AND 
--OUTPUTS THE REQUIRED CYCLES

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use work.butter_lib.all ;

entity and_gates is
port (
      waves_and : in std_logic_vector(3 downto 0) ;
      clock_main , c0_en : in std_logic ;
      c0,c1,c2,c3 : out std_logic ;
      c0_c1,c2_c3,c0_c2,c1_c3 : out std_logic ) ;
end and_gates ;

architecture rtl of and_gates is
begin 
process(clock_main,waves_and)
begin
if (c0_en = '1' and clock_main='1') then

c0 <= waves_and(3) ;
c1 <= waves_and(2) ;
c2 <= waves_and(1) ;
c3 <= waves_and(0) ;
c0_c1 <= waves_and(3) or waves_and(2) ;
c0_c2 <= waves_and(3) or waves_and(1) ;
c2_c3 <= waves_and(1) or waves_and(0) ;
c1_c3 <= waves_and(0) or waves_and(2) ;
else 
c0 <= '0' ;
c1 <= '0' ;
c2 <= '0' ;
c3 <= '0' ;
c0_c1 <= '0' ;
c0_c2 <= '0' ;
c2_c3 <= '0' ;
c1_c3 <= '0' ;
end if ;
end process ;
end rtl ;
