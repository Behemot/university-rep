library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
 
entity control_main is
port ( 
       a_small , sign_a , sign_b : in std_logic ;
       sign_out , add_sub , reset_all : out std_logic ;
       en_sub , en_swap , en_shift , addpulse , normalise : out std_logic ;
       fin_sub , fin_swap , finish_shift , add_finish , end_all : in std_logic ;
       clock_main , clock , reset , enbl , zero_num , change: in std_logic ) ;
end control_main ; 
 
architecture rtl of control_main is
signal current_state , next_state : state ;
begin
process (current_state , add_finish , finish_shift , end_all , enbl , clock_main , fin_sub , fin_swap , zero_num , change)
begin

case current_state is
 when reset1 =>
  if( enbl = '1' and clock_main = '1') then
  normalise <= '0' ;
  addpulse <= '0' ;
  reset_all <= '1' ;
  next_state <= reset2 ;
  elsif ( enbl = '0') then
  next_state <= reset7 ;-- last state to exit 
  else 
  next_state <= reset1 ;
  end if ;

 when reset2 =>
  reset_all <= '0' ; -- else values willnot cchange
  en_sub <= '1' ;
  en_swap <= '1' ;
  next_state <= reset3 ;

 when reset3 =>
  if (zero_num = '1') then
  next_state <= reset5 ;
  en_sub <= '0' ;
  en_swap <= '0' ;
  elsif(fin_sub= '1') then
  if(fin_swap = '1') then
  en_shift <= '1' ;
  en_sub <= '0' ;
  en_swap <= '0' ;
  next_state <= reset4 ;
  end if ;
  else
  next_state <= reset3 ;
  end if ;
  
 when reset4 =>
 if (finish_shift = '1') then
 en_shift <= '0' ;
 addpulse <= '1' ;
 next_state <= reset5 ;
 else
 next_state <= reset4 ;
 end if ;

 when reset5 =>
 if (zero_num = '1') then
 normalise <= '1' ;
 next_state <= reset6 ;
 elsif (add_finish = '1') then
 normalise <= '1' ;
 addpulse <= '0' ;
 next_state <= reset6 ;
 else
 next_state <= reset5 ;
 end if ;
 
 when reset6 =>
 if (end_all = '1' and clock_main = '1') then
 normalise <= '0' ;
 next_state <= reset6 ;
 elsif (end_all = '1' and clock_main = '0') then
 next_state <= reset1 ;
 else
 next_state <= reset6 ;
 end if ;

 when reset7 =>
 next_state <= reset7 ;

 when others =>
 next_state <= reset1 ;

end case ;
end process ;

process(clock , reset , change)
begin
if(change = '1') then
current_state <= reset1 ;
elsif (reset = '1') then
current_state <= reset1 ;
elsif (clock= '1' and clock'event) then
current_state <= next_state ;
end if ;
end process ;

process (a_small , sign_a , sign_b)
begin
if (sign_a = '0' and sign_b = '0') then
sign_out <= '0' ;
add_sub <= '1' ;

elsif (sign_a = '1' and sign_b = '1') then
sign_out <= '1' ;
add_sub <= '1' ;

elsif (a_small = '1' and sign_a = '0') then
sign_out <= '1' ;
add_sub <= '0' ;

elsif (a_small = '0' and sign_a = '1') then
sign_out <= '1' ;
add_sub <= '0' ;

else 
sign_out <= '0' ;
add_sub <= '0' ;
end if ;

end process ;

end rtl ;
















 


