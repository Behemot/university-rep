-- POSITIVE LEVEL TRIGGERED FLIP FLOPS
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity l_block is 
 port (
       data_l : in std_logic_vector(31 downto 0) ;
       trigger_l : in std_logic ;
       l_out : out std_logic_vector(31 downto 0) ) ;
end l_block ;

architecture rtl of l_block is
begin
process(data_l , trigger_l)
begin
if (trigger_l='1') then
l_out <= data_l ;
end if ;
end process ;
end rtl ;