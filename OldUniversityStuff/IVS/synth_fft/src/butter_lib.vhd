-- THIS FILE DECLARES THE SIGNALS USED IN THE PROCESSOR
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use ieee.std_logic_unsigned.all ;

package butter_lib is

signal ram_data,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,out_data : std_logic_vector(31 downto 0) := (others => '0') ;
signal data_rom , rom_ff : std_logic_vector(31 downto 0) ;
signal clock_main , reset , enbl , clock : std_logic := '0' ;
signal c0 , c1 , c2 , c3 , c0_c1 , c2_c3 , c0_c2 , c1_c3 : std_logic ;
signal c0_and,c1_and,c2_and,c3_and,c0_c1and,c2_c3and,c0_c2and,c1_c3and : std_logic ;
signal reset_count : std_logic ;
type state is (reset1 , reset2 , reset3 , reset4 , reset5 , reset6 , reset7) ;

signal final_sum : std_logic_vector (31 downto 0) := (others => '0') ;
signal shift , finish_sum , signbit , normalise , end_all , a_small , addsub ,sum_out2 , shift_done , done , num_rec , setbit ,  addpulse : std_logic := '0' ;
signal shift_outa , swap_num2 : std_logic_vector ( 31 downto 0 ) := (others => '0') ;
signal swap_num1 , sum_out : std_logic_vector (32 downto 0) := (others => '0') ;
signal sub2 : std_logic_vector (8 downto 0) := (others => '0') ;
signal suba : std_logic_vector (8 downto 0) := (others => '0') ;
signal exp : std_logic_vector (7 downto 0) := (others => '0') ;
signal rst , enswap , ensub , enshift , finsub , finswap , finshift , numzero : std_logic := '0' ;
signal zerodetect : std_logic_vector(1 downto 0) ;
signal changea : std_logic ;

signal final_sumb : std_logic_vector (31 downto 0) := (others => '0') ;
signal shiftb , finish_sumb , signbitb , normaliseb , end_allb , a_smallb , addsubb,sum_out2b , shift_doneb , doneb , num_recb , setbitb ,  addpulseb , clockb : std_logic := '0' ;
signal shift_outb , swap_num2b : std_logic_vector ( 31 downto 0 ) := (others => '0') ;
signal swap_num1b , sum_outb : std_logic_vector (32 downto 0) := (others => '0') ;
signal sub2b : std_logic_vector (8 downto 0) := (others => '0') ;
signal subb : std_logic_vector (8 downto 0) := (others => '0') ;
signal expb: std_logic_vector (7 downto 0) := (others => '0') ;
signal rstb , enswapb , ensubb , enshiftb , finsubb , finswapb , finshiftb , numzerob , clock_mainb , resetb , enblb : std_logic := '0' ;
signal zerodetectb : std_logic_vector(1 downto 0) ;
signal changeb : std_logic ;

signal incr , clear , io_mode , staged , iod : std_logic ;
signal butterfly,fftadd_rd,shift1,shift3,shift4,shift5,shift6,ram_wr,ram_rd,io_add : std_logic_vector(3 downto 0) := (others => '0') ;
signal fftd , fft_en , ip , op , init : std_logic ;
signal stage : std_logic_vector(1 downto 0) ;
--signal clock_main,c0,c1,c2,c3,c0_c1,c2_c3,c0_c2,c1_c3 : std_logic ;
signal preset,disable,c0_en,rom_en,romgen_en : std_logic ;
signal clk_count : std_logic_vector(2 downto 0) ;
signal enbw , enbor : std_logic ;
signal data_io : std_logic_vector(31 downto 0) := (others => '0') ;
signal rom_add : std_logic_vector(2 downto 0) ;
type state_values is (st0 , st1 , st2 ,  st3) ;
signal pres_state1 , next_state1 : state_values ;

signal butterfly_iod : std_logic_vector(3 downto 0) ;
signal cyc_clear : std_logic ;
signal add_rd , add_wr : std_logic_vector(3 downto 0) ;  

end butter_lib ;


