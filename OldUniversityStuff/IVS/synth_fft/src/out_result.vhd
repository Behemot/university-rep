-- OUTPUT RESULTS. SYNTHESISABLE
library ieee ;
use ieee.std_logic_1164.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity print_result is
port (clock,op : in std_logic ;
      fin_res : out std_logic_vector(31 downto 0) ;
      result : in std_logic_vector(31 downto 0));
end print_result ;

architecture rtl of print_result is
begin
process(op,clock)
variable count : integer := 1  ;
begin 
if (op = '1') then
if (count < 17) then
if(clock='0' and clock'event) then
fin_res <= result ;
count := count + 1 ;
end if ;
end if ;
end if ;
end process ;
end rtl ;







