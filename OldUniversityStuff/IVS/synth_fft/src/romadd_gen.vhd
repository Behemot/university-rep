-- ADDRESS GENERATOR FOR ROM
library ieee ;
use ieee.std_logic_1164.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity romadd_gen is
port (
      io_rom,c0,c1,c2,c3 : in std_logic ;
      stage_rom : in std_logic_vector(1 downto 0) ;
      butterfly_rom : in std_logic_vector(3 downto 0) ;
      romadd : out std_logic_vector(2 downto 0) ;
      romgen_en : in std_logic );
end romadd_gen ;

architecture rtl of romadd_gen is
begin 
process(io_rom,c0,c1,c2,c3,stage_rom,butterfly_rom)
begin
if(romgen_en = '1') then
if(io_rom = '0') then
 case stage_rom is

 when "00" =>
 if(c0='1' or c2='1') then
 romadd <= "000" ;
 elsif(c1='1' or c3='1') then
 romadd <= "001" ;
 end if ;

 when "01" =>
 if(butterfly_rom=0 or butterfly_rom=1) then
  if(c0='1' or c2='1') then
  romadd <= "000" ;
  elsif(c1='1' or c3='1') then
  romadd <= "001" ;
  end if ;
 elsif(butterfly_rom=2 or butterfly_rom=3) then
  if(c0='1' or c2='1') then
  romadd <= "100" ;
  elsif(c1='1' or c3='1') then
  romadd <= "101" ;
  end if ;
 end if ;

 when "10" =>
  if(butterfly_rom=0) then
   if(c0='1' or c2='1') then
   romadd <= "000" ;
   elsif(c1='1' or c3='1') then
   romadd <= "001" ;
   end if ;
  elsif(butterfly_rom=1) then
   if(c0='1' or c2='1') then
   romadd <= "100" ;
   elsif(c1='1' or c3='1') then
   romadd <= "101" ;
   end if ;
  elsif(butterfly_rom=2) then
   if(c0='1' or c2='1') then
   romadd <= "010" ;
   elsif(c1='1' or c3='1') then
   romadd <= "011" ;
   end if ;
  elsif (butterfly_rom=3) then
   if(c0='1' or c2='1') then
   romadd <= "110" ;
   elsif(c1='1' or c3='1') then
   romadd <= "111" ;
   end if ;
  end if ;

 when others =>
  romadd <= "000" ;

 end case ;
end if ;
end if ;
end process ;
end rtl ;

   























