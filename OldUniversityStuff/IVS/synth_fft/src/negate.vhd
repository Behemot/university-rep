--NEGATION UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity negate is
 port (
       neg_in : in std_logic_vector(31 downto 0) ;
       neg_en , clock_main : in std_logic ;
       neg_out : out std_logic_vector(31 downto 0) ) ;
end negate ;

architecture rtl of negate is
begin
process(neg_in , neg_en , clock_main) 
variable neg_temp : std_logic_vector(31 downto 0) ;
begin
neg_temp := neg_in(31 downto 0) ;
if (clock_main = '1') then
if (neg_en = '1') then
if(neg_in(31) = '0') then
neg_temp := '1' & neg_temp (30 downto 0) ;
else
neg_temp := '0' & neg_temp (30 downto 0) ;
end if ;
neg_out <= neg_temp ;
else
neg_out <= neg_in(31 downto 0) ;
end if ;
end if ;
end process ;
end rtl ;

