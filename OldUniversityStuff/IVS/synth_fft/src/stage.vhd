-- STAGE NUMBER GENERATOR.
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity stage_gen is
port (
      add_staged , add_clear : in std_logic ;
      st_stage : out std_logic_vector(1 downto 0) ) ;  
end stage_gen ;

architecture rtl of stage_gen is
begin
process(add_staged , add_clear)
variable s_count : std_logic_vector(1 downto 0) ;
begin
if (add_clear = '1') then
st_stage <= "00" ;
s_count := "00" ;
elsif(add_staged'event and add_staged= '1' ) then
st_stage <= s_count + 1 ;
s_count := s_count + 1 ;
end if ;
end process ;
end rtl ;
  
