-- MULTIPLEXER TO CHOOSE BETWEEN CLOCK AND C0
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity mult_clock is
port (
      clock_main , mult1_c0 , mult1_iomode , mult_clear : in std_logic ;
      mult1_addincr : out std_logic ) ;
end mult_clock ;

architecture rtl of mult_clock is
begin
process(clock_main , mult1_c0 , mult1_iomode , mult_clear) 
variable temp1 : std_logic ;
variable temp2 : std_logic ;
begin
if(mult1_iomode = '0') then -- ie, fft computation mode
temp2 := mult1_c0 ; 
elsif(mult1_iomode = '1') then -- ie, io mode
temp1 := clock_main ;
end if ;
if (mult1_iomode = '1') then
mult1_addincr <= temp1 ;
elsif(mult1_iomode = '0') then
mult1_addincr <= temp2 ;
end if ;
end process ;
end rtl ;