--BASE INDEX GENERATOR
library ieee;
use ieee.std_logic_1164.all;
use work.butter_lib.all ;

entity baseindex is
 port(
   ind_butterfly: in std_logic_vector(3 downto 0);
   ind_stage: in std_logic_vector(1 downto 0);
   add_fft: in std_logic;
   fftadd_rd: out std_logic_vector(3 downto 0);
   c0,c1,c2,c3: in std_logic);
end baseindex;

architecture rtl of baseindex is
begin
process(ind_butterfly,ind_stage,add_fft,c0,c1,c2,c3)
variable out_sig : std_logic_vector(3 downto 0);
begin
if (add_fft='1') then
if(c2='1') then -- address for 'x'. Since this is the real part, 
case ind_stage is -- M.S.B is '0'.
   when "00" => out_sig := "00" & ind_butterfly(1 downto 0);
   when "01" => out_sig := '0' & ind_butterfly(1) & '0' & ind_butterfly(0);
-- when "10" => out_sig := '0' & '1' & '1' & ind_butterfly(3);
   when "10" => out_sig := '0' & ind_butterfly(1 downto 0) & '0';
   when others => out_sig := "0000";
end case;
 
elsif(c0='1') then -- address for 'y'.
case ind_stage is
  when "00" => out_sig := "01" & ind_butterfly(1 downto 0);
  when "01" => out_sig := '0' & ind_butterfly(1) & '1' & ind_butterfly(0);
  when "10" => out_sig := '0' & ind_butterfly(1 downto 0) & '1';
  when others => out_sig := "0000";
end case;

elsif(c1='1') then -- addresss for 'Y'
case ind_stage is
  when "00" => out_sig := "11" & ind_butterfly(1 downto 0);
  when "01" => out_sig := '1' & ind_butterfly(1) & '1' & ind_butterfly(0);
  when "10" => out_sig := '1' & ind_butterfly(1 downto 0) & '1';
  when others => out_sig := "0000";
end case;

elsif(c3='1') then -- address for 'X'
case ind_stage is
 when "00" => out_sig := "10" & ind_butterfly(1 downto 0);
  when "01" => out_sig := '1' & ind_butterfly(1) & '0' & ind_butterfly(0);
  when "10" => out_sig := '1' & ind_butterfly(1 downto 0) & '0';
  when others => out_sig := "0000";
--else
--out_sig := "ZZZZ";
end case;
end if;
end if;
fftadd_rd <= out_sig (3 downto 0) ;
end process;
end rtl;


