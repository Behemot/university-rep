-- POSITIVE EDGE TRIGGERED FLIPFLOPS PLACED BEFORE THE DIVIDE BY TWO UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity level_edge is 
 port (
       data_edge : in std_logic_vector(31 downto 0) ;
       trigger_edge : in std_logic ;
       edge_out : out std_logic_vector(31 downto 0) ) ;
end level_edge ;

architecture rtl of level_edge is
begin
process(data_edge , trigger_edge)
begin
if (trigger_edge='1' and trigger_edge'event) then
edge_out <= data_edge(31 downto 0) ;
end if ;
end process ;
end rtl ;