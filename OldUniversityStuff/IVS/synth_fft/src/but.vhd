-- BUTTERFLY GENERATOR
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity but_gen is
port (
      add_incr , add_clear , stagedone : in std_logic ;
      but_butterfly : out std_logic_vector(3 downto 0) ) ;
end but_gen ;

architecture rtl of but_gen is
begin
process(add_clear , add_incr , stagedone)
variable cnt : integer ;
variable count : std_logic_vector(3 downto 0) ;
begin
if(add_clear = '1' or stagedone = '1') then
count := "0000" ;
but_butterfly <= "0000" ;
elsif (add_incr'event and add_incr = '1') then
but_butterfly <= (count + 1) ;
count := count + 1 ;
end if ;
end process ;
end rtl ;




















