-- USED TO PRINT THE RESULTS IN A NEAT FORMAT. NOT SYNTHESISABLE.
-- USED ONLY FOR SIMULATION PURPOSE. 
library ieee ;
use ieee.std_logic_1164.all ;
use std.textio.all ;
use work.butter_lib.all ;
use ieee.std_logic_textio.all ;
use ieee.std_logic_unsigned.all ;

entity print_result is
port (clock, op : in std_logic ;
      result : in std_logic_vector(31 downto 0));
end print_result ;

architecture rtl of print_result is
begin
process(op,clock)
file vectorw_file : text open write_mode is "d:\user\ray\project\vhdl_code\synth_but\result.txt" ;
variable l , l2 : line ;
variable q , p : integer := 0 ;
variable count : integer := 1  ;
begin 
if (op = '1') then
--if(count=1) then
--q:=31;
--for p in 1 to 32 loop -- data from RAM
--write(l2 , '0') ;
--q := q - 1 ;
--end loop ;
--q := 31 ;
--writeline(vectorw_file , l2) ;
--count := 0 ;
if (count < 17) then
if(clock='0' and clock'event) then
q := 31 ;
count := count + 1 ;
for p in 1 to 32 loop -- data from RAM
write(l2 , result(q)) ;
q := q - 1 ;
end loop ;
q := 31 ;
writeline(vectorw_file , l2) ;
end if ;
end if ;
end if ;
end process ;
end rtl ;

--while not endfile(vector_file) loop
--for count in 1 to 16 loop





