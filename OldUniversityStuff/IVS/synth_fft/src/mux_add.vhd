-- multiplexer in the address generation unit
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity mux_add is
port (
      a , b : in std_logic_vector(3 downto 0) ;
      sel : in std_logic ;
      q : out std_logic_vector(3 downto 0) ) ;
end mux_add ;

architecture rtl of mux_add is
begin
process (a , b , sel)
begin
if(sel = '0') then
q <= a(3 downto 0) after 2 ns ;
elsif(sel = '1') then
q <= b(3 downto 0) after 2 ns ;
end if ;
end process ;
end rtl ;