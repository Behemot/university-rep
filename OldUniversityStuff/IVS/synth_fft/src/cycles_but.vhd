--WAVEFORM GENERATOR
-- THE 4 BITS OF "DATA_OUT" ARE "C0 C1 C2 C3"
library ieee ; 
use ieee.std_logic_1164.all ;
use work.butter_lib.all ;

entity cycles is 
port (
      clock_main , preset , c0_en , cycles_clear : in std_logic ;
      waves : out std_logic_vector(3 downto 0) ) ;
end cycles ;
architecture rtl of cycles is
--type state_values is (st0 , st1 , st2 ,  st3) ;
--signal pres_state1 , next_state1 : state_values ;
shared variable data_out : std_logic_vector(3 downto 0)  ;
begin
process (clock_main , preset , c0_en,cycles_clear)
variable t : std_logic ;
begin
if (c0_en = '1') then
 if (preset = '1' and t='1')then
 pres_state1 <= st0 ;
 t := '0' ;
 elsif (clock_main'event and clock_main= '0') then
 pres_state1 <= next_state1 ;
 end if ;
end if ;
if(cycles_clear = '1') then
t := '1' ;
end if ;
end process ;

process(pres_state1 , c0_en , clock_main)
variable temp_clock : std_logic ;
begin

 case pres_state1 is
  when st0 =>
   data_out := "1000" ; 
   next_state1 <= st1 ;

  when st1 =>
    data_out :=  "0100" ;
    next_state1 <= st2 ;
 
 when st2 =>
   data_out := "0010" ;
   next_state1 <= st3 ;

 when st3 =>
   data_out := "0001" ;
   next_state1 <= st0 ;

 when others =>
    next_state1 <= st0 ;

end case ;

waves <= data_out ;

end process ;
end rtl ;





















