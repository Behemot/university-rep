-- MULTIPLY UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity multiply is
 port(
      num_mux , num_rom : in std_logic_vector(31 downto 0) ;
      clock : in std_logic ;
      mult_out : out std_logic_vector(31 downto 0) ) ;
end multiply ;

architecture rtl of multiply is
begin
process(num_mux , num_rom , clock)
variable sign_mult , t : std_logic := '0' ;
variable temp1 , temp2 : std_logic_vector(22 downto 0) ;
variable exp_mux , exp_rom : std_logic_vector(7 downto 0) ;
variable mant_temp : std_logic_vector(45 downto 0) ;
variable exp_mult , mux_temp , rom_temp : std_logic_vector(8 downto 0) ;
variable res_temp : std_logic_vector(31 downto 0) ;
begin

temp1 := '1' & num_mux(22 downto 1) ; -- '1' for implicit '1'. 
temp2 := '1' & num_rom(22 downto 1) ;
if (num_mux(31) = '1' and num_rom(31) = '1' and clock = '1') then -- sign of results
sign_mult := '0' ;
elsif (num_mux(31) = '0' and num_rom(31) = '0' and clock = '1') then
sign_mult := '0' ;
elsif(clock = '1') then
sign_mult := '1' ;
end if ;

if (num_mux = 0 and clock = '1') then -- ie, the number is zero.
t := '1' ;
elsif (num_rom = 0 and clock = '1') then
t := '1' ;
elsif (clock = '1') then
t := '0' ;
end if ;

if (t = '0' and clock = '1') then -- separation of mantissa and exponent 
exp_mux := num_mux (30 downto 23) ;
exp_rom := num_rom (30 downto 23) ;
mux_temp := '0' & exp_mux(7 downto 0) ;
rom_temp := '0' & exp_rom(7 downto 0) ;
exp_mult := mux_temp + rom_temp ;
exp_mult := exp_mult - 127 ;

mant_temp := temp1 * temp2 ;

if(mant_temp(45) = '1') then -- normalisation.
exp_mult := exp_mult + 1 ;
res_temp := sign_mult & exp_mult(7 downto 0) & mant_temp(44 downto 22) ;
mult_out <= res_temp(31 downto 0) ;
elsif(mant_temp(45) = '0') then
res_temp := sign_mult & exp_mult(7 downto 0) & mant_temp(43 downto 21) ;
mult_out <= res_temp(31 downto 0) ;
end if ;
elsif (t = '1' and clock = '1') then -- number zero
mult_out <= "00000000000000000000000000000000" ;
t := '0' ;
end if ;
end process ;
end rtl ;



















      
