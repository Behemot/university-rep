-- IO ADDRESS GENERATOR
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity ioadd_gen is
port (
      io_butterfly : in std_logic_vector(3 downto 0) ;
      add_iomode , add_ip , add_op : in std_logic ;
      base_ioadd : out std_logic_vector(3 downto 0) ) ;
end ioadd_gen ;

architecture rtl of ioadd_gen is
begin
process(io_butterfly , add_iomode , add_ip , add_op)
variable out_data : std_logic_vector(3 downto 0) ;
begin
if(add_iomode = '1') then
 if (add_ip = '1') then
  out_data := io_butterfly(3 downto 0) ;
 elsif(add_op = '1') then
  if(io_butterfly(3) = '0') then -- ie, real part
  out_data := '0' & io_butterfly(0) & io_butterfly(1) & io_butterfly(2) ;
  elsif(io_butterfly(3)='1') then -- ie, complex part
  out_data := '1' & io_butterfly(0) & io_butterfly(1) & io_butterfly(2) ;
  end if ;
 end if ;
end if ;
base_ioadd <= out_data(3 downto 0) ;
end process ;
end rtl ;

