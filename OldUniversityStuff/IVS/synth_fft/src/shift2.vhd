-- SHIFT UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use work.butter_lib.all ;
use ieee.std_logic_arith.all ;
use ieee.std_logic_unsigned.all ;

entity shift2 is
port (
       sub_control : in std_logic_vector (8 downto 0) ;	
       c_in  : in std_logic_vector (32 downto 0) ;
       shift_out : out std_logic_vector (31 downto 0) ;
       clock , shift_en , rst_shift : in std_logic ;
       finish_out : out std_logic ) ;
end shift2 ;
architecture rtl of shift2 is
begin
process(clock)
variable sub_temp : std_logic_vector(7 downto 0) ;
variable  temp2 , temp4 : std_logic_vector(31 downto 0) ;
variable temp3 , t : std_logic ;
begin
if(rst_shift='0') then
if(shift_en = '1') then
if(temp3 = '1') then
if(sub_control(8) = '1') then 
sub_temp := sub_control (7 downto 0) ;
temp2 := '1' & c_in (31 downto 1) ; --'1' for implicit one
temp3 := '0' ;
end if ;
end if ;
end if ;
end if ;

if(rst_shift='0') then
if(shift_en = '1') then
if(t = '1') then
if (sub_control(8) = '1') then
if (conv_integer(sub_temp(7 downto 0)) = 0) then
shift_out <= temp2 ;
finish_out <= '1' ;
t := '0' ;
elsif ( clock = '1') then
temp2 := '0' & temp2 (31 downto 1) ;
sub_temp := sub_temp - "00000001" ;
end if ;
end if ;
end if ;
end if ;
elsif(rst_shift='1') then
temp3 := '1' ;
finish_out <= '0' ;
t := '1' ;
end if ;

end process ;
end rtl ;
   


 
