-- THIS FILE COUNTS THE NUMBER OF CYCLES AFTER FFT COMPUTATION IS ENABLED.
-- THIS IS REQUIRED BECAUSE WRITING INTO THE RAM BEGINS ONLY AFTER 5 CYCLES (DURING C1)
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity counter is
port (
      c : out std_logic_vector(2 downto 0) ;
      disable , clock_main , reset : in std_logic) ;
end counter ;

architecture rtl of counter is 
begin
process (reset , clock_main , disable)
variable temp : std_logic_vector(2 downto 0) ;
begin
 if (disable <= '0') then
 if(reset = '1') then
 c <= "000" ;
 temp := "000" ;
 elsif(clock_main = '1' and clock_main'event) then
 c <= (temp + 1) ;
 temp := temp + 1 ;
 end if ;
end if ;
end process ;
end rtl ;      