-- CONTROL UNIT OF THE PROCESSOR
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity cont_gen is
port (
      con_staged , con_iod , con_fftd , con_init : in std_logic ;
      con_ip , con_op , con_iomode , con_fft : out std_logic ;
      con_enbw , con_enbor , c0_enable , con_preset : out std_logic ;
      con_clear , disable : out std_logic ;
      c0 , clock_main : in std_logic ;
      en_rom , en_romgen , reset_counter : out std_logic ; 
      con_clkcount : in std_logic_vector(2 downto 0) ) ;
end cont_gen ;

architecture rtl of cont_gen is
type state is (rst1,rst2,rst3,rst4,rst5,rst6,rst7) ;
signal current_state , next_state : state ;
shared variable counter , temp2 : std_logic_vector(1 downto 0) := "00" ;
begin
process (current_state ,con_staged , con_iod , con_fftd , con_clkcount , c0) 

begin
case current_state is
 when rst1 =>
 con_iomode <= '1' ; -- set mode to io.
 con_ip <= '1' ; -- input mode
 con_clear <= '1' ; -- clear all blocks
 con_enbw <= '1' ; -- enable write to RAM
 con_enbor <= '0' ; -- disable read
 c0_enable <= '0' ; -- disable cycles unit
 disable <= '1' ; -- disable counter
 next_state <= rst2 ;

 when rst2 =>
  con_clear <= '0' ; -- bring clear signal back to zero
  next_state <=rst3 ;

 when rst3 =>
  if(con_iod = '1') then
   con_preset <= '1' ; -- reset cycles 
   reset_counter <= '1' ; -- reset counter
   c0_enable <= '1' ; -- enable cycles
   con_iomode <= '0' ; -- set io mode to '0'
   con_fft <= '1' ; -- fft mode
   en_rom <= '1' ; -- enable ROM
   en_romgen <= '1' ; -- enable ROM address generator
   con_clear <= '1' ; -- clear all blocks
   con_enbw <= '0' ; -- disable write to RAM
   con_enbor <= '1' ; -- enable read from ROM
   disable <= '0' ; -- enable  counter unit. 
   next_state <= rst4 ;
  else
  next_state <= rst3 ;
  end if ;

 when rst4 =>
  con_preset <= '0' ; -- reset for cycles 
  reset_counter <= '0' ; -- reset for counter
  con_clear <= '0' ; -- clear all signals
  if (con_clkcount = 5) then -- check whether 4 or not
   con_enbw <= '1' ; -- enable write to ROM
   disable <= '1' ; -- disable counter 
   reset_counter <= '1' ; -- reset counter
   next_state <= rst5 ;
  else
   next_state <= rst4 ;
  end if ;
 
 when rst5 =>
  
  if (con_fftd = '1') then 
  disable <= '0' ; -- enable counter
  reset_counter <= '0' ; 
  con_clear <= '1' ; -- clear butterfly generator
  con_fft <= '0' ; -- disable fft address generator
  if (con_clkcount = 4) then
    disable <= '1';
    con_enbw <= '0' ;
    con_iomode <= '1' ;
    con_op <= '1' ;
    con_ip <= '0' ;
    next_state <= rst6 ;
    else
   next_state <= rst5 ;
   end if ;
  else
  next_state <= rst5 ; 
 end if ;
 
 when rst6 =>
  con_clear <= '0' ;
  next_state <= rst7 ;

 when rst7 =>
  if(con_iod = '1') then
   con_clear <= '1' ;
   con_preset <= '1' ;
   con_enbor <= '0';
  else
  next_state <= rst7 ;
  end if ;
  
 
 when others =>
  next_state <= rst1 ;

end case ;
end process ;

process(clock_main , con_init)
begin
if(con_init = '1') then
current_state <= rst1 ;
elsif (clock_main'event and clock_main = '0') then
current_state <= next_state ;
end if ;
end process ;
end rtl ;
    





   











 