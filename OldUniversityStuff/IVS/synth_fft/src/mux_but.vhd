-- MULTIPLEXER IN THE BUTTERFLY PROCESSING UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity mux is
 port (
       d0 , d1 : in std_logic_vector(31 downto 0) ;
       mux_out : out std_logic_vector(31 downto 0) ;
       choose : in std_logic ) ;
end mux ;
 
architecture rtl of mux is
begin
process(d0 , d1 , choose)
begin
if (choose = '0') then
mux_out <= d0(31 downto 0) ;
elsif (choose = '1') then
mux_out <= d1(31 downto 0) ;
end if ;
end process ;
end rtl ;