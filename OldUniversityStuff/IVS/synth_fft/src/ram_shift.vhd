-- PARALLE IN PARALLEL OUT SHIFTER IN THE ADDRESS GENERATION UNIT.
-- REQUIRED BECAUSE FFT IS COMPUTED ON DATA AND WRITTEN BACK INTO THE SAME
-- LOCATION AFTER 5 CYCLES. SO THE READ ADDRESS IS SHIFTED THROUGH 5 CYCLES
-- AND GIVEN AS WRITE ADDRESS.
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity ram_shift is
port (
      data_in : in std_logic_vector(3 downto 0) ;
      clock_main : in std_logic ;
      data_out : out std_logic_vector(3 downto 0) ) ;
end ram_shift ;

architecture rtl of ram_shift is
begin
process(clock_main , data_in)
begin
if (clock_main'event and clock_main = '0') then
data_out <= data_in(3 downto 0) ;
end if ;
end process ;
end rtl ;







