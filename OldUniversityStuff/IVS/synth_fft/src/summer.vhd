-- SUMMER
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity summer is
port ( 
       num1 , num2 : in std_logic_vector (31 downto 0) ;
       exp : in std_logic_vector (7 downto 0) ;
       addpulse_in , addsub , rst_sum : in std_logic ;
       add_finish : out std_logic ;
       sumout : out std_logic_vector ( 32 downto 0) ) ;
end summer ;
architecture rtl of summer is
begin
process (num1 , num2 , addpulse_in , rst_sum)
variable temp_num1 , temp_sum , temp_num2 , temp_sum2 , res : std_logic_vector (32 downto 0);
variable temp_exp : std_logic_vector (7 downto 0) ;
begin
if (rst_sum = '0') then
if (addpulse_in = '1') then
temp_num1 := '0' & num1 (31 downto 0) ; --0 to find whether normalisation is required.
temp_num2 := '0' & num2 (31 downto 0) ; --if required MSB will be 1 after addition

if (addsub = '1') then
temp_sum := temp_num1 + temp_num2 ;
sumout <= temp_sum ;
add_finish <= '1' ;

else
temp_sum := temp_num2 - temp_num1 ;
--res := temp_sum + temp_num1 ;
sumout <= temp_sum ;
add_finish <= '1' ;
end if ;
end if ;

elsif (rst_sum = '1') then
add_finish <= '0';
end if ;
end process ;

end rtl ;



