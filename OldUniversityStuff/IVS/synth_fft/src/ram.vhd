-- Behavioral description of dual-port SRAM with :
-- Active High write enable (WE)
-- Active High read enable (RE)
-- Rising clock edge (Clock)
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use work.butter_lib.all ;
entity reg_dpram is
port (
      data_fft , data_io : in std_logic_vector (31 downto 0);
      q : out std_logic_vector (31 downto 0);
      clock , io_mode : in std_logic;
      we , re : in std_logic;
      waddress: in std_logic_vector (3 downto 0);
      raddress: in std_logic_vector (3 downto 0));
end reg_dpram;
architecture behav of reg_dpram is
type MEM is array (0 to 15) of std_logic_vector(31 downto 0);
signal ramTmp : MEM;

begin

-- Write Functional Section
process (clock,waddress,we)
begin
if (clock='0') then
if (we = '1') then
if (io_mode = '0') then
ramTmp (conv_integer (waddress)) <= data_fft ;
elsif (io_mode = '1') then
ramTmp (conv_integer (waddress)) <= data_io ;
end if ;
end if ;
end if ;
end process ;

-- Read Functional Section
process (clock,raddress,re)
begin
if (clock='1') then
if (re = '1') then
q <= ramTmp(conv_integer (raddress)) ;
end if;
end if;
end process;
end behav;
