-- THIS FILE OUTPUTS THE "IO DONE" AND "STAGE DONE" AND "FFT DONE" SIGNALS AT THE 
-- CORRECT TIME. IT ALSO RECEIVES THE OUTPUT OF THE BUTTERFLY GENERATOR
-- AND OUTPUTS IT UNCHANGED.
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity iod_staged is
port (
      but_fly : in std_logic_vector(3 downto 0) ;
      stage_no : in std_logic_vector(1 downto 0) ;
      add_incr , io_mode  : in std_logic ;
      add_iod , add_staged , add_fftd : out std_logic ;
      butterfly_iod : out std_logic_vector(3 downto 0) ) ;
end iod_staged ;

architecture rtl of iod_staged is
begin
process(but_fly,add_incr,io_mode)
begin
if(but_fly = 15 and io_mode = '1' and add_incr='0') then
add_iod <= '1' ; -- io done signal
butterfly_iod <= but_fly ;
elsif(but_fly = 4 and io_mode = '0' and add_incr='1') then
butterfly_iod <= but_fly ;
add_iod <= '0' ;
add_staged <= '1' ; -- stage done signal
else
butterfly_iod <= but_fly ;
add_staged <= '0' ;
end if ;
end process ;

process(stage_no)
begin
if (stage_no=3) then
add_fftd <= '1' ; -- fft done signal
end if ;
end process ;

end rtl;