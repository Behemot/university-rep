-- SUBTRACTOR UNIT
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all ;
use work.butter_lib.all ;
use ieee.std_logic_unsigned.all ;

entity subtractor is
 port ( 
       a : in std_logic_vector (31 downto 0) ;
       b : in std_logic_vector (31 downto 0) ;
       clock , rst_sub , sub_en : in std_logic ;
       a_smaller , fin_sub , num_zero : out std_logic ;
       zero_detect : out std_logic_vector(1 downto 0) ;
       sub : out std_logic_vector (8 downto 0) ;
       change : out std_logic ) ;
end subtractor ;

architecture rtl of subtractor is
begin
process (a , b , clock , rst_sub , sub_en)
variable temp ,c , d : std_logic_vector (7 downto 0) ;
variable e , f : std_logic_vector (22 downto 0) ;
begin
if (rst_sub = '0') then
c := a (30 downto 23) ;
d := b (30 downto 23) ;
e := a (22 downto 0) ;
f := b (22 downto 0) ;

if(sub_en = '1') then
if (clock = '1') then
if ((c=0)) then
zero_detect <= "01" ;
num_zero <= '1' ;

elsif ((d=0)) then
zero_detect <= "10" ;
num_zero <= '1' ;

elsif (c < d ) then
temp := d - c ;
a_smaller <= '1' ;
sub <= '1' & temp (7 downto 0) ;
fin_sub <= '1' ;
zero_detect <= "00" ;
num_zero <= '0' ;

elsif (d < c) then
temp := c - d ; 
a_smaller <= '0' ;
sub <= '1' & temp (7 downto 0) ;
fin_sub <= '1' ;
zero_detect <= "00" ;
num_zero <= '0' ;

elsif((c=d) and e < f) then
a_smaller <= '1' ;
temp:= c-d ;
sub <= '1' & temp (7 downto 0) ;
fin_sub <= '1' ;
zero_detect <= "00" ;
num_zero <= '0' ;

elsif ((c=d) and e > f) then 
a_smaller <= '0' ;
temp := c-d ;
sub <= '1' & temp (7 downto 0) ;
zero_detect <= "00" ;
num_zero <= '0' ;
fin_sub <= '1' ;

elsif ((c=d) and (e = f)) then
temp := c-d ;
a_smaller <= '0' ;
sub <= '1' & "00000000" ;
fin_sub <= '1' ;
zero_detect <= "00" ;
num_zero <= '0' ;


end if ;
end if ;
end if ;

elsif(rst_sub = '1') then
fin_sub <= '0' ;
sub <= "000000000" ;
num_zero <= '0' ;
zero_detect <= "00" ;

end if ;

end process ;

process(a , b) -- process to identify when a new number comes
begin
change <= transport '1' after 1 ns ;
change <= transport '0' after 5 ns ;
end process ;

end rtl ;
