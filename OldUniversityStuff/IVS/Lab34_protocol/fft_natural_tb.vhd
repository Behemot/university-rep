--osardar1@jhu.edu
--fft_tb.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.all;

entity fft_natural_tb is
end fft_natural_tb;

architecture Behavioral of fft_natural_tb is
	--Delay element subtype
	type index_type is array(0 to 50) of std_logic_vector(5 downto 0);
	signal index_tb			: index_type;

	--Clk Divider Input
	signal clk50_tb : STD_LOGIC := '0';
	
	--Tone Generator
	signal freq_tb			: STD_LOGIC_VECTOR(3 downto 0)  := x"0";
	signal sine_fixed		: STD_LOGIC_VECTOR(7 downto 0) := x"00";

	
	--FFT Inputs
	signal clk1_tb			: STD_LOGIC;
	signal start_tb			: STD_LOGIC := '0';
	signal fwd_inv_tb		: STD_LOGIC := '1';
	signal fwd_inv_we_tb	: STD_LOGIC := '1';
	signal xn_re_tb			: STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal xn_im_tb			: STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal unload_tb		: STD_LOGIC := '0';
	
	--FFT Outputs
	signal rfd_tb		: STD_LOGIC;
	signal dv_tb		: STD_LOGIC;
	signal done_tb		: STD_LOGIC;
	signal busy_tb		: STD_LOGIC;
	signal edone_tb		: STD_LOGIC;
	signal xk_im_tb		: STD_LOGIC_VECTOR(14 downto 0) := b"000000000000000";
	signal xk_re_tb		: STD_LOGIC_VECTOR(14 downto 0) := b"000000000000000";
	signal xn_index_tb	: STD_LOGIC_VECTOR(5 downto 0);
	signal xk_index_tb	: STD_LOGIC_VECTOR(5 downto 0);
	
	--Calculations
	--signal xk_re_flip	: STD_LOGIC_VECTOR(7 downto 0) := x"00000000";
	--signal xk_im_flip	: STD_LOGIC_VECTOR(7 downto 0) := x"00000000";
	--signal xk_re_sq		: STD_LOGIC_VECTOR(7 downto 0) := x"00000000";
	--signal xk_im_sq		: STD_LOGIC_VECTOR(7 downto 0) := x"00000000";
	--signal xk_sq_sum	: STD_LOGIC_VECTOR(7 downto 0) := x"00000000";
	signal xk_sqrt		: STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal xk_sqrt_max	: STD_LOGIC_VECTOR(7 downto 0) := x"00";
		
	--Components
	component xfft_v7_1_pipeline port( 
		rfd : out STD_LOGIC; 
		start : in STD_LOGIC := 'X'; 
		fwd_inv : in STD_LOGIC := 'X'; 
		dv : out STD_LOGIC; 
		done : out STD_LOGIC; 
		clk : in STD_LOGIC := 'X'; 
		busy : out STD_LOGIC; 
		fwd_inv_we : in STD_LOGIC := 'X'; 
		edone : out STD_LOGIC; 
		xn_re : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
		xk_im : out STD_LOGIC_VECTOR ( 14 downto 0 ); 
		xn_index : out STD_LOGIC_VECTOR ( 5 downto 0 ); 
		xk_re : out STD_LOGIC_VECTOR ( 14 downto 0 ); 
		xn_im : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
		xk_index : out STD_LOGIC_VECTOR ( 5 downto 0 ));
	end component;
	
	component fp_fixconv port(
		a: IN std_logic_VECTOR(15 downto 0);
		clk: IN std_logic;
		result: OUT std_logic_VECTOR(31 downto 0));
	end component;

	component clk_gen port(
		clk50	: in STD_LOGIC;
		clk1	: out STD_LOGIC);
	end component;
	
	component tone_gen port(
		clk1	: in STD_LOGIC;
		freq	: in STD_LOGIC_VECTOR(3 downto 0);
		sine_out: out STD_LOGIC_VECTOR(7 downto 0));
	end component;
	

begin
	clk50_tb <= not clk50_tb after 10 ns;
	
	xn_re_tb <= sine_fixed;
	xn_im_tb <= (others => '0');
	
	unload_tb <= edone_tb;
	
	process(clk1_tb)
	begin		
		--Reverses the bits (puts them in correct order)
		if(rising_edge(clk1_tb)) then	
			index_tb(0) <= xk_index_tb;
				
			for j in 1 to 50 loop
				index_tb(j) <= index_tb(j-1);
			end loop;
		end if;
		
		--Records largest amplitude (so it's easy to see on waveform)
		if(xk_index_tb = x"000") then
			xk_sqrt_max <= (others => '0');
		end if;		
		
		if(index_tb(47) < x"400") then --1024
			if(xk_sqrt > xk_sqrt_max) then
				xk_sqrt_max <= xk_sqrt;
			end if;
		end if;		
	end process;
	
	--Begin TB
	process
	begin
		fwd_inv_tb <= '0';
		start_tb <= '0'; 
		fwd_inv_we_tb <= '0';
		freq_tb <= x"5";	--2840 Hz
		wait for 10.5 us;
		
		fwd_inv_tb <= '1'; 
		fwd_inv_we_tb <= '1';
		wait for 1 us;
		
		fwd_inv_we_tb <= '0';	
		start_tb <= '1';
		wait for 2070 us;
		
		freq_tb <= x"4";	--12500 Hz
		wait for 12000 us;
		
		freq_tb <= x"1";	--31250 Hz
		wait for 7500 us;
		
		freq_tb <= x"9";	--??? Hz
		wait;
				
	end process;	
	
	--Instantiations
	clk_gen0 : clk_gen port map(
		clk50 => clk50_tb,
		clk1 => clk1_tb);
	
	tone_gen0 : tone_gen port map(
		clk1 => clk1_tb,
		freq => freq_tb,
		sine_out => sine_fixed);	
		

	
	fft0 : xfft_v7_1_pipeline port map(
	    rfd => rfd_tb, 
	    start => start_tb, 
	    fwd_inv => fwd_inv_tb, 
	    dv => dv_tb, 
	    done => done_tb, 
	    clk => clk1_tb, 
	    busy => busy_tb, 
	    fwd_inv_we => fwd_inv_we_tb, 
	    edone => edone_tb, 
	    xn_re => xn_re_tb, 
	    xk_im => xk_im_tb, 
	    xn_index => xn_index_tb, 
	    xk_re => xk_re_tb, 
	    xn_im => xn_im_tb, 
	    xk_index => xk_index_tb);
	    
end Behavioral;
