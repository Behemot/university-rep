-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
 component xfft_v7_1
	port (
	clk: in std_logic;
	start: in std_logic;
	unload: in std_logic;
	xn_re: in std_logic_vector(7 downto 0);
	xn_im: in std_logic_vector(7 downto 0);
	fwd_inv: in std_logic;
	fwd_inv_we: in std_logic;
	rfd: out std_logic;
	xn_index: out std_logic_vector(5 downto 0);
	busy: out std_logic;
	edone: out std_logic;
	done: out std_logic;
	dv: out std_logic;
	xk_index: out std_logic_vector(5 downto 0);
	xk_re: out std_logic_vector(14 downto 0);
	xk_im: out std_logic_vector(14 downto 0));
end component;

-- COMP_TAG_END ------ End COMPONENT Declaration ------------

-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.

------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
your_instance_name : xfft_v7_1
		port map (
			clk => clk,
			start => start,
			unload => unload,
			xn_re => xn_re,
			xn_im => xn_im,
			fwd_inv => fwd_inv,
			fwd_inv_we => fwd_inv_we,
			rfd => rfd,
			xn_index => xn_index,
			busy => busy,
			edone => edone,
			done => done,
			dv => dv,
			xk_index => xk_index,
			xk_re => xk_re,
			xk_im => xk_im);

  BEGIN

  -- Component Instantiation
your_instance_name : xfft_v7_1
		port map (
			clk => clk,
			start => start,
			unload => unload,
			xn_re => xn_re,
			xn_im => xn_im,
			fwd_inv => fwd_inv,
			fwd_inv_we => fwd_inv_we,
			rfd => rfd,
			xn_index => xn_index,
			busy => busy,
			edone => edone,
			done => done,
			dv => dv,
			xk_index => xk_index,
			xk_re => xk_re,
			xk_im => xk_im);


  --  Test Bench Statements
     tb : PROCESS
     BEGIN

        wait for 100 ns; -- wait until global set/reset completes

        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
