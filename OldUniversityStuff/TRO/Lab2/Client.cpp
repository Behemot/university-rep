#define _WIN32_WINNT 0x501
            
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#define DEFAULT_PORT "9000"
char* server_address = "127.0.0.1";

int* A = NULL;
int* B = NULL;
int* C = NULL;
int* T = NULL;
int* MO = NULL;
int* MX = NULL;
int* MT = NULL;
int* MZ = NULL;

int N;
int H;
int p = 4;
int H2;

HANDLE* mid_calc_semaphores;
HANDLE* job_completed_semaphores;
HANDLE* transfer_done;

void prepareStorage()
{
    if(A)
        free(A);
    if(B)
        free(B);
    if(C)
        free(C);
    if(T)
        free(T);
    if(MO)
        free(MO);
    if(MX)
        free(MX);
    if(MT)
        free(MT);
    if(MZ)
        free(MZ);
    A = new int[H];
    B = new int[H];
    C = new int[N];
    T = new int[H];
    MO = new int[N*H];
    MX = new int[N*H];
    MT = new int[N*N];
    MZ = new int[N*H];
   
}

DWORD WINAPI workerThread(LPVOID par)
{
    int params = *((int*)par);
    printf("Worker thread %d - %d started\n", params, params+H2 );

    for(int i = params; i < params + H2; i++)
    {
        T[i] = 0;
        for(int j = 0; j < N; j++)
        {
            T[i] += C[j]*MO[i * N + j];
        }
    }
    
    for(int i = params; i < params + H2; i++)
    {
        for(int j = 0; j < N; j++)
        {
            MZ[i * N + j] = 0;
            for(int k = 0; k < N; k++)
            {
                MZ[i * N + j] += MX[i * N + k]*MT[k * N + j];
            }
        }
        
    }

    ReleaseSemaphore(mid_calc_semaphores[params/ H2], 1, NULL);
    printf("Worker thread %d - %d waiting for mid-calculation input\n", params, params+H2);
    WaitForSingleObject(transfer_done[params/H2], INFINITE);

    for(int i = params; i < params + H2; i++)
    {
        A[i] = B[i];
        for(int j = 0; j < N; j++)
        {
            A[i] += T[j]*MZ[i * N + j];
        }
    }
    
    ReleaseSemaphore(job_completed_semaphores[params / H2], 1, NULL);
    printf("Worker thread %d - %d closing\n", params, params+H2);
}


int main(int argc, char* argv[])
{
    WSADATA wsaData;
    int iResult;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }
    
    struct addrinfo *result = NULL,
                *ptr = NULL,
                hints;

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(server_address, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    SOCKET ConnectSocket = INVALID_SOCKET;

    // Attempt to connect to the first address returned by
    // the call to getaddrinfo
    ptr=result;

    // Create a SOCKET for connecting to server
    ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    
    if (ConnectSocket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // Connect to server.
    iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        closesocket(ConnectSocket);
        ConnectSocket = INVALID_SOCKET;
    }

    // Should really try the next address returned by getaddrinfo
    // if the connect call failed
    // But for this simple example we just free the resources
    // returned by getaddrinfo and print an error message

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }
    
    int iSendResult,iRecvResult;

    printf("Client receiving data\n");
    iRecvResult = recv(ConnectSocket, (char*)&N , sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed1: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    iRecvResult = recv(ConnectSocket, (char*)&H, sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed2: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    
    H2 = H / p;
    prepareStorage();

    iRecvResult = recv(ConnectSocket, (char*)(B), H * sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed3: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    iRecvResult = recv(ConnectSocket, (char*)C, N * sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed4: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    iRecvResult = recv(ConnectSocket, (char*)MO, H * N * sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed5: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    iRecvResult = recv(ConnectSocket, (char*)MX, H * N * sizeof(int), 0);
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed6: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    iRecvResult = recv(ConnectSocket, (char*)MT, N * N * sizeof(int), 0);
    
    if (iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed7: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    printf("Data received. Starting worker threads\n");
    mid_calc_semaphores = new HANDLE[p];
    job_completed_semaphores = new HANDLE[p];
    transfer_done = new HANDLE[p];
    for(int i = 0; i < p; i++)
    {
        mid_calc_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
        job_completed_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
        transfer_done[i] = CreateSemaphore(NULL, 0, 1, NULL);
    }
   
    HANDLE* workerThreads = new HANDLE[p];
    int* params = new int[p];
    for (int i = 0; i < p; i++ )
    {
        params[i] = i * H2;
        workerThreads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) &workerThread,&(params[i]), 0, NULL);
        printf("%d - %d - %d \n",i, H2, params[i]);

    }

    WaitForMultipleObjects(p, mid_calc_semaphores, true, INFINITE);

    printf("Sending mid-calculation data to server\n");
    
    iSendResult = send(ConnectSocket, (const char*)T, H * sizeof(int), 0);
    iSendResult = send(ConnectSocket, (const char*)MZ, N * H * sizeof(int),0);

    if(iSendResult == SOCKET_ERROR) 
    {
        printf("recv failed: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }
    
    delete(T);
    T = new int[N];

    printf("Receiving rearranged mid-calculation data from server\n");
    iRecvResult = recv(ConnectSocket, (char*)T, N * sizeof(int), 0);
    iRecvResult = recv(ConnectSocket, (char*)MZ, N * H * sizeof(int), 0);
    
    for(int i = 0; i < p; i++)
    {
        ReleaseSemaphore(transfer_done[i],1,NULL);
    }

    printf("Waiting for end results\n");
    WaitForMultipleObjects(p, job_completed_semaphores, true, INFINITE);

    printf("Sending end results\n");
    iSendResult = send(ConnectSocket, (const char*)A, H * sizeof(int), 0);
  
    closesocket(ConnectSocket);
}
