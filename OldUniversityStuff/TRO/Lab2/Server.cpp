#define _WIN32_WINNT 0x501
            
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#define DEFAULT_PORT "9000"

int* A = NULL;
int* B = NULL;
int* C = NULL;
int* T = NULL;
int* MO = NULL;
int* MX = NULL;
int* MT = NULL;
int* MZ = NULL;

int N = 8;
int a = 1;
int p = 1;
int H = N / p;

HANDLE* mid_calc_semaphores;
HANDLE* job_completed_semaphores;

struct thread_parameters
{
    int start;
    int end;
    SOCKET io_socket;
};

void generateData()
{
    if(A)
        free(A);
    if(B)
        free(B);
    if(C)
        free(C);
    if(T)
        free(T);
    if(MO)
        free(MO);
    if(MX)
        free(MX);
    if(MT)
        free(MT);
    if(MZ)
        free(MZ);
    A = new int[N];
    B = new int[N];
    C = new int[N];
    T = new int[N];
    MO = new int[N*N];
    MX = new int[N*N];
    MT = new int[N*N];
    MZ = new int[N*N];
    for(int i = 0; i < N; i++)
    {
        B[i] = a;
        C[i] = a;
        for(int j = 0; j < N; j++)
        {
            MO[i * N + j] = a;
            MX[i * N + j] = a;
            MT[i * N + j] = a;
        }
    }

}

void transpone(int* mat)
{
    int temp;
    for(int i = 0; i < N; i++)
    {
        for(int j = i; j < N; j++)
        {
            temp = mat[i * N + j];
            mat[i * N + j] = mat[j * N + i];
            mat[j * N + i] = temp;
        }
    }
}

DWORD WINAPI IOThread(LPVOID par)
{
    thread_parameters params = *((thread_parameters*)par);
    
    int iSendResult,iRecvResult;
    
    printf("Thread %d - %d sending data \n", params.start, params.end);

    iSendResult = send(params.io_socket, (const char*)(&N) , sizeof(int), 0);
   
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }

    iSendResult = send(params.io_socket, (const char*)(&H), sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed1: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }

    iSendResult = send(params.io_socket, (const char*)(B + params.start), H*sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed2: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }

    iSendResult = send(params.io_socket, (const char*)(C), N * sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed3: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }
   
    int* temp = new int[N*N];
    memcpy(temp,MO,N*N*sizeof(int));
    transpone(temp);
    iSendResult = send(params.io_socket, (const char*)(temp + params.start * N), H * N * sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed4: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }


    iSendResult = send(params.io_socket, (const char*)(MX + params.start * N), H * N * sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed5: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }


    iSendResult = send(params.io_socket, (const char*)(MT), N * N * sizeof(int), 0);
    
    if (iSendResult == SOCKET_ERROR) 
    {
        printf("send failed6: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }

    iRecvResult = recv(params.io_socket, (char*)(T + params.start), H * sizeof(int), 0);
    iRecvResult = recv(params.io_socket, (char*)(MZ + params.start * N), N * H * sizeof(int),0);

    if(iRecvResult == SOCKET_ERROR) 
    {
        printf("recv failed7: %d\n", WSAGetLastError());
        closesocket(params.io_socket);
        WSACleanup();
    }
    
    printf("Thread %d - %d waiting for mid-results \n", params.start, params.end);

    ReleaseSemaphore(mid_calc_semaphores[params.start/ H], 1, NULL);
    WaitForMultipleObjects(p, mid_calc_semaphores, true, INFINITE);
    
    printf("Thread %d - %d sending rearranged data \n", params.start, params.end);

    iSendResult = send(params.io_socket, (const char*)(T), N * sizeof(int), 0);
    memcpy(temp, MZ, N * N * sizeof(int));
    transpone(temp);
    iSendResult = send(params.io_socket, (const char*)(temp + params.start * N), N * H * sizeof(int), 0);

    printf("Thread %d - %d receiving end results \n", params.start, params.end);

    iRecvResult = recv(params.io_socket, (char*)(A + params.start), H * sizeof(int), 0);
    ReleaseSemaphore(job_completed_semaphores[params.start / H], 1, NULL);
    
    printf("Thread %d - %d complete \n", params.start, params.end);

    closesocket(params.io_socket);
}

int main(int argc, char* argv[])
{
    
    
    generateData(); 
    WSADATA wsaData;
    int iResult;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    struct addrinfo *result = NULL, *ptr = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the local address and port to be used by the server
    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    SOCKET ServerSocket = INVALID_SOCKET;
    // Create a SOCKET for the server to listen for client connections
    ServerSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if (ServerSocket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // Setup the TCP listening socket
    iResult = bind( ServerSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        printf("bind failed with error: %d\n", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(ServerSocket);
        WSACleanup();
        return 1;
    }

    
    if ( listen( ServerSocket, SOMAXCONN ) == SOCKET_ERROR ) {
        printf( "Listen failed with error: %ld\n", WSAGetLastError() );
        closesocket(ServerSocket);
        WSACleanup();
        return 1;
    }



    mid_calc_semaphores = new HANDLE[p];
    job_completed_semaphores = new HANDLE[p];
    for(int i = 0; i < p; i++)
    {
        mid_calc_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
        job_completed_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
    }
    
    HANDLE* IOThreads = new HANDLE[p];
    for(int i = 0; i < p; i++)
    {
        SOCKET IOSocket = INVALID_SOCKET;

        IOSocket = accept(ServerSocket, NULL, NULL);
        if (IOSocket == INVALID_SOCKET) {
            printf("accept failed: %d\n", WSAGetLastError());
            closesocket(ServerSocket);
            WSACleanup();
            return 1;
        }
        thread_parameters params = {i * H, (i + 1) * H, IOSocket};
        IOThreads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) &IOThread,(LPVOID) &params, 0, NULL);
    }
    
    WaitForMultipleObjects(p, job_completed_semaphores, true, INFINITE);

    if (N < 11)
    {
        for(int i = 0; i < N; i++)
        {
            printf("%d ", A[i]);
        }
        printf("\n");
    }

}
