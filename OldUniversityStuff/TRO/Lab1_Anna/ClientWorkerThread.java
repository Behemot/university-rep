package client;

import core.Matrix;

public class ClientWorkerThread extends Thread{
	int start,end;
	
	public ClientWorkerThread(int Start, int End){
		start = Start;
		end = End;
	}
	
	public void run(){
		ClientDataStorage data_storage = ClientDataStorage.getObject();
		Matrix MA_part = data_storage.getPartMA(start, end);
		Matrix MB_part = data_storage.getPartMB(start, end);
		Matrix MC = data_storage.getMC();
		Matrix MT = data_storage.getMT();
		Matrix MX_part = new Matrix(end - start, MA_part.getWidht(), 0);		
		Matrix temp = new Matrix(end - start, MA_part.getWidht(), 0);
		for(int i = 0 ; i < end-start; i++){
			for(int j = 0; j < MA_part.getWidht(); j++){
				int buf = 0;
				for(int k = 0; k < MA_part.getWidht(); k++){
					buf += MB_part.get(k, i) * MC.get(j,k);
				}
				temp.set(j, i, buf);
			}
		}
		
		for(int i = 0 ; i < end-start; i++){
			for(int j = 0; j < MA_part.getWidht(); j++){
				int buf = MA_part.get(j,i);
				for(int k = 0; k < MA_part.getWidht(); k++){
					buf += temp.get(k, i) * MT.get(j,k);
				}
				MX_part.set(j, i, buf);
			}
		}
		
		data_storage.mergeResult(MX_part, start, end);
		ClientThreadSynchronizer.task_done();
		
	}
	
}
