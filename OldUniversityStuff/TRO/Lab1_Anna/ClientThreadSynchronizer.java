package client;

public class ClientThreadSynchronizer {
	private static Object job_done_object = new Object();
	private static int num_threads = 3;
	private static int threads_done = 0;
	

	
	public static void setNumThreads(int n){
		num_threads = n;
	}
	
	public static void block_until_done(){
		synchronized(job_done_object){
			try {
				job_done_object.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void task_done(){
		synchronized(job_done_object){
			threads_done++;
			if(threads_done == num_threads){
				job_done_object.notify();
				threads_done = 0;
			}
		}
	}
}
