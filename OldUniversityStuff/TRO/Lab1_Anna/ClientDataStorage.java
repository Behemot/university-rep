package client;

import core.Matrix;

public class ClientDataStorage {

	private static int N = 16;
	private static int H = 4;
	private Matrix MX,MA,MB,MC,MT; 

	private static ClientDataStorage object = null;

	
	
	static ClientDataStorage getObject(){
		if (object == null)
		{
			object = new ClientDataStorage();
		}
		return object;
	
	}

	void setParams(int n, int h){
		N = n;
		H = h;
	}
	
	int getN(){
		return N;
	}
	
	int getH(){
		return H;
	}
	
	void setMA(Matrix MA_new){
		MA = MA_new;
	}
	
	Matrix getPartMA(int start, int end){
		return MA.getPart(start, end);
	}

	void setMB(Matrix MB_new){
		MB = MB_new;
	}

	Matrix getPartMB(int start, int end){
		return MB.getPart(start, end);
	}
	
	void setMC(Matrix MC_new){
			MC = MC_new;
	}

	
	Matrix getMC(){
		synchronized(MC){
			return MC.copy();
		}
	}
	
	void setMT(Matrix MT_new){
		MT = MT_new;
	}

	
	Matrix getMT(){
		synchronized(MT){
			return MT.copy();
		}
	}

	void setMX(Matrix MX_new){
		MX = MX_new;
	}
	
	Matrix getMX(){
		return MX;
	}

	void mergeResult(Matrix MX_part, int start, int end){
		for(int i = start; i < end; i++){
			for(int j = 0; j < MX.getWidht(); j++){
				MX.set(j, i, MX_part.get(j, i - start));
			}
		}
	}
}
