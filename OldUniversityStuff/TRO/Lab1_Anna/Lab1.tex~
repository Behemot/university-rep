\documentclass[a4paper,12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[english,russian,ukrainian]{babel}
\usepackage[T2A]{fontenc}
\usepackage{indentfirst}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{titlesec}
\usepackage{titletoc}
\usepackage{listings}
\usepackage{lscape}
\usepackage{float}
\usepackage{listings}

\textwidth=6.9in
\textheight=10in
\oddsidemargin=0.7in
\voffset=-1in
\hoffset=-1in

\pagestyle{plain}

\newcommand{\titletop}{%
Національний технічний університет України ``Київський політехнічний інститут''\par
Факультет інформатики та обчислювальної техніки\par
Кафедра обчислювальної техніки}
\newcommand{\titlelineI}{Лабораторна робота №1\par
з курсу ``Теорія розподілених обчилень''}
\newcommand{\titlelineIII}{Виконала:\par
Вдовенко А.С.\par
ФІОТ гр. ІО-71\par
\vspace{5mm}
}
\newcommand{\titlebottom}{Київ 2011}

\renewcommand{\maketitle}
{
  \thispagestyle{empty}
  \parbox[c][0.3\vsize][t]{\hsize}
  {\begin{center}\fontsize{13pt}{15pt}\selectfont\titletop\end{center}}

  \parbox[c][0.1\vsize][t]{\hsize}
  {\begin{center}\fontfamily{fma}\fontsize{18pt}{20pt}\selectfont\titlelineI\end{center}}


  \parbox[t][0.3\vsize][t]{\hsize}
  {
    \parbox[t]{0.4\hsize}{\raggedright}\hfill
    \parbox[t]{0.4\hsize}{\raggedright\fontsize{14pt}{15pt}\selectfont\titlelineIII}
  }

  \vfill
  \begin{center}\fontsize{13pt}{14pt}\selectfont\titlebottom\end{center}
}

%opening
\author{Anthony Mykhailenko}


\begin{document}
\maketitle
\pagebreak
\section*{Лабораторная работа №1. Java. Сокеты.}
\section{Техническое задание на лабораторную работу}
Разработать программу на языке Java для распределенной системы (Рис. \ref{fig1}) для параллельного выполнения заданной функции:
\[
    MX = MA + MB \cdot MC \cdot MT;
\]
Разработать параллельный алгоритм, алгоритмы клиента и сервера, схему взаимодействия узлов.
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Net.png}
\caption{Модель распределенной системы}
\label{fig1}
\end{figure}

\section{Выполнение лабораторной работы}
\subsection{Разработка параллельного математического алгоритма}
Математический алгоритм:
\[
    MZ_H = MB_H \cdot MC
\]
\[
    MX_H = MA_H + MZ_H \cdot MT
\]
\textbf{Общие ресурсы:} MC, MT
где $H = \dfrac{N}{p \cdot C}$, $N$ - размерность задачи, $p$ - количество машин в распределенной системе, $C$ - количество процессоров на каждой машине.

\pagebreak

\subsection{Разработка алгоритмов сервера и клиента}
\begin{description}
    \item[Сервер]
    \begin{enumerate}
        \item Ввод $MA, MB, MC, MT$.
        \item Отправить клиенту $K_i$ $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Принять от клиента $K_i$ $MX_{C \cdot H}$.
        \item Вывод $MX$
    \end{enumerate}
    \textbf{Счет на серверной машине производиться путём запуска дополнительного клиентского процесса на ней}
    \item[Клиент]
    \begin{enumerate}
        \item Получить от сервера $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Счет $MX_{C \cdot H}$.
        \item Отправить серверу $MX_{C \cdot H}$.
    \end{enumerate}

\end{description}

\subsection{Разработка схемы взаимодействия клиента и сервера}

\begin{figure}[H]
\includegraphics[width=0.8\linewidth]{Interaction.png}
\caption{Схема взаимодействия клиента и сервера}
\label{fig2}
\end{figure}

\subsection{Разработка структуры сервера и клиента}
\begin{figure}[H]
\includegraphics[width=0.8\linewidth]{Interaction2.png}
\caption{Структура сервера и клиентов}
\label{fig3}
\end{figure}
\subsection{Разработка алгоритмов потоков}
\subsubsection*{Потоки в сервере}
\begin{description}
    \item[Main]
    \begin{enumerate}
        \item Ввод $MA, MB, MC, MT$.
        \item Создать серверный сокет.
        \item Для каждого из $p$ принятых соединений создать сокет, поток ввода-вывода $IO_{K_i}$ и передать ему созданый сокет.
        \item Ждать окончания потоков $IO_{K_i}$.
        \item Вывод $MX$.
    \end{enumerate}
    \item[IOki]
    \begin{enumerate}
        \item Передать клиенту $K_i$ $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Принять от клиента $K_i$ $MX_{C \cdot H}$
    \end{enumerate}
\end{description}

\subsubsection*{Потоки в клиенте}
\begin{description}
    \item[Main]
    \begin{enumerate}
        \item Создать сокет для связи с сервером.
        \item Принять от сервера $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Cоздать для каждого процессора рабочий поток $T_i$.
        \item Ждать окончания вычислений $MX_{C \cdot H}$.
        \item Передать серверу $MX_{C \cdot H}$.
    \end{enumerate}
    \item[Ti]
    \begin{enumerate}
        \item Счет $MX_H$.
    \end{enumerate}

\end{description}

Для контроля за данными используються два вида мониторов - ServerDataStorage и ClientDataStorage.

\begin{description}
\item[ServerDataStorage] - монитор, отвечающий за доступ к данным на сервере.
Методы, относящиеся к защите общих ресурсов:
\begin{itemize}
    \item Matrix getMC() - метод, позволяющий получить копию матрицы $MC$;
    \item Matrix getMT() - метод, позволяющий получить копию матрицы $MT$.
\end{itemize}

\item[ClientDataStorage] - монитор, отвечающий за доступ к данным на клиенте.
Методы, относящиеся к защите общих ресурсов:
\begin{itemize}
    \item void setMC(Matrix MC\_new) - метод, позволяющий инициализировать матрицу $MC$ под защитой монитора значениями из матрицы $MC\_new$; 
    \item Matrix getMC() - метод, позволяющий получить копию матрицы $MC$;
    \item void setMT(Matrix MT\_new) - метод, позволяющий инициализировать матрицу $MT$ под защитой монитора значениями из матрицы $MT\_new$; 
    \item Matrix getMT() - метод, позволяющий получить копию матрицы $MT$.
\end{itemize}
\end{description}


\subsection{Листинг программы}
\lstset{language=Java, basicstyle=\small, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=MainServer.java]{MainServer.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=ServerTransportThread.java]{ServerTransportThread.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=ServerDataStorage.java]{ServerDataStorage.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=ClientDataStorage.java]{ClientDataStorage.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=ClientWorkerThread.java]{ClientWorkerThread.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=Matrix.java]{Matrix.java}

\end{document}
