package core;

import java.io.Serializable;

public class Matrix implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int height;
	int width;
	int M[][];
	public Matrix(int height, int width, int a){
		this.height = height;
		this.width = width;
		M = new int[height][width];
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				M[i][j] = a;
			}
		}
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getWidht(){
		return width;
	}
	
	public void set(int x, int y, int a){
	 	M[y][x] = a;
	}
	
	public int get(int x, int y){
		return M[y][x];
	}
	
	public Matrix getPart(int start, int end){
		Matrix T = new Matrix(end - start, width, 0);
		for(int i = start; i < end; i++){
			for(int j = 0; j < width; j++){
				T.set(j, i - start, M[i][j]);
			}
		}
		return T;
	}
	
	public Matrix copy(){
		Matrix T = new Matrix(height, width, 0);
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				T.set(j, i, M[i][j]);
			}
		}
		return T;
	}
}
