package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import core.Matrix;

public class MainClient {
	static String ip = "127.0.0.1";
	static int port = 9000;
	static int num_worker_threads = 4;
	
	public static void main(String args[]){
		try {
			System.out.println("Client started");
			Socket connection = new Socket(ip, port);
			System.out.println("Client connection established");
			ClientDataStorage data_storage = ClientDataStorage.getObject();
			System.out.println("1");
			ObjectInputStream inp = new ObjectInputStream(connection.getInputStream());
			System.out.println("2");
			System.out.println("Client IO streams created. Receiving data");
			//while (inp.available() == 0){System.out.println(inp.available());};

			Matrix MA_part = (Matrix) inp.readObject();
			data_storage.setMA(MA_part);
			data_storage.setMB((Matrix) inp.readObject());
			data_storage.setMC((Matrix) inp.readObject());
			data_storage.setMT((Matrix) inp.readObject());
			
			Matrix MX_new = new Matrix(MA_part.getHeight(),MA_part.getWidht(),0);
			data_storage.setMX(MX_new);
			//inp.close();

			System.out.println("Client data received");
			
			
			data_storage.setParams(MA_part.getWidht(), MA_part.getHeight());
			
			ClientThreadSynchronizer.setNumThreads(num_worker_threads);
			
			int h2 = data_storage.getH()/num_worker_threads;
			System.out.println("Client starting worker threads");
			for(int i = 0; i < num_worker_threads; i++){
				System.out.println(h2*i+" - " +(i+1)*h2);
				ClientWorkerThread t = new ClientWorkerThread(i * h2, (i + 1) * h2);
				t.start();
			}
			System.out.println("Client waiting for calculation results");
			ClientThreadSynchronizer.block_until_done();
			System.out.println("Client sending results");

			ObjectOutputStream outp = new ObjectOutputStream(connection.getOutputStream());

			outp.writeObject(data_storage.getMX());

			outp.flush();
			
			//outp.close();
			connection.close();
			System.out.println("Client shutting down");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
