package server;

import core.Matrix;
import java.net.Socket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ServerTransportThread extends Thread{
	int Start, End;
	Socket Connection;
	public ServerTransportThread(int start, int end, Socket connection){
		Start = start;
		End = end;
		Connection = connection;
	}
	
	public void run(){
		try {
			System.out.println("Server transport thread "+Start+"-"+End+" started");
			ServerDataStorage data_storage = ServerDataStorage.getObject();
			System.out.println("1");
			System.out.println("2");
			
			ObjectOutputStream outp = new ObjectOutputStream(Connection.getOutputStream());
			
			System.out.println("Server transport thread "+Start+"-"+End+" created IO streams. Sending data");

			outp.writeObject(data_storage.getPartMA(Start, End));
			outp.writeObject(data_storage.getPartMB(Start, End));
			outp.writeObject(data_storage.getMC());
			outp.writeObject(data_storage.getMT());
			
			outp.flush();
			//outp.close();
			
			System.out.println("Server transport thread "+Start+"-"+End+" sent data");


			ObjectInputStream inp = new ObjectInputStream(Connection.getInputStream());
			
			
			
			System.out.println("Server transport thread "+Start+"-"+End+" waiting for results");

			//while(inp.available() == 0){};
			
			try {
				data_storage.mergeResult((Matrix)inp.readObject(), Start, End);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("Server transport thread "+Start+"-"+End+" received results");

			
			//inp.close();
			Connection.close();
			
			ServerThreadSynchronizer.task_done();
			
			System.out.println("Server transport thread "+Start+"-"+End+" shutting down");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
