package server;

import core.Matrix;

public class ServerDataStorage {

	private static int N = 2400;
	private static int a = 1;
	private Matrix MX,MA,MB,MC,MT; 

	private static ServerDataStorage object = null;

	static ServerDataStorage getObject(){
		if (object == null)
		{
			object = new ServerDataStorage();
		}
		return object;
	}

	private ServerDataStorage(){

		MX = new Matrix(N,N,a);
		MA = new Matrix(N,N,a);
		MB = new Matrix(N,N,a);
		MC = new Matrix(N,N,a);
		MT = new Matrix(N,N,a);
	}
	
	int getN(){
		return N;
	}
	
	int getA(){
		return a;
	}
		
	Matrix getPartMA(int start, int end){
		synchronized(MA){
			return MA.getPart(start, end);
		}
	}
	
	Matrix getPartMB(int start, int end){
		synchronized(MB){
			return MB.getPart(start, end);
		}
	}
	
	Matrix getMC(){
		synchronized(MC){
			return MC.copy();
		}
	}
	
	Matrix getMT(){
		synchronized(MT){
			return MT.copy();
		}
	}

	Matrix getMX(){
		return MX;
	}

	
	void mergeResult(Matrix MX_part, int start, int end){
		for(int i = start; i < end; i++){
			for(int j = 0; j < MX.getWidht(); j++){
				MX.set(j, i, MX_part.get(j, i - start));
			}
		}
	}

}
