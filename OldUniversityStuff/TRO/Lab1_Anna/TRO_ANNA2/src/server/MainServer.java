package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import core.Matrix;

public class MainServer {

	static int numOfClients = 1;
	static int port = 9000;
	
	public static void main(String args[]){
		try {
			System.out.println("Server started");
			ServerDataStorage data_storage = ServerDataStorage.getObject();
			ServerSocket s = new ServerSocket(port);
			ServerThreadSynchronizer.setNumThreads(numOfClients);
			int connections = 0;
			int h = data_storage.getN() / numOfClients;			
			System.out.println("Initialized server socket. Listening for clients");
			while (connections < numOfClients){
				Socket client = s.accept();
				System.out.println("Client "+ connections + " found. Establishing connection.");
				ServerTransportThread t = new ServerTransportThread(connections * h, (connections + 1) * h, client);
				t.start();
				connections++;
				System.out.println(connections);
			}
			System.out.println("Server waiting for results");
			ServerThreadSynchronizer.block_until_done();
			System.out.println("Results:");
			if(data_storage.getN() < 11){
			Matrix MX = data_storage.getMX();
			for(int i = 0; i < MX.getHeight(); i++){
				for(int j = 0; j < MX.getWidht(); j++){
					System.out.print(MX.get(j, i)+" ");
				}
				System.out.println();
			}
			}
			System.out.println("Server shutting down");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
