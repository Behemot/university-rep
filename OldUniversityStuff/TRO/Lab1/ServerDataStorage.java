package server;
public class ServerDataStorage {

private static int N = 64;
private static int a = 1;
private int[] A,B,C, TEMP1;
private int[][] MO,MX,MT,MTEMP2; 

private static ServerDataStorage object = null;

static ServerDataStorage getObject(){
	if (object == null)
	{
		object = new ServerDataStorage();
	}
	return object;
}

private ServerDataStorage(){
	A = new int[N];
	B = new int[N];
	C = new int[N];
	TEMP1 = new int[N];
	MO = new int[N][N];
	MX = new int[N][N];
	MT = new int[N][N];
	MTEMP2 = new int[N][N];
	
	for(int i = 0; i < N; i++){
		B[i] = a;
		C[i] = a;
		for(int j = 0; j < N; j++){
			MO[i][j] = a;
			MX[i][j] = a;
			MT[i][j] = a;
		}
	}
}

void setN(int n){
	N = n;
	object = new ServerDataStorage();
}

int getN(){
	return N;
}

void setA(int index, int value){
	A[index] = value;
}

void setTEMP1(int index, int value){
	synchronized(TEMP1){
		TEMP1[index] = value;
	}
}

void setMTEMP2(int x, int y, int value){
		MTEMP2[y][x] = value;
}

int getA(int index){
	return A[index];
}

int getB(int index){
	return B[index];
}

int getC(int index){
	synchronized (C){
		return C[index];
	}
}

int getTEMP1(int index){
	synchronized (TEMP1){
		return TEMP1[index];
	}
}

int getMO(int x, int y){
	return MO[y][x];
}

int getMX(int x, int y){
	return MX[y][x];
}

int getMT(int x, int y){
	synchronized(MT){
		return MT[y][x];
	}
}

int getMTEMP2(int x, int y){
	return MTEMP2[y][x];
}
}
