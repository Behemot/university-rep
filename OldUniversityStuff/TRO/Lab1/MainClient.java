package client;



public class MainClient {
	

	public static void main(String[] args) {
		System.out.println("Client started");
		ClientTransportThread transport_thread = new ClientTransportThread();
		transport_thread.start();
		System.out.println("Client shutting down");
	}

}
