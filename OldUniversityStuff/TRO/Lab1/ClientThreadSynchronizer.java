package client;

public class ClientThreadSynchronizer {
	public static Object sync_object = new Object();
	private static Object block_object = new Object();
	private static Object job_done_object = new Object();
	private static Object job_done_object1 = new Object();
	
	private static Object transfer_block_object = new Object();
	private static int num_threads = 3;
	private static int current_threads = 0;
	private static int threads_done = 0;
	private static int threads_done1 = 0;
	
	
	public static void barrier(){
		synchronized(block_object){
			
			if (current_threads == num_threads - 1){
				block_object.notifyAll();
				current_threads = 0;
			}
			else{
				current_threads++;
				try {
					block_object.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static void setNumThreads(int n){
		num_threads = n;
	}
	
	public static void block_until_done(){
		synchronized(job_done_object){
			if (threads_done < num_threads){
				try {
					job_done_object.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void task_done(){
		synchronized(job_done_object){
			
			threads_done += 1;
			if(threads_done == num_threads){
				job_done_object.notifyAll();
			}
		}
	}
	
	
	public static void block_until_transfer(){
		synchronized(transfer_block_object){
			try {
				transfer_block_object.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void block_until_done1(){
		synchronized(job_done_object1){
			if (threads_done1 < num_threads){
				try {
					job_done_object1.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void task_done1(){
		synchronized(job_done_object1){
			
			threads_done1 += 1;
			if(threads_done1 == num_threads){
				job_done_object1.notifyAll();
			
			}
		}
	}
	
	
	
	public static void transfer_done(){
		synchronized(transfer_block_object){
			transfer_block_object.notifyAll();
		}
	}
	
}
