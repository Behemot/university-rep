package server;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

public class ServerTransportThread extends Thread{

private String ip;
private int start,end;
private int input_port;
private int output_port;

public ServerTransportThread(String IP, int Start, int End, int inp_port, int outp_port){
	ip = IP;
	start = Start;
	end = End;
	input_port = inp_port;
	output_port = outp_port;
}

public void run()
{
	ServerDataStorage data_storage = ServerDataStorage.getObject();
	
	try {
		System.out.println("Server transport thread "+start+"-"+end+" started");
		Socket output_socket = new Socket(ip, output_port);
		ServerSocket input_server_socket = new ServerSocket(input_port);
		Socket input_socket;
		DataInputStream input_stream;
		DataOutputStream output_stream = new DataOutputStream(new BufferedOutputStream(output_socket.getOutputStream()));
		output_stream.writeInt(data_storage.getN());
		output_stream.writeInt(end - start);
		System.out.println("Server transport thread "+start+"-"+end+" sending data");
	
		for(int i = 0; i < data_storage.getN(); i++){
			output_stream.writeInt(data_storage.getC(i));
			for(int j = 0; j <data_storage.getN(); j ++){
				output_stream.writeInt(data_storage.getMT(i, j));
			}
		}
		for(int i = start; i < end; i++){
			output_stream.writeInt(data_storage.getB(i));
			for(int j = 0; j < data_storage.getN(); j++){
				output_stream.writeInt(data_storage.getMO(i, j));
				output_stream.writeInt(data_storage.getMX(j, i));
			}
		}
		
		output_stream.flush();
		output_stream.close();
		output_socket.close();
		
		System.out.println("Server transport thread "+start+"-"+end+" waiting for transitional results");

		input_socket = input_server_socket.accept();
		
		input_stream = new DataInputStream(new BufferedInputStream(input_socket.getInputStream()));
		
		System.out.println("Server transport thread "+start+"-"+end+" receiving transitional results");

		while(input_stream.available() == 0){}
		
		for(int i = start; i < end; i++){
			data_storage.setTEMP1(i, input_stream.readInt());
			for(int j = 0; j < data_storage.getN(); j++){
				data_storage.setMTEMP2(j, i, input_stream.readInt());
			}
		}
		
		input_server_socket.close();
		input_stream.close();
		input_socket.close();
		input_server_socket = new ServerSocket(input_port);

		System.out.println("Server transport thread "+start+"-"
				+end+" waiting for other transport threads to get transitional results");
		
		ServerThreadSynchronizer.barrier();
		
		output_socket = new Socket(ip, output_port);
		output_stream = new DataOutputStream(new BufferedOutputStream(output_socket.getOutputStream()));
		
		System.out.println("Server transport thread "+start+"-"+end+" redistributing data");

		for(int i = 0; i < data_storage.getN(); i++){
			output_stream.writeInt(data_storage.getTEMP1(i));
		}
		for(int i = start; i < end; i++){
			for(int j = 0; j < data_storage.getN(); j++){
				output_stream.writeInt(data_storage.getMTEMP2(i, j));
			}
		}
		
		output_stream.flush();
		output_stream.close();
		output_socket.close();

		System.out.println("Server transport thread "+start+"-"+end+" waiting for end results");

		input_socket = input_server_socket.accept();
		input_stream = new DataInputStream(new BufferedInputStream(input_socket.getInputStream()));
		
		while(input_stream.available() == 0){}
		
		System.out.println("Server transport thread "+start+"-"+end+" receiving end results");

		for(int i = start; i < end; i++){
			data_storage.setA(i, input_stream.readInt());
		}
		
		input_stream.close();
		input_socket.close();
		input_server_socket.close();
		System.out.println("Server transport thread "+start+"-"+end+" shutting down");

		ServerThreadSynchronizer.task_done();
		
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

}
