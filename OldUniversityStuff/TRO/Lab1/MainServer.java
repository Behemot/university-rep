package server;


public class MainServer {

	public static void main(String[] args){
		System.out.println("Server started");
		String ip_table[] = {"127.0.0.1", "127.0.0.1"};
		int ports[][] = {{9000, 9001}, {9002, 9003}};
		int num_threads = 1;
		ServerDataStorage data_storage = ServerDataStorage.getObject();
		int h = data_storage.getN() / num_threads;
		ServerThreadSynchronizer.setNumThreads(num_threads);
		System.out.println("Creating server transport threads");
		for(int i = 0; i < num_threads; i++){
			ServerTransportThread t = new ServerTransportThread(ip_table[i],i * h, (i + 1) * h,
					ports[i][0], ports[i][1]);
			t.start();
		}
		System.out.println("Waiting for results");
		ServerThreadSynchronizer.block_until_done();
		System.out.println("Result:");
		for(int i = 0; i < data_storage.getN(); i++){
			System.out.print(data_storage.getA(i)+" ");
		}
		System.out.println();
		System.out.println("Server shutting down");
	}

}
