package client;

public class ClientWorkerThread extends Thread{
	
	int start, end;
	
	public ClientWorkerThread(int Start, int End){
		start = Start;
		end = End;
	}
	
	public void run(){
		System.out.println("Client worker thread "+start+"-"+end+" started");
		System.out.println("Client worker thread "+start+"-"+end+" calculating TEMP_H");
		
		ClientDataStorage data_storage = ClientDataStorage.getObject();
		for(int i = 0; i < data_storage.getH(); i++){
			int temp = 0;
			for(int j = 0; j < data_storage.getN(); j++){
				temp += data_storage.getC(j) * data_storage.getMO(i, j);
			}
			data_storage.setTEMP1(i, temp);
		}
		System.out.println("Client worker thread "+start+"-"+end+" calculating MTEMP_H");
		
		for(int i = 0; i < data_storage.getH(); i++){
			for(int j = 0; j < data_storage.getN(); j++){
				int temp = 0;
				for(int k = 0; k < data_storage.getN(); k++){
					temp += data_storage.getMX(k, i) * data_storage.getMT(j, k);
				}
				data_storage.setMTEMP2(j, i, temp);
			}
		}
		System.out.println("Client worker thread "+start+"-"+end+" waiting for redistributed data");
		
		ClientThreadSynchronizer.task_done();
		ClientThreadSynchronizer.block_until_transfer();
		
		System.out.println("Client worker thread "+start+"-"+end+" calculating A_H");
		
		
		for(int i = 0; i < data_storage.getH(); i++){
			int temp = data_storage.getB(i);
			for(int j = 0; j < data_storage.getN(); j++){
				temp += data_storage.getTEMP3(j) * data_storage.getMTEMP4(i, j);
			}
			data_storage.setA(i, temp);
		}
		
		ClientThreadSynchronizer.task_done1();
		System.out.println("Client worker thread "+start+"-"+end+" shutting down");

	}
}
