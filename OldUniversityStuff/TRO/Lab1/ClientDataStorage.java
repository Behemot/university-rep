package client;

public class ClientDataStorage {
	private static int N = 8;
	private static int H = 2;
	private int[] A,B,C, TEMP1, TEMP3;
	private int[][] MO,MX,MT,MTEMP2, MTEMP4; 

	private static ClientDataStorage object = null;

	static ClientDataStorage getObject(){
		if (object == null)
		{
			object = new ClientDataStorage();
		}
		return object;
	}

	private ClientDataStorage(){
		A = new int[H];
		B = new int[H];
		C = new int[N];
		TEMP1 = new int[H];
		TEMP3 = new int[N];
		MO = new int[N][H];
		MX = new int[H][N];
		MT = new int[N][N];
		MTEMP2 = new int[H][N];
		MTEMP4 = new int [N][H];
		
	}

	void setDimensions(int n, int h){
		N = n;
		H = h;
		object = new ClientDataStorage();
	}

	int getN(){
		return N;
	}
	
	int getH(){
		return H;
	}

	void setA(int index, int value){
		A[index] = value;
	}
	
	void setB(int index, int value){
		B[index] = value;
	}
	
	void setC(int index, int value){
		synchronized(C){	
			C[index] = value;
		}
	}

	void setTEMP1(int index, int value){
			TEMP1[index] = value;
	}

	void setTEMP3(int index, int value){
		synchronized(TEMP3){
			TEMP3[index] = value;
		}
	}
	
	
	void setMTEMP2(int x, int y, int value){
		MTEMP2[y][x] = value;
	}

	
	void setMTEMP4(int x, int y, int value){
		MTEMP4[y][x] = value;
	}

	void setMO(int x, int y, int value){
		MO[y][x] = value;
	}
	
	void setMX(int x, int y, int value){
		MX[y][x] = value;
	}
	
	void setMT(int x, int y, int value){
		synchronized(MT){
			MT[y][x] = value;
		}
	}
	
	int getA(int index){
		return A[index];
	}

	int getB(int index){
		return B[index];
	}

	int getC(int index){
		synchronized(C){
			return C[index];
		}
	}

	int getTEMP1(int index){
			return TEMP1[index];
	}
	
	int getTEMP3(int index){
		synchronized(TEMP3){	
			return TEMP3[index];
		}
	}

	int getMO(int x, int y){
		return MO[y][x];
	}

	int getMX(int x, int y){
		return MX[y][x];
	}

	int getMT(int x, int y){
		synchronized (MT){
			return MT[y][x];
		}
	}

	int getMTEMP2(int x, int y){
			return MTEMP2[y][x];
	}

	int getMTEMP4(int x, int y){
			return MTEMP4[y][x];
	}

}
