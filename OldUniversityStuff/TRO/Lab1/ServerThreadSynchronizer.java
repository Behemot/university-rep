package server;
public class ServerThreadSynchronizer {
	private static Object block_object = new Object();
	private static Object job_done_object = new Object();
	private static int num_threads = 3;
	private static int current_threads = 0;
	private static int threads_done = 0;
	
	public static void barrier(){
		synchronized(block_object){
			
			if (current_threads == num_threads - 1){
				block_object.notifyAll();
				current_threads = 0;
			}
			else{
				current_threads++;
				try {
					block_object.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void setNumThreads(int n){
		num_threads = n;
	}
	
	public static void block_until_done(){
		synchronized(job_done_object){
			try {
				job_done_object.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void task_done(){
		synchronized(job_done_object){
			threads_done++;
			if(threads_done == num_threads){
				job_done_object.notify();
				threads_done = 0;
			}
		}
	}
	
}
