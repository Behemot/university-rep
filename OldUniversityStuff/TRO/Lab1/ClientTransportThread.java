package client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

public class ClientTransportThread extends Thread{
	static String ip = "127.0.0.1";
	static int output_port = 9000;
	static int input_port = 9001;
	static int num_worker_threads = 2;
	
	
	public void run()
	{
		try {
			System.out.println("Client transport thread started");
			ServerSocket input_server_socket = new ServerSocket(input_port);
			Socket input_socket = input_server_socket.accept();
			DataInputStream input_stream = new DataInputStream(new BufferedInputStream(input_socket.getInputStream()));
			
			System.out.println("Client transport thread receiving data");
			
			int N = input_stream.readInt();
			int h = input_stream.readInt();
			
			ClientDataStorage data_storage = ClientDataStorage.getObject();
			data_storage.setDimensions(N, h);
			data_storage = ClientDataStorage.getObject();
			
			for(int i = 0; i < data_storage.getN(); i++){
				System.out.println(input_stream.available());
				data_storage.setC(i,input_stream.readInt());
				for(int j = 0; j <data_storage.getN(); j ++){
					System.out.println(input_stream.available());
					data_storage.setMT(i, j, input_stream.readInt());
				}
			}
			
			for(int i = 0; i < h; i++){
				data_storage.setB(i, input_stream.readInt());
				for(int j = 0; j < data_storage.getN(); j++){
					while(input_stream.available() == 0){
						System.out.println(input_stream.available());
					}
					data_storage.setMO(i, j, input_stream.readInt());
					System.out.println(input_stream.available());
					
					data_storage.setMX(j, i, input_stream.readInt());
				}
			}
			
			input_stream.close();
			input_socket.close();
			input_server_socket.close();
			
			System.out.println("Starting worker threads");
			ClientThreadSynchronizer.setNumThreads(num_worker_threads);
			int h2 = h / num_worker_threads;
			for (int i = 0; i < num_worker_threads; i++){
				ClientWorkerThread worker = new ClientWorkerThread(i * h2, (i + 1) * h2);
				worker.start();
			}
	
			System.out.println("Client transport thread waiting for transitional results");

			ClientThreadSynchronizer.block_until_done();
			
			
			System.out.println("Client transport thread sending transitional results to server");
			
			Socket output_socket = new Socket(ip, output_port);
			DataOutputStream output_stream = new DataOutputStream(new BufferedOutputStream(output_socket.getOutputStream()));
			
			for(int i = 0; i < h; i++){
				output_stream.writeInt(data_storage.getTEMP1(i));
				for(int j = 0; j < data_storage.getN(); j++){
					output_stream.writeInt(data_storage.getMTEMP2(j,i));
				}
			}
			output_stream.flush();
			output_stream.close();
			output_socket.close();
			
			
			System.out.println("Client transport thread waiting for redistributed transitional data");

			
			input_server_socket = new ServerSocket(input_port);
			input_socket = input_server_socket.accept();
			input_stream = new DataInputStream(new BufferedInputStream(input_socket.getInputStream()));
			
			System.out.println("Client transport thread receiving redistributed transitional data");

			while(input_stream.available() == 0){}
			
			for(int i = 0; i < data_storage.getN(); i++){
				data_storage.setTEMP3(i,input_stream.readInt());
			}
			for(int i = 0; i < h; i++){
				for(int j = 0; j < data_storage.getN(); j++){
					data_storage.setMTEMP4(i, j, input_stream.readInt());
				}
			}
			
			input_stream.close();
			input_socket.close();
			input_server_socket.close();
	
			
			ClientThreadSynchronizer.transfer_done();
			System.out.println("Client transport thread waiting for end results");

			ClientThreadSynchronizer.block_until_done1();
			
			
			output_socket = new Socket(ip, output_port);
			output_stream = new DataOutputStream(new BufferedOutputStream(output_socket.getOutputStream()));

			System.out.println("Client transport thread sending end results to server");

			for(int i = 0; i < h; i++){
				output_stream.writeInt(data_storage.getA(i));
			}
			output_stream.flush();
			output_stream.close();
			output_socket.close();
			System.out.println("Client transport thread shutting down");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
