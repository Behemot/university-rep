#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int N = 16;
int a = 1;
int rank;
int p;
int H;
int H2;
int wt = 4;


int* A = NULL;
int* B = NULL;
int* C = NULL;
int* T = NULL;
int* MO = NULL;
int* MX = NULL;
int* MT = NULL;
int* MZ = NULL;

int* A_c = NULL;
int* B_c = NULL;
int* C_c = NULL;
int* T_c = NULL;
int* MO_c = NULL;
int* MX_c = NULL;
int* MT_c = NULL;
int* MZ_c = NULL;

void generateData()
{
    if(rank == 0)
    {
        if(A)
            free(A);
        if(B)
            free(B);
        if(C)
            free(C);
        if(T)
            free(T);
        if(MO)
            free(MO);
        if(MX)
            free(MX);
        if(MT)
            free(MT);
        if(MZ)
            free(MZ);
        A = new int[N];
        B = new int[N];
        C = new int[N];
        T = new int[N];
        MO = new int[N*N];
        MX = new int[N*N];
        MT = new int[N*N];
        MZ = new int[N*N];
        for(int i = 0; i < N; i++)
        {
            B[i] = a;
            C[i] = a;
            for(int j = 0; j < N; j++)
            {
                MO[i * N + j] = a;
                MX[i * N + j] = a;
                MT[i * N + j] = a;
            }
        }
    }
    if(A_c)
        free(A_c);
    if(B_c)
        free(B_c);
    if(C_c)
        free(C_c);
    if(T_c)
        free(T_c);
    if(MO_c)
        free(MO_c);
    if(MX_c)
        free(MX_c);
    if(MT_c)
        free(MT_c);
    if(MZ_c)
        free(MZ_c);
    A_c = new int[H];
    B_c = new int[H];
    C_c = new int[N];
    T_c = new int[H];
    MO_c = new int[N*H];
    MX_c = new int[N*H];
    MT_c = new int[N*N];
    MZ_c = new int[N*H];

}

void transpone(int* mat)
{
    int temp;
    for(int i = 0; i < N; i++)
    {
        for(int j = i+1; j < N; j++)
        {
            temp = mat[i * N + j];
            mat[i * N + j] = mat[j * N + i];
            mat[j * N + i] = temp;
        }
    }
}


int main(int argc, char *argv[])
{
    if(argc == 2)
    {
        N = atoi(argv[1]);
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&p);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    printf("Process %d started \n", rank);
 
    H = N / p;
    
    generateData();

    printf("Scattering initial data process %d \n", rank);
 
    MPI_Scatter(B, H, MPI_INTEGER, B_c, H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    
    if(0 == rank)
    {
        memcpy(C_c, C, N * sizeof(int));
    }
    MPI_Bcast(C_c , N, MPI_INTEGER, 0, MPI_COMM_WORLD);

    MPI_Scatter(MO, N * H, MPI_INTEGER, MO_c, N*H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    MPI_Scatter(MX, N * H, MPI_INTEGER, MX_c, N*H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    
    if(0 == rank)
    {
        memcpy(MT_c, MT, N * N * sizeof(int));
    }
    
    MPI_Bcast(MT_c, N * N, MPI_INTEGER, 0, MPI_COMM_WORLD);

    printf("Process %d calculating phase one \n", rank);
 

    for(int i = 0; i < H; i++)
    {
        T_c[i] = 0;
        for(int j = 0; j < N; j++)
        {
            T_c[i] += C_c[j] * MO_c[i * N + j];
        }
    }
    
    for(int i = 0; i < H; i++)
    {
        for(int j = 0; j < N; j++)
        {
            MZ_c[i * N + j] = 0;
            for(int k = 0; k < N; k++)
            {
                MZ_c[i * N + j] += MX_c[i * N + k] * MT_c[k * N + j];
            }
        }
        
    }

    printf("Process %d distributing mid-results \n", rank);
 
    MPI_Gather(T_c, H, MPI_INTEGER, T, H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    MPI_Gather(MZ_c, N * H, MPI_INTEGER, MZ, N * H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    
    delete T_c;
    T_c = new int[N];
    
    if( 0 == rank)
    {
        memcpy(T_c, T, N * sizeof(int));
    }
    
    MPI_Bcast(T_c, N, MPI_INTEGER, 0, MPI_COMM_WORLD);
    

    if(0 == rank)
    {
        transpone(MZ);
    }

    MPI_Scatter(MZ, N * H, MPI_INTEGER, MZ_c, N * H, MPI_INTEGER, 0, MPI_COMM_WORLD);
    
    printf("Process %d calculating phase two \n", rank);
 
    for(int i = 0; i < H; i++)
    {
        A_c[i] = B_c[i];
        for(int j = 0; j < N; j++)
        {
            A_c[i] += T_c[j] * MZ_c[i * N + j];
        }
    }

    MPI_Gather(A_c, H, MPI_INTEGER, A, H, MPI_INTEGER, 0, MPI_COMM_WORLD);
   
    MPI_Finalize();
    
    printf("Process %d finalizing \n", rank);
 
    if(rank == 0 && N < 21)
    {
        for(int i = 0; i < N; i++)
        {
            printf("%d ",A[i]);
        }
        printf("\n");
    }
}
