package server;

import datastorages.ServerDataStorage;
import monitors.ServerSyncMonitor;


public class Server {
	static int N_ = 500;
	static int p_ = 1;
	static String[] clients = {"127.0.0.1"};
	
	public static void main(String[] args){
		ServerDataStorage.setDimensions(N_, N_/p_);
		for(int i = 0; i < clients.length; i++)
		{
			 ServerIOThread ioThread = new ServerIOThread(clients[i], i);
			 ioThread.start();
		}
		ServerSyncMonitor.waitForJobFinish();
		int[] A = ServerDataStorage.getObject().getA();
		if(N_ < 21){
			for(int i = 0; i < N_; i++){
				System.out.print(A[i]+ " ");
			}
			System.out.println();
		}
		else{
			System.out.println(A[0]);
		}
	}
		
}
