package client;

import monitors.ClientSyncMonitor;
import datastorages.InitialData;
import datastorages.MidCalcOutputStorage;

public class StageOneWorkerThread extends Thread{
	private InitialData data_;
	private MidCalcOutputStorage outputData_;
	private int number_;
	private int total_;
	public StageOneWorkerThread(InitialData data, MidCalcOutputStorage outputData, 
			int number, int total){
		data_ = data;
		number_ = number;
		outputData_ = outputData;
		total_ = total;
	}
	
	public void run(){
		int h2 = data_.getH() / total_;
		int [][] MO = data_.getMO();
		int [][] MX = data_.getMX();
		int [][] MT = data_.getMT();
		int [] C = data_.getC();
 		int [] T = outputData_.getT();
 		int [][] MZ = outputData_.getMZ();
		for(int i = number_ * h2; i < (number_ + 1) * h2; i++){
			T[i] = 0;
			for(int j = 0; j < data_.getN(); j++){
				T[i] += C[j] * MO[i][j]; 
			}
		}
		for(int i = number_ * h2; i < (number_ + 1) * h2; i++){
			for(int j = 0; j < data_.getN(); j++){
				MZ[i][j] = 0;
				for(int k = 0; k < data_.getN(); k++){
					MZ[i][j] += MX[i][k] * MT[k][j];
				}
			}
		}
		ClientSyncMonitor.signal();
	}
}
