package client;

import monitors.ClientSyncMonitor;
import datastorages.MidCalcInputStorage;
import datastorages.OutputData;

public class StageTwoWorkerThread extends Thread{
	private MidCalcInputStorage data_;
	private OutputData outputData_;
	private int number_;
	private int total_;
	public StageTwoWorkerThread(MidCalcInputStorage data, OutputData outputData, 
			int number, int total){
		data_ = data;
		outputData_ = outputData;
		number_ = number;
		total_ = total;
	}
	
	public void run(){
		int h2 = data_.getH() / total_;
		
		int [] A = outputData_.getA();
		int [] B = data_.getB();
		int [] T = data_.getT();
		int [][] MZ = data_.getMZ();
		
		for(int i = number_ * h2; i < (number_ + 1) * h2; i++){
			A[i] = B[i];
			for(int j = 0; j < data_.getN(); j++){
				A[i] += T[j] * MZ[i][j]; 
			}
		}
	
		ClientSyncMonitor.signal2();
	}
}
