package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import datastorages.InitialData;
import datastorages.MidCalcInputStorage;
import datastorages.MidCalcOutputStorage;
import datastorages.OutputData;

public interface Client extends Remote{
	public MidCalcOutputStorage stageOneCalc(InitialData data) throws RemoteException;
	public OutputData stageTwoCalc(MidCalcInputStorage data) throws RemoteException;
}
