package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import datastorages.InitialData;
import datastorages.MidCalcInputStorage;
import datastorages.MidCalcOutputStorage;
import datastorages.OutputData;
import datastorages.ServerDataStorage;

import monitors.ServerSyncMonitor;
import interfaces.Client;

public class ServerIOThread extends Thread{
	String clientIP_;
	int number_;
	
	public ServerIOThread(String clientIP, int number)
	{
		clientIP_ = clientIP;
		number_ = number;
	}
	
	public void run()
	{
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(clientIP_);
            Client client = (Client) registry.lookup("client");
            ServerDataStorage dataStorage = ServerDataStorage.getObject();
            System.out.println(dataStorage.getN() + " - " + dataStorage.getH());
            InitialData initData = new InitialData(dataStorage.getN(), dataStorage.getH());
            int [] C = dataStorage.getC();
            int [] Ch = initData.getC();
            int [][] MO = dataStorage.getMO();
            int [][] MOh = initData.getMO();
            int [][] MX = dataStorage.getMX();
            int [][] MXh = initData.getMX();
            int [][] MT = dataStorage.getMT();
            int [][] MTh = initData.getMT();
            
            for(int i = 0; i < dataStorage.getH(); i++){
            	for(int j = 0; j < dataStorage.getN(); j++){
            		MOh[i][j] = MO[i + dataStorage.getH() * number_][j];
            		MTh[i][j] = MT[i + dataStorage.getH() * number_][j];
            	}
            }
            for(int i = 0; i < dataStorage.getN(); i++){
            	Ch[i] = C[i];
            	for(int j = 0; j < dataStorage.getN(); j++){
            		MXh[i][j] = MX[i][j];
            	}
            }
            
            MidCalcOutputStorage midOutput = client.stageOneCalc(initData);
            
            int[] Th = midOutput.getT();
            int[][] MZh = midOutput.getMZ();
            int[] T = dataStorage.getT();
            int[][] MZ = dataStorage.getMZ();
            
            for(int i = 0; i < dataStorage.getH(); i++){
            	T[i + number_* dataStorage.getH()] = Th[i];
                for(int j = 0; j < dataStorage.getN(); j++){
                	MZ[i + number_*dataStorage.getH()][j] = MZh[i][j];
            	}
            }
            
            ServerSyncMonitor.barrier();
            
            MidCalcInputStorage midInput = new MidCalcInputStorage(dataStorage.getN(), dataStorage.getH());
            int [] Bh = midInput.getB();
            int [] B = dataStorage.getB();
            int [][] MZh2 = midInput.getMZ(); 
            int [] T2 = midInput.getT();
            for(int i = 0; i < dataStorage.getH(); i++){
              	Bh[i] = B[i + dataStorage.getH() * number_];
              	for(int j = 0; j < dataStorage.getN(); j++){
              		MZh2[i][j] = MZ[i + number_*dataStorage.getH()][j];
              	}
            }
            
            for(int i = 0; i < dataStorage.getN(); i++){
            	T2[i] = T[i];
            }
                        
            OutputData output = client.stageTwoCalc(midInput);
            int[] Ah = output.getA();
            int[] A = dataStorage.getA();
            for(int i = 0; i < dataStorage.getH(); i++){
            	A[i + number_ * dataStorage.getH()] = Ah[i];
            }
            ServerSyncMonitor.signal();
        } catch (Exception e) {
            System.err.println("Failed to connect");
            e.printStackTrace();
        }
	}
}
