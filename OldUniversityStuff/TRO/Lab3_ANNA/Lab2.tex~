\documentclass[a4paper,12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[english,russian,ukrainian]{babel}
\usepackage[T2A]{fontenc}
\usepackage{indentfirst}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{titlesec}
\usepackage{titletoc}
\usepackage{listings}
\usepackage{lscape}
\usepackage{float}
\usepackage{listings}

\textwidth=6.9in
\textheight=10in
\oddsidemargin=0.7in
\voffset=-1in
\hoffset=-1in

\pagestyle{plain}

\newcommand{\titletop}{%
Національний технічний університет України ``Київський політехнічний інститут''\par
Факультет інформатики та обчислювальної техніки\par
Кафедра обчислювальної техніки}
\newcommand{\titlelineI}{Лабораторна робота №3\par
з курсу ``Теорія розподілених обчилень''}
\newcommand{\titlelineIII}{Студентки:\par
Вдовенко А.С.\par
ФІОТ гр. ІО-71\par
\vspace{5mm}
}
\newcommand{\titlebottom}{Київ 2011}

\renewcommand{\maketitle}
{
  \thispagestyle{empty}
  \parbox[c][0.3\vsize][t]{\hsize}
  {\begin{center}\fontsize{13pt}{15pt}\selectfont\titletop\end{center}}

  \parbox[c][0.1\vsize][t]{\hsize}
  {\begin{center}\fontfamily{fma}\fontsize{18pt}{20pt}\selectfont\titlelineI\end{center}}


  \parbox[t][0.3\vsize][t]{\hsize}
  {
    \parbox[t]{0.4\hsize}{\raggedright}\hfill
    \parbox[t]{0.4\hsize}{\raggedright\fontsize{14pt}{15pt}\selectfont\titlelineIII}
  }

  \vfill
  \begin{center}\fontsize{13pt}{14pt}\selectfont\titlebottom\end{center}
}

%opening
\author{Anthony Mykhailenko}


\begin{document}
\maketitle
\pagebreak
\section*{Лабораторная работа №3. Java RMI}
\section{Техническое задание на лабораторную работу}
Разработать программу с использованием механизма Java RMI для распределенной системы (Рис. \ref{fig1}) для параллельного выполнения заданной функции:
\[
    MX = MA + MB \cdot MC \cdot MT;
\]
Разработать параллельный алгоритм, алгоритмы клиента и сервера, схему взаимодействия узлов.
\begin{figure}[H]
\includegraphics[width=0.5\linewidth]{Net.png}
\caption{Модель распределенной системы}
\label{fig1}
\end{figure}

\section{Выполнение лабораторной работы}
\subsection{Разработка параллельного математического алгоритма}
Математический алгоритм:
\[
    MX_H = MA_H + MB_H \cdot MC \cdot MT;
\]
\textbf{Общие ресурсы:} MC, MT, \\
где $H = \dfrac{N}{p \cdot C}$, $N$ - размерность задачи, $p$ - количество машин в распределенной системе, $C$ - количество процессоров на каждой машине.

\newpage


\subsection{Разработка алгоритмов сервера и клиента}
\begin{description}
    \item[Сервер]
    \begin{enumerate}
        \item Ввод $MA, MB, MC, MT$.
        \item Отправить клиенту $K_i$ $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Принять от клиента $K_i$ $MX_{C \cdot H}$.
        \item Вывод $MX$
    \end{enumerate}
    \textbf{Счет на серверной машине производиться путём запуска дополнительного клиентского процесса на ней}
    \item[Клиент]
    \begin{enumerate}
        \item Получить от сервера $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Счет $MX_{C \cdot H}$.
        \item Отправить серверу $MX_{C \cdot H}$.
    \end{enumerate}

\end{description}

\subsection{Разработка схемы взаимодействия клиента и сервера}

\begin{figure}[H]
\includegraphics[width=0.8\linewidth]{Interaction.png}
\caption{Схема взаимодействия клиента и сервера}
\label{fig2}
\end{figure}

\subsection{Разработка структуры сервера и клиента}
\begin{figure}[H]
\includegraphics[width=0.8\linewidth]{Interaction2.png}
\caption{Структура сервера и клиентов}
\label{fig3}
\end{figure}

\subsection{Разработка алгоритмов потоков}
\subsubsection*{Потоки в сервере}
\begin{description}
    \item[Main]
    \begin{enumerate}
        \item Ввод $MA, MB, MC, MT$.
	\item Для каждого из $p$ узлов создать поток ввода-вывода $IO_{K_i}$.
        \item Ждать окончания потоков $IO_{K_i}$.
        \item Вывод $MX$.
    \end{enumerate}
    \item[IOki]
    \begin{enumerate}

       	\item Соединиться с реестром удаленных объектов клиента.
        \item Получить объект клиента из реестра.
        \item Сформировать $K_i$ $MA_{C \cdot H}, MB_{C \cdot H}, MC, MT$.
        \item Вызвать удалённый метод calculate клиента $K_i$ .
        \item Ждать пока все потоки $IO_{K_i}$ закончат вызов метода calculate.
        \item Записать $MX_{C \cdot H}$ в $MX$.

    \end{enumerate}
\end{description}

\subsubsection*{Потоки в клиенте}
\begin{description}
    \item[Main]
    \begin{enumerate}
         \item Соединиться с реестром удаленных объектов клиента.
        \item Зарегистрировать объект клиента.
    \end{enumerate}
    \item[Ti]
    \begin{enumerate}
         \item Запустить вычислительные задачи.
        \item Ждать окончания потоков.
        \item Вернуть как результат $MX_{C \cdot H}$.
    \end{enumerate}

\end{description}


Для контроля за данными используються два вида мониторов - ServerDataStorage и ClientDataStorage.
\begin{description}
\item[ServerDataStorage] - монитор, отвечающий за доступ к данным на сервере.
Методы, относящиеся к защите общих ресурсов:
\item[ClientDataStorage] - монитор, отвечающий за доступ к данным на клиенте.
\end{description}
\newpage

\subsection{Листинг программы}
\lstset{language=Java, basicstyle=\small, commentstyle=\tiny,numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstinputlisting[breaklines=true,columns=flexible,caption=Client.java]{Client.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=Server.java]{Server.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=StageOneWorkerThread.java]{ServerIOThread.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=ClientImplementation.java]{ClientImplementation.java}
\lstinputlisting[breaklines=true,columns=flexible,caption=StageOneWorkerThread.java]{StageOneWorkerThread.java}

\end{document}
