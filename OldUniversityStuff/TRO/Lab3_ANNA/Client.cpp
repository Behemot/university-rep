#define _WIN32_WINNT 0x501
            
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#define DEFAULT_PORT "22"
char* server_address = "192.168.1.2";

int* MX = NULL;
int* MZ = NULL;
int* MA = NULL;
int* MB = NULL;
int* MC = NULL;
int* MT = NULL;

int N;
int H;
int p = 4; //Number of worker threads
int H2;

HANDLE* mid_calc_semaphores;
HANDLE* job_completed_semaphores;
HANDLE* transfer_done;

void prepareStorage()
{
    MX = new int[N*H];
    MZ = new int[N*H];
    MA = new int[N*H];
    MB = new int[N*H];
    MC = new int[N*N];
    MT = new int[N*N];
}

DWORD WINAPI workerThread(LPVOID par)
{
    int params = *((int*)par);
    printf("Worker thread %d - %d started\n", params, params+H2 );

    
    for(int i = params; i < params + H2; i++)
    {
        for(int j = 0; j < N; j++)
        {
            MZ[i * N + j] = 0;
            for(int k = 0; k < N; k++)
            {
                MZ[i * N + j] += MB[i * N + k] * MC[k * N + j];
            }
        }
        
    }
    
    for(int i = params; i < params + H2; i++)
    {
        for(int j = 0; j < N; j++)
        {
            MX[i * N + j] = MA[i * N + j];
            for(int k = 0; k < N; k++)
            {
                MX[i * N + j] += MZ[i * N + k] * MT[k * N + j];
            }
        }
        
    }


    ReleaseSemaphore(job_completed_semaphores[params / H2], 1, NULL);
    printf("Worker thread %d - %d closing\n", params, params+H2);
}


int main(int argc, char* argv[])
{
    WSADATA wsaData;
    int iResult;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(1,1), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }
    
    struct addrinfo *result = NULL,
                *ptr = NULL,
                hints;

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(server_address, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    SOCKET ConnectSocket = INVALID_SOCKET;

    // Attempt to connect to the first address returned by
    // the call to getaddrinfo
    ptr=result;

    // Create a SOCKET for connecting to server
    ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    
    if (ConnectSocket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // Connect to server.
    iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        closesocket(ConnectSocket);
        ConnectSocket = INVALID_SOCKET;
    }

    // Should really try the next address returned by getaddrinfo
    // if the connect call failed
    // But for this simple example we just free the resources
    // returned by getaddrinfo and print an error message

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }
    
    int iSendResult,iRecvResult;

    printf("Client receiving data\n");
    iRecvResult = recv(ConnectSocket, (char*)&N , sizeof(int), 0);
    iRecvResult = recv(ConnectSocket, (char*)&H, sizeof(int), 0);

     
    H2 = H / p;
    
    prepareStorage();


    int received = 0;
    char* buf = (char*)MA;
    while (received < H * N * sizeof(int))
    {
        iRecvResult = recv(ConnectSocket, buf, H * N * sizeof(int) - received, 0);
        received +=iRecvResult;
        buf = (char*)MA + received;
    }


    received = 0;
    buf = (char*)MB;
    while (received < H * N * sizeof(int))
    {
        iRecvResult = recv(ConnectSocket, buf, H * N * sizeof(int) - received, 0);
        received +=iRecvResult;
        buf = (char*)MB + received;
    }


    received = 0;
    buf = (char*)MC;
    while (received < N * N * sizeof(int))
    {
        iRecvResult = recv(ConnectSocket, buf, N * N * sizeof(int) - received, 0);
        received +=iRecvResult;
        buf = (char*)MC + received;
    }

    received = 0;
    buf = (char*)MT;
    while (received < N * N * sizeof(int))
    {
        iRecvResult = recv(ConnectSocket, buf, N * N * sizeof(int) - received, 0);
        received +=iRecvResult;
        buf = (char*)MT + received;
    }


    printf("Data received. Starting worker threads\n");
    job_completed_semaphores = new HANDLE[p];
    for(int i = 0; i < p; i++)
    {
        job_completed_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
    }
   
    HANDLE* workerThreads = new HANDLE[p];
    int* params = new int[p];
    for (int i = 0; i < p; i++ )
    {
        params[i] = i * H2;
        workerThreads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) &workerThread,&(params[i]), 0, NULL);
    }

    WaitForMultipleObjects(p, job_completed_semaphores, true, INFINITE);

    printf("Sending end results\n");


    int sent = 0;
    const char* send_buf = (const char*)(MX);
    while(sent < H * N * sizeof(int))
    {
        iSendResult = send(ConnectSocket, send_buf, H * N * sizeof(int) - sent, 0);
        sent += iSendResult;
        send_buf = (const char*)(MX) + sent;
    }

  
    closesocket(ConnectSocket);
}
