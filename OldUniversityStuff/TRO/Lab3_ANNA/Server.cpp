#define _WIN32_WINNT 0x501
            
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#define DEFAULT_PORT "22"

int* MX = NULL;
int* MA = NULL;
int* MB = NULL;
int* MC = NULL;
int* MT = NULL;

int N = 4800;
int a = 1;
int p = 1; //Number of clients
int H = N / p;

HANDLE* job_completed_semaphores;

struct thread_parameters
{
    int start;
    int end;
    SOCKET io_socket;
};

void generateData()
{
    MX = new int[N*N];
    MA = new int[N*N];
    MB = new int[N*N];
    MC = new int[N*N];
    MT = new int[N*N];

    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < N; j++)
        {
            MA[i * N + j] = a;
            MB[i * N + j] = a;
            MC[i * N + j] = a;
            MT[i * N + j] = a;
        }
    }
}

void transpone(int* mat)
{
    int temp;
    for(int i = 0; i < N; i++)
    {
        for(int j = i+1; j < N; j++)
        {
            temp = mat[i * N + j];
            mat[i * N + j] = mat[j * N + i];
            mat[j * N + i] = temp;
        }
    }
}

DWORD WINAPI IOThread(LPVOID par)
{
    thread_parameters params = *((thread_parameters*)par);
    
    int iSendResult,iRecvResult;
    
    printf("Thread %d - %d sending data \n", params.start, params.end);

    iSendResult = send(params.io_socket, (const char*)(&N) , sizeof(int), 0);
   
    iSendResult = send(params.io_socket, (const char*)(&H), sizeof(int), 0);
    
    int sent = 0;
    const char* buf = (const char*)(MA + params.start * N);
    while(sent < H * N * sizeof(int))
    {
        iSendResult = send(params.io_socket, buf, H * N * sizeof(int) - sent, 0);
        sent += iSendResult;
        buf = (const char*)(MA + params.start * N) + sent;
    }
   
    sent = 0;
    buf = (const char*)(MB + params.start * N);
    while(sent < H * N * sizeof(int))
    {
        iSendResult = send(params.io_socket, buf, H * N * sizeof(int) - sent, 0);
        sent += iSendResult;
        buf = (const char*)(MB + params.start * N) + sent;
    }
  
    sent = 0;
    buf = (const char*)(MC);
    while(sent < N * N * sizeof(int))
    {
        iSendResult = send(params.io_socket, buf, N * N * sizeof(int) - sent, 0);
        sent += iSendResult;
        buf = (const char*)(MC) + sent;
    }

    sent = 0;
    buf = (const char*)(MT);
    while(sent < N * N * sizeof(int))
    {
        iSendResult = send(params.io_socket, buf, N * N * sizeof(int) - sent, 0);
        sent += iSendResult;
        buf = (const char*)(MT) + sent;
    }

       
    int received = 0;
    char* received_buf = (char*)(MX + params.start * N);
    while(received < N * H * sizeof(int))
    {
        iRecvResult = recv(params.io_socket, received_buf, N * H * sizeof(int) - received, 0);
        received += iRecvResult;
        received_buf = (char*)(MX + params.start * N) + received;
     }

    
    ReleaseSemaphore(job_completed_semaphores[params.start / H], 1, NULL);
    
    printf("Thread %d - %d complete \n", params.start, params.end);

    closesocket(params.io_socket);
}

int main(int argc, char* argv[])
{
    if(argc == 2)
    {
        N = atoi(argv[1]);
        H = N / p;
    }
    
    generateData(); 
    WSADATA wsaData;
    int iResult;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(1,1), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    struct addrinfo *result = NULL, *ptr = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the local address and port to be used by the server
    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    SOCKET ServerSocket = INVALID_SOCKET;
    // Create a SOCKET for the server to listen for client connections
    ServerSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if (ServerSocket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // Setup the TCP listening socket
    iResult = bind( ServerSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        printf("bind failed with error: %d\n", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(ServerSocket);
        WSACleanup();
        return 1;
    }

    
    if ( listen( ServerSocket, SOMAXCONN ) == SOCKET_ERROR ) {
        printf( "Listen failed with error: %ld\n", WSAGetLastError() );
        closesocket(ServerSocket);
        WSACleanup();
        return 1;
    }



    job_completed_semaphores = new HANDLE[p];
    for(int i = 0; i < p; i++)
    {
        job_completed_semaphores[i] = CreateSemaphore(NULL, 0, 1, NULL);
    }
    
    HANDLE* IOThreads = new HANDLE[p];
    SOCKET IOSocket[p];
    for(int i = 0; i < p; i++)
    {
        IOSocket[i] = INVALID_SOCKET;
        IOSocket[i] = accept(ServerSocket, NULL, NULL);
        if (IOSocket[i] == INVALID_SOCKET) {
            printf("accept failed: %d\n", WSAGetLastError());
            closesocket(ServerSocket);
            WSACleanup();
            return 1;
        }
        thread_parameters params = {i * H, (i + 1) * H, IOSocket[i]};
        IOThreads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) &IOThread,(LPVOID) &params, 0, NULL);
    }
    
    WaitForMultipleObjects(p, job_completed_semaphores, true, INFINITE);

    if (N < 21)
    {
        for(int i = 0; i < N; i++)
        {
            for(int j = 0; j < N; j++)
            {
                printf("%d ", MX[i]);
            }
            printf("\n");
        }
        printf("\n");
    }
    else 
    {
        printf("Result %d\n",MX[0]);
    }

}
