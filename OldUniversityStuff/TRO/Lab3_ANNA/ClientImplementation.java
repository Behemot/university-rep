package client;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import monitors.ClientSyncMonitor;

import datastorages.InitialData;
import datastorages.OutputData;
import interfaces.Client;

public class ClientImplementation extends UnicastRemoteObject implements Client{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int p = 2;
	
	protected ClientImplementation() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args)
	{
		try {
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("client" , new ClientImplementation());
			System.out.println("Client Object Exported");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public OutputData calculate(InitialData data)
			throws RemoteException {
		ClientSyncMonitor.setP(p);
		OutputData output = new OutputData(data.getN(), data.getH());
		for(int i = 0; i < p; i++){
			StageOneWorkerThread worker = new StageOneWorkerThread(data, output, i, p);
			worker.start();
		}
		ClientSyncMonitor.waitForJobFinish();
		System.out.println("Stage One finished");
		return output;
	}

}
