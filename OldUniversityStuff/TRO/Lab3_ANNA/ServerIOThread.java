package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import datastorages.InitialData;
import datastorages.OutputData;
import datastorages.ServerDataStorage;

import monitors.ServerSyncMonitor;
import interfaces.Client;

public class ServerIOThread extends Thread{
	String clientIP_;
	int number_;
	
	public ServerIOThread(String clientIP, int number)
	{
		clientIP_ = clientIP;
		number_ = number;
	}
	
	public void run()
	{
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(clientIP_);
            Client client = (Client) registry.lookup("client");
            ServerDataStorage dataStorage = ServerDataStorage.getObject();
            System.out.println(dataStorage.getN() + " - " + dataStorage.getH());
            InitialData initData = new InitialData(dataStorage.getN(), dataStorage.getH());
    
            int [][] MA = dataStorage.getMA();
            int [][] MAh = initData.getMA();
            int [][] MB = dataStorage.getMB();
            int [][] MBh = initData.getMB();
            int [][] MC = dataStorage.getMC();
            int [][] MCh = initData.getMC();
            int [][] MT = dataStorage.getMT();
            int [][] MTh = initData.getMT();
            
            for(int i = 0; i < dataStorage.getH(); i++){
            	for(int j = 0; j < dataStorage.getN(); j++){
            		MAh[i][j] = MA[i + dataStorage.getH() * number_][j];
            		MBh[i][j] = MB[i + dataStorage.getH() * number_][j];
            	}
            }
            for(int i = 0; i < dataStorage.getN(); i++){
            	for(int j = 0; j < dataStorage.getN(); j++){
            		MCh[i][j] = MC[i][j];
            		MTh[i][j] = MT[i][j];
            	}
            }
            
            OutputData output = client.calculate(initData);
            
            int[][] MXh = output.getMX();
            int[][] MX = dataStorage.getMX();
            for(int i = 0; i < dataStorage.getH(); i++){
            	for(int j = 0; j < dataStorage.getN(); j++){
            		MX[i + dataStorage.getH() * number_][j] = MXh[i][j];
            	}
            }
            ServerSyncMonitor.signal();
        } catch (Exception e) {
            System.err.println("Failed to connect");
            e.printStackTrace();
        }
	}
}
