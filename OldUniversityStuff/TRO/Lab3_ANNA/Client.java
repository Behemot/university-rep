package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import datastorages.InitialData;
import datastorages.OutputData;

public interface Client extends Remote{
	public OutputData calculate(InitialData data) throws RemoteException;
}
