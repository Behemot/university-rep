package client;

import monitors.ClientSyncMonitor;
import datastorages.InitialData;
import datastorages.OutputData;

public class StageOneWorkerThread extends Thread{
	private InitialData data_;
	private OutputData outputData_;
	private int number_;
	private int total_;
	public StageOneWorkerThread(InitialData data, OutputData outputData, 
			int number, int total){
		data_ = data;
		number_ = number;
		outputData_ = outputData;
		total_ = total;
	}
	
	public void run(){
		int h2 = data_.getH() / total_;
		int [][] MA = data_.getMA();
		int [][] MB = data_.getMB();
		int [][] MC = data_.getMC();
		int [][] MT = data_.getMT();
		int [][] MZ = data_.getMZ();
		int [][] MX = outputData_.getMX();
		
		for(int i = number_ * h2; i < (number_ + 1) * h2; i++){
			for(int j = 0; j < data_.getN(); j++){
				MZ[i][j] = 0;
				for(int k = 0; k < data_.getN(); k++){
					MZ[i][j] += MB[i][k] * MC[k][j];
				}
			}
		}
		
		ClientSyncMonitor.barrier();
		
		for(int i = number_ * h2; i < (number_ + 1) * h2; i++){
			for(int j = 0; j < data_.getN(); j++){
				MX[i][j] = MA[i][j];
				for(int k = 0; k < data_.getN(); k++){
					MX[i][j] += MZ[i][k] * MT[k][j];
				}
			}
		}
		ClientSyncMonitor.signal();
	}
}
