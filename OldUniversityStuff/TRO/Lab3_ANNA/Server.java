package server;

import datastorages.ServerDataStorage;
import monitors.ServerSyncMonitor;


public class Server {
	static int N_ = 1200;
	static int p_ = 1;
	static String[] clients = {"127.0.0.1"};
	
	public static void main(String[] args){
		ServerDataStorage.setDimensions(N_, N_/p_);
		for(int i = 0; i < clients.length; i++)
		{
			 ServerIOThread ioThread = new ServerIOThread(clients[i], i);
			 ioThread.start();
		}
		ServerSyncMonitor.waitForJobFinish();
		int[][] MX = ServerDataStorage.getObject().getMX();
		if(N_ < 21){
			for(int i = 0; i < N_; i++){
				for(int j = 0; j < N_; j++){
					System.out.print(MX[i][j]+ " ");
				}
				System.out.println();
			}
			System.out.println();
		}
		else{
			System.out.println(MX[0][0]);
		}
	}
		
}
